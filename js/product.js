//
$(document).ready(function() {
    reflush("auto");

    //ウォッチリストに追加する処理
    $("#add_watch_button").click(function(){
        var src=$("#add_watch_image").attr('src');
        var imageName=src.substr(src.lastIndexOf("/")+1);
        var validate = $("#validate").text();
        if(imageName=="add_watch_finish.png")
            return false;
        var url = $(this).attr('href');
        $.ajax({
            url : url+"&validate="+validate,
            type : "POST",
            dataType : 'text',
            cache : false,
            success : function(data){
                if(data.indexOf("ok")!=-1){
                    src=src.replace(imageName,"") + "add_watch_finish.png";
                    $("#add_watch_image").attr('src',src);
                }
            }
        });
        return false;
    });

    
    // bidding
    $(".bid_button").click(function() {
        var url = $(this).attr('href');
        var id=url.split("=")[1];
        var validate = $("#validate").text();
        $("#button_index_page_"+id).hide(1);
        $("#bid_loading_"+id).show(1);
        $.ajax( {
            url : url+"&validate="+validate,
            dataType : 'json',
            cache : false,
            type : "POST",
            success : function(data) {
                if(data.message){
                    $("#result_"+id).html(data.message).show(1).animate({
                        opacity: 1.0
                    }, 2000).hide(1);
                    $("#bid_loading_"+id).hide(1);
                    $("#button_index_page_"+id).show(1);
                }
                else{
                    $("#result_"+id).html("入札しました！").show(1).animate({
                        opacity: 1.0
                    }, 2000).hide(1);
                    $("#member_bid_count").html(data.bid_count);
                    $("#coin_count").html(data.coin);
                    $("#user_"+id).html(data.last_bidder);
                    $("#price_"+id).html(data.price);
                    $("#bid_loading_"+id).hide(1);
                    $("#button_index_page_"+id).show(1);
                    $("#discount_price_"+id).html(data.discount_price);
                    $("#discount_rate_"+id).html(data.discount_rate);
                    $("#my_bid_count_"+id).html(data.product_bid_count);
                    setHighlight(id);
                    setTime(id,data.time);
                }
                
            }
            
        });
        
        return false;
    });

    var img=$("#pro_img > tbody > tr > td >img");
    for (i = 0; i < img.length; i++) {
        var src=$(img[i]).attr("src");
        if(!src)
            $(img[i]).attr("src",$("#no_image").attr("src"));
        else{
            $(img[i]).click(function () {
                $("img[name='prdimg']").attr("src",$(this).attr("src"));
            });
        }
    }
    /*
    //auto bid setting
    $("form").submit(function() {
        var url=$(this).attr("action");
        $.ajax( {
            url : url,
            type: 'POST',
            data: "price_from="+$("#price_from").attr("value")+"&price_to="+$("#price_to").attr("value")+"&bid_count="+$("#bid_count").attr("value"),
            dataType : 'json',
            success : function(data) {
                if(data.message){
                    $("#result").html(data.message).show(1).animate({
                        opacity: 1.0
                    }, 5000).hide(1);
                    
                }
            }
        });
    });
     */

});


function setTime(id,seconds){
    $("#time_"+id).html(getStringTime(seconds));
    if(seconds<10){
        if(seconds>0)
            $("#time_" + id).css('color', '#ff0000');
        else
            $("#bid_button_"+id).hide();
    }
    else
        $("#time_" + id).removeAttr('style');
}

//function push() {
//    auto("push");
//}

function setAutoBid(){
    
}


function reflush(){
    auto("auto");
}

function auto(method) {
    var id = $("#product_main").attr('class');
    if(!id)
        return;
    setInterval(function(){
        $.ajax( {
            url : "/index.php/product/" + method,
            type: 'POST',
            data: "id="+id,
            dataType : 'json',
            cache : false,
            success : function(data){
                var id = data.id;
                if(data.message){
                    $("#time_" + id).css('color', '#ff0000');
                    $("#time_" + id).css('font-size', '16px');
                    $("#time_" + id).html(data.message);
                }
                else{
                    var sec = $("#time_" + id).attr("sec");
                    if(!sec)
                        $("#time_" + id).attr("sec",data.sec);
                    else{
                        if(sec >= data.sec)
                            return;
                        else
                            $("#time_" + id).attr("sec",data.sec);
                    }

                    $("#price_" + id).html(data.price);
                    $("#time_" + id).html(
                    getStringTime(data.time));
                    if($("#user_"+id).html() !=data.user){
                        $("#user_"+id).html(data.user);
                        setHighlight(id);
                    }
                    if(data.time<10){
                        if(data.time>0)
                            $("#time_" + id).css('color', '#ff0000');
                        else
                            $("#bid_button_"+id).hide();
                    }
                    else
                        $("#time_" + id).removeAttr('style');
                    
                    $("#discount_price_"+id).html(data.discount_price);
                    $("#discount_rate_"+id).html(data.discount_rate);
                    setTime(id,data.time);
                    var logs=data.logs;
                    if(logs.length>0){
                        var log_ht =getBidLogTag(logs);　
                        $(".bid_log_stats").html(log_ht);
                    }
                }
            }
            
        });

},1000);
}

function getStringTime(diff) {
if (diff > 0) {
    hours=Math.floor(diff / 3600)
    minutes=Math.floor((diff / 3600 - hours) * 60)
    seconds=Math.round((((diff / 3600 - hours) * 60) - minutes) * 60)
} else {
    hours = 0;
    minutes = 0;
    seconds = 0;
    return "処理中";
}
if (seconds == 60)
    seconds = 0;

if (minutes < 10) {
    if (minutes < 0)
        minutes = 0;
    minutes = '0' + minutes;
}
if (seconds < 10) {
    if (seconds < 0)
        seconds = 0;
    seconds = '0' + seconds;
}
if (hours < 10) {
    if (hours < 0)
        hours = 0;
    hours = '0' + hours;
}
return hours + ":" + minutes + ":" + seconds;
}

/**
* 商品をウォッチリストに追加する処理
*/
function addwatch(url){

$.ajax( {
    url : url,
    cache : false,
    dataType : 'text',
    success : function(data) {
        if(data=="ok"){
            //イメージ変更処理
        }
    }
});
}