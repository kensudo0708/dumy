<?php
ob_start();
$start = microtime(true);
define('FRAMEWORK_DIR', 'mvc');
require(FRAMEWORK_DIR."/load.php");
ActionService::init();
ActionService::service();
$limit = intval(constant('SLOW_PROCESS'));
if ( $limit >= 0 ) {
    $exectime = round( microtime(true) - $start, 2);
    if ( true || $exectime > $limit ) {
        Logger::warn('slow process('.$exectime.'s). '.$_SERVER['REQUEST_URI'].' '.$_SERVER['HTTP_USER_AGENT']);
        // @see ActionService.service()
        if( Config::value("LOG_RECORD") ) {
            Logger::record();
        }
    }
}
ob_end_flush();
?>
