<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PHPAppender
 *
 * @author mw
 */
class PHPAppender implements Appender {
    private static $content=array();

    //put your code here
    public function log($message) {
        array_push(self::$content, $message);
    }
    public function record($type, $destination, $extra_headers) {
        error_log(implode("",self::$content), $type,$destination ,$extra_headers);
        self::$content = array();
    }
}
?>
