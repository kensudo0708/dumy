<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ログを記録するクラス
 *
 * @author mw
 */
final class Logger {
    //put your code here
    const FATAL="FATAL";        //致命的なエラー。プログラムの異常終了を伴うようなもの
    const ERROR="ERROR";        //エラー。予期しないその他の実行時エラー。
    const WARN="WARN";         //警告
    const INFO="INFO";          //情報
    const DEBUG="DEBUG";       //デバッグ用の情報

    const SYSTEM = 0;
    const MAIL= 1;
    const FILE = 3;

    private static $levels;
    private static $content=array();
    private static $format="[Y/m/d H:i:s]";

    /**
     * 初期化
     */
    public static function init(){
        self::$levels=explode(",", Config::value("LOG_LEVEL"));

    }

    private static function log($message,$level,$force){
        if($force || in_array($level,self::$levels)){
            $now = date(self::$format);
            array_push(self::$content, "{$now} {$level}: {$message}\r\n");
        }
    }

    //ログ内容をレコードする
    public static function record($type=self::FILE,$destination='',$extra_headers=''){
        if(count(self::$content)==0)
            return ;
        if(empty($destination))
            $destination = Config::value("LOG_DIR").Config::value("LOG_FILE_NAME").date('y_m_d').".log";
        if(self::FILE == $type) {
            if(is_file($destination) && floor(Config::value('LOG_FILE_SIZE')) <= filesize($destination) )
                  rename($destination,dirname($destination).'/'.basename($destination,".log").'-'.time().".log");
        }
        error_log(implode("",self::$content), $type,$destination ,$extra_headers);
        self::$content = array();
    }


    /**
     *情報レベルのログ
     * @param <String> $message
     * @param <boolean> $force 強制的に記録するかどうか、示す
     */
    public static function info($message,$force=false){
        self::log($message, self::INFO,$force);
    }

    /**
     *致命的なエラーレベルのログ
     * @param <String> $message
     * @param <boolean> $force 強制的に記録するかどうか、示す
     */
    public static function fatal($message,$force=false){
        self::log($message, self::FATAL,$force);
    }

    /**
     *致命的なエラーレベルのログ
     * @param <String> $message
     * @param <boolean> $force 強制的に記録するかどうか、示す
     */
    public static function error($message,$force=false){
        self::log($message, self::ERROR,$force);
    }

    /**
     *警告レベルのログ
     * @param <String> $message
     * @param <boolean> $force 強制的に記録するかどうか、示す
     */
    public static function warn($message,$force=false){
        self::log($message, self::WARN,$force);
    }

    /**
     *デバッグレベルのログ
     * @param <String> $message
     * @param <boolean> $force 強制的に記録するかどうか、示す
     */
    public static function debug($message,$force=false){
        self::log($message, self::DEBUG,$force);
    }
}
?>
