<?php

return  array (

/*  ACTION   */
'DEFAULT_ACTION'=>'index',		//アクションが指定しない場合、デフォルトアクション名
'DEFAULT_METHOD'=>'execute',            //メソッドが指定しない場合、デフォルトメソッド名
'ACTION_DIR'=>APP_PATH.'/classes/action',
'ACTION_BEFORE_METHOD'=>'before',
'ACTION_AFTER_METHOD'=>'after',


/* プロジェクト設定  */
'ACTION_MAP'=>APP_PATH.'/maping.php',	//マッピングファイルパス
'DEBUG_MODE'=>FALSE,             //アプリケーションがデッバグモードで実行する
//'DEFAULT_FILE'=>'index.php',	//デフォルトのアクセスIndexファイル名

/**  フレームワーク自動設定 START
 * ※手動で設定しなくても、反映しない
 * http://arkauction.ddo.jp/PC2/index.phpの場合
 * HTTP_HOST = arkauction.ddo.jp
 * CONTEXT_PATH = /PC2
 * SERVER_PATH = /PC2/index.php
 * APP_PATH = hard disk上のディレクトリのpath  例:/home/ark/acution
*/
'HTTP_HOST'=>'',
'CONTEXT_PATH'=>'',
'SERVER_PATH'=>'',
'APP_PATH'=>'',
'DEFAULT_FILE'=>'',
/*       自動設定 END          */


/*  URL MODE   */
'URL_MODE'=>true,                       //URLルータ（URLリダイレクト）設定
'ACTION_PREFIX'=>'c',			//アクションの接頭辞（URL_MODE=trueの場合）
'METHOD_PREFIX'=>'m',			//メソッドの接頭辞（URL_MODE=trueの場合）

/**
 * 'URL_MODE'=>trueの場合、urlについているパラメータを取得する
 * 例：http://localhost/index.php/index/execute/adcode/123/frends/acvad?id=1の場合
 * [http://localhost/index.php/index/execute]から[?id=1]の間のものをパラメータとして取得する
 *パラメータはarray(0=>adcode,1=>123,2=>frends,3=>acvad)になります
 */
'URL_PARAMS'=>array(),                  //urlについているパラメータを取得する

/*  Tarce情報   */
'SHOW_TRACE_INFO'=>true,        //Tarce情報が表示するかどうか設定する（DEBUG_MODE=falseの場合、Tarce情報が表示しない）
'TRACE_INFO_TPL'=>FRAMEWORK_DIR.'/tpl/TraceInfo.php',


/*  例外処理   */
'EXCEPTION_HANDLER'=>FRAMEWORK_DIR.'.exception.AppExceptionHandler',	//例外処理のクラスを指定する
//'EXCEPTION_HANDLER'=>FRAMEWORK_DIR.'.exception.DefaultExceptionHandler',	//例外処理のクラスを指定する

    
    
/*     application setting      */
//他の配置ファイル、ここで設定、複数設定の場合、[,]で区切る

/*   メッセージ                       */
'MESSAGE_FILE_PATH'=>'message/message.php',
    
'INCLUDE_FILES'=>array(
'config/action.php',
'config/agent.php',
'config/session.php',
'config/db.php',
'config/log.php',
'config/template.php',
'config/image.php'),

    'NO_AGENT'=>true,


    
);
?>
