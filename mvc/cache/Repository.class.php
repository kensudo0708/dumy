<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *Entityのキャッシュ処理インタフェース
 * @author mw
 */
interface Repository {
    //put your code here

    /**
     *キャッシュエンジンを設置する、RepositoryManagerより自動に設置される
     * @param <CacheEngine> $engine CacheEngineのインスタンス
     */
    public function setCacheEngine($engine);

    /**
     *スタートアップした時のみ、呼ばれる、[初期処理がここで]
     */
    public function startup();
}
?>
