<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import(FRAMEWORK_DIR.".cache.CacheEngine");
/**
 * Description of ApcCache
 *
 * @author mw
 */
class ApcCache implements CacheEngine {
    //put your code here
    
    /**
     *指定されたキャッシュ名のキャッシュに要素を追加する
     * @param <string> $cacheName キャッシュ名
     * @param <String> $key 要素のキー
     * @param <Object> $element 要素
     */
    public function addElement($cacheName, $key, $element) {
        $cache=apc_fetch($cacheName);
        if($cache===FALSE)
            $cache=array();
        $cache[$key]=serialize($element);
        apc_store($cacheName,$cache);
    }
    /**
     *変数をデータ領域にキャッシュする
     * @param <string> $varName 変数名
     * @param <type> $value 値
     */
    public function addVariable($varName,$value){
        apc_store($varName, $value);
    }

    /**
     * 格納されている変数をキャッシュから取得する
     * @param <string> $varName 変数名
     * @return <type> 変数の値
     */
    public function getVariable($varName){
        $value=apc_fetch($varName);
        return $value===FALSE ? "" : $value;
    }
    
    /**
     *APCキャッシュから指定されたキャッシュ名のキャッシュ[array]を取得
     * @param <string> $cacheName キャッシュ名
     * @return <array> 指定されたキャッシュ名のキャッシュ
     */
    public function getCache($cacheName) {
        return apc_fetch($cacheName);
    }

    /**
     *指定されたキャッシュ名のキャッシュからキーにマッピングした要素を取得する
     * @param <string> $cacheName キャッシュ名
     * @param <string> $key 要素のキー
     * @return <Object> 
     */
    public function getElement($cacheName, $key) {
        $cache=$this->getCache($cacheName);
        if($cache==null)
            return null;
        if($this->isKeyInCache($cacheName, $key))
            return unserialize($cache[$key]);
        return null;
    }

    /**
     *指定されたキャッシュ名のキャッシュのすべての要素を取得する
     * @param <string> $cacheName キャッシュ名
     * @return <array> 指定されたキャッシュ名が存在しない場合、NULLを返し
     */
    public function getElements($cacheName) {
        $cache=$this->getCache($cacheName);
        if($cache==null)
            return null;
        foreach ($cache as $key=>$element)
            $result[$key]=unserialize($element);
        return $result;
    }

    /**
     *指定されたキャッシュ名の要素の数を取得
     * @param <string> $cacheName キャッシュ名
     * @return <int>
     */
    public function getCacheSize($cacheName){
        $elements=$this->getElements($cacheName);
        if($elements==null)
            return 0;
        return count($elements);
    }

    /**
     *指定されたキャッシュ名のキャッシュのすべての要素のキーを取得
     * @param <string> $cacheName キャッシュ名
     * @return <array> 指定されたキャッシュ名が存在しない場合、NULLを返し
     */
    public function getKeys($cacheName) {
        $cache=$this->getCache($cacheName);
        if($cache==null)
            return null;
        return array_keys($cache);
    }

    /**
     *指定されたキャッシュ名のキャッシュに指定された要素のキー[$key]が存在するかどうか、調べる
     * @param <string> $cacheName キャッシュ名
     * @param <string> $key 要素のキー
     * @return <boolean> 存在しない場合、FALSEを返し
     */
    public function isKeyInCache($cacheName, $key) {
        $cache=$this->getCache($cacheName);
        if($cache==null)
            return FALSE;
        return array_key_exists($key,$cache);
    }

    /**
     *指定されたキャッシュ名のキャッシュのすべての要素を削除する
     * @param <string> $cacheName キャッシュ名
     */
    public function removeAllElement($cacheName) {
        apc_store($cacheName,array());
    }

    /**
     *指定されたキャッシュ名のキャッシュをキャッシュエンジンから削除する
     * @param <string> $cacheName キャッシュ名
     */
    public function removeCache($cacheName) {
        apc_delete($cacheName);
    }

    /**
     *指定されたキャッシュ名のキャッシュから指定されたキー[$key]の要素を削除する
     * @param <string> $cacheName キャッシュ名
     * @param <string> $key 削除する要素のキー
     */
    public function removeElement($cacheName, $key) {
        $cache=$this->getCache($cacheName);
        if($cache==null)
            return;
        unset($cache[$key]);
        apc_store($cacheName,$cache);
    }
}
?>
