<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.".cache.Repository");
/**
 * Description of RepositoryManager
 *
 * @author mw
 */
final class RepositoryManager {
    //put your code here
    private static $repository_initialized="repository_manager_initialized";

    public static function init() {
        //初期化済みの場合、do nothing
        $context=SeverContext::getServerContext();
        if($context->isExist(self::$repository_initialized))
            return;
        
        //初期化処理
//        $cacheEngine = createInstance(Config::value("CACHE_ENGINE_CLASS"));
        $cacheEngine = Config::value("CACHE_ENGINE_CLASS");

        $repositories=explode(",", Config::value("REPOSITORY_OBJECTS"));
        DB::openConnection();
        foreach ($repositories as $value) {
            $instance=createInstance($value);
            if($instance instanceof Repository) {
                $instance->setCacheEngine($cacheEngine);
                $instance->startup();
            }
            else
            //定義したrepositoryクラスがインタフェース[REPOSITORY]に実装してない場合
                throw new AppException(sprintf(ConstKeys::NOT_REPOSITORY_INSTANCE,$value));
        }
        DB::closeConnection();
        $context->addParam(self::$repository_initialized,true);

    }
}
?>
