<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
class AppExceptionHandler extends BaseAction implements ExceptionHandler {

    public function __construct() {
        parent::__construct();
    }

    public function process($e, $request) {
        import(FRAMEWORK_DIR.'.result.TemplateResult');
        $this->setRequest($request);
        if($e instanceof NotFoundException)
            $this->addTplParam("message",  Msg::get("REQUEST_NOT_FOUND"));
        else
            $this->addTplParam("message", sprintf(Msg::get("REQUEST_ERROR"),strftime('%Y/%m/%d %H:%M:%S')));

        $this->addTplParam("url", Config::value("SERVER_PATH"));
        $this->addTplParam("str", "ホームへ");
        $this->loadCommonTplParam();
        $result=new TemplateResult("message.html", $this->request->getAttributesArray());
        $result->view();

    }
}
?>
