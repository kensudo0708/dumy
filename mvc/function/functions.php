<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


 
function import($class,$ext='.class.php'){
    static $classes = array();
    
    if(isset ($classes[$class]))
        return true;
    else{
        $class_file=str_replace(".","/",$class).$ext;
        
        if(!is_file($class_file))
            throw new Exception($class_file."ファイルが存在しません。",0);
        $classes[$class]=$class_file;
        require $class_file;
    }
}

function createInstance($classFullName){
    import($classFullName);
    $c=explode ('.',$classFullName);
    return new $c[count($c)-1]();
}

function xml_encode($data,$encoding='utf-8',$root="app",$item="item") {
    $xml = '<?xml version="1.0" encoding="'.$encoding.'"?>';
    $xml.= '<'.$root.'>';

    $xml.= dataToXml($data);
    $xml.= '</'.$root.'>';
    return $xml;
}

function dataToXml($data) {
    if(is_object($data)) {
        $data = get_object_vars($data);
    }
    $xml = '';
    foreach($data as $key=>$val) {
        is_numeric($key) && $key="item id=\"$key\"";
        $xml.="<$key>";
        $xml.=(is_array($val)||is_object($val))?dataToXml($val):$val;
        list($key,)=explode(' ',$key);
        $xml.="</$key>";
    }
    return $xml;
}
?>
