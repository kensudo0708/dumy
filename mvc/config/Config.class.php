<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.'.utils.AgentInfo');
/**
 * Description of Config
 *
 * @author mw
 */
final class Config {

    private static $instance=null;

    private $configMap=null;

    private $actionMap=null;

    public static function load() {
        self::$instance=new Config();
        $cfg=self::setAutoConfigValue(
                self::addIncludeConfigFile(include(FRAMEWORK_DIR.'/init.php')));
        self::$instance->configMap=new HashMap($cfg);

        self::$instance->actionMap=new HashMap();

        $actionPath=self::value("ACTION_DIR");
        $agent=AgentInfo::createAgentInfo();
        $agentPath=$agent->getAgentDir();
        $actionCfg=include($cfg["ACTION_MAP"]);
        foreach ($actionCfg as $key => $value) {
            if(!defined("NO_AGENT_CHECK"))
                $value=$actionPath.".".$agentPath.".".$value;
            else
                $value=$actionPath.".".$value;
            $actionMapping=new ActionMapping(strtolower($key),$value);
            self::$instance->actionMap->put(strtolower($key),$actionMapping);
        }
        //アプリケーションメッセージ初期処理
        if(self::value("MESSAGE_FILE_PATH")!="") {
            import(FRAMEWORK_DIR.'.config.Msg');
            Msg::init();
        }
    }

    private static function addIncludeConfigFile($cfg) {
        if(is_array($cfg["INCLUDE_FILES"]))
            $files=$cfg["INCLUDE_FILES"];
        else
            $files=explode(",", $cfg["INCLUDE_FILES"]);
        foreach ($files as $f) {
            $configs=include(APP_PATH."/".$f);
            foreach ($configs as $key => $value)
                $cfg[$key]=$value;
        }
        
        return $cfg;
    }

    private static function setAutoConfigValue($cfg) {
        $context=substr($_SERVER["SCRIPT_NAME"],0,strrpos($_SERVER["SCRIPT_NAME"],"/"));
        $cfg['HTTP_HOST']=$_SERVER["HTTP_HOST"];
        $cfg['CONTEXT_PATH']=$context;
        $cfg['DEFAULT_FILE']=str_replace($context."/", "", $_SERVER["SCRIPT_NAME"]);
        $cfg['SERVER_PATH']=$context."/".$cfg['DEFAULT_FILE'];
        $cfg['APP_PATH']=APP_PATH;
        $cfg['PRODUCT_IMAGE_PATH']=$context."/".PRODUCT_IMAGES_FOLDER_NAME;
        return $cfg;
    }

    /**
     *設定ファイルに指定されたキーの値を取得する
     * @param <String> $key 設定値のキー
     * @return <type>
     */
    public static function value($key) {
        return self::$instance->configMap->get($key);
    }

    /**
     *このリクエストのアクションマッピングを取得する
     * @return <ActionMapping> アクションマッピングのインスタンス
     */
    public static function  getActionMapping() {
        if(self::value("URL_MODE")) {
            //アクションがURLモードと設定された場合
            if(strlen ($_SERVER['QUERY_STRING'])!=0)
                $url=str_replace($_SERVER['QUERY_STRING'], "", $_SERVER['REQUEST_URI']);
            else
                $url=$_SERVER['REQUEST_URI'];
            $url=explode("/",substr(str_replace("?", "", $url),1));
            $index=array_search(self::value("DEFAULT_FILE"), $url);
            if($index===false || $index==count($url)-1) {
                //デフォルトページをアクセス場合、デフォルトのアクションとメソッドと設定する
                $action=self::value("DEFAULT_ACTION");
                $method=self::value("DEFAULT_METHOD");
            }
            else {
                //URLから
                $urls=array_slice($url,$index+1);
/*
                $action=current($urls);
                $method=next($urls);
*/              $params=self::value("URL_PARAMS");
                $adcodeArray = explode(",", self::value("ADCODE"));
                $friendArray = explode(",", self::value("FRIEND_CODE"));
                $w_buff[0]=current($urls);
//				if(($w_buff[0] == self::value("ADCODE")) || ($w_buff[0] == self::value("FRIEND_CODE")))
                                if(in_array($w_buff[0],$adcodeArray) || in_array($w_buff[0],$friendArray))
				{
	                $action=self::value("DEFAULT_ACTION");
	                $method=self::value("DEFAULT_METHOD");
					
	                $w_buff[1]=next($urls);
					$params[0]=$w_buff[0];
					$params[1]=$w_buff[1];
//G.Chin 2010-09-09 add sta
					$urls_cnt = count($urls);
					if($urls_cnt > 2)
					{
						$w_buff[2]=next($urls);
						$params[2]=$w_buff[2];
						$w_buff[3]=next($urls);
						$params[3]=$w_buff[3];
						$w_buff[4]=next($urls);
						$params[4]=$w_buff[4];
					}
//G.Chin 2010-09-09 add end
				}
				else
				{
					$action=$w_buff[0];
                                        $w_buff[1]=next($urls);
//					if(($w_buff[1] == self::value("ADCODE")) || ($w_buff[1] == self::value("FRIEND_CODE")))
                                        if(in_array($w_buff[1],$adcodeArray) || in_array($w_buff[1],$friendArray))
					{
						$method="";
                                                $params[0]=$w_buff[1];
//                                                $params[1]=next($urls);
					}
					else
					{
//		                $w_buff[1]=next($urls);
						$method=$w_buff[1];
					}
	                /**
	                 * urlについているパラメータを取得する
	                 * 例：http://localhost/index.php/index/execute/adcode/123/frends/acvad?id=1の場合
	                 * [http://localhost/index.php/index/execute]から[?id=1]の間のものをパラメータとして取得する
	                 *パラメータはarray(0=>adcode,1=>123,2=>frends,3=>acvad)になります
	                 */
	                
	                while(next($urls)) {
	                    array_push($params, current($urls));
	                }
				}
                if(count($params)>0)
                    self::$instance->configMap->put("URL_PARAMS",$params);

                $method=empty($method)?self::value("DEFAULT_METHOD") : $method;
            }
        }
        else {
            //アクションがURLのパラメータモードと設定された場合
            $action=$_REQUEST[self::value("ACTION_PREFIX")];
            $method=$_REQUEST[self::value("METHOD_PREFIX")];
            $action=empty ($action) ? self::value("DEFAULT_ACTION") :$action;
            $method=empty ($method) ? self::value("DEFAULT_METHOD") :$method;
        }
        //アクション名の元でマッピングを取得
        $actionMapping=self::$instance->actionMap->get(strtolower($action));
        if(empty ($actionMapping))
        //マッピングの設定ファイルに該当マッピングがない場合、例外をスロー
            throw new NotFoundException(sprintf(ConstKeys::URL_ERROR_ACTION, $_SERVER['REQUEST_URI']));
        $actionObj=$actionMapping->getAction();
        if(!method_exists($actionObj, $method))
            throw new NotFoundException(sprintf(ConstKeys::URL_ERROR_METHOD, $_SERVER['REQUEST_URI']));
        $actionMapping->setMethod($method);
        return $actionMapping;
    }


}
?>
