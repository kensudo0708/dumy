<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of Msg
 *
 * @author mw
 */
class Msg {
    //put your code here
    private static $message;
    public static function init() {
        self::$message=include(APP_PATH."/".Config::value("MESSAGE_FILE_PATH"));
    }

    /**
     *定義済みのアプリケーションメッセージを取得
     * @param <String> $key メッセージkey
     * @return <String> メッセージ
     */
    public static function get($key) {
        return self::$message[$key];
    }
}
?>
