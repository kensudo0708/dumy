<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConstKeysclass
 *
 * @author mw
 */
class ConstKeys {
    //put your code here
    const NO_DEFAULT_FILE="DEFAULT_FILEが設定されていません。";
    const URL_ERROR_ACTION="[URL:%s]URLのアクションが正しく設定されていません。アクションマッピング設定ファイルを確認してください";
    const URL_ERROR_METHOD="[URL:%s]URLのメソッドが正しく設定されていません。";
    const TEMPLATE_NOT_IMPLEMENT="テンプレートエンジンクラスがインタフェース[TemplateEngine]を実装していません。";
    const RETURN_NOT_TEMPLATE="アクションの戻り値は正しくありません。";
    const ACTION_RESULT_ERROR="アクションの実行結果がインタフェース[ActionResult]を実装されていません。";
    const DATA_ENGINE_NO_IMPLEMENT="DBアクセスエンジンがインタフェース[DataAcessEngine]を実装されていません。";
    const APPENDER_NOT_IMPLEMENT="ログAppenderがインタフェース[Appender]を実装されていません。";
    const CONNECTION_OPEN_ERROR="DB接続がオープンできません。";
    const CONNECTION_CLOSE_ERROR="DB接続がクローズできません。";
    const DATA_GET_FAILED="データ取得が失敗しました";
    const TRAN_BEGIN_FAILED="トランザクション開始できません。";
    const TRAN_COMMIT_FAILED="トランザクションがコミットできません";
    const TRAN_ROLLBACK_FAILED="トランザクションがロールバックできません";
    const NOT_EXCEPTION_HANDLER_INSTANCE="例外処理クラスがインタフェース[ExceptionHandler]を実装していません。";
    const NOT_UPLOAD_PATH="ファイルアップロード先のパスが設定されていません。";

    const NOT_EXIST_PATH="指定されたパス[%s]が存在しません。";
    const NOT_WRITEABLE="指定されたパス[%s]にファイルの書き込みができません。";
    const NOT_REPOSITORY_INSTANCE="[%s]はRepositoryのインスタンスではありません。";
    const NOT_ACTION_INSTANCE="無効なActionインスタンス。";


    
}
?>
