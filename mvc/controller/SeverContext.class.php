<?php
/**
 * APCキャッシュを利用する、SeverContextクラスを構築する
 * SeverContextに保存されたパラメータはサーバのメモリ上にキャッシュされ、
 * PHPスクリプトの処理完了に伴い、解放することがない
 *
 * @author mw
 */
class SeverContext {
    //put your code here
    const CACHE_NAME="SERVER_CONTEXT";
    /**
     * SeverContextのインスタンスがメモリ上にキャッシュする時間
     */
    const CACHE_TIME=0;

    /**
     *SeverContextに保存されたパラメータが更新されたかどうか、示す
     * @var <boolean>
     */
    private $isUpdate=false;

    private $p=array();

    private static $instance;

    /**
     * SeverContextのインスタンスが作成させないよう
     */
    private function __construct() { }

    /**
     *ServerContextに指定されたパラメータ(name,value)を追加する
     * @param <String> $name
     * @param <Object> $value
     */
    public function addParam($name,$value){
        $this->p[$name] =$value;
        $this->isUpdate=true;
    }

    public function isExist($name){
        return array_key_exists($name, $this->p);
    }

    /**
     *指定された名前でServerContextに保存されたパラメータを取得
     * @param <String> $name　パラメータの名前
     * @return <Object> パラメータの値
     */
    public function getParam($name){
        if (array_key_exists($name, $this->p)) {
            return $this->p[$name];
        }
    }

    /**
     *ServerContextのインスタンスを取得、（シングルトン・パターン）
     * @return <ServerContext> ServerContextのインスタンス
     */
    public static function getServerContext(){
        $instance=apc_fetch(self::CACHE_NAME);
        if(!$instance){
            $instance=new SeverContext();
            apc_store(self::CACHE_NAME,$instance,self::CACHE_TIME);
        }
        self::$instance=$instance;
        return $instance;
    }

    /**
     *ServerContextを更新する
     * @param <ServerContext> $instance
     */
    public static function updateServerContext(){
        if(self::$instance->isUpdate){
            apc_store(self::CACHE_NAME,self::$instance,self::CACHE_TIME);
            self::$instance->isUpdate=false;
        }
    }

}
?>
