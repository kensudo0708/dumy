<?php

/**
 * リクエスト
 */
class Request
{
    /**
     * リクエストに格納されたattributeのマッピング
     * @var	HashMapのインスタンス
     */
    private $requestAttributes;
    /**
     * リクエストに格納されたパラメータのマッピング
     * @var	HashMapのインスタンス
     */
    private $requestParameters;

    /**
     * コンストラクタ.
     * @access public
     */
    public function __construct() {
        $this->requestAttributes = new HashMap();
        $this->requestParameters = new HashMap();
        $this->init();
    }

    /**
     * リクエストの初期化
     * @access protected
     */
    private function init(){
        while (list ($key, $val) = each ($_REQUEST)) {
                $this->setParameter($key, $val);
        }
    }

    /**
     * リクエストで利用可能な属性名を含む配列arrayを返す
     * @access public
     * @return array
     */
    public function getAttributeNames(){
        return $this->requestAttributes->keySet();
    }

    /**
     * リクエストに属性をセットする
     * @param $name 属性名
     * @param $obj セットするObject
     * @access public
     */
    public function setAttribute($name, $obj){
        $this->requestAttributes->put($name, $obj);
    }

    /**
     * 指定された名前の属性値を返す
     * @access public
     * @param $name string
     * @return 属性の値
     */
    public function getAttribute($name){
        return $this->requestAttributes->get($name);
    }

    /**
     * リクエストに含まれている属性の値を配列で返す
     * @return array
     */
    public function getAttributes(){
        return $this->requestAttributes->values();
    }

    /**
     *
     * @return array
     */
     public function getAttributesArray(){
        return $this->requestAttributes->getArray();
    }

    /**
     * リクエストから指定された名前の属性を削除する
     * @access public
     * @param $name 属性の名前
      * @return 削除された属性
     */
    public function removeAttribute($name){
        $this->requestAttributes->remove($name);
    }

    /**
     * 指定されたリクエストパラメータの値を返す
     * @access public
     * @param $name パラメータの名前を指定する
     * @return パラメータの値
     */
    public function getParameter($name){
        return $this->requestParameters->get($name);
    }

    /**
     * このリクエストに含まれているパラメータ名を配列arrayの構成で返す
     * @access public
     * @return array
     */
    public function getParameterNames(){
        return $this->requestParameters->keySet();
    }

    /**
     * リクエストに含まれているパラメータの値を配列で返す
     * @return array
     */
    function getParameters(){
        return $this->requestParameters->values();
    }

    /**
     * リクエストにパラメータをセットする
     * @param $name パラメータ名
     * @param $obj セットするObject
     * @access public
     */
    function setParameter($name, $obj){
        $this->requestParameters->put($name, $obj);
    }

    /**
     * セッションのIDを返す
     * @access public
     * @return セッションのID
     */
    public function getRequestedSessionId(){
        return $_COOKIE[session_name()];
    }

    /**
     * リクエストされたURIのうち、 リクエストのコンテキストを指す部分を返す
     * @access public
     * @return リクエストのコンテキスト
     */
    public function getContextPath(){
        return substr($_SERVER["SCRIPT_NAME"],0,strrpos($_SERVER["SCRIPT_NAME"],"/"));
    }

    /**
     * リクエストを生成した時にクライアントが URLに関連づけて送った拡張パス情報を返す
     * @access public
     * @return 拡張パス情報
     */
    public function getPathInfo(){
        $contextPath = $this->getContextPath();
        $pos=strpos($_SERVER["REQUEST_URI"],$contextPath);
        return substr($_SERVER["REQUEST_URI"],$pos+strlen($contextPath));
    }
}

?>
