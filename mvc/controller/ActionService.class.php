<?php

/**
 * アプリケーションのコントローラ
 *
 * @author mw
 */
final class ActionService {
    /**
     * アプリケーション初期化
     */
    public static function init() {
        Config::load();
        Logger::init();
    }

    /**
     * アプリケーションの中心コントローラ
     */
    public static function service() {
        try {
            //リクエストのインスタンスを作成
            $request=new Request();
            //クラスのマッピング
            $actionMapping=Config::getActionMapping();
            //データベース初期化
            self::dataAccessInit();
            //キャッシュ初期化
            self::cacheInit();
            //アクション　Go　Go!
            $result=self::execute($actionMapping,$request);
            //アクション実行した結果よりテンプレートを取得、画面表示
            if(empty ($result))
                throw new AppException(ConstKeys::RETURN_NOT_TEMPLATE);
            self::resultView($result);
            //最後、ログを記録しよう
            if(Config::value("LOG_RECORD"))
                Logger::record();

        }
        catch (Exception $e) {
            //例外処理
            self::exceptionProcess($e, $request);
        }
    }

    /**
     *アクション実行する
     * @param <ActionMapping> $actionMapping
     * @param <Request> $request
     * @return <ActionResult>
     */
    private static function execute($actionMapping,$request) {
        $action=$actionMapping->getAction();
        $action->setRequest($request);
        $method=$actionMapping->getMethod();
        try {
            //データベース接続オープン
            DB::openConnection();
            //マッピングしたメソッドを実行する前、実行するメソッドが存在したら、実行する
            $before=Config::value("ACTION_BEFORE_METHOD");
            if(is_callable(array($action, $before)))
                $action->$before();
            $result=$action->$method();
            //マッピングしたメソッドを実行した後、実行するメソッドが存在したら、実行する
            $after=Config::value("ACTION_AFTER_METHOD");
            if(is_callable(array($action, $after)))
                $action->$after();
            DB::closeConnection();
        }
        catch (Exception $e) {
            DB::closeConnection();
            throw $e;
        }
        return $result;
    }

    /**
     *実行結果が表示する
     * @param <ActionResult> $result
     */
    private static function resultView($result) {
        //$resultがインタフェース[ActionResult]を実装したかどうかチェック
        if($result instanceof ActionResult) {
            $result->view();
            if(Config::value("DEBUG_MODE") && Config::value("SHOW_TRACE_INFO"))
                print(include(Config::value("TRACE_INFO_TPL")));
        }
        else
            throw new AppException(ConstKeys::ACTION_RESULT_ERROR);
    }

    /**
     *例外処理
     * @param <Exception> $e
     * @param <Request> $request
     */
    private static function exceptionProcess($e,$request) {
        $className=Config::value("EXCEPTION_HANDLER");
        $obj=createInstance($className);
        if($obj instanceof ExceptionHandler){
            $obj->process($e, $request);
            Logger::error("[Message]:".$e->getMessage()."\n\t[Trace]:".$e->getTraceAsString());
        }
        else {
            print ConstKeys::NOT_EXCEPTION_HANDLER_INSTANCE;
            Logger::error(ConstKeys::NOT_EXCEPTION_HANDLER_INSTANCE);
        }
        
        if(Config::value("LOG_RECORD"))
            Logger::record();
    }

    /**
     * データベース初期化
     */
    private static function dataAccessInit() {
        DB::init(Config::value("host"),
                Config::value("port"),
                Config::value("user"),
                Config::value("pwd"),
                Config::value("dbname"),
                Config::value("charset"));
        $engine=createInstance(FRAMEWORK_DIR.'.database.engine.'.ucfirst(strtolower(Config::value("type"))).'Engine');
        DB::setDataAccessEngine($engine);
    }

    private static function cacheInit() {
        import(FRAMEWORK_DIR.".cache.RepositoryManager");
        if(Config::value("CACHE_ENABLE")) {
            //キャッシュManager初期処理
            import(FRAMEWORK_DIR.".cache.RepositoryManager");
            RepositoryManager::init();
        }
    }
}
?>
