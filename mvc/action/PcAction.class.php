<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of PcAction
 *
 * @author mw
 */
class PcAction extends BaseAction {
    //put your code here

    public function __construct() {
        parent::__construct();
        //HTML内の相対リンクにセッションIDを自動的に追加しない
        ini_set('session.use_trans_sid',0);
        //セッション
        if(Config::value("SESSION_AUTO_START") || session_id()=="")
            session_start();
    }


    /**
     *AjaxのActionResultを作成
     * @param <type> $obj
     * @param <String> $type Ajaxの通信の結果タイプ　（xml,json)
     * @return AjaxResult
     */
    public final function createAjaxResult($obj,$type='') {
        import(FRAMEWORK_DIR.'.result.AjaxResult');
        return new AjaxResult($obj,$type);
    }

    /**
     *Ajaxのリクエストかどうかチェック
     * @return <boolean>
     */
    public final function isAjax() {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
            if('xmlhttprequest' == strtolower($_SERVER['HTTP_X_REQUESTED_WITH']))
                return true;
        }
        return false;
    }

    /**
     *ベースクラスのメソッドをオーバーライド、javascriptのパスを追加する
     * @param <String> $tpl テンプレートの相対パス
     * @return TemplateResult ActionResultのインスタンス
     */
    public function createTemplateResult($tpl, $theme = '') {
        $this->addTplParam("JAVASCRIPT_DIR",Config::value("CONTEXT_PATH")."/".Config::value("JAVASCRIPT_DIR"));
        $this->addTplParam("TEMPLATE_PARTS_DIR",Config::value("CONTEXT_PATH")."/templates/pc/parts");
        //brower情報をテンプレートに追加
        $brower=AgentInfo::getBrowerInfo();
        $this->addTplParam("brower",$brower);
        $this->addTplParam("ajax_times",AJAX_TIMES);
        $this->addTplParam("ajax_host",AJAX_HOST);
        if(defined("AJAX_LIMIT_COUNT"))
            $this->addTplParam("ajax_limit",AJAX_LIMIT_COUNT);
        if(defined("AJAX_STOP_MESSAGE"))
            $this->addTplParam("ajax_message",AJAX_STOP_MESSAGE);
//        //検証コード
//        import(FRAMEWORK_DIR.'.utils.StringBuilder');
//        $validate_code=StringBuilder::uuid();
//        $this->setSession("validate", $validate_code);
//        $this->addTplParam("validate", $validate_code);
        return parent::createTemplateResult($tpl, $theme);
    }

    /**
     *チェックユーザセッション情報
     * @param <boolean> $addMemberToTemplate  TRUEの場合:ログインメンバー情報をテンプレートに追加する
     */
    public final function checkSessionInfo($addMemberToTemplate=True) {
        if($this->isLogin()) {
            $this->addTplParam("isLogin",true);
            if($addMemberToTemplate)
                $this->addMemberToTemplate();
        }
        else {
            //クッキ情報を確認する
            if (isset($_COOKIE[MEMBER_COOKIE_NAME])) {
                $cookie=$_COOKIE[MEMBER_COOKIE_NAME];
                if(count($cookie)==3) {
                    //有効なcookie
                    $member_id=$cookie[0];
                    $login_id=$cookie[1];
                    $pass=$cookie[2];
                    import("classes.model.MemberModel");
                    $model=new MemberModel();
                    $member = $model->getMemberMasterById($member_id);
                    if(!empty($member) && $member->f_login_id==$login_id && $member->f_login_pass==$pass
                            && $member->f_activity == 1 && $member->f_delete_flag != 1) {
                        $this->addSessionInfo($member);
                        $this->checkSessionInfo($addMemberToTemplate);
                    }
                }
            }
        }
    }

    /**
     *メンバー情報をクッキに保存する
     * @param <object> $member
     */
    public function addCookieMemberInfo($member) {
        if(setcookie(MEMBER_COOKIE_NAME."[0]", $member->fk_member_id, time() + MEMBER_COOKIE_EXPIRE,'/')) {
            setcookie(MEMBER_COOKIE_NAME."[1]", $member->f_login_id, time() + MEMBER_COOKIE_EXPIRE,'/');
            setcookie(MEMBER_COOKIE_NAME."[2]", $member->f_login_pass, time() + MEMBER_COOKIE_EXPIRE,'/');
        }
    }

    /**
     * クッキをクリアする
     */
    public final function clearCookieInfo() {
        if (isset($_COOKIE[MEMBER_COOKIE_NAME])) {
            setcookie(MEMBER_COOKIE_NAME."[0]", "", time() - MEMBER_COOKIE_EXPIRE,'/');
            setcookie(MEMBER_COOKIE_NAME."[1]", "", time() - MEMBER_COOKIE_EXPIRE,'/');
            setcookie(MEMBER_COOKIE_NAME."[2]", "", time() - MEMBER_COOKIE_EXPIRE,'/');
        }
    }

    /**
     *ログインメンバー情報をテンプレートに追加する
     */
    public final function addMemberToTemplate() {
        if(isset($_SESSION['member_id'])) {
            $member=$this->request->getAttribute("member");
            if(empty($member)) {
                //メンバー情報がテンプレートに存在しない場合、メンバーを取得する
                import('classes.model.MemberModel');
                $model=new MemberModel();
                $member=$model->getMemberMasterById($this->getMemberIdFromSession());
            }
            //コイン情報
            $member->coin_count=Utils::getInt($member->f_coin,0) + Utils::getInt($member->f_free_coin,0);
            //ユーザが参加しているオークション数の情報
            import("classes.model.MyProductModel");
            $model=new MyProductModel();
            $member->bid_count=Utils::getInt($model->getBidProductCount($member),0);
            $this->addTplParam("member", $member);
        }
    }

    /**
     *不正なアクセスかどうか、チェックする
     * @return <boolean>
     */
    public final function isFailedAccess(){
//        $validate=$this->getRequestParam("validate");
//        if(!empty($validate) && isset($_SESSION["validate"])){
//            return $validate != $this->session("validate");
//        }
//        return true;
        return $this->isPost() && $this->isAjax() ;
    }


}
?>
