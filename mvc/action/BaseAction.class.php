<?php

/**
 * ベースクラス
 * @author mw
 */
class BaseAction {
    /**
     *リクエスト
     * @var <type>
     */
    protected $request;

    protected $serverContext;

    public function __construct() {
        $this->serverContext=SeverContext::getServerContext();
    }

    public function __destruct() {
        SeverContext::updateServerContext();
    }

    /**
     *ServerContextに指定されたパラメータ(name,value)を追加する
     * @param <String> $name
     * @param <Object> $value
     */
    public final function addContextParam($name,$value) {
        $this->serverContext->addParam($name,$value);
    }

    /**
     *指定された名前でServerContextに保存されたパラメータを取得
     * @param <String> $name　パラメータの名前
     * @return <Object> パラメータの値
     */
    public final function getContextParam($name) {
        return $this->serverContext->getParam($name);
    }

    /**
     *指定された名前のリクエスト引数を取得する
     * @param <string> $name
     * @return <type>
     */
    public final function getRequestParam($name) {
        return StringBuilder::htmlDecode($this->request->getParameter($name));
    }

    /**
     *指定された名前のリクエストのAttributeを取得する
     * @param <string> $name
     * @return <type>
     */
    public final function getRequestAttribute($name) {
        return $this->request->getAttribute($name);
    }

    /**
     *リクエストのインスタンスをセットする
     * @param <Request> $request リクエスト
     */
    public final function setRequest($request) {
        $this->request=$request;
    }



    /**
     *リクエストのコンテキストパスを取得
     * @return <String> path
     */
    public final function getContextPath() {
        return $this->request->getContextPath();
    }

    /**
     *テンプレートの引数を追加する
     * @param <String> $key テンプレートの引数
     * @param <Object> $value 引数の値
     */
    public final function addTplParam($key,$value) {
        $this->request->setAttribute($key,$value);
    }

    /**
     *テンプレートのActionResultを作成
     * @param <String> $tpl テンプレートの相対パス
     * @return TemplateResult ActionResultのインスタンス
     */
    public function createTemplateResult($tpl,$theme='') {
        import(FRAMEWORK_DIR.'.result.TemplateResult');
        if($_SERVER["SCRIPT_NAME"] !="/cli_ep.php"
                && (MAINTE_FLAG ==SYS_CLOSE || (defined("MOBILE_CLOSE") && MOBILE_CLOSE==1 && !AgentInfo::isPc()))) {
            //メンテナンス中の場合(決済会社からのアクセスを例外とする）
            if($_SERVER['REMOTE_ADDR'] == "219.101.37.233" || $_SERVER['REMOTE_ADDR'] == USER_SERVER)
            {
                
            }
            else
            {
                return new TemplateResult("mainte.html", array());
            }
        }

        $listing_tag = "";	//G.Chin 2010-07-29 add
        $listing_tag2 = "";	//G.Chin 2010-11-05 add
        $url_str=(!empty($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'');
        $befor_url=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'');
        $acc_flag=0;
        $tmp_adcode=0;
        $jump_url="";

//        if($url_str =='/' || $url_str =='/index.php')
//        {
//            $url_params=Config::value("URL_PARAMS");
//            echo $url_params;
//            if( ereg(SITE_URL,$befor_url) || ereg("ipservice.jp",$befor_url))
//            {
//
//            }
//            else
//            {
//                return new TemplateResult("mainte.html", array());
//            }
//        }
        //広告主あり広告コードつきの場合
        if(ereg('/index.php/a/',$url_str))
        {
            $url_params=Config::value("URL_PARAMS");
            $adcodeArray = explode(",", Config::value("ADCODE"));
            in_array($url_params[0],$adcodeArray);
            import('classes.model.MemberModel');
            $model = new MemberModel();
            $ad = $model->getAdcodeMasterId($url_params[1]);
            if(!empty($ad))
            {
                $tmp_adcode=$url_params[1];
                //広告コードが存在する場合
                //商品詳細・カテゴリへのリンクの場合
                if(ereg('/category',$url_str) || ereg('/product',$url_str))
                {
                    if(ereg(SITE_URL,$befor_url))
                    {
                        //echo "カウントアップなし1-1<BR>";
                    }
                    else if(isset($_SESSION['URL_PARAMS']))
                    {
                        unset( $_SESSION["URL_PARAMS"] );
                        $acc_flag=1;
                    }
                    else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                    {
                        //echo "カウントアップあり1<BR>";
                        $acc_flag=1;
                    }
                    $jump_url = SITE_URL."index.php/".$url_params[2]."?".$url_params[3]."=".$url_params[4];
                }
                //トップページへのリンクの場合
                else if(ereg("/index.php/a/$url_params[1]",$url_str))
                {
                    if(ereg(SITE_URL,$befor_url))
                    {
                        //echo "カウントアップなし2-1<BR>";
                    }
                    else if(isset($_SESSION['URL_PARAMS']))
                    {
                        //echo "カウントアップなし2-2<BR>";
                    }
                    else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                    {
                        //echo "カウントアップあり2<BR>";
                        $acc_flag=1;
                    }
                }
            }
            else
            {
                //該当する広告コードがない場合
                if(ereg(SITE_URL,$befor_url))
                {
                    //echo "カウントアップなし3-1<BR>";
                }
                else if(isset($_SESSION['URL_PARAMS']))
                {
                    //echo "カウントアップなし3-2<BR>";
                }
                else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                {
                    //echo "カウントアップあり3<BR>";
                    $tmp_adcode=0;
                    $acc_flag=3;
                }
            }
        }
//G.Chin AWKC-198 2010-10-21 add sta
        //会員登録画面への広告主あり広告コードつきの場合
        else if(ereg('/index.php/ar/',$url_str))
        {
            $url_params=Config::value("URL_PARAMS");
            $adcodeArray = explode(",", Config::value("ADCODE"));
            in_array($url_params[0],$adcodeArray);
            import('classes.model.MemberModel');
            $model = new MemberModel();
            $ad = $model->getAdcodeMasterId($url_params[1]);
            if(!empty($ad))
            {
                $tmp_adcode=$url_params[1];
                //広告コードが存在する場合
                //会員登録ページへのリンクの場合
                if(ereg("/index.php/ar/$url_params[1]",$url_str))
                {
                    if(ereg(SITE_URL,$befor_url))
                    {
                        //echo "カウントアップなし1-1<BR>";
                    }
                    else if(isset($_SESSION['URL_PARAMS']))
                    {
                        //echo "カウントアップなし1-2<BR>";
                    }
                    else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                    {
                        //echo "カウントアップあり1<BR>";
                        $acc_flag=1;
                    }
                }
            }
            else
            {
                //該当する広告コードがない場合
                if(ereg(SITE_URL,$befor_url))
                {
                    //echo "カウントアップなし1-1<BR>";
                }
                else if(isset($_SESSION['URL_PARAMS']))
                {
                    //echo "カウントアップなし1-2<BR>";
                }
                else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                {
                    //echo "カウントアップあり1<BR>";
                    $tmp_adcode=0;
                    $acc_flag=3;
                }
            }
            $isPc = AgentInfo::isPc();
			if($isPc == true)
			{
				$jump_url = SITE_URL."index.php/member/register";
			}
			else
			{
				$jump_url = SITE_URL."index.php/member/entry?PHPSESSID=".$_REQUEST["PHPSESSID"]."&guid=ON&PHPSESSID=".$_REQUEST["PHPSESSID"];
			}
        }
//G.Chin AWKC-198 2010-10-21 add end
        //友達紹介の場合
        else if(ereg('/index.php/member/register/f/',$url_str))
        {
            $url_params=Config::value("URL_PARAMS");	//G.Chin AWKT-820 2010-12-06 add
            $friendArray = explode(",", Config::value("FRIEND_CODE"));
            in_array($url_params[1],$friendArray);
            $tmp_adcode=$url_params[1];
            import('classes.model.MemberModel');
            $model = new MemberModel();
            $ad = $model->getAdcodeMasterId($url_params[1]);
            if(!empty($ad))
            {
                //紹介者が存在する場合
                if(ereg(SITE_URL,$befor_url))
                {
                    //echo "カウントアップなし2-1<BR>";
                }
                else if(isset($_SESSION['URL_PARAMS']))
                {
                    Logger::DEBUG("自爆:".$_SERVER['REMOTE_ADDR'].";access:$url_str;adcode:$tmp_adcode");
                    //echo "カウントアップなし2-2<BR>";
                }
                else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                {
                    //echo "カウントアップあり2<BR>";
                    $acc_flag=2;
                }
            }
            else
            {
                //紹介者が存在しない場合
                if(ereg(SITE_URL,$befor_url))
                {
                    //echo "カウントアップなし2-1<BR>";
                }
                else if(isset($_SESSION['URL_PARAMS']))
                {
                    $tmp_adcode=-1;
                    //echo "カウントアップなし2-2<BR>";
                }
                else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
                {
                    //echo "カウントアップあり2<BR>";
                    $tmp_adcode=0;
                    $acc_flag=3;
                }
            }
        }
//G.Chin AWKT-947 2010-12-22 add sta
        //友達紹介(MB版)の場合
        else if(ereg('/index.php/member/entry',$url_str))
        {
            $friend_code=$this->session("friend_code");
			if($friend_code != "")	//友達コードが存在する場合
			{
	            $tmp_adcode=$friend_code;
	            import('classes.model.MemberModel');
	            $model = new MemberModel();
	            $ad = $model->getAdcodeMasterId($tmp_adcode);
	            if(!empty($ad))
	            {
	                //紹介者が存在する場合
	                if(ereg(SITE_URL,$befor_url))
	                {
	                    //echo "カウントアップなし3-1<BR>";
	                }
/*
	                else if(isset($_SESSION['URL_PARAMS']))
	                {
	                    Logger::DEBUG("自爆:".$_SERVER['REMOTE_ADDR'].";access:$url_str;adcode:$tmp_adcode");
	                    //echo "カウントアップなし3-2<BR>";
	                }
*/
	                else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
	                {
	                    //echo "カウントアップあり3<BR>";
	                    $acc_flag=2;
	                }
	            }
	            else
	            {
	                //紹介者が存在しない場合
	                if(ereg(SITE_URL,$befor_url))
	                {
	                    //echo "カウントアップなし4-1<BR>";
	                }
/*
	                else if(isset($_SESSION['URL_PARAMS']))
	                {
	                    $tmp_adcode=-1;
	                    //echo "カウントアップなし4-2<BR>";
	                }
*/
	                else if($befor_url =='' || ! ereg(Config::value("SITE_URL"),$befor_url))
	                {
	                    //echo "カウントアップあり4<BR>";
	                    $tmp_adcode=0;
	                    $acc_flag=3;
	                }
	            }
			}
        }
//G.Chin AWKT-947 2010-12-22 add end
        //一般アクセスの場合
        elseif($url_str =='/' || $url_str =='/index.php' || ereg('/index.php/product',$url_str) ||ereg('/index.php/category',$url_str) )
        {
            if(isset($_SESSION['URL_PARAMS']))
            {
                //echo "カウントアップなし3-1<BR>";
            }
            else if(ereg(SITE_URL,$befor_url))
            {
                //echo "カウントアップなし3-2<BR>";
            }
            else
            {
                //echo "カウントアップあり3<BR>";
                $tmp_adcode=0;
                $acc_flag=3;
            }
        }

        if(isset($_SESSION['URL_PARAMS']))
        {
                $this->setSession("URL_PARAMS",$_SESSION['URL_PARAMS']);
        }
        else
        {
                $this->setSession("URL_PARAMS",Config::value("URL_PARAMS"));
        }


//        if($acc_flag==1 || $acc_flag==2 || $acc_flag==3 || $tmp_adcode == -1)
//        {
//            Logger::DEBUG($_SERVER['REMOTE_ADDR'].";access:$url_str;adcode:$tmp_adcode");
//        }

        if($acc_flag==1)
        {
            $aflkey=$this->getRequestParam("aflkey");
            $this->setSession("aflkey",  $aflkey);
            import('classes.model.Total');
            Logger::DEBUG($_SERVER['REMOTE_ADDR'].";access:$url_str;adcode:$tmp_adcode");
            Total::access($ad->fk_admaster_id);
            $all_include_path = COMMON_LIB."all_include_lib.php";
            include $all_include_path;
            $access_pc = AgentInfo::isPc();
            if($access_pc == false)
            {
                    $term_type = 1;	//MB
            }
            else
            {
                    $term_type = 0;	//PC
            }
            //リスティングTOPタグ作成関数
            MakeListingTopTag($ad->fk_admaster_id,$term_type,$listing_tag);
			//リスティングTOPタグ作成関数２
			MakeListingTopTag2($ad->fk_admaster_id,$term_type,$listing_tag2);	//G.Chin 2010-11-05 add
        }
        else if($acc_flag==2)
        {
            import('classes.model.Total');
            Logger::DEBUG($_SERVER['REMOTE_ADDR'].";access:$url_str;adcode:$tmp_adcode");
            Total::access($ad->fk_admaster_id);
        }
        else if($acc_flag==3)
        {
            import('classes.model.MemberModel');
             $model = new MemberModel();
             $ad = $model->getAdcodeMasterId("0");
             import('classes.model.Total');
             Logger::DEBUG($_SERVER['REMOTE_ADDR'].";access:$url_str;adcode:$tmp_adcode");
             Total::access($ad->fk_admaster_id);
             $this->setSession("URL_PARAMS",  0);
        }

        //echo $jump_url;
        if($jump_url !="")
        {
            header("Location: $jump_url");
        }
        /*
//G.Chin 2010-06-18 add sta
        //URLについていたパラメータがあったら、
        $url_params=Config::value("URL_PARAMS");
        if(count($url_params)>0) {
            $adcodeArray = explode(",", Config::value("ADCODE"));
            $friendArray = explode(",", Config::value("FRIEND_CODE"));
            //URL_PARAMSを記録する(広告コートなど)
//            if(!isset($_SESSION['URL_PARAMS']) && count($url_params) >= 2 && $url_params[0]==Config::value("ADCODE")) {
//            if(!isset($_SESSION['URL_PARAMS']) && count($url_params) >= 2 && (($url_params[0]==Config::value("ADCODE") || $url_params[0]==Config::value("FRIEND_CODE")))) {
            if(!isset($_SESSION['URL_PARAMS']) && count($url_params) >= 2 && (in_array($url_params[0],$adcodeArray) || in_array($url_params[0],$friendArray))) {
                if(in_array($url_params[0],$adcodeArray)){
                    //広告主の場合
                    import('classes.model.MemberModel');
                    $model = new MemberModel();
                    $ad = $model->getAdcodeMasterId($url_params[1]);
                    if(!empty ($ad)) {
    //G.Chin 2010-07-30 add sta
                            $aflkey=$this->getRequestParam("aflkey");
                            $this->setSession("aflkey",  $aflkey);
    //G.Chin 2010-07-30 add end
                        import('classes.model.Total');
                        Total::access($ad->fk_admaster_id);
    //G.Chin 2010-07-29 add sta
                                            //☆★	ライブラリ読込み	★☆
                                            $all_include_path = COMMON_LIB."all_include_lib.php";
                                            include $all_include_path;
    //G.Chin 2010-08-03 chg sta
                                            //リスティングTOPタグ作成関数
    //					MakeListingTopTag($ad->fk_admaster_id,$listing_tag);
    //G.Chin 2010-08-03 chg end
                                            //▼PCアクセス判定
                                            $access_pc = AgentInfo::isPc();
                                            if($access_pc == false)
                                            {
                                                    $term_type = 1;	//MB
                                            }
                                            else
                                            {
                                                    $term_type = 0;	//PC
                                            }
                                            //リスティングTOPタグ作成関数
                                            MakeListingTopTag($ad->fk_admaster_id,$term_type,$listing_tag);
    //G.Chin 2010-07-29 add end
                          $this->setSession("URL_PARAMS",  Config::value("URL_PARAMS"));
                          Logger::DEBUG("session:".$url_params[0].":".$url_params[1]);
//G.Chin 2010-09-09 add sta
							$param_cnt = count($url_params);
							if($param_cnt == 5)
							{
								$jump_url = SITE_URL."index.php/".$url_params[2]."?".$url_params[3]."=".$url_params[4];
								header("Location: $jump_url");
							}
//G.Chin 2010-09-09 add end
                    }//2010-07-30 shoji add
                    else if(!isset($_SESSION['URL_PARAMS'])){
                         import('classes.model.MemberModel');
                         $model = new MemberModel();
                         $ad = $model->getAdcodeMasterId("0");
                         import('classes.model.Total');
                         Total::access($ad->fk_admaster_id);
                         $this->setSession("URL_PARAMS",  0);
                         Logger::DEBUG("session:adcode:0");
                    }
                }
                else{
                        $this->setSession("URL_PARAMS",  Config::value("URL_PARAMS"));
                }
            }
            //Logger::DEBUG("data:".$url_params[0].":".$url_params[1]);
        }        
        else
        {
            if(!isset($_SESSION['URL_PARAMS']))
            {
                import('classes.model.MemberModel');
                $model = new MemberModel();
                $ad = $model->getAdcodeMasterId("0");
                import('classes.model.Total');
                Total::access($ad->fk_admaster_id);
                $this->setSession("URL_PARAMS",  0);
                Logger::DEBUG("session:adcode:0");
            }
        }
        */
        //2010-07-30 shoji add end
//G.Chin 2010-06-18 add end
        //G.Chin 2010-07-29 add sta
        //MicroAdタグ
        $this->addTplParam("LISTING_TAG", $listing_tag);
        $this->addTplParam("LISTING_TAG2", $listing_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-07-29 add end

        $this->loadCommonTplParam($theme);
        return new TemplateResult($tpl, $this->request->getAttributesArray());
    }

    /**
     *テンプレート共用置換文字をロードする
     */
    public final function loadCommonTplParam($theme=''){
        /**
         * http://arkauction.ddo.jp/PC2/index.phpの場合
         * CONTEXT_PATH:/PC2
         * SERVER_PATH:/PC2/index.php
         * APP_PATH:hard disk上のディレクトリのpath  例:/home/ark/acution
         */
        $this->addTplParam("CONTEXT_PATH", Config::value("CONTEXT_PATH"));
        $this->addTplParam("SERVER_PATH", Config::value("SERVER_PATH"));
        $this->addTplParam("APP_PATH", Config::value("APP_PATH"));
        $this->addTplParam("HTTP_HOST", Config::value("HTTP_HOST"));
//        $this->addTplParam("STYLE_DIR",Config::value("CONTEXT_PATH")."/".AgentInfo::getStyleDir($theme));
        $this->addTplParam("CSS_PATH",Config::value("CONTEXT_PATH")."/".AgentInfo::getStyleDir($theme));
        $this->addTplParam("IMAGE_PATH",Config::value("CONTEXT_PATH")."/".AgentInfo::getStyleDir($theme)."/images");
        $this->addTplParam("CURRENT_URL", $_SERVER['REQUEST_URI']);
        $this->addTplParam("AGENT_DIR",AgentInfo::getAgentDir());
        //コインの名前
        $this->addTplParam("POINT_NAME", POINT_NAME);
        //コインの単価
        $this->addTplParam("POINT_VALUE", POINT_VALUE);
        //サイト名前
        $this->addTplParam("SITE_NAME", SITE_NAME);

        //現在のページに遷移する前のURL
        if(isset ($_SERVER["HTTP_REFERER"]))
            $this->addTplParam("PREV_URL",$_SERVER["HTTP_REFERER"]);
        //定義したテンプレートキーワードファイルを読み込み
        $this->loadTemplateWord();
    }

    /**
     *直接テキストを出力
     * @param <String> $text
     * @return ActionResultのインスタンス
     */
    public final function createTextResult($text) {
        import(FRAMEWORK_DIR.'.result.TextResult');
        return new TextResult($text);
    }

    /**
     *Requestのリダイレクト
     * @param <String> $url
     * @return ActionResultのインスタンス
     */
    public function createRedirectResult($url) {
        import(FRAMEWORK_DIR.'.result.RedirectResult');
        return new RedirectResult($url);
    }

    /**
     *Imageを出力処理
     * @param <resourc> $image 画像ソース
     * @param <String> $type 画像タイプ
     * @param <String> $filename ファイル名
     * @return ImageResult ActionResultのインスタンス
     */
    public final function createImageResult($image,$type,$filename="") {
        import(FRAMEWORK_DIR.'.result.ImageResult');
        import(FRAMEWORK_DIR.'.utils.Image');
        import(FRAMEWORK_DIR.'.utils.File');
        return new ImageResult($image,$type,$filename);
    }

    /**
     *認証画像を作成する
     * @return ImageResult ActionResultのインスタンス
     */
    public final function createVerifyImageResult() {
        import(FRAMEWORK_DIR.'.result.ImageResult');
        import(FRAMEWORK_DIR.'.utils.Image');
        import(FRAMEWORK_DIR.'.utils.File');
        $image=Image::buildImageVerify(Config::value("STRING_LENGTH"), Config::value("STRING_MODE"),
                Config::value("IMAGE_TYPE"),Config::value("IMAGE_WIDTH"),
                Config::value("IMAGE_HEIGHT"),Config::value("SESSION_VERIFY_NAME"),
                Config::value("IGNORE_CASW"));
        return new ImageResult($image,Config::value("IMAGE_TYPE"));
    }

    /**
     *入力した認証コートをチェックする
     * @return <boolean> 正確の場合:true
     */
    public final function checkVerifyCode() {
        $verifyName=Config::value("SESSION_VERIFY_NAME");
        $v=$this->getRequestParam($verifyName);
        if(empty ($v) || !isset($_SESSION[$verifyName]))
            return FALSE;
        $result= md5(strtolower($v))==$_SESSION[$verifyName];
        if($result===TRUE)
            unset($_SESSION[$verifyName]);
        return $result;
    }

    /**
     *HtmlフォームのToken検証コードの作成
     * @return <String>　<input type="hidden" name="'.$tokenName.'" value="'.$tokenValue.'" />
     */
    public final function buildFormToken() {
        //
        $tokenName   = Config::value("TOKEN_NAME");
        $tokenType = Config::value("TOKEN_TYPE");
        $tokenValue = $tokenType(microtime(TRUE));
        $token   =  '<input type="hidden" name="'.$tokenName.'" value="'.$tokenValue.'" />';
        $_SESSION[$tokenName]  =  $tokenValue;
        return $token;
    }

    /**
     *Tokenのチェック処理
     * @return <boolean> 検証結果
     */
    public final function checkToken() {
        $name= Config::value("TOKEN_NAME");
        if(isset($_SESSION[$name])) {
            //Token検証
            $value=$this->request->getParameter($name);
            if(empty($value) || $_SESSION[$name] != $value) {
                //検証失敗
                return false;
            }
            //検証した後、Tokenを削除する
            unset($_SESSION[$name]);
        }
        return true;
    }

    /**
     *リクエストのメソッドはPostかどうか、チェックする
     * @return <boolean>
     */
    public final function isPost() {
        if(isset($_SERVER["REQUEST_METHOD"])) {
            return $_SERVER["REQUEST_METHOD"]=="POST" ? true : false;
        }
        return FALSE;
    }

    /**
     * ログイン成功の場合、Sessionにログイン情報を追加
     */
    public final function addSessionInfo($member) {
        $_SESSION['isLogin']=TRUE;
        $_SESSION['member_id']=$member->fk_member_id;
        $_SESSION['member_handle']=$member->f_handle;
        import("classes.model.MemberModel");
        $model=new MemberModel();
        $model->updateMemberEntryTime($member->fk_member_id);
    }

    /**
     * セッションに登録されたデータを全て破棄する
     */
    public final function clearSessionInfo() {
        if(session_id()!="")
            session_destroy();
    }

    /**
     *指定されたセッション変数の登録を削除する
     * @param <string> $name セッション変数の名前
     */
    public final function removeSession($name) {
        if(isset($_SESSION[$name]))
            unset ($_SESSION[$name]);
    }

    /**
     *セッションにパラメータを追加する
     * @param <type> $name
     * @param <type> $value
     */
    public final function setSession($name,$value) {
        $_SESSION[$name]=$value;
    }

    /**
     *指定されたセッション名前の値を取得
     * @param <String> $name　セッション名前
     * @return <Object> 値
     */
    public final function session($name) {
        if(isset($_SESSION[$name]))
            return $_SESSION[$name];
        return FALSE;
    }

    /**
     *セッションからログイン者のmemberidを取得
     * @return <string> member_id
     */
    public final function getMemberIdFromSession() {
        return $this->session('member_id');
    }

    /**
     *チェックログイン状況
     * @return <type>
     */
    public final function isLogin() {

        if(isset($_SESSION['member_id'])) {
            import("classes.model.MemberModel");
            $model = new MemberModel();
            $member = $model->getMemberMasterById($_SESSION['member_id']);
            if(!empty($member) && $member->f_activity == 1 && $member->f_delete_flag != 1)
                return true;
            else{
                unset ($_SESSION['member_id']);
                return FALSE;
            }

        }
        return FALSE;
    }

    private function loadTemplateWord() {
        $file = Config::value("TEMPLATE_PATH")."/".AgentInfo::getAgentDir()."/".Config::value("TEMPLATE_WORD_FILE");
        $handle =fopen($file, "r");
        if ($handle) {
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
                $buffer=mb_convert_encoding($buffer, "UTF-8","auto");
                $buffer =trim($buffer);
                if(strlen($buffer)>2 && substr($buffer, 0, 2)=="//")
                    continue;
                if(!empty($buffer) && stripos($buffer, "=")!==FALSE) {
                    $index = stripos($buffer, "=");
                    $key=substr($buffer, 0, $index);
                    $value=substr($buffer, $index+1);
                    if(!empty ($key) && !empty ($value))
                        $this->addTplParam($key, $value);
                }
            }
            fclose($handle);
        }
    }



}
?>
