<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of MobileAction
 *
 * @author mw
 */
class MobileAction extends BaseAction {
    //put your code here

    public function __construct() {
        parent::__construct();
        //HTML内の相対リンクにセッションIDを自動的に追加しない
        ini_set('session.use_trans_sid',1);
        if(isset ($_REQUEST["PHPSESSID"])) {
            session_id($_REQUEST["PHPSESSID"]);
            //セッション
            session_start();
        }
        else {
            if(Config::value("SESSION_AUTO_START") || session_id()=="")
                session_start();
        }
    }

    /**
     * 端末判定
     */
    public static function getMobileType() {
        import(FRAMEWORK_DIR.'.utils.AgentInfo');
        $userAgent=AgentInfo::getUserAgent();
        if (ereg("DoCoMo", $userAgent))
            return "IMODE";
        else if (ereg("KDDI", $userAgent))
            return "KDDI_AU";
        else if (ereg("SoftBank", $userAgent))
            return "SOFTBANK";
        else if (ereg("Vodafone", $userAgent))
            return "SOFTBANK";
        else
            return "";
    }

    public function getPhoneCode() {
        $code=AgentInfo::getPhoneCode();
        return $code;
    }

    /**
     *チェックユーザセッション情報
     * @param <boolean> $addMemberToTemplate  TRUEの場合:ログインメンバー情報をテンプレートに追加する
     */
    public final function checkSessionInfo($addMemberToTemplate=True) {
        if(isset($_SESSION['member_id'])) {
            $this->addTplParam("isLogin",true);
            if($addMemberToTemplate)
                $this->addMemberToTemplate();
        }
    }

    /**
     *ログインメンバー情報をテンプレートに追加する
     */
    public final function addMemberToTemplate() {
        if(isset($_SESSION['member_id'])) {
            $member=$this->request->getAttribute("member");
            if(empty($member)) {
                //メンバー情報がテンプレートに存在しない場合、メンバーを取得する
                import('classes.model.MemberModel');
                $model=new MemberModel();
                $member=$model->getMemberMasterById($this->getMemberIdFromSession());
            }
            //コイン情報
            $member->coin_count=Utils::getInt($member->f_coin,0) + Utils::getInt($member->f_free_coin,0);
            $this->addTplParam("member", $member);
        }
    }



    /**
     *ベースクラスのメソッドをオーバーライド、javascriptのパスを追加する
     * @param <String> $tpl テンプレートの相対パス
     * @return TemplateResult ActionResultのインスタンス
     */
    public function createTemplateResult($tpl, $theme = '') {
        //絵文字対応
        $inc="";
        $type=AgentInfo::getPhoneType();
        switch (strtolower($type)) {
            case "docomo":
                $inc="./config/def_docomo.php";
                break;
            case "kddi":
                $inc="./config/def_au.php";
                break;
            case "softbank":
                $inc="./config/def_softbank.php";
                break;
            case "vodafone":
                $inc="./config/def_softbank.php";
                break;
            case "vodafone":
                $inc="./config/def_vodafone.php";
                break;
        }
        //テストコード、firefoxでagentを変えたことより、正しく絵文字が正しく表示されるため
        unset ($_SESSION['MOBILE_PICTURE_CODE']);
        //テストコード、end

        if(!empty ($inc) && !isset($_SESSION['MOBILE_PICTURE_CODE'])) {
            $pic=include($inc);
            $this->setSession("MOBILE_PICTURE_CODE", $pic);
        }
        return parent::createTemplateResult($tpl, $theme);
    }

    /**
     *Requestのリダイレクト
     * @param <String> $url
     * @return ActionResultのインスタンス
     */
    public function createRedirectResult($url) {
        import(FRAMEWORK_DIR.'.result.RedirectResult');
        $phone=AgentInfo::getPhoneType();
        if(!empty($phone) || $phone=="DoCoMo") {
            if (strpos($url,"?") > 0)
                $url .= "&";
            else
                $url .="?";
            $url.="PHPSESSID=".session_id();
        }
        return new RedirectResult($url);
    }

    /**
     *チェック携帯タイプ、DoCoMoの場合、固定識別番号を取れない場合、URLをredirectさせる
     * @return <ActionResult>
     */
    public final function checkPhoneSerNo() {
        import("classes.model.MemberModel");
        $model=new MemberModel();
        //固定識別番号を取得する
        $code=AgentInfo::getPhoneCode();
        if($this->isLogin()) {
            if(!empty ($code) && $model->isBlackSerNo($code)) {
                import("classes.utils.Message");
                $this->clearSessionInfo();
                return Message::showConfirmMessage($this, Msg::get("IS_BLACK_SER")," "," ");
            }
            return ;
        }

        if(empty($code)) {
            $phone = AgentInfo::getPhoneType();
            if(!empty ($phone) && $phone=="DoCoMo") {
                $guid=$this->getRequestParam("guid");
                if(empty ($guid) || $guid!="ON" ) {
                    //チェック携帯タイプ、DoCoMoの場合、固定識別番号を取れない場合、URLをredirectさせる
                    $redirectURL=Utils::addUrlParam($_SERVER["REQUEST_URI"], "guid=ON");
                    return $this->createRedirectResult($redirectURL);
                }
            }
            else {
                //未知の端末
            }
        }
        else {
            if($model->isBlackSerNo($code)) {
                import("classes.utils.Message");
                return Message::showConfirmMessage($this, Msg::get("IS_BLACK_SER")," "," ");
            }
            $this->addSessionInfoBySerNo($code);
            return null;
        }
    }

    /**
     *端末コードでユーザ情報をチェックし、会員の場合、セッションに追加する
     * @param <type> $code
     * @return <type>
     */
    public final function addSessionInfoBySerNo($code) {
        if(empty ($code))
            return ;
        import("classes.model.MemberModel");
        $model=new MemberModel();
        $member=$model->getMemberBySerNo($code);
        if(!empty ($member))
        //ユーザ情報をセッションに追加する
            $this->addSessionInfo($member);
    }



}
?>
