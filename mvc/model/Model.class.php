<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import(FRAMEWORK_DIR.'.database.DB');
/**
 * Description of Model
 *
 * @author mw
 */
class Model {
    /**
     * コンストラクタ
     */
    public function __construct() {
        //データベース初期化
//        DB::init(Config::value("host"),
//                 Config::value("port"),
//                 Config::value("user"),
//                 Config::value("pwd"),
//                 Config::value("dbname"));
//
//        $engine=createInstance(FRAMEWORK_DIR.'.database.engine.'.ucfirst(strtolower(Config::value("type"))).'Engine');
//        DB::setDataAccessEngine($engine);
    }
    
    /**
     * 指定された条件でSQL文のクリエートする
     * @param <String> $tableName
     * @param <array> $fields フィールドの配列
     * @return <String> sql文
     */
    private function createSelectSql($tableName,$fields){
    	if(empty($fields))
    		$columns="*";
    	else{
	    	$columns="";
	    	foreach ($fields as $value) 
	    		$columns.=$value.",";
	    	$columns=substr($columns,0,-1);
    	}
    	$sql="select $columns from $tableName";
    	return $sql;
    }

    /**
     * 指定されたテーブルのデータを取得し、Objectの配列として返す
     * @param <String> $tableName テーブル名前
     * @param <array> $fields フィールドの配列
     * @return Object配列  Objectの配列
     */
    public function selectObjects($tableName,$fields=''){
    	$sql=$this->createSelectSql($tableName,$fields);
    	$objs=DB::getObjectByQuery($sql);
    	return $objs;
    }
    
    /**
     * 指定されたテーブルのデータを取得し、配列として返す
     * @param <String> $tableName テーブル名前
     * @param <array> $fields フィールドの配列
     * @return <array> array配列
     */
    public function selectArray($tableName,$fields=''){
    	$sql=$this->createSelectSql($tableName,$fields);
    	$ar=DB::getArrayByQuery($sql);
    	return $ar;
    }
    
    /**
     * insert処理
     * @param <String> $tableName テーブル名前
     * @param <array> $fieldValues フィールド名と値のマッピング配列
     * @return <int> 影響を受けた行数、実行失敗の場合、-1を返し
     */
    public function insert($tableName,$fieldValues){
    	$fields="";$values="";
    	foreach ($fieldValues as $k=>$v) {
    		$fields.=$k.",";
    		$values.="?,";
    	}
    	$fields=substr($fields,0,-1);
    	$values=substr($values,0,-1);
    	$sql="insert into $tableName ($fields) values ($values)";
    	$rows=DB::executeNonQuery($sql,$fieldValues);
    	return $rows;
    	
    }

//    public function get($tableName,$id){
////        $sql=$this->createSelectSql($tableName, $fields)."wh"
//    }
    
    
    
    

}
?>
