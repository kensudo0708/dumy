<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if(!defined('APP_PATH')) define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
$GLOBALS['_START_TIME_']=microtime(true);

include(FRAMEWORK_DIR.'/function/functions.php');
import(FRAMEWORK_DIR.'.exception.AppException');
import(FRAMEWORK_DIR.'.exception.ExceptionHandler');
import(FRAMEWORK_DIR.'.exception.NotFoundException');
import(FRAMEWORK_DIR.'.config.Config');
import(FRAMEWORK_DIR.'.config.ConstKeys');
import(FRAMEWORK_DIR.'.log.Logger');
import(FRAMEWORK_DIR.'.utils.HashMap');
import(FRAMEWORK_DIR.'.utils.Utils');
import(FRAMEWORK_DIR.'.utils.StringBuilder');
import(FRAMEWORK_DIR.'.controller.Request');
import(FRAMEWORK_DIR.'.controller.ActionService');
import(FRAMEWORK_DIR.'.controller.ActionMapping');
import(FRAMEWORK_DIR.'.result.ActionResult');
import(FRAMEWORK_DIR.'.database.DB');
import(FRAMEWORK_DIR.'.action.BaseAction');
import(FRAMEWORK_DIR.'.model.Model');
import(FRAMEWORK_DIR.'.controller.SeverContext');
//import(FRAMEWORK_DIR.'.template.TemplateEngine');

/* *********** フレームワーク以外のファイル  ************ */

//include('function/define.php');
include('lib/define.php');
include('lib/server.php');
?>
