<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'Smarty.class.php';
$smarty=new Smarty();
$smarty->config_dir="Config_File.class.php";

$smarty->template_dir=Config::value("TEMPLATE_PATH");
$smarty->compile_dir=FRAMEWORK_DIR."/smarty/templates_c";

$smarty->caching=false;
$smarty->cache_dir="./smarty_cache";
$smarty->cache_lifetime=600;
$smarty->plugins_dir = array('plugins','../../classes/plugins' );

$smarty->left_delimiter="{%";
$smarty->right_delimiter="%}";
?>
