<?php /* Smarty version 2.6.26, created on 2010-12-13 19:28:37
         compiled from pc/buymethod.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div id="main_bg" class="pkg">
<div id="mynote1">
<h2>マイオークション</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/index_ext">マイページ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/coins"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
残高</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/watch">ウォッチリスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct">入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/autobid">自動入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/finish">落札リスト</a></li>
                </ul>



<h2>アカウント管理</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/profile">プロフィール変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/password">パスワード変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/email">メールアドレス変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address">配送先変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/mailmagazein">メルマガ変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/friend">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/close">退　会</a></li>
                </ul>
</div>

<div id="mynote2">

<h2><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入ページ</h2>
<div style="color:red"><?php echo $this->_tpl_vars['message']; ?>
</div>

<h3>
<?php echo $this->_tpl_vars['SITE_NAME']; ?>
ではオークションの入札手数料として「<?php echo $this->_tpl_vars['POINT_NAME']; ?>
」が必要です。<br>
</h3>
<p>
・オークション入札毎に<?php echo $this->_tpl_vars['POINT_NAME']; ?>
が必要になります。(１枚＝<?php echo $this->_tpl_vars['POINT_VALUE']; ?>
円)<br>
・ご購入された<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は、一切返金できませんのでご了承ください。<br>
・一度入札に利用した<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は、落札できる・できないにかかわらず返却できませんのでご了承ください。
</p>

               <!-- ここはコイン購入のメニュー  start -->
<h3>
<?php echo $this->_tpl_vars['POINT_NAME']; ?>
ご購入内容
</h3>
<p style="font-size:18px;font-weight:bold;line-height:1.8;">
ご購入内容：<?php echo $this->_tpl_vars['POINT_NAME']; ?>
<span style="color:red;"><?php echo $this->_tpl_vars['menu']->f_coin; ?>
</span>枚<br>
<!-- (<?php echo $this->_tpl_vars['menu']->f_coin; ?>
入札分)<br> -->
ご購入金額：<span style="color:red;"><?php echo $this->_tpl_vars['menu']->f_inp_money; ?>
</span>円
</p>

<h3>
お支払い方法選択
</h3>
以下のお支払い方法からひとつ選択し、決済に進むボタンをクリックしてください。<br>

                <script type="text/javascript">
                        //<![CDATA[
                        /*
                        if (!$.browser.msie) {
                            $("#not_ie").show();
                        } else {
                            $("#only_ie").show();
                        }
                        */
                        $("#payment_credit").attr('checked', 'checked');

                        function paymentSelect() {
                            $('#credit_box').toggle($("#payment_credit").attr('checked'));
                            $('#banking_box').toggle($("#payment_banking").attr('checked'));
                            $('#edy_box').toggle($("#payment_edy").attr('checked'));
                        }

                        //]]>
                    </script>

                <!--
                <form method="get" action="https://credit.ipservice.jp/gateway/payform.aspx">
                -->
                <!--削除禁止 ここから-->
                <form method="GET" action="/cli_ep.php/pcpay/">
                <input type="hidden" value="<?php echo $this->_tpl_vars['menu']->f_inp_money; ?>
" name="buy_money"/>
                <input type="hidden" value="<?php echo $this->_tpl_vars['menu']->f_coin; ?>
" name="coin_count"/>
                <input type="hidden" value="0" name="sf_money"/>
                <!--削除禁止 ここまで-->


<p id="payment_choice">
<!-- ++++++++++++++++++++++++++++++++++++++++ここはコイン購入のメニュー  start -->
<?php $_from = $this->_tpl_vars['methods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['buymethod'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['buymethod']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['method']):
        $this->_foreach['buymethod']['iteration']++;
?>
<?php if (($this->_foreach['buymethod']['iteration'] <= 1) == 1): ?>
<span>
<input type="radio"  onclick="paymentSelect()"  name="payment" id="payment_credit_<?php echo $this->_tpl_vars['method']->fk_shiharai_type_id; ?>
" value="<?php echo $this->_tpl_vars['method']->fk_shiharai_type_id; ?>
" checked><label for="payment_credit"><?php echo $this->_tpl_vars['method']->f_shiharai_name; ?>
</label>

</span><br>
<?php else: ?>
<span>
<input type="radio" onclick="paymentSelect()"  name="payment" id="payment_credit_<?php echo $this->_tpl_vars['method']->fk_shiharai_type_id; ?>
" value="<?php echo $this->_tpl_vars['method']->fk_shiharai_type_id; ?>
"><label for="payment_credit"><?php echo $this->_tpl_vars['method']->f_shiharai_name; ?>
</label>

</span><br>
<?php endif; ?>


<?php endforeach; endif; unset($_from); ?>
<!-- ++++++++++++++++++++++++++++++++++++ここはコイン購入のメニュー  end -->
</p>





<input type="submit" value="決済に進む" class="form_put"/>
<input type="hidden" value="<?php echo $this->_tpl_vars['menu']->fk_coin_id; ?>
" name="coin_menu_id"/>
<input type="hidden" value="<?php echo $this->_tpl_vars['type']; ?>
"	name="type"/>
</form>
</p>

</div>









</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>