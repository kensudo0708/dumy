<?php /* Smarty version 2.6.26, created on 2011-01-19 17:49:40
         compiled from pc/r_meminput.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['JAVASCRIPT_DIR']; ?>
/validationEngine.js" ></script>
<link href="<?php echo $this->_tpl_vars['CSS_PATH']; ?>
/validationEngine.css" media="screen" rel="stylesheet" type="text/css">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main_bg" class="pkg">

    <div id="adform">
        <h2>楽天ハンドル名決定</h2>

        <div style="color: red;"><?php echo $this->_tpl_vars['message']; ?>
</div>


        <div style="margin:0 auto;text-align:center;">
            <form method="post" action="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/regrakuten" id="new_user" class="new_user">
                <table width="100%">
                    <tr>
                        <td class="lside">ニックネーム</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['handle']; ?>
" name="handle" id="handle" gtbfieldid="318" class="validate[required,custom[handle],custom[handle_ng],length[3,15]]"></td>
                    </tr>
                    <tr>
                        <td class="lside" colspan=2>※ニックネームは入札時に表示されるあなたの名前になります。</td>
                    </tr>
                    <tr>
                        <td class="lside">ﾒｰﾙｱﾄﾞﾚｽ</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['mail_address']; ?>
" name="mail_address" gtbfieldid="321" id="mail_address" class="validate[required,custom[email],length[6,30]]"></td>
                    </tr>
                    <tr>
                        <td class="rside" colspan="2">
                            <input type="hidden" value="<?php echo $this->_tpl_vars['openId']; ?>
" name="openId">
                            <input type="hidden" value="memregist" name="mode">
                            <input type="submit" value="認証メール送信" id="submit">
                        </td>
                    </tr>

                </table>

                <script type="text/javascript">
                    function freshVerify(){
                        var time = new Date().getTime();
			document.getElementById('verifyImg').src= "<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/verify?time="+time;
                    }
                </script>

            </form>
        </div>
    </div>


</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>