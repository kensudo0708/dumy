<?php /* Smarty version 2.6.26, created on 2010-12-13 15:32:34
         compiled from pc/parts/footer_after.html */ ?>
<div id="main_bg" class="pkg">
<a id="validate" style="display: none"><?php echo $this->_tpl_vars['validate']; ?>
</a><!-- 削除禁止 -->

<div id="foot_box">

<div id="foot_box2">

	<div id="foot_box_l">

		<div id="foot_categ">
		<h3 class="catee1"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
について</h3>
		<ul>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
とは？</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">運営会社</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/terms">利用規約</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/privacy">プライバシーポリシー</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">特定商取引法に基づく表記</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/sitemap">サイトマップ</a></li>
		</ul>
		</div>

		<div id="foot_categ">
		<h3 class="catee2">ご利用ガイド</h3>
		<ul>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/how"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
の使い方</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/question">よくある質問</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/help">ヘルプ</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/register">新規会員登録</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/free?p=free1">お友達紹介</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/contact">お問い合わせ</a></li>
		<!-- コメント化　4/21
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/about_auction">上級者ガイド</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/auto_bid">自動落札とは？</a></li>
		-->
		</ul>
		</div>

		<div id="foot_categ">
		<h3 class="catee3">オークション</h3>
		<ul>
		<!-- <li><a href="#">カテゴリ一覧</a></li> -->
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/index_ext">マイページ</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/list">開催中オークション一覧</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/end">終了オークション一覧</a></li>
		<li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/winners_voice">落札者様のコメント</a></li>
		</ul>
		</div>

		<div id="foot_categ">
		<h3 class="catee3">お支払い方法</h3>
		<ul><li>各種クレジット、銀行決済、Edyに対応</li></ul><br>
		<img alt="支払い" src="/style/pc/default/images/kessai.gif">
		</div>

		<div id="foot_categ">
		<h3 class="catee3"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
モバイル</h3>
		<li><img src="/style/pc/default/images/qrcode.jpg" alt="ケータイでも<?php echo $this->_tpl_vars['SITE_NAME']; ?>
！" /></li>
		<li>docomo/au/softbank対応</li>
		</div>


	</div>


        <div class="clear"></div>

</div>

</div>

</div>