<?php /* Smarty version 2.6.26, created on 2011-01-14 10:01:38
         compiled from pc/IPSConvSelectStore.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div id="main_bg" class="pkg">
<div id="mynote1">
<h2>マイオークション</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my">マイページ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/coins"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
残高</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/watch">ウォッチリスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct">入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/autobid">自動入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/finish">落札リスト</a></li>
                </ul>



<h2>アカウント管理</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/profile">プロフィール変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/password">パスワード変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/email">メールアドレス変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address">配送先変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/mailmagazein">メルマガ変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/friend">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/close">退　会</a></li>
                </ul>
</div>

<div id="mynote2">

<h2>コンビニ決済ページ</h2>
<div style="color:red"><?php echo $this->_tpl_vars['message']; ?>
</div>

<h3>
<?php echo $this->_tpl_vars['SITE_NAME']; ?>
ではオークションの入札手数料として「<?php echo $this->_tpl_vars['POINT_NAME']; ?>
」が必要です。<br>
</h3>
<p>
・オークション入札毎に<?php echo $this->_tpl_vars['POINT_NAME']; ?>
が必要になります。(１枚＝<?php echo $this->_tpl_vars['POINT_VALUE']; ?>
円)<br>
・ご購入された<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は、一切返金できませんのでご了承ください。<br>
・一度入札に利用した<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は、落札できる・できないにかかわらず返却できませんのでご了承ください。
</p>

<h3>
<?php echo $this->_tpl_vars['POINT_NAME']; ?>
ご購入内容
</h3>
<p style="font-size:18px;font-weight:bold;line-height:1.8;">

   ご購入内容：<?php echo $this->_tpl_vars['POINT_NAME']; ?>
<span style="color:red;"><?php echo $this->_tpl_vars['coin_count']; ?>
</span><br>
   ご購入金額：<span style="color:red;"><?php echo $this->_tpl_vars['buy_money']; ?>
</span>円
</p>


<h3>
店舗選択
</h3>
以下からお支払い店舗を選択し、購入ボタンをクリックしてください。

<form method="GET" action="http://<?php echo $this->_tpl_vars['HTTP_HOST']; ?>
/cli_ep.php/pcpay/">
                    <input type="hidden" value="<?php echo $this->_tpl_vars['buy_money']; ?>
"  name="buy_money"/>
                    <input type="hidden" value="<?php echo $this->_tpl_vars['payment']; ?>
"    name="payment"/>
<p id="payment_choice">
<input type="radio" name="st" value="1">ローソン<BR>
<input type="radio" name="st" value="2">セブンイレブン<BR>
<input type="radio" name="st" value="3">ファミリーマート
</p>

<input type="submit" value="購入する" class="form_put">
</form>












</div>









</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>





