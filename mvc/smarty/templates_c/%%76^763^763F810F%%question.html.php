<?php /* Smarty version 2.6.26, created on 2011-01-05 18:20:52
         compiled from pc/question.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main_bg" class="pkg">



<div id="gguide1">
<h2>ご利用ガイド</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/how"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
の使い方</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/question">よくある質問</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/help">ヘルプ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/free?p=free1">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/contact">お問い合わせ</a></li>
                </ul>

<h2><?php echo $this->_tpl_vars['SITE_NAME']; ?>
について</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
とは？</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">運営会社</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/terms">利用規約</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/privacy">プライバシーポリシー</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">特定商取引法に基づく表記</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/sitemap">サイトマップ</a></li>
                </ul>
</div>





<div id="gguide2">
<h2>よくある質問</h2>





<table>
<tr>
<td class="tdttl2">Ｑ.<?php echo $this->_tpl_vars['SITE_NAME']; ?>
に参加するには？</td>
<td class="tdtext2">Ａ.まずは<?php echo $this->_tpl_vars['SITE_NAME']; ?>
への<a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/register">無料会員登録</a>を行ってください。（<?php echo $this->_tpl_vars['SITE_NAME']; ?>
に登録できるのは、日本にお住まいの個人に限ります。但し利用者が未成年者であるときは、本サービス利用の都度、本サービスの利用について事前に保護者の同意を得るものとします）<br>
登録が完了したら、コインを購入することによりオークションに入札することができるようになります。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.入札の方法は？</td>
<td class="tdtext2">Ａ.開催中の商品の中からお好きなものを選び、商品の金額等をご確認いただいてから「入札」ボタンをクリックしてください。現在入札されている方のお名前が金額の下に表示されます。<br>※必要に応じて<?php echo $this->_tpl_vars['POINT_NAME']; ?>
を購入してください</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.自動入札って何？</td>
<td class="tdtext2">Ａ.会員様ご自身で設定をしていただいた金額で自動的に入札を行う機能です。<br>詳しくはご利用ガイドをご覧ください。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.<?php echo $this->_tpl_vars['POINT_NAME']; ?>
を買うには？</td>
<td class="tdtext2">Ａ.「<?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入」のボタンより購入画面にお進みいただいて、購入する<?php echo $this->_tpl_vars['POINT_NAME']; ?>
を選択し、決済方法を選んでお支払いください。購入完了しますと、<?php echo $this->_tpl_vars['POINT_NAME']; ?>
の枚数が追加されます。枚数をご確認ください。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.<?php echo $this->_tpl_vars['POINT_NAME']; ?>
の決済方法は？</td>
<td class="tdtext2">Ａ.お支払いには銀行振込、インターネットバンク、クレジットカードがご利用いただけます。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.<?php echo $this->_tpl_vars['POINT_NAME']; ?>
の有効期限はあるの？</td>
<td class="tdtext2">Ａ.ご購入いただきました<?php echo $this->_tpl_vars['POINT_NAME']; ?>
につきましては、ご購入日から180日間とさせていただいております。
有効期限を過ぎると<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は消滅し利用できなくなりすのでご注意ください。<br>
友達紹介等によって獲得したボーナス<?php echo $this->_tpl_vars['POINT_NAME']; ?>
の有効期限は入手から30日間となります。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.落札した商品はどこから<br>購入できますか？</td>
<td class="tdtext2">Ａ.商品の詳細画面もしくはマイページの落札済リストからご購入いただけます。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.落札商品の決済方法は？</td>
<td class="tdtext2">Ａ.お支払いには銀行振込、インターネットバンク、クレジットカードがご利用いただけます。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.送料はかかりますか？</td>
<td class="tdtext2">Ａ.一律315円の送料がかかります。</td>
</tr>
</tr>
<tr>
<td class="tdttl2">Ｑ.落札商品の購入期限は<br>ありますか？</td>
<td class="tdtext2">Ａ.落札してから21日間です。それを過ぎますと、購入できなくなりますのでご注意ください。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.商品の保証書は有りますか？</td>
<td class="tdtext2">Ａ.<?php echo $this->_tpl_vars['SITE_NAME']; ?>
で取り扱っている商品はすべて新品ですので保証書も付いております。保証書の捺印はされている場合と、されていない場合があります。捺印がされていない場合は、一緒に送付されます納品書が捺印の代わりになりますので、大切に保管をお願いします。 </td>
</tr>
<tr>
<td class="tdttl2">Ｑ.オークションの種類は<br>何種類ありますか？</td>
<td class="tdtext2">Ａ.<?php echo $this->_tpl_vars['SITE_NAME']; ?>
では色々な種類のオークションを開催しております。詳しくはヘルプのオークションについてをご覧ください。</td>
</tr>
<tr>
<td class="tdttl2">Ｑ.商品を出品したいのですが</td>
<td class="tdtext2">Ａ.<?php echo $this->_tpl_vars['SITE_NAME']; ?>
では、商品を出品することができません。オークションに出品されている商品は、すべて新品、メーカー保証書付きの商品となっております。</td>
</tr>
</table>



</div>




</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>