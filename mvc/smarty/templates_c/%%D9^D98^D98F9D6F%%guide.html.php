<?php /* Smarty version 2.6.26, created on 2010-12-22 16:10:54
         compiled from pc/guide.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main_bg" class="pkg">



<div id="gguide1">
<h2>ご利用ガイド</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/how"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
の使い方</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/question">よくある質問</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/help">ヘルプ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/free?p=free1">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/contact">お問い合わせ</a></li>
                </ul>

<h2><?php echo $this->_tpl_vars['SITE_NAME']; ?>
について</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
とは？</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">運営会社</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/terms">利用規約</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/privacy">プライバシーポリシー</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">特定商取引法に基づく表記</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/sitemap">サイトマップ</a></li>
                </ul>
</div>


<div id="gguide2">
<h2><?php echo $this->_tpl_vars['SITE_NAME']; ?>
とは？</h2>
<?php echo $this->_tpl_vars['SITE_NAME']; ?>
は、最新の家電や人気ゲーム機など様々な商品が豊富です！手軽に格安で落札できる新感覚のオークションです！今までの中古品オークションとは違いすべて新品でお届けします！
入札状況によっては最大【98％オフ】で落札することも！？<br>
<br>
会員は、欲しい商品の入札ボタンを押すだけで手軽に参加が出来るシンプルなルールになっています。<br>
<br>
全てのオークションは0円からスタート。入札には<?php echo $this->_tpl_vars['POINT_NAME']; ?>
という入札専用のコインが必要となります。<br>
入札毎に<?php echo $this->_tpl_vars['POINT_NAME']; ?>
が消費され、入札価格が1円～15円アップ（オークションの種類によって異なります）また、1入札でオークションの時間が20秒延長されます。<br>
残り時間がゼロになった時点での最終入札者が落札者となり、その落札価格で商品を購入することができます。<br>
<br>
欲しい商品を手軽で安く購入することが出来るのは！このオークションの最大のメリットです！<br>
是非チャレンジしてください！<br>
<br>
<?php echo $this->_tpl_vars['SITE_NAME']; ?>
をご利用の際は、無料の会員登録が必要となります。<br>
無料会員登録をしたらまずは、<?php echo $this->_tpl_vars['POINT_NAME']; ?>
が無くても参加できるフリーオークションで、<?php echo $this->_tpl_vars['SITE_NAME']; ?>
を体験してください！<br>
慣れたら<?php echo $this->_tpl_vars['POINT_NAME']; ?>
を購入し、人気商品のオークションに参加して欲しい商品をGETしてください！


</div>


</div>



<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>