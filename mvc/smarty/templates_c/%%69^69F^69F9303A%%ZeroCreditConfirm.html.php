<?php /* Smarty version 2.6.26, created on 2011-01-14 10:02:51
         compiled from pc/ZeroCreditConfirm.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div id="main_bg" class="pkg">
<div id="mynote1">
<h2>マイオークション</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my">マイページ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/coins"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
残高</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/watch">ウォッチリスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct">入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/autobid">自動入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/finish">落札リスト</a></li>
                </ul>



<h2>アカウント管理</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/profile">プロフィール変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/password">パスワード変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/email">メールアドレス変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address">配送先変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/mailmagazein">メルマガ変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/friend">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/close">退　会</a></li>
                </ul>
</div>

<div id="mynote2">

<h2>ZEROクレジット決済ページ</h2>
<div style="color:red"><?php echo $this->_tpl_vars['message']; ?>
</div>

   <!-- ここはコイン購入のメニュー  start -->
<h3>ZEROクレジットでご購入いただけます。</h3>


   <?php if ($this->_tpl_vars['type'] == '0'): ?>

<h3>
<?php echo $this->_tpl_vars['POINT_NAME']; ?>
ご購入内容
</h3>
<p style="font-size:18px;font-weight:bold;line-height:1.8;">

   ご購入内容：<?php echo $this->_tpl_vars['POINT_NAME']; ?>
<span style="color:red;"><?php echo $this->_tpl_vars['coin']->count; ?>
</span><br>
   ご購入金額：<span style="color:red;"><?php echo $this->_tpl_vars['coin']->total_price; ?>
</span>円
</p>
   <?php else: ?>

<h3>
落札商品のご購入方法選択
</h3>
<p style="font-size:18px;font-weight:bold;line-height:1.8;">
   商品名：<span style="color:red;"><?php echo $this->_tpl_vars['product']->f_products_name; ?>
</span><br>
   落札価格：<span style="color:red;"><?php echo $this->_tpl_vars['product']->f_end_price; ?>
</span>円<br>
   運賃：<span style="color:red;"><?php echo $this->_tpl_vars['carriage']; ?>
</span>円<br>
</p>
   <?php endif; ?>




<table border="0">
<tr>
<td align="center">
<a href="https://credit.zeroweb.ne.jp/cgi-bin/pc_exp.cgi?clientip=1019000091">クレジットカード決済に関するご説明<br>必ずお読みください</a><br><br>
カード決済に関するお問い合わせ<br>
決済システムは（株）ゼロを利用しています<br>
TEL:0570-03-6000（TEL03-3498-6200）<br>
<a href="mailto:creditinfo@zeroweb.co.jp">creditinfo@zeroweb.co.jp</a><br>
ゼロカスタマーサポート（24時間365日)<br>
</td>
</tr>
</table>



<h3>
確認
</h3>
購入内容に間違いが無ければ、購入ボタンをクリックしてください。
<form method="POST" action="<?php echo $this->_tpl_vars['jump_url']; ?>
">
    <input type="hidden" value="<?php echo $this->_tpl_vars['buy_money']; ?>
"  name="buy_money"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['clientip']; ?>
"   name="clientip"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['send']; ?>
"       name="send"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['custom']; ?>
"     name="custom"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['money']; ?>
"      name="money"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['usrtel']; ?>
"     name="usrtel"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['usrmail']; ?>
"    name="usrmail"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['sendid']; ?>
"     name="sendid"/>
    <input type="hidden" value="<?php echo $this->_tpl_vars['sendpoint']; ?>
"  name="sendpoint"/>


<input type="submit" value="購入する" class="form_put">
</form>



</div>









</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>
