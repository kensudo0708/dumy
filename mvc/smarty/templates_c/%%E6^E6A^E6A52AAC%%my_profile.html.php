<?php /* Smarty version 2.6.26, created on 2011-01-20 17:54:40
         compiled from pc/parts/my_profile.html */ ?>
<h2><?php echo $this->_tpl_vars['session_member']->f_handle; ?>
さん　のプロフィール変更ページ</h2>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['JAVASCRIPT_DIR']; ?>
/validationEngine.js" ></script>
<link href="<?php echo $this->_tpl_vars['CSS_PATH']; ?>
/validationEngine.css" media="screen" rel="stylesheet" type="text/css">
<form method="post" id="edit_use" class="edit_user" action="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/update_profile">
<table id="address_entry">
<tr>
<td class="tdttl2">氏　名</td>
<td class="tdtext2"><input type="text" name="f_handle" value="<?php echo $this->_tpl_vars['member']->f_handle; ?>
"/></td>
</tr>
<tr>
<td class="tdttl2">誕生日</td>
<td class="tdtext2">
<select name="birth_year" id="birth_year"><!-- class="validate[required,custom[onlyNumber]">-->
<option value="00">年 </option>
<?php $_from = $this->_tpl_vars['brithday']['year']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
<option value="<?php echo $this->_tpl_vars['year']; ?>
" <?php if ($this->_tpl_vars['member']->birth_year == $this->_tpl_vars['year']): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>年
<select name="birth_month" id="birth_month"><!-- class="validate[required,custom[onlyNumber]">-->
<option value="00">月</option>
<?php $_from = $this->_tpl_vars['brithday']['month']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['month']):
?>
<option value="<?php echo $this->_tpl_vars['month']; ?>
" <?php if ($this->_tpl_vars['member']->birth_month == $this->_tpl_vars['month']): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['month']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select>月
<select name="birth_day" id="birth_day"><!-- class="validate[required,custom[onlyNumber]">-->
<option value="00">日</option>
<?php $_from = $this->_tpl_vars['brithday']['day']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['day']):
?>
<option value="<?php echo $this->_tpl_vars['day']; ?>
" <?php if ($this->_tpl_vars['member']->birth_day == $this->_tpl_vars['day']): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['day']; ?>


</option>
<?php endforeach; endif; unset($_from); ?>
</select>日
 </td>
</tr>
<tr>
<td class="tdttl2">名　前</td>
<td class="tdtext2"><input type="text" name="name" value="<?php echo $this->_tpl_vars['member']->f_name; ?>
"/></td>
</tr>
<tr>
<td class="tdttl2">電話番号</td>
<td class="tdtext2"><input type="tel" name="tel" value="<?php echo $this->_tpl_vars['member']->f_tel_no; ?>
"/></td>
</tr>
<tr>
 <td class="tdttl2">性別</td>
 <td class="tdtext2">
   <select name="sex" id="sex" >
     <option value="">---</option>
     <option value="0" <?php if ($this->_tpl_vars['member']->f_sex == '0'): ?>selected="selected" <?php endif; ?>>男性</option>
     <option value="1" <?php if ($this->_tpl_vars['member']->f_sex != '0'): ?>selected="selected" <?php endif; ?>>女性</option>
   </select>
 </td>
</tr>
<!--
<tr>
<td class="tdttl2">旧パスワード</td>
<td class="tdtext2"><input type="password" name="old_pass"/></td>
</tr>
<tr>
<td class="tdttl2">新しいパスワード</td>
<td class="tdtext2"><input type="password" name="new_pass"/></td>
</tr>
<tr>
<td class="tdttl2">再入力</td>
<td class="tdtext2"><input type="password" name="repeat_pass"/></td>
</tr>
-->
<tr>
<td></td>
<td>
<input type="submit" value="　変　更　" class="form_put">
</td>
</tr>
</table>
</form>