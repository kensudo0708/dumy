<?php /* Smarty version 2.6.26, created on 2010-12-27 12:24:42
         compiled from pc/parts/my_finish_adrnew.html */ ?>
<h2><?php echo $this->_tpl_vars['member']->f_handle; ?>
さん　の落札商品配送先登録</h2>



<script type="text/javascript" src="<?php echo $this->_tpl_vars['JAVASCRIPT_DIR']; ?>
/validationEngine.js" ></script>
<link href="<?php echo $this->_tpl_vars['CSS_PATH']; ?>
/validationEngine.css" media="screen" rel="stylesheet" type="text/css">

    <form method="post" id="new_address" action="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/mypay/new_address">
        <input type="hidden" value="<?php echo $this->_tpl_vars['product']->fk_end_products_id; ?>
" name="pro_id">
        配送先住所を記入してください。

<table id="address">
<tr>
                <td class="tdttl2">配送先名</td>
                <td class="tdtext2">
                    <input type="text" value="<?php echo $this->_tpl_vars['address']['f_name']; ?>
" size="30" name="f_name" id="name" class="validate[required,length[1,20]]">
                </td>
            </tr>
            <tr>
                <td class="tdttl2">連絡電話番号*</td>
                <td class="tdtext2"><input type="text" value="<?php echo $this->_tpl_vars['address']['f_tel_no']; ?>
" size="30" name="f_tel_no" id="user_phone_number" class="validate[required,custom[telephone]]" gtbfieldid="44"></td>
            </tr>
            <tr>
                <td class="tdttl2">郵便番号*</td>
                <td class="tdtext2"><input type="text" value="<?php echo $this->_tpl_vars['address']['f_post_code']; ?>
" size="9" name="f_post_code"  class="validate[required]"></td>
            </tr>
            <tr>
                <td class="tdttl2">都道府県*</td>
                <td class="tdtext2">
                    <select name="fk_perf_id" id ="fk_perf_id" class="validate[required]">
                        <option value="0" >  選択  </option>
                        <?php $_from = $this->_tpl_vars['perfs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['perf']):
?>
                        <option value="<?php echo $this->_tpl_vars['perf']->t_pref_id; ?>
" <?php if ($this->_tpl_vars['address']['fk_perf_id'] == $this->_tpl_vars['perf']->t_pref_id): ?>selected="selected" <?php endif; ?>>
                        <?php echo $this->_tpl_vars['perf']->t_pref_name; ?>

                        </option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="tdttl2">市区町村*</td>
                <td class="tdtext2"><input type="text" value="<?php echo $this->_tpl_vars['address']['f_address1']; ?>
" size="60" name="f_address1" id="user_address1" class="validate[required,length[1,50]]"></td>
            </tr>
            <tr>
                <td class="tdttl2">番地*</td>
                <td class="tdtext2"><input type="text" value="<?php echo $this->_tpl_vars['address']['f_address2']; ?>
" size="60" name="f_address2" id="user_address2" class="validate[required,length[1,50]]"></td>
            </tr>
            <tr>
                <td class="tdttl2">建物名など</td>
                <td class="tdtext2"><input type="text" value="<?php echo $this->_tpl_vars['address']['f_address3']; ?>
" size="60" name="f_address3" id="user_address3" class="validate[length[0,50]]"></td>
            </tr>

            <tr>
                <td colspan="2" style="border: medium none; font-size: 0.8em; color: rgb(67, 67, 67);">* は必須項目です</td>
            </tr>

    </table>
        <div align="center"><input type="submit" value="次へ" class="form_put"></div>

    </form>
</div>