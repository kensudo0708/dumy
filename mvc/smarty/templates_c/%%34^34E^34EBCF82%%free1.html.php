<?php /* Smarty version 2.6.26, created on 2011-01-05 18:20:53
         compiled from pc/free1.html */ ?>
free1.html<br>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main_bg" class="pkg">


<div id="gguide1">
<h2>ご利用ガイド</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/how"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
の使い方</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/question">よくある質問</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/help">ヘルプ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/free?p=free1">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/contact">お問い合わせ</a></li>
                </ul>

<h2><?php echo $this->_tpl_vars['SITE_NAME']; ?>
について</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide"><?php echo $this->_tpl_vars['SITE_NAME']; ?>
とは？</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">運営会社</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/terms">利用規約</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/privacy">プライバシーポリシー</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/company">特定商取引法に基づく表記</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/guide/sitemap">サイトマップ</a></li>
                </ul>
</div>





<div id="gguide2">
<h2>友達紹介</h2>
<strong><?php echo $this->_tpl_vars['SITE_NAME']; ?>
をあなたのお友達に紹介してください！</strong><br>
手続きは簡単。<br>
マイページの「友達紹介」より招待メールを送信するだけ。<br>
招待したお友達が<?php echo $this->_tpl_vars['SITE_NAME']; ?>
に入会すれば、特典として●●●をあなたにプレゼント！！
</div>





</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>