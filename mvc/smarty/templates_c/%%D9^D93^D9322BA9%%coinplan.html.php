<?php /* Smarty version 2.6.26, created on 2010-12-13 19:28:03
         compiled from pc/coinplan.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div id="main_bg" class="pkg">
<div id="mynote1">
<h2 id="menu_my"><img src="<?php echo $this->_tpl_vars['IMAGE_PATH']; ?>
/spacer.gif" alt="マイオークション"/></h2>
                <ul class="bk_orange">
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/index_ext">マイページ</a></li>
                    <li class="on"><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/coins"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
残高</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/watch">ウォッチリスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct">入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/autobid">自動入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/finish">落札リスト</a></li>
                </ul>



<h2 id="menu_acc"><img src="<?php echo $this->_tpl_vars['IMAGE_PATH']; ?>
/spacer.gif" alt="アカウント管理"/></h2>
                <ul class="bk_blue">
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/profile">プロフィール変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/password">パスワード変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/email">メールアドレス変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address">配送先変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/mailmagazein">メルマガ変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/friend">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/close">退　会</a></li>
                </ul>
</div>

<div id="mynote2">

<h2 class="title_my"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入ページ</h2>
<div style="color:red"><?php echo $this->_tpl_vars['message']; ?>
</div>

<h3>
<?php echo $this->_tpl_vars['SITE_NAME']; ?>
ではオークションの入札手数料として「<?php echo $this->_tpl_vars['POINT_NAME']; ?>
」が必要です。<br>
購入したい<?php echo $this->_tpl_vars['POINT_NAME']; ?>
(枚数）を選択し購入ボタンをクリックしてください。
</h3>


購入ボタンをクリックすると決済選択画面に移動します。


<!--    [foreach....../foreach]はコイン購入プランデータをループ -->
<?php $_from = $this->_tpl_vars['menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['coin']):
?>
<table summary="ポケオクのコイン購入ページ">
<tr>
<td class="tdttl2">
<?php echo $this->_tpl_vars['coin']->f_coin; ?>
枚（<?php echo $this->_tpl_vars['coin']->f_inp_money; ?>
円）
</td>
<td class="tdtext2">

<a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin/buy?coin_id=<?php echo $this->_tpl_vars['coin']->fk_coin_id; ?>
">購入</a>
</td>
</tr>
</table>
<?php endforeach; endif; unset($_from); ?>
<!--    [foreach....../foreach]はコイン購入プランデータをループ -->
<!--G.Chin APROS 2010-09-01 add sta-->
<br>
<br>
<?php if ($this->_tpl_vars['publicsite_cnt'] > 0): ?>
       ↓公式サイト決済を利用される方はこちらをクリック。<br>
　　　<?php echo $this->_tpl_vars['publicsite_str']; ?>

<?php endif; ?>
<!--G.Chin APROS 2010-09-01 add end-->


</div>









</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>



