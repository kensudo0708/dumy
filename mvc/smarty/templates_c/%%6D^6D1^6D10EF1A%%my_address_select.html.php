<?php /* Smarty version 2.6.26, created on 2010-12-27 12:23:54
         compiled from pc/parts/my_address_select.html */ ?>
<h2><?php echo $this->_tpl_vars['member']->f_handle; ?>
さん　の配送先設定ページ</h2>
配送先の登録・修正を行います。複数の配送先が登録可能です。<br>
（商品配送先は商品一つあたり一箇所のみとなります。）
<div style="border:1px solid #ccc;padding:10px;">
    <form method="post" action="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address_edit">
        <ul style="font-size: 17px">
           
            <?php $_from = $this->_tpl_vars['address']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ad']):
?>
            <li style="margin:5px;">
                <input type="radio" name="address" value="<?php echo $this->_tpl_vars['ad']->fk_address_id; ?>
"><?php echo $this->_tpl_vars['ad']->f_name; ?>

            </li>
            <?php endforeach; endif; unset($_from); ?>
            <li style="margin:5px;">
                <input type="radio" name="address" checked value="new">新規追加
            </li>
        </ul>
</div>
        <input type="submit" value="次へ" class="form_put">
    </form>
