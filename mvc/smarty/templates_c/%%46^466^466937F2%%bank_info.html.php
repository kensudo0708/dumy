<?php /* Smarty version 2.6.26, created on 2011-01-14 10:01:58
         compiled from pc/bank_info.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div id="main_bg" class="pkg">
<div id="mynote1">
<h2>マイオークション</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my">マイページ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/coins"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
残高</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/watch">ウォッチリスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct">入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/autobid">自動入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/finish">落札リスト</a></li>
                </ul>



<h2>アカウント管理</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/profile">プロフィール変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/password">パスワード変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/email">メールアドレス変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address">配送先変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/mailmagazein">メルマガ変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/friend">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/close">退　会</a></li>
                </ul>
</div>

<div id="mynote2">

<h2>銀行振り込み</h2>
<div style="color:red"><?php echo $this->_tpl_vars['message']; ?>
</div>

<h3>
<?php echo $this->_tpl_vars['SITE_NAME']; ?>
ではオークションの入札手数料として「<?php echo $this->_tpl_vars['POINT_NAME']; ?>
」が必要です。<br>
</h3>
<p>
・オークション入札毎に<?php echo $this->_tpl_vars['POINT_NAME']; ?>
が必要になります。(１枚＝<?php echo $this->_tpl_vars['POINT_VALUE']; ?>
円)<br>
・ご購入された<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は、一切返金できませんのでご了承ください。<br>
・一度入札に利用した<?php echo $this->_tpl_vars['POINT_NAME']; ?>
は、落札できる・できないにかかわらず返却できませんのでご了承ください。
</p>







<?php if ($this->_tpl_vars['type'] == 0): ?>
<h3>
<?php echo $this->_tpl_vars['POINT_NAME']; ?>
ご購入内容
</h3>
<p style="font-size:18px;font-weight:bold;line-height:1.8;">
ご購入内容：<?php echo $this->_tpl_vars['POINT_NAME']; ?>
<span style="color:red;"><?php echo $this->_tpl_vars['coin']->f_coin; ?>
</span>枚<br>
振り込み額：<span style="color:red;"><?php echo $this->_tpl_vars['coin']->f_inp_money; ?>
</span>円
</p>

    <?php else: ?>
 <h3>
<?php echo $this->_tpl_vars['POINT_NAME']; ?>
ご購入内容
</h3>
<p style="font-size:18px;font-weight:bold;line-height:1.8;">
商品名：<span style="color:red;"><?php echo $this->_tpl_vars['product']->f_products_name; ?>
</span><br>
振り込み額：<span style="color:red;"><?php echo $this->_tpl_vars['product']->total_price; ?>
</span>円
</p>

    <?php endif; ?>


 <h3>
お振り込みの前に必ずご確認ください。
</h3>
<p>
<span style="color:red;">ご注意ください</span>：お振込の際は必ず【振込名義】にお客様の専用の<span style="color:red;">ログインIDのみ</span>をご入力下さい。当サイトは会員情報をIDにて管理しているため口座名義による実名、電話番号（認証を取っていない為）を入力されましてもご本人確認が取れないため処理できず不明金となるケースがありますのでご注意ください。<br>
<span style="color:red;">※</span>お客様のログインID:<span style="color:red;"><?php echo $this->_tpl_vars['member']->f_login_id; ?>
</span>
</p>


<h3>
振込口座
</h3>
下記の銀行口座に上記振り込み額をご入金ください。
<table>
<tr>
<td class="tdttl2">振込先銀行名</TD>
<td class="tdtext2">三菱東京ＵＦＪ銀行(コード:0005)</td>
</tr>
<tr>
<td class="tdttl2">支店名</TD>
<td class="tdtext2"></td>
</tr>
<tr>
<td class="tdttl2">預金科目</TD>
<td class="tdtext2">普通</td>
</tr>
<tr>
<td class="tdttl2">口座番号</TD>
<td class="tdtext2"></td>
</tr>
<tr>
<td class="tdttl2">受取人口座名義</TD>
<td class="tdtext2">ユ）アークコミュニケーションズ</td>
</tr>
<!--
   <tr><td><NOBR></TD><td><NOBR></td></tr>

   <tr><td><NOBR>振込先銀行名</TD><td><NOBR>ジャパンネット銀行(:0033)</td></tr>
   <tr><td><NOBR>振込先支店名</TD><td><NOBR>すずめ支店（スズメ）(コード:002)</td></tr>
   <tr><td><NOBR>振込先預金科目</TD><td><NOBR>普通</td></tr>
   <tr><td><NOBR>振込先口座番号</TD><td><NOBR></td></tr>
   <tr><td><NOBR>受取人口座名義</TD><td><NOBR>カ）</td></tr>
-->
</table>
 




</div>









</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>