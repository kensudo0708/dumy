<?php /* Smarty version 2.6.26, created on 2010-12-13 15:32:37
         compiled from pc/my.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['page_name'] == 'index'): ?>
<!-- マイページのindex -->

<?php elseif ($this->_tpl_vars['page_name'] == 'address'): ?>
<!-- 配送先変更 -->

<?php elseif ($this->_tpl_vars['page_name'] == 'profile'): ?>
<!-- プロフィール変更 -->

<?php elseif ($this->_tpl_vars['page_name'] == 'password'): ?>
<!-- パスワード変更 -->

<?php elseif ($this->_tpl_vars['page_name'] == 'mailmagazein'): ?>
<!-- メルアド変更 -->

<?php elseif ($this->_tpl_vars['page_name'] == 'close'): ?>
<!-- 退会手続き -->

<?php elseif ($this->_tpl_vars['page_name'] == 'coin'): ?>
<!-- コイン購入 -->

<?php elseif ($this->_tpl_vars['page_name'] == 'coins'): ?>
<!-- コイン購入履歴 -->


<?php elseif ($this->_tpl_vars['page_name'] == 'email'): ?>
<!-- メール変更 -->


<?php elseif ($this->_tpl_vars['page_name'] == 'watch'): ?>
<!-- ウォッチリスト -->

<?php elseif ($this->_tpl_vars['page_name'] == 'myproduct'): ?>
<!-- 入札中オクション -->

<?php elseif ($this->_tpl_vars['page_name'] == 'autobid'): ?>
<!-- 自動入札中オークション -->

<?php elseif ($this->_tpl_vars['page_name'] == 'finish'): ?>
<!-- 落札済み一覧 -->

<?php elseif ($this->_tpl_vars['page_name'] == 'friend'): ?>
<!-- 友達紹介 -->

<?php endif; ?>

<!-- <script type="text/javascript" src="<?php echo $this->_tpl_vars['JAVASCRIPT_DIR']; ?>
/ark.js"></script> --><!--消去禁止-->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TEMPLATE_PARTS_DIR']; ?>
/ark.js"></script><!--削除禁止-->

<div id="main_bg" class="pkg">
<div id="mynote1">
<h2>マイオークション</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/index_ext">マイページ</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/coin"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
購入</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/coins"><?php echo $this->_tpl_vars['POINT_NAME']; ?>
残高</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/watch">ウォッチリスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct">入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/autobid">自動入札中リスト</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/myproduct/finish">落札リスト</a></li>
                </ul>



<h2>アカウント管理</h2>
                <ul>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/profile">プロフィール変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/password">パスワード変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/email">メールアドレス変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/address">配送先変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/mailmagazein">メルマガ変更</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/friend">友達紹介</a></li>
                    <li><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/my/close">退　会</a></li>
                </ul>
</div>

<div id="mynote2">


<!-- コンテンツ内容インクル -->
<div style="color:red;"><?php echo $this->_tpl_vars['message']; ?>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_file'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- コンテンツ内容インクル -->



</div>

<!--AFFILIATE:アフィリエイトのタグ:削除禁止！-->
<?php echo $this->_tpl_vars['affiliate_tag']; ?>

<?php echo $this->_tpl_vars['affiliate_tag2']; ?>

<!--AFFILIATE:アフィリエイトのタグ:削除禁止！-->









</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>