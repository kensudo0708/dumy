<?php /* Smarty version 2.6.26, created on 2011-01-18 19:39:04
         compiled from pc/register.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['JAVASCRIPT_DIR']; ?>
/validationEngine.js" ></script>
<link href="<?php echo $this->_tpl_vars['CSS_PATH']; ?>
/validationEngine.css" media="screen" rel="stylesheet" type="text/css">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main_bg" class="pkg">

    <div id="adform">
        <h2>新規無料会員登録</h2>

        <div style="color: red;"><?php echo $this->_tpl_vars['message']; ?>
</div>
<p>必要項目に入力し送信してください。<br>
数分後にご登録頂いたメールアドレスに仮登録メールが送られます。<br>
仮登録メール本文に記載されたリンクをクリックすれば登録完了となります。</p>


        <div style="margin:0 auto;text-align:center;">
            <form method="post" action="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/register" id="new_user" class="new_user">
                <table width="100%">
                    <tr>
                        <td class="lside">ニックネーム</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['handle']; ?>
" name="handle" id="handle" gtbfieldid="318" class="validate[required,custom[handle],custom[handle_ng],length[3,15]]"></td>
                    </tr>
                    <tr>
                        <td class="lside">ﾛｸﾞｲﾝID</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['login_id']; ?>
" name="login_id" id="login_id" gtbfieldid="319" class="validate[required,custom[noSpecialCaracters],length[3,40]]"></td>
                    </tr>
                    <tr>
                        <td class="lside">ﾛｸﾞｲﾝPASS</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['login_pass']; ?>
" name="login_pass" gtbfieldid="320" id="login_pass" class="validate[required,length[6,30]]"></td>
                    </tr>
                    <tr>
                        <td class="lside">ﾒｰﾙｱﾄﾞﾚｽ</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['mail_address']; ?>
" name="mail_address" gtbfieldid="321" id="mail_address" class="validate[required,custom[email],length[6,30]]"></td>
                    </tr>
                    <tr>
                        <td class="lside">性別</td>
                        <td class="rside"><select name="sex" gtbfieldid="322" ><option value="0">男性</option><option value="2">女性</option></select></td>
                    </tr>
<!--
                    <tr>
                        <td class="lside">電話番号</td>
                        <td class="rside"><input type="text" istyle="4" value="<?php echo $this->_tpl_vars['p']['tel_no']; ?>
" name="tel_no" gtbfieldid="323" id="login_pass" class="validate[required,custom[telephone],length[10,20]]"></td>
                    </tr>
-->
                    <tr>
                        <td class="lside">生年月日</td>
                        <td class="rside"><select name="birth_year" id="birth_year"><!-- 

class="validate[required,custom[onlyNumber]">-->
                                <option value="00">  年  </option>
                                <?php $_from = $this->_tpl_vars['brithday']['year']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
                                <option value="<?php echo $this->_tpl_vars['year']; ?>
" <?php if ($this->_tpl_vars['p']['birth_year'] == $this->_tpl_vars['year']): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>


</option>
                                <?php endforeach; endif; unset($_from); ?>
                            </select>
                            <select name="birth_month" id="birth_month"><!-- class="validate[required,custom[onlyNumber]">-->
                                <option value="00">月</option>
                                <?php $_from = $this->_tpl_vars['brithday']['month']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['month']):
?>
                                <option value="<?php echo $this->_tpl_vars['month']; ?>
" <?php if ($this->_tpl_vars['p']['birth_month'] == $this->_tpl_vars['month']): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['month']; ?>


</option>
                                <?php endforeach; endif; unset($_from); ?>
                            </select>
                            <select name="birth_day" id="birth_day"><!-- class="validate[required,custom[onlyNumber]">-->
                                <option value="00">日</option>
                                <?php $_from = $this->_tpl_vars['brithday']['day']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['day']):
?>
                                <option value="<?php echo $this->_tpl_vars['day']; ?>
" <?php if ($this->_tpl_vars['p']['birth_day'] == $this->_tpl_vars['day']): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['day']; ?>
</option>
                                <?php endforeach; endif; unset($_from); ?>
                            </select>
                      </td>
                    </tr>

                    <tr>
                        <td class="lside">お名前</td>
                        <td class="rside"><input type="text" value="<?php echo $this->_tpl_vars['p']['onamae']; ?>
" name="onamae" gtbfieldid="328" id="onamae" class="validate[required,length[3,15]]"></td>
                    </tr>
		   <!-- 地域追加ここから始まる-->
		   <tr> 
                        <td class="lside">地域</td>
                        <td class="rside">
			<select name="area" style="width:500" gtbfieldid="322" >
			    
			    <?php $_from = $this->_tpl_vars['a_key_value']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['area_ids'] => $this->_tpl_vars['area_names']):
?>
			    
			　　	
			　　	<option value="<?php echo $this->_tpl_vars['area_ids']; ?>
" <?php if ($this->_tpl_vars['p']['area'] == $this->_tpl_vars['area_ids']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['area_names']; ?>
</option>
			　　<?php endforeach; endif; unset($_from); ?> 
			</select></td>
                    </tr> 
		  <!-- 地域追加ここまで終わる-->
		  
		  <!-- 職業追加ここから始まる-->
		   <tr> 
                        <td class="lside">職業</td>
                        <td class="rside">
			<select name="job" style="width:500" gtbfieldid="322" >
			<?php $_from = $this->_tpl_vars['j_key_value']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['job_ids'] => $this->_tpl_vars['job_names']):
?>
			　　	<option value="<?php echo $this->_tpl_vars['job_ids']; ?>
" <?php if ($this->_tpl_vars['p']['job'] == $this->_tpl_vars['job_ids']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['job_names']; ?>
</option>
		　　    <?php endforeach; endif; unset($_from); ?>			    
			</select></td>
                    </tr> 		
		  <!-- 職業追加ここまで終わる-->
                    
                  <tr>
                        <td class="lside">画像認証</td>
                        <td class="rside"><input id='verify' type='text' name='verify' autocomplete=off  class="validate[required]"><br>
                            <img id="verifyImg" src="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/verify" align="middle">
                            <a href="javascript:freshVerify()">見づらい、別の画像を表示</a></td>
                    </tr>
                    <tr>
                        <td class="rside" colspan="2">
                            <input type="checkbox" name="mailmagazine" <?php if ($this->_tpl_vars['p']['mailmagazine'] == '1'): ?>checked <?php endif; ?> value="1">ﾒｰﾙﾏｶﾞｼﾞﾝを受け取る<br>
                            <input type="checkbox" name="agreement" <?php if ($this->_tpl_vars['p']['agreement'] == '1'): ?>checked <?php endif; ?> value="1" id ="agreement"><!-- class="validate[required]">-->規約に同意して登録する<br><br>
                            <input type="hidden" value="<?php echo $this->_tpl_vars['mode']; ?>
" name="mode">
                            <input type="hidden" value="ON" name="guid">
                            <input type="submit" value="確認ページへ" id="submit">
                        </td>
                    </tr>

                </table>

                <script type="text/javascript">
                    function freshVerify(){
                        var time = new Date().getTime();
			document.getElementById('verifyImg').src= "<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/verify?time="+time;
                    }
                </script>

            </form>
        </div>
    </div>


</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>