<?php /* Smarty version 2.6.26, created on 2011-01-15 18:39:50
         compiled from pc/password_forget.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/head.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/discr.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/top.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['JAVASCRIPT_DIR']; ?>
/validationEngine.js" ></script>
    <link href="<?php echo $this->_tpl_vars['CSS_PATH']; ?>
/validationEngine.css" media="screen" rel="stylesheet" type="text/css">
<div id="main_bg" class="pkg">


    <div id="gguide2">
        <h2>パスワードを忘れた場合</h2>
        <div><?php echo $this->_tpl_vars['message']; ?>
</div>
        <div><?php echo $this->_tpl_vars['SITE_NAME']; ?>
にご登録されたメールアドレスをご記入の上、送信ボタンを押してください。</div>
        <div>ご登録いただいているメールアドレスにパスワード再発行の手順をお送りします。</div>
        <span>メールアドレス:</span>
        <form action="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/member/forget" method="post" class="new_pass">
            <input type="hidden" name="mode" value="<?php echo $this->_tpl_vars['mode']; ?>
"/>
            <tr>
            <input type="text" name="email" size="30" id="email" class="validate[required,custom[email],length[6,30]]"/>
            </tr>
            <tr><input type="submit" value="送信"/></tr>
        </form>
    </div>





</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/footer.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pc/parts/copyright.html', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>