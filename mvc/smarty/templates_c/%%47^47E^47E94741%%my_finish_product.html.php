<?php /* Smarty version 2.6.26, created on 2010-12-17 21:38:59
         compiled from pc/parts/my_finish_product.html */ ?>
<h2><?php echo $this->_tpl_vars['member']->f_handle; ?>
さん　の落札済み一覧</h2>
    <!--終了商品一覧　start-->
<table  style="text-align: center">
    <tr>
        <td colspan="2" class="tdendlist">商品名</td>
        <td class="tdendlist">落札価格</td>
        <td class="tdendlist">ステイタス</td>
        <td class="tdendlist">お支払い</td>
        <td class="tdendlist">配送先住所</td>
        <td class="tdendlist">キャンセル</td>
    </tr>

    
<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['product']):
?>
<tr>
<td><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/product?pro_id=<?php echo $this->_tpl_vars['product']->id; ?>
"><img width="44" height="35" alt="落札商品" src="<?php echo $this->_tpl_vars['product']->image; ?>
"></a> </td>
<td nowrap><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/product?pro_id=<?php echo $this->_tpl_vars['product']->id; ?>
"><?php echo $this->_tpl_vars['product']->name; ?>
</a></td>
<td nowrap><?php echo $this->_tpl_vars['product']->price; ?>
円</td>
<td nowrap><?php echo $this->_tpl_vars['product']->status; ?>
</td>
<td nowrap>
    <?php if ($this->_tpl_vars['product']->f_status == 0): ?>
    <a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/mypay/pay?pro_id=<?php echo $this->_tpl_vars['product']->id; ?>
"><?php echo $this->_tpl_vars['product']->pay_status; ?>
</a>
    <?php else: ?>
    <span style="color: red"><?php echo $this->_tpl_vars['product']->pay_status; ?>
</span>
    <?php endif; ?>
</td>
<td nowrap>
    <?php if ($this->_tpl_vars['product']->flag == 1): ?>
        -
    <?php else: ?>
        <?php if ($this->_tpl_vars['product']->fk_address_id): ?>
            <!--住所が選択済みの場合、リングをつけない-->
            <span><?php echo $this->_tpl_vars['product']->address_status; ?>
</span>
        <?php else: ?>
            <!--住所が未選択の場合、リングをつける-->
            <a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/mypay/address?pro_id=<?php echo $this->_tpl_vars['product']->id; ?>
"><?php echo $this->_tpl_vars['product']->address_status; ?>
</a>
        <?php endif; ?>
    <?php endif; ?>

</td>
<td><a href="<?php echo $this->_tpl_vars['SERVER_PATH']; ?>
/mypay/cancel?pro_id=<?php echo $this->_tpl_vars['product']->id; ?>
"><?php echo $this->_tpl_vars['product']->cancel_status; ?>
</a></td>
</tr>
<tr><td colspan="7"><hr size="1" color="#dedede"></td></tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<div><?php echo $this->_tpl_vars['pageNumber']; ?>
</div>