<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultTemplateEngine
 *
 * @author mw
 */
class DefaultTemplateEngine implements TemplateEngine {
    //put your code here

    public function display($tpl,$array) {
        $tpl=Config::value("TEMPLATE_PATH")."/".$tpl;
        $stream=fopen($tpl, "r");
        $contents=fread($stream, filesize($tpl));
        fclose($stream);
        foreach ($array as $key=>$value){
            $contents=str_replace('{$'.$key."}", $value, $contents);
        }
        print ($contents);
    }
}
?>
