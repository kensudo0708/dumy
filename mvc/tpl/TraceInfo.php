<div id="traceinfo" style="background:white;margin:6px;font-size:14px;border:1px dashed silver;padding:8px">
<fieldset id="querybox" style="margin:5px;">
<legend style="color:gray;font-weight:bold">Trace情報</legend>
<div style="overflow:auto;height:300px;text-align:left;">
<?php
$trace=array();
$trace['Process Time']=microtime(true)- $GLOBALS['_START_TIME_'];
$trace['カレント頁']=$_SERVER['REQUEST_URI'];
$trace['HTTPメソッド']=$_SERVER['REQUEST_METHOD'];
$trace['プロトコル名']= $_SERVER['SERVER_PROTOCOL'];
$trace['時刻']=date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']);
$trace['エージェント']=$_SERVER['HTTP_USER_AGENT'];
$trace['セッションID']=session_id();
//$log= Log::$log;

$files =  get_included_files();
$trace['includeファイル']= count($files).str_replace("\n",'<br/>',substr(substr(print_r($files,true),7),0,-2));
foreach ($trace as $key=>$info){
    print($key.' : '.$info.'<br/>');
}
?>
</div>
</fieldset>
</div>