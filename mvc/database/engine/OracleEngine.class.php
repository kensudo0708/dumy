<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Oracleデータベースのアクセスエンジン
 *
 * @author mw
 */
class OracleEngine implements DataAccessEngine {
    //put your code here

    public function beginTransaction() {
    }
    public function closeConnection() {
    }
    public function commitTransaction() {
    }
    public function getArrayByQuery($sql, $params = false) {
    }
    public function getObjectByQuery($sql, $params = false) {
    }
    public function isOpen() {
    }
    public function openConnection() {
    }
    public function query($sql, $params = false) {
    }
    public function resultRows($sql, $params = false) {
    }
    public function rollbackTransaction() {
    }
    public function startup($host, $dbname, $user, $pwd, $charset) {
    }
    public function affectedRows($resource) {
    }
}
?>
