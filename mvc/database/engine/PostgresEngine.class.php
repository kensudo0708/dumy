<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import(FRAMEWORK_DIR.'.database.DataAccessEngine');
/**
 * Postgresデータベースのアクセスエンジン
 *
 * @author mw
 */
class PostgresEngine implements DataAccessEngine {

    private $connStrting="host=%s port=%s dbname=%s user=%s password=%s";

    private $conn;
    
    public function startup($host, $port, $dbname, $user, $pwd, $charset) {
        $this->connStrting=sprintf($this->connStrting,
                                  $host,$port,$dbname,$user,$pwd);
    }

    /**
     * DB接続オープンする
     * @return unknown_type
     */
    public function isOpen(){
        return !is_null($this->conn);
    }

    /**
     *DB接続をオープンする
     */
    public function openConnection(){
        $this->conn=pg_connect($this->connStrting);
//        mysql_select_db($this->dbname, $this->conn);
//        mysql_set_charset($this->charset,$this->conn);
    }

     /**
     * DB接続をクローズする
     */
    public function closeConnection(){
        if(pg_close($this->conn)){
            $this->conn=null;
            if($this->isTran)
                $this->isTran=false;
        }
    }

    /**
     *指定されたクエリを実行し、Objectの配列を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return Array() Object配列
     */
    public function getObjectByQuery($sql,$params=false){
        $objList=array();
        $query=$this->query($sql,$params);
        while($obj=pg_fetch_object($query))
            array_push($objList, $obj);

        return $objList;
    }

    /**
     *指定されたクエリを実行し、配列を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return Array()　実行結果
     */
    public function getArrayByQuery($sql,$params=false){
        $objList=array();
        $query=$this->query($sql,$params);
        while($obj=pg_fetch_array($query))
            array_push($objList, $obj);

        return $objList;
    }


    /**
     *指定されたクエリを実行し、成功した場合に TRUE 、エラー時に FALSE を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     */
    public function query($sql,$params=false){
        if ($params) {
            $sql = $this->paramChange($sql,$params);
            $result = pg_query_params ($this->conn,$sql,$params);
        }
        else
            $result = pg_query($this->conn,$sql);
        return $result;
    }

    /**
     *指定されたクエリを実行し、影響を受けた行数を返します
     * @param <String> $resource クエリ
     * @return <int> 影響を受けた行数
     */
    public function affectedRows($resource){
        return pg_affected_rows($resource);
    }

    /**
     *指定されたクエリを実行し、結果における行の数を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return <int> 行数
     */
    public function resultRows($sql,$params=false){
        return pg_num_rows($this->query($sql,$params));
    }

    private function paramChange($sql,$params){
        $sql=str_replace(DB::PARAM_STR, "%s", $sql);
        $p=array();
        for ($i=1;$i<=count($params);$i++){
            array_push( $p,"$".$i);
        }
        return vsprintf($sql, $p);
    }

    /**
     * トランザクションの開始
     */
    public function beginTransaction(){
//        if($this->isTran)
//            return true;
        $this->openConnection();
        if($this->query("BEGIN")){
            $this->isTran=true;
            return true;
        }
        return false;
    }

    /**
     * トランザクションのコミット
     */
    public function commitTransaction(){
        if(!$this->isTran)
                return;
        if($this->query("COMMIT")){
            $this->closeConnection();
            $this->isTran=false;
            return true;
        }
        return false;
    }

    /**
     *トランザクションのロールバック
     */
    public function rollbackTransaction(){
        if($this->query("ROLLBACK")){
            $this->closeConnection();
            $this->isTran=false;
            return true;
        }
        return false;
    }

    /**
     *データベース操作のエラーメッセージを返す
     * @param <type> $link DBの接続。
     * @return <type>
     */
    public function error() {
        return pg_last_error($this->conn);
    }


}
?>
