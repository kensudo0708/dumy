<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Sqlserverデータベースのアクセスエンジン
 *
 * @author mw
 */
class SqlserverEngine implements DataAccessEngine {
    //put your code here
    
    public function affectedRows($resource) {
    }
    public function beginTransaction() {
    }
    public function closeConnection() {
    }
    public function commitTransaction() {
    }
    public function getArrayByQuery($sql, $params = false) {
    }
    public function getObjectByQuery($sql, $params = false) {
    }
    public function isOpen() {
    }
    public function openConnection() {
    }
    public function query($sql, $params = false) {
    }
    public function resultRows($sql, $params = false) {
    }
    public function rollbackTransaction() {
    }
    public function startup($host, $port, $dbname, $user, $pwd, $charset) {
    }
}
?>
