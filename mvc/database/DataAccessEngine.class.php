<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *データアクセスエンジン
 * @author mw
 */
interface DataAccessEngine {

    /**
     *データアクセスエンジンを起動する
     * @param <type> $host
     * @param <type> $dbname
     * @param <type> $user
     * @param <type> $pwd
     * @param <type> $charset
     */
    public function startup($host,$port,$dbname,$user,$pwd,$charset);

    /**
     *接続しているかどうか、
     * @return <boolean>
     */
    public function isOpen();

    /**
     *DB接続をオープンする
     */
    public function openConnection();

    /**
     * DB接続をクローズする
     */
    public function closeConnection();

    /**
     *指定されたクエリを実行し、Objectの配列を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return Array() Object配列
     */
    public function getObjectByQuery($sql,$params=false);

    /**
     *指定されたクエリを実行し、配列を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return (Array)　実行結果
     */
    public function getArrayByQuery($sql,$params=false);

    /**
     *指定されたクエリを実行し、成功した場合に TRUE 、エラー時に FALSE を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return 実行結果
     */
    public function query($sql,$params=false);

    /**
     *指定されたクエリを実行し、影響を受けた行数を返します
     * @param <String> $resource クエリ
     * @return <int> 影響を受けた行数
     */
    public function affectedRows($resource);
    /**
     *指定されたクエリを実行し、結果における行の数を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return <int> 行数
     */
    public function resultRows($sql,$params=false);

    
    /**
     * トランザクションの開始
     */
    public function beginTransaction();

    /**
     * トランザクションのコミット
     */
    public function commitTransaction();

    /**
     *トランザクションのロールバック
     */
    public function rollbackTransaction();


    /**
     *データベース操作のエラーメッセージを返す
     */
    public function error();

    
}
?>
