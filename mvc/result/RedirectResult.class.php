<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of RedirectResult
 *Requestのリダイレクト
 * @author mw
 */
class RedirectResult implements ActionResult {
    //put your code here
    private $url;

    public function __construct($url) {
        $this->url=$url;
    }

    public function view() {
        //ログを記録しよう
        if(Config::value("LOG_RECORD"))
            Logger::record();
        if (!headers_sent()) {
//            header("refresh:{5};url={$this->url}");
            header('Location: '.$this->url);
            exit;
        }
        else
            exit("<meta http-equiv='refresh' content='10;URL=$this->url'/>");
    }
}
?>
