<?php
/**
 * テンプレートのActionResult
 *
 * @author mw
 */
class TemplateResult implements ActionResult {
    //put your code here
    private $tpl;
    private $params;

    public function __construct($tpl,$params) {
        $this->tpl=$tpl;
        $this->params=$params;
    }
    public function setTemplate($tpl){
        $this->tpl=$tpl;
    }

    public function setParams($params){
        $this->params=$params;
    }

    
    public function view() {
        $engine=$this->getTemplateEngine();

        $engine->display($this->tpl,$this->params);
    }

    /**
     *テンプレートエンジンのインスタンスを取得
     * @return <TemplateEngine> テンプレートエンジンのインスタンス
     */
    private function getTemplateEngine(){
        $tplClass=Config::value("TEMPLATE_ENGINE_CLASS");
        $engine= createInstance($tplClass);
        if($engine instanceof TemplateEngine)
            return $engine;
        else
            throw new AppException(CostKeys::TEMPLATE_NOT_IMPLEMENT);
    }
}
?>
