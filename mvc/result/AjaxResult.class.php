<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * AjaxのActionResult
 *
 * @author mw
 */
class AjaxResult implements ActionResult {
    //put your code here
    private $params;
    private $type;
    
    public function __construct($params,$type) {
        $this->params=$params;
        $this->type=$type;
    }

    public function view() {
        if(empty ($type)) $type=Config::value("DEFAULT_AJAX_RETURN");

        $type=strtolower($type);
        if($type=="xml"){
            header("Content-Type:text/xml; charset=utf-8");
            print(xml_encode($this->params));
        }
        elseif ($type=="json") {
            header("Content-Type:text/html; charset=utf-8");
            print(json_encode($this->params));
        }
    }

}
?>
