<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * 画像のActionResult
 *
 * @author mw
 */
class ImageResult implements ActionResult {
    //put your code here
    private $image;
    private $type;
    private $filename;

    /**
     *コンストラクタ
     * @param <resourc> $image 画像ソース
     * @param <String> $type 画像タイプ
     * @param <String> $filename ファイル名
     */
    public function __construct($image,$type,$filename='') {
        $this->image=$image;
        $this->type=$type;
        $this->filename=$filename;
    }
    
    public function view() {
        ob_clean();
        header("Content-type: image/".$this->type);
        $ImageFun='image'.$this->type;
        if(empty($this->filename)) {
            $ImageFun($this->image);
        }else {
            $ImageFun($this->image,$this->filename);
        }
        imagedestroy($this->image);
    }
}
?>
