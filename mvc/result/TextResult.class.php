<?php
/**
 * 直接テキスト出力のActionResult
 *
 * @author mw
 */
class TextResult implements ActionResult {
    //put your code here
    private $text;
    public function __construct($text) {
        $this->text=$text;
    }
    
    public function view() {
        print($this->text);
    }
}
?>
