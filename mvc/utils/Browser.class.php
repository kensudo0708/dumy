<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * browser 情報を取得
 *
 * @author mw
 */
final class Browser//class Browser
{
//    private $props    = array("version" => "0.0.0",
//                                "name" => "unknown",
//                                "agent" => "unknown") ;

    public function __Construct()
    {
        $browsers = array("firefox", "msie", "opera", "chrome", "safari",
                            "mozilla", "seamonkey",    "konqueror", "netscape",
                            "gecko", "navigator", "mosaic", "lynx", "amaya",
                            "omniweb", "avant", "camino", "flock", "aol");

        $this->agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        foreach($browsers as $browser)
        {
            if (preg_match("#($browser)[/ ]?([0-9.])#", $this->agent, $match))
            {
                $this->name = $match[1] ;
                $this->version = $match[2] ;
                break ;
            }
        }
    }

//    public function __Get($name)
//    {
//        if (!array_key_exists($name, $this->props))
//        {
//            die( "No such property or function $name)" );
//        }
//        return $this->props[$name] ;
//    }
//
//    public function __Set($name, $val)
//    {
//        if (!array_key_exists($name, $this->props))
//            die ;
//        $this->props[$name] = $val ;
//    }

}
?>
