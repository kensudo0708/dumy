<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of AgentInfo
 *
 * @author mw
 */
class AgentInfo {
    /**
     *AgentInfoのインスタンス
     * @var <AgentInfo>
     */
    private static $info;
    /**
     *agentのフォルダ名
     * @var <string>
     */
    private $agentDir;
    /**
     *$_SERVER['HTTP_USER_AGENT']
     * @var <string>　
     */
    private $userAgent;

    /**
     *携帯のキャリアの名称、（携帯のみ、使う）
     * @var <string>　'DoCoMo, KDDI, softbank,Vodafone'
     */
    private $phone;

    private $isPc=false;
    /**
     * コンストラクタ
     */
    private function __construct() {
        //printf($_SERVER['HTTP_USER_AGENT']);
        $this->userAgent=@$_SERVER['HTTP_USER_AGENT'];
        //モバイルからのアクセスかどうか、しらべる
        $mobile=explode( ",",Config::value("MOBILE_AGENT"));
        if(defined("MOBILE_AGENT_MODE") || $this->checkAgent($mobile)) {
            $this->agentDir=Config::value("MOBILE_DIR");
            import(FRAMEWORK_DIR.'.action.MobileAction');
            return;
        }

        //PCからのアクセスかどうか、しらべる
        $pc=explode( ",",Config::value("PC_AGENT"));
        if($this->checkAgent($pc)) {
            $this->agentDir=Config::value("PC_DIR");
            $this->isPc=true;
            import(FRAMEWORK_DIR.'.action.PcAction');
            return;
        }

        //その他の場合…
        //PCとして振り分ける
        $this->agentDir=Config::value("PC_DIR");
        $this->isPc=true;
        import(FRAMEWORK_DIR.'.action.PcAction');
    }

    /**
     *チェックHTTP_USER_AGENT
     * @param <array> $agents ユーザのagentの配列
     * @return <boolean>
     */
    private function checkAgent($agents) {
        foreach ($agents as $value) {
            if(stripos($this->userAgent, trim($value))!==FALSE){
                $this->phone=trim($value);
                return true;
            }
        }
        return false;
    }

    /**
     *AgentInfoのインスタンスを作成
     * @return <type>
     */
    public static function createAgentInfo() {
        if(!isset(self::$info))
            self::$info=new AgentInfo();
        return self::$info;
    }

    /**
     *AgentDirを取得
     * @return <string> dir名
     */
    public static function getAgentDir() {
        return self::$info->agentDir;
    }

    /**
     * PCからのアクセスかどうか、チェック
     * @return <boolean>
     */
    public static function isPc(){
        return self::$info->isPc;
    }

    /**
     *テンプレートPathを取得
     * @return <string> Path
     */
    public static function getTemplateDir() {
        return Config::value("TEMPLATE_PATH")."/".self::$info->agentDir."/";
    }

    /**
     *ThemeのPathを取得
     * @param <string> $themeName
     * @return <string>
     */
    public static function getStyleDir($themeName='') {
        if(empty ($themeName))
            return Config::value("STYLE_DIR")."/".self::$info->agentDir."/".Config::value("DEFAULT_THEME");
        else {
            $themeName = $themeName[0]=="/" ? substr($themeName, 1) : $themeName;
            return Config::value("STYLE_DIR")."/".self::$info->agentDir."/".$themeName;
        }
    }

    /**
     *$_SERVER['HTTP_USER_AGENT']の情報
     * @return <string>
     */
    public static function getUserAgent(){
        return self::$info->userAgent;
    }

    /**
     *brower情報を取得
     * @return <Browser>
     */
    public static function getBrowerInfo(){
        if(self::$info->agentDir==Config::value("PC_DIR")){
            import(FRAMEWORK_DIR.".utils.Browser");
            return new Browser;
        }
    }

    public static function getPhoneType(){
        return self::$info->phone;
    }

    /**
     *携帯の識別コードを取得する
     * @return <string>
     */
    public static function getPhoneCode(){

        if  (ereg("DoCoMo", self::$info->userAgent)) {
            //DoCoMoか？
            $ser_no = isset($_SERVER['HTTP_X_DCMGUID']) ? $_SERVER['HTTP_X_DCMGUID'] : "";
        }
        else if (ereg("KDDI", self::$info->userAgent)) {
            $ser_no = isset($_SERVER['HTTP_X_UP_SUBNO']) ? $_SERVER['HTTP_X_UP_SUBNO'] : "";
        }
        else if (ereg("J-PHONE", self::$info->userAgent)) {
            //J-PHONE
            preg_match("/^.+\/SN([0-9a-zA-Z]+).*$/", self::$info->userAgent, $match);
            if(count($match)>=2)
                $ser_no = $match[1];
            if (empty ($ser_no) || $ser_no == "SN") {
                $ser_no = "";
            }
        }
        else if (ereg("Vodafone", self::$info->userAgent)) {
            //SoftBank(Vodafone)??
            preg_match("/^.+\/SN([0-9a-zA-Z]+).*$/", self::$info->userAgent, $match);
            if(count($match)>=2)
                $ser_no = $match[1];
            if (empty ($ser_no) || $ser_no == "SN") {
                $ser_no = "";
            }
        }
        else if (ereg("SoftBank", self::$info->userAgent)) {
            //SoftBank??
            preg_match("/^.+\/SN([0-9a-zA-Z]+).*$/", self::$info->userAgent, $match);
            if(count($match)>=2)
                $ser_no = $match[1];
//                $ser_no = sprintf("SN%s", $match[1]);
            if (empty ($ser_no) || $ser_no == "SN") {
                $ser_no = "";
            }
        }
        else {
            $ser_no = "";
        }
        return $ser_no;
    }



    /**
     *
     */
    public static function getAccessTerminal()
    {
        $HTTP_USER_AGENT = @$_SERVER['HTTP_USER_AGENT'];
		// 端末判定とURL分岐
		if (ereg("DoCoMo", $HTTP_USER_AGENT))
		{
			return "IMODE";
		}
		else if (ereg("KDDI", $HTTP_USER_AGENT))
		{
			return "KDDI_AU";
		}
		else if (ereg("SoftBank", $HTTP_USER_AGENT))
		{
			return "SOFTBANK";
		}
		else if (ereg("Vodafone", $HTTP_USER_AGENT))
		{
			return "SOFTBANK";
		}
		else
		{
			//PC??
			return "PC";
		}
    }
}

?>