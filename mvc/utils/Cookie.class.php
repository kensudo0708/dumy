<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of Cookie
 *
 * @author mw
 */
final class Cookie {
    //put your code here

    /**
     *指定した名前のクッキーが存在するかどうか、調べる
     * @param <string> $name クッキーの名前。
     * @return <boolean> 存在した場合:true
     */
    public static function isExist($name) {
        return isset($_COOKIE[Config::value("COOKIE_PREFIX").$name]);
    }

    /**
     *指定した名前のクッキーを取得
     * @param <string> $name クッキーの名前。
     * @return <Object> $value クッキーの値
     */
    public static function get($name) {
        $value=$_COOKIE[Config::value("COOKIE_PREFIX").$name];
        $value=unserialize(base64_decode($value));
        return $value;
    }

    /**
     *クッキーを送信する
     * @param <string> $name クッキーの名前。
     * @param <Object> $value クッキーの値
     * @param <int> $expire クッキーの有効期限。
     * @param <string> $path クッキーのpath
     * @param <string> $domain クッキーが有効なドメイン
     */
    public static function set($name,$value,$expire='',$path='',$domain='') {
        if($expire=='') {
            $expire= Config::value("COOKIE_EXPIRE");
        }
        if(empty($path)) {
            $path= Config::value("COOKIE_PATH");
        }
        if(empty($domain)) {
            $domain=Config::value("COOKIE_DOMAIN");
        }
        $expire=!empty($expire) ? time()+$expire : 0;
        $value=base64_encode(serialize($value));
        setcookie(Config::value("COOKIE_PREFIX").$name, $value,$expire,$path,$domain);
        $_COOKIE[Config::value("COOKIE_PREFIX").$name]=value;
    }

    /**
     *指定された名のクッキーを削除する
     * @param <string> $name クッキー名
     */
    public static function delete($name) {
        Cookie::set($name,'',time()-3600);
        unset($_COOKIE[Config::value("COOKIE_PREFIX").$name]);
    }

    /**
     * クッキーをクリアする
     */
    public static function clear() {
        unset($_COOKIE);
    }
}
?>
