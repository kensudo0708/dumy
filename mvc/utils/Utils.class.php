<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
//import('classes.repository.Setting');
/**
 * Description of Utilsclass
 *
 * @author ark
 */
final class Utils {
    //put your code here

    /**
     *指定された引数をInt型として、値を取得、取得失敗の場合、デフォルドの値を使い
     * @param <type> $var
     * @param <type> $default
     * @return <type>
     */
    public static function getInt($var,$default=0) {
        if(is_numeric($var))
            return intval($var);
        return $default;
    }

    /**
     *
     * @param <type> $var
     * @param <type> $default
     */
    public static function getString($var,$default=0) {
        if(is_string($var))
            return $var;
        return $default;
    }

    /**
     *指定された文字のレングスが引数$maxLenを超えているかどうか、チェック
     * @param <string> $str
     * @param <int> $maxLen 
     * @param <string> $encoding 文字のエンコード
     * @return <boolean> 超えた場合、true
     */
    public static function moreMaxLen($str,$maxLen,$encoding="utf8"){
        $len=empty ($str) ? 0 : mb_strlen($str, $encoding);
        return $len > self::getInt($maxLen, 0) ? true : false;
    }

    /**
     *指定された文字のレングスが引数$minLenより小さいかどうか、チェック
     * @param <string> $str
     * @param <int> $minLen
     * @param <string> $encoding 文字のエンコード
     * @return <boolean> 小さい場合、true
     */
    public static function lessMinLen($str,$minLen,$encoding="utf8"){
        $len=empty ($str) ? 0 : mb_strlen($str, $encoding);
        return $len < self::getInt($minLen, 0) ? true : false;
    }

    /**
     *指定された文字のレングスのbetweenチェックする
     * @param <string> $str
     * @param <int> $minLen
     * @param <int> $maxLen
     * @param <string> $encoding
     * @return <boolean> 範囲外の場合 true
     */
    public static function betweenLen($str,$minLen,$maxLen,$encoding="utf8"){
        $len=empty ($str) ? 0 : mb_strlen($str, $encoding);
        return $len < self::getInt($minLen, 0) || $len > self::getInt($maxLen, 0) ? true : false;
    }


    /**
     *URLに指定されたパラメータをつける
     * @param <type> $url
     * @param <type> $param
     * @return <type>
     */
    public static function addUrlParam($url,$param){
        if(empty ($param))
            return $url;
        if(strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        return $url.$param;
    }

    /**
     *有効なemailかどうか、チェック
     * @param <type> $email
     * @return <boolean>
     */
    public static function isMailAddress($email) {
        return preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email);
    }

    public static function objectMerge($targetObj,$obj) {
        if(is_object($targetObj) && is_object($obj)) {
            $vars=get_object_vars($obj);
            foreach ($vars as $key => $value) {
//                if(!property_exists($targetObj,$key))
                $targetObj->$key=$obj->$key;
            }
        }
        return $targetObj;
    }

    /**
     *DateTimeをフォーマットし、string型として、戻す
     * @param <DataTime> $datetime
     * @return <string>
     */
    public static function getDateTimeString($datetime) {
        return strftime(DATE_TIME_FORMAT,strtotime($datetime));
    }

    /**
     *新しい時刻を取得する
     * @param <type> $strTime
     * @param <type> $dif   引く秒数
     * @return <type>
     */
    public static function getTimeString($secondTotal,$dif=0) {
        if($secondTotal==0)
            return "00:00:00";
        $secondTotal-=$dif;
        $hour=intval($secondTotal/3600);
        $minutes=intval(($secondTotal-$hour*3600)/60);
        $second=intval($secondTotal-$hour*3600-$minutes*60);
        return self::addZero($hour).":".self::addZero($minutes).":".self::addZero($second);
    }

    /**
     *メール送信
     * @param <string> $to
     * @param <string> $subject
     * @param <type> $body
     * @param <type> $headers
     * @param <type> $params
     * @return <type>
     */
    public static function sendMail($to,$subject,$body,$headers) {
        mb_language("japanese");
        mb_internal_encoding("UTF-8");
        return mb_send_mail($to,$subject,$body,$headers);
    }


    /**
     *テンプレートからbodyをロード、メール送信する
     * @param <string> $to
     * @param <string> $subject
     * @param <type> $tpl
     * @param <type> $headers
     * @param <type> $params
     * @return <type>
     */
    public static function sendTemplateMail($to,$subject,$tpl,$headers,$params="") {
        $message=self::loadTemplateContent($tpl,$params);
        return self::sendMail($to,$subject,$message,$headers);
    }


    /**
     *テンプレートの引数を引き換え、bodyを戻る
     * @param <type> $tplFile
     * @param <array> $params キーと値の配列
     * @return <string> body
     */
    public static function loadTemplateContent($tplFile,$params="") {
        $handle=fopen($tplFile, "r");
        $body=fread($handle, filesize($tplFile));
        fclose($handle);
        if(is_array($params)) {
            foreach ($params as $key=>$value) {
                if(stripos($body,$key)!==FALSE)
                    $body=str_ireplace("{%".$key."%}", $value, $body);
            }
        }
        return $body;
    }

    /**
     *ゼロを埋める処理
     * @param <type> $value
     * @return <type>
     */
    public static function addZero($value) {
        if(strlen($value)==1)
            return "0".$value;
        return $value;
    }


    /**
     *PC用のページNuber
     * @param <type> $curPage
     * @param <type> $countPage
     * @param <type> $url
     * @param <type> $extendPage
     * @param <type> $style
     * @param <type> $pagetag
     * @return <type>
     */
    public static function getPageNumbers($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        $nums=self::getPageNumber($curPage, $countPage,$url,$extendPage,$style,$pagetag);
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;
        $t="";
        if(empty($style))
            $style='class="pagenum_box"';
        $curStyle='class="pagenum_box2"';
        $indexStyle='class="pagenum_box3"';
        $firstStr="<<先頭";
        $prevStr="<前";
        $nextStr="次>";
        $lastStr="最後>>";

        $curPage=$curPage < 1 ? 1 : $curPage;
        $countPage=$countPage < 1 ? 1 : $countPage;
        $curPage=$curPage > $countPage ? $countPage :$curPage;

        $pageView="<span $indexStyle>".$curPage."/".$countPage."</span>";
        $mid=$nums;
        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $first="<span $style>".$curPage."/".$countPage."</span>";
        if($countPage > 1) {
            if($curPage != 1) {
                $firstUrl=$url.$pagetag."=1";
                $first="<span $style><a href=".$firstUrl.">$firstStr</a></span>";
                $prevUrl=$url.$pagetag."=".($curPage-1);
                $prev="<span $style><a href=".$prevUrl.">$prevStr</a></span>";
            }
            else {
                $first="<span $style >$firstStr</span>";
                $prevUrl=$url.$pagetag."=1";
                $prev="<span $style >$prevStr</span>";
            }
            $t=$first.$prev.$mid;
            if($curPage < $countPage) {
                $nextUrl=$url.$pagetag."=".($curPage+1);
                $next="<span $style ><a href=".$nextUrl.">$nextStr</a></span>";
                $lastUrl=$url.$pagetag."=".$countPage;
                $last="<span $style ><a href=".$lastUrl.">$lastStr</a></span>";
            }
            else {
                $nextUrl=$url.$pagetag."=".$countPage;
                $next="<span $style>$nextStr</span>";
                $last="<span $style >$lastStr</span>";
            }
            $t.=$next.$last.$pageView;
        }
        return $t;
    }

    /**
     *モバイル用のページnumber
     * @param <type> $curPage
     * @param <type> $countPage
     * @param <type> $url
     * @param <type> $extendPage
     * @param <type> $style
     * @param <type> $pagetag
     * @return <type>
     */
    public static function getPageNumbersI($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;
        $t="";
        if(empty($style))
            $style='class="pagenum_box"';
        $curStyle='class="pagenum_box2"';
        $indexStyle='class="pagenum_box3"';
        $firstStr="<<先頭";
        $prevStr="<前";
        $nextStr="次>";
        $lastStr="最後>>";

        $curPage=$curPage < 1 ? 1 : $curPage;
        $countPage=$countPage < 1 ? 1 : $countPage;
        $curPage=$curPage > $countPage ? $countPage :$curPage;

        $mid=$curPage."/".$countPage;
        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $first=$curPage."/".$countPage;
        if($countPage > 1) {
            if($curPage != 1) {
                $firstUrl=$url.$pagetag."=1";
                $first="<a href=".$firstUrl.">$firstStr</a>";
                $prevUrl=$url.$pagetag."=".($curPage-1);
                $prev="<a href=".$prevUrl.">$prevStr</a>";
            }
            else {
                $first=$firstStr;
                $prevUrl=$url.$pagetag."=1";
                $prev=$prevStr;
            }
            $t=$first." ".$prev." ".$mid;
            if($curPage < $countPage) {
                $nextUrl=$url.$pagetag."=".($curPage+1);
                $next="<a href=".$nextUrl.">$nextStr</a>";
                $lastUrl=$url.$pagetag."=".$countPage;
                $last="<a href=".$lastUrl.">$lastStr</a>";
            }
            else {
                $nextUrl=$url.$pagetag."=".$countPage;
                $next=$nextStr;
                $last=$lastStr;
            }
            $t.=" ".$next." ".$last;
        }
        return $t;
    }

    /**
     *管理画面用ページnumber
     * @param <type> $curPage
     * @param <type> $countPage
     * @param <type> $url
     * @param <type> $extendPage
     * @param <type> $style
     * @param <type> $pagetag
     * @return <type>
     */
    public static function getPageNumbersII($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        $nums=self::getPageNumber($curPage, $countPage,$url,$extendPage,$style,$pagetag);
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;
        $t="";
        if(empty($style))
            $style='class="mypagenum_box"';
        $curStyle='class="mypagenum_box2"';
        $indexStyle='class="mypagenum_box3"';
        $firstStr="<<先頭";
        $prevStr="<前";
        $nextStr="次>";
        $lastStr="最後>>";

        $curPage=$curPage < 1 ? 1 : $curPage;
        $countPage=$countPage < 1 ? 1 : $countPage;
        $curPage=$curPage > $countPage ? $countPage :$curPage;

        $pageView="<span $indexStyle>".$curPage."/".$countPage."</span>";
        $mid=$nums;
        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $first="<span $style>".$curPage."/".$countPage."</span>";
        if($countPage > 1) {
            if($curPage != 1) {
                $firstUrl=$url.$pagetag."=1";
                $first="<span $style><a href=".$firstUrl.">$firstStr</a></span>";
                $prevUrl=$url.$pagetag."=".($curPage-1);
                $prev="<span $style><a href=".$prevUrl.">$prevStr</a></span>";
            }
            else {
                $first="<span $style >$firstStr</span>";
                $prevUrl=$url.$pagetag."=1";
                $prev="<span $style >$prevStr</span>";
            }
            $t=$first.$prev.$mid;
            if($curPage < $countPage) {
                $nextUrl=$url.$pagetag."=".($curPage+1);
                $next="<span $style ><a href=".$nextUrl.">$nextStr</a></span>";
                $lastUrl=$url.$pagetag."=".$countPage;
                $last="<span $style ><a href=".$lastUrl.">$lastStr</a></span>";
            }
            else {
                $nextUrl=$url.$pagetag."=".$countPage;
                $next="<span $style>$nextStr</span>";
                $last="<span $style >$lastStr</span>";
            }
            
            $jump='<span class="mypagenum_box4"><form action="'.$url.'" method="get" style="display: inline"><input type="text" size="2" name="'.$pagetag.'"/><input type="submit" value="Jump"/></form></span>';
//            $t.=$next.$last.$pageView.$jump;
            $t.=$next.$last.$pageView;
/*
            $jump='<span class="mypagenum_box4"><input type="text" size="2" name="'.$pagetag.'"/><input type="submit" value="Jump"/></span>';
            $t.='<form action="'.$url.'" method="get">'.$next.$last.$pageView.$jump.'</form>';
*/
        }
        return $t;
    }

    public static function getPageNumber($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;

        if(empty($style)) {
            $style='class="pagenum_box"';
            $curStyle='class="pagenum_box2"';
//        $indexStyle='class="pagenum_box3"';
        }

        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $t1 = "<a $style href=".$url."&".$pagetag."=1";
        $t2 = "<a $style href=".$url."&".$pagetag."=".$countPage;
        $t1 .= ">&laquo;</a>";
        $t2 .= ">&raquo;</a>";
        if ($countPage < 1)
            $countPage = 1;
        if ($extendPage < 3)
            $extendPage = 2;

        if ($countPage > $extendPage) {
            if ($curPage - ($extendPage / 2) > 0) {
                if ($curPage + floor($extendPage / 2) < $countPage) {
                    $startPage = $curPage - floor($extendPage / 2);
                    $endPage = $startPage + $extendPage - 1;
                }
                else {
                    $endPage = $countPage;
                    $startPage = $endPage - $extendPage + 1;
                    $t2 = "";
                }
            }
            else {
                $endPage = $extendPage;
                $t1 = "";
            }
        }
        else {
            $startPage = 1;
            $endPage = $countPage;
            $t1 = "";
            $t2 = "";
        }

        $s="";// $t1;
        for ($i = $startPage;$i <= $endPage;$i++) {
            if ($i == $curPage) {
                $s.="<span $curStyle>";
                $s.=$i;
                $s.="</span>";
            }
            else {
                $s.="<span $style><a href=";
                $s.=$url;
                $s.=$pagetag;
                $s.="=";
                $s.=$i;

                $s.=">";
                $s.=$i;
                $s.="</a></span>";
            }
        }
//    $s.=$t2;
        return $s;
    }
}
?>
