<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of Image
 *
 * @author mw
 */
class Image {
    /**
     *画像認証を生成する
     * @param <String> $length 数字、また英字の数
     * @param <String> $mode 生成する乱数のタイプ
     * @param <type> $type 画像のタイプ(png,gif,jpegなど、詳細はPHPのドキュメントを参照
     * @param <int> $width 画像の幅
     * @param <int> $height 画像の高さ
     * @param <String> $verifyName sessionに保存するkey
     */
    public static function buildImageVerify($length=4,$mode='1',$type='png',$width=70,$height=30,$verifyName='verify',$ignore=false) {
        import(FRAMEWORK_DIR.'.utils.StringBuilder');
        $randomValue = StringBuilder::stringRandom($length,$mode);
        $_SESSION[$verifyName]= md5($ignore ? strtolower($randomValue) : $randomValue);
        $width = ($length*10+10)>$width?$length*10+10:$width;
        $im = @imagecreate($width,$height);
        $r = Array(225,255,255,223);
        $g = Array(225,236,237,255);
        $b = Array(225,236,166,125);
        $key = mt_rand(0,3);

        $backColor = imagecolorallocate($im, $r[$key],$g[$key],$b[$key]);
        $borderColor = imagecolorallocate($im, 100, 100, 100);
        $pointColor = imagecolorallocate($im,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));

        @imagefilledrectangle($im, 0, 0, $width - 1, $height - 1, $backColor);
        @imagerectangle($im, 0, 0, $width-1, $height-1, $borderColor);
        $stringColor = imagecolorallocate($im,mt_rand(0,200),mt_rand(0,120),mt_rand(0,120));

        for($i=0;$i<10;$i++) {
            $fontcolor=imagecolorallocate($im,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
            imagearc($im,mt_rand(-10,$width),mt_rand(-10,$height),mt_rand(30,300),mt_rand(20,200),55,44,$fontcolor);
        }
//        for($i=0;$i<25;$i++) {
//            $fontcolor=imagecolorallocate($im,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
//            imagesetpixel($im,mt_rand(0,$width),mt_rand(0,$height),$pointColor);
//        }
        for($i=0;$i<$length;$i++)
            imagestring($im,5,$i*15+12,mt_rand(1,8),$randomValue{$i}, $stringColor);
        
        return $im;
    }

    /**
     *Imageを出力処理
     * @param <resourc> $im 画像リソース
     * @param <String> $type 画像タイプ
     */
    public static function output($im,$type='png') {
        header("Content-type: image/".$type);
        $ImageFun='image'.$type;
        $ImageFun($im);
        imagedestroy($im);
    }

    /**
     *指定された画像の情報を取得（PHPドキュメントgetimagesizeを参照）
     * @param <resourc> $im 画像リソース
     * @return <array> 画像情報の配列
     */
    public static function getImageInfo($img) {
        $imageInfo = getimagesize($img);
        if( $imageInfo!== false) {
            if ( function_exists('image_type_to_extension') ) {
                $imageType = strtolower(substr(image_type_to_extension($imageInfo[2]),1));
            } else {
                $imageType = 'jpeg';
                $tmp = pathinfo($img);
                if ( $tmp['extension'] ) {
                    $tmp['extension'] = strtolower( $tmp['extension'] );
                    switch ( $tmp['extension'] ) {
                    case 'png':
                        $imageType = 'png';
                        break;
                    case 'jpeg':
                    case 'jpg':
                    default:
                        $imageType = 'jpeg';
                        break;
                    }
                }
            }
            $imageSize = filesize($img);
            $info = array(
                    "width"=>$imageInfo[0],
                    "height"=>$imageInfo[1],
                    "type"=>$imageType,
                    "size"=>$imageSize,
                    "mime"=>$imageInfo['mime']
            );
            return $info;
        }else {
            return false;
        }
    }

    /**
     *画像のバランスを保つ、縮小した画像が作成する
     * @param <type> $image
     * @param <type> $thumbname
     * @param <type> $type
     * @param <type> $maxWidth
     * @param <type> $maxHeight
     * @param <type> $interlace
     * @return <type>
     */
    public static function thumb($image,$thumbname,$type='',$maxWidth=200,$maxHeight=50,$interlace=true) {
        $info  = Image::getImageInfo($image);
        if($info !== false) {
            $srcWidth  = $info['width'];
            $srcHeight = $info['height'];
            $type = empty($type)?$info['type']:$type;
            $type = strtolower($type);
            $interlace  =  $interlace? 1:0;

            $scale = min($maxWidth/$srcWidth, $maxHeight/$srcHeight);
            if($scale>=1) {
                $width   =  $srcWidth;
                $height  =  $srcHeight;
            }else {
                $width  = (int)($srcWidth*$scale);
                $height = (int)($srcHeight*$scale);
            }

            //元のImageロード
            $createFun = 'ImageCreateFrom'.($type=='jpg'?'jpeg':$type);
            $srcImg     = $createFun($image);

            //画像タイプより、処理する
            if($type!='gif' && function_exists('imagecreatetruecolor'))
                $thumbImg = imagecreatetruecolor($width, $height);
            else
                $thumbImg = imagecreate($width, $height);

            if(function_exists("ImageCopyResampled"))
                imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth,$srcHeight);
            else
                imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height,  $srcWidth,$srcHeight);
            if('gif'==$type || 'png'==$type) {
                $background_color=imagecolorallocate($thumbImg,  0,255,0);
                imagecolortransparent($thumbImg,$background_color);
            }

            //jpg,jpegの画像がimageinterlaceより処理されたら、一部の携帯端末が表示されないことがありますので、無効にする
//            if('jpg'==$type || 'jpeg'==$type)
//                imageinterlace($thumbImg,$interlace);

            $imageFun = 'image'.($type=='jpg'?'jpeg':$type);
            $imageFun($thumbImg,$thumbname);
            imagedestroy($thumbImg);
            imagedestroy($srcImg);
            return $thumbname;
        }
        return false;
    }
}
?>
