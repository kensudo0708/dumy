<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of RandomString
 *
 * @author mw
 */
class StringBuilder {
    //put your code here

    /**
     *乱数のStringを生成する
     * @param <int> $len 乱数の数
     * @param <String> $type 生成する乱数のタイプ
     * @param <String> $addChars タイプ以外、他に追加したいストリング
     * @return <type>
     */
    public static function stringRandom($len=6,$type='0',$addChars='') {
        $str ='';
        switch($type) {
            case 0:
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.$addChars;
                break;
            case 1:
                $chars= str_repeat('0123456789',3);
                break;
            case 2:
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ'.$addChars;
                break;
            case 3:
                $chars='abcdefghijklmnopqrstuvwxyz'.$addChars;
                break;
            case 4:
                $chars = "あいうえおかきくけこ学校".$addChars;
                break;
            default :
                $chars='ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789'.$addChars;
                break;
        }
        if($len>10 ) {
            $chars= $type==1? str_repeat($chars,$len) : str_repeat($chars,5);
        }

        $chars = str_shuffle($chars);
        $str= substr($chars,0,$len);
        return $str;
    }


    /**
     *一意な ID を生成する
     * @return <string> 一意な識別子を文字列で返します。
     */
    public static function uuid() {
        $charid = md5(uniqid(mt_rand(), true));
        $uuid = substr($charid, 0, 8)
                .substr($charid, 8, 4)
                .substr($charid,12, 4)
                .substr($charid,16, 4)
                .substr($charid,20,12);
        return $uuid;
    }

    public static function htmlDecode(&$p){
        if(!empty($p)){
            if(is_array($p)){
                foreach ($p as $v) 
                    $v = htmlspecialchars($v);
            }
            else
                $p= htmlspecialchars($p);
        }
        return $p;
    }
}
?>
