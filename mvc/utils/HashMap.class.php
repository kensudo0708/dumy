<?php
/**
 * HashMap
 */
class HashMap {
    /**
     * @var array
     */
    private $_values = array();
    /**
     * マッピングされないデフォルトの値
     */
    const DEFAULT_VALUE=null;
    /**
     *指定されたarrayでHashMapを構築する
     * @access	public
     * @param	$values array
     */
    public function HashMap($values = array()) {
        if (!empty($values))
            $this->_values = $values;
    }
    /**
     *すべてのマッピングをマップから削除する
     * @access	public
     */
    public function clear() {
        $this->_values = array();
    }
    /**
     * マップが指定されたキーのマッピングを保持する場合に true を返す
     * @access	public
     * @param	$key　マップにあるかどうかが判定されるキー
     * @return	boolean
     */
    public function containsKey($key) {
        return array_key_exists($key, $this->_values);
    }

    /**
     * マップが1つまたは複数のキーと指定された値をマッピングしている場合に true を返す
     * @access	public
     * @param	$value　マップにあるかどうかを判定される値
     * @return	boolean
     */
    public function containsValue($value) {
        return in_array($value, $this->_values);
    }
    /**
     * マップが指定されたマッピングを保持する場合に true を返す
     * @access	public
     * @param	$key マップにあるかどうかが判定されるキー
     * @param	$value マップにあるかどうかを判定される値
     * @return	boolean
     */
    public function contains($key, $value) {
        if ($this->containsKey($key))
            return ($this->get($key) == $value);
        return false;
    }
    /**
     * 指定されたキーがマップされている値を返す
     * @access	public
     * @param	$key　マッピングのキー
     * @return	マップにあるかどうかを判定される値
     */
    public function get($key) {
        if ($this->containsKey($key)) {
            return $this->_values[$key];
        } else
            return HashMap::DEFAULT_VALUE;
    }
    /**
     * マップがキーと値のマッピングを保持しない場合に trueを返す
     * @access	public
     * @return	boolean
     */
    public function isEmpty() {
        return empty($this->_values);
    }
    /**
     * このマップに含まれる全てのキーを返す
     * @access	public
     * @return	array
     */
    public function keySet() {
        return array_keys($this->_values);
    }
    /**
     * 指定された値と指定されたキーをこのマップに関連付け
     * @access	public
     * @param	$key マッピングのキー
     * @param	$value マッピングの値
     * @return	key に以前に関連付けられていた値、key のマッピングが存在しなかった場合は null
     */
    public function put($key, $value) {
        $previous = $this->get($key);
        $this->_values[$key] =& $value;
        return $previous;
    }
    /**
     * 指定されたarrayからすべてのマッピングをマップにコピーする
     * @access	public
     * @param	$values array
     */
    public function putAll($values) {
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                $this->put($key, $value);
            }
        }
    }
    /**
     * 指定されたキーのマッピングがあればマップから削除する
     * @access	public
     * @param	$key マッピングのキー
     */
    public function remove($key) {
        $value = $this->get($key);
        if (!is_null($value))
            unset($this->_values[$key]);
        return $value;
    }
    /**
     * マップ内のキー値マッピングの数を返す
     * @access	public
     * @return	integer
     */
    public function size() {
        return count($this->_values);
    }
    /**
     * マップ内の全てのマッピングの値を返す
     * @access	public
     * @return	array
     */
    public function values() {
        return array_values($this->_values);
    }

    /**
     *マップ内の全てのマッピングを返す
     * @return array
     */
    public function getArray() {
        return $this->_values;
    }
}
?>
