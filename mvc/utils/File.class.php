<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of File
 *
 * @author ark
 */
final class File {
    public $allowTypes;
    public $allowExts;
    public $maxSize;
    public $savePath;
    public $uploadFileInfo;
    public $uuidFileName=false;
    public $uploadReplace = false;


    public $thumbRemoveOrigin = false;
    public $thumb=false;
    public $thumbMaxWidth;
    public $thumbMaxHeight;
    public $thumbPrefix='thumb_';
    public $thumbSuffix='';
    public $thumbPath='';
    public $thumbFile='';

    /**
     * コンストラクタ
     */
    public function __construct() {
        //配置ファイルの設定を読み
//        $this->allowTypes=explode(',',strtolower(Config::value('ALLOW_TYPE')));
//        $this->allowExts=explode(',',strtolower(Config::value('ALLOW_EXTS')));
//        $this->maxSize=Config::value('MAX_SIZE')*1024;
//        $this->savePath=Config::value('SAVE_PATH');
                $this->allowTypes=explode(',','image/bmp,image/gif,image/jpeg,image/png');
                $this->allowExts=explode(',','jpg,jpeg,gif,png,bmp');
                $this->maxSize=10240*1024;
                $this->savePath="/home/auction/images";
    }

    /**
     *ファイルアップロード処理
     * @param <String> $savePath アップロード先（デフォルド:init.phpの設定を参照)
     * @return <array> アップロードしたファイルの情報
     */
    public function upload($savePath=null) {
        if(!$this->savePathCheck($savePath))
            return false;

        if(substr($savePath,-1)!="/")
            $savePath=$savePath."/";

        $fileInfo=array();
        $files=$this->getFiles($_FILES);
        foreach($files as $key => $file) {
            if(!empty($file['name'])) {
                $file['key']= $key;
                $file['extension']=$this->getExt($file['name']);
                $file['savepath']=$savePath;
                $file['savename']=$this->getSaveName($file['name'],$file['extension']);

                if(!$this->check($file))
                    return false;
                if(!$this->save($file))
                    return false;

                $fileInfo[] = $file;
            }
//G.Chin 2010-08-17 add sta
			else
			{
                $fileInfo[] = "";
			}
//G.Chin 2010-08-17 add end
        }
        $this->uploadFileInfo = $fileInfo;
        return $fileInfo;
    }

    /**
     *画像情報取得
     */
	public function getImageInfo($img) {
	    $imageInfo = getimagesize($img);
	    if( $imageInfo!== false) {
                if ( function_exists('image_type_to_extension') ) {
                    $imageType = strtolower(substr(image_type_to_extension($imageInfo[2]),1));
                } else {
                    $imageType = 'jpeg';
                    $tmp = pathinfo($img);
                    if ( $tmp['extension'] ) {
                        $tmp['extension'] = strtolower( $tmp['extension'] );
                        switch ( $tmp['extension'] ) {
                        case 'png':
                            $imageType = 'png';
                            break;
                        case 'jpeg':
                        case 'jpg':
                        default:
                            $imageType = 'jpeg';
                            break;
                        }
                    }
                }
	        $imageSize = filesize($img);
	        $info = array(
	                "width"=>$imageInfo[0],
	                "height"=>$imageInfo[1],
	                "type"=>$imageType,
	                "size"=>$imageSize,
	                "mime"=>$imageInfo['mime']
	        );
	        return $info;
	    }else {
	        return false;
	    }
	}

    /**
     *画像サイズ変更処理
     */
	public function FixImage($image,$thumbname,$type='',$maxWidth=200,$maxHeight=50,$interlace=true) {
	    $info  = $this->getImageInfo($image);
	    if($info !== false) {
	        $srcWidth  = $info['width'];
	        $srcHeight = $info['height'];
	        $type = empty($type)?$info['type']:$type;
	        $type = strtolower($type);
	        $interlace  =  $interlace? 1:0;

	        $scale = min($maxWidth/$srcWidth, $maxHeight/$srcHeight);
	        if($scale>=1) {
	            $width   =  $srcWidth;
	            $height  =  $srcHeight;
	        }else {
	            $width  = (int)($srcWidth*$scale);
	            $height = (int)($srcHeight*$scale);
	        }

	        //元のImageロード
	        $createFun = 'ImageCreateFrom'.($type=='jpg'?'jpeg':$type);
	        $srcImg     = $createFun($image);

	        //画像タイプより、処理する
	        if($type!='gif' && function_exists('imagecreatetruecolor'))
	            $thumbImg = imagecreatetruecolor($width, $height);
	        else
	            $thumbImg = imagecreate($width, $height);

	        if(function_exists("ImageCopyResampled"))
	            imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth,$srcHeight);
	        else
	            imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height,  $srcWidth,$srcHeight);
	        if('gif'==$type || 'png'==$type) {
	            $background_color=imagecolorallocate($thumbImg,  0,255,0);
	            imagecolortransparent($thumbImg,$background_color);
	        }

	        //jpg,jpegの画像がimageinterlaceより処理されたら、一部の携帯端末が表示されないことがありますので、無効にする
//	            if('jpg'==$type || 'jpeg'==$type)
//	                imageinterlace($thumbImg,$interlace);

	        $imageFun = 'image'.($type=='jpg'?'jpeg':$type);
	        $imageFun($thumbImg,$thumbname);
	        imagedestroy($thumbImg);
	        imagedestroy($srcImg);
	        return $thumbname;
	    }
	    return false;
	}
    /**
     *
     * @param <type> $image 入力画像のパス
     * @param <type> $thumbname　出力画像のパス
     * @param <type> $type　入力画像の形式
     * @param <type> $maxWidth
     * @param <type> $maxHeight
     * @param <type> $interlace
     * @param <type> $to_type　出力画像の形式。未指定時は入力画像の形式。例）'jpeg'
     * @return <type>
     */
	public function FixImage2($image,$thumbname,$type='',$maxWidth=200,$maxHeight=50,$interlace=true,$to_type='') {
	    $info  = $this->getImageInfo($image);
	    if($info !== false) {
	        $srcWidth  = $info['width'];
	        $srcHeight = $info['height'];
	        $type = empty($type)?$info['type']:$type;
	        $type = strtolower($type);
	        $interlace  =  $interlace? 1:0;
            if (!$to_type) {
                $to_type = $type;
            }

	        $scale = min($maxWidth/$srcWidth, $maxHeight/$srcHeight);
	        if($scale>=1) {
	            $width   =  $srcWidth;
	            $height  =  $srcHeight;
	        }else {
	            $width  = (int)($srcWidth*$scale);
	            $height = (int)($srcHeight*$scale);
	        }

	        //元のImageロード
	        $createFun = 'ImageCreateFrom'.($type=='jpg'?'jpeg':$type);
	        $srcImg     = $createFun($image);

	        //画像タイプより、処理する
	        if($to_type!='gif' && function_exists('imagecreatetruecolor')) {
	            $thumbImg = imagecreatetruecolor($width, $height);
            } else {
	            $thumbImg = imagecreate($width, $height);
            }
	        if(function_exists("ImageCopyResampled")) {
	            imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth,$srcHeight);
            } else {
	            imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height,  $srcWidth,$srcHeight);
            }
	        if('gif'==$type || 'png'==$type) {
	            $background_color=imagecolorallocate($thumbImg,  0,255,0);
	            imagecolortransparent($thumbImg,$background_color);
	        }

	        $imageFun = 'image'.($to_type=='jpg'?'jpeg':$to_type);
	        $imageFun($thumbImg,$thumbname);
	        imagedestroy($thumbImg);
	        imagedestroy($srcImg);
	        return $thumbname;
	    }
	    return false;
	}

    /**
     *画像が保存する
     * @param <type> $file ファイルのリソース
     * @return <boolean>
     */
    private function save($file) {
        $dir=$file['savepath'];
        if(!file_exists($dir))
            mkdir($dir);
        $filename = $dir."/".$file['savename'];

        if(!$this->uploadReplace && is_file($filename)) {
            return false;
        }
        if(!move_uploaded_file($file['tmp_name'],$filename)) {
            return false;
        }
        if($this->thumb) {
//            import(FRAMEWORK_DIR.'.utils.Image');
            $image=Image::getImageInfo($filename);
            if($image) {
                $thumbWidth= explode(',',$this->thumbMaxWidth);
                $thumbHeight=explode(',',$this->thumbMaxHeight);
                $thumbPrefix=explode(',',$this->thumbPrefix);
                $thumbSuffix =explode(',',$this->thumbSuffix);
                $thumbFile=explode(',',$this->thumbFile);
                $thumbPath=explode(',',$this->thumbPath);
                for($i=0,$len=count($thumbWidth); $i<$len; $i++) {
                    $thumbPrefix[$i]=isset($thumbPrefix[$i]) ? $thumbPrefix[$i] : "";
                    $thumbSuffix[$i]=isset($thumbSuffix[$i]) ? $thumbSuffix[$i] : "";
                    $thumbPath[$i]= isset($thumbPath[$i]) && strlen($thumbPath[$i])>0 ? $file['savepath'].$thumbPath[$i]."/" : $file['savepath'];
                    $thumbname=	$thumbPath[$i].$thumbPrefix[$i].substr($file['savename'],0,strrpos($file['savename'], '.')).$thumbSuffix[$i].'.'.$file['extension'];
                    Image::thumb($filename,$thumbname,'',$thumbWidth[$i],$thumbHeight[$i],true);
                }
                if($this->thumbRemoveOrigin) {
                    //モードの画像を削除する
                    unlink($filename);
                }
            }
        }
        return true;
    }


    private function savePathCheck(&$savePath) {
        if(empty($savePath))
            $savePath=$this->savePath;

        if(empty($savePath))
        //保存先が指定されない
            throw new Exception(ConstKeys::NOT_UPLOAD_PATH);

        if(!is_dir($savePath)) {
            //存在しない場合、作成する
            if(!mkdir($savePath))
            //作成失敗の場合
                throw new Exception(sprintf(ConstKeys::NOT_UPLOAD_PATH,$savePath));
        }
        else {
            //存在する場合
            if(!is_writeable($savePath))
                throw new Exception(sprintf(ConstKeys::NOT_WRITEABLE,$savePath));
        }
        return true;
    }


    private function getFiles($files) {
        $fileArray = array();
        foreach ($files as $file) {
            if(is_array($file['name'])) {
                $keys = array_keys($file);
                $count=count($file['name']);
                for ($i=0; $i<$count; $i++) {
                    foreach ($keys as $key)
                        $fileArray[$i][$key] = $file[$key][$i];
                }
            }else
                $fileArray=$files;
            break;
        }
        return $fileArray;
    }


    private function check($file) {
        //サイズチェック
        if(!$this->checkSize($file['size'])) {
            return false;
        }
        //タイプチェック
//        if(!$this->checkType($file['type'])) {
//            return false;
//        }

        //拡張子チェック
        if(!$this->checkExt($file['extension'])) {
            return false;
        }

        //アップロードされたファイルかどうか、チェック
        if(!$this->checkIsUpload($file['tmp_name'])) {
            return false;
        }
        return true;
    }
    /**
     *ファイルの拡張子を取得
     * @param <String> $filename ファイル名
     * @return <String> 拡張子
     */
    private function getExt($filename) {
        $pathinfo=pathinfo($filename);
        return $pathinfo['extension'];
    }

    /**
     *ファイルのサイズチェック
     * @param <int> $size
     * @return <boolean>
     */
    private function checkSize($size) {
        return !($size > $this->maxSize) || (-1 == $this->maxSize);
    }

    /**
     *ファイルの拡張子をチェック
     * @param <type> $ext
     * @return <type>
     */
    private function checkExt($ext) {
        if(!empty($this->allowExts)) {
            return in_array(strtolower($ext),$this->allowExts,true);
        }
        return true;
    }

    /**
     * HTTP POST でアップロードされたファイルかどうか、チェック
     */
    private function checkIsUpload($filename) {
        return is_uploaded_file($filename);
    }

    /**
     *画像のタイプをチェック
     * @param <type> $type
     * @return <type>
     */
    private function checkType($type) {
        if(!empty($this->allowTypes)) {
            return in_array(strtolower($type),$this->allowTypes);
        }
        return true;
    }

    /**
     *ファイル保存する時、つける名前を取得
     * @param <type> $filename
     * @return <type>
     */
    private function getSaveName($filename,$ext) {
        if($this->uuidFileName) {
//            import(FRAMEWORK_DIR.'.utils.StringBuilder');
            return StringBuilder::uuid().".$ext";
        }
        return $filename;
    }

    
}
?>
