@charset "utf-8";


#mp_main_box{
width:898px;
padding-left:13px;
padding-right:10px;
}

#mp_main_box_l{
width:206px;
float:left;
}

#mp_main_box_l h3{
margin:0px;
padding:0px;
margin-top:10px;
}



.mp_smenu_foot{
width:206px;
height:23px;
background:url(../images/mp_smenu_foot.jpg);
clear:both;
}

#mp_main_box_r{
width:681px;
float:right;
}

.mp_cate{
width:681px;
background:url(../images/mp_cate1_bg.gif);
}

.mp_cate img{
display:block;
margin:auto;
}

.mp_cate strong{
color:#666666;
font-size:22px;
padding-left:40px;
}

.mp_cate strong span{
color:#ff3366;
font-size:50px;
}

.mp_cate_foot{
width:681px;
height:24px;
background:url(../images/mp_cate_foot.jpg) no-repeat;
clear:both;
}

#mp_tend_box{
margin:auto;
width:617px;
border-right-width: 3px;
border-bottom-width: 3px;
border-left-width: 3px;
border-top-style: none;
border-right-style: solid;
border-bottom-style: solid;
border-left-style: solid;
border-right-color: #FF5790;
border-bottom-color: #FF5790;
border-left-color: #FF5790;
text-align:center;
}

.mp_tend_box_s{
width:617px;
line-height:20px;
height:20px;
font-size:12px;
color:#666666;
}

.mp_tend_box_1{
width:120px;
float:left;
padding-right:5px;
}


.mp_tend_box_2{
width:275px;
float:left;
padding-right:5px;

}

.mp_tend_box_2 div{
font-size:18px;
}

.mp_tend_box_2 span{
color:#FF6600;
}

.mp_tend_box_4 {
width:90px;
float:left;
padding-right:5px;
}

.mp_tend_box_4 p{
font-size:18px;
height:90px;
line-height:90px;
margin:0px;
padding:0px;
}

.mp_tend_box_5{
width:112px;
float:left;
padding-right:5px;
}

.mp_tend_box_5 img{
margin-top:22px;

}


.mp_tend_box_w{
width:617px;
height:90px;
padding-top:10px;
font-size:12px;
font-weight:bold;
color:#ff3366;
}

.mp_tend_box_y{
width:617px;
height:90px;
background:#ffffcc;
padding-top:10px;
font-size:12px;
font-weight:bold;
color:#ff3366;
}

.mp_r_text{
width:617px;
text-align:right;
padding-top:10px;
margin:auto;
}

#mp_bnr_box{
padding-left:5px;
height:109px;
}

.mp_bnr_box img{
margin-top:10px;
display:block;
float:left;
}

.mp_yazirusi{
width:343px;
height:47px;
margin:auto;
background:url(../images/mp_yazirusi.jpg) no-repeat;
padding-top:10px;
padding-bottom:10px;
background-position:0px 10px;
clear:both;
}

#mp_itemcate{
width:654px;
height:70px;
padding-left:11px;
padding-right:11px;
}

#mp_itemcate img{
margin:0px;
padding-left:11px;
padding-right:11px;
float:left;
display:block;
}
