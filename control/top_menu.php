<script type="text/javascript">
<!--
function stat(){
            parent.main_r.location.href ="top_stat.php"
}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/09/03												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	session_start();
	$sid = $_SESSION["staff_id"];
	if($sid == "")
	{
		$dsp_tbl  = "";
		$dsp_tbl .= "<body>\n";
		$dsp_tbl .= "<P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>\n";
		$dsp_tbl .= "</body>\n";
		
		//管理画面入力ページ表示関数
		PrintAdminInputPage($dsp_tbl);
	}
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class = 'main_l'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center' colspan=2>\n";
	$dsp_tbl .= "リアルタイム集計用\n";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='button' style='background-color:#FFFBEC; color:#C84B00; width:80px; border-color:#FFFAFA;' value='集計実施' onClick=\"stat()\">\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>