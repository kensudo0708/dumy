<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION['staff_id'];
	
	$pid = $_REQUEST["pid"];
	$page = $_REQUEST["page"];
	$type= $_REQUEST["type"];
	$offset=50;
	$limit=$offset*($page-1);
	
	//商品情報取得関数
	GetTProductsInfo($pid,$tpm_f_products_name,$tpm_fk_item_category_id,$tpm_f_photo1,$tpm_f_photo2,$tpm_f_photo3,$tpm_f_photo4,$tpm_f_photo5,$tpm_f_photo1mb,$tpm_f_photo2mb,$tpm_f_photo3mb,$tpm_f_photo4mb,$tpm_f_photo5mb,$tpm_f_top_description_pc,$tpm_f_main_description_pc,$tpm_f_top_description_mb,$tpm_f_main_description_mb,$tpm_f_start_price,$tpm_f_min_price,$tpm_f_r_min_price,$tpm_f_r_max_price,$tpm_f_market_price,$tpm_fk_auction_type_id,$tpm_f_delflg,$tpm_f_memo,$tpm_f_target_hard,$tpm_f_recmmend,$tpm_f_regdate,$tp_f_bit_buy_count,$tp_f_bit_free_count,$tp_f_now_price,$tp_f_last_bidder,$tp_f_last_bidder_handle,$tp_f_auction_time,$tp_t_r_status,$tp_f_status,$tp_f_tm_stamp,$tp_f_start_time,$tpt_f_plus_coins,$tpt_f_address_flag,$tmp_fk_products_template_id,$tmp_initial_stop_time);
	
	//商品入札履歴一覧検索関数
	ProductsBitList($pid,$limit,$offset,$f_tm_stamp,$f_price,$fk_member_id,$f_handle,$f_bid_type,$f_coin_type,$data_cnt,$r_count,$p_count_free,$p_count_buy,$p_count_a_free,$p_count_a_buy,$all_count);
	
	$url = SITE_URL."images/";
	
	$disp_tab .= "<fieldset style=\"width:800\">\n";
	$disp_tab .= "<legend>■自動入札設定一覧</legend>\n";

	$disp_tab .= "<table class='list'>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<td rowspan = '3'><img src ='$url$tpm_f_photo1' style = 'height:100px; width:100px;'></td>\n";
	$disp_tab .= "<th style = 'height:30px; width:100px;'>商品ID</td>\n";
	$disp_tab .= "<td style = 'width:200px;'>$pid</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr style = 'height:40px;'>\n";
	$disp_tab .= "<th>商品名</td>\n";
	$disp_tab .= "<td>$tpm_f_products_name</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<th>現在価格</td>\n";
	$disp_tab .= "<td>".number_format($tp_f_now_price)."円</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "</table><br>\n";
        if($type==1)
        {
            $disp_tab .= "現在までの入札状況<br>\n";
        }
        else
        {
            $disp_tab .= "終了までの入札状況<br>\n";
        }
	$disp_tab .= "<br>\n";
        $disp_tab .= "<table class = 'list'>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<th style = 'width:100px;'>入札タイプ</th>\n";
	$disp_tab .= "<th style = 'width:60px;'>回数</th>\n";
	$disp_tab .= "<th style = 'width:120px;'>フリーコイン入札</th>\n";
	$disp_tab .= "<th style = 'width:120px;'>課金コイン入札</th>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<td>手動入札</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($p_count_free+$p_count_buy)."回</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($p_count_free)."回</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($p_count_buy)."回</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<td>手動入札2</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($r_count)."回</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($r_count)."回</td>\n";
	$disp_tab .= "<td class=\"right\">-回</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<td>自動入札</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($p_count_a_free+$p_count_a_buy)."回</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($p_count_a_free)."回</td>\n";
	$disp_tab .= "<td class=\"right\">".number_format($p_count_a_buy)."回</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "</table><br>\n";
	
	ProductsAutoBitList($pid,$limit,$offset,$data_cnt,$fk_member_id,$f_handle,$f_min_price,$f_max_price,
						$f_free_coin,$f_buy_coin,$f_set_free_coin,$f_set_buy_coin,$f_status,$all_count);
	
	$disp_tab .= Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/product/product_autobit_list.php?pid=$pid", 5);
	$disp_tab .= "<br><br>\n";
	$disp_tab .= "<table class='list'>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<th rowspan='2' style = 'width:180px;'>設定者</th>\n";
	$disp_tab .= "<th rowspan='2' style = 'width:100px;'>最小価格</th>\n";
	$disp_tab .= "<th rowspan='2' style = 'width:100px;'>最大価格</th>\n";
	$disp_tab .= "<th colspan='2'>設定コイン</th>\n";
        if($type==1)
        {
            $disp_tab .= "<th colspan='2' style='width:100px;'>残コイン</th>\n";
            $disp_tab .= "<th rowspan='2' style='width:100px;'>状態</th>\n";
        }
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<th style='width:100px;'>フリーコイン</th>\n";
	$disp_tab .= "<th style='width:100px;'>課金コイン</th>\n";
	if($type==1)
        {
            $disp_tab .= "<th style='width:100px;'>フリーコイン</th>\n";
            $disp_tab .= "<th style='width:100px;'>課金コイン</th>\n";
        }
	$disp_tab .= "</tr>\n";
	
	for($i=0;$i<$data_cnt;$i++)
	{
		//会員状態取得関数
		GetTMemberActivity($fk_member_id[$i],$tmm_f_activity);
		
		//▼会員状態を判定
		if($tmm_f_activity == 99)
		{
			//ロボット
			$disp_tab .= "<tr bgcolor='#dcdcdc'>\n";
		}
		else
		{
			$disp_tab .= "<tr>\n";
		}
		if($f_handle[$i] !="")
		{	
			$link="../member/mem_log.php?limit=50&offset=0&page=&sid=$sid&mid=$fk_member_id[$i]&ltype=1";
			$disp_tab .= "<td><a href='$link'>$f_handle[$i]</a></td>\n";
		}
		else
		{
			$disp_tab .= "<td>削除済み</td>\n";
		}
		$disp_tab .= "<td class=\"right\">".number_format($f_min_price[$i])."円</td>\n";
		$disp_tab .= "<td class=\"right\">".number_format($f_max_price[$i])."円</td>\n";
		$disp_tab .= "<td class=\"right\">".number_format($f_set_free_coin[$i])."枚</td>\n";
		$disp_tab .= "<td class=\"right\">".number_format($f_set_buy_coin[$i])."枚</td>\n";
                if($type==1)
                {
                    $disp_tab .= "<td class=\"right\">".number_format($f_free_coin[$i])."枚</td>\n";
                    $disp_tab .= "<td class=\"right\">".number_format($f_buy_coin[$i])."枚</td>\n";
                    $status="";
                    switch($f_status[$i])
                    {
                            case 0:
                                            $status = "実施中";
                                            break;
                            case 1:
                                            $status = "終了";
                                            break;
                            case 2:
                                            $status = "中止";
                                            break;
			    case 9:
					    $status = "処理済";
					    break;
                    }
                    $disp_tab .= "<td>$status</td>\n";
                }
	}
	$disp_tab .= "</table>\n";
      	$disp_tab .= "</fieldset>\n";
	$disp_tab .= Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/product/product_autobit_list.php?pid=$pid&type=$type", 5);
	$disp_tab .="<P>※『削除済み』:退会又は強制退会により削除されたユーザです</P>";
	$disp_tab .="<P>※『処理済』  :コイン返却チェックが済んでいる状態です</P>";
        $disp_tab .= "<button style='width:80px;' onClick='window.close();'>閉じる</button>\n";	
	//管理画面入力ページ表示関数
	PrintAdminPage("自動入札設定一覧",$disp_tab);
?>
