<script type="text/javascript">
    /**
     * 確認ダイアログの返り値によりフォーム送信
    */
<!--
	function submitChk ()
	{
		/* 確認ダイアログ表示 */
		var flag = confirm ('指定した写真を削除します。よろしいですか？');
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
-->
</script>
<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$fk_item_category = $_REQUEST["pid"];
	$mode = $_REQUEST["mode"];
	$photo_num = $_REQUEST["photo_num"];

	$newFlg=false;

	if(isset($fk_item_category) && $fk_item_category != "")
	{
		//商品カテゴリ取得関数
            //■ＤＢ接続
            $db=db_connect();
            if($db == false) {
                exit;
            }

            $sql  = "select fk_item_category,f_category_name,f_mail_category_val,f_tm_stamp,f_rbstp,f_rbedp,f_status,f_dspno ";
            $sql .= "from auction.t_item_category where fk_item_category='$fk_item_category' ";

            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ＤＢ切断
                db_close($db);
                return false;
            }
            //■データ件数
            $rows = mysql_num_rows($result);

            if($rows > 0) {
                $fk_item_category	= mysql_result($result, 0, "fk_item_category");
                $f_category_name	= mysql_result($result, 0, "f_category_name");
                $f_mail_category_val	= mysql_result($result, 0, "f_mail_category_val");
                $f_tm_stamp	= mysql_result($result, 0, "f_tm_stamp");
                $f_rbstp	= mysql_result($result, 0, "f_rbstp");
                $f_rbedp	= mysql_result($result, 0, "f_rbedp");
                $f_status	= mysql_result($result, 0, "f_status");
                $f_dspno	= mysql_result($result, 0, "f_dspno");
            }
            else {
                $fk_item_category	= "";
                $f_category_name	= "";
                $f_mail_category_val	= "";
                $f_tm_stamp	= "";
                $f_rbstp	= "";
                $f_rbedp	= "";
                $f_status	= "";
                $f_dspno	= "";
            }
            //■ＤＢ切断
            db_close($db);
        }
	else
	{
            $newFlg = true;
            $fk_item_category	= "";
            $f_category_name	= "";
            $f_mail_category_val	= "";
            $f_tm_stamp	= "";
            $f_rbstp	= "";
            $f_rbedp	= "";
            $f_status	= "";
            $f_dspno	= "";
	}

        $f_status_id[0] = "0";
	$f_status_name[0] = "使用中";
	$f_status_id[1] = "1";
	$f_status_name[1] = "未使用";
	$f_status_cnt = 2;
	$name = "f_status";

        if($f_status == "")
	{
		$f_status = 0;
	}
	else if($f_status > 2)
	{
		$f_status = 1;
	}
	$select_num = $f_status;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);

	$dsp_tbl  = "";
        $dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .= "<legend>商品カテゴリ編集</legend>\n";
	$dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>商品カテゴリID</th>\n";
	$dsp_tbl .= "<td style = 'width:200px;'>$fk_item_category\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<FORM action='product_category_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>商品カテゴリ名</th>\n";
	$dsp_tbl .= "<td style = 'width:200px;'><input type='text' name='f_category_name' value='$f_category_name' size='50'></td>\n";
	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>状態</th>\n";
        if(isset($fk_item_category) && $fk_item_category>0){
            $dsp_tbl .= "<td style = 'width:200px;'>$f_status_select</font></td>\n";
        }else{
            $dsp_tbl .= "<td style = 'width:200px;'>使用中<INPUT TYPE='hidden' NAME='f_status' value='0'></td>\n";
        }

	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";

        $dsp_tbl .= "<div style = 'text-align:center;'>\n";
        if($newFlg){
            $dsp_tbl .= "<input type='hidden' name='pid' value='-1'>\n";
        }else{
            $dsp_tbl .= "<input type='hidden' name='pid' value='$fk_item_category'>\n";
        }
	$dsp_tbl .= "<button type='submit' style='width:80px;'>更新</button>\n";
        $dsp_tbl .= "</FORM>\n";
        if(isset($fk_item_category) && $fk_item_category>0 && $f_status==0){
            $dsp_tbl .= "<FORM action='product_category_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
            $dsp_tbl .= "<input type='hidden' name='fk_item_category' value='$fk_item_category'>\n";
            $dsp_tbl .= "<input type='submit' class = 'button' style='width:80px;' value='削除'>\n";
            $dsp_tbl .= "</FORM>\n";
        }
        $dsp_tbl .= "</div>\n";
	//管理画面入力ページ表示関数
	PrintAdminPage("商品カテゴリ編集",$dsp_tbl);
?>
