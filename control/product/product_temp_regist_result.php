<?php
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$image_class = HOME_PATH."mvc/utils/Image.class.php";
	$file_class = HOME_PATH."mvc/utils/File.class.php";
	$string_class = HOME_PATH."mvc/utils/StringBuilder.class.php";
	$constkeys_class = HOME_PATH."mvc/config/ConstKeys.class.php";
	include $image_class;
	include $file_class;
	include $string_class;
	include $constkeys_class;

    /**
     * モバイルFlash用の商品画像生成対応
     * ※関連するシステムパラメータが全て設定されていないと動作しません
     */
    $mob_flash = FALSE;
    $flash_config_array = array(
        'MOBILE_FLASH_PRD_IMG_SIZE'=>'',
        'MOBILE_FLASH_PRD_IMG_PREFIX'=>NULL,
    );
    if ( defined('MOBILE_FLASH_ENABLE') && constant('MOBILE_FLASH_ENABLE') ) {
        $mob_flash = TRUE;
        foreach ( $flash_config_array as $key=>$value ) {
            if ( defined($key) && constant($key) ) {
                $flash_config_array[($key)] = constant($key);
            } else {
                error_log('MOBILE_FLASH: constant '.$key.' is undefiend or empty.');
                $mob_flash = FALSE;
                break;
            }
        }
    }

	//---------------------------------------------------------------------------------
	//▼画像
	$PATH = HOME_PATH."images/";

	$file=new File();
	$file->thumb=true;
	$file->thumbPath=',mb';
	$file->thumbPrefix   = 'pc_,mb_';
	$file->thumbMaxWidth = PRODUCT_IMAGE_WIDTH_PC.",".PRODUCT_IMAGE_WIDTH_MB;
	$file->thumbMaxHeight = PRODUCT_IMAGE_HEIGHT_PC.",".PRODUCT_IMAGE_HEIGHT_MB;
	$file->thumbRemoveOrigin=false;
	$file->uuidFileName=true;
	$info=$file->upload($PATH);

	$com1="";
	if(empty($_FILES["file1"])==FALSE)
	{
	    if($_FILES["file1"]["size"] > 0)
	    {
			$com1="<P>".$_FILES["file1"]["name"]."をアップしました</P>";
			print $com1."<br>\n";
			$f_photo1 = "pc_".$info[0]["savename"];
			$f_photo1mb = "mb_".$info[0]["savename"];

			$f=$PATH.$f_photo1;
			$file->FixImage($f,$PATH.PRODUCT_LITTLE_PREFIX.$f_photo1,'',PRODUCT_LITTLE_WIDTH,PRODUCT_LITTLE_HEIGHT,true);
            if ( $mob_flash ) {
    			$file->FixImage2(
                    $PATH.$f_photo1
                    ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo1mb).'.jpg'
                    ,'' // 元画像の形式
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                    ,true  // interlace->未使用臭い
                    ,'jpeg'
                );
            }
	    }
	    else
	    {
	        $com1="";
	    }
	}

	$com2="";
	if(empty($_FILES["file2"])==FALSE)
	{
	    if($_FILES["file2"]["size"] > 0)
	    {
			$com2="<P>".$_FILES["file2"]["name"]."をアップしました</P>";
			print $com2."<br>\n";
			$f_photo2 = "pc_".$info[1]["savename"];
			$f_photo2mb = "mb_".$info[1]["savename"];
            if ( $mob_flash ) {
    			$file->FixImage2(
                    $PATH.$f_photo2
                    ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo2mb).'.jpg'
                    ,'' // 元画像の形式
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                    ,true  // interlace->未使用臭い
                    ,'jpeg'
                );
            }
	    }
	    else
	    {
	        $com2="";
	    }
	}

	$com3="";
	if(empty($_FILES["file3"])==FALSE)
	{
	    if($_FILES["file3"]["size"] > 0)
	    {
			$com3="<P>".$_FILES["file3"]["name"]."をアップしました</P>";
			print $com3."<br>\n";
			$f_photo3 = "pc_".$info[2]["savename"];
			$f_photo3mb = "mb_".$info[2]["savename"];
            if ( $mob_flash ) {
    			$file->FixImage2(
                    $PATH.$f_photo3
                    ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo3mb).'.jpg'
                    ,'' // 元画像の形式
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                    ,true  // interlace->未使用臭い
                    ,'jpeg'
                );
            }
	    }
	    else
	    {
	        $com3="";
	    }
	}

	$com4="";
	if(empty($_FILES["file4"])==FALSE)
	{
	    if($_FILES["file4"]["size"] > 0)
	    {
			$com4="<P>".$_FILES["file4"]["name"]."をアップしました</P>";
			print $com4."<br>\n";
			$f_photo4 = "pc_".$info[3]["savename"];
			$f_photo4mb = "mb_".$info[3]["savename"];
            if ( $mob_flash ) {
    			$file->FixImage2(
                    $PATH.$f_photo4
                    ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo4mb).'.jpg'
                    ,'' // 元画像の形式
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                    ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                    ,true  // interlace->未使用臭い
                    ,'jpeg'
                );
            }
	    }
	    else
	    {
	        $com4="";
	    }
	}
	//---------------------------------------------------------------------------------

	$code="'";
	$code_2="&#39;";
	$pid = $_REQUEST["pid"];
	$f_products_name = str_replace($code,$code_2,$_REQUEST["f_products_name"]);
	$fk_item_category_id = str_replace($code,$code_2,$_REQUEST["fk_item_category_id"]);
	$f_top_description_pc = str_replace($code,$code_2,$_REQUEST["f_top_description_pc"]);
	$f_main_description_pc = str_replace($code,$code_2,$_REQUEST["f_main_description_pc"]);
	$f_top_description_mb = str_replace($code,$code_2,$_REQUEST["f_top_description_mb"]);
	$f_main_description_mb = str_replace($code,$code_2,$_REQUEST["f_main_description_mb"]);
	$f_start_price = str_replace($code,$code_2,$_REQUEST["f_start_price"]);
	$f_min_price = str_replace($code,$code_2,$_REQUEST["f_min_price"]);
	$f_r_min_price = str_replace($code,$code_2,$_REQUEST["f_r_min_price"]);
	$f_r_max_price = str_replace($code,$code_2,$_REQUEST["f_r_max_price"]);
	$f_market_price = str_replace($code,$code_2,$_REQUEST["f_market_price"]);
	$f_plus_coins= str_replace($code,$code_2,$_REQUEST["f_plus_coins"]);
	$f_address_flag= str_replace($code,$code_2,$_REQUEST["f_address_flag"]);

	if($pid == "")
	{
		//print "不正な処理です。<br>\n";
		PrintAdminPage("テンプレート商品登録完了","<P>不正な処理です</P>");
		exit;
	}
	else if($pid == "0")
	{
		//商品テンプレート登録関数
		$ret = RegistTProductsTemplate($f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_plus_coins,$f_address_flag);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "テンプレートの新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//商品テンプレート更新関数
		$ret = UpdateTProductsTemplate($pid,$f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_plus_coins,$f_address_flag);
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "テンプレートの更新処理に成功しました。<br>\n";
		}
	}

	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";

	        if (SERVER_NUM > 1)
        {
            `/home/auction/script/php/date_sync/sync_prod_image.sh`;
        }
	//管理画面入力ページ表示関数
	PrintAdminPage("テンプレート商品登録完了",$dsp_tbl);

?>
