<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$image_class = HOME_PATH."mvc/utils/Image.class.php";
	$file_class = HOME_PATH."mvc/utils/File.class.php";
	$string_class = HOME_PATH."mvc/utils/StringBuilder.class.php";
	$constkeys_class = HOME_PATH."mvc/config/ConstKeys.class.php";

	$pid = $_REQUEST["pid"];
	$f_category_name = $_REQUEST["f_category_name"];
	$f_rbedp = $_REQUEST["f_rbedp"];
        $f_status = $_REQUEST["f_status"];
        $now = date("Y-m-d H:i:s", time());
        $maxdspno = GetCategoryDspno();

	if($pid == "")
	{
		//print "不正な処理です。<br>\n";
		PrintAdminPage("商品カテゴリ登録完了","<P>不正な処理です</P>");
		exit;
	}
	else if($pid == "-1")
	{
		//商品テンプレート登録関数
		$ret = RegistProductsCategory($f_category_name,$now,$f_status,$maxdspno);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "商品カテゴリ新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//商品テンプレート更新関数
		$ret = UpdateCategory($pid,$f_category_name,$f_status);
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "テンプレートの更新処理に成功しました。<br>\n";
		}
	}

	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "下の閉じるﾎﾞﾀﾝを押して下さい。\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "<button type='button' style='width:80px;' onclick='window.close()'>閉じる</button>\n";
	$dsp_tbl .= "</form>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("商品カテゴリ登録完了",$dsp_tbl);
?>
