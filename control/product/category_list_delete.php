<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/07												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;

	session_start();
	$sid = $_SESSION["staff_id"];

	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$pid = $_REQUEST["prod_id"];
	$sort = $_REQUEST["sort"];
        $tic_fk_item_category = $_REQUEST["tic_fk_item_category"];
        $f_dspno = $_REQUEST["f_dspno"];
        $dspno_desc = $_REQUEST["dspno_desc"];

        $act_flg = isset($_REQUEST["act_flg"])?$_REQUEST["act_flg"]:"";
        $fk_item_category = isset($_REQUEST["fk_item_category"])?$_REQUEST["fk_item_category"]:"";
        if($act_flg == "del" && $fk_item_category!=""){
             //■ＤＢ接続
            $ret = false;
            try{
                $db=db_connect();
                if($db == false) {
                    exit;
                }

                //■トランザクション開始
                mysql_query("BEGIN", $db);

                //t_products_template
                $sql = "update auction.t_item_category set f_status=1 where fk_item_category='$fk_item_category' ";

                //■ＳＱＬ実行
                $result = mysql_query($sql, $db);
                if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ロールバック
                    mysql_query("ROLLBACK", $db);
                    //■ＤＢ切断
                    db_close($db);
                    return false;
                }

                //■コミット
                mysql_query("COMMIT", $db);

                //■ＤＢ切断
                db_close($db);
                //return true;
                $ret = true;
            }
            catch(Exception $e) {
                DB::rollback();
            }

            if(!$ret){
                //
            }
        }

	if ($limit == "")
	{
		$limit = 50;
	}

	if ($offset == "")
	{
		$offset = 0;
	}

	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}

	//商品カテゴリ一覧取得関数

        GetItemCategoryList_1($tic_fk_item_category, $tic_f_category_name, $tic_f_mail_category_val, $tic_f_tm_stamp, $f_status, $tic_data_cnt, $tic_all_cnt, $sort, $limit, $offset);

	//▼表示開始～終了番号
	if($tic_all_cnt == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tic_data_cnt;

	//■表示一覧
	$dsp_tbl  = "";

	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tic_all_cnt / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='category_list.php?f_status=0&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
				}
		}
	}

	if($tic_all_cnt > $limit)
	{
		$page_first = "<A href='category_list.php?f_status=0&offset=0&limit=$limit&sort=$sort'>";
		$page_first .= "|<";
		$page_first .= "</A> ";

		$max_page = $page_count - 1;
		$page_last  = "<A href='category_list.php?f_status=0&offset=$page_offset[$max_page]&limit=$limit&sort=$sort'>";
		$page_last .= ">|";
		$page_last .= "</A> ";

		$next_num = $tic_all_cnt - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='category_list.php?f_status=0&offset=$next_page&limit=$limit&sort=$sort'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}

		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='category_list.php?f_status=0&offset=$before_page&limit=$limit&sort=$sort'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}

		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
		}
		$url = "category_list.php?f_status=0&limit=$limit&offset=&sort=$sort";
		$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);

		$dsp_tbl .= "<left>\n";
		$dsp_tbl .= $pageNumber;

		$form_str = "";
		if($pageNumber != "")
		{
			$form_str .= "<span class='mypagenum_box4'>\n";
			$form_str .= "<form action='category_list.php' method='get' style='display: inline'>\n";
			$form_str .= "<input type='text' size='2' name='page'/>\n";
			$form_str .= "<input type='submit' value='Jump'/>\n";
                        $dsp_tbl .= "<td align='center'>$status_str</td>\n";
			$form_str .= "<input type='hidden' name='offset' value=''>";
			$form_str .= "</form>\n";
			$form_str .= "</span>\n";
		}
		$dsp_tbl .= $form_str;
		$dsp_tbl .= "</left>\n";


		$dsp_tbl .= "<table class = 'list'>\n";
		$dsp_tbl .= "<tr style = 'height:30px;'>\n";
		$dsp_tbl .= "<th style = 'width:80px;'>データ件数</th>\n";
		$dsp_tbl .= "<td style = 'width:50px;'>$tic_all_cnt</th>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "</table>\n<br>\n";
		$dsp_tbl .= "$start_num&nbsp;\n";
		$dsp_tbl .= "件目&nbsp;～&nbsp; \n";
		$dsp_tbl .= "$end_num&nbsp;\n";
		$dsp_tbl .= "件目\n";
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<table class = 'list'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>カテゴリID</th>\n";
		$dsp_tbl .= "<th style = 'width:180px;'>カテゴリ名前</th>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>状態</th>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>操作</th>\n";

		$dsp_tbl .= "</tr>\n";
		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$tic_data_cnt;$i++)
		{

			//▼状態
			switch($f_status[$i])
			{
				case 0:		$status_str = "使用中";
	                                        break;
				case 1:		$status_str = "未使用";
	                                        break;
			}

			//▼登録・編集画面へのリンク
			$link_reg = "<A href='product_category_regist.php?pid=$tic_fk_item_category[$i]&mode=&photo_num=&tmpid=$tmppht_next' target='_blank'>編集</A>";

                        $link_reg2 = " ";
			//表示順でソートを設定
                        //下向こう矢印押す場合
                        $dspno_sort_1 = "<A href='category_list.php?tic_fk_item_category=$tic_fk_item_category[$i]&f_dspno=$f_dspno[$i]&sort=$sort&dspno_desc=1'>↓</A>";
                        //上向こう矢印押す場合
                        $dspno_sort_2 = "<A href='category_list.php?tic_fk_item_category=$tic_fk_item_category[$i]&f_dspno=$f_dspno[$i]&sort=$sort&dspno_desc=2'>↑</A>";

			$dsp_tbl .= "<tr style = 'height:30px;'>\n";
			$dsp_tbl .= "<td align='center'>$tic_fk_item_category[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tic_f_category_name[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$status_str</td>\n";
			$dsp_tbl .= "<td align='center'>$link_reg</td>\n";

			$dsp_tbl .= "</tr>\n";
		}

		$dsp_tbl .= "</table><br>\n";

		//▼ページ移動リンク
		$dsp_tbl .= "<left>\n";
		$dsp_tbl .= $pageNumber;
		$dsp_tbl .= $form_str;
		$dsp_tbl .= "</left>\n";
		$dsp_tbl .= "<br>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("商品カテゴリ編集",$dsp_tbl);
?>
