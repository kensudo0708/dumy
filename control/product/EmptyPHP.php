<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/08												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	//▼画像
	$PATH = HOME_PATH."images/";
	$com1="";
	if(empty($_FILES["file1"])==FALSE)
	{
	    if($_FILES["file1"]["size"] > 0)
	    {
	        $up_file=$PATH .$_FILES["file1"]["name"];
	        $result = @move_uploaded_file( $_FILES["file1"]["tmp_name"], $up_file);
			chmod($up_file, 0755);
	        if($result==TRUE)
	        {
	            $com1="<P>".$_FILES["file1"]["name"]."をアップしました</P>";
				$f_photo1 = $_FILES["file1"]["name"];
	        }
	        else
	        {
	            $com1="<P>アップロード失敗しました</P>";
	        }
			print $com1."<br>\n";
	    }
	    else
	    {
	        $com1="";
	    }
	}

	$com2="";
	if(empty($_FILES["file2"])==FALSE)
	{
	    if($_FILES["file2"]["size"] > 0)
	    {
	        $up_file=$PATH .$_FILES["file2"]["name"];
	        $result = @move_uploaded_file( $_FILES["file2"]["tmp_name"], $up_file);
			chmod($up_file, 0755);
	        if($result==TRUE)
	        {
	            $com2="<P>".$_FILES["file2"]["name"]."をアップしました</P>";
				$f_photo2 = $_FILES["file2"]["name"];
	        }
	        else
	        {
	            $com2="<P>アップロード失敗しました</P>";
	        }
			print $com2."<br>\n";
	    }
	    else
	    {
	        $com2="";
	    }
	}

	$com3="";
	if(empty($_FILES["file3"])==FALSE)
	{
	    if($_FILES["file3"]["size"] > 0)
	    {
	        $up_file=$PATH .$_FILES["file3"]["name"];
	        $result = @move_uploaded_file( $_FILES["file3"]["tmp_name"], $up_file);
			chmod($up_file, 0755);
	        if($result==TRUE)
	        {
	            $com3="<P>".$_FILES["file3"]["name"]."をアップしました</P>";
				$f_photo3 = $_FILES["file3"]["name"];
	        }
	        else
	        {
	            $com3="<P>アップロード失敗しました</P>";
	        }
			print $com3."<br>\n";
	    }
	    else
	    {
	        $com3="";
	    }
	}

	$com4="";
	if(empty($_FILES["file4"])==FALSE)
	{
	    if($_FILES["file4"]["size"] > 0)
	    {
	        $up_file=$PATH .$_FILES["file4"]["name"];
	        $result = @move_uploaded_file( $_FILES["file4"]["tmp_name"], $up_file);
			chmod($up_file, 0755);
	        if($result==TRUE)
	        {
	            $com4="<P>".$_FILES["file4"]["name"]."をアップしました</P>";
				$f_photo4 = $_FILES["file4"]["name"];
	        }
	        else
	        {
	            $com4="<P>アップロード失敗しました</P>";
	        }
			print $com4."<br>\n";
	    }
	    else
	    {
	        $com4="";
	    }
	}

	$pid = $_REQUEST["pid"];
	$f_status = $_REQUEST["f_status"];
	$f_stock = $_REQUEST["f_stock"];
	$f_pickup_flg = $_REQUEST["f_pickup_flg"];
	$f_memo = $_REQUEST["f_memo"];
	$cost = $_REQUEST["cost"];
	$mem_id = $_REQUEST["mem_id"];
	$sid = $_REQUEST["sid"];

	$hold_status = $_REQUEST["hold_status"];

	if($pid == "")
	{
		$dsp_string = "不正な処理です。<br>\n";
	}
	else
	{
		//▼ピックアップ商品は削除不可
		if(($f_pickup_flg == 1) && ($f_status == 9))
		{
			$dsp_string = "ピックアップ商品は削除できません。<br>\n";
		}
		else
		{
			if(($f_status == 4) && ($hold_status != 4))
			{
				$ret = SetCostPay($pid,$sid,$mem_id,$cost);
			}
                        else if(($f_status == 5) && ($hold_status != 3))
			{
				$ret = 4;
			}
			else if($f_status == 9)
			{
				DeleteTProductsData($pid);
			}

                        $dsp_string="";
                        //オークション終了商品状態価格更新関数
			if($ret == 0 || $ret == 3)
                        {
                            if($ret == 3)
                            {
                                $dsp_string .= "出荷データが存在します。仕入額二重計上防止のため状態だけを変更します。<br>\n";
                            }
                            $ret = SetTEndProductsStatusPrice($pid, $f_status, $f_stock, $f_pickup_flg, $cost);
                        }

                        if($ret == 1)
			{
				$dsp_string .= "更新処理に失敗しました。<br>\n";
			}
                        else if($ret == 2)
                        {
                            $dsp_string .= "決済が完了していないため更新処理に失敗しました。<br>終わらせたい場合は削除にしてください\n";
                        }
                        else if($ret == 4)
                        {
                            $dsp_string .= "発送先が設定されていません。<BR>設定されてから発注してください\n";
                        }
			else
			{
				//オークション終了商品メモ更新関数
				$ret = UpdateTEndProductsMemo($pid, $f_memo);
				if($ret == false)
				{
					$dsp_string .= "商品メモの更新処理に失敗しました。<br>\n";
				}
				else
				{
                                        if($f_status==4){
                                            $member=$model->getMemberById($mem_id);
                                            //配送済みの確認メールを送信処理
                                            $to=$member->f_mail_address_pc;
                                            echo $to;
                                            $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/mail_delivery.html";
                                            $from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
                                            $params=array("member_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
                                                    "reg_confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/login/confirm?code=".$member->fk_adcode_id,
                                                    "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                                                    "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
                                            $result = MailReserver::sendTemplateMail(MailReserver::REGIST,REGIST_MAIL_SUBJECT, $tpl, $params, $from, $to);
                                            if($result)
                                            //メールを送信成功
                                                return Message::showConfirmMessage($this, Msg::get("REGISTER_TEMP_SUCCESS"));
                                            else {
                                                //メールを送信失敗
                                                $this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
                                                return $this->createTemplateResult("register.html");
                                            }

                                        }
					$dsp_string .= "発注・出荷情報の更新処理に成功しました。<br>\n";
				}
			}
		}
	}

	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("発注・出荷編集完了",$dsp_tbl);

?>

