<script type="text/javascript">
    /**
     * 確認ダイアログの返り値によりフォーム送信
    */
<!--
	function submitChk ()
	{
		/* 確認ダイアログ表示 */
		var flag = confirm ('指定した写真を削除します。よろしいですか？');
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
-->
</script>
<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$pid = $_REQUEST["pid"];
	$mode = $_REQUEST["mode"];
	$photo_num = $_REQUEST["photo_num"];
	
	//写真削除モード
	if($mode == "dltphoto")
	{
		//商品テンプレート写真削除関数
		DeleteTProductsTemplatePhoto($pid, $photo_num);
	}
	
	if(($pid != "") && ($pid != 0))
	{
		//商品テンプレート取得関数
		GetTProductsTemplateInfo($pid,$tpt_f_products_name,$tpt_fk_item_category_id,$tpt_f_photo1,$tpt_f_photo2,$tpt_f_photo3,$tpt_f_photo4,$tpt_f_photo5,$tpt_f_photo1mb,$tpt_f_photo2mb,$tpt_f_photo3mb,$tpt_f_photo4mb,$tpt_f_photo5mb,$tpt_f_top_description_pc,$tpt_f_main_description_pc,$tpt_f_top_description_mb,$tpt_f_main_description_mb,$tpt_f_market_price,$tpt_f_min_price,$tpt_f_start_price,$tpt_f_r_min_price,$tpt_f_r_max_price,$tpt_f_status,$tpt_f_memo,$tpt_f_tm_stamp,$tpt_f_plus_coins,$tpt_f_address_flag);
	}
	else
	{
		$tpt_f_products_name			= "";
		$tpt_fk_item_category_id		= "";
		$tpt_f_photo1					= "";
		$tpt_f_photo2					= "";
		$tpt_f_photo3					= "";
		$tpt_f_photo4					= "";
		$tpt_f_photo5					= "";
		$tpt_f_photo1mb					= "";
		$tpt_f_photo2mb					= "";
		$tpt_f_photo3mb					= "";
		$tpt_f_photo4mb					= "";
		$tpt_f_photo5mb					= "";
		$tpt_f_top_description_pc		= "";
		$tpt_f_main_description_pc		= "";
		$tpt_f_top_description_mb		= "";
		$tpt_f_main_description_mb		= "";
		$tpt_f_market_price				= "0";
		$tpt_f_min_price				= "0";
		$tpt_f_start_price				= "0";
		$tpt_f_r_min_price				= "100";
		$tpt_f_r_max_price				= "99999999";
		$tpt_f_status					= "";
		$tpt_f_memo						= "";
		$tpt_f_tm_stamp					= "";
		$tpt_f_plus_coins				= "0";
		$tpt_f_address_flag				= "0";
	}
	
	//▼カテゴリ選択作成
	//商品カテゴリ一覧取得関数
	GetTItemCategoryList($tic_fk_item_category, $tic_f_category_name, $tic_f_mail_category_val, $tic_f_tm_stamp, $tic_data_cnt);
	$name = "fk_item_category_id";
	$select_num = $tpt_fk_item_category_id;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $tic_fk_item_category, $tic_f_category_name, $tic_data_cnt, $select_num, $category_select);
	
	//▼写真
	if($tpt_f_photo1 != "")
	{
		$f_photo_1_path = SITE_URL."images/".$tpt_f_photo1;
		$f_photo_1_dsp = "<img src='$f_photo_1_path'><br>";
		$link_dltphoto1 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=1' onClick=\"return submitChk()\">削除</A>";
	}
	else
	{
		$f_photo_1_dsp = "";
		$link_dltphoto1 = "";
	}
	
	if($tpt_f_photo2 != "")
	{
		$f_photo_2_path = SITE_URL."images/".$tpt_f_photo2;
		$f_photo_2_dsp = "<img src='$f_photo_2_path'><br>";
		$link_dltphoto2 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=2' onClick=\"return submitChk()\">削除</A>";
	}
	else
	{
		$f_photo_2_dsp = "";
		$link_dltphoto2 = "";
	}
	
	if($tpt_f_photo3 != "")
	{
		$f_photo_3_path = SITE_URL."images/".$tpt_f_photo3;
		$f_photo_3_dsp = "<img src='$f_photo_3_path'><br>";
		$link_dltphoto3 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=3' onClick=\"return submitChk()\">削除</A>";
	}
	else
	{
		$f_photo_3_dsp = "";
		$link_dltphoto3 = "";
	}
	
	if($tpt_f_photo4 != "")
	{
		$f_photo_4_path = SITE_URL."images/".$tpt_f_photo4;
		$f_photo_4_dsp = "<img src='$f_photo_4_path'><br>";
		$link_dltphoto4 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=4' onClick=\"return submitChk()\">削除</A>";
	}
	else
	{
		$f_photo_4_dsp = "";
		$link_dltphoto4 = "";
	}
	
	// CKEditorインクルード
	$dsp_tbl  = '<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>'."\n";
	$dsp_tbl .= '<script type="text/javascript">'."\n";
	$dsp_tbl .= 'CKEDITOR.config.toolbar = ['."\n";
	$dsp_tbl .= "    ['Source','-','Undo','Redo'],"."\n";
	$dsp_tbl .= "    ['FontSize','Bold','Italic','Underline','Strike','Superscript'],"."\n";
	$dsp_tbl .= "    ['TextColor','BGColor'],"."\n";
	$dsp_tbl .= "    ['Outdent','Indent','HorizontalRule','Find'],"."\n";
	$dsp_tbl .= '];'."\n";
	$dsp_tbl .= "CKEDITOR.config.width  = '570px';"."\n";
	$dsp_tbl .= "CKEDITOR.config.height = '160px';"."\n";
	$dsp_tbl .= '</script>'."\n";

        // FORM
        $dsp_tbl .= '<FORM action="product_temp_regist_result.php" method="POST" ENCTYPE="multipart/form-data" style="display: inline">'."\n";

        // 商品概要
        $dsp_tbl .= '<fieldset>'."\n";
        $dsp_tbl .= '<legend>商品概要</legend>'."\n";
        $dsp_tbl .= '<table class="form">'."\n";
        $dsp_tbl .= '<tr>'."\n";
        $dsp_tbl .= '<th>テンプレートID</th>'."\n";
        $dsp_tbl .= '<td>'.$pid.'<input type="hidden" name="pid" value="'.$pid.'"></td>'."\n";
        $dsp_tbl .= '</tr>'."\n";
        $dsp_tbl .= '<tr>'."\n";
        $dsp_tbl .= '<th>商品名</th>'."\n";
        $dsp_tbl .= '<td><input type="text" name="f_products_name" value="'.$tpt_f_products_name.'" style="width:570px;ime-mode:active"></td>'."\n";
        $dsp_tbl .= '</tr>'."\n";
        $dsp_tbl .= '<tr>'."\n";
        $dsp_tbl .= '<th>カテゴリ</th>'."\n";
        $dsp_tbl .= '<td>'."\n";
        $dsp_tbl .= $category_select."\n";
        $dsp_tbl .= '</td>'."\n";
        $dsp_tbl .= '</tr>'."\n";
        $dsp_tbl .= '</table>'."\n";
        $dsp_tbl .= '</fieldset>'."\n";

        // 商品画像
	$dsp_tbl .= '<fieldset>'."\n";
	$dsp_tbl .= '<legend>商品画像</legend>'."\n";
	$dsp_tbl .= '<INPUT TYPE="hidden" NAME="MAX_FILE_SIZE" SIZE="65536">'."\n";
	$dsp_tbl .= '<table class="form">'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>写真①</th>'."\n";
	$dsp_tbl .= '<td>'.$f_photo_1_dsp.'<input type="file" name="file1">'.$link_dltphoto1.'</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>写真②</th>'."\n";
	$dsp_tbl .= '<td>'.$f_photo_2_dsp.'<input type="file" name="file2">'.$link_dltphoto2.'</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>写真③</th>'."\n";
	$dsp_tbl .= '<td>'.$f_photo_3_dsp.'<input type="file" name="file3">'.$link_dltphoto3.'</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>写真④</th>'."\n";
	$dsp_tbl .= '<td>'.$f_photo_4_dsp.'<input type="file" name="file4">'.$link_dltphoto4.'</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '</table>'."\n";
	$dsp_tbl .= '<tr><th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th><td>
<span style="color:#ff0000">※&nbsp;画像ファイルは、半角の英数字のみにしてさい。
<br><th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>※&nbsp;jpgファイルのみ対応しています。</span></t></tr>'."\n";
	$dsp_tbl .= '</fieldset>'."\n";

        // 商品詳細
	$dsp_tbl .= '<fieldset>'."\n";
	$dsp_tbl .= '<legend>商品詳細</legend>'."\n";
	$dsp_tbl .= '<table class="form">'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>PC用（見出し）</th>'."\n";
	$dsp_tbl .= '<td>HTMLタグをご利用の際は属性値の囲み文字にはシングルクオートを使用してください。<br><input type="text" name="f_top_description_pc" value="'.$tpt_f_top_description_pc.'" style="width:570px;ime-mode:active"></td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<th>PC用（詳細）</th>'."\n";
	$dsp_tbl .= '<td><textarea class="ckeditor" name="f_main_description_pc" style="width:570px;height:150px;ime-mode:active;font-size:9pt">'.$tpt_f_main_description_pc.'</textarea></td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>MB用（見出し）</th>'."\n";
	$dsp_tbl .= '<td>HTMLタグをご利用の際は属性値の囲み文字にはシングルクオートを使用してください。<br><input type="text" name="f_top_description_mb" value="'.$tpt_f_top_description_mb.'" style="width:570px;ime-mode:active"></td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>MB用（詳細）</th>'."\n";
	$dsp_tbl .= '<td><textarea name="f_main_description_mb" style="width:570px;height:150px;ime-mode:active;font-size:9pt">'.$tpt_f_main_description_mb.'</textarea></td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '</table>'."\n";
	$dsp_tbl .= '</fieldset>'."\n";

        // 料金設定
	$dsp_tbl .= '<fieldset>'."\n";
	$dsp_tbl .= '<legend>料金設定</legend>'."\n";
	$dsp_tbl .= '<table class="form">'."\n";
	$dsp_tbl .= '<table class="form">'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>一般市場価格<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td><input type="text" name="f_market_price" value="'.$tpt_f_market_price.'" style="width:160px;ime-mode:disabled"> 円</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>仕入価格<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td><input type="text" name="f_min_price" value="'.$tpt_f_min_price.'" style="width:160px;ime-mode:disabled"> 円</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>開始価格<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td><input type="text" name="f_start_price" value="'.$tpt_f_start_price.'" style="width:160px;ime-mode:disabled"> 円</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>増額時の最大価格<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td><input type="text" name="f_r_max_price" value="'.$tpt_f_r_max_price.'" style="width:160px;ime-mode:disabled"> 円</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>減額時の最小価格<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td><input type="text" name="f_r_min_price" value="'.$tpt_f_r_min_price.'" style="width:160px;ime-mode:disabled"> 円</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";
	$dsp_tbl .= '<th>ポイントオークション<br>でのポイント数<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td><input type="text" name="f_plus_coins" value="'.$tpt_f_plus_coins.'" style="width:160px;ime-mode:disabled"> 枚</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '<tr>'."\n";

        $selected0 = '';
        $selected1 = '';
       	if($tpt_f_address_flag == 0)
	{
		$selected0="selected";
		$selected1="";
	}
	else if($tpt_f_address_flag == 1)
	{
		$selected0="";
		$selected1="selected";
	}

	$dsp_tbl .= '<th>配送先設定<span style="color:#FF809F">*</span></th>'."\n";
	$dsp_tbl .= '<td>'."\n";
	$dsp_tbl .= '<select name="f_address_flag" style="width:160px">'."\n";
	$dsp_tbl .= '<OPTION value="0" '.$selected0.'>配送先必要</OPTION><OPTION value="1" '.$selected1.'>配送先不要</OPTION>'."\n";
	$dsp_tbl .= '</select>'."\n";
	$dsp_tbl .= '</td>'."\n";
	$dsp_tbl .= '</tr>'."\n";
	$dsp_tbl .= '</table>'."\n";
	$dsp_tbl .= '</fieldset>'."\n";

        // 操作ボタン
	$dsp_tbl .= '<div style="padding-bottom:5px;text-align:center">'."\n";
	$dsp_tbl .= '<input type="submit" style="background-color:#828282; width:120px; color:#FFFFFF; border-color:#FFFAFA" value="更新">'."\n";
	$dsp_tbl .= '</form>'."\n";
/*
	$dsp_tbl .= '<FORM action="product_temp_delete.php" method="POST" ENCTYPE="multipart/form-data" style="display: inline">'."\n";
        $dsp_tbl .= '<input type="hidden" name="pid" value="'.$pid.'">'."\n";
        $dsp_tbl .= '<input type="submit" style="background-color:#828282; width:120px; color:#FFFFFF; border-color:#FFFAFA" value="自動出品設定">'."\n";
        $dsp_tbl .= '</form>'."\n";
*/
	$dsp_tbl .= '<FORM action="product_temp_delete.php" method="POST" ENCTYPE="multipart/form-data" style="display: inline">'."\n";
	$dsp_tbl .= '<input type="hidden" name="pid" value="'.$pid.'">'."\n";
	$dsp_tbl .= '<input type="submit" style="background-color:#828282; width:120px; color:#FFFFFF; border-color:#FFFAFA" value="削除">'."\n";
	$dsp_tbl .= '</form>'."\n";
	$dsp_tbl .= '</div>'."\n";

	//管理画面入力ページ表示関数...control/templ/basic.html
	PrintAdminPage("テンプレート商品編集",$dsp_tbl);
?>
