<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	$inp_status = $_REQUEST["inp_status"];
	$inp_category = $_REQUEST["inp_category"];
	$inp_auction = $_REQUEST["inp_auction"];
	$inp_regdate = $_REQUEST["inp_regdate"];
	$inp_recmmend = isset($_REQUEST["inp_recmmend"]) ? $_REQUEST["inp_recmmend"]:NULL;
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$pid = $_REQUEST["prod_id"];
	$sort = $_REQUEST["sort"];
	$disp_type = $_REQUEST["disp_type"];	//G.Chin 2010-07-16 add

        $dist_path = CSV_PRODUCT_PATH;
        
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	if($disp_type == 0)	//●商品一覧
	{
		//▼登録日
		if($inp_regdate != "")
		{
			//日付文字列作成関数
			MakeDateString($inp_regdate, $w_regdate);
		}
		else
		{
			$w_regdate = "";
		}
		
		//商品一覧取得関数
		GetTProductsList($inp_status,$inp_category,$inp_auction,$w_regdate,$inp_recmmend,$limit,$offset,$p_fk_products_id,$p_fk_item_category_id,$p_f_products_name,$p_f_start_time,$p_f_status,$p_f_photo1,$p_f_now_price,$at_f_auction_name,$p_all_count,$p_data_cnt,$pid,$sort);
                //▼表示開始～終了番号
		if($p_all_count == 0)
		{
			$start_num = 0;
		}
		else
		{
			$start_num = $offset + 1;
		}
		$end_num = $offset + $p_data_cnt;
		
		//■表示一覧
		$dsp_tbl  = "";
		
		//■ページ移行リンク
		$current_page = 1;
		$page_link_str = "";
		$page_count = ceil($p_all_count / $limit);
		if($page_count > 1)
		{
			for($i=0; $i<$page_count; $i++)
			{
				$page_offset[$i] = $limit * $i;
				$num = $i + 1;
				if($page_offset[$i] == $offset)
				{
					$page_link_str .= "$num</A> ";
					$current_page = $num;
				}
				else
				{
					$page_link_str .= "<A href='product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&offset=$page_offset[$i]&limit=$limit&sort=$sort&disp_type=$disp_type'>$num</A> ";
				}
			}
		}
		
		if($p_all_count > $limit)
		{
			$page_first = "<A href='product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&offset=0&limit=$limit&sort=$sort&disp_type=$disp_type'>";
			$page_first .= "|<";
			$page_first .= "</A> ";
			
			$max_page = $page_count - 1;
			$page_last  = "<A href='product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&offset=$page_offset[$max_page]&limit=$limit&sort=$sort&disp_type=$disp_type'>";
			$page_last .= ">|";
			$page_last .= "</A> ";
			
			$next_num = $p_all_count - ($offset + $limit);
			$next_page = $offset + $limit;
			if($next_num > 0)
			{
				$page_next  = "<A href='product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&offset=$next_page&limit=$limit&sort=$sort&disp_type=$disp_type'>";
				$page_next .= ">";
				$page_next .= "</A> ";
			}
			else
			{
				$page_next = "";
			}
			
			$before_page = $offset - $limit;
			if($offset > 0)
			{
				$page_before  = "<A href='product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&offset=$before_page&limit=$limit&sort=$sort&disp_type=$disp_type'>";
				$page_before .= "<";
				$page_before .= "</A> ";
			}
			else
			{
				$page_before = "";
			}
			
			$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
		}
		$url = "product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&limit=$limit&offset=&sort=$sort&disp_type=$disp_type";
		$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
		
		$dsp_tbl .= $pageNumber;
		
		$form_str = "";
		if($pageNumber != "")
		{
			$form_str .= "<span class='mypagenum_box4'>\n";
			$form_str .= "<form action='product_list.php' method='get' style='display: inline'>\n";
			$form_str .= "<input type='text' size='2' name='page'/>\n";
			$form_str .= "<input type='submit' value='Jump' class = 'button'>\n";
			$form_str .= "<input type='hidden' name='inp_status' value='$inp_status'>";
			$form_str .= "<input type='hidden' name='inp_category' value='$inp_category'>";
			$form_str .= "<input type='hidden' name='inp_auction' value='$inp_auction'>";
			$form_str .= "<input type='hidden' name='inp_regdate' value='$inp_regdate'>";
			$form_str .= "<input type='hidden' name='inp_recmmend' value='$inp_recmmend'>";
			$form_str .= "<input type='hidden' name='sort' value='$sort'>";
			$form_str .= "<input type='hidden' name='disp_type' value='$disp_type'>";
			$form_str .= "<input type='hidden' name='limit' value='$limit'>";
			$form_str .= "<input type='hidden' name='offset' value=''>";
			$form_str .= "</form>\n";
			$form_str .= "</span>\n";
		}
		$dsp_tbl .= $form_str;
		
		$dsp_tbl .= "<div style = 'margin-top:10px;'>\n";
		$dsp_tbl .= "<table class = 'data'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>データ件数</th>\n";
		$dsp_tbl .= "<td>$p_all_count</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "</table>\n";
		$dsp_tbl .= "</div><br>\n";

                $dsp_tbl .= "<p style = 'color:#555555;'>$start_num\n";
		$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
		$dsp_tbl .= "$end_num\n";
		$dsp_tbl .= "件目</p>\n";

		$dsp_tbl .= "<table class = 'list'>\n";
		$dsp_tbl .= "<tr style =  'height:40px;'>\n";
		$dsp_tbl .= "<th style = 'width:50px;'>商品ID</th>\n";
		$dsp_tbl .= "<th style = 'width:120px;'>ｶﾃｺﾞﾘ</th>\n";
		$dsp_tbl .= "<th style = 'width:120px;'>ｵｰｸｼｮﾝﾀｲﾌﾟ</th>\n";
		$dsp_tbl .= "<th style = 'width:80px;'>開催開始</th>\n";
		$dsp_tbl .= "<th style = 'width:250px;'>商品名</th>\n";
		$dsp_tbl .= "<th style = 'width:70px;'>状態</th>\n";
		$dsp_tbl .= "<th style = 'width:70px;'>写真</th>\n";
		$dsp_tbl .= "<th style = 'width:70px;'>現在価格</th>\n";
		$dsp_tbl .= "<th style = 'width:50px;'>操作</th>\n";
		
		$dsp_tbl .= "<th style = 'width:50px;'>履歴</th>\n";
		$dsp_tbl .= "<th style = 'width:50px;'>自動入札設定</th>\n";
		$dsp_tbl .= "</tr>\n";
		
		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$p_data_cnt;$i++)
		{
			//▼カテゴリ
			//商品カテゴリ名取得関数
			GetTItemCategoryName($p_fk_item_category_id[$i], $f_category_name);
			
			//▼開催開始時間
			$start_time_str = $p_f_start_time[$i];
			
			//▼状態
	                //追加開始 2010-06-27 shoji
			switch($p_f_status[$i])
			{
				case 0:		$status_str = "待機中";
	                                        //2010-06-27 start
	                                        $link_reg3 = "<P>-</P>";
	                                        //2010-06-27 end
	                                        break;
				case 1:		$status_str = "開催中";	
	                                        //2010-06-27 start
	                                        $link_reg3 = "<A href='product_autobit_list.php?pid=$p_fk_products_id[$i]&page=1' target='_blank'>確認</A>";
	                                        //2010-06-27 end
	                                        break;
				case 2:
	                        case 6:         $status_str = "終了";
	                                        //2010-06-27 start
	                                        $link_reg3 = "<P>-</P>";
	                                        //2010-06-27 end
	                                        break;
				default:	$status_str = "開催中";
	                                        //2010-06-27 start
	                                        $link_reg3 = "<A href='product_autobit_list.php?pid=$p_fk_products_id[$i]&page=1' target='_blank'>確認</A>";
	                                        //2010-06-27 end
	                                        break;
			}
			
			//▼写真
			if($p_f_photo1[$i] == "")
			{
				$picture_path_dsp = "";
			}
			else
			{
				$photo_path = SITE_URL."images/".$p_f_photo1[$i];
				$picture_path_dsp = "<img src='$photo_path' width=40 height=40>";
			}
			
			//テンプレート写真番号
			$tmppht_next = "";
			for($j=0; $j<5; $j++)
			{
				$tmppht_next .= "&tmppht[]=";
			}
			
			//▼登録・編集画面へのリンク
			$link_reg = "<A href='product_regist.php?pid=$p_fk_products_id[$i]&mode=&photo_num=&tmpid=$tmppht_next' target='_blank'>編集</A>";
			$link_reg2 = "<A href='product_bit_log.php?pid=$p_fk_products_id[$i]&page=1' target='_blank'>履歴</A>";
			$link_reg3 = "<A href='product_autobit_list.php?pid=$p_fk_products_id[$i]&page=1&type=1' target='_blank'>確認</A>";
			
			$dsp_tbl .= "<tr bgcolor='#FFFFFF' style = 'height:40px;'>\n";
			$dsp_tbl .= "<td align='center'>$p_fk_products_id[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$f_category_name</td>\n";
			$dsp_tbl .= "<td align='center'>$at_f_auction_name[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$start_time_str</td>\n";
			$dsp_tbl .= "<td align='center'>$p_f_products_name[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$status_str</td>\n";
			$dsp_tbl .= "<td align='center'>$picture_path_dsp</td>\n";
			$dsp_tbl .= "<td align='right'>".number_format($p_f_now_price[$i])." 円</td>\n";
			$dsp_tbl .= "<td align='center'>$link_reg</td>\n";
			
			$dsp_tbl .= "<td align='center'>$link_reg2</td>\n";
			$dsp_tbl .= "<td align='center'>$link_reg3</td>\n";
			
			$dsp_tbl .= "</tr>\n";
		}
		
		$dsp_tbl .= "</table><br>\n";
		
		//▼ページ移動リンク
		$dsp_tbl .= $pageNumber;
		$dsp_tbl .= $form_str;
		$dsp_tbl .= "<br>\n";
	}
        //●自動入札
	else if($disp_type == 1){
                //商品自動入札履歴全一覧取得関数
		GetProductsAutoBitAllList($limit,$offset,$tab_fk_auto_id,$tab_fk_member_id,$tab_fk_products_id,$tab_f_handle,$tab_login_id,$tab_f_min_price,$tab_f_max_price,$tab_f_free_coin,$tab_f_buy_coin,$tab_f_set_free_coin,$tab_f_set_buy_coin,$tab_f_status,$tab_data_cnt_auto,$tab_all_count);
		//▼表示開始～終了番号
		if($tab_all_count == 0)
		{
			$start_num = 0;
		}
		else
		{
			$start_num = $offset + 1;
		}
		$end_num = $offset + $tab_data_cnt_auto;
		
		//■ページ移行リンク
		$current_page = 1;
		$page_count = ceil($tab_all_count / $limit);
		if($page_count > 1)
		{
			for($i=0; $i<$page_count; $i++)
			{
				$page_offset[$i] = $limit * $i;
				$num = $i + 1;
				if($page_offset[$i] == $offset)
				{
					$current_page = $num;
				}
			}
		}
		
		//■自動入札
		$dsp_tbl  = "";
		
		$url = "product_list.php?inp_status=$inp_status&inp_category=$inp_category&inp_auction=$inp_auction&inp_regdate=$inp_regdate&inp_recmmend=$inp_recmmend&limit=$limit&offset=&sort=$sort&disp_type=$disp_type";
		$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
		
		$dsp_tbl .= $pageNumber;
		
		$form_str = "";
		if($pageNumber != "")
		{
			$form_str .= "<span class='mypagenum_box4'>\n";
			$form_str .= "<form action='product_list.php' method='get' style='display: inline'>\n";
			$form_str .= "<input type='text' size='2' name='page'/>\n";
			$form_str .= "<input type='submit' value='Jump' class = 'button'>\n";
			$form_str .= "<input type='hidden' name='inp_status' value='$inp_status'>";
			$form_str .= "<input type='hidden' name='inp_category' value='$inp_category'>";
			$form_str .= "<input type='hidden' name='inp_auction' value='$inp_auction'>";
			$form_str .= "<input type='hidden' name='inp_regdate' value='$inp_regdate'>";
			$form_str .= "<input type='hidden' name='inp_recmmend' value='$inp_recmmend'>";
			$form_str .= "<input type='hidden' name='sort' value='$sort'>";
			$form_str .= "<input type='hidden' name='disp_type' value='$disp_type'>";
			$form_str .= "<input type='hidden' name='limit' value='$limit'>";
			$form_str .= "<input type='hidden' name='offset' value=''>";
			$form_str .= "</form>\n";
			$form_str .= "</span>\n";
		}
		$dsp_tbl .= $form_str;
		$dsp_tbl .= "</center>\n";
		
		$dsp_tbl .= "<table class = 'list'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>データ件数</th>\n";
		$dsp_tbl .= "<td>$tab_all_count</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "</table>\n<br>\n";
		$dsp_tbl .= "$start_num\n";
		$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
		$dsp_tbl .= "$end_num\n";
		$dsp_tbl .= "件目\n";
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<table class = 'list'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>ID</th>\n";
		$dsp_tbl .= "<th>商品名</th>\n";
		$dsp_tbl .= "<th>写真</th>\n";
		$dsp_tbl .= "<th>設定者</th>\n";
		$dsp_tbl .= "<th>最小価格</th>\n";
		$dsp_tbl .= "<th>最大価格</th>\n";
		$dsp_tbl .= "<th>ﾌﾘｰｺｲﾝ(使用ｺｲﾝ)</th>\n";
		$dsp_tbl .= "<th>課金ｺｲﾝ(使用ｺｲﾝ)</th>\n";
		$dsp_tbl .= "<th>ﾌﾘｰｺｲﾝ(残ｺｲﾝ)</th>\n";
		$dsp_tbl .= "<th>課金ｺｲﾝ(残ｺｲﾝ)</th>\n";
		$dsp_tbl .= "<th>状態</th>\n";
		$dsp_tbl .= "</tr>\n";
		
		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$tab_data_cnt_auto;$i++)
		{
			//商品名取得関数
			GetTProductsName($tab_fk_products_id[$i],$tpm_f_products_name);
			
			//商品指定写真取得関数
			$photo_num = 1;
			GetTProductsSrcPhoto($tab_fk_products_id[$i],$photo_num,$tpm_f_photo,$tpm_f_photo_mb);
			
			//▼写真
			if($tpm_f_photo_mb == "")
			{
				$picture_path_dsp = "";
			}
			else
			{
				$photo_path = SITE_URL."images/mb/".$tpm_f_photo_mb;
				$picture_path_dsp = "<img src='$photo_path' width=50 height=40>";
			}

                        //▼設定者
			$name_str  = "";
			$name_str .= $tab_login_id[$i];
			$name_str .= "(";
			$name_str .= $tab_f_handle[$i];
			$name_str .= ")";
			
			//▼リンク
			$link_memlog = "<A href='../member/mem_log.php?limit=50&offset=0&page=&sid=$sid&mid=$tab_fk_member_id[$i]&ltype=1' target='_blank'>$name_str</A>";
			
			//▼状態
			switch($tab_f_status[$i])
			{
				case 0:
						$status_str = "実施中";
						break;
				case 1:
						$status_str = "終了";
						break;
				case 2:
						$status_str = "中止";
						break;
				case 3:
						$status_str = "ｺｲﾝ返却";
						break;
				default:
						$status_str = "無効";
						break;
			}
			
			$dsp_tbl .= "<tr bgcolor='#FFFFFF'>\n";
			$dsp_tbl .= "<td align='center'>$tab_fk_auto_id[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tpm_f_products_name</td>\n";
			$dsp_tbl .= "<td align='center'>$picture_path_dsp</td>\n";
			$dsp_tbl .= "<td align='center'>$link_memlog</td>\n";
			$dsp_tbl .= "<td align='center'>$tab_f_min_price[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tab_f_max_price[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tab_f_set_free_coin[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tab_f_set_buy_coin[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tab_f_free_coin[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$tab_f_buy_coin[$i]</td>\n";
			$dsp_tbl .= "<td align='center'>$status_str</td>\n";
			$dsp_tbl .= "</tr>\n";
		}
                
            }else if($disp_type == 2){

                GetTProductsListCSV($dist_path,$inp_status,$inp_category,$inp_auction,$inp_regdate,$inp_recmmend,$fk_products_id,$fk_item_category_id,$f_products_name,$f_start_time,$f_status,$f_now_price,$f_auction_name,$csv_all_count,$data_cnt,$pid,$sort,$csv_file_name);

                $dsp_tbl  = "";
                $dsp_tbl .= "<HR>\n";
                $dsp_tbl .= "<CENTER>生成ファイル一覧</CENTER>\n";
                $dsp_tbl .= "<HR>\n";
                $dsp_tbl .= "今までに生成されたファイルを表示します。<BR>\n";
                $dsp_tbl .= "ダウンロードしたいファイル名を右クリックして「対象をファイルに保存」でご使用のPCに保存して下さい。\n";
                $dsp_tbl .= "<HR>\n";

                $dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='3' cellpadding='3'>\n";
                $dsp_tbl .= "<tr>\n";
                $dsp_tbl .= "<th><font size='-1'>データ件数</font></th>\n";
                $dsp_tbl .= "<th><font color='#5D478B' size='-1'>$csv_all_count</font></th>\n";
                $dsp_tbl .= "</tr>\n";
                $dsp_tbl .= "</table>\n<br>\n";

                $FileDir = CSV_PRODUCT_PATH;
                $FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
                for($i=0; $FileList[$i]!=""; $i++) {
                    $filename = str_replace("$FileDir", "", $FileList[$i]);
                    $url = CSV_PRODUCT_URL.$filename;
                    $dsp_tbl .= "<A HREF='$url' target=_blank>$filename</A><BR>\n";
                }
                $dsp_tbl .= "<HR>\n";
            }else{
                //商品自動入札履歴CSV作成関数
                GetProductsAutoBitAllListCSV($dist_path,$tab_fk_auto_id,$tab_fk_member_id,$tab_fk_products_id,$tab_f_handle,$tab_login_id,$tab_f_min_price,$tab_f_max_price,$tab_f_free_coin,$tab_f_buy_coin,$tab_f_set_free_coin,$tab_f_set_buy_coin,$tab_f_status,$csv_file_name,$csv_all_count);
                $dsp_tbl  = "";

                $dsp_tbl .= "<HR>\n";
                $dsp_tbl .= "<CENTER>生成ファイル一覧</CENTER>\n";
                $dsp_tbl .= "<HR>\n";
                $dsp_tbl .= "今までに生成されたファイルを表示します。<BR>\n";
                $dsp_tbl .= "ダウンロードしたいファイル名を右クリックして「対象をファイルに保存」でご使用のPCに保存して下さい。\n";
                $dsp_tbl .= "<HR>\n";

                $dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='3' cellpadding='3'>\n";
                $dsp_tbl .= "<tr>\n";
                $dsp_tbl .= "<th><font size='-1'>データ件数</font></th>\n";
                $dsp_tbl .= "<th><font color='#5D478B' size='-1'>$csv_all_count</font></th>\n";
                $dsp_tbl .= "</tr>\n";
                $dsp_tbl .= "</table>\n<br>\n";

                $FileDir = CSV_PRODUCT_PATH;
                $FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
                for($i=0; $FileList[$i]!=""; $i++) {
                    $filename = str_replace("$FileDir", "", $FileList[$i]);
                    $url = CSV_PRODUCT_URL.$filename;
                    $dsp_tbl .= "<A HREF='$url' target=_blank>$filename</A><BR>\n";
                }
                $dsp_tbl .= "<HR>\n";
            }

	//管理画面入力ページ表示関数
	PrintAdminPage("商品管理",$dsp_tbl);
?>
