<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/07												*/
/*		修正日		:	2010/04/13 ｵｰｸｼｮﾝ時間(開始秒)追加						*/
/*		修正日		:	2010/04/18 ポイントオークション対応						*/
/*                              　 :       2010/04/24 落札者の声対応
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;


	$pid = $_REQUEST["pid"];
	$mode = $_REQUEST["mode"];
	$photo_num = $_REQUEST["photo_num"];
	$tmpid = $_REQUEST["tmpid"];
	$tmppht = $_REQUEST["tmppht"];
	
	//テンプレート写真番号
	$tmppht_cnt = count($tmppht);
	
	//写真削除モード
	if($mode == "dltphoto")
	{
		//商品写真削除関数
		DeleteTProductsPhoto($pid, $photo_num);
	}
	
	if(($pid != "") && ($pid != 0))
	{
		//商品情報取得関数
		GetTProductsInfo($pid,$tpm_f_products_name,$tpm_fk_item_category_id,$tpm_f_photo1,$tpm_f_photo2,$tpm_f_photo3,$tpm_f_photo4,$tpm_f_photo5,$tpm_f_photo1mb,$tpm_f_photo2mb,$tpm_f_photo3mb,$tpm_f_photo4mb,$tpm_f_photo5mb,$tpm_f_top_description_pc,$tpm_f_main_description_pc,$tpm_f_top_description_mb,$tpm_f_main_description_mb,$tpm_f_start_price,$tpm_f_min_price,$tpm_f_r_min_price,$tpm_f_r_max_price,$tpm_f_market_price,$tpm_fk_auction_type_id,$tpm_f_delflg,$tpm_f_memo,$tpm_f_target_hard,$tpm_f_recmmend,$tpm_f_regdate,$tp_f_bit_buy_count,$tp_f_bit_free_count,$tp_f_now_price,$tp_f_last_bidder,$tp_f_last_bidder_handle,$tp_f_auction_time,$tp_t_r_status,$tp_f_status,$tp_f_tm_stamp,$tp_f_start_time,$tpt_f_plus_coins,$tpt_f_address_flag,$tmp_fk_products_template_id,$tmp_initial_stop_time);
	}
	else
	{
		//現在日時
		$now = time();
		$tpm_f_products_name			= "";
		$tpm_fk_item_category_id		= "";
		$tpm_f_photo1					= "";
		$tpm_f_photo2					= "";
		$tpm_f_photo3					= "";
		$tpm_f_photo4					= "";
		$tpm_f_photo5					= "";
		$tpm_f_photo1mb					= "";
		$tpm_f_photo2mb					= "";
		$tpm_f_photo3mb					= "";
		$tpm_f_photo4mb					= "";
		$tpm_f_photo5mb					= "";
		$tpm_f_top_description_pc		= "";
		$tpm_f_main_description_pc		= "";
		$tpm_f_top_description_mb		= "";
		$tpm_f_main_description_mb		= "";
		$tpm_f_start_price				= "0";
		$tpm_f_min_price				= "0";
		$tpm_f_r_min_price				= "100";
		$tpm_f_r_max_price				= "99999999";
		$tpm_f_market_price				= "0";
		$tpm_fk_auction_type_id			= "";
		$tpm_f_delflg					= "";
		$tpm_f_memo						= "";
		$tpm_f_target_hard				= "";
		$tpm_f_recmmend					= "";
		$tpm_f_regdate					= "";
		$tp_f_bit_buy_count				= "";
		$tp_f_bit_free_count			= "";
		$tp_f_now_price					= "0";
		$tp_f_last_bidder				= "";
		$tp_f_last_bidder_handle		= "";
		$tp_f_auction_time				= "10800";
		$tp_t_r_status					= "";
		$tp_f_status					= "";
		$tp_f_tm_stamp					= "";
		$tp_f_start_time				= date("Y-m-d H:i:s", $now);
		$tpt_f_plus_coins				= 0;
		$tpt_f_address_flag				= 0;
		$tmp_fk_products_template_id	= 0;	//20100424 落札者の声対応
		$tmp_initial_stop_time			= date("Y-m-d H:i:s", $now + $tp_f_auction_time);
	}
	//テンプレート選択の場合
	if($tmpid != "")
	{
		//商品テンプレート取得関数
		GetTProductsTemplateInfo($tmpid,$tpm_f_products_name,$tpm_fk_item_category_id,$tpm_f_photo1,$tpm_f_photo2,$tpm_f_photo3,$tpm_f_photo4,$tpm_f_photo5,$tpm_f_photo1mb,$tpm_f_photo2mb,$tpm_f_photo3mb,$tpm_f_photo4mb,$tpm_f_photo5mb,$tpm_f_top_description_pc,$tpm_f_main_description_pc,$tpm_f_top_description_mb,$tpm_f_main_description_mb,$tpm_f_market_price,$tpm_f_min_price,$tpm_f_start_price,$tpm_f_r_min_price,$tpm_f_r_max_price,$tpm_f_status,$tpm_f_memo,$tpm_f_tm_stamp,$tpt_f_plus_coins,$tpt_f_address_flag);
		$tmp_fk_products_template_id = $tmpid;//20100424 落札者の声対応
	}
	
	//テンプレート写真削除モード
	if($mode == "dlttmpphoto")
	{
		//削除写真判定
		$num = $photo_num - 1;
		$tmppht[$num] = 1;
		if($tmppht[0] == 1)	$tpm_f_photo1 = "";
		if($tmppht[1] == 1)	$tpm_f_photo2 = "";
		if($tmppht[2] == 1)	$tpm_f_photo3 = "";
		if($tmppht[3] == 1)	$tpm_f_photo4 = "";
		if($tmppht[4] == 1)	$tpm_f_photo4 = "";
	}
	$tmppht_next = "";
	for($i=0; $i<$tmppht_cnt; $i++)
	{
		$tmppht_next .= "&tmppht[]=";
		$tmppht_next .= $tmppht[$i];
	}
	//▼状態
	if($pid !=0 )
	{
	$f_status_id[0] = "0";
	$f_status_name[0] = "待機中";
	$f_status_id[1] = "1";
	$f_status_name[1] = "開催中";
	$f_status_id[2] = "9";
	$f_status_name[2] = "削除";
	$f_status_cnt = 3;
	}
	else
	{
	$f_status_id[0] = "0";
        $f_status_name[0] = "待機中";
	$f_status_id[1] = "9";
        $f_status_name[1] = "削除";
        $f_status_cnt = 2;
	}
	$name = "f_status";
//G.Chin 2010-07-01 add sta
	if($tp_f_status == "")
	{
		$tp_f_status = 0;
	}
	else if($tp_f_status > 2)
	{
		$tp_f_status = 1;
	}
//G.Chin 2010-07-01 add end
	$select_num = $tp_f_status;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);
	
	//▼開始日時
	$s_year		= substr($tp_f_start_time, 0, 4);
	$s_month	= substr($tp_f_start_time, 5, 2);
	$s_day		= substr($tp_f_start_time, 8, 2);
	$s_hour		= substr($tp_f_start_time, 11, 2);
	$s_minutes	= substr($tp_f_start_time, 14, 2);
	
	//▼終了日時
	$s_year_stop	= substr($tmp_initial_stop_time, 0, 4);
	$s_mont_stop	= substr($tmp_initial_stop_time, 5, 2);
	$s_day_stop		= substr($tmp_initial_stop_time, 8, 2);
	$s_hour_stop	= substr($tmp_initial_stop_time, 11, 2);
	$s_minutes_stop	= substr($tmp_initial_stop_time, 14, 2);
	
	//年
	$start_time_year_id[0] = "2010";
	$start_time_year_name[0] = "2010年";
	$start_time_year_id[1] = "2011";
	$start_time_year_name[1] = "2011年";
	$start_time_year_id[2] = "2012";
	$start_time_year_name[2] = "2012年";
	$start_time_year_id[3] = "2013";
	$start_time_year_name[3] = "2013年";
	$start_time_year_cnt = 4;
	$name = "start_time_year";
	$select_num = $s_year;
        $name_stop = "stop_time_year";
	$select_num_stop = $s_year_stop;


	//選択オブジェクト作成関数
	MakeSelectObject($name, $start_time_year_id, $start_time_year_name, $start_time_year_cnt, $select_num, $start_time_year_select);
        MakeSelectObject($name_stop, $start_time_year_id, $start_time_year_name, $start_time_year_cnt, $select_num_stop, $stop_time_year_select);
	
	//月
	for($i=0; $i<12; $i++)
	{
		$num = $i + 1;
		if($num < 10)
		{
			$month_num = "0".$num;
			$month_str = "0".$num."月";
		}
		else
		{
			$month_num = $num;
			$month_str = $num."月";
		}
		$start_time_month_id[$i] = $month_num;
		$start_time_month_name[$i] = $month_str;
	}
	$start_time_month_cnt = 12;
	$name = "start_time_month";
        $name_stop = "stop_time_month";
	$select_num = $s_month;
        $select_num_stop = $s_mont_stop;

	//選択オブジェクト作成関数
	MakeSelectObject($name, $start_time_month_id, $start_time_month_name, $start_time_month_cnt, $select_num, $start_time_month_select);
        MakeSelectObject($name_stop, $start_time_month_id, $start_time_month_name, $start_time_month_cnt, $select_num_stop, $stop_time_month_select);
	
	//日
	for($i=0; $i<31; $i++)
	{
		$num = $i + 1;
		if($num < 10)
		{
			$day_num = "0".$num;
			$day_str = "0".$num."日";
		}
		else
		{
			$day_num = $num;
			$day_str = $num."日";
		}
		$start_time_day_id[$i] = $day_num;
		$start_time_day_name[$i] = $day_str;
	}
	$start_time_day_cnt = 31;
	$name = "start_time_day";
        $name_stop = "stop_time_day";
	$select_num = $s_day;
        $select_num_stop = $s_day_stop;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $start_time_day_id, $start_time_day_name, $start_time_day_cnt, $select_num, $start_time_day_select);
        MakeSelectObject($name_stop, $start_time_day_id, $start_time_day_name, $start_time_day_cnt, $select_num_stop, $stop_time_day_select);
	
	//時
	for($i=0; $i<24; $i++)
	{
		$num = $i;
		if($num < 10)
		{
			$hour_num = "0".$num;
			$hour_str = "0".$num."時";
		}
		else
		{
			$hour_num = $num;
			$hour_str = $num."時";
		}
		$start_time_hour_id[$i] = $hour_num;
		$start_time_hour_name[$i] = $hour_str;
	}
	$start_time_hour_cnt = 24;
	$name = "start_time_hour";
        $name_stop = "stop_time_hour";
	$select_num = $s_hour;
        $select_num_stop = $s_hour_stop;
	//選択オブジェクト作成関数
        MakeSelectObject($name, $start_time_hour_id, $start_time_hour_name, $start_time_hour_cnt, $select_num, $start_time_hour_select);
	MakeSelectObject($name_stop, $start_time_hour_id, $start_time_hour_name, $start_time_hour_cnt, $select_num_stop, $stop_time_hour_select);
	
	//分
	for($i=0; $i<6; $i++)
//        for($i=0; $i<60; $i++)
	{
		$num = $i;
		$start_time_minutes_id[$i]  = $num;
		$start_time_minutes_id[$i] .= "0";
		$start_time_minutes_name[$i]  = "";
		$start_time_minutes_name[$i] .= $num;
		$start_time_minutes_name[$i] .= "0";
                $start_time_minutes_name[$i] .= "分";
	}
	$start_time_minutes_cnt = 6;
//        $start_time_minutes_cnt = 60;
	$name = "start_time_minutes";
        $name_stop = "stop_time_minutes";
	$select_num = $s_minutes;
        $select_num_stop = $s_minutes_stop;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $start_time_minutes_id, $start_time_minutes_name, $start_time_minutes_cnt, $select_num, $start_time_minutes_select);
        MakeSelectObject($name_stop, $start_time_minutes_id, $start_time_minutes_name, $start_time_minutes_cnt, $select_num_stop, $stop_time_minutes_select);
	
	//▼オークション選択作成
	//オークション名一覧取得関数
	GetFAuctionNameList($fan_fk_auction_type_id, $fan_f_auction_name, $fan_data_cnt);
	$name = "fk_auction_type_id";
	$select_num = $tpm_fk_auction_type_id;
	//選択オブジェクト作成関数

        if($tpm_fk_item_category_id==COIN_CATEGORY)
        {
            MakeSelectObject($name, $fan_fk_auction_type_id, $fan_f_auction_name, $fan_data_cnt, $select_num, $auction_select,1);
        }
        else
        {
            MakeSelectObject($name, $fan_fk_auction_type_id, $fan_f_auction_name, $fan_data_cnt, $select_num, $auction_select);
        }
	//▼注目設定
	if($tpm_f_recmmend == 1)
	{
		$check_str = "checked";
	}
	else
	{
		$check_str = "";
	}
	
	//▼商品テンプレート選択作成
	//商品テンプレート名一覧取得関数
	GetFProductTemplateNameList($fk_products_template_id, $template_name, $fpt_data_cnt);
	$name = "tmpid";
	$select_num = 1;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $fk_products_template_id, $template_name, $fpt_data_cnt, $select_num, $template_select);
	
	//▼カテゴリ選択作成
	//商品カテゴリ一覧取得関数
	GetTItemCategoryList($tic_fk_item_category, $tic_f_category_name, $tic_f_mail_category_val, $tic_f_tm_stamp, $tic_data_cnt);
	$name = "fk_item_category_id";
	$select_num = $tpm_fk_item_category_id;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $tic_fk_item_category, $tic_f_category_name, $tic_data_cnt, $select_num, $category_select);
	
	//▼写真
	if($tpm_f_photo1 != "")
	{
		$f_photo_1_path = SITE_URL."images/".$tpm_f_photo1;
		$f_photo_1_dsp = "<img src='$f_photo_1_path'><br>";
		
		if($tmpid == "")
		{
			$link_dltphoto1 = "<A href='product_regist.php?pid=$pid&mode=dltphoto&photo_num=1&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
		else
		{
			$link_dltphoto1 = "<A href='product_regist.php?pid=$pid&mode=dlttmpphoto&photo_num=1&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
	}
	else
	{
		$f_photo_1_dsp = "";
		$link_dltphoto1 = "";
	}
	
	if($tpm_f_photo2 != "")
	{
		$f_photo_2_path = SITE_URL."images/".$tpm_f_photo2;
		$f_photo_2_dsp = "<img src='$f_photo_2_path'><br>";
		
		if($tmpid == "")
		{
			$link_dltphoto2 = "<A href='product_regist.php?pid=$pid&mode=dltphoto&photo_num=2&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
		else
		{
			$link_dltphoto2 = "<A href='product_regist.php?pid=$pid&mode=dlttmpphoto&photo_num=2&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
	}
	else
	{
		$f_photo_2_dsp = "";
		$link_dltphoto2 = "";
	}
	
	if($tpm_f_photo3 != "")
	{
		$f_photo_3_path = SITE_URL."images/".$tpm_f_photo3;
		$f_photo_3_dsp = "<img src='$f_photo_3_path'><br>";
		
		if($tmpid == "")
		{
			$link_dltphoto3 = "<A href='product_regist.php?pid=$pid&mode=dltphoto&photo_num=3&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
		else
		{
			$link_dltphoto3 = "<A href='product_regist.php?pid=$pid&mode=dlttmpphoto&photo_num=3&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
	}
	else
	{
		$f_photo_3_dsp = "";
		$link_dltphoto3 = "";
	}
	
	if($tpm_f_photo4 != "")
	{
		$f_photo_4_path = SITE_URL."images/".$tpm_f_photo4;
		$f_photo_4_dsp = "<img src='$f_photo_4_path'><br>";
		
		if($tmpid == "")
		{
			$link_dltphoto4 = "<A href='product_regist.php?pid=$pid&mode=dltphoto&photo_num=4&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
		else
		{
			$link_dltphoto4 = "<A href='product_regist.php?pid=$pid&mode=dlttmpphoto&photo_num=4&tmpid=$tmpid$tmppht_next'>削除</A>";
		}
	}
	else
	{
		$f_photo_4_dsp = "";
		$link_dltphoto4 = "";
	}
	
	
	
	$dsp_tbl  = "";
	// CKEditorインクルード
	$dsp_tbl  = '<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>'."\n";
	$dsp_tbl .= '<script type="text/javascript">'."\n";
	$dsp_tbl .= 'CKEDITOR.config.toolbar = ['."\n";
	$dsp_tbl .= "    ['Source','-','Undo','Redo'],"."\n";
	$dsp_tbl .= "    ['FontSize','Bold','Italic','Underline','Strike','Superscript'],"."\n";
	$dsp_tbl .= "    ['TextColor','BGColor'],"."\n";
	$dsp_tbl .= "    ['Outdent','Indent','HorizontalRule','Find'],"."\n";
	$dsp_tbl .= '];'."\n";
	$dsp_tbl .= "CKEDITOR.config.width  = '570px';"."\n";
	$dsp_tbl .= "CKEDITOR.config.height = '160px';"."\n";
//        $dsp_tbl .= "CKEDITOR.config.contentsCss = ['../../style/pc/default/layout.css','../../style/pc/default/character.css'];"."\n";
//	$dsp_tbl .= "CKEDITOR.config.resize_enabled = false;"."\n";
	$dsp_tbl .= '</script>'."\n";

        //FORM

	$dsp_tbl .= "<FORM action='product_regist.php' method='GET' ENCTYPE='multipart/form-data'>\n";

        //商品テンプレート選択
        $dsp_tbl .= '<fieldset>'."\n";
        $dsp_tbl .= '<legend>商品テンプレート</legend>'."\n";
        

        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	//$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 商品ﾃﾝﾌﾟﾚｰﾄ選択 </tt></th>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<font color='#B22222'><tt>$template_select</tt></font>\n";
	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='photo_num' value=''>\n";
	
	for($i=0; $i<$tmppht_cnt; $i++)
	{
		$dsp_tbl .= "<input type='hidden' name='tmppht[]' value='0'>\n";
	}
	
	if($tp_f_status != 1)
	{
		$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='選択'>\n";
	}
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</FORM>\n";
        $dsp_tbl .= '</fieldset>'."\n";


        //商品概要
        $dsp_tbl .= '<fieldset>'."\n";
        $dsp_tbl .= '<legend>商品概要</legend>'."\n";
        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 商品ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$pid</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";


	$dsp_tbl .= "<FORM onSubmit='return checkTime();' action='product_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 商品テンプレートID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$tmp_fk_products_template_id</font>\n";
	$dsp_tbl .= "<input type = 'hidden' name='template_id' value='$tmp_fk_products_template_id'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 状態 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$f_status_select</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 開催開始日時 </tt></th>\n";
	if($tp_f_status == 1)
	{
		$ws_year = substr($tp_f_start_time, 0, 4);
		$ws_month = substr($tp_f_start_time, 5, 2);
		$ws_day = substr($tp_f_start_time, 8, 2);
		$ws_hour = substr($tp_f_start_time, 11, 2);
		$ws_minutes = substr($tp_f_start_time, 14, 2);
		$start_time_str = $ws_year."年".$ws_month."月".$ws_day."日 ".$ws_hour."時".$ws_minutes."分";
		$dsp_tbl .= "<td colspan=3 id = 'shou_time'><font color='#B22222'><tt>$start_time_str</tt></font></td>\n";
		$dsp_tbl .= "<input type='hidden' name='start_time_year' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='start_time_month' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='start_time_day' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='start_time_hour' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='start_time_minutes' value=''>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3 id = 'shou_time'><font color='#B22222'><tt id='start_input'>$start_time_year_select $start_time_month_select $start_time_day_select $start_time_hour_select $start_time_minutes_select</tt></font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .='<script type="text/javascript" src="../../js/jquery.js"></script>';
        $dsp_tbl .= '<script type="text/javascript" src="../../js/ark.js"></script>';
	
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 終了予定日時 </tt></th>\n";
	if($tp_f_status == 1)
	{
		$ws_year_stop = substr($tmp_initial_stop_time, 0, 4);
		$ws_month_stop = substr($tmp_initial_stop_time, 5, 2);
		$ws_day_stop = substr($tmp_initial_stop_time, 8, 2);
		$ws_hour_stop = substr($tmp_initial_stop_time, 11, 2);
		$ws_minutes_stop = substr($tmp_initial_stop_time, 14, 2);
		$stop_time_str = $ws_year_stop."年".$ws_month_stop."月".$ws_day_stop."日 ".$ws_hour_stop."時".$ws_minutes_stop."分";
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$stop_time_str</tt>&nbsp;<tt>残り秒数".$tp_f_auction_time."秒</tt></font></td>\n";
		$dsp_tbl .= "<input type='hidden' name='stop_time_year' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='stop_time_month' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='stop_time_day' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='stop_time_hour' value=''>\n";
		$dsp_tbl .= "<input type='hidden' name='stop_time_minutes' value=''>\n";
                
        }	else
	{
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt id='end_input'>$stop_time_year_select $stop_time_month_select $stop_time_day_select $stop_time_hour_select $stop_time_minutes_select</tt>&nbsp;<input id='show_time' type='button' name='keisann' value='計算'><br>残り秒数<tt id='result_input'>".$tp_f_auction_time."</tt>秒</font></td>\n";
                $dsp_tbl .= "<td colspan=3><font color='#B22222'><tt id='end_input'>$stop_time_year_select $stop_time_month_select $stop_time_day_select $stop_time_hour_select $stop_time_minutes_select</tt>&nbsp;<input id='show_time' type='button' name='keisann' size='5' value='計算'><br><tt style='font-weight:bold'>(この日付は計算用です)</tt><br>残り秒数<tt id='result_input'><input type='text' style='text-align:right' name='f_auction_time' size='10' value='$tp_f_auction_time'  maxLength= '9 '></tt>秒</font></td>\n";
	}
	
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";



	//$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 開始秒 </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$tp_f_auction_time 秒</tt></font></td>\n";
//	}
///	else
//	{
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input id='result_input' type='text' name='f_nokori_time' value='$tp_f_auction_time'> 秒</font><input id='show_time' type='button' name='keisann' value='計算'></td>\n";
//              $dsp_tbl .= "<input type='hidden' name='f_auction_time' value=''>\n";
//	}
//        $dsp_tbl .= "<td colspan=3><font color='#B22222'><tt><input type='submit' name='keisann' value='計算'></tt></font></td>\n";
//        $dsp_tbl .= "<input type='hidden' name='shou_time' value=''>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> ｵｰｸｼｮﾝﾀｲﾌﾟ </tt></th>\n";
	if($tp_f_status == 1)
	{
		//オークション名単体取得関数
		GetFAuctionNameUnit($tpm_fk_auction_type_id, $f_auction_name);
		
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$f_auction_name</tt></font></td>\n";
		$dsp_tbl .= "<input type='hidden' name='fk_auction_type_id' value=''>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$auction_select</tt></font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 注目設定 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt><input type='checkbox' name='f_recmmend' value='1' $check_str>&nbsp;注目商品</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 商品名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_products_name' value='$tpm_f_products_name' style = 'width:570px;'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> カテゴリ </tt></th>\n";
	if($tp_f_status == 1)
	{
		//商品カテゴリ名取得関数
		GetTItemCategoryName($tpm_fk_item_category_id, $f_category_name);
		
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$f_category_name</tt></font></td>\n";
		$dsp_tbl .= "<input type='hidden' name='fk_item_category_id' value=''>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$category_select</tt></font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "</table>\n";
	$dsp_tbl .= '</fieldset>'."\n";


        //商品画像
        $dsp_tbl .= '<fieldset>'."\n";
        $dsp_tbl .= '<legend>商品画像</legend>'."\n";
        $dsp_tbl .= '<table class = "form">'."\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真① </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_1_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
	$dsp_tbl .= "<INPUT TYPE='file' NAME='file1' SIZE='25'>\n";
	$dsp_tbl .= $link_dltphoto1;
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真② </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_2_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
	$dsp_tbl .= "<INPUT TYPE='file' NAME='file2' SIZE='25'>\n";
	$dsp_tbl .= $link_dltphoto2;
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真③ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_3_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
	$dsp_tbl .= "<INPUT TYPE='file' NAME='file3' SIZE='25'>\n";
	$dsp_tbl .= $link_dltphoto3;
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真④ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_4_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
	$dsp_tbl .= "<INPUT TYPE='file' NAME='file4' SIZE='25'>\n";
	$dsp_tbl .= $link_dltphoto4;
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= '</table>'."\n";
        
        $dsp_tbl .= '<tr><th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th><td>
<span style="color:#ff0000">※&nbsp;画像ファイルは、半角の英数字のみにしてさい。
<br><th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>※&nbsp;jpgファイルのみ対応しています。</span></t></tr>'."\n";
        
        $dsp_tbl .= '</fieldset>'."\n";



	//商品詳細

        $dsp_tbl .= '<fieldset>'."\n";
        $dsp_tbl .= '<legend>商品詳細</legend>'."\n";
        $dsp_tbl .= '<table class = "form">'."\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> PC用(見出し) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3>HTMLタグをご利用の際は属性値の囲み文字にはシングルクオートを使用してください。<br><input type='text' name='f_top_description_pc' value='$tpm_f_top_description_pc' style = 'width:570px;'></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> PC用(詳細) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><textarea class = 'ckeditor' name='f_main_description_pc' cols='50' rows='5'>$tpm_f_main_description_pc</textarea></font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> MB用(見出し) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3>HTMLタグをご利用の際は属性値の囲み文字にはシングルクオートを使用してください。<br><input type='text' name='f_top_description_mb' value='$tpm_f_top_description_mb' style = 'width:570px;'></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> MB用(詳細) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><textarea name='f_main_description_mb' cols='50' rows='5' style = 'width:570px;'>$tpm_f_main_description_mb</textarea></font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= '</table>'."\n";
        $dsp_tbl .= '</fieldset>'."\n";


        //料金設定
        $dsp_tbl .= '<fieldset>'."\n";
        $dsp_tbl .= '<legend>料金設定</legend>'."\n";
        $dsp_tbl .= '<table class = "form">'."\n";
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=center colspan=4><tt><font color='#FF0000'><nobr>料金設定</font></tt></th>\n";
//	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=center>一般市場価格<span style = 'color:#FF809F;'>*</span></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_market_price' value='$tpm_f_market_price'> 円</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FF0000' align=center>開始価格<span style = 'color:#FF809F;'>*</span></font></th>\n";

	if(($pid != "") && ($pid != 0))
	{
		if($tp_f_status == 1)
		{
			$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$tpm_f_start_price 円</tt></font></td>\n";
			$dsp_tbl .= "<input type='hidden' name='f_start_price' value=''>\n";
		}
		else
		{
			$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_start_price' value='$tpm_f_start_price'> 円</tt></font></td>\n";
		}
	}
	else
	{
		if(0 != $tpm_f_start_price){
			$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_start_price' value='".$tpm_f_start_price."'> 円</tt></font></td>\n";
		}else{
			$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_start_price' value='".DEF_START_PRICE."'> 円</tt></font></td>\n";
		}
	}

	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FF0000' align=centert>仕入価格<span style = 'color:#FF809F;'>*</span></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_min_price' value='$tpm_f_min_price'> 円</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=center>増額時の最大価格<span style = 'color:#FF809F;'>*</span></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_r_max_price' value='$tpm_f_r_max_price'> 円</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=center>減額時の最小価格<span style = 'color:#FF809F;'>*</span></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_r_min_price' value='$tpm_f_r_min_price'> 円</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	//▼ポイントオークションでのポイント数
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=center>ポイントオークションでのポイント数<span style = 'color:#FF809F;'>*</span></th>\n";
	if($tp_f_status == 1)
	{
		$plus_coins_str = $tpt_f_plus_coins." ".POINT_NAME;
		
		$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$plus_coins_str</tt></font></td>\n";
		$dsp_tbl .= "<input type='hidden' name='f_plus_coins' value=''>\n";
	}
	else
	{
		$plus_coins_str = POINT_NAME;
		
		$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_plus_coins' value='$tpt_f_plus_coins'> $plus_coins_str</tt></font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	if($tpt_f_address_flag == 0)
	{
	    $selected0="selected";
	    $selected1="";
	}
	else if($tpt_f_address_flag == 1)
	{
	    $selected0="";
	    $selected1="selected";
	}
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=center>配送先設定<span style = 'color:#FF809F;'>*</span></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><SELECT name='f_address_flag'><OPTION value='0' ".$selected0.">配送先必要</OPTION><OPTION value='1' ".$selected1.">配送先不要</OPTION></SELECT></tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
	$dsp_tbl .= "<input type='hidden' name='hold_status' value='$tp_f_status'>\n";
	
	if($tmpid != "")
	{
		$dsp_tbl .= "<input type='hidden' name='tmpid' value='$tmpid'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo1' value='$tpm_f_photo1'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo2' value='$tpm_f_photo2'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo3' value='$tpm_f_photo3'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo4' value='$tpm_f_photo4'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo5' value='$tpm_f_photo5'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo1mb' value='$tpm_f_photo1mb'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo2mb' value='$tpm_f_photo2mb'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo3mb' value='$tpm_f_photo3mb'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo4mb' value='$tpm_f_photo4mb'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_photo5mb' value='$tpm_f_photo5mb'>\n";
	}
	
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        $dsp_tbl .= '</fieldset>'."\n";

        $dsp_tbl .= '<div style = "padding-bottom:5px; text-align:center;">'."\n";
        $dsp_tbl .= "<button type='submit' style='width:80px;'>更新</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='product_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
	$dsp_tbl .= "<input type='hidden' name='tmpid' value='$tmpid'>\n";
	$dsp_tbl .= "<button type='submit' style='width:80px;'>削除</button>\n";
	$dsp_tbl .= "</FORM>\n";
        $dsp_tbl .= '</div>'."\n";
        //管理画面入力ページ表示関数
	PrintAdminPage("商品編集",$dsp_tbl);


        

//      2010/10/19 backup
//        	$dsp_tbl  = "";
//
//	$dsp_tbl .= "<FORM action='product_regist.php' method='GET' ENCTYPE='multipart/form-data'>\n";
//	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 商品ﾃﾝﾌﾟﾚｰﾄ選択 </tt></th>\n";
//	$dsp_tbl .= "<td>\n";
//	$dsp_tbl .= "<font color='#B22222'><tt>$template_select</tt></font>\n";
//	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
//	$dsp_tbl .= "<input type='hidden' name='photo_num' value=''>\n";
//
//	for($i=0; $i<$tmppht_cnt; $i++)
//	{
//		$dsp_tbl .= "<input type='hidden' name='tmppht[]' value='0'>\n";
//	}
//
//	if($tp_f_status != 1)
//	{
//		$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='選択'>\n";
//	}
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
//	$dsp_tbl .= "</FORM>\n";
//
//	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 商品ID </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$pid</font>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<FORM onSubmit='return checkTime();' action='product_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 商品テンプレートID </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$tmp_fk_products_template_id</font>\n";
//	$dsp_tbl .= "<input type = 'hidden' name='template_id' value='$tmp_fk_products_template_id'>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 状態 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$f_status_select</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 開催開始日時 </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		$ws_year = substr($tp_f_start_time, 0, 4);
//		$ws_month = substr($tp_f_start_time, 5, 2);
//		$ws_day = substr($tp_f_start_time, 8, 2);
//		$ws_hour = substr($tp_f_start_time, 11, 2);
//		$ws_minutes = substr($tp_f_start_time, 14, 2);
//		$start_time_str = $ws_year."年".$ws_month."月".$ws_day."日 ".$ws_hour."時".$ws_minutes."分";
//		$dsp_tbl .= "<td colspan=3 id = 'shou_time'><font color='#B22222'><tt>$start_time_str</tt></font></td>\n";
//		$dsp_tbl .= "<input type='hidden' name='start_time_year' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='start_time_month' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='start_time_day' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='start_time_hour' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='start_time_minutes' value=''>\n";
//	}
//	else
//	{
//		$dsp_tbl .= "<td colspan=3 id = 'shou_time'><font color='#B22222'><tt id='start_input'>$start_time_year_select $start_time_month_select $start_time_day_select $start_time_hour_select $start_time_minutes_select</tt></font></td>\n";
//	}
//	$dsp_tbl .= "</tr>\n";
//        $dsp_tbl .='<script type="text/javascript" src="../../js/jquery.js"></script>';
//        $dsp_tbl .= '<script type="text/javascript" src="../../js/ark.js"></script>';
//
//        $dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 終了予定日時 </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		$ws_year_stop = substr($tmp_initial_stop_time, 0, 4);
//		$ws_month_stop = substr($tmp_initial_stop_time, 5, 2);
//		$ws_day_stop = substr($tmp_initial_stop_time, 8, 2);
//		$ws_hour_stop = substr($tmp_initial_stop_time, 11, 2);
//		$ws_minutes_stop = substr($tmp_initial_stop_time, 14, 2);
//		$stop_time_str = $ws_year_stop."年".$ws_month_stop."月".$ws_day_stop."日 ".$ws_hour_stop."時".$ws_minutes_stop."分";
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$stop_time_str</tt>&nbsp;<tt>残り秒数".$tp_f_auction_time."秒</tt></font></td>\n";
//		$dsp_tbl .= "<input type='hidden' name='stop_time_year' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='stop_time_month' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='stop_time_day' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='stop_time_hour' value=''>\n";
//		$dsp_tbl .= "<input type='hidden' name='stop_time_minutes' value=''>\n";
//
//        }	else
//	{
////		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt id='end_input'>$stop_time_year_select $stop_time_month_select $stop_time_day_select $stop_time_hour_select $stop_time_minutes_select</tt>&nbsp;<input id='show_time' type='button' name='keisann' value='計算'><br>残り秒数<tt id='result_input'>".$tp_f_auction_time."</tt>秒</font></td>\n";
//                $dsp_tbl .= "<td colspan=3><font color='#B22222'><tt id='end_input'>$stop_time_year_select $stop_time_month_select $stop_time_day_select $stop_time_hour_select $stop_time_minutes_select</tt>&nbsp;<input id='show_time' type='button' name='keisann' size='5' value='計算'><br><tt style='font-weight:bold'>(この日付は計算用です)</tt><br>残り秒数<tt id='result_input'><input type='text' style='text-align:right' name='f_auction_time' size='10' value='$tp_f_auction_time'  maxLength= '9 '></tt>秒</font></td>\n";
//	}
//
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//
//
//
//	//$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> 開始秒 </tt></th>\n";
////	if($tp_f_status == 1)
////	{
////		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$tp_f_auction_time 秒</tt></font></td>\n";
////	}
/////	else
////	{
////		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input id='result_input' type='text' name='f_nokori_time' value='$tp_f_auction_time'> 秒</font><input id='show_time' type='button' name='keisann' value='計算'></td>\n";
////              $dsp_tbl .= "<input type='hidden' name='f_auction_time' value=''>\n";
////	}
////        $dsp_tbl .= "<td colspan=3><font color='#B22222'><tt><input type='submit' name='keisann' value='計算'></tt></font></td>\n";
////        $dsp_tbl .= "<input type='hidden' name='shou_time' value=''>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt><nobr> ｵｰｸｼｮﾝﾀｲﾌﾟ </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		//オークション名単体取得関数
//		GetFAuctionNameUnit($tpm_fk_auction_type_id, $f_auction_name);
//
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$f_auction_name</tt></font></td>\n";
//		$dsp_tbl .= "<input type='hidden' name='fk_auction_type_id' value=''>\n";
//	}
//	else
//	{
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$auction_select</tt></font></td>\n";
//	}
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 注目設定 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt><input type='checkbox' name='f_recmmend' value='1' $check_str>&nbsp;注目商品</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 商品名 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_products_name' value='$tpm_f_products_name' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> カテゴリ </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		//商品カテゴリ名取得関数
//		GetTItemCategoryName($tpm_fk_item_category_id, $f_category_name);
//
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$f_category_name</tt></font></td>\n";
//		$dsp_tbl .= "<input type='hidden' name='fk_item_category_id' value=''>\n";
//	}
//	else
//	{
//		$dsp_tbl .= "<td colspan=3><font color='#B22222'><tt>$category_select</tt></font></td>\n";
//	}
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真① </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_1_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file1' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto1;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真② </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_2_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file2' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto2;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真③ </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_3_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file3' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto3;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> 写真④ </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_4_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file4' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto4;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> PC用(見出し) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_top_description_pc' value='$tpm_f_top_description_pc' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> PC用(詳細) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><textarea name='f_main_description_pc' cols='50' rows='5'>$tpm_f_main_description_pc</textarea></font>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> MB用(見出し) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_top_description_mb' value='$tpm_f_top_description_mb' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt><nobr> MB用(詳細) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><textarea name='f_main_description_mb' cols='50' rows='5'>$tpm_f_main_description_mb</textarea></font>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=center colspan=4><tt><font color='#FF0000'><nobr>料金設定</font></tt></th>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'><nobr>一般市場価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_market_price' value='$tpm_f_market_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FF0000' align=right><tt> <font color='#FFFFFF'><nobr>開始価格</font> </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$tpm_f_start_price 円</tt></font></td>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_start_price' value=''>\n";
//	}
//	else
//	{
//		$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_start_price' value='".DEF_START_PRICE."'> 円</tt></font></td>\n";
//	}
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FF0000' align=right><tt> <font color='#FFFFFF'><nobr>仕入価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_min_price' value='$tpm_f_min_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'><nobr>増額時の最大価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_r_max_price' value='$tpm_f_r_max_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'><nobr>減額時の最小価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_r_min_price' value='$tpm_f_r_min_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	//▼ポイントオークションでのポイント数
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'><nobr>ﾎﾟｲﾝﾄｵｰｸｼｮﾝでのﾎﾟｲﾝﾄ数</font> </tt></th>\n";
//	if($tp_f_status == 1)
//	{
//		$plus_coins_str = $tpt_f_plus_coins." ".POINT_NAME;
//
//		$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$plus_coins_str</tt></font></td>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_plus_coins' value=''>\n";
//	}
//	else
//	{
//		$plus_coins_str = POINT_NAME;
//
//		$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_plus_coins' value='$tpt_f_plus_coins'> $plus_coins_str</tt></font></td>\n";
//	}
//	$dsp_tbl .= "</tr>\n";
//
//	if($tpt_f_address_flag == 0)
//	{
//	    $selected0="selected";
//	    $selected1="";
//	}
//	else if($tpt_f_address_flag == 1)
//	{
//	    $selected0="";
//	    $selected1="selected";
//	}
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'><nobr>配送先設定</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><SELECT name='f_address_flag'><OPTION value='0' ".$selected0.">配送先必要</OPTION><OPTION value='1' ".$selected1.">配送先不要</OPTION></SELECT></tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='hold_status' value='$tp_f_status'>\n";
//
//	if($tmpid != "")
//	{
//		$dsp_tbl .= "<input type='hidden' name='tmpid' value='$tmpid'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo1' value='$tpm_f_photo1'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo2' value='$tpm_f_photo2'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo3' value='$tpm_f_photo3'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo4' value='$tpm_f_photo4'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo5' value='$tpm_f_photo5'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo1mb' value='$tpm_f_photo1mb'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo2mb' value='$tpm_f_photo2mb'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo3mb' value='$tpm_f_photo3mb'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo4mb' value='$tpm_f_photo4mb'>\n";
//		$dsp_tbl .= "<input type='hidden' name='f_photo5mb' value='$tpm_f_photo5mb'>\n";
//	}
//
//	$dsp_tbl .= "<td colspan=4 align=center>\n";
//	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='更新'>\n";
//	$dsp_tbl .= "</FORM>\n";
//	$dsp_tbl .= "<FORM action='product_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
//	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='tmpid' value='$tmpid'>\n";
//	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='削除'>\n";
//	$dsp_tbl .= "</FORM>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
//
//	//管理画面入力ページ表示関数
//	PrintAdminPage("商品編集",$dsp_tbl);

?>
