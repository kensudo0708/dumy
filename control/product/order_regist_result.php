 <?php
    //☆★	ライブラリ読込み	★☆
    include "../../lib/define.php";
    $all_include_path = COMMON_LIB."all_include_lib.php";
    include $all_include_path;
    include "../../classes/utils/MailReserver.class.php";

    //---------------------------------------------------------------------------------
    //▼画像
    //---------------------------------------------------------------------------------

    $pid = $_REQUEST["pid"];
    $f_status = $_REQUEST["f_status"];
    $f_stock = $_REQUEST["f_stock"];
    $f_pickup_flg = $_REQUEST["f_pickup_flg"];
    $f_memo = $_REQUEST["f_memo"];
    $cost = $_REQUEST["cost"];
    $mem_id = $_REQUEST["mem_id"];
    $sid = $_REQUEST["sid"];

    $hold_status = $_REQUEST["hold_status"];
    $f_send_mail = !empty ($_REQUEST["f_send_mail"])?$_REQUEST["f_send_mail"]:'';
    $f_send_mail_id = !empty($_REQUEST["f_send_mail_id"])?$_REQUEST["f_send_mail_id"]:'';
    $f_address_flag = $_REQUEST["address_flag"];

    if($pid == "") {
        $dsp_string = "不正な処理です。<br>\n";
    }
    else {
        //▼ピックアップ商品は削除不可
        if(($f_pickup_flg == 1) && ($f_status == 9)) {
            $dsp_string = "ピックアップ商品は削除できません。<br>\n";
        }
        else {
            if(($f_status == 4) && ($hold_status != 4)) {
                $ret = SetCostPay($pid,$sid,$mem_id,$cost);
            }
            else if(($f_status == 5) && ($hold_status != 3)) {
                $ret = 4;
            }
            else if($f_status == 9) {
                DeleteTProductsData($pid);
            }

            $dsp_string="";
            //オークション終了商品状態価格更新関数
            if($ret == 0 || $ret == 3) {
                if($ret == 3) {
                    $dsp_string .= "出荷データが存在します。仕入額二重計上防止のため状態だけを変更します。<br>\n";
                }
                $ret = SetTEndProductsStatusPrice($pid, $f_status, $f_stock, $f_pickup_flg, $cost);
            }

            if($ret == 1) {
                $dsp_string .= "更新処理に失敗しました。<br>\n";
            }
            else if($ret == 2) {
                $dsp_string .= "決済が完了していないため更新処理に失敗しました。<br>終わらせたい場合は削除にしてください\n";
            }
            else if($ret == 4) {
                $dsp_string .= "発送先が設定されていません。<BR>設定されてから発注してください\n";
            }
            else {
                //オークション終了商品メモ更新関数
                $ret = UpdateTEndProductsMemo($pid, $f_memo);
                if($ret == false) {
                    $dsp_string .= "商品メモの更新処理に失敗しました。<br>\n";
                }
                else {
                    $dsp_string .= "発注・出荷情報の更新処理に成功しました。<br>\n";
                    if(($f_address_flag !='' && $f_address_flag==0)  && ($f_send_mail==1 && $f_send_mail !='')) {
                        $context=substr($_SERVER["SCRIPT_NAME"],0,strrpos($_SERVER["SCRIPT_NAME"],"/"));
                        $DEFAULT_FILE=str_replace($context."/", "", $_SERVER["SCRIPT_NAME"]);
                        $HTTP_HOST=$_SERVER["HTTP_HOST"];
                        $f_start_price='';
                        $f_end_price='';
                        $f_market_price='';

                        getMemberById($mem_id,$mem_handle,$f_login_id,$f_mail_address_pc,$f_mail_address_mb,$rows);
                        GetTEndProductsPrice($pid,$f_start_price,$f_end_price,$f_market_price);
                        GetTEndProductsName($pid,$f_products_name);
                        for($i=0; $i<$rows; $i++) {
                            //配送済みメールを送信処理
                            if((!empty ($f_mail_address_pc[$i]) && $f_mail_address_pc[$i] != NULL) or (!empty ($f_mail_address_mb[$i]) && $f_mail_address_mb[$i] != NULL)) {
                                $to=!empty($f_mail_address_pc[$i])?$f_mail_address_pc[$i]:$f_mail_address_mb[$i];
                                if ($f_mail_address_pc[$i]!='') {
                                    $tpl='/home/auction/templates/pc/parts/mail_delivery.html';
                                }else {
                                    $tpl='/home/auction/templates/mobile/parts/mail_delivery.html';
                                }

                                //                                                $from = INFO_MAIL;
                                $from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
                                $params=array("member_name"=>$mem_handle[$i],"site_name"=>SITE_NAME,
                                        "login_id"=>$f_login_id[$i],"date"=>date('Y年m月d日'),
                                        "time"=>date('G時i分'),"price"=>$f_end_price,"product_name"=>$f_products_name,
                                        "point_name"=>POINT_NAME,"carriage"=>CARRIAGE,"toiawase_code"=>$f_send_mail_id,
                                        "question_url"=>"http://".$HTTP_HOST."/index.php/guide/contact",
                                        "url_home"=>"http://".$HTTP_HOST."/index.php");

                                $handle=fopen($tpl, "r");
                                $body=fread($handle, filesize($tpl));
                                fclose($handle);
                                if(is_array($params)) {
                                    foreach ($params as $key=>$value) {
                                        if(stripos($body,$key)!==FALSE)
                                            $body=str_ireplace("{%".$key."%}", $value, $body);
                                          $tpl = $body;
                                    }
                                }

                                $ret = DeliveryMailRequest(70,DELIVERY_MAIL_SUBJECT, $tpl,$from, $to,$pid,$f_send_mail,$f_send_mail_id);
                                if($ret == false) {
                                    $dsp_string .= "<div id='div1' style=' color:#C84B00; '>　　※配送お知らせメール送信が失敗しました</div><br>\n";

                                }
                                else {
                                    $dsp_string .= "<div id='div1' style=' color:#C84B00; '>　　※配送お知らせメール送信を行いました</div><br>\n";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    $dsp_tbl  = "";
    $dsp_tbl .= "<br><br>\n";
    $dsp_tbl .= "<b>$dsp_string</b>\n";
    $dsp_tbl .= "<br><br>\n";
    $dsp_tbl .= "下の閉じるﾎﾞﾀﾝを押して下さい。\n";
    $dsp_tbl .= "<br>\n";
    $dsp_tbl .= "<form>\n";
    $dsp_tbl .= "<button type='button' onclick='window.close()' style=\"width:110px\">閉じる</button>\n";
    $dsp_tbl .= "</form>\n";

    //管理画面入力ページ表示関数
    PrintAdminPage("発注・出荷編集完了",$dsp_tbl);
?>

