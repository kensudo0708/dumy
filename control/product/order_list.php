<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	$inp_status = $_REQUEST["inp_status"];
	$inp_stock = $_REQUEST["inp_stock"];
	$inp_pickup = $_REQUEST["inp_pickup"];
	$inp_group = $_REQUEST["inp_group"];	//G.Chin 2010-07-14 add
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$pid = $_REQUEST["end_prod_id"];
	$sort = $_REQUEST["sort"];
	
	if ($limit == "" || $limit ==0)
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//▼会員グループ選択判定
	if($inp_group == "")
	{
		//オークション終了商品一覧取得関数
		GetTEndProductsList($inp_status,$inp_stock,$inp_pickup,$limit,$offset,$tep_fk_end_products_id,$tep_f_products_name,$tep_f_photo1,$tep_f_status,$tep_f_stock,$tep_f_end_price,$tep_f_bit_buy_count,$tep_f_bit_free_count,$tep_f_bit_r_count,$tep_f_last_bidder,$f_end_time,$tep_all_count,$tep_data_cnt,$pid,$sort);
	}
	else
	{
		//オークション終了商品会員グループ指定一覧取得関数
		GetTEndProductsSrcMemGroupList($inp_status,$inp_stock,$inp_pickup,$inp_group,$limit,$offset,$tep_fk_end_products_id,$tep_f_products_name,$tep_f_photo1,$tep_f_status,$tep_f_stock,$tep_f_end_price,$tep_f_bit_buy_count,$tep_f_bit_free_count,$tep_f_bit_r_count,$tep_f_last_bidder,$f_end_time,$tep_all_count,$tep_data_cnt,$pid,$sort);
	}
	
	//▼表示開始～終了番号
	if($tep_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tep_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";

	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tep_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='order_list.php?inp_status=$inp_status&inp_stock=$inp_stock&inp_pickup=$inp_pickup&inp_group=$inp_group&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
			}
		}
	}
	
	if($tep_all_count > $limit)
	{
		$page_first = "<A href='order_list.php?inp_status=$inp_status&inp_stock=$inp_stock&inp_pickup=$inp_pickup&inp_group=$inp_group&offset=0&limit=$limit'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='order_list.php?inp_status=$inp_status&inp_stock=$inp_stock&inp_pickup=$inp_pickup&inp_group=$inp_group&offset=$page_offset[$max_page]&limit=$limit'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tep_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='order_list.php?inp_status=$inp_status&inp_stock=$inp_stock&inp_pickup=$inp_pickup&inp_group=$inp_group&offset=$next_page&limit=$limit'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='order_list.php?inp_status=$inp_status&inp_stock=$inp_stock&inp_pickup=$inp_pickup&inp_group=$inp_group&offset=$before_page&limit=$limit'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
	$url = "order_list.php?inp_status=$inp_status&inp_stock=$inp_stock&inp_pickup=$inp_pickup&inp_group=$inp_group&limit=$limit&offset=&sort=$sort";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= "<left>\n";
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='order_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump' class = 'button'>\n";
		$form_str .= "<input type='hidden' name='inp_status' value='$inp_status'>";
		$form_str .= "<input type='hidden' name='inp_stock' value='$inp_stock'>";
		$form_str .= "<input type='hidden' name='inp_pickup' value='$inp_pickup'>";
		$form_str .= "<input type='hidden' name='inp_group' value='$inp_group'>";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "</left>\n";

	$dsp_tbl .= "<div style = 'text-align:left; margin-top:10px;'>\n";
        $dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$tep_all_count</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table></div>\n<br>\n";
	$dsp_tbl .="<P>※『削除済み』:退会又は強制退会により削除されたユーザです</P>";

	$dsp_tbl .= "<p style = 'color:#555555;'>$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目</p>\n";
        
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:40px;'>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>商品ID</th>\n";
	$dsp_tbl .= "<th style = 'width:250px;'>商品名</th>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>写真</th>\n";
	$dsp_tbl .= "<th style = 'width:150px;'>状態</th>\n";
	$dsp_tbl .= "<th style = 'width:100px;'>在庫・発注</th>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>落札者</th>\n";
	$dsp_tbl .= "<th style = 'width:100px;'><tt>落札価格</tt></th>\n";
	$dsp_tbl .= "<th style = 'width:150px;'><tt>ｺｲﾝ(購入/ｻｰﾋﾞｽ/支援)</tt></th>\n";
	$dsp_tbl .= "<th style = 'width:60px;'><tt>操作</tt></th>\n";
	$dsp_tbl .= "<th style = 'width:80px;'><tt>終了日時</tt></th>\n";
	
	$dsp_tbl .= "<th style = 'width:60px;'><tt>履歴</tt></th>\n";
        $dsp_tbl .= "<th style = 'width:60px;'><tt>自動入札履歴</tt></th>\n";
	
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tep_data_cnt;$i++)
	{
		$plus_coin = "";
		
		//▼終了状態
		switch($tep_f_status[$i])
		{
			case 0:
					$status_str = "落札のみ";
					$bgcolor = "#FFFFFF";
					break;
			case 1:
					$status_str = "購入拒否ｷｬﾝｾﾙ";
					$bgcolor = "#FFFFFF";
					break;
			case 2:
					$status_str = "支払済み";
					$bgcolor = "#FFFFFF";
					break;
			case 3:
					$status_str = "配送先決定";
					$bgcolor = "#FFC8C8";
					break;
                        case 5:
					$status_str = "発注済";
					$bgcolor = "#00F200";
					break;
			case 4:
					$status_str = "配送済み(完了)";
					$bgcolor = "#C0C0C0";
					break;
			case 9:
					$status_str = "削除";
					$bgcolor = "#FFFFFF";
					break;
			default:
					$status_str = "";
					$bgcolor = "#FFFFFF";
					break;
		}
		
		//▼在庫・発注状態
		switch($tep_f_stock[$i])
		{
			case 0:		$stock_str = "";			break;
			case 1:		$stock_str = "在庫あり";	break;
			case 2:		$stock_str = "発注済み";	break;
			default:	$stock_str = "";			break;
		}
		
		//▼写真
		if($tep_f_photo1[$i] == "")
		{
			$picture_path_dsp = "";
		}
		else
		{
			$photo_path = SITE_URL."images/".$tep_f_photo1[$i];
			$picture_path_dsp = "<img src='$photo_path' width=50 height=40>";
		}
		
		//▼落札者
		//会員ログインIDハンドル取得関数
		GetTMemberLoginIdHandle($tep_f_last_bidder[$i], $f_login_id, $f_handle);
		if($f_handle !="")
		{
			$member_str  = "";
			$member_str .= $f_login_id;
			$member_str .= "(";
			$member_str .= $f_handle;
			$member_str .= ")";
			$mem_edit_path = "/control/member/mem_regist.php?sid=$sid&mid=$tep_f_last_bidder[$i]";
			$link_mem_edit = "<A href='$mem_edit_path' target='_blank'>$member_str</A>";
		}
		else
		{
			 $link_mem_edit ="削除済み";
		}
		//▼コイン文字列
		$coin_str  = "";
		$coin_str .= $tep_f_bit_buy_count[$i];
		$coin_str .= "/";
		$coin_str .= $tep_f_bit_free_count[$i];
		$coin_str .= "/";
                $coin_str .=$tep_f_bit_r_count[$i];
		//▼登録・編集画面へのリンク
		$link_reg = "<A href='order_regist.php?pid=$tep_fk_end_products_id[$i]&sid=$sid' target='_blank'>編集</A>";
		
		$link_reg2 = "<A href='product_bit_log.php?pid=$tep_fk_end_products_id[$i]&page=1' target='_blank'>履歴</A>";
                $link_reg3 = "<A href='product_autobit_list.php?pid=$tep_fk_end_products_id[$i]&page=1&type=2' target='_blank'>確認</A>";
		
		$dsp_tbl .= "<tr bgcolor='$bgcolor' style = 'height:40px;'>\n";
		$dsp_tbl .= "<td>$tep_fk_end_products_id[$i]</td>\n";
		$dsp_tbl .= "<td>$tep_f_products_name[$i]</td>\n";
		$dsp_tbl .= "<td>$picture_path_dsp</td>\n";
		$dsp_tbl .= "<td>$status_str</td>\n";
		$dsp_tbl .= "<td>$stock_str</td>\n";
		$dsp_tbl .= "<td>$link_mem_edit</td>\n";
		$dsp_tbl .= "<td>$tep_f_end_price[$i] 円</td>\n";
		$dsp_tbl .= "<td>$coin_str</td>\n";
		$dsp_tbl .= "<td>$link_reg</td>\n";
		$dsp_tbl .= "<td>$f_end_time[$i]</td>\n";
		
		$dsp_tbl .= "<td>$link_reg2</td>\n";
		$dsp_tbl .= "<td>$link_reg3</td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	
	//管理画面入力ページ表示関数
	PrintAdminPage("発注・出荷管理",$dsp_tbl);
?>
