<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	//▼会員グループ選択作成
	//会員グループ名一覧取得関数
	GetFMGroupNameList($fmg_fk_member_group_id, $fmg_f_member_group_name, $fmg_data_cnt);
	$inp_group_id[0] = "";
	$inp_group_name[0] = "(指定しない)";
	for($i=0; $i<$fmg_data_cnt; $i++)
	{
		$inp_group_id[$i+1] = $fmg_fk_member_group_id[$i];
		$inp_group_name[$i+1] = $fmg_f_member_group_name[$i];
	}
	$inp_group_cnt = $fmg_data_cnt + 1;
	$name = "inp_group";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_group_id, $inp_group_name, $inp_group_cnt, $select_num, $group_select);
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='order_list.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<table class='main_l'>\n";
	$dsp_tbl .= "<caption>④発注・出荷管理</caption>\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>商品ID</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center'><tt><input type = 'text' name='end_prod_id' istyle='4'></tt></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>状態</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_status'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>落札のみ\n";
	$dsp_tbl .= "<OPTION value='1'>購入拒否ｷｬﾝｾﾙ\n";
	$dsp_tbl .= "<OPTION value='2'>支払済み\n";
	$dsp_tbl .= "<OPTION value='3'>配送先決定\n";
        $dsp_tbl .= "<OPTION value='5'>発注済\n";
	$dsp_tbl .= "<OPTION value='4'>完了(配送済み)\n";
	$dsp_tbl .= "<OPTION value='9'>削除\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>在庫・発注状態</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_stock'>\n";
	$dsp_tbl .= "<OPTION value='0'>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='1'>在庫あり\n";
	$dsp_tbl .= "<OPTION value='2'>発注済み\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>会員ｸﾞﾙｰﾌﾟ(最終入札者)</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$group_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>その他</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='checkbox' name='inp_pickup' value='1'>&nbsp;ﾋﾟｯｸｱｯﾌﾟ商品\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	
	$dsp_tbl .= "<td align='left'><tt><input type='radio' name='sort' value='0' checked>終了時間(降順)</input><BR>";
	$dsp_tbl .= "<input type='radio' name='sort' value='1' >商品ID(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='2' >商品ID(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='3' >商品名(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='4' >商品名(降順)</input><BR></tt></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);
?>
