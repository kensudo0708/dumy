<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	$inp_category = $_REQUEST["inp_category"];
	$inp_name = $_REQUEST["inp_name"];	//G.Chin AREQ-437 2010-12-01 add
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];
	
	if ($limit == "")
	{
		$limit = 50;
	}
	echo $limit;	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//商品テンプレート一覧取得関数
	GetTProductsTemplateList($inp_category,$inp_name,$limit,$offset,$pt_fk_products_template_id,$pt_fk_item_category_id,$pt_f_products_name,$pt_f_photo1,$pt_f_market_price,$pt_all_count,$pt_data_cnt,$sort);
	
	//▼表示開始～終了番号
	if($pt_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $pt_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($pt_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='product_temp_list.php?inp_category=$inp_category&offset=$page_offset[$i]&limit=$limit&sort=$sort'>$num</A> ";
			}
		}
	}
	
	if($pt_all_count > $limit)
	{
		$page_first = "<A href='product_temp_list.php?inp_category=$inp_category&offset=0&limit=$limit&sort=$sort'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='product_temp_list.php?inp_category=$inp_category&offset=$page_offset[$max_page]&limit=$limit&sort=$sort'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $pt_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='product_temp_list.php?inp_category=$inp_category&offset=$next_page&limit=$limit&sort=$sort'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='product_temp_list.php?inp_category=$inp_category&offset=$before_page&limit=$limit&sort=$sort'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
	$url = "product_temp_list.php?inp_category=$inp_category&limit=$limit&offset=&sort=$sort";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='product_temp_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='inp_category' value='$inp_category'>";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
                $form_str .= "<input type='hidden' name='sort' value='$sort'>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;

        $dsp_tbl .= "<div style = 'text-align:left; margin-top:10px;'>\n";
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$pt_all_count</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        $dsp_tbl .= "</div><br>\n";

	$dsp_tbl .= "<p style = 'color:#555555;'>$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目</p>\n";

	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:40px;'>\n";
	$dsp_tbl .= "<th colspan = '2' style = 'width:100px;'>操作</th>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>ﾃﾝﾌﾟﾚID</th>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>ｶﾃｺﾞﾘ</th>\n";
	$dsp_tbl .= "<th style = 'width:300px;'>商品名</th>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>写真</th>\n";
	$dsp_tbl .= "<th style = 'width:80px;'>市場価格</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$pt_data_cnt;$i++)
	{
		//▼カテゴリ
		//商品カテゴリ名取得関数
		GetTItemCategoryName($pt_fk_item_category_id[$i], $f_category_name);
		
		//▼写真
		if($pt_f_photo1[$i] == "")
		{
			$picture_path_dsp = "";
		}
		else
		{
			$photo_path = SITE_URL."images/".$pt_f_photo1[$i];
                        //$photo_path = "http://localhost/images/".$pt_f_photo1[$i];
			$picture_path_dsp = "<img src='$photo_path' width=30 height=30>";
		}
		
		//テンプレート写真番号
		$tmppht_next = "";
		for($j=0; $j<5; $j++)
		{
			$tmppht_next .= "&tmppht[]=";
		}
		
		//▼登録・編集画面へのリンク
		$link_product = "<A href='product_regist.php?tmpid=$pt_fk_products_template_id[$i]&pid=0&mode=&photo_num=$tmppht_next' target='_blank'>商品登録</A>";
		$link_reg = "<A href='product_temp_regist.php?pid=$pt_fk_products_template_id[$i]&mode=&photo_num=' target='_blank'>編集</A>";
		
		$dsp_tbl .= "<tr style = 'height:40px;'>\n";
		$dsp_tbl .= "<td>$link_product</td>\n";
                $dsp_tbl .= "<td>$link_reg</td>\n";
		$dsp_tbl .= "<td>$pt_fk_products_template_id[$i]</td>\n";
		$dsp_tbl .= "<td>$f_category_name</td>\n";
		$dsp_tbl .= "<td>$pt_f_products_name[$i]</td>\n";
		$dsp_tbl .= "<td>$picture_path_dsp</td>\n";
		$dsp_tbl .= "<td align='right'>$pt_f_market_price[$i] 円</td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "<br><br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("テンプレート商品",$dsp_tbl);
?>
