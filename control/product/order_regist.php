<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$pid = $_REQUEST["pid"];
	$sid = $_REQUEST["sid"];
	$cost = 0;

	if(($pid != "") && ($pid != 0))
	{
		//オークション終了商品取得関数
		GetTEndProductsInfo($pid,$tep_f_products_name,$tep_fk_item_category_id,$tep_f_photo1,$tep_f_photo2,$tep_f_photo3,$tep_f_photo4,$tep_f_status,$tep_f_stock,$tep_fk_address_id,$tep_f_last_bidder,$tep_f_pickup_flg,$tep_f_end_time,$tep_f_address_flag);
		//オークション終了商品配送済みメールフラグと配達識別コード取得関数
		GetTEndProductsMail($pid,$f_send_mail,$f_send_mail_id);
		//商品アドレスフラグ取得関数
		GetTProductsAddressFlag($pid,$f_address_flag);
		//オークション終了商品メモ取得関数
		GetTEndProductsMemo($pid, $tep_f_memo);
		//オークション終了商品仕入価格取得関数
		GetTEndProductsMinPrice($pid, $cost);
		//商品ポイントオークション時加算枚数取得関数
		GetTProductsPlusCoins($pid,$tpm_f_plus_coins);
	}
	else
	{
		$tep_f_products_name		= "";
		$tep_fk_item_category_id	= "";
		$tep_f_photo1				= "";
		$tep_f_photo2				= "";
		$tep_f_photo3				= "";
		$tep_f_photo4				= "";
		$tep_f_status				= "";
		$tep_f_stock				= "";
		$tep_f_last_bidder			= "";
		$tep_fk_address_id			= "";
		$tep_f_end_time				= "";
		$tep_f_address_flag			= "";
		$tep_f_memo					= "";
		$tpm_f_plus_coins			= "";

		$f_send_mail				= 0;
		$f_send_mail_id				= "";
	}

	//▼カテゴリ
	//商品カテゴリ名取得関数
	GetTItemCategoryName($tep_fk_item_category_id, $f_category_name);
	//▼終了状態
	$f_status_id[0] = "0";
	$f_status_name[0] = "落札のみ";
	$f_status_id[1] = "1";
	$f_status_name[1] = "購入拒否ｷｬﾝｾﾙ";
	//2010/06/09 meng  modify start
	$INDEX=2;
	if($tep_fk_address_id != 1){
		$f_status_id[$INDEX] = "3";
		$f_status_name[$INDEX] = "配送先決定";
		$INDEX++;
	}
	//2010/06/09 meng  modify end
	$f_status_id[$INDEX] = "5";
	$f_status_name[$INDEX] = "発注済";
	$INDEX++;
	$f_status_id[$INDEX] = "4";
	$f_status_name[$INDEX] = "完了(配送済み)";
	$f_status_id[++$INDEX] = "9";
	$f_status_name[$INDEX] = "削除";
	$f_status_cnt = ++$INDEX;
	$name = "f_status";
	$select_num = $tep_f_status;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);

	//▼在庫・発注状態
	$f_stock_id[0] = "0";
	$f_stock_name[0] = "(未指定)";
	$f_stock_id[1] = "1";
	$f_stock_name[1] = "在庫あり";
	$f_stock_id[2] = "2";
	$f_stock_name[2] = "発注済み";
	$f_stock_cnt = 3;
	$name = "f_stock";
	$select_num = $tep_f_stock;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_stock_id, $f_stock_name, $f_stock_cnt, $select_num, $f_stock_select);

	//▼写真
	if($tep_f_photo1 != "")
	{
		$f_photo_1_path = SITE_URL."images/".$tep_f_photo1;
		$f_photo_1_dsp = "<img src='$f_photo_1_path'>";
	}
	else
	{
		$f_photo_1_dsp = "";
	}

	//▼メール送信フラグ
	if($f_send_mail == 1)
	{
		$chk_send_mail = "checked";
	}
	else
	{
		$chk_send_mail = "";
	}
	//▼ピックアップ商品
	if($tep_f_pickup_flg == 2)
	{
		$chk_pickup = "checked";
	}
	else
	{
		$chk_pickup = "";
	}

	//▼落札者
	//会員ログインIDハンドル取得関数
	GetTMemberLoginIdHandle($tep_f_last_bidder, $f_login_id, $f_handle);

	if($f_handle!="")
	{
		$member_str  = "";
		$member_str .= $f_login_id;
		$member_str .= "(";
		$member_str .= $f_handle;
		$member_str .= ")";
	}
	else
	{
		$member_str  = "削除済み";
	}

	//▼配送先の必要/不要判定
	if($tep_f_address_flag == 1 || $f_handle=="")
	{
		$ta_f_name = "";
		$address_str = "";
	}
	else
	{
		//会員住所取得関数
		GetTAddressInfo($tep_fk_address_id,$ta_f_status,$ta_fk_member_id,$ta_f_last_send,$ta_f_name,$ta_f_tel_no,$ta_f_post_code,$ta_fk_perf_id,$ta_f_address1,$ta_f_address2,$ta_f_address3,$ta_f_tm_stamp);
		//都道府県名取得関数
		if($ta_fk_perf_id != "") GetTPrefName($ta_fk_perf_id, $tp_t_pref_name);

		//▼配送先住所
		$post_code1 = substr($ta_f_post_code, 0, 3);
		$post_code2 = substr($ta_f_post_code, 3, 4);
		$address_str  = "";
		$address_str .= $post_code1;
		$address_str .= "-";
		$address_str .= $post_code2;
		$address_str .= "<br>\n";
		$address_str .= $tp_t_pref_name;
		$address_str .= " ";
		$address_str .= $ta_f_address1;
		$address_str .= $ta_f_address2;
		$address_str .= "<br>\n";
		$address_str .= $ta_f_address3;
		$address_str .= "<br>\n";
		$address_str .= "Tel. ";
		$address_str .= $ta_f_tel_no;
	}

	//▼配送先表示権限を取得
	//スタッフ管理者権限判定関数
	$staff_address = CheckStaffTAuthority($sid,"25");

	//▼ポイントオークションでのポイント数
	$plus_coins_str = $tpm_f_plus_coins." ".POINT_NAME;

	$dsp_tbl  = "";
	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>発注・出荷編集</legend>\n";
	$dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>商品ID</th>\n";
	$dsp_tbl .= "<td>$pid</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<FORM action='order_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>商品名</th>\n";
	$dsp_tbl .= "<td>$tep_f_products_name</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>商品カテゴリ</th>\n";
	$dsp_tbl .= "<td>$f_category_name</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>落札日時</th>\n";
	$dsp_tbl .= "<td>$tep_f_end_time</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>写真</th>\n";
	$dsp_tbl .= "<td>$f_photo_1_dsp</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>仕入価格</th>\n";
	$dsp_tbl .= "<td><input type='text' name='cost' value='$cost'></td>\n";
	$dsp_tbl .= "</tr>\n";

	if($tep_f_status == 2)
	{
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>状態</th>\n";
		$dsp_tbl .= "<td>支払済み<input type='hidden' name='f_status' value='2'></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	else
	{
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>状態</th>\n";
		$dsp_tbl .= "<td>$f_status_select</td>\n";
		$dsp_tbl .= "</tr>\n";
	}

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>配送お知らせメール</th>\n";

	if($f_address_flag==0){
		if($f_send_mail==1){
			$dsp_tbl .= "<td>既にお知らせメールが送信されています。<br><input type='checkbox' name='f_send_mail' value='1'>配送お知らせする(状態が完了の時のみ)</td>\n";
		}else{
			$dsp_tbl .= "<td>お知らせメールが送信されていません。<br><input type='checkbox' name='f_send_mail' value='1'>配送お知らせする(状態が完了の時のみ)</td>\n";
		}
	}else{
		$dsp_tbl .= "<td>お知らせメールが送信されていません。<br><input type='checkbox' name='f_send_mail' value='1' disabled='false'>配送お知らせする(状態が完了の時のみ)</td>\n";
	}

	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>配送お問い合わせコード</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_send_mail_id' value='$f_send_mail_id'></td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ﾋﾟｯｸｱｯﾌﾟ商品</th>\n";
	$dsp_tbl .= "<td><input type='checkbox' name='f_pickup_flg' value='1' $chk_pickup></td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>在庫・発注状態</th>\n";
	$dsp_tbl .= "<td>$f_stock_select</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>落札者</th>\n";
	$dsp_tbl .= "<td>$member_str</td>\n";
	$dsp_tbl .= "</tr>\n";

	//▼配送先表示権限を判定
	if($staff_address == true)
	{
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>配送先宛名</th>\n";
		$dsp_tbl .= "<td>$ta_f_name</td>\n";
		$dsp_tbl .= "</tr>\n";

		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>配送先住所</th>\n";
		$dsp_tbl .= "<td>$address_str</td>\n";
		$dsp_tbl .= "</tr>\n";
	}

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>落札者の声</th>\n";
	$dsp_tbl .= "<td><textarea name='f_memo' cols='50' rows='5'>$tep_f_memo</textarea></td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ﾎﾟｲﾝﾄｵｰｸｼｮﾝでのﾎﾟｲﾝﾄ数</th>\n";
	$dsp_tbl .= "<td>$plus_coins_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";

	$dsp_tbl .= "<div style = 'text-align:center;'>\n";
	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mem_id' value='$tep_f_last_bidder'>\n";
	$dsp_tbl .= "<input type='hidden' name='hold_status' value='$tep_f_status'>\n";
	$dsp_tbl .= "<input type='hidden' name='address_flag' value='$f_address_flag'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='更新'>\n";
	$dsp_tbl .= "<input type='button' class = 'button1' value='閉じる' onClick='window.close();'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</div><br>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("発注・出荷編集",$dsp_tbl);
?>
