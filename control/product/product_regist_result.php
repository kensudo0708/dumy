<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$image_class = HOME_PATH."mvc/utils/Image.class.php";
	$file_class = HOME_PATH."mvc/utils/File.class.php";
	$string_class = HOME_PATH."mvc/utils/StringBuilder.class.php";
	$constkeys_class = HOME_PATH."mvc/config/ConstKeys.class.php";
	include $image_class;
	include $file_class;
	include $string_class;
	include $constkeys_class;
	
	$pid = $_REQUEST["pid"];
	$tmpid = $_REQUEST["tmpid"];

	/**
	 * モバイルFlash用の商品画像生成対応
	 * ※関連するシステムパラメータが全て設定されていないと動作しません
	 */
	$mob_flash = FALSE;
	$flash_config_array = array(
		'MOBILE_FLASH_PRD_IMG_SIZE'=>'',
		'MOBILE_FLASH_PRD_IMG_PREFIX'=>NULL,
	);
	if ( defined('MOBILE_FLASH_ENABLE') && constant('MOBILE_FLASH_ENABLE') ) {
		$mob_flash = TRUE;
		foreach ( $flash_config_array as $key=>$value ) {
			if ( defined($key) && constant($key) ) {
				$flash_config_array[($key)] = constant($key);
			} else {
				error_log('MOBILE_FLASH: constant '.$key.' is undefiend or empty.');
				$mob_flash = FALSE;
				break;
			}
		}
	}
	
	//---------------------------------------------------------------------------------
	//▼画像
	$PATH = HOME_PATH."images/";
	if($tmpid != "")
	{
		$f_photo1 = $_REQUEST["f_photo1"];
		$f_photo2 = $_REQUEST["f_photo2"];
		$f_photo3 = $_REQUEST["f_photo3"];
		$f_photo4 = $_REQUEST["f_photo4"];
		$f_photo5 = $_REQUEST["f_photo5"];
		$f_photo1mb = $_REQUEST["f_photo1mb"];
		$f_photo2mb = $_REQUEST["f_photo2mb"];
		$f_photo3mb = $_REQUEST["f_photo3mb"];
		$f_photo4mb = $_REQUEST["f_photo4mb"];
		$f_photo5mb = $_REQUEST["f_photo5mb"];
	}
	else
	{
		$f_photo1 = "";
		$f_photo2 = "";
		$f_photo3 = "";
		$f_photo4 = "";
		$f_photo5 = "";
		$f_photo1mb = "";
		$f_photo2mb = "";
		$f_photo3mb = "";
		$f_photo4mb = "";
		$f_photo5mb = "";
	}
	
	$file=new File();
	$file->thumb=true;
	$file->thumbPath=',mb';
	$file->thumbPrefix   = 'pc_,mb_';
	$file->thumbMaxWidth = PRODUCT_IMAGE_WIDTH_PC.",".PRODUCT_IMAGE_WIDTH_MB;
	$file->thumbMaxHeight = PRODUCT_IMAGE_HEIGHT_PC.",".PRODUCT_IMAGE_HEIGHT_MB;
	$file->thumbRemoveOrigin=false;
	$file->uuidFileName=true;
	$info=$file->upload($PATH);
	
	$com1="";
	if(empty($_FILES["file1"])==FALSE)
	{
	    if($_FILES["file1"]["size"] > 0)
	    {
			$com1="<P>".$_FILES["file1"]["name"]."をアップしました</P>";
			print $com1."<br>\n";
			$f_photo1 = "pc_".$info[0]["savename"];
			$f_photo1mb = "mb_".$info[0]["savename"];
			$f=$PATH.$f_photo1;
			$file->FixImage($f,$PATH.PRODUCT_LITTLE_PREFIX.$f_photo1,'',PRODUCT_LITTLE_WIDTH,PRODUCT_LITTLE_HEIGHT,true);
			if ( $mob_flash ) {
				$file->FixImage2(
					$PATH.$f_photo1
					,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo1mb).'.jpg'
					,'' // 元画像の形式
					,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
					,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
					,true  // interlace->未使用臭い
					,'jpeg'
				);
			}
	    }
	    else
	    {
	        $com1="";
	    }
	}
	
	$com2="";
	if(empty($_FILES["file2"])==FALSE)
	{
	    if($_FILES["file2"]["size"] > 0)
	    {
			$com2="<P>".$_FILES["file2"]["name"]."をアップしました</P>";
			print $com2."<br>\n";
			$f_photo2 = "pc_".$info[1]["savename"];
			$f_photo2mb = "mb_".$info[1]["savename"];
                        $f=$PATH.$f_photo2;
                        $file->FixImage($f,$PATH.PRODUCT_LITTLE_PREFIX.$f_photo2,'',PRODUCT_LITTLE_WIDTH,PRODUCT_LITTLE_HEIGHT,true);
                        if ( $mob_flash ) {
                                $file->FixImage2(
                                        $PATH.$f_photo2
                                        ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo2mb).'.jpg'
                                        ,'' // 元画像の形式
                                        ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                                        ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                                        ,true  // interlace->未使用臭い
                                        ,'jpeg'
                                );
                        }
	    }
	    else
	    {
	        $com2="";
	    }
	}
	
	$com3="";
	if(empty($_FILES["file3"])==FALSE)
	{
	    if($_FILES["file3"]["size"] > 0)
	    {
			$com3="<P>".$_FILES["file3"]["name"]."をアップしました</P>";
			print $com3."<br>\n";
			$f_photo3 = "pc_".$info[2]["savename"];
			$f_photo3mb = "mb_".$info[2]["savename"];
                        $f=$PATH.$f_photo3;
                        $file->FixImage($f,$PATH.PRODUCT_LITTLE_PREFIX.$f_photo3,'',PRODUCT_LITTLE_WIDTH,PRODUCT_LITTLE_HEIGHT,true);
                        if ( $mob_flash ) {
                                $file->FixImage2(
                                        $PATH.$f_photo3
                                        ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo3mb).'.jpg'
                                        ,'' // 元画像の形式
                                        ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                                        ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                                        ,true  // interlace->未使用臭い
                                        ,'jpeg'
                                );
                        }
	    }
	    else
	    {
	        $com3="";
	    }
	}
	$com4="";
	if(empty($_FILES["file4"])==FALSE)
	{
	    if($_FILES["file4"]["size"] > 0)
	    {
			$com4="<P>".$_FILES["file4"]["name"]."をアップしました</P>";
			print $com4."<br>\n";
			$f_photo4 = "pc_".$info[3]["savename"];
			$f_photo4mb = "mb_".$info[3]["savename"];
                        $f=$PATH.$f_photo4;
                        $file->FixImage($f,$PATH.PRODUCT_LITTLE_PREFIX.$f_photo4,'',PRODUCT_LITTLE_WIDTH,PRODUCT_LITTLE_HEIGHT,true);
                        if ( $mob_flash ) {
                                $file->FixImage2(
                                        $PATH.$f_photo4
                                        ,$PATH.'mb/'.$flash_config_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($f_photo4mb).'.jpg'
                                        ,'' // 元画像の形式
                                        ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE']  // 幅上限px
                                        ,$flash_config_array['MOBILE_FLASH_PRD_IMG_SIZE'] // 高さ上限px
                                        ,true  // interlace->未使用臭い
                                        ,'jpeg'
                                );
                        }
	    }
	    else
	    {
	        $com4="";
	    }
	}
	//---------------------------------------------------------------------------------
	
	$code="'";
	$code_2="&#39;";
	$fk_item_category_id = $_REQUEST["fk_item_category_id"];
	$f_start_price = $_REQUEST["f_start_price"];
	$f_min_price = $_REQUEST["f_min_price"];
	$f_r_min_price = $_REQUEST["f_r_min_price"];
	$f_r_max_price = $_REQUEST["f_r_max_price"];
	$f_market_price = $_REQUEST["f_market_price"];
	
	$f_products_name = str_replace($code,$code_2,$_REQUEST["f_products_name"]);
	$f_top_description_pc = str_replace($code,$code_2,$_REQUEST["f_top_description_pc"]);
	$f_main_description_pc = str_replace($code,$code_2,$_REQUEST["f_main_description_pc"]);
	$f_top_description_mb = str_replace($code,$code_2,$_REQUEST["f_top_description_mb"]);
	$f_main_description_mb = str_replace($code,$code_2,$_REQUEST["f_main_description_mb"]);
	
	$start_time_year = $_REQUEST["start_time_year"];
	$start_time_month = $_REQUEST["start_time_month"];
	$start_time_day = $_REQUEST["start_time_day"];
	$start_time_hour = $_REQUEST["start_time_hour"];
	$start_time_minutes = $_REQUEST["start_time_minutes"];
	$stop_time_year = $_REQUEST["stop_time_year"];
	$stop_time_month = $_REQUEST["stop_time_month"];
	$stop_time_day = $_REQUEST["stop_time_day"];
	$stop_time_hour = $_REQUEST["stop_time_hour"];
	$stop_time_minutes = $_REQUEST["stop_time_minutes"];
	$fk_auction_type_id = $_REQUEST["fk_auction_type_id"];
	$f_recmmend = $_REQUEST["f_recmmend"];
	$f_status = $_REQUEST["f_status"];
	$f_auction_time = $_REQUEST["f_auction_time"];
	
	//ポイントオークション対応 start
	$f_plus_coins = $_REQUEST["f_plus_coins"];
	$f_address_flag = $_REQUEST["f_address_flag"];
	//end
	
	//落札者の声対応 start
	$f_products_template_id = $_REQUEST["template_id"];
	// end
	
	$hold_status = $_REQUEST["hold_status"];
	
	//▼NULLチェック
	$chknull_str = "";
	if($hold_status == 0)
	{
		//待機中の場合
		if($f_status == "")
		{
			$chknull_str .= "<b>\"状態\"が入力されていません。</b><br>\n";
		}
		if($start_time_year == "")
		{
			$chknull_str .= "<b>\"開催開始日時(年)\"が入力されていません。</b><br>\n";
		}
		if($start_time_month == "")
		{
			$chknull_str .= "<b>\"開催開始日時(月)\"が入力されていません。</b><br>\n";
		}
		if($start_time_day == "")
		{
			$chknull_str .= "<b>\"開催開始日時(日)\"が入力されていません。</b><br>\n";
		}
		if($start_time_hour == "")
		{
			$chknull_str .= "<b>\"開催開始日時(時)\"が入力されていません。</b><br>\n";
		}
		if($start_time_minutes == "")
		{
			$chknull_str .= "<b>\"開催開始日時(分)\"が入力されていません。</b><br>\n";
		}
		if($stop_time_year == "")
		{
			$chknull_str .= "<b>\"終了予定日時(年)\"が入力されていません。</b><br>\n";
		}
		if($stop_time_month == "")
		{
			$chknull_str .= "<b>\"終了予定日時(月)\"が入力されていません。</b><br>\n";
		}
		if($stop_time_day == "")
		{
			$chknull_str .= "<b>\"終了予定日時(日)\"が入力されていません。</b><br>\n";
		}
		if($stop_time_hour == "")
		{
			$chknull_str .= "<b>\"終了予定日時(時)\"が入力されていません。</b><br>\n";
		}
		if($stop_time_minutes == "")
		{
			$chknull_str .= "<b>\"終了予定日時(分)\"が入力されていません。</b><br>\n";
		}
		if($f_auction_time == "")
		{
			$chknull_str .= "<b>\"残り秒数\"が入力されていません。</b><br>\n";
		}
		if($fk_auction_type_id == "")
		{
			$chknull_str .= "<b>\"ｵｰｸｼｮﾝﾀｲﾌﾟ\"が入力されていません。</b><br>\n";
		}
		if($fk_item_category_id == "")
		{
			$chknull_str .= "<b>\"カテゴリ\"が入力されていません。</b><br>\n";
		}
		if($f_start_price == "")
		{
			$chknull_str .= "<b>\"開始価格\"が入力されていません。</b><br>\n";
		}
		if($f_plus_coins == "")
		{
			$chknull_str .= "<b>\"ポイントオークションでのポイント数\"が入力されていません。</b><br>\n";
		}
	}
	if($f_products_name == "")
	{
		$chknull_str .= "<b>\"商品名\"が入力されていません。</b><br>\n";
	}
	if($f_market_price == "")
	{
		$chknull_str .= "<b>\"一般市場価格\"が入力されていません。</b><br>\n";
	}
	if($f_min_price == "")
	{
		$chknull_str .= "<b>\"仕入価格\"が入力されていません。</b><br>\n";
	}
	if($f_r_max_price == "")
	{
		$chknull_str .= "<b>\"増額時の最大価格\"が入力されていません。</b><br>\n";
	}
	if($f_r_min_price == "")
	{
		$chknull_str .= "<b>\"減額時の最小価格\"が入力されていません。</b><br>\n";
	}
	if($f_address_flag == "")
	{
		$chknull_str .= "<b>\"配送先設定\"が入力されていません。</b><br>\n";
	}
	
	if($chknull_str != "")
	{
        $dsp_tbl  = "";
        $dsp_tbl .= "<br><br>\n";
        $dsp_tbl .= $chknull_str;
        $dsp_tbl .= "<br><br>\n";
        $dsp_tbl .= "下の戻るﾎﾞﾀﾝを押して下さい。\n";
        $dsp_tbl .= "<br>\n";
		$dsp_tbl .= "<form>\n";
		$dsp_tbl .= "<button type='button' onclick='history.back()' style='width:80px'>戻る</button>\n";
		$dsp_tbl .= "</form>\n";
		PrintAdminPage("商品登録完了",$dsp_tbl);
		exit;
	}
	
	$f_start_time  = "";
	$f_start_time .= $start_time_year;
	$f_start_time .= "-";
	$f_start_time .= $start_time_month;
	$f_start_time .= "-";
	$f_start_time .= $start_time_day;
	$f_start_time .= " ";
	$f_start_time .= $start_time_hour;
	$f_start_time .= ":";
	$f_start_time .= $start_time_minutes;
	$f_start_time .= ":00";
	
	$f_stop_time  = "";
	$f_stop_time .= $stop_time_year;
	$f_stop_time .= "-";
	$f_stop_time .= $stop_time_month;
	$f_stop_time .= "-";
	$f_stop_time .= $stop_time_day;
	$f_stop_time .= " ";
	$f_stop_time .= $stop_time_hour;
	$f_stop_time .= ":";
	$f_stop_time .= $stop_time_minutes;
	$f_stop_time .= ":00";
	
	if($f_min_price == 0 || $f_market_price == 0)
        {
            $dsp_tbl  = "";
            $dsp_tbl .= "<br><br>\n";
            $dsp_tbl .= "<b>登録失敗。<BR>仕入値又は市場価格が0です。</b>\n";
            $dsp_tbl .= "<br><br>\n";
            $dsp_tbl .= "下の閉じるﾎﾞﾀﾝを押して下さい。\n";
            $dsp_tbl .= "<br>\n";
            $dsp_tbl .= "<form>\n";
            $dsp_tbl .= "<button type='button' onclick='window.close()' style='width:80px'>閉じる</button>\n";
            $dsp_tbl .= "</form>\n";
            PrintAdminPage("商品登録完了",$dsp_tbl);
            exit;
        }	
	$t_r_status = 0;
	GetRStatus($fk_auction_type_id,$t_r_status);
	if($pid == "")
	{
		PrintAdminPage("商品登録完了","<P>不正な処理です</P>");
		//print "不正な処理です。<br>\n";
		exit;
	}
	else if($pid == "0")
	{
		//商品情報登録関数
		$ret = RegistTProductsData($f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_start_time,$fk_auction_type_id,$f_recmmend,$f_status,$f_auction_time,$f_plus_coins,$f_address_flag,$f_products_template_id,$t_r_status);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "商品の新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//▼状態を判定
		if($hold_status == 1)	//開催中
		{
			//開催中商品更新関数
			$ret = UpdateTProductsOpen($pid,$f_products_name,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_recmmend,$f_status,$f_address_flag,$f_products_template_id);
		}
		else
		{
			//商品情報更新関数
			$ret = UpdateTProductsData($pid,$f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_start_time,$fk_auction_type_id,$f_recmmend,$f_status,$f_auction_time,$f_plus_coins,$f_address_flag,$f_products_template_id);
		}
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "商品情報の更新処理に成功しました。<br>\n";
		}
	}
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<b>$dsp_string</b>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "下の閉じるﾎﾞﾀﾝを押して下さい。\n";
	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "<button type='button' onclick='window.close()' style='width:80px'>閉じる</button>\n";
	$dsp_tbl .= "</form>\n";
//	if (SERVER_NUM > 1)
//        {
//            `/home/auction/script/php/date_sync/sync_prod_image.sh`;
//            ` /home/auction/script/php/date_sync/item_image_up.sh`;
//        }
	//管理画面入力ページ表示関数
	PrintAdminPage("商品登録完了",$dsp_tbl);

?>
