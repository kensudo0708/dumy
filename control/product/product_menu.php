<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

        session_start();
        $sid = $_SESSION["staff_id"];
        $csvprint=CheckStaffTAuthority($sid,"29");
	
	//▼カテゴリ選択作成
	//商品カテゴリ一覧取得関数
	GetTItemCategoryList($tic_fk_item_category, $tic_f_category_name, $tic_f_mail_category_val, $tic_f_tm_stamp, $tic_data_cnt);
	$inp_category_id[0] = "";
	$inp_category_name[0] = "(指定しない)";
	for($i=0; $i<$tic_data_cnt; $i++)
	{
		$inp_category_id[$i+1] = $tic_fk_item_category[$i];
		$inp_category_name[$i+1] = $tic_f_category_name[$i];
	}
	$inp_category_cnt = $tic_data_cnt + 1;
	$name = "inp_category";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_category_id, $inp_category_name, $inp_category_cnt, $select_num, $category_select);
	
	//▼オークション選択作成
	//オークション名一覧取得関数
	GetFAuctionNameList($fan_fk_auction_type_id, $fan_f_auction_name, $fan_data_cnt);
	$inp_auction_id[0] = "";
	$inp_auction_name[0] = "(指定しない)";
	for($i=0; $i<$fan_data_cnt; $i++)
	{
		$inp_auction_id[$i+1] = $fan_fk_auction_type_id[$i];
		$inp_auction_name[$i+1] = $fan_f_auction_name[$i];
	}
	$inp_auction_cnt = $fan_data_cnt + 1;
	$name = "inp_auction";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_auction_id, $inp_auction_name, $inp_auction_cnt, $select_num, $auction_select);
	
	//テンプレート写真番号
	$tmppht_next = "";
	for($i=0; $i<5; $i++)
	{
		$tmppht_next .= "&tmppht[]=";
	}
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class='main_l'>\n";
	$dsp_tbl .= "<caption>&#x2460;ｵｰｸｼｮﾝ商品管理</caption>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center'>\n";
	$dsp_tbl .= "<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='新規商品登録' type='button' onclick=\"OpenWin('product_regist.php?pid=0&mode=&photo_num=&tmpid=$tmppht_next')\">\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='product_list.php' method='GET' target='main_r'>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>商品ID</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center'><input type='text' name='prod_id' istyle='4'></td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>状態</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_status'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>待機中\n";
	$dsp_tbl .= "<OPTION value='1'>開催中\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>カテゴリ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$category_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>ｵｰｸｼｮﾝﾀｲﾌﾟ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$auction_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "登録日(yyyymmdd)";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_regdate' size='10' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>その他</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='checkbox' name='inp_recmmend' value='1'>&nbsp;注目商品\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='left'><tt>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='5' >残り時間(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='4' checked>残り時間(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='0' >商品ID(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='1' >商品ID(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='2' >開始時間(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='3' >開始時間(降順)</input><BR></tt></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>表示種別</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='left'><tt>\n";
	$dsp_tbl .= "<input type='radio' name='disp_type' value='0' checked>商品一覧</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='disp_type' value='1' >自動入札</input><BR>\n";
        if($csvprint){
        $dsp_tbl .= "<input type='radio' name='disp_type' value='2' >CSV(商品一覧)</input><BR>\n";
        $dsp_tbl .= "<input type='radio' name='disp_type' value='3' >CSV(自動入札)</input><BR>\n";
        }
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='商品リスト検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);
?>
