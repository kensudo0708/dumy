<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION['staff_id'];
	
	$pid = $_REQUEST["pid"];
	$page = $_REQUEST["page"];
	
	$offset = 50;
	$limit = $offset*($page-1);
	
	//商品情報取得関数
	GetTProductsInfo($pid,$tpm_f_products_name,$tpm_fk_item_category_id,$tpm_f_photo1,$tpm_f_photo2,$tpm_f_photo3,$tpm_f_photo4,$tpm_f_photo5,$tpm_f_photo1mb,$tpm_f_photo2mb,$tpm_f_photo3mb,$tpm_f_photo4mb,$tpm_f_photo5mb,$tpm_f_top_description_pc,$tpm_f_main_description_pc,$tpm_f_top_description_mb,$tpm_f_main_description_mb,$tpm_f_start_price,$tpm_f_min_price,$tpm_f_r_min_price,$tpm_f_r_max_price,$tpm_f_market_price,$tpm_fk_auction_type_id,$tpm_f_delflg,$tpm_f_memo,$tpm_f_target_hard,$tpm_f_recmmend,$tpm_f_regdate,$tp_f_bit_buy_count,$tp_f_bit_free_count,$tp_f_now_price,$tp_f_last_bidder,$tp_f_last_bidder_handle,$tp_f_auction_time,$tp_t_r_status,$tp_f_status,$tp_f_tm_stamp,$tp_f_start_time,$tpt_f_plus_coins,$tpt_f_address_flag,$tmp_fk_products_template_id,$tmp_initial_stop_time);
	
	//商品入札履歴一覧検索関数
	ProductsBitList($pid,$limit,$offset,$f_tm_stamp,$f_price,$fk_member_id,$f_handle,$f_bid_type,$f_coin_type,$data_cnt,$r_count,$p_count_free,$p_count_buy,$p_count_a_free,$p_count_a_buy,$all_count);
	
	$url = SITE_URL."images/";

        $disp_tab  = "";

        $disp_tab .= "<fieldset>\n";
        $disp_tab .= "<legend>商品入札履歴</legend>\n";
	$disp_tab .= "<table class = 'list'>\n";

        $disp_tab .= "<tr style = 'height:30px;'>\n";
        $disp_tab .= "<th style = 'width:100px;'>商品ID</th>\n";
	$disp_tab .= "<td style = 'width:200px;'>$pid</td>\n";
        $disp_tab .= "</tr>\n";

        $disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<th>商品名</th>\n";
	$disp_tab .= "<td>$tpm_f_products_name</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr style = 'height:100px;'>\n";
	$disp_tab .= "<th style = 'width:80px;'>商品画像</th>\n";
	$disp_tab .= "<td style = 'width:150px;'><img src ='$url$tpm_f_photo1' height='100' width='100'></td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "</table><br>\n";
	
	$disp_tab .= "<table class = 'list'>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<th style = 'width:80px;'>入札タイプ</th>\n";
	$disp_tab .= "<th style = 'width:80px;'>回数</th>\n";
	$disp_tab .= "<th style = 'width:100px;'>フリーコイン入札</th>\n";
	$disp_tab .= "<th style = 'width:100px;'>課金コイン入札</th>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<td>手動入札</td>\n";
	$disp_tab .= "<td>".number_format($p_count_free+$p_count_buy)."回</td>\n";
	$disp_tab .= "<td>".number_format($p_count_free)."回</td>\n";
	$disp_tab .= "<td>".number_format($p_count_buy)."回</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<td>手動入札2</td>\n";
	$disp_tab .= "<td>".number_format($r_count)."回</td>\n";
	$disp_tab .= "<td>".number_format($r_count)."回</td>\n";
	$disp_tab .= "<td>-</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<td>自動入札</td>\n";
	$disp_tab .= "<td>".number_format($p_count_a_free+$p_count_a_buy)."回</td>\n";
	$disp_tab .= "<td>".number_format($p_count_a_free)."回</td>\n";
	$disp_tab .= "<td>".number_format($p_count_a_buy)."回</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "</table>\n";
	$disp_tab .= "<BR><BR>\n";
	
	$disp_tab .= Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/product/product_bit_log.php?pid=$pid", 5);
	$disp_tab .= "<BR><BR>\n";
        $disp_tab .= "<table class = 'list'>\n";
	$disp_tab .= "<tr style = 'height:30px;'>\n";
	$disp_tab .= "<th style = 'width:150px;'>時間</th>\n";
	$disp_tab .= "<th style = 'width:80px;'>入札額</th>\n";
	$disp_tab .= "<th style = 'width:120px;'>入札者</th>\n";
	$disp_tab .= "<th style = 'width:120px;'>入札タイプ</th>\n";
	$disp_tab .= "<th style = 'width:120px;'>使用コイン</th>\n";
	$disp_tab .= "</tr>\n";
	
	for($i=0;$i<$data_cnt;$i++)
	{
		//会員状態取得関数
		GetTMemberActivity($fk_member_id[$i],$tmm_f_activity);
		
		//▼会員状態を判定
		if($tmm_f_activity == 99)
		{
			//ロボット
			$disp_tab .= "<tr class='disabled' style='height:30px;'>\n";
		}
		else
		{
			$disp_tab .= "<tr style = 'height:30px;'>\n";
		}
		$disp_tab .= "<td>$f_tm_stamp[$i]</td>\n";
		$disp_tab .= "<td>".number_format($f_price[$i])."</td>\n";
		
		//../member/mem_log.php?limit=50&offset=0&page=&sid=38&mid=10176&ltype=1
		if($f_handle[$i] !="")
		{
			$link = "../member/mem_log.php?limit=50&offset=0&page=&sid=$sid&mid=$fk_member_id[$i]&ltype=1";
			$disp_tab .= "<td><a href='$link'>$f_handle[$i]</a></td>\n";
		}
		else
		{
			$disp_tab .="<td>削除済み</td>\n";
		}
		$bit_type = "";
		switch($f_bid_type[$i])
		{
			case 0:
					$bit_type = "手動入札";
					break;
			case 1:
					$bit_type = "自動入札";
					break;
			case 2:
					$bit_type = "手動入札2";
					break;
		}
		$disp_tab .= "<td>$bit_type</td>\n";
		
		$coin_type = "";
		switch($f_coin_type[$i])
		{
			case 0:
					$coin_type = "フリー";
					break;
			case 1:
					$coin_type = "課金";
					break;
		}
		$disp_tab .= "<td>$coin_type</td>\n";
	}
	$disp_tab .= "</table>\n";
	$disp_tab .= "</fieldset>\n";
	$disp_tab .= Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/product/product_bit_log.php?pid=$pid", 5);
        $disp_tab .="<P>※『削除 済み』:退会又は強制退会により削除されたユーザです</P>";
        $disp_tab .="<P>※『処理済   :コイン返却チェックが済んでいる状態です</P>";
        $disp_tab .= "<form><input type='button' class = 'button1' value='閉じる' style = 'margin-left:5px;' onClick='window.close();'></form>\n";
        $disp_tab .= "<BR><BR>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("商品入札履歴",$disp_tab);
?>
