<?php
	//☆★	ライブラリ読込み	★☆
	include "../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	session_start();
	$sid = $_SESSION["staff_id"];

	$inp_hour = $_REQUEST["inp_hour"];

	if($inp_hour == "")
	{
		$inp_hour = 1;
	}

	$limit = 100;
	$offset = 0;

	//全会員入札一覧取得関数
	GetAllMemBidLogList($inp_hour,$limit,$offset,$tbl_fk_bid_log_id,$tbl_f_products_id,$tbl_fk_member_id,$tbl_f_price,$tbl_f_bid_type,$tbl_f_coin_type,$tbl_f_tm_stamp,$tbl_data_cnt);

	$dsp_tbl  = "";

	$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='1' cellpadding='1' width='100%' class = 'main_r'>\n";
	$dsp_tbl .= "<tr bgcolor='#CCCCFF'>\n";
	$dsp_tbl .= "<th><NOBR><tt>時刻</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>会員ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>ハンドル名</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>入札額</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>商品ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>商品名</tt></NOBR></th>\n";
	$dsp_tbl .= "</tr>\n";

	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tbl_data_cnt;$i++)
	{
		//▼時刻
		$time_str = substr($tbl_f_tm_stamp[$i], 11, 5);

		//会員ログインIDハンドル取得関数
		GetTMemberLoginIdHandle($tbl_fk_member_id[$i], $tmm_f_login_id, $tmm_f_handle);

		//商品名取得関数
		GetTProductsName($tbl_f_products_id[$i],$tpm_f_products_name);

		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$time_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tbl_fk_member_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tmm_f_handle</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tbl_f_price[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tbl_f_products_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left'><NOBR><tt>$tpm_f_products_name</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}

	$dsp_tbl .= "</table><br>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("TOP自動入札履歴画面",$dsp_tbl);

?>
