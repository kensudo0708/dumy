<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/09/02												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
//	$inp_hour = $_REQUEST["inp_hour"];
	$mode = $_REQUEST["mode"];
	
	//▼更新モード
	if($mode == "update")
	{
		$upid = $_REQUEST["upid"];
		
		//ニュース状態更新関数
		UpdateTNewsStatus($upid, "1");
	}
	
	
	//▼表示日数
	$inp_days = 30;
	
	//ニュース一覧取得関数
	GetTNewsList($inp_days,$tn_fk_news_id,$tn_f_category,$tn_f_body,$tn_f_status,$tn_f_regist_date,$tn_data_cnt);
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<div style= 'font-size:9pt;'>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tn_data_cnt;$i++)
	{
		//▼時刻
		$time_str = substr($tn_f_regist_date[$i], 5, 11);
		$time_str = str_replace("-","/" , $time_str);
		
		//▼未読/既読
		if($tn_f_status[$i] == 0)
		{
			//未読
			$n_string = $time_str."&nbsp;<b>[".$tn_f_category[$i]."]&nbsp;".$tn_f_body[$i]."</b>";
			$dsp_tbl .= "[未読]<A href='top_news.php?mode=update&upid=$tn_fk_news_id[$i]'>$n_string</A><br>\n";
		}
		else
		{
			//既読
			$n_string = $time_str."&nbsp;[".$tn_f_category[$i]."]&nbsp;".$tn_f_body[$i];
			$dsp_tbl .= "[既読]".$n_string."<br>\n";
		}
		
//		$dsp_tbl .= "<input type='checkbox' name='status_no[]' value='$tn_f_status[$i]'>&nbsp; $n_string\n";
	}
	
	$dsp_tbl .= "</div><br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("TOPニュース画面",$dsp_tbl);

?>
