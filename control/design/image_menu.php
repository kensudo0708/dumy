<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/layout.css" type="text/css" rel="stylesheet">
        <title></title>
    </head>
    <body class="left">
<?php
session_start();
include '../control/exec_select.php';

if($_SESSION['staff_id']==NULL )
{
?>
    <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>
</body>
</html>
<?PHP
    return ;
}
$PATH="";
$title="";
$term="";

//メニュー呼び出しかアップロードリロードかの判定
if(empty($_GET["term"])==FALSE)
{
    $term=$_GET["term"];
}
elseif(empty($_POST["term"])==FALSE)
{
    $term=$_POST["term"];
}

//表示領域へのURLの設定・画像保存先の取得
$uri='./disp_image.php?term=';
if($term==0)
{
    $uri .=$term;
    $title="&#x2462;PC画像管理";
    $PATH="../../style/pc/default/images/";
}
else
{
    $uri .=$term;
    $title="&#x2463;MB画像管理";
    $PATH="../../style/mobile/default/images/";
}

//画像保存処理
$com="";
if(empty($_FILES["file"])==FALSE)
{
    if($_FILES["file"]["size"] > 0)
    {
        $up_file=$PATH .$_FILES["file"]["name"];
        $result = @move_uploaded_file( $_FILES["file"]["tmp_name"], $up_file);
        chmod($up_file, 0755);
        if($result==TRUE)
        {
            $com="<P>".$_FILES["file"]["name"]."をアップしました</P>";
            if (SERVER_NUM > 1)
            {
                `/home/auction/script/php/date_sync/sync_design.sh 0`;
            }
        }
        else
        {
            $com="<P>アップロード失敗しました</P>";
        }
    }
    else
    {
        $com="<P>アップロードファイルがありません</P>";
    }
}

?>
<script type="text/javascript">
<!--

function jump(uri){

	// 設定開始（表示するフレーム名とリンク先URLを設定してください）

	parent.main_r.location.href = uri;
	// 設定終了
}

// -->
</script>
<table class = "main_l">
    <caption style = "font-size:9pt; text-align:left;"><?PHP print($title); ?></caption>

<FORM name='form' enctype="multipart/form-data" action='image_menu.php' method='post'>
<input type="hidden" name="s_code" value="%%S_CODE%%">

<tr><td>
<nobr><font color='blue' size="2">画像を選択し『新規追加』を押して下さい。</font><nobr>
</td></tr>
<tr><td style = "color:#FF0000;">
<?PHP print($com); ?>
</td></tr>
<tr>
<td>
<INPUT TYPE="hidden" NAME="MAX_FILE_SIZE" SIZE="65536">
<INPUT TYPE="file" NAME="file" SIZE="25">
<input type="hidden" NAME="term" value="<?PHP print($term);?>"
</td>
</tr>

<tr>
	<td align='center' colspan='2'>
            <input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:80px; border-color:#FFFAFA' value='新規追加' onclick="jump('<?PHP print($uri);?>')">
	</td>
</tr>
</FORM>
</table>
    </body>
</html>
