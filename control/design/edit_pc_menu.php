<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/15												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	session_start();
	$sid = $_SESSION["staff_id"];
	if($sid == "")
	{
		$dsp_tbl  = "";
		$dsp_tbl .= "<body>\n";
		$dsp_tbl .= "<P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>\n";
		$dsp_tbl .= "</body>\n";
		
		//管理画面入力ページ表示関数
		PrintAdminInputPage($dsp_tbl);
	}
	
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<table class = 'main_l'>";
            
        $dsp_tbl .= "<caption>&#x2460;PCページ編集</caption>\n";
	$dsp_tbl .= "<tr><td><a href='edit_pc_page.php?cat=0' target='main_r'>1.メインページ</a></td></tr>\n";
	$dsp_tbl .= "<tr><td><a href='edit_pc_page.php?cat=1' target='main_r'>2.パーツページ</a></td></tr>\n";
	$dsp_tbl .= "<tr><td><a href='edit_pc_page.php?cat=2' target='main_r'>3.スタイル</a></td></tr>\n";
	//$dsp_tbl .= "<tr><td><a href='edit_pc_page.php?cat=2' target='main_r'>3-1.基本テーマ</a></td></tr>\n";
        $dsp_tbl .= "<tr><td><a href='edit_pc_page.php?cat=3' target='main_r'>4.メール</a></td></tr>\n";
	$dsp_tbl .= "</table>";
	
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
