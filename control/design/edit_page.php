<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="../../control/css/layout.css">
        <title></title>
    </head>
<script type="text/javascript">
<!--
function reload(id)
{
//        if(window.confirm("更新しますか？")){

		parent.location.href="../design/edit_page.php?action=EXIT&id="+id;

//	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
//	else{

//		window.alert('キャンセルされました'); // 警告ダイアログを表示

//	}
}
// -->
</script>
<?php
include '../control/exec_select.php';
include '../control/exec_update.php';

session_start();
if($_SESSION['staff_id']==NULL )
{
?>
<body>
    <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>

</body>
</html>
<?PHP
    exit;
}
if(empty($_POST["action"]) == FALSE || empty($_GET["action"]) == FALSE)
{
    $comment="";
    if(empty($_POST["action"]) == FALSE && $_POST["action"]=='SAVE')
    {
        $file = $_POST["file"];
        $fp=fopen($file,"w");
        if($fp)
        {
            fwrite($fp, $_POST["page"]);
            fclose($fp);
        }
        $comment="更新完了しました。";
        edit_page($_POST["id"],0);
        if (SERVER_NUM > 1)
        {

            `/home/auction/script/php/date_sync/sync_design.sh 0`;

        }
    }
    elseif(empty($_GET["action"]) == FALSE && $_GET["action"]=='EXIT')
    {
        $comment="作業を取りやめましました。";
        edit_page($_GET["id"],0);
    }
    
?>
<body onload="window.close();window.opener.location.reload();">
</body>
</html>

<?PHP
    return;
}
$rs = get_edit_page($_GET["id"]);
if($rs==1)
{
?>
<body>
    <P>修正対象がしないか、別の管理者が編集中です。</P>
    <P>このままページを閉じてください</P>
</body>
</html>
<?PHP
    exit;
}
edit_page($_GET["id"],1);
$PC="../../templates/pc/";
$MB="../../templates/mobile/";

$ret = mysql_fetch_array($rs);
$file="";
if($ret["term"]==0)
{
    $file .=$PC;
    if($ret["cat"] == 2)
    {
        $file ="../../style/pc/default/";
    }
}
else
{
    $file .=$MB;
    if($ret["cat"] == 2)
    {
        $file ="../../style/mobile/default/";
    }
}
if($ret["cat"] == 1 || $ret["cat"] == 3)
{
    $file .="parts/";
}

$file .= $ret["uri"];
$file = trim($file);

//編集対象ファイル読み込み
$fsize  = filesize($file);
$fp=fopen($file,"r");
$buff="";

if($fp ==false)
{
    $buff="ファイルオープンに失敗しました。";
}
else
{
    $buffer = fread($fp, $fsize);
    $buffer = str_replace("<","&lt;" , $buffer);
    $buffer = str_replace(">","&gt;" , $buffer);
    fclose($fp);
}

?>
<!--body onBeforeUnload="reload('<?PHP print($_GET["id"]); ?>')"-->
<body>
    <P>この画面を終了するときは必ず、『更新』ボタンで終了してください</P>
    <form method="POST" action="edit_page.php">
        <table class="list" width="100%">
        <tr>
            <th width="30%"><?PHP print($ret["uri"]); ?></th>
            <td width="50%"><?PHP print($ret["exp"]); ?></td>
            <td width="10%" style="text-align:center"><button type="submit" style="width:80px">更新</button></td>
            <td width="10%" style="text-align:center"><button type="button" onclick="reload('<?PHP print($_GET["id"]); ?>')" style="width:80px">中止</button></td>
        </tr>
        <tr>
            <td colspan="4">
    <TEXTAREA name='page' rows='40' style="width:100%"><?PHP print($buffer); ?></TEXTAREA>
    <input type="hidden" value="SAVE" name="action">
    <input type="hidden" value="<?PHP print($file); ?>" name="file" >
    <input type="hidden" value="<?PHP print($_GET["id"]); ?>" name="id">
            </td>
        </tr>
    </table>
    </form>
    </body>
</html>
