<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="../../control/css/layout.css">
        <title></title>
    </head>
    <body>
<?php

session_start();
include '../control/exec_select.php';
if(empty($_GET["slp"]) == FALSE)
{
    //画像アップロード時の場合 表示させるため1秒スリープ
    sleep($_GET["slp"]);
}
if($_SESSION['staff_id']==NULL )
{
?>
    <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>
</body>
</html>
<?PHP
    return ;
}
$PATH="";
if($_GET["term"]==0)
{
    $PATH="../../style/pc/default/images/";
}
else
{
    $PATH="../../style/mobile/default/images/";
}

$file_name="";
//削除処理の場合
if(empty($_GET["action"])==FALSE)
{
    if($_GET["action"]=="DEL")
    {
        $file_name=$PATH . $_GET["filename"];
        if(file_exists($file_name)==true)
        {
            unlink($file_name);
            $mes=$_GET["filename"]."を削除しました";
            print($mes);
        }
    }
}

$FileList = explode("\n", `ls -l $PATH | grep rwx | awk '{print $9}' `);

$file_count = count($FileList);
if($file_count == 0 )
{
?>
    <P>画像は存在しません。</P>
</body>
</html>
<?PHP
    return ;
}
?>
<TABLE border=1 bordercolor='#CCCCCC' cellspacing='1' cellpadding='1'>
<?PHP
    for($i = 0;$i< $file_count/6 ; $i++)
    {
?>
    <tr>
<?PHP
        for($j=0 ; $j<6 ;$j++  )
        {
            $index = ($i*6)+$j;
            if($index >= $file_count)
            {
                break;
            }
            if($FileList[$index] =="")
            {
                continue;
            }
            $file_path=$PATH . $FileList[$index] ;
            $file_name=$FileList[$index] ;
?>
        <TD>
            <a href="<?PHP print($file_path);?>"><img src="<?PHP print($file_path);?>" width="100" height="100"></a><BR>
            <?PHP print($FileList[$index]); ?><br>
            <a href="disp_image.php?term=<?PHP print($_GET['term']); ?>&filename=<?PHP print($file_name); ?>&action=DEL" onClick="return confirm('削除しますか？')"> 削除</a>

        </TD>
<?PHP
        }
?>
    </tr>
<?PHP
    }
?>
</TABLE>
</body>
</html>
