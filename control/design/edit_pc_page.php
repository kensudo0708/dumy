<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="../../control/css/layout.css">
        <title></title>
    </head>
    <body>
<?php
session_start();
include '../control/exec_select.php';

if($_SESSION['staff_id']==NULL )
{
?>
    <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>
</body>
</html>
<?PHP
    exit;
}

$rs=get_edit_page_list(0, $_GET['cat']);
if($rs == -1)
{
?>
    <P>編集対象が存在しません</P>
</body>
</html>
<?PHP
    return;
}

?>
<script type="text/javascript">
<!--

function disp(url){

	window.open(url, "window_name", "width=1015,height=900,scrollbars=yes,resizable = no,fullscreen = no");

}
function check(url,file){

	window.open(url+file, "window_name", "width=1015,height=900,scrollbars=yes,resizable = no,fullscreen = no");

}
function reload(url)
{
        parent.main_r.location.href=url;
}
// -->
</script>

<P>重要度が『必須』になっているページは必ず作成してあることを確認してください(決済認証で必須になります)。</P>
<button type="submit" onClick="reload('edit_pc_page.php?cat=<?PHP print($_GET['cat']);?>')" style="width:200px">編集中状態の更新</button>
<table class="list" width="100%">
    <th width="5%">NO</th>
    <th width="35%">Page</th>
    <th width="5%">重要度</th>
    <th width="35%">説明</th>
    <th width="10%">編集中フラグ</th>
    <th width="10%">操作</th>
<?PHP
$URL="../../templates/pc/";
if($_GET['cat']==0)
{
}
else if($_GET['cat']==2)
{
    $URL="../../style/pc/default/";
}
else if($_GET['cat']==1 || $_GET['cat']==3)
{
    $URL.="parts/";
}
    $rs_num =mysql_num_rows($rs);
    for($i = 0; $i<$rs_num ; $i++)
    {
        $ret = mysql_fetch_array($rs);
?>
    <tr>
        <TD><center><?PHP print($ret["id"]) ?></center></TD>
        <TD><?PHP print($ret["uri"]) ?></TD>
        <?PHP if($ret["lev"] == 0){?>
            <TD><center>一般</center></TD>
        <?PHP } else { ?>
            <TD class="alert"><center>必須</center></TD>
        <?PHP } ?>
        <TD><?PHP print($ret["exp"]) ?></TD>
        <?PHP if($ret["of"] == 0){ ?>
            <TD><nobr>編集可能</nobr></TD>
        <?PHP } else { ?>
            <TD class="warn"><nobr>編集中</nobr></TD>
        <?PHP } ?>
        <TD><a href="#" onClick="disp('edit_page.php?id=<?PHP print($ret["id"]) ?>')">編集</a></TD>
        <!-- <TD width="10%"><a href="#" onClick="check('<?PHP print($URL);?>','<?PHP print($ret['uri']);?>')">確認</a></TD> -->
    </tr>
<?PHP
    }
?>
</table>
    </body>
</html>
