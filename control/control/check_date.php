<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function check_date($date)
{
    $day="01";
    $year=substr($date,0,4);
    $month=substr($date,4,2);
    if(strlen($date)==8)
    {
        $day=substr($date,6,2);
    }

    //日付が存在するかチェック
    if(checkdate($month, $day, $year))
    {
        return $date;
    }

    //年のチェック
    if($year < 2010 && $year > 2099)
    {
        $year =date('Y');
    }
    //月のチェック
    if(sprintf('%d',$month) < 1 || sprintf('%d',$month) > 12)
    {
        print($month."\n");
        $month =date('m');
    }
    //日付が存在の再チェック
    if(checkdate($month, $day, $year))
    {
        return $year.$month.$day;
    }

    switch($month)
    {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if($day < 1 )
            {
               $day="01";
            }
            if($day > 31 )
            {
               $day="31";
            }
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            if($day < 1 )
            {
               $day="01";
            }
            if($day > 30 )
            {
               $day="30";
            }
            break;
        case 2:
            if($day < 1 )
            {
               $day="01";
            }
            if($year%4 == 0 && $date >29 )
            {
               $day="29";
            }
            elseif($year%4 !=0 && $date>28)
            {
                $day="28";
            }
            break;
    }
    return $year.$month.$day;
}

function check_month($date)
{
    $year=substr($date,0,4);
    $month=substr($date,4,2);

    if($year < 2010 && $year > 2099)
    {
        $year =date('Y');
    }
    //月のチェック
    if(sprintf('%d',$month) < 1 || sprintf('%d',$month) > 12)
    {
        $date =date('m');
    }
    //日付が存在の再チェック
    return $year.$month.$day;
}
//日付修正
function computeday($date) {
    $year=substr($date,0,4);
    $month=substr($date,4,2);
    $day=substr($date,6,2);
    $baseSec = mktime(0, 0, 0, $month, $day, $year);//基準日を秒で取得
    $addSec = 86400;//日数×１日の秒数
    $targetSec = $baseSec + $addSec;
    return date("Ymd", $targetSec);
}

//月修正
function computemonth($date) {
    $year=substr($date,0,4);
    $month=substr($date,4,2);
    $day="01";

    $baseSec = mktime(0, 0, 0, $month, $day, $year);//基準日を秒で取得

    $addDays=0;
    switch($month)
    {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            $addDays=31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            $addDays=30;
            break;
        case 2:
            if($year%4 == 0 && $date >29 )
            {
               $addDays="29";
            }
            elseif($year%4 !=0 && $date>28)
            {
                $addDays="28";
            }
            break;
    }

    $addSec = $addDays * 86400;//日数×１日の秒数
    $targetSec = $baseSec + $addSec;
    return date("Ym", $targetSec);
}

function replace_day($num)
{
    $tmp="";
    switch($num)
    {
        case 1:
            $tmp="(日)";
            break;
        case 2:
            $tmp="(月)";
            break;
        case 3:
            $tmp="(火)";
            break;
        case 4:
            $tmp="(水)";
            break;
        case 5:
            $tmp="(木)";
            break;
        case 6:
            $tmp="(金)";
            break;
        case 7:
            $tmp="(土)";
            break;
    }
    return $tmp;
}
?>
