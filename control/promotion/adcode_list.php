<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];
	
	$inp_adcode = $_REQUEST["inp_adcode"];	//G.Chin 2010-08-19 add
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
//G.Chin 2010-08-19 chg sta
/*
	//広告主一覧取得関数
//	GetTAdmasterList($limit,$offset,$tam_fk_admaster_id,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp,$tam_all_count,$tam_data_cnt);
	GetTAdmasterListII($limit,$offset,$sort,$tam_fk_admaster_id,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp,$tam_all_count,$tam_data_cnt);
*/
	//▼入力広告コード判定
	if($inp_adcode == "")
	{
		//広告主一覧取得関数II
		GetTAdmasterListII($limit,$offset,$sort,$tam_fk_admaster_id,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp,$tam_all_count,$tam_data_cnt);
	}
	else
	{
		//広告コード指定広告主取得関数
		SrcAdcodeGetTAdmaster($limit,$offset,$inp_adcode,$tam_fk_admaster_id,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp,$tam_all_count,$tam_data_cnt);
	}
//G.Chin 2010-08-19 chg end
	
	//▼表示開始～終了番号
	if($tam_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tam_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tam_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='adcode_list.php?offset=$page_offset[$i]&limit=$limit&sort=$sort&inp_adcode=$inp_adcode'>$num</A> ";
			}
		}
	}
	
	if($tam_all_count > $limit)
	{
		$page_first = "<A href='adcode_list.php?offset=0&limit=$limit&sort=$sort&inp_adcode=$inp_adcode'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='adcode_list.php?offset=$page_offset[$max_page]&limit=$limit&sort=$sort&inp_adcode=$inp_adcode'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tam_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='adcode_list.php?offset=$next_page&limit=$limit&sort=$sort&inp_adcode=$inp_adcode'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='adcode_list.php?offset=$before_page&limit=$limit&sort=$sort&inp_adcode=$inp_adcode'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "adcode_list.php?limit=$limit&offset=&sort=$sort&inp_adcode=$inp_adcode";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= "<center>\n";
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='adcode_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "<input type='hidden' name='sort' value='$sort'>";				//AREQ-77 G.Chin 2010-08-05 add
		$form_str .= "<input type='hidden' name='inp_adcode' value='$inp_adcode'>";	//G.Chin 2010-10-05 add
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "</center>\n";
	
	$dsp_tbl .= "<table class='data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>データ件数</th>\n";
	$dsp_tbl .= "<th>$tam_all_count</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<tt>$start_num</tt>\n";
	$dsp_tbl .= "<tt>件目 ～ </tt>\n";
	$dsp_tbl .= "<tt>$end_num</tt>\n";
	$dsp_tbl .= "<tt>件目</tt>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class='list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style=\"width:60px\"><NOBR><tt>ID</tt></NOBR></th>\n";
//AREQ-77 G.Chin 2010-08-05 chg sta
//	$dsp_tbl .= "<th><NOBR><tt><a href='adcode_list.php?limit=$limit&sort=asc&offset=$offset&inp_adcode=$inp_adcode'>▲</a>広告コード<a href='adcode_list.php?limit=$limit&sort=desc&offset=$offset&inp_adcode=$inp_adcode'>▼</a></tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:180px\"><NOBR><tt>広告コード</tt></NOBR></th>\n";
//AREQ-77 G.Chin 2010-08-05 chg end
	$dsp_tbl .= "<th style=\"width:200px\"><NOBR><tt>広告主名</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:200px\"><NOBR><tt>代理店名</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>会員登録数</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:60px\"><NOBR><tt>操作</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:60px\"><NOBR><tt>削除</tt></NOBR></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tam_data_cnt;$i++)
	{
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($tam_fk_admaster_id[$i],$tac_fk_adcode_id,$tac_f_adcode,$tac_f_type,$tac_f_tm_stamp);
		
		//▼各種リンク
		$link_edit = "<A href='adcode_regist.php?sid=$sid&aid=$tam_fk_admaster_id[$i]&limit=$limit&offset=$offset&page=$page&sort=$sort&inp_adcode=$inp_adcode'>編集</A>";
		$link_delete = "<A href='adcode_delete.php?sid=$sid&aid=$tam_fk_admaster_id[$i]&limit=$limit&offset=$offset&page=$page&sort=$sort&inp_adcode=$inp_adcode'>削除</A>";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$tam_fk_admaster_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='left'><NOBR><tt>$tac_f_adcode</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='left'><NOBR><tt>$tam_k_adname[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='left'><NOBR><tt>$tam_f_agent_name[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$tam_f_counter[$i]</tt></NOBR></td>\n";
//G.Chin AWKT-937 2010-12-20 chg sta
/*
		$dsp_tbl .= "<td class='center'><NOBR><tt>$link_edit</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$link_delete</tt></NOBR></td>\n";
*/
		if($tac_f_adcode == "0")
		{
			$dsp_tbl .= "<td class='center'><NOBR><tt>-</tt></NOBR></td>\n";
			$dsp_tbl .= "<td class='center'><NOBR><tt>-</tt></NOBR></td>\n";
		}
		else
		{
			$dsp_tbl .= "<td class='center'><NOBR><tt>$link_edit</tt></NOBR></td>\n";
			$dsp_tbl .= "<td class='center'><NOBR><tt>$link_delete</tt></NOBR></td>\n";
		}
//G.Chin AWKT-937 2010-12-20 chg end
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= "<center>\n";
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "</center>\n";
	$dsp_tbl .= "<br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告媒体管理",$dsp_tbl);

?>
