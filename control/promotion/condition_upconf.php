<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/14												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$mode = $_REQUEST["mode"];
	
	if($mode == "delete")
	{
		$acid = $_REQUEST["acid"];
		$ins_f_adpricode = $_REQUEST["ins_f_adpricode"];
		
		//支払条件一覧取得関数
		GetTAdConditionList($fk_adcon_id,$f_type,$f_adpricode,$f_spend_value,$f_pay_value,$f_status,$f_tm_stamp,$up_cnt);
	}
	else
	{
		$acid = "";
		
		$up_cnt = $_REQUEST["up_cnt"];
		$fk_adcon_id = $_REQUEST["fk_adcon_id"];
		$f_adpricode = $_REQUEST["f_adpricode"];
		$f_status = $_REQUEST["f_status"];
		$f_type = $_REQUEST["f_type"];
		$f_spend_value = $_REQUEST["f_spend_value"];
		$f_pay_value = $_REQUEST["f_pay_value"];
		
		$ins_f_adpricode = $_REQUEST["ins_f_adpricode"];
		$ins_f_status = $_REQUEST["ins_f_status"];
		$ins_f_type = $_REQUEST["ins_f_type"];
		$ins_f_spend_value = $_REQUEST["ins_f_spend_value"];
		$ins_f_pay_value = $_REQUEST["ins_f_pay_value"];
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<FORM action='condition_list.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table class='data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><font size='-1'>データ件数</font></th>\n";
	$dsp_tbl .= "<td><font color='#5D478B' size='-1'>$up_cnt</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class='list'>\n";
	$dsp_tbl .= "<tr bgcolor='#CCCCFF'>\n";
	$dsp_tbl .= "<th style='width:80px'><tt>優先度</tt></th>\n";
	$dsp_tbl .= "<th style='width:80px'><tt>状態</tt></th>\n";
	$dsp_tbl .= "<th style='width:180px'><tt>タイプ</tt></th>\n";
	$dsp_tbl .= "<th style='width:180px'><tt>条件</tt></th>\n";
	$dsp_tbl .= "<th style='width:120px'><tt>紹介者支払ｺｲﾝ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$adpricode_str = "";
	$error_cnt = 0;
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$up_cnt;$i++)
	{
		//削除モード判定
		if($fk_adcon_id[$i] == $acid)
		{
			$bgcolor = "#C0C0C0";
		}
		else
		{
			$bgcolor = "#FFFFFF";
		}
		
		//優先順位判定
		if(ereg($f_adpricode[$i], $adpricode_str))
		{
			$f_adpricode_str = "<font color='#FF0000'>".$f_adpricode[$i]."</font>";
			$error_cnt++;
		}
		else
		{
			$f_adpricode_str = $f_adpricode[$i];
		}
		$adpricode_str .= $f_adpricode[$i];
		$adpricode_str .= " ";
		
		//▼状態
		if($f_status[$i] == 0)		$f_status_str = "有効";
		else if($f_status[$i] = 9)	$f_status_str = "無効";
		
		//▼タイプ
		if($f_type[$i] == 0)		$f_type_str = "会員紹介・会員ﾊﾞﾅｰ";
		else if($f_type[$i] == 1)	$f_type_str = "広告ｱﾌｨﾘ";
		
		$dsp_tbl .= "<tr bgcolor='$bgcolor'>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_adpricode_str</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_status_str</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_type_str</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_spend_value[$i]&nbsp;円(会員登録時0円)</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_pay_value[$i]&nbsp;ｺｲﾝ</tt></td>\n";
		
		$dsp_tbl .= "<input type='hidden' name='fk_adcon_id[]' value='$fk_adcon_id[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_type[]' value='$f_type[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_adpricode[]' value='$f_adpricode[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_spend_value[]' value='$f_spend_value[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_pay_value[]' value='$f_pay_value[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='f_status[]' value='$f_status[$i]'>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	//▼追加モード判定
	if($ins_f_adpricode != "")
	{
		$mode = "insert";
		
		//優先順位判定
		if(ereg($ins_f_adpricode, $adpricode_str))
		{
			$f_adpricode_str = "<font color='#FF0000'>".$ins_f_adpricode."</font>";
			$error_cnt++;
		}
		else
		{
			$f_adpricode_str = $ins_f_adpricode;
		}
		$adpricode_str .= $ins_f_adpricode;
		$adpricode_str .= " ";
		
		//▼状態
		if($ins_f_status == 0)		$f_status_str = "有効";
		else if($ins_f_status = 9)	$f_status_str = "無効";
		
		//▼タイプ
		if($ins_f_type == 0)		$f_type_str = "会員紹介・会員ﾊﾞﾅｰ";
		else if($ins_f_type == 1)	$f_type_str = "広告ｱﾌｨﾘ";
		
		//▼条件
		if($ins_f_spend_value == "")
		{
			$ins_f_spend_value = 0;
		}
		
		//▼紹介者支払ｺｲﾝ
		if($ins_f_pay_value == "")
		{
			$ins_f_pay_value = 0;
		}
		
		$dsp_tbl .= "<tr bgcolor='#FFFFFF'>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_adpricode_str</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_status_str</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$f_type_str</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$ins_f_spend_value&nbsp;円(会員登録時0円)</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$ins_f_pay_value&nbsp;ｺｲﾝ</tt></td>\n";
		
		$dsp_tbl .= "<input type='hidden' name='ins_f_type' value='$ins_f_type'>\n";
		$dsp_tbl .= "<input type='hidden' name='ins_f_adpricode' value='$ins_f_adpricode'>\n";
		$dsp_tbl .= "<input type='hidden' name='ins_f_spend_value' value='$ins_f_spend_value'>\n";
		$dsp_tbl .= "<input type='hidden' name='ins_f_pay_value' value='$ins_f_pay_value'>\n";
		$dsp_tbl .= "<input type='hidden' name='ins_f_status' value='$ins_f_status'>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	if($error_cnt > 0)
	{
		$dsp_tbl .= "<tt><font color='#FF0000'>※．</font>優先順位の重複が".$error_cnt."件あります。</tt><br>\n";
	}
	
	$dsp_tbl .= "<input type='hidden' name='mode' value='$mode'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_cnt' value='$up_cnt'>\n";
	$dsp_tbl .= "<input type='hidden' name='acid' value='$acid'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='更新'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='condition_list.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value='disp'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='戻る'>\n";
	$dsp_tbl .= "</FORM>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("サービスコイン設定",$dsp_tbl);

?>
