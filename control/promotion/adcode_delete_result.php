<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/07/05												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];	//AREQ-77 G.Chin 2010-08-05 add
	
	//▼支払タイプ("金銭"固定)
	$f_pay_type = 1;
	
	//▼広告タイプ("運営者広告"固定)
	$f_type = 1;
	
	$dsp_string  = "";
	$back_string = "";
	
	if($aid == "")
	{
		PrintAdminPage("広告媒体編集完了","<P>不正な処理です。</P>");
		print "不正な処理です。<br>\n";
		exit;
	}
	else
	{
		//広告主削除関数
		$ret = DeleteTAdmaster($aid);
		if($ret == false)
		{
			$dsp_string = "広告主情報の削除処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "広告主情報の削除処理に成功しました。<br>\n";
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= $back_string;
	$dsp_tbl .= "<font><b><A href='adcode_list.php?sid=$sid&limit=$limit&offset=$offset&page=$page&sort=$sort'>リストに戻る</A></b></font>\n";
/*
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
*/
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告媒体削除完了",$dsp_tbl);

?>

