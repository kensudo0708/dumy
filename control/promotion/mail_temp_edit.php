<script type="text/javascript">
    /**
     * 確認ダイアログの返り値によりフォーム送信
    */
<!--
	function submitChk ()
	{
		/* 確認ダイアログ表示 */
		var flag = confirm ('一覧に戻ります。よろしいですか？');
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/06/01												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mtid = $_REQUEST["mtid"];
	
	
	if(($mtid != "") && ($mtid != 0))
	{
		//メルマガテンプレート情報取得関数
		GetTMailTemplateInfo($mtid,$tmt_f_status,$tmt_f_name,$tmt_f_subject,$tmt_f_body,$tmt_f_picture,$tmt_f_target,$tmt_f_memo,$tmt_f_tm_stamp);
	}
	else
	{
		$tmt_fk_mail_id	= "";
		$tmt_f_status	= "";
		$tmt_f_name		= "";
		$tmt_f_subject	= "";
		$tmt_f_body		= "";
		$tmt_f_picture	= "";
		$tmt_f_target	= "";
		$tmt_f_memo		= "";
		$tmt_f_tm_stamp	= "";
	}
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class='form'>\n";
	
	$dsp_tbl .= "<FORM action='mail_temp_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt><nobr> ﾃﾝﾌﾟﾚｰﾄ名 </nobr></tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_name' value='$tmt_f_name' size='50'>&nbsp;テンプレートの名前を指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt><nobr> 送信者 </nobr></tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_target' value='$tmt_f_target' size='50'>&nbsp;送信者のメールアドレス</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt><nobr> 件名 </nobr></tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_subject' value='$tmt_f_subject' size='50'></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt><nobr> 本文 </nobr></tt></th>\n";
	$dsp_tbl .= "<td>【件名・本文の置換文字】{%member_name%}=ハンドル名<br><textarea name='f_body' cols='50' rows='20'>$tmt_f_body</textarea></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=2 style='text-align:center'>\n";
	$dsp_tbl .= "<font color='#FF0000'>※．編集後は、「更新」するか「取消」してください。これを行わないと送信できません。</font>\n";
	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<input type='hidden' name='mtid' value='$mtid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px'>更新</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mail_temp_list.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px;' onclick=\"return submitChk()\">取消</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガテンプレート設定編集",$dsp_tbl);
?>
