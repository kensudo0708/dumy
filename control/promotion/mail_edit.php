<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mrid = $_REQUEST["mrid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	$mtid = $_REQUEST["mtid"];
//	$fromreg = $_REQUEST["fromreg"];	//G.Chin 2010-07-19 del
	
	
	if(($mrid != "") && ($mrid != 0))
	{
		//メルマガ送信予約情報取得関数
		GetTMailReserveInfo($mrid,$tmr_f_subject,$tmr_f_body,$tmr_f_picture,$tmr_f_target,$tmr_f_smtp_host,$tmr_f_smtp_port,$tmr_f_send_type,$tmr_f_reserve_time,$tmr_f_send_counts,$tmr_f_reserve,$tmr_f_end_time,$tmr_f_send_to,$tmr_f_status,$tmr_f_req_stop,$tmr_f_sql,$tmr_f_sql_desc,$tmr_f_tm_stamp);
		
		$tmr_f_sql = str_replace("'", "\'", $tmr_f_sql);;
	}
	else
	{
		$tmr_f_subject		= "";
		$tmr_f_body			= "";
		$tmr_f_picture		= "";
		$tmr_f_target		= "";
		$tmr_f_smtp_host	= "";
		$tmr_f_smtp_port	= "";
		$tmr_f_send_type	= "0";
		$tmr_f_reserve_time	= "";
		$tmr_f_send_counts	= "0";
		$tmr_f_reserve		= "";
		$tmr_f_end_time		= "";
		$tmr_f_send_to		= "0";
		$tmr_f_status		= "0";
		$tmr_f_req_stop		= "";
		$tmr_f_sql			= "";
		$tmr_f_sql_desc		= "";
		$tmr_f_tm_stamp		= "";
	}
	
	//テンプレート選択の場合
	if($mtid != "")
	{
		//メルマガテンプレート情報取得関数
		GetTMailTemplateInfo($mtid,$tmr_f_status,$tmr_f_name,$tmr_f_subject,$tmr_f_body,$tmr_f_picture,$tmr_f_target,$tmr_f_memo,$tmr_f_tm_stamp);
	}
	
	//▼メルマガテンプレート選択作成
	//メルマガテンプレート名一覧取得関数
	GetTMailTemplateNameList($fk_mail_id, $f_name, $w_data_cnt);
	$template_id[0] = "";
	$template_name[0] = "-";
	for($i=0; $i<$w_data_cnt; $i++)
	{
		$template_id[$i+1] = $fk_mail_id[$i];
		$template_name[$i+1] = $f_name[$i];
	}
	$fmt_data_cnt = $w_data_cnt + 1;
	$name = "mtid";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $template_id, $template_name, $fmt_data_cnt, $select_num, $template_select);
	
//G.Chin 2010-07-19 del sta
/*
	$chk_regist = $_REQUEST["chk_regist"];
	$where_regist = "";
	$string_regist = "";
	if($chk_regist == 1)
	{
		$s_regist = $_REQUEST["s_regist"];
		$e_regist = $_REQUEST["e_regist"];
		//日付文字列作成関数
		MakeDateString($s_regist, $inp_s_regist);
		MakeDateString($e_regist, $inp_e_regist);
		$s_regist_date = $inp_s_regist." 00:00:00";
		$e_regist_date = $inp_e_regist." 23:59:59";
		$where_regist = "and tmm.f_regist_date>=\'$s_regist_date\' and tmm.f_regist_date<=\'$e_regist_date\' ";
		$string_regist = "登録日:".$s_regist."～".$e_regist."\n";
	}
	
	$chk_last = $_REQUEST["chk_last"];
	$where_last = "";
	$string_last = "";
	if($chk_last == 1)
	{
		$s_last = $_REQUEST["s_last"];
		$e_last = $_REQUEST["e_last"];
		//日付文字列作成関数
		MakeDateString($s_last, $inp_s_last);
		MakeDateString($e_last, $inp_e_last);
		$s_last_entry = $inp_s_last." 00:00:00";
		$e_last_entry = $inp_e_last." 23:59:59";
		$where_last = "and tm.f_last_entry>=\'$s_last_entry\' and tm.f_last_entry<=\'$e_last_entry\' ";
		$string_last = "最終ログイン日:".$s_last."～".$e_last."\n";
	}
	
	$chk_contact = $_REQUEST["chk_contact"];
	$where_contact = "";
	$string_contact = "";
	if($chk_contact == 1)
	{
		$s_contact = $_REQUEST["s_contact"];
		$e_contact = $_REQUEST["e_contact"];
		$where_contact = "and tm.t_contact_count>=\'$s_contact\' and tm.t_contact_count<=\'$e_contact\' ";
		$string_contact = "入札回数:".$s_contact."回～".$e_contact."回\n";
	}
	
	$chk_hummer = $_REQUEST["chk_hummer"];
	$where_hummer = "";
	$string_hummer = "";
	if($chk_hummer == 1)
	{
		$s_hummer = $_REQUEST["s_hummer"];
		$e_hummer = $_REQUEST["e_hummer"];
		$where_hummer = "and tm.t_hummer_count>=\'$s_hummer\' and tm.t_hummer_count<=\'$e_hummer\' ";
		$string_hummer = "落札回数:".$s_hummer."回～".$e_hummer."回\n";
	}
	
	$chk_total = $_REQUEST["chk_total"];
	$where_total = "";
	$string_total = "";
	if($chk_total == 1)
	{
		$s_total = $_REQUEST["s_total"];
		$e_total = $_REQUEST["e_total"];
		$where_total = "and tm.f_total_pay>=\'$s_total\' and tm.f_total_pay<=\'$e_total\' ";
		$string_total = "購入合計金額:".$s_total."円～".$e_total."円\n";
	}
	
	$chk_coin = $_REQUEST["chk_coin"];
	$where_coin = "";
	$string_coin = "";
	if($chk_coin == 1)
	{
		$s_coin = $_REQUEST["s_coin"];
		$e_coin = $_REQUEST["e_coin"];
		$where_coin = "and tm.f_coin>=\'$s_coin\' and tm.f_coin<=\'$e_coin\' ";
		$string_coin = "残り購入コイン枚数:".$s_coin."ｺｲﾝ～".$e_coin."ｺｲﾝ\n";
	}
	
	$chk_free = $_REQUEST["chk_free"];
	$where_free = "";
	$string_free = "";
	if($chk_free == 1)
	{
		$s_free = $_REQUEST["s_free"];
		$e_free = $_REQUEST["e_free"];
		$where_free = "and tm.f_free_coin>=\'$s_free\' and tm.f_free_coin<=\'$e_free\' ";
		$string_free = "残りサービスコイン枚数:".$s_free."ｺｲﾝ～".$e_free."ｺｲﾝ\n";
	}
	
	$chk_sex = $_REQUEST["chk_sex"];
	$where_sex = "";
	$string_sex = "";
	if($chk_sex == 1)
	{
		$f_sex = $_REQUEST["f_sex"];
		$where_sex = "and tmm.f_sex=\'$f_sex\' ";
		$string_sex = "性別:";
		if($f_sex == 0)
		{
			$string_sex .= "男性\n";
		}
		else
		{
			$string_sex .= "女性\n";
		}
	}
	
	$chk_birth = $_REQUEST["chk_birth"];
	$where_birthday = "";
	$string_birthday = "";
	if($chk_birth == 1)
	{
		$birth_year = $_REQUEST["birth_year"];
		$birth_month = $_REQUEST["birth_month"];
		$birth_day = $_REQUEST["birth_day"];
		if(($birth_year != "") && ($birth_month != "") && ($birth_day != ""))
		{
			$f_birthday = $birth_year."-".$birth_month."-".$birth_day;
			$where_birthday = "and tmm.f_birthday=\'$f_birthday\' ";
			$string_birthday = "誕生日:".$f_birthday."\n";
			$string_birthday = "誕生日:".$f_birthday."\n";
		}
	}
	else
	{
		$chk_age = $_REQUEST["chk_age"];
		if($chk_age == 1)
		{
			$age = $_REQUEST["age"];
			//▼現在年
			$now = time();
			$year = date("Y", $now);
			$f_birth_year = $year - $age;
			$where_birthday = "and tmm.f_birthday like \'$f_birth_year%\' ";
			$string_birthday = "年齢:".$age."(誕生日:".$f_birthday.")\n";
		}
	}
	
	$where_mailmaga = "";
	$string_mailmaga = "";
	$mailmaga = $_REQUEST["mailmaga"];
	if($mailmaga == 0)
	{
		$where_mailmaga = "tmm.f_mailmagazein_pc=1 ";
		$string_mailmaga = "送信タイプ:PC\n";
		$mail_feeld = "tmm.f_login_id ,tmm.f_handle ,tmm.f_mail_address_pc as mail_to ";
	}
	else
	{
		$where_mailmaga = "tmm.f_mailmagazein_mb=1 ";
		$string_mailmaga = "送信タイプ:MB\n";
		$mail_feeld = "tmm.f_login_id ,tmm.f_handle ,tmm.f_mail_address_mb as mail_to ";
	}
	
	//メールアドレス
	$inp_mail = $_REQUEST["inp_mail"];
	$where_inpmail = "";
	$string_inpmail = "";
	if($inp_mail != "")
	{
		if($mailmaga == 0)
		{
			$where_inpmail = "and tmm.f_mail_address_pc=\'$inp_mail\' ";
		}
		else
		{
			$where_inpmail = "and tmm.f_mail_address_mb=\'$inp_mail\' ";
		}
		$string_inpmail = "メールアドレス:".$inp_mail."\n";
	}
	
	//▼検索SQL作成
//	$where_str = $where_mailmaga.$where_regist.$where_last.$where_contact.$where_hummer.$where_total.$where_coin.$where_free.$where_sex.$where_birthday.$where_inpmail;
	$where_str = $where_mailmaga.$where_regist.$where_last.$where_contact.$where_hummer.$where_total.$where_coin.$where_free.$where_sex.$where_birthday;
	$sql_cnt = "select count(*) from auction.t_member_master as tmm,auction.t_member as tm where ".$where_str."and tmm.fk_member_id=tm.fk_member_id ";
	
	if($mailmaga == 0)
	{
		$sql = "select tmm.fk_member_id,tmm.f_handle,tmm.f_mail_address_pc as mail_to from auction.t_member_master as tmm,auction.t_member as tm where ".$where_str."and tmm.fk_member_id=tm.fk_member_id";
	}
	else
	{
		$sql = "select tmm.fk_member_id,tmm.f_handle,tmm.f_mail_address_mb as mail_to from auction.t_member_master as tmm,auction.t_member as tm where ".$where_str."and tmm.fk_member_id=tm.fk_member_id";
	}
*/
//G.Chin 2010-07-19 del end
	
//G.Chin 2010-07-19 chg sta
/*
	//▼遷移経路確認
	if($fromreg == "1")
	{
		//登録画面より来た場合
		$f_sql  = "select ";
		$f_sql .= $mail_feeld;
		$f_sql .= " from auction.t_member_master as tmm,auction.t_member as tm where ";
		$f_sql .= $where_str;
		$f_sql .= "and tmm.fk_member_id=tm.fk_member_id and tmm.f_activity = 1 ";
		$f_sql .= "order by tmm.fk_member_id ";
		//SQL実行件数取得関数
		$w_sql_cnt = str_replace("\'", "'", $sql_cnt);
		GetDataCount($w_sql_cnt,$f_send_counts);
		
		//▼SQL説明
		$f_sql_desc  = "";
		$f_sql_desc .= $string_regist;
		$f_sql_desc .= $string_last;
		$f_sql_desc .= $string_contact;
		$f_sql_desc .= $string_hummer;
		$f_sql_desc .= $string_total;
		$f_sql_desc .= $string_coin;
		$f_sql_desc .= $string_free;
		$f_sql_desc .= $string_sex;
		$f_sql_desc .= $string_birthday;
//		$f_sql_desc .= $string_inpmail;
		$f_sql_desc .= $string_mailmaga;
	}
	else
	{
		if($tmr_f_sql == "")
		{
			$f_sql  = "select ";
			$f_sql .= $mail_feeld;
			$f_sql .= " from auction.t_member_master as tmm,auction.t_member as tm where ";
			$f_sql .= $where_str;
			$f_sql .= "and tmm.fk_member_id=tm.fk_member_id and tmm.f_activity = 1 ";
			$f_sql .= "order by tmm.fk_member_id ";
		}
		else
		{
			$f_sql = $tmr_f_sql;
			
			$f_pos = strpos($f_sql, "from");
			$e_pos = strpos($f_sql, "order by");
			$w_len = $e_pos - $f_pos;
			$w_str = substr($f_sql, $f_pos, $w_len);
			$sql_cnt  = "select count(*)";
			$sql_cnt .= $w_str;
		}
		//SQL実行件数取得関数
		$w_sql_cnt = str_replace("\'", "'", $sql_cnt);
		GetDataCount($w_sql_cnt,$f_send_counts);
		
		//▼SQL説明
		if($tmr_f_sql_desc == "")
		{
			$f_sql_desc  = "";
			$f_sql_desc .= $string_regist;
			$f_sql_desc .= $string_last;
			$f_sql_desc .= $string_contact;
			$f_sql_desc .= $string_hummer;
			$f_sql_desc .= $string_total;
			$f_sql_desc .= $string_coin;
			$f_sql_desc .= $string_free;
			$f_sql_desc .= $string_sex;
			$f_sql_desc .= $string_birthday;
//			$f_sql_desc .= $string_inpmail;
			$f_sql_desc .= $string_mailmaga;
		}
		else
		{
			$f_sql_desc = $tmr_f_sql_desc;
		}
	}
*/
	if($tmr_f_sql == "")
	{
		$f_sql  = "select ";
		$f_sql .= $mail_feeld;
		$f_sql .= " from auction.t_member_master as tmm,auction.t_member as tm where ";
		$f_sql .= $where_str;
		$f_sql .= "and tmm.fk_member_id=tm.fk_member_id and tmm.f_activity=1 ";
		$f_sql .= "order by tmm.fk_member_id ";
		
		$sql_cnt  = "select count(*)";
		$sql_cnt .= " from auction.t_member_master as tmm,auction.t_member as tm where ";
		$sql_cnt .= $where_str;
		$sql_cnt .= "and tmm.fk_member_id=tm.fk_member_id and tmm.f_activity=1 ";
	}
	else
	{
		$f_sql = $tmr_f_sql;
		
		$f_pos = strpos($f_sql, "from");
		$e_pos = strpos($f_sql, "order by");
		$w_len = $e_pos - $f_pos;
		$w_str = substr($f_sql, $f_pos, $w_len);
		$sql_cnt  = "select count(*)";
		$sql_cnt .= " ";
		$sql_cnt .= $w_str;
	}
	//SQL実行件数取得関数
	$w_sql_cnt = str_replace("\'", "'", $sql_cnt);
	GetDataCount($w_sql_cnt,$f_send_counts);
	
	//▼SQL説明
	if($tmr_f_sql_desc == "")
	{
		$f_sql_desc  = "";
		$f_sql_desc .= $string_regist;
		$f_sql_desc .= $string_last;
		$f_sql_desc .= $string_contact;
		$f_sql_desc .= $string_hummer;
		$f_sql_desc .= $string_total;
		$f_sql_desc .= $string_coin;
		$f_sql_desc .= $string_free;
		$f_sql_desc .= $string_sex;
		$f_sql_desc .= $string_birthday;
//		$f_sql_desc .= $string_inpmail;
		$f_sql_desc .= $string_mailmaga;
	}
	else
	{
		$f_sql_desc = $tmr_f_sql_desc;
	}
//G.Chin 2010-07-19 chg end
	
	//▼送信日時
	if($tmr_f_reserve_time == "")
	{
		$r_hour = "";
		$r_minutes = "";
		$now = time();
		$reserve_date = date("Ymd", $now);
	}
	else
	{
		$r_year = substr($tmr_f_reserve_time, 0, 4);
		$r_month = substr($tmr_f_reserve_time, 5, 2);
		$r_day = substr($tmr_f_reserve_time, 8, 2);
		$r_hour = substr($tmr_f_reserve_time, 11, 2);
		$r_minutes = substr($tmr_f_reserve_time, 14, 2);
		$reserve_date = $r_year.$r_month.$r_day;
	}
	
	//時
	for($i=0; $i<24; $i++)
	{
		$num = $i;
		if($num < 10)
		{
			$hour_num = "0".$num;
			$hour_str = "0".$num."時";
		}
		else
		{
			$hour_num = $num;
			$hour_str = $num."時";
		}
		$reserve_hour_id[$i] = $hour_num;
		$reserve_hour_name[$i] = $hour_str;
	}
	$reserve_hour_cnt = 24;
	$name = "reserve_hour";
	$select_num = $r_hour;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $reserve_hour_id, $reserve_hour_name, $reserve_hour_cnt, $select_num, $reserve_hour_select);
	
	//分
//G.Chin 2010-08-11 chg sta
/*
	for($i=0; $i<6; $i++)
	{
		$num = $i;
		$reserve_minutes_id[$i]  = $num;
		$reserve_minutes_id[$i] .= "0";
		$reserve_minutes_name[$i]  = "";
		$reserve_minutes_name[$i] .= $num;
		$reserve_minutes_name[$i] .= "0分";
	}
	$reserve_minutes_cnt = 6;
*/
	$num = 0;
	for($i=0; $i<12; $i++)
	{
		if($i % 2 == 0)
		{
			$reserve_minutes_id[$i]  = $num;
			$reserve_minutes_id[$i] .= "0";
			$reserve_minutes_name[$i]  = "";
			$reserve_minutes_name[$i] .= $num;
			$reserve_minutes_name[$i] .= "0分";
		}
		else
		{
			$reserve_minutes_id[$i]  = $num;
			$reserve_minutes_id[$i] .= "5";
			$reserve_minutes_name[$i]  = "";
			$reserve_minutes_name[$i] .= $num;
			$reserve_minutes_name[$i] .= "5分";
			$num++;
		}
	}
	$reserve_minutes_cnt = 12;
//G.Chin 2010-08-11 chg end
	$name = "reserve_minutes";
	$select_num = $r_minutes;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $reserve_minutes_id, $reserve_minutes_name, $reserve_minutes_cnt, $select_num, $reserve_minutes_select);
	
	//▼送信対象
	$f_send_to = $mailmaga;
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='mail_regist.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table class='form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>該当件数</th>\n";
	$dsp_tbl .= "<td colspan=5 bordercolor='#C0C0C0'><font color='#B22222'>".number_format($f_send_counts)." 人</font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>検索条件</th>\n";
	$dsp_tbl .= "<td colspan=5><font color='#B22222'>$f_sql_desc</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt><nobr> 条件指定 </nobr></tt></th>\n";
	$dsp_tbl .= "<td colspan=5>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='inp_mail' value='$inp_mail'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='mtid' value='$mtid'>\n";
	$dsp_tbl .= "<button type='submit' style='width:120'>条件指定</button>&nbsp;<font color='#ff0000'>※．</font>条件指定した場合、上記の条件はクリアされます。\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</FORM>\n";
	
	$w_sql = str_replace("\'", "'", $f_sql);	//G.Chin 2010-07-19 add
	
	$dsp_tbl .= "<th>送信予定者</th>\n";
//G.Chin 2010-07-19 chg sta
/*
	$dsp_tbl .= "<td colspan=5 bordercolor='#C0C0C0'><font color='#B22222'>
		<form  action='mail_send_list.php' method='POST' target='_blank'>
		<input type='hidden' name='sql' value='$sql'>
		<input type='hidden' name='page' value='1'>
		<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='確認'>
		</form></font>\n";
*/
	$dsp_tbl .= "<td colspan=5 bordercolor='#C0C0C0'><font color='#B22222'>
		<form  action='mail_send_list.php' method='POST' target='_blank'>
		<input type='hidden' name='sql' value=\"$w_sql\">
		<input type='hidden' name='page' value='1'>
		<button type='submit' style='width=110px;'>確認</button>
		</form></font>\n";
//G.Chin 2010-07-19 chg end
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<FORM action='mail_edit.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>テンプレートの適用</th>\n";
	$dsp_tbl .= "<td colspan=3>メールテンプレートを適用します<br>$template_select&nbsp;&nbsp;<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='適用'></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'></font></td>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
//	$dsp_tbl .= "<input type='hidden' name='fromreg' value=''>\n";	//G.Chin 2010-07-19 del
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</FORM>\n";
	
	$dsp_tbl .= "<FORM action='mail_edit_result.php' method='POST' ENCTYPE='multipart/form-data'>\n";
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=left><tt><nobr> メールアドレス </nobr></tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$inp_mail</font></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'>受信者のメールアドレス</font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>送信日時</th>\n";
	$dsp_tbl .= "<td colspan=3>配信する日時を指定します<br><input type='text' name='reserve_date' value='$reserve_date' size='10'>&nbsp;$reserve_hour_select&nbsp;$reserve_minutes_select</td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>送信者</th>\n";
	$dsp_tbl .= "<td colspan=3>送信者のメールアドレス<br><input type='text' name='f_target' value='$tmr_f_target' size='50'></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>件名</th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_subject' value='$tmr_f_subject' size='50'></font></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>本文</th>\n";
	$dsp_tbl .= "<td colspan=3>【本文の置換文字】{%member_name%}=ハンドル名<br><textarea name='f_body' cols='50' rows='20'>$tmr_f_body</textarea></td>\n";
	$dsp_tbl .= "<td colspan=2></td>\n";
        $dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=6 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='inp_mail' value='$inp_mail'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_send_counts' value='$f_send_counts'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_send_to' value='$f_send_to'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_status' value=\"$tmr_f_status\">\n";
	$dsp_tbl .= "<input type='hidden' name='f_sql' value=\"$f_sql\">\n";
	$dsp_tbl .= "<input type='hidden' name='f_sql_desc' value='$f_sql_desc'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px;'>登録</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガ設定編集",$dsp_tbl);
?>
