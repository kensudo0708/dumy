<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/07/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$sid = $_REQUEST["sid"];
	$mrid = $_REQUEST["mrid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	$mtid = $_REQUEST["mtid"];
	
	
	$tmr_f_subject		= "";
	$tmr_f_body			= "";
	$tmr_f_picture		= "";
	$tmr_f_target		= "";
	$tmr_f_smtp_host	= "";
	$tmr_f_smtp_port	= "";
	$tmr_f_send_type	= "0";
	$tmr_f_reserve_time	= "";
	$tmr_f_send_counts	= "0";
	$tmr_f_reserve		= "";
	$tmr_f_end_time		= "";
	$tmr_f_send_to		= "0";
	$tmr_f_status		= "0";
	$tmr_f_req_stop		= "";
	$tmr_f_sql			= "";
	$tmr_f_sql_desc		= "";
	$tmr_f_tm_stamp		= "";
	
	//テンプレート選択の場合
	if($mtid != "")
	{
		//メルマガテンプレート情報取得関数
		GetTMailTemplateInfo($mtid,$tmr_f_status,$tmr_f_name,$tmr_f_subject,$tmr_f_body,$tmr_f_picture,$tmr_f_target,$tmr_f_memo,$tmr_f_tm_stamp);
	}
	
	//▼メルマガテンプレート選択作成
	//メルマガテンプレート名一覧取得関数
	GetTMailTemplateNameList($fk_mail_id, $f_name, $w_data_cnt);
	$template_id[0] = "";
	$template_name[0] = "-";
	for($i=0; $i<$w_data_cnt; $i++)
	{
		$template_id[$i+1] = $fk_mail_id[$i];
		$template_name[$i+1] = $f_name[$i];
	}
	$fmt_data_cnt = $w_data_cnt + 1;
	$name = "mtid";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $template_id, $template_name, $fmt_data_cnt, $select_num, $template_select);
	
        //落札履歴、コイン購入実績ある会員検索機能追加
        $search = $_REQUEST["search"];
        $sql_where='';
        if($search=='andsearch'){
            $sql_where='and';
        }else{
            $sql_where='or';
        }
	
	$chk_regist = $_REQUEST["chk_regist"];
	$where_regist = "";
	$string_regist = "";
	if($chk_regist == 1)
	{
		$s_regist = $_REQUEST["s_regist"];
		$e_regist = $_REQUEST["e_regist"];
		//日付文字列作成関数
		MakeDateString($s_regist, $inp_s_regist);
		MakeDateString($e_regist, $inp_e_regist);
		$s_regist_date = $inp_s_regist." 00:00:00";
		$e_regist_date = $inp_e_regist." 23:59:59";
		$where_regist = $sql_where." tmm.f_regist_date>=\'$s_regist_date\' and tmm.f_regist_date<=\'$e_regist_date\' ";
		$string_regist = "登録日:".$s_regist."～".$e_regist."\n";
	}
	
	$chk_last = $_REQUEST["chk_last"];
	$where_last = "";
	$string_last = "";
	if($chk_last == 1)
	{
		$s_last = $_REQUEST["s_last"];
		$e_last = $_REQUEST["e_last"];
		//日付文字列作成関数
		MakeDateString($s_last, $inp_s_last);
		MakeDateString($e_last, $inp_e_last);
		$s_last_entry = $inp_s_last." 00:00:00";
		$e_last_entry = $inp_e_last." 23:59:59";
		$where_last = $sql_where." tm.f_last_entry>=\'$s_last_entry\' and tm.f_last_entry<=\'$e_last_entry\' ";
		$string_last = "最終ログイン日:".$s_last."～".$e_last."\n";
	}
	
	$chk_contact = $_REQUEST["chk_contact"];
	$where_contact = "";
	$string_contact = "";
	if($chk_contact == 1)
	{
		$s_contact = $_REQUEST["s_contact"];
		$e_contact = $_REQUEST["e_contact"];
		$where_contact = $sql_where." tm.t_contact_count>=\'$s_contact\' and tm.t_contact_count<=\'$e_contact\' ";
		$string_contact = "入札回数:".$s_contact."回～".$e_contact."回\n";
	}
	
	$chk_hummer = $_REQUEST["chk_hummer"];
	$where_hummer = "";
	$string_hummer = "";
	if($chk_hummer == 1)
	{
		$s_hummer = $_REQUEST["s_hummer"];
		$e_hummer = $_REQUEST["e_hummer"];
		$where_hummer = $sql_where." tm.t_hummer_count>=\'$s_hummer\' and tm.t_hummer_count<=\'$e_hummer\' ";
		$string_hummer = "落札回数:".$s_hummer."回～".$e_hummer."回\n";
	}
	
	$chk_total = $_REQUEST["chk_total"];
	$where_total = "";
	$string_total = "";
	if($chk_total == 1)
	{
		$s_total = $_REQUEST["s_total"];
		$e_total = $_REQUEST["e_total"];
		$where_total = $sql_where." tm.f_total_pay>=\'$s_total\' and tm.f_total_pay<=\'$e_total\' ";
		$string_total = "購入合計金額:".$s_total."円～".$e_total."円\n";
	}
	
	$chk_coin = $_REQUEST["chk_coin"];
	$where_coin = "";
	$string_coin = "";
	if($chk_coin == 1)
	{
		$s_coin = $_REQUEST["s_coin"];
		$e_coin = $_REQUEST["e_coin"];
		$where_coin = $sql_where." tm.f_coin>=\'$s_coin\' and tm.f_coin<=\'$e_coin\' ";
		$string_coin = "残り購入コイン枚数:".$s_coin."ｺｲﾝ～".$e_coin."ｺｲﾝ\n";
	}
	
	$chk_free = $_REQUEST["chk_free"];
	$where_free = "";
	$string_free = "";
	if($chk_free == 1)
	{
		$s_free = $_REQUEST["s_free"];
		$e_free = $_REQUEST["e_free"];
		$where_free = $sql_where." tm.f_free_coin>=\'$s_free\' and tm.f_free_coin<=\'$e_free\' ";
		$string_free = "残りサービスコイン枚数:".$s_free."ｺｲﾝ～".$e_free."ｺｲﾝ\n";
	}
	
	$chk_sex = $_REQUEST["chk_sex"];
	$where_sex = "";
	$string_sex = "";
	if($chk_sex == 1)
	{
		$f_sex = $_REQUEST["f_sex"];
		$where_sex = $sql_where." tmm.f_sex=\'$f_sex\' ";
		$string_sex = "性別:";
		if($f_sex == 0)
		{
			$string_sex .= "男性\n";
		}
		else
		{
			$string_sex .= "女性\n";
		}
	}
	
	$chk_birth = $_REQUEST["chk_birth"];
	$where_birthday = "";
	$string_birthday = "";
	if($chk_birth == 1)
	{
		$birth_year = $_REQUEST["birth_year"];
		$birth_month = $_REQUEST["birth_month"];
		$birth_day = $_REQUEST["birth_day"];
		if(($birth_year != "") && ($birth_month != "") && ($birth_day != ""))
		{
			$f_birthday = $birth_year."-".$birth_month."-".$birth_day;
			$where_birthday = $sql_where." tmm.f_birthday=\'$f_birthday\' ";
			$string_birthday = "誕生日:".$f_birthday."\n";
			$string_birthday = "誕生日:".$f_birthday."\n";
		}
	}
	else
	{
		$chk_age = $_REQUEST["chk_age"];
		if($chk_age == 1)
		{
			$age = $_REQUEST["age"];
			//▼現在年
			$now = time();
			$year = date("Y", $now);
			$f_birth_year = $year - $age;
			$where_birthday = $sql_where." tmm.f_birthday like \'$f_birth_year%\' ";
			$string_birthday = "年齢:".$age."(誕生日:".$f_birthday.")\n";
		}
	}
	
	$chk_member_id = isset($_REQUEST["chk_member_id"])?$_REQUEST["chk_member_id"]:"";
	$where_member_id="";
	$string_member_id="";
	if($chk_member_id==1) {
		$f_member_id=isset($_REQUEST["member_id"])?$_REQUEST["member_id"]:"";
		$where_member_id=$sql_where." tmm.fk_member_id=".$f_member_id." ";
		$string_member_id="会員ID:".$f_member_id;
	}
	
	
	$chk_login_id= isset($_REQUEST["chk_login_id"])?$_REQUEST["chk_login_id"]:0;
	$where_login_id = "";
	$string_login_id = "";
	if($chk_login_id == 1){
		$f_login_id = isset($_REQUEST["login_id"])?$_REQUEST["login_id"]:"";
		$where_login_id = $sql_where." tmm.f_login_id=\'".$f_login_id."\' ";
		$string_login_id = "ログインID:".$f_login_id;
	}
	
	
	$chk_handle= isset($_REQUEST["chk_handle"])?$_REQUEST["chk_handle"]:0;
	$where_handle = "";
	$string_handle = "";
	if($chk_handle == 1){
		$f_handle = isset($_REQUEST["handle"])?$_REQUEST["handle"]:"";
		$where_handle = $sql_where." tmm.f_handle=\'".$f_handle."\' ";
		$string_handle = "ハンドル名:".$f_handle;
	}

//kensudo 2011-01-19 add start
	$chk_ad     = isset($_REQUEST["chk_ad"])?$_REQUEST["chk_ad"]:0;
	$where_ad   = "";
	$string_ad  = "";
	$inner_ad   = "";
	if($chk_ad == 1){
		$f_adcode  = isset($_REQUEST["ad"])?$_REQUEST["ad"]:"";
		//$where_ad  = $sql_where." tmm.fk_adcode=\'".$f_adcode."\' ";
$inner_ad = "inner join auction.t_adcode as ta on tmm.fk_parent_ad = ta.fk_adcode_id";
$where_ad  = $sql_where." (tmm.fk_parent_ad = ta.fk_adcode_id and ta.f_adcode =\'".$f_adcode."\') ";
		$string_ad = "広告コード:".$f_adcode;
	}
	$chk_grp       = isset($_REQUEST["chk_grp"])?$_REQUEST["chk_grp"]:0;
	$where_grp     = "";
	$string_grp    = "";
	if($chk_grp   == 1){
		$f_member_group  = isset($_REQUEST["f_member_group"])?$_REQUEST["f_member_group"]:"";
		$where_grp  = $sql_where." tmm.fk_member_group_id=\'".$f_member_group."\' ";
		$string_grp = "会員グループコード:".$f_member_group;
	}
	//kensudo 2011-01-19 add end

        //コイン購入履歴と落札履歴がある会員検索機能追加
//        $chk_bidcount = $_REQUEST["chk_bidcount"];
//	$where_bidcount = "";
//	$string_bidcount = "";
//	if($chk_bidcount == 1)
//	{
//		$s_bidcount = $_REQUEST["s_bidcount"];
//		$e_bidcount = $_REQUEST["e_bidcount"];
//		$where_bidcount = $sql_where."tm.t_contact_count>=$s_bidcount ".$sql_where." tm.t_contact_count<=$e_bidcount ";
//		$string_bidcount = "落札回数:".$s_bidcount."回～".$e_bidcount."回\n";
//	}

//        $chk_money = $_REQUEST["chk_money"];
//	$where_money = "";
//	$string_money = "";
//	if($chk_money == 1)
//	{
//		$s_money = $_REQUEST["s_money"];
//		$e_money = $_REQUEST["e_money"];
//		$where_money = $sql_where."tm.f_total_pay>=$s_money and tm.f_total_pay<=$e_money ";
//		$string_money = "コイン購入実績:".$s_contact."円～".$e_contact."円\n";
//	}

        $chk_activity = $_REQUEST["chk_activity"];
	$where_activity = "";
	$string_activity = "";
        $where_member_type=' and tmm.f_activity = 1 ';
	if($chk_activity == 1)
	{
		$where_member_type = " ".$sql_where." (tmm.f_activity = 1 or tmm.f_activity = 2) ";
		$string_activity = "退会者も含める\n";
	}

	$where_mailmaga = "";
	$string_mailmaga = "";
	$mailmaga = $_REQUEST["mailmaga"];
	if($mailmaga == 0)
	{
		$where_mailmaga = "tmm.f_mailmagazein_pc=1 ";
		$string_mailmaga = "送信タイプ:PC\n";
		$mail_feeld = "tmm.f_login_id ,tmm.f_handle ,tmm.f_mail_address_pc as mail_to ";
	}
	else
	{
		$where_mailmaga = "tmm.f_mailmagazein_mb=1 ";
		$string_mailmaga = "送信タイプ:MB\n";
		$mail_feeld = "tmm.f_login_id ,tmm.f_handle ,tmm.f_mail_address_mb as mail_to ";
	}


/*
	//メールアドレス
	$inp_mail = $_REQUEST["inp_mail"];
	$where_inpmail = "";
	$string_inpmail = "";
	if($inp_mail != "")
	{
		if($mailmaga == 0)
		{
			$where_inpmail = "and tmm.f_mail_address_pc=\'$inp_mail\' ";
		}
		else
		{
			$where_inpmail = "and tmm.f_mail_address_mb=\'$inp_mail\' ";
		}
		$string_inpmail = "メールアドレス:".$inp_mail."\n";
	}
*/

        
	//▼検索SQL作成
//	$where_str = $where_mailmaga.$where_regist.$where_last.$where_contact.$where_hummer.$where_total.$where_coin.$where_free.$where_sex.$where_birthday.$where_inpmail;
//	$where_str = $where_mailmaga.$where_regist.$where_last.$where_contact.$where_hummer.$where_total.$where_coin.$where_free.$where_sex.$where_birthday;
//kensudo 2011-01-19 comment
//	$where_str = $where_mailmaga.$where_regist.$where_last.$where_contact.$where_hummer.$where_total.$where_coin.$where_free.$where_sex.$where_birthday;
//kensudo 2011-01-19 modify start
	$where_str = $where_mailmaga.$where_regist.$where_last.$where_contact.$where_hummer.$where_total.$where_coin.$where_free.$where_sex.$where_birthday.$where_ad.$where_grp;
//kensudo 2011-01-19 modify end

	$where_str .= $where_member_id;
	$where_str .= $where_login_id;
	$where_str .= $where_handle;

//kensudo 2011-01-21 add start
		$sql_cnt = "select count(*) from auction.t_member_master as tmm inner join auction.t_member as tm on tmm.fk_member_id=tm.fk_member_id $inner_ad where ".$where_str." ";
		if($mailmaga == 0)
		{
			$sql = "select tmm.fk_member_id,tmm.f_handle,tmm.f_mail_address_pc as mail_to from auction.t_member_master as tmm inner join auction.t_member as tm on tmm.fk_member_id=tm.fk_member_id $inner_ad where ".$where_str." ";
		}
		else
		{
			$sql = "select tmm.fk_member_id,tmm.f_handle,tmm.f_mail_address_mb as mail_to from auction.t_member_master as tmm inner join auction.t_member as tm on tmm.fk_member_id=tm.fk_member_id $inner_ad where ".$where_str." ";
		}
//kensudo 2011-11-21 add end

//	$sql_cnt = "select count(*) from auction.t_member_master as tmm,auction.t_member as tm,auction.t_adcode as ta where ".$where_str." and tmm.fk_member_id=tm.fk_member_id ";
//	if($mailmaga == 0)
//	{
//		$sql = "select tmm.fk_member_id,tmm.f_handle,tmm.f_mail_address_pc as mail_to from auction.t_member_master as tmm,auction.t_member as tm,auction.t_adcode as ta where ".$where_str." and tmm.fk_member_id=tm.fk_member_id";
//	}
//	else
//	{
//		$sql = "select tmm.fk_member_id,tmm.f_handle,tmm.f_mail_address_mb as mail_to from auction.t_member_master as tmm,auction.t_member as tm,auction.t_adcode as ta where ".$where_str." and tmm.fk_member_id=tm.fk_member_id";
//	}
	
	//▼SQL作成
	$f_sql  = "select ";
	$f_sql .= $mail_feeld;
	$f_sql .= " from auction.t_member_master as tmm,auction.t_member as tm where ";
	$f_sql .= $where_str;
//	$f_sql .= "and tmm.fk_member_id=tm.fk_member_id and tmm.f_activity = 1 ";
        $f_sql .= "and tmm.fk_member_id=tm.fk_member_id ".$where_member_type;
	$f_sql .= "order by tmm.fk_member_id ";
	//SQL実行件数取得関数
	$w_sql_cnt = str_replace("\'", "'", $sql_cnt);
	GetDataCount($w_sql_cnt,$f_send_counts);
	
        //コイン購入実績、落札履歴がある会員検索追加
        GetDataCount($all_cnt,$counts);
        $send_counts='';
        if ($f_send_counts!=''){
            $send_counts=$f_send_counts;
        }else{
            $send_counts=$counts;
        }
	
	//▼SQL説明
	$f_sql_desc  = "";
	$f_sql_desc .= $string_regist;
	$f_sql_desc .= $string_last;
	$f_sql_desc .= $string_contact;
	$f_sql_desc .= $string_hummer;
	$f_sql_desc .= $string_total;
	$f_sql_desc .= $string_coin;
	$f_sql_desc .= $string_free;
	$f_sql_desc .= $string_sex;
	$f_sql_desc .= $string_birthday;
//	$f_sql_desc .= $string_inpmail;
	$f_sql_desc .= $string_member_id;
	$f_sql_desc .= $string_login_id;
	$f_sql_desc .= $string_handle;
	$f_sql_desc .= $string_mailmaga;
        //コイン購入実績、落札履歴がある会員検索追加
       // $f_sql_desc .= $string_bidcount;
       // $f_sql_desc .= $string_money;
        $f_sql_desc .= $string_activity;
//kensudo 2011-01-19 add start
	$f_sql_desc .= $string_ad;
	$f_sql_desc .= $string_grp;
//kensudo 2011-01-19 add end 

	
	//▼送信日時
	if($tmr_f_reserve_time == "")
	{
		$r_hour = "";
		$r_minutes = "";
		$now = time();
		$reserve_date = date("Ymd", $now);
	}
	else
	{
		$r_year = substr($tmr_f_reserve_time, 0, 4);
		$r_month = substr($tmr_f_reserve_time, 5, 2);
		$r_day = substr($tmr_f_reserve_time, 8, 2);
		$r_hour = substr($tmr_f_reserve_time, 11, 2);
		$r_minutes = substr($tmr_f_reserve_time, 14, 2);
		$reserve_date = $r_year.$r_month.$r_day;
	}
	
	//時
	for($i=0; $i<24; $i++)
	{
		$num = $i;
		if($num < 10)
		{
			$hour_num = "0".$num;
			$hour_str = "0".$num."時";
		}
		else
		{
			$hour_num = $num;
			$hour_str = $num."時";
		}
		$reserve_hour_id[$i] = $hour_num;
		$reserve_hour_name[$i] = $hour_str;
	}
	$reserve_hour_cnt = 24;
	$name = "reserve_hour";
	$select_num = $r_hour;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $reserve_hour_id, $reserve_hour_name, $reserve_hour_cnt, $select_num, $reserve_hour_select);
	
	//分
//G.Chin 2010-08-11 chg sta
/*
	for($i=0; $i<6; $i++)
	{
		$num = $i;
		$reserve_minutes_id[$i]  = $num;
		$reserve_minutes_id[$i] .= "0";
		$reserve_minutes_name[$i]  = "";
		$reserve_minutes_name[$i] .= $num;
		$reserve_minutes_name[$i] .= "0分";
	}
	$reserve_minutes_cnt = 6;
*/
	$num = 0;
	for($i=0; $i<12; $i++)
	{
		if($i % 2 == 0)
		{
			$reserve_minutes_id[$i]  = $num;
			$reserve_minutes_id[$i] .= "0";
			$reserve_minutes_name[$i]  = "";
			$reserve_minutes_name[$i] .= $num;
			$reserve_minutes_name[$i] .= "0分";
		}
		else
		{
			$reserve_minutes_id[$i]  = $num;
			$reserve_minutes_id[$i] .= "5";
			$reserve_minutes_name[$i]  = "";
			$reserve_minutes_name[$i] .= $num;
			$reserve_minutes_name[$i] .= "5分";
			$num++;
		}
	}
	$reserve_minutes_cnt = 12;
//G.Chin 2010-08-11 chg end
	$name = "reserve_minutes";
	$select_num = $r_minutes;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $reserve_minutes_id, $reserve_minutes_name, $reserve_minutes_cnt, $select_num, $reserve_minutes_select);
	
	//▼送信対象
	$f_send_to = $mailmaga;
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='mail_regist.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table class='form' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style=\"width:140px\">該当件数</th>\n";
	$dsp_tbl .= "<td><font color='#B22222'>$send_counts 人</font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>検索条件</th>\n";
	$dsp_tbl .= "<td><font color='#B22222'>$f_sql_desc</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>条件指定</th>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='inp_mail' value='$inp_mail'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='mtid' value='$mtid'>\n";
	$dsp_tbl .= "<button type='submit' class='white' style='width:120;'>条件指定</button>&nbsp;<font color='#ff0000'>※．</font>条件指定した場合、上記の条件はクリアされます。\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</FORM>\n";
	
	$sql = str_replace("\'", "'", $sql);
	$dsp_tbl .= "<th>送信予定者</th>\n";
	$dsp_tbl .= "<td><font color='#B22222'>
		<form  action='mail_send_list.php' method='POST' target='_blank'>
		<input type='hidden' name='sql' value=\"$sql\">
		<input type='hidden' name='page' value='1'>
		<button type='submit' style='width:120px;'>確認</button>
		</form></font>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<FORM action='mail_regist_input.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>テンプレートの適用</th>\n";
	$dsp_tbl .= "<td>メールテンプレートを適用します<br>$template_select&nbsp;&nbsp;<button type='submit' style='width:110px;'>適用</button></td>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_regist' value='$chk_regist'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_regist' value='$s_regist'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_regist' value='$e_regist'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_last' value='$chk_last'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_last' value='$s_last'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_last' value='$e_last'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_contact' value='$chk_contact'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_contact' value='$s_contact'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_contact' value='$e_contact'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_hummer' value='$chk_hummer'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_hummer' value='$s_hummer'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_hummer' value='$e_hummer'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_total' value='$chk_total'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_total' value='$s_total'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_total' value='$e_total'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_coin' value='$chk_coin'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_coin' value='$s_coin'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_coin' value='$e_coin'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_free' value='$chk_free'>\n";
	$dsp_tbl .= "<input type='hidden' name='s_free' value='$s_free'>\n";
	$dsp_tbl .= "<input type='hidden' name='e_free' value='$e_free'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_sex' value='$chk_sex'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_sex' value='$f_sex'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_birth' value='$chk_birth'>\n";
	$dsp_tbl .= "<input type='hidden' name='birth_year' value='$birth_year'>\n";
	$dsp_tbl .= "<input type='hidden' name='birth_month' value='$birth_month'>\n";
	$dsp_tbl .= "<input type='hidden' name='birth_day' value='$birth_day'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_age' value='$chk_age'>\n";
	$dsp_tbl .= "<input type='hidden' name='age' value='$age'>\n";
//G.Chin 2010-08-16 chg sta
/*
        $dsp_tbl .= "<input type='hidden' name='chk_member_id' value='$chk_member_id'>\n";
        $dsp_tbl .= "<input type='hidden' name='f_member_id' value='$f_member_id'>\n";
        $dsp_tbl .= "<input type='hidden' name='chk_login_id' value='$chk_login_id'>\n";
        $dsp_tbl .= "<input type='hidden' name='f_login_id' value='$f_login_id'>\n";
        $dsp_tbl .= "<input type='hidden' name='chk_handle' value='$chk_handle'>\n";
        $dsp_tbl .= "<input type='hidden' name='f_handle' value='$f_handle'>\n";
*/
	$dsp_tbl .= "<input type='hidden' name='chk_member_id' value='$chk_member_id'>\n";
	$dsp_tbl .= "<input type='hidden' name='member_id' value='$f_member_id'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_login_id' value='$chk_login_id'>\n";
	$dsp_tbl .= "<input type='hidden' name='login_id' value='$f_login_id'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_handle' value='$chk_handle'>\n";
	$dsp_tbl .= "<input type='hidden' name='handle' value='$f_handle'>\n";
//G.Chin 2010-08-16 chg end
	$dsp_tbl .= "<input type='hidden' name='mailmaga' value='$mailmaga'>\n";
	$dsp_tbl .= "<input type='hidden' name='search' value='$search'>\n";	//G.Chin AWKT-564 2010-10-27 add
//kensudo 2011-01-19 add start
	$dsp_tbl .= "<input type='hidden' name='chk_ad' value='$chk_ad'>\n";
	$dsp_tbl .= "<input type='hidden' name='ad' value='$f_adcode'>\n";
	$dsp_tbl .= "<input type='hidden' name='chk_grp' value='$chk_grp'>\n";
	$dsp_tbl .= "<input type='hidden' name='grp' value='$f_member_group'>\n";
//kensudo 2011-01-19 add end 
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mail_regist_result.php' method='POST' ENCTYPE='multipart/form-data'>\n";
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=left><tt><nobr> メールアドレス </nobr></tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$inp_mail</font></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'>受信者のメールアドレス</font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>送信日時</th>\n";
	$dsp_tbl .= "<td>配信する日時を指定します<br><input type='text' name='reserve_date' value='$reserve_date' size='10'>&nbsp;$reserve_hour_select&nbsp;$reserve_minutes_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>送信者</th>\n";
	$dsp_tbl .= "<td>送信者のメールアドレス<br><input type='text' name='f_target' value='$tmr_f_target' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>件名</th>\n";
	$dsp_tbl .= "<td><font color='#B22222'><input type='text' name='f_subject' value='$tmr_f_subject' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>本文</th>\n";
	$dsp_tbl .= "<td>【本文の置換文字】{%member_name%}=ハンドル名<br><textarea name='f_body' cols='50' rows='20'>$tmr_f_body</textarea></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=2 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='inp_mail' value='$inp_mail'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_send_counts' value='$f_send_counts'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_send_to' value='$f_send_to'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_status' value=\"$tmr_f_status\">\n";
	$dsp_tbl .= "<input type='hidden' name='f_sql' value=\"$f_sql\">\n";
	$dsp_tbl .= "<input type='hidden' name='f_sql_desc' value='$f_sql_desc'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px'>登録</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガ設定登録入力",$dsp_tbl);
?>
