<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/10/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];
	
	$afflt_status = $_REQUEST["afflt_status"];
	$f_regist = $_REQUEST["f_regist"];
	$f_regist_value = $_REQUEST["f_regist_value"];
	$f_first_buy = $_REQUEST["f_first_buy"];
	$f_first_buy_price = $_REQUEST["f_first_buy_price"];
	$f_first_buy_value = $_REQUEST["f_first_buy_value"];
	$f_terminal = $_REQUEST["f_terminal"];
	$f_type = $_REQUEST["f_type"];
	$f_bank_pay = $_REQUEST["f_bank_pay"];
	$f_report = $_REQUEST["f_report"];
	
	$f_top_pc = $_REQUEST["f_top_pc"];
	$f_top_mb = $_REQUEST["f_top_mb"];
	$f_reg_pc = $_REQUEST["f_reg_pc"];
	$f_reg_mb = $_REQUEST["f_reg_mb"];
	$f_buy_pc = $_REQUEST["f_buy_pc"];
	$f_buy_mb = $_REQUEST["f_buy_mb"];
	
	//▼支払タイプ("金銭"固定)
	$f_pay_type = 1;
	
	//▼表示タグ
	$f_top_pc = str_replace("'", "\'", $f_top_pc);
	$f_top_mb = str_replace("'", "\'", $f_top_mb);
	$f_reg_pc = str_replace("'", "\'", $f_reg_pc);
	$f_reg_mb = str_replace("'", "\'", $f_reg_mb);
	$f_buy_pc = str_replace("'", "\'", $f_buy_pc);
	$f_buy_mb = str_replace("'", "\'", $f_buy_mb);
	
	$dsp_string  = "";
	$back_string = "";
	
	if($aid == "")
	{
		PrintAdminPage("広告媒体編集完了","<P>不正な処理です。</P>");
		print "不正な処理です。<br>\n";
		exit;
	}
	else
	{
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($aid,$ta_fk_adcode_id,$ta_f_adcode,$ta_f_type,$ta_f_tm_stamp);
		
		//アフィリエイト広告件数取得関数２
		GetTAffiliateCount2($ta_fk_adcode_id,$afflt_cnt);
		if($afflt_cnt > 0)
		{
			//アフィリエイト広告更新関数２
			$ret = UpdateTAffiliate2($ta_fk_adcode_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_type,$afflt_status,$f_bank_pay,$f_report,$f_top_pc,$f_top_mb,$f_reg_pc,$f_reg_mb,$f_buy_pc,$f_buy_mb);
			if($ret == false)
			{
				$dsp_string = "アフィリエイト更新処理に失敗しました。<br>\n";
			}
			else
			{
				$dsp_string = "広告主情報の更新処理に成功しました。<br>\n";
			}
		}
		else
		{
			//▼アフィリエイト有効の場合
			if($afflt_status == 1)
			{
				//アフィリエイト広告登録関数２
				$ret = RegistTAffiliate2($ta_fk_adcode_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_type,$afflt_status,$f_bank_pay,$f_report,$f_top_pc,$f_top_mb,$f_reg_pc,$f_reg_mb,$f_buy_pc,$f_buy_mb);
				if($ret == false)
				{
					$dsp_string = "アフィリエイト登録処理に失敗しました。<br>\n";
				}
				else
				{
					$dsp_string = "広告主情報の更新処理に成功しました。<br>\n";
				}
			}
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= $back_string;
	$dsp_tbl .= "<font><b><A href='adcode_list.php?sid=$sid&limit=$limit&offset=$offset&page=$page&sort=$sort'>リストに戻る</A></b></font>\n";
/*
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
*/
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告アフィリエイト編集完了",$dsp_tbl);

?>

