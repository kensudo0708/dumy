<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/06/01												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	
	//メルマガテンプレート一覧取得関数
	GetTMailTemplateList($tmt_fk_mail_id,$tmt_f_status,$tmt_f_name,$tmt_f_subject,$tmt_f_body,$tmt_f_picture,$tmt_f_target,$tmt_f_memo,$tmt_f_tm_stamp,$tmt_data_cnt);
	
	//■表示一覧
	$dsp_tbl  = "";
	$dsp_tbl .= "<table class='list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style='width:60px'>ID</th>\n";
	$dsp_tbl .= "<th style='width:240px'>テンプレート名</th>\n";
	$dsp_tbl .= "<th style='width:300px'>件名</th>\n";
	$dsp_tbl .= "<th style='width:60px'>操作</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tmt_data_cnt;$i++)
	{
		//▼各種リンク
		$link_edit = "<A href='mail_temp_edit.php?sid=$sid&mtid=$tmt_fk_mail_id[$i]'>編集</A>";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tmt_fk_mail_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td><NOBR><tt>$tmt_f_name[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td><NOBR><tt>$tmt_f_subject[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$link_edit</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	$dsp_tbl .= "</table><br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガテンプレート一覧",$dsp_tbl);
?>
