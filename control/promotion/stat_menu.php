<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	//▼現在日付
	$now = time();
	$yesterday = $now - 86400;
	$inp_s_date = date("Ym01", $now);
//	$inp_e_date = date("Ymd", $yesterday);
	$inp_e_date = date("Ymd", $now);
	$inp_s_month = date("Ym", $now);
	$inp_e_month = date("Ym", $now);
	
	//▼広告主選択作成
//AREQ-77 G.Chin 2010-08-05 chg sta
/*
	//広告主名一覧取得関数
	GetFAdNameList($fa_fk_admaster_id, $fa_k_adname, $fa_data_cnt);
*/
	//広告主名一覧取得関数
	$order_str = "order by k_adname ";
	GetFAdNameList($order_str, $fa_fk_admaster_id, $fa_k_adname, $fa_data_cnt);
//AREQ-77 G.Chin 2010-08-05 chg end
	$inp_admaster_id[0] = "";
	$inp_admaster_name[0] = "(指定しない)";
	for($i=0; $i<$fa_data_cnt; $i++)
	{
		$inp_admaster_id[$i+1] = $fa_fk_admaster_id[$i];
		$inp_admaster_name[$i+1] = $fa_k_adname[$i];
	}
	$inp_admaster_cnt = $fa_data_cnt + 1;
	$name = "inp_ad";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_admaster_id, $inp_admaster_name, $inp_admaster_cnt, $select_num, $admaster_select);
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class='main_l'>\n";
//	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<caption>&#x2460;広告効果測定</caption>\n";
//	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='stat_list.php' method='GET' target='main_r'>\n";
//	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='radio' name='inp_type' value=1 checked>日別集計(yyyymmdd指定)";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "開始<input type='text' name='inp_s_date' size='10' value='$inp_s_date'><br>\n";
	$dsp_tbl .= "終了<input type='text' name='inp_e_date' size='10' value='$inp_e_date'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='radio' name='inp_type' value=2>月別集計(yyyymm指定)";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "開始<input type='text' name='inp_s_month' size='10' value='$inp_s_month'><br>\n";
	$dsp_tbl .= "終了<input type='text' name='inp_e_month' size='10' value='$inp_e_month'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>登録媒体</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "$admaster_select";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='checkbox' name='ad_flag' value='1'>&nbsp;コード別\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin AWKC-181 2010-10-04 add sta
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>ｱｸｾｽ数ｾﾞﾛ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='radio' name='dsp_zero' value=1 checked>表示&nbsp;<input type='radio' name='dsp_zero' value=0>非表示";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin AWKC-181 2010-10-04 add end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='集計表示'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
	
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
