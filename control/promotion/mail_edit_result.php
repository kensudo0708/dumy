<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/25												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mrid = $_REQUEST["mrid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	$f_send_counts = $_REQUEST["f_send_counts"];
	$f_send_to = $_REQUEST["f_send_to"];
	$f_status = $_REQUEST["f_status"];
	$f_sql = $_REQUEST["f_sql"];
	$f_sql_desc = $_REQUEST["f_sql_desc"];
	$reserve_date = $_REQUEST["reserve_date"];
	$reserve_hour = $_REQUEST["reserve_hour"];
	$reserve_minutes = $_REQUEST["reserve_minutes"];
	$f_target = $_REQUEST["f_target"];
	$f_subject = $_REQUEST["f_subject"];
	$f_body = $_REQUEST["f_body"];
	
	//▼予約時間
	$r_year = substr($reserve_date, 0, 4);
	$r_month = substr($reserve_date, 4, 2);
	$r_day = substr($reserve_date, 6, 2);
	$f_reserve_time = $r_year."-".$r_month."-".$r_day." ".$reserve_hour.":".$reserve_minutes.":00";
	
	$dsp_string  = "";
	
	if($mrid == "")
	{
		PrintAdminPage("メルマガ設定編集完了","<P>不正な処理です。</P>");
		exit;
	}
	else if($mrid == "0")
	{
		//メルマガ送信予約登録関数
		$ret = RegistTMailReserve($f_subject,$f_body,$f_target,$f_reserve_time,$f_send_counts,$f_send_to,$f_sql,$f_sql_desc);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "メルマガ送信予約の新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//メルマガ送信予約更新関数
		$ret = UpdateTMailReserve($mrid,$f_subject,$f_body,$f_target,$f_reserve_time,$f_send_counts,$f_send_to,$f_status,$f_sql,$f_sql_desc);
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string .= "メルマガ送信予約の更新処理に成功しました。<br>\n";
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font><b><A href='mail_list.php?sid=$sid&mode=&limit=$limit&offset=$offset&page='>リストに戻る</A></b></font>\n";
/*
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
*/
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガ設定編集完了",$dsp_tbl);

?>

