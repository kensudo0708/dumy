<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/25												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	$mode = $_REQUEST["mode"];
	
	if($mode == "stop")
	{
		$stopid = $_REQUEST["stopid"];
		
		//メルマガ送信予約中断要求更新関数
		$f_req_stop = 1;
		UpdateFReqStop($stopid,$f_req_stop);
	}
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//メルマガ送信予約一覧取得関数
	GetTMailReserveList($limit,$offset,$tmr_fk_reserve_id,$tmr_f_subject,$tmr_f_body,$tmr_f_picture,$tmr_f_target,$tmr_f_smtp_host,$tmr_f_smtp_port,$tmr_f_send_type,$tmr_f_reserve_time,$tmr_f_send_counts,$tmr_f_start_time,$tmr_f_end_time,$tmr_f_send_to,$tmr_f_status,$tmr_f_req_stop,$tmr_f_sql,$tmr_f_sql_desc,$tmr_f_tm_stamp,$tmr_all_count,$tmr_data_cnt);
	
	//▼表示開始～終了番号
	if($tmr_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tmr_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tmr_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='mail_list.php?mode=&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
			}
		}
	}
	
	if($tmr_all_count > $limit)
	{
		$page_first = "<A href='mail_list.php?mode=&offset=0&limit=$limit'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='mail_list.php?mode=&offset=$page_offset[$max_page]&limit=$limit'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tmr_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='mail_list.php?mode=&offset=$next_page&limit=$limit'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='mail_list.php?mode=&offset=$before_page&limit=$limit'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "mail_list.php?mode=&limit=$limit&offset=";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='mail_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
	
	$dsp_tbl .= "<table class='data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>データ件数</th>\n";
	$dsp_tbl .= "<td>$tmr_all_count</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<tt>$start_num</tt>\n";
	$dsp_tbl .= "<tt>件目 ～ </tt>\n";
	$dsp_tbl .= "<tt>$end_num</tt>\n";
	$dsp_tbl .= "<tt>件目</tt>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class='list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style=\"width:60px\"><NOBR><tt>ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:140px\"><NOBR><tt>送信予約日</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:60px\"><NOBR><tt>状態</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:120px\"><NOBR><tt>件数</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:300px\"><NOBR><tt>件名</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:40px\"><NOBR><tt>中止</tt></NOBR></th>\n";
	$dsp_tbl .= "<th style=\"width:40px\"><NOBR><tt>操作</tt></NOBR></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tmr_data_cnt;$i++)
	{
		//▼送信予約日
		$reserve_time_str = substr($tmr_f_reserve_time[$i], 0, 16);
		$reserve_time_str = str_replace("-", "/", $reserve_time_str);
		
		//▼状態
		switch($tmr_f_status[$i])
		{
			case 0:		$status_str = "予約中";		break;
			case 1:		$status_str = "送信中";		break;
			case 2:		$status_str = "送信終了";	break;
			case 8:		$status_str = "編集中";		break;
			case 9:		$status_str = "送信中止";	break;
			default:	$status_str = "";			break;
		}
		
		//▼中断要求フラグ
		if($tmr_f_req_stop[$i] == 1)
		{
			$status_str .= "(中止)";
		}
		
		//▼各種リンク
		if((($tmr_f_status[$i] == 0) || ($tmr_f_status[$i] == 1)) && ($tmr_f_req_stop[$i] != 1))
		{
			$link_stop = "<A href='mail_list.php?sid=$sid&mrid=$tmr_fk_reserve_id[$i]&mode=stop&stopid=$tmr_fk_reserve_id[$i]&limit=$limit&offset=$offset&page='>中止</A>";
		}
		else
		{
			$link_stop = "";
		}
//G.Chin 2010-07-19 chg sta
//		$link_edit = "<A href='mail_edit.php?sid=$sid&mrid=$tmr_fk_reserve_id[$i]&limit=$limit&offset=$offset&mtid=&fromreg='>編集</A>";
		$link_edit = "<A href='mail_edit.php?sid=$sid&mrid=$tmr_fk_reserve_id[$i]&limit=$limit&offset=$offset&mtid='>編集</A>";
//G.Chin 2010-07-19 chg end
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$tmr_fk_reserve_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$reserve_time_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$status_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='right'><NOBR><tt>".number_format($tmr_f_send_counts[$i])."</tt></NOBR></td>\n";
		$dsp_tbl .= "<td><NOBR><tt>$tmr_f_subject[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$link_stop</tt></NOBR></td>\n";
		$dsp_tbl .= "<td class='center'><NOBR><tt>$link_edit</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "<br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガ設定",$dsp_tbl);

?>
