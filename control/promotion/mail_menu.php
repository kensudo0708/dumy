<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/15												*/
/*		修正日		:	2010/04/25	実装										*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	
	$dsp_tbl  = "";
        
	$dsp_tbl .= "<table class = 'main_l'>\n";
	$dsp_tbl .= "<caption>&#x2462;メルマガ設定</caption>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center' colspan=2>\n";
	$dsp_tbl .= "送信設定\n";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mail_list.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='一覧表示'>\n";
	$dsp_tbl .= "</form>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mail_regist.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='mtid' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='新規登録'>\n";
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
/*
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mail_list.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='一覧表示'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mail_regist.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "メールアドレス";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='text' name='inp_mail' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='新規登録'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
*/
	
//	$dsp_tbl .= "<table class = 'main_l'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center' colspan=2>\n";
	$dsp_tbl .= "ﾃﾝﾌﾟﾚｰﾄ設定\n";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mail_temp_list.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='一覧表示'>\n";
	$dsp_tbl .= "</form>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mail_temp_edit.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mtid' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='新規登録'>\n";
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
