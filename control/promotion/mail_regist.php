<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/25												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mrid = $_REQUEST["mrid"];
	
//	$inp_mail = $_REQUEST["inp_mail"];
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	$mtid = $_REQUEST["mtid"];

//kensudo 2011-01-19 add start
	$ad      = $_REQUEST["ad"];
	$chk_ad  = $_REQUEST["chk_ad"];
	$grp     = $_REQUEST["grp"];
	$chk_grp = $_REQUEST["chk_grp"];
//kensudo 2011-01-19 add end	
	//▼現在日付
	$now = time();
	$today = date("Ymd", $now);
	$year = date("Y", $now);
	
	//▼性別
	$f_sex_id[0] = "0";
	$f_sex_name[0] = "男性";
	$f_sex_id[1] = "1";
	$f_sex_name[1] = "女性";
	$f_sex_cnt = 2;
	$name = "f_sex";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_sex_id, $f_sex_name, $f_sex_cnt, $select_num, $sex_select);
	
	//年
	$birth_year_id[0] = "";
	$birth_year_name[0] = "(全て)";
	for($i=0; $i<80; $i++)
	{
		$w_year = $year - $i;
		$num = $i + 1;
		$birth_year_id[$num] = $w_year;
		$birth_year_name[$num] = $w_year;
	}
	$birth_year_cnt = 81;
	$name = "birth_year";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $birth_year_id, $birth_year_name, $birth_year_cnt, $select_num, $birth_year_select);
	
	//月
	$birth_month_id[0] = "";
	$birth_month_name[0] = "(全て)";
	for($i=0; $i<12; $i++)
	{
		$num = $i + 1;
		if($num < 10)
		{
			$month_str = "0".$num;
		}
		else
		{
			$month_str = $num;
		}
		$birth_month_id[$num] = $month_str;
		$birth_month_name[$num] = $month_str;
	}
	$birth_month_cnt = 13;
	$name = "birth_month";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $birth_month_id, $birth_month_name, $birth_month_cnt, $select_num, $birth_month_select);
	
	//日
	$birth_day_id[0] = "";
	$birth_day_name[0] = "(全て)";
	for($i=0; $i<31; $i++)
	{
		$num = $i + 1;
		if($num < 10)
		{
			$day_str = "0".$num;
		}
		else
		{
			$day_str = $num;
		}
		$birth_day_id[$num] = $day_str;
		$birth_day_name[$num] = $day_str;
	}
	$birth_day_cnt = 32;
	$name = "birth_day";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $birth_day_id, $birth_day_name, $birth_day_cnt, $select_num, $birth_day_select);
	
	//▼送信タイプ
	$mailmaga_id[0] = "0";
	$mailmaga_name[0] = "PCから送信";
	$mailmaga_id[1] = "1";
	$mailmaga_name[1] = "MBから送信";
	$mailmaga_cnt = 2;
	$name = "mailmaga";
	$select_num = 0;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $mailmaga_id, $mailmaga_name, $mailmaga_cnt, $select_num, $mailmaga_select);
	
	
	$dsp_tbl  = "";
	
//G.Chin 2010-07-19 chg sta
//	$dsp_tbl .= "<FORM action='mail_edit.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<FORM action='mail_regist_input.php' name =form_1 method='GET' ENCTYPE='multipart/form-data'>\n";
//G.Chin 2010-07-19 chg end
	$dsp_tbl .= "<table class='form'>\n";
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> メールアドレス </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><font color='#B22222'></font></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'>$inp_mail</font></td>\n";
	$dsp_tbl .= "<td colspan=2><font color='#B22222'>PC/MBどちらかで指定</font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 登録日 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_regist' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_regist' value='$today' size='10'> ～ <input type='text' name='e_regist' value='$today' size='10'></td>\n";
	$dsp_tbl .= "<td colspan=2>yyyymmdd形式で指定</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 最終ﾛｸﾞｲﾝ日 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_last' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_last' value='$today' size='10'> ～ <input type='text' name='e_last' value='$today' size='10'></td>\n";
	$dsp_tbl .= "<td colspan=2>yyyymmdd形式で指定</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 入札回数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_contact' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_contact' value='0' size='10'> 回 ～ <input type='text' name='e_contact' value='0' size='10'> 回</td>\n";
	$dsp_tbl .= "<td colspan=2>入札した回数を指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 落札回数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_hummer' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_hummer' value='0' size='10'> 回 ～ <input type='text' name='e_hummer' value='0' size='10'> 回</td>\n";
	$dsp_tbl .= "<td colspan=2>落札した回数を指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 購入合計金額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_total' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_total' value='0' size='10'> 円 ～ <input type='text' name='e_total' value='0' size='10'> 円</td>\n";
	$dsp_tbl .= "<td colspan=2>落札商品に支払った金額は含みません</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 残り購入ｺｲﾝ枚数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_coin' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_coin' value='0' size='10'> ｺｲﾝ ～ <input type='text' name='e_coin' value='0' size='10'> ｺｲﾝ</td>\n";
	$dsp_tbl .= "<td colspan=2>購入したコイン数</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 残りｻｰﾋﾞｽｺｲﾝ枚数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_free' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='s_free' value='0' size='10'> ｺｲﾝ ～ <input type='text' name='e_free' value='0' size='10'> ｺｲﾝ</td>\n";
	$dsp_tbl .= "<td colspan=2>紹介やコインオークションで取得したコイン数</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 性別 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_sex' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2>$sex_select</td>\n";
	$dsp_tbl .= "<td colspan=2>性別を指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 誕生日 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_birth' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2>$birth_year_select 年 $birth_month_select 月 $birth_day_select 日</td>\n";
	$dsp_tbl .= "<td colspan=2>誕生日を指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 年齢 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_age' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='age' value='20' size='4'></td>\n";
	$dsp_tbl .= "<td colspan=2>今年誕生日を迎えた時の年齢<BR>誕生日との併用指定はできません</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 会員ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_member_id' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='member_id' value='' size='20'></td>\n";
	$dsp_tbl .= "<td colspan=2>会員IDを指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ログインID </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_login_id' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='login_id' value='' size='20'></td>\n";
	$dsp_tbl .= "<td colspan=2>ログインIDを指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ハンドル名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_handle' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='handle' value='' size='20'></td>\n";
	$dsp_tbl .= "<td colspan=2>ハンドル名を指定します</td>\n";
	$dsp_tbl .= "</tr>\n";

//        $dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th width=108 bgcolor='#C0C0C0' align=right><tt> 落札回数 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=1><font color='#B22222'><input type='checkbox' name='chk_bidcount' value='1'></font></td>\n";
//	$dsp_tbl .= "<td colspan=2><font color='#B22222'><input type='text' name='s_bidcount' value='' size='10'> 回 ～\n<input type='text' name='e_bidcount' value='' size='10'> 回</font></td>\n";
//	$dsp_tbl .= "<td colspan=2><font color='#B22222'>落札された回数範囲を指定します。</font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//
//        $dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th width=108 bgcolor='#C0C0C0' align=right><tt> コイン購入実績 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=1><font color='#B22222'><input type='checkbox' name='chk_money' value='1'></font></td>\n";
//	$dsp_tbl .= "<td colspan=2><font color='#B22222'><input type='text' name='s_money' value='0' size='10'> 円 ～\n<input type='text' name='e_money' value='0' size='10'> 円</font></td>\n";
//	$dsp_tbl .= "<td colspan=2><font color='#B22222'>コイン購入された金額範囲を指定します。</font></td>\n";
//	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 会員タイプ </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_activity' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2>退会者も含める</td>\n";
	$dsp_tbl .= "<td colspan=2>退会した会員も送信します。</td>\n";
	$dsp_tbl .= "</tr>\n";
//kensudo 2011-01-18 add start
//▼
GetFMGroupNameList($fk_member_group_id, $f_member_group_name, $data_cnt);
	for($i = 0;$i < $data_cnt; $i++ ){
		$f_member_group[$i]      = $fk_member_group_id[$i];
    	$f_member_group_name[$i] = $f_member_group_name[$i];
	}
    $f_member_group_cnt     = $data_cnt;
    $name                   = "f_member_group";
    $select_num             = 0;
    
    MakeSelectObject($name, $f_member_group, $f_member_group_name, $f_member_group_cnt, $select_num, $grp);

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 広告コード </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_ad' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='ad' value='$ad' size='20'></td>\n";
	$dsp_tbl .= "<td colspan=2>広告コードを指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 会員グループ </tt></th>\n";
	$dsp_tbl .= "<td colspan=1><input type='checkbox' name='chk_grp' value='1'></td>\n";
	$dsp_tbl .= "<td colspan=2>$grp</td>\n";
	$dsp_tbl .= "<td colspan=2>会員グループを指定します</td>\n";
	$dsp_tbl .= "</tr>\n";
	//kensudo 2011-01-18 add end

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 検索方法 </tt></th>\n";
	$dsp_tbl .= "<td colspan=1></td>\n";
	$dsp_tbl .= "<td colspan=2><input type='radio' name='search' checked='true' value='andsearch'>AND検索\n<input type='radio' name='search' value='orsearch'>OR検索</td>\n";
        $dsp_tbl .= "<td colspan=2>AND=全条件一致、OR=何かの条件が一致。</td>\n";
	$dsp_tbl .= "</td>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 送信ﾀｲﾌﾟ </tt></th>\n";
	$dsp_tbl .= "<td colspan=1></td>\n";
	$dsp_tbl .= "<td colspan=2>$mailmaga_select</td>\n";
	$dsp_tbl .= "<td colspan=2>PC/MB</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=6 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mrid' value='$mrid'>\n";
//	$dsp_tbl .= "<input type='hidden' name='inp_mail' value='$inp_mail'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='mtid' value='$mtid'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px;'>抽出</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガ設定",$dsp_tbl);
?>
