<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/06/01												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mtid = $_REQUEST["mtid"];
	
	$f_name = $_REQUEST["f_name"];
	$f_target = $_REQUEST["f_target"];
	$f_subject = $_REQUEST["f_subject"];
	$f_body = $_REQUEST["f_body"];
	
	$dsp_string  = "";
	
	if($mtid == "")
	{
		PrintAdminPage("メルマガテンプレート編集完了","<P>不正な処理です。</P>");
		exit;
	}
	else if($mtid == "0")
	{
		//メルマガテンプレート登録関数
		$ret = RegistTMailTemplate($f_name,$f_subject,$f_body,$f_target);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "メルマガテンプレートの新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//メルマガテンプレート更新関数
		$ret = UpdateTMailTemplate($mtid,$f_name,$f_subject,$f_body,$f_target);
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string .= "メルマガテンプレートの更新処理に成功しました。<br>\n";
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font><b><A href='mail_temp_list.php'>リストに戻る</A></b></font>\n";
/*
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
*/
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("メルマガテンプレート編集完了",$dsp_tbl);

?>

