<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	session_start();
	$sid = $_SESSION["staff_id"];

	$inp_type = $_REQUEST["inp_type"];
	$inp_ad = $_REQUEST["inp_ad"];
	$ad_flag = $_REQUEST["ad_flag"];
	$dsp_zero = $_REQUEST["dsp_zero"];	//G.Chin AWKC-181 2010-10-04 add

	$inp_s_date = $_REQUEST["inp_s_date"];
	$inp_e_date = $_REQUEST["inp_e_date"];

	$inp_s_month = $_REQUEST["inp_s_month"];
	$inp_e_month = $_REQUEST["inp_e_month"];


	//▼入力判定
	if($inp_type == 0)
	{
		//日付文字列作成関数
		MakeDateString($inp_s_date, $start_date);
		MakeDateString($inp_e_date, $end_date);
		$inp_start = $start_date." 00:00:00";
		$inp_end = $end_date." 23:59:59";

		$s_year = substr($inp_s_date, 0, 4);
		$s_month = substr($inp_s_date, 4, 2);
		$s_day = substr($inp_s_date, 6, 2);
		$e_year = substr($inp_e_date, 0, 4);
		$e_month = substr($inp_e_date, 4, 2);
		$e_day = substr($inp_e_date, 6, 2);
		$src_term = "集計期間(".$s_year."年".$s_month."月".$s_day."日 00時 ～ 23時)";
	}
	else if($inp_type == 1)
	{
		$error_msg = "";
		if($inp_s_date == "")
		{
			$error_msg .= "開始日が入力されていません。<br>\n";
		}
		if($inp_e_date == "")
		{
			$error_msg .= "終了日が入力されていません。<br>\n";
		}

		if($error_msg != "")
		{
			//管理画面入力ページ表示関数
			PrintAdminPage("広告効果測定",$error_msg);
			exit;
		}
		else
		{
			//日付文字列作成関数
			MakeDateString($inp_s_date, $start_date);
			MakeDateString($inp_e_date, $end_date);
			$inp_start = $start_date." 00:00:00";
			$inp_end = $end_date." 23:59:59";
		}

		$s_year = substr($inp_s_date, 0, 4);
		$s_month = substr($inp_s_date, 4, 2);
		$s_day = substr($inp_s_date, 6, 2);
		$e_year = substr($inp_e_date, 0, 4);
		$e_month = substr($inp_e_date, 4, 2);
		$e_day = substr($inp_e_date, 6, 2);
		$src_term = "集計期間(".$s_year."年".$s_month."月".$s_day."日 ～ ".$e_year."年".$e_month."月".$e_day."日)";
	}
	else if($inp_type == 2)
	{
		$error_msg = "";
		if($inp_s_month == "")
		{
			$error_msg .= "開始月が入力されていません。<br>\n";
		}
		if($inp_e_month == "")
		{
			$error_msg .= "終了月が入力されていません。<br>\n";
		}

		if($error_msg != "")
		{
			//管理画面入力ページ表示関数
			PrintAdminPage("広告効果測定",$error_msg);
			exit;
		}
		else
		{
			$s_year = substr($inp_s_month, 0, 4);
			$s_month = substr($inp_s_month, 4, 2);
			$e_year = substr($inp_e_month, 0, 4);
			$e_month = substr($inp_e_month, 4, 2);
			//月末日取得関数
			$limit_day = GetMonthlimit($e_year, $e_month);
			$inp_start = $s_year."-".$s_month."-01 00:00:00";
			$inp_end = $e_year."-".$e_month."-".$limit_day." 23:59:59";
		}

		$src_term = "集計期間(".$s_year."年".$s_month."月 ～ ".$e_year."年".$e_month."月)";
	}

	//▼表示種別
	if($ad_flag == 1)	//広告コード別
	{
		//▼全選択(固定)
		//$inp_ad = "";

		//広告集計広告主別合計取得関数
		GetTStatAdAdmasterSum($inp_start,$inp_end,$inp_type,$inp_ad,$tsa_fk_admaster_id,$tsa_f_pc_memreg,$tsa_f_mb_memreg,$tsa_f_pc_access,$tsa_f_mb_access,$tsa_f_pc_paycoin,$tsa_f_mb_paycoin,$tsa_data_cnt,$tas_f_pc_prod,$tas_f_mb_prod,$tas_f_pc_coinpack,$tas_f_mb_coinpack,$pc_reg_woman,$mb_reg_woman);

		if($inp_ad == "")
		{
			$tam_k_adname = "全選択";
		}
		else
		{
			//広告主情報取得関数
			GetTAdmasterInfo($inp_ad,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
		}

		$dsp_tbl  = "";
		$dsp_tbl .= $src_term;
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<table class = 'data'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>データ件数</th>\n";
		$dsp_tbl .= "<td>$tsa_data_cnt</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "</table>\n<br>\n";
		$dsp_tbl .= "<br><br>\n";


		$dsp_tbl .= "<table class='list' style='width:1100px'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan=4 style = 'width:30px; height:100px;'>ID</th>\n";	//G.Chin AWKC-182 2010-10-04 add
		$dsp_tbl .= "<th rowspan=4 style = 'width:150px;'>広告名</th>\n";
		if($inp_type == 2)
		{
			$dsp_tbl .= "<th colspan=16 style = 'height:25px;'>広告($tam_k_adname)</th>\n";
		}
		else
		{
			$dsp_tbl .= "<th colspan=12 style = 'height:25px;'>広告($tam_k_adname)</th>\n";
		}
		$dsp_tbl .= "</tr>\n";

		if( $inp_type == 2)
		{
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<th colspan=8 style = 'width:350px; height:25px;'>PC</th>\n";
			$dsp_tbl .= "<th colspan=8 style = 'width:350px; background-color:#C8FFC8;'>MB</th>\n";
			$dsp_tbl .= "</tr>\n";
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:60px; height:50px;'>ｱｸｾｽ数</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>ｱｸｾｽ単価</th>\n";
			$dsp_tbl .= "<th colspan='2' style = 'width:120px;'>登録数</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>登録単価</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コイン<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>商品<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コインパック<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:60px;'>ｱｸｾｽ数</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>ｱｸｾｽ単価</th>\n";
			$dsp_tbl .= "<th colspan='2' style = 'width:120px;'>登録数</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>登録単価</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コイン<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>商品<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コインパック<br>購入金額</th>\n";
			$dsp_tbl .= "</tr>\n";
			$dsp_tbl .= "<tr bgcolor='#FFFFFF'>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>男</th>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>女</th>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>男</th>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>女</th>\n";
			$dsp_tbl .= "</tr>\n";
		}
		else
		{
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<th colspan=6 style = 'width:350px;'>PC</th>\n";
			$dsp_tbl .= "<th colspan=6 style = 'width:350px; background-color:#C8FFC8;'>MB</th>\n";
			$dsp_tbl .= "</tr>\n";
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:60px; height:50px;'>ｱｸｾｽ数</th>\n";
			$dsp_tbl .= "<th colspan='2' style = 'width:80px;'>登録数</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コイン<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>商品<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コインパック<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:60px;'>ｱｸｾｽ数</th>\n";
			$dsp_tbl .= "<th colspan='2' style = 'width:80px;'>登録数</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コイン<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>商品<br>購入金額</th>\n";
			$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コインパック<br>購入金額</th>\n";
			$dsp_tbl .= "</tr>\n";
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>男</th>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>女</th>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>男</th>\n";
			$dsp_tbl .= "<th style = 'width:60px;'>女</th>\n";
			$dsp_tbl .= "</tr>\n";
		}

		//▼現在日
		$now = time();
		$today = date("Y-m-d", $now);

		$ttl_pc_access = 0;
		$ttl_pc_memreg = 0;
		$ttl_pc_paycoin = 0;
		$ttl_mb_access = 0;
		$ttl_mb_memreg = 0;
		$ttl_mb_paycoin = 0;
		$ttl_f_pc_buy_prod = 0;
		$ttl_f_mb_buy_prod = 0;
		$ttl_f_pc_buy_coin = 0;
		$ttl_f_mb_buy_coin = 0;
		$ttl_pc_memreg_woman=0;
		$ttl_mb_memreg_woman=0;

		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$tsa_data_cnt;$i++)
		{
			//広告主情報取得関数
			GetTAdmasterInfo($tsa_fk_admaster_id[$i],$w_k_adname,$w_f_agent_name,$w_f_tel_no,$w_f_mail_address,$w_f_login_id,$w_f_login_pass,$w_f_kickback_url,$w_f_pay_type,$w_f_pay_value,$w_f_memo,$w_f_counter,$w_f_tm_stamp);

//G.Chin AWKC-181 2010-10-04 add sta
			//▼アクセス数判定
			$all_access = $tsa_f_pc_access[$i] + $tsa_f_mb_access[$i];
			if(($dsp_zero == 0) && ($all_access == 0))
			{
				//アクセス数０は非表示
			}
			else
			{
//G.Chin AWKC-181 2010-10-04 add end
			//▼入力判定
			if($inp_type == 0)		//時間別
			{
				$link_detail = "";
			}
			else if($inp_type == 1)	//日別
			{
				//▼日付
				$src_date = $inp_s_date;
				$date_str = str_replace("-", "/", $src_date);
				$w_date = str_replace("-", "", $src_date);
				//曜日文字列生成関数
				MakeWeekDayStr($src_date, $weekstr);
				$date_str = $date_str."(".$weekstr.")";
//G.Chin AWKC-181 2010-10-04 chg sta
//				$link_detail = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$inp_s_date&inp_e_date=$inp_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$tsa_fk_admaster_id[$i]&ad_flag='>$w_k_adname</A>";
				$link_detail = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$inp_s_date&inp_e_date=$inp_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$tsa_fk_admaster_id[$i]&ad_flag=&dsp_zero=$dsp_zero'>$w_k_adname</A>";
//G.Chin AWKC-181 2010-10-04 chg end
			}
			else if($inp_type == 2)	//月別
			{
				//▼日付
				$src_date = $inp_s_month;
				$date_str = str_replace("-", "/", $src_date);
				$w_date = str_replace("-", "", $src_date);
				$w_year = substr($w_date, 0, 4);
//G.Chin AWKT-516 2010-10-12 chg sta
//				$w_month = substr($w_date, 5, 2);
				$w_month = substr($w_date, 4, 2);
//G.Chin AWKT-516 2010-10-12 chg end
				//月末日取得関数
				$w_limit_day = GetMonthlimit($w_year, $w_month);
				$w_s_date = $w_date."01";
				$w_e_date = $w_date.$w_limit_day;
//G.Chin AWKC-181 2010-10-04 chg sta
//				$link_detail = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$inp_s_date&inp_e_date=$inp_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$tsa_fk_admaster_id[$i]&ad_flag='>$w_k_adname</A>";
				$link_detail = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$inp_s_date&inp_e_date=$inp_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$tsa_fk_admaster_id[$i]&ad_flag=&dsp_zero=$dsp_zero'>$w_k_adname</A>";
//G.Chin AWKC-181 2010-10-04 chg end

				$tmp_reg = $pc_reg_woman[$i] + $tsa_f_pc_memreg[$i];
				$pc_acc_price =$tsa_f_pc_access[$i]==0?$w_f_pay_value:round($w_f_pay_value/$tsa_f_pc_access[$i],2);
				$mb_acc_price =$tsa_f_mb_access[$i]==0?$w_f_pay_value:round($w_f_pay_value/$tsa_f_mb_access[$i],2);
				$tmp_reg = $pc_reg_woman[$i] + $tsa_f_pc_memreg[$i];
				$pc_reg_price =$tmp_reg==0?$w_f_pay_value:round($w_f_pay_value/$tmp_reg,2);
				$pc_reg_price =$tsa_f_pc_memreg[$i]==0?"<font color='darkgray'>$pc_reg_price</font>":$pc_reg_price;

				$tmp_reg = $mb_reg_woman[$i] + $tsa_f_mb_memreg[$i];
				$mb_reg_price =$tmp_reg==0?$w_f_pay_value:round($w_f_pay_value/$tmp_reg,2);
				$mb_reg_price =$tsa_f_mb_memreg[$i]==0?"<font color='darkgray'>$mb_reg_price</font>":$mb_reg_price;

				$pc_acc_price =number_format($pc_acc_price,2);
				$mb_acc_price =number_format($mb_acc_price,2);
				$pc_reg_price =number_format($pc_reg_price,2);
				$mb_reg_price =number_format($mb_reg_price,2);

				$pc_acc_price =$tsa_f_pc_access[$i]==0?"<font color='darkgray'>$pc_acc_price</font>":$pc_acc_price;
				$mb_acc_price =$tsa_f_mb_access[$i]==0?"<font color='darkgray'>$mb_acc_price</font>":$mb_acc_price;
				//$pc_reg_price =$tsa_f_pc_memreg[$i]==0?"<font color='darkgray'>$pc_reg_price</font>":$pc_reg_price;
				//$mb_reg_price =$tsa_f_mb_memreg[$i]==0?"<font color='darkgray'>$mb_reg_price</font>":$mb_reg_price;
			}

			$dsp_tbl .= "<tr style = 'height:25px;'>\n";
//G.Chin AWKC-182 2010-10-04 chg sta
//			$dsp_tbl .= "<td align='center'>$link_detail</td>\n";
			$dsp_tbl .= "<td align='center'>$tsa_fk_admaster_id[$i]</td>\n";
			$dsp_tbl .= "<td align='left'>$link_detail</td>\n";
//G.Chin AWKC-182 2010-10-04 chg end
//G.Chin 2010-12-24 chg sta
//			$dsp_tbl .= "<td align='right'>$tsa_f_pc_access[$i]</td>\n";
			$dsp_tbl .= "<td align='right' bgcolor='#C8F1FF'>$tsa_f_pc_access[$i]</td>\n";
//G.Chin 2010-12-24 chg end
			if($inp_type == 2)
			{
				$dsp_tbl .= "<td align='right' bgcolor ='#FFC8C8' >$pc_acc_price</td>\n";
			}
			$dsp_tbl .= "<td align='right'>$tsa_f_pc_memreg[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$pc_reg_woman[$i]</td>\n";

			if($inp_type == 2)
			{
				$dsp_tbl .= "<td align='right' bgcolor ='#FFC8C8'>$pc_reg_price</td>\n";
			}
			$dsp_tbl .= "<td align='right'>$tsa_f_pc_paycoin[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_pc_prod[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_pc_coinpack[$i]</td>\n";

//G.Chin 2010-12-24 chg end
//			$dsp_tbl .= "<td align='right'>$tsa_f_mb_access[$i]</td>\n";
			$dsp_tbl .= "<td align='right' bgcolor='#C8F1FF'>$tsa_f_mb_access[$i]</td>\n";
//G.Chin 2010-12-24 chg end
			if($inp_type == 2)
			{
				$dsp_tbl .= "<td align='right' bgcolor ='#FFC8C8'>$mb_acc_price</td>\n";
			}
			$dsp_tbl .= "<td align='right'>$tsa_f_mb_memreg[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$mb_reg_woman[$i]</td>\n";
			if($inp_type == 2)
			{
				$dsp_tbl .= "<td align='right' bgcolor ='#FFC8C8'>$mb_reg_price</td>\n";
			}
			$dsp_tbl .= "<td align='right'>$tsa_f_mb_paycoin[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_mb_prod[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_mb_coinpack[$i]</td>\n";
			$dsp_tbl .= "</tr>\n";

			//▼合計加算
			$ttl_pc_access = $ttl_pc_access + $tsa_f_pc_access[$i];
			$ttl_pc_memreg = $ttl_pc_memreg + $tsa_f_pc_memreg[$i];
			$ttl_pc_memreg_woman = $ttl_pc_memreg_woman + $pc_reg_woman[$i];
			$ttl_pc_paycoin = $ttl_pc_paycoin + $tsa_f_pc_paycoin[$i];
			$ttl_mb_access = $ttl_mb_access + $tsa_f_mb_access[$i];
			$ttl_mb_memreg = $ttl_mb_memreg + $tsa_f_mb_memreg[$i];
			$ttl_mb_memreg_woman = $ttl_mb_memreg_woman + $mb_reg_woman[$i];
			$ttl_mb_paycoin = $ttl_mb_paycoin + $tsa_f_mb_paycoin[$i];
			$ttl_f_pc_buy_prod = $ttl_f_pc_buy_prod+$tas_f_pc_prod[$i];
			$ttl_f_mb_buy_prod = $ttl_f_mb_buy_prod+$tas_f_mb_prod[$i];
			$ttl_f_pc_buy_coin = $ttl_f_pc_buy_coin+$tas_f_pc_coinpack[$i];
			$ttl_f_mb_buy_coin = $ttl_f_mb_buy_coin+$tas_f_mb_coinpack[$i];
//G.Chin AWKC-181 2010-10-04 add sta
			}
//G.Chin AWKC-181 2010-10-04 add end
		}

		$dsp_tbl .= "<tr style = 'height:25px;'>\n";
		$dsp_tbl .= "<td align='center' colspan=2>合計</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_pc_access</td>\n";
		if($inp_type == 2)
		{
			$dsp_tbl .= "<td align='right'>--</td>\n";
		}
		$dsp_tbl .= "<td align='right'>$ttl_pc_memreg</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_pc_memreg_woman</td>\n";
		if($inp_type == 2)
		{
			$dsp_tbl .= "<td align='right'>--</td>\n";
		}
		$dsp_tbl .= "<td align='right'>$ttl_pc_paycoin</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_pc_buy_prod</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_pc_buy_coin</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_mb_access</td>\n";
		if($inp_type == 2)
		{
			$dsp_tbl .= "<td align='right'>--</td>\n";
		}
		$dsp_tbl .= "<td align='right'>$ttl_mb_memreg</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_mb_memreg_woman</td>\n";
		if($inp_type == 2)
		{
			$dsp_tbl .= "<td align='right'>--</td>\n";
		}
		$dsp_tbl .= "<td align='right'>$ttl_mb_paycoin</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_mb_buy_prod</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_mb_buy_coin</td>\n";
		$dsp_tbl .= "</tr>\n";

		$dsp_tbl .= "</table><br>\n";
	}
	else				//日時別
	{
		//広告集計合計取得関数
		GetTStatAdSum($inp_start,$inp_end,$inp_type,$inp_ad,$tsa_f_stat_dt,$tsa_f_pc_memreg,$tsa_f_mb_memreg,$tsa_f_pc_access,$tsa_f_mb_access,$tsa_f_pc_paycoin,$tsa_f_mb_paycoin,$tsa_data_cnt,$tas_f_pc_prod,$tas_f_mb_prod,$tas_f_pc_coinpack,$tas_f_mb_coinpack,$pc_reg_woman,$mb_reg_woman);

		if($inp_ad == "")
		{
			$tam_k_adname = "全選択";
		}
		else
		{
			//広告主情報取得関数
			GetTAdmasterInfo($inp_ad,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
		}

		$dsp_tbl  = "";
		$dsp_tbl .= $src_term;
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<table class = 'data'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>データ件数</th>\n";
		$dsp_tbl .= "<td>$tsa_data_cnt</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "</table>\n<br>\n";


		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<table class='list' style='width:1100px'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan=4 style='width:100px;'>日付</th>\n";
		if($inp_type != 0)
		{
			$dsp_tbl .= "<th rowspan=4 colspan=2 style='width:120px;'>操作</th>\n";
		}
		$dsp_tbl .= "<th colspan=12 style = 'height:25px;'>広告($tam_k_adname)</th>\n";
		$dsp_tbl .= "</tr>\n";

		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th colspan=6 style = 'width:350px; height:25px;'>PC</th>\n";
		$dsp_tbl .= "<th colspan=6 style = 'width:350px; background-color:#C8FFC8;'>MB</th>\n";
		$dsp_tbl .= "</tr>\n";

		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:60px; height:50px;'>ｱｸｾｽ数</th>\n";
		$dsp_tbl .= "<th colspan='2' style = 'width:120px;'>登録数</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コイン<br>購入金額</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>商品<br>購入金額</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コインパック<br>購入金額</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:60px;'>ｱｸｾｽ数</th>\n";
		$dsp_tbl .= "<th colspan='2' style = 'width:120px;'>登録数</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コイン<br>購入金額</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>商品<br>購入金額</th>\n";
		$dsp_tbl .= "<th rowspan='2' style = 'width:80px;'>コインパック<br>購入金額</th>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th style = 'width:60px;'>男</th>\n";
		$dsp_tbl .= "<th style = 'width:60px;'>女</th>\n";
		$dsp_tbl .= "<th style = 'width:60px;'>男</th>\n";
		$dsp_tbl .= "<th style = 'width:60px;'>女</th>\n";
		$dsp_tbl .= "</tr>\n";

		//▼現在日
		$now = time();
		$today = date("Y-m-d", $now);

		$ttl_pc_access = 0;
		$ttl_pc_memreg = 0;
		$ttl_pc_paycoin = 0;
		$ttl_mb_access = 0;
		$ttl_mb_memreg = 0;
		$ttl_mb_paycoin = 0;
		$ttl_f_pc_buy_prod = 0;
		$ttl_f_mb_buy_prod = 0;
		$ttl_f_pc_buy_coin = 0;
		$ttl_f_mb_buy_coin = 0;
		$ttl_pc_memreg_woman=0;
		$ttl_mb_memreg_woman=0;

		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$tsa_data_cnt;$i++)
		{
//G.Chin AWKC-181 2010-10-04 add sta
			//▼アクセス数判定
			$all_access = $tsa_f_pc_access[$i] + $tsa_f_mb_access[$i];
			if(($dsp_zero == 0) && ($all_access == 0))
			{
				//アクセス数０は非表示
			}
			else
			{
//G.Chin AWKC-181 2010-10-04 add end
			//▼入力判定
			if($inp_type == 0)		//時間別
			{
				//▼日付
				$hour = substr($tsa_f_stat_dt[$i], 11, 5);
				$date_str = $hour."～";
			}
			else if($inp_type == 1)	//日別
			{
				//▼日付
				$src_date = substr($tsa_f_stat_dt[$i], 0, 10);
				$date_str = str_replace("-", "/", $src_date);
				$w_date = str_replace("-", "", $src_date);
				//曜日文字列生成関数
				MakeWeekDayStr($src_date, $weekstr);
				$date_str = $date_str."(".$weekstr.")";
//G.Chin AWKC-181 2010-10-04 chg sta
/*
				$link_date = "<A href='stat_list.php?sid=$sid&inp_type=0&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag='>時別</A>";
				$link_ad = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag=1'>広告比較</A>";
*/
//G.Chin AWKT-978 2010-12-29 chg sta
/*
				$link_date = "<A href='stat_list.php?sid=$sid&inp_type=0&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag=&dsp_zero=$dsp_zero'>時別</A>";
				$link_ad = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag=1&dsp_zero=$dsp_zero'>広告比較</A>";
*/
				$src_month = substr($w_date, 0, 6);
				$link_date = "<A href='stat_list.php?sid=$sid&inp_type=0&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$src_month&inp_e_month=$src_month&inp_ad=$inp_ad&ad_flag=&dsp_zero=$dsp_zero'>時別</A>";
				$link_ad = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$src_month&inp_e_month=$src_month&inp_ad=$inp_ad&ad_flag=1&dsp_zero=$dsp_zero'>広告比較</A>";
//G.Chin AWKT-978 2010-12-29 chg end
//G.Chin AWKC-181 2010-10-04 chg end
			}
			else if($inp_type == 2)	//月別
			{
				//▼日付
				$src_date = substr($tsa_f_stat_dt[$i], 0, 7);
				$date_str = str_replace("-", "/", $src_date);
				$w_date = str_replace("-", "", $src_date);
				$w_year = substr($w_date, 0, 4);
//G.Chin AWKT-516 2010-10-12 chg sta
//				$w_month = substr($w_date, 5, 2);
				$w_month = substr($w_date, 4, 2);
//G.Chin AWKT-516 2010-10-12 chg end
				//月末日取得関数
				$w_limit_day = GetMonthlimit($w_year, $w_month);
				$w_s_date = $w_date."01";
				$w_e_date = $w_date.$w_limit_day;
//G.Chin AWKC-181 2010-10-04 chg sta
/*
				$link_date = "<A href='stat_list.php?sid=$sid&inp_type=1&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag='>日別</A>";
				$link_ad = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag=1'>広告比較</A>";
*/
//G.Chin AWKT-978 2010-12-29 chg sta
/*
				$link_date = "<A href='stat_list.php?sid=$sid&inp_type=1&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag=&dsp_zero=$dsp_zero'>日別</A>";
				$link_ad = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad&ad_flag=1&dsp_zero=$dsp_zero'>広告比較</A>";
*/
				$src_month = substr($w_date, 0, 6);
				$link_date = "<A href='stat_list.php?sid=$sid&inp_type=1&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$src_month&inp_e_month=$src_month&inp_ad=$inp_ad&ad_flag=&dsp_zero=$dsp_zero'>日別</A>";
				$link_ad = "<A href='stat_list.php?sid=$sid&inp_type=$inp_type&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$src_month&inp_e_month=$src_month&inp_ad=$inp_ad&ad_flag=1&dsp_zero=$dsp_zero'>広告比較</A>";
//G.Chin AWKT-978 2010-12-29 chg end
//G.Chin AWKC-181 2010-10-04 chg end
			}

/*
			//※．当日は非表示
			if($inp_type != 0)
			{
				if($src_date == $today)
				{
					break;
				}
			}
*/

			$dsp_tbl .= "<tr style = 'height:25px;'>\n";
			$dsp_tbl .= "<td align='center'>$date_str</td>\n";
			if($inp_type != 0)
			{
				$dsp_tbl .= "<td align='center'>$link_date</td>\n";
				$dsp_tbl .= "<td align='center'>$link_ad</td>\n";
			}
//G.Chin 2010-12-24 chg end
//			$dsp_tbl .= "<td align='right'>$tsa_f_pc_access[$i]</td>\n";
			$dsp_tbl .= "<td align='right' bgcolor='#C8F1FF'>$tsa_f_pc_access[$i]</td>\n";
//G.Chin 2010-12-24 chg end
			$dsp_tbl .= "<td align='right'>$tsa_f_pc_memreg[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$pc_reg_woman[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tsa_f_pc_paycoin[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_pc_prod[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_pc_coinpack[$i]</td>\n";
//G.Chin 2010-12-24 chg end
//			$dsp_tbl .= "<td align='right'>$tsa_f_mb_access[$i]</td>\n";
			$dsp_tbl .= "<td align='right' bgcolor='#C8F1FF'>$tsa_f_mb_access[$i]</td>\n";
//G.Chin 2010-12-24 chg end
			$dsp_tbl .= "<td align='right'>$tsa_f_mb_memreg[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$mb_reg_woman[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tsa_f_mb_paycoin[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_mb_prod[$i]</td>\n";
			$dsp_tbl .= "<td align='right'>$tas_f_mb_coinpack[$i]</td>\n";
			$dsp_tbl .= "</tr>\n";

			//▼合計加算
			$ttl_pc_access = $ttl_pc_access + $tsa_f_pc_access[$i];
			$ttl_pc_memreg = $ttl_pc_memreg + $tsa_f_pc_memreg[$i];
			$ttl_pc_memreg_woman = $ttl_pc_memreg_woman + $pc_reg_woman[$i];
			$ttl_pc_paycoin = $ttl_pc_paycoin + $tsa_f_pc_paycoin[$i];
			$ttl_mb_access = $ttl_mb_access + $tsa_f_mb_access[$i];
			$ttl_mb_memreg = $ttl_mb_memreg + $tsa_f_mb_memreg[$i];
			$ttl_mb_memreg_woman = $ttl_mb_memreg_woman + $mb_reg_woman[$i];
			$ttl_mb_paycoin = $ttl_mb_paycoin + $tsa_f_mb_paycoin[$i];
			$ttl_f_pc_buy_prod = $ttl_f_pc_buy_prod+$tas_f_pc_prod[$i];
			$ttl_f_mb_buy_prod = $ttl_f_mb_buy_prod+$tas_f_mb_prod[$i];
			$ttl_f_pc_buy_coin = $ttl_f_pc_buy_coin+$tas_f_pc_coinpack[$i];
			$ttl_f_mb_buy_coin = $ttl_f_mb_buy_coin+$tas_f_mb_coinpack[$i];
//G.Chin AWKC-181 2010-10-04 add sta
			}
//G.Chin AWKC-181 2010-10-04 add end
		}

		$dsp_tbl .= "<tr bgcolor='#FFFFFF' style = 'height:25px;'>\n";
		if($inp_type == 0)
		{
			$dsp_tbl .= "<td align='center'>合計</td>\n";
		}
		else
		{
			$dsp_tbl .= "<td align='center' colspan=3>合計</td>\n";
		}
		$dsp_tbl .= "<td align='right'>$ttl_pc_access</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_pc_memreg</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_pc_memreg_woman</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_pc_paycoin</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_pc_buy_prod</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_pc_buy_coin</td>\n";

		$dsp_tbl .= "<td align='right'>$ttl_mb_access</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_mb_memreg</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_mb_memreg_woman</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_mb_paycoin</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_mb_buy_prod</td>\n";
		$dsp_tbl .= "<td align='right'>$ttl_f_mb_buy_coin</td>\n";
		$dsp_tbl .= "</tr>\n";

		$dsp_tbl .= "</table><br>\n";
	}

	//管理画面入力ページ表示関数
	PrintAdminPage("広告効果測定",$dsp_tbl);

?>
