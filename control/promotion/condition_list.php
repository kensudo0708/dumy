<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/14												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$mode = $_REQUEST["mode"];
	
	if($mode == "disp")			//表示
	{
		$dsp_string = "";
	}
	else if($mode == "insert")	//追加
	{
		$up_cnt = $_REQUEST["up_cnt"];
		$fk_adcon_id = $_REQUEST["fk_adcon_id"];
		$f_adpricode = $_REQUEST["f_adpricode"];
		$f_status = $_REQUEST["f_status"];
		$f_type = $_REQUEST["f_type"];
		$f_spend_value = $_REQUEST["f_spend_value"];
		$f_pay_value = $_REQUEST["f_pay_value"];
		
		$ins_f_adpricode = $_REQUEST["ins_f_adpricode"];
		$ins_f_status = $_REQUEST["ins_f_status"];
		$ins_f_type = $_REQUEST["ins_f_type"];
		$ins_f_spend_value = $_REQUEST["ins_f_spend_value"];
		$ins_f_pay_value = $_REQUEST["ins_f_pay_value"];
		
		for($i=0; $i<$up_cnt; $i++)
		{
			//支払条件更新関数
			UpdateTAdCondition($fk_adcon_id[$i],$f_type[$i],$f_adpricode[$i],$f_spend_value[$i],$f_pay_value[$i],$f_status[$i]);
		}
		//支払条件追加関数
		InsertTAdCondition($ins_f_type,$ins_f_adpricode,$ins_f_spend_value,$ins_f_pay_value,$ins_f_status);
		
		$dsp_string = "<font color='#FF0000'>※．</font>支払条件を追加しました。<br>\n";
	}
	else if($mode == "update")	//変更
	{
		$up_cnt = $_REQUEST["up_cnt"];
		$fk_adcon_id = $_REQUEST["fk_adcon_id"];
		$f_adpricode = $_REQUEST["f_adpricode"];
		$f_status = $_REQUEST["f_status"];
		$f_type = $_REQUEST["f_type"];
		$f_spend_value = $_REQUEST["f_spend_value"];
		$f_pay_value = $_REQUEST["f_pay_value"];
		
		for($i=0; $i<$up_cnt; $i++)
		{
			//支払条件状態更新関数
			UpdateTAdCondition($fk_adcon_id[$i],$f_type[$i],$f_adpricode[$i],$f_spend_value[$i],$f_pay_value[$i],$f_status[$i]);
		}
		
		$dsp_string = "<font color='#FF0000'>※．</font>支払条件を変更しました。<br>\n";
	}
	else if($mode == "delete")	//削除
	{
		$acid = $_REQUEST["acid"];
		
		//支払条件削除関数
		DeleteTAdCondition($acid);
		
		$dsp_string = "<font color='#FF0000'>※．</font>支払条件を削除しました。<br>\n";
	}
	
	//支払条件一覧取得関数
	GetTAdConditionList($tac_fk_adcon_id,$tac_f_type,$tac_f_adpricode,$tac_f_spend_value,$tac_f_pay_value,$tac_f_status,$tac_f_tm_stamp,$tac_data_cnt);
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<FORM action='condition_upconf.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table class='data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>データ件数</th>\n";
	$dsp_tbl .= "<td>$tac_data_cnt</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<br>\n";
	
	$dsp_tbl .= "<font><b>$dsp_string</b></font>\n";
	$dsp_tbl .= "<br>\n";
	
	$dsp_tbl .= "<table class='list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style='width:120px'><tt>優先度</tt></th>\n";
	$dsp_tbl .= "<th style='width:80px'><tt>状態</tt></th>\n";
	$dsp_tbl .= "<th style='width:180px'><tt>タイプ</tt></th>\n";
	$dsp_tbl .= "<th style='width:180px'><tt>条件</tt></th>\n";
	$dsp_tbl .= "<th style='width:120px'><tt>紹介者支払ｺｲﾝ</tt></th>\n";
	$dsp_tbl .= "<th style='width:80px'><tt>操作</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tac_data_cnt;$i++)
	{
		//▼状態
		$f_status_id[0] = "0";
		$f_status_name[0] = "有効";
		$f_status_id[1] = "9";
		$f_status_name[1] = "無効";
		$f_status_cnt = 2;
		$name = "f_status[]";
		$select_num = $tac_f_status[$i];
		//選択オブジェクト作成関数
		MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);
		
		//▼タイプ
		$f_type_id[0] = "0";
		$f_type_name[0] = "会員紹介・会員ﾊﾞﾅｰ";
		$f_type_id[1] = "1";
		$f_type_name[1] = "広告ｱﾌｨﾘ";
		$f_type_cnt = 2;
		$name = "f_type[]";
		$select_num = $tac_f_type[$i];
		//選択オブジェクト作成関数
		MakeSelectObject($name, $f_type_id, $f_type_name, $f_type_cnt, $select_num, $f_type_select);
		
		//▼更新確認画面へのリンク
		$link_reg = "<A href='condition_upconf.php?acid=$tac_fk_adcon_id[$i]&mode=delete&ins_f_adpricode='>削除</A>";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td class='center'><tt><input class='right' type='text' name='f_adpricode[]' value='$tac_f_adpricode[$i]' size='5'></tt></td>\n";
		$dsp_tbl .= "<td class='center'><tt>$f_status_select</tt></td>\n";
		$dsp_tbl .= "<td class='center'><tt>$f_type_select</tt></td>\n";
		$dsp_tbl .= "<td class='center'><tt><input class='right' type='text' name='f_spend_value[]' value='".number_format($tac_f_spend_value[$i])."' size='6'>&nbsp;円(会員登録時0円)</tt></td>\n";
		$dsp_tbl .= "<td class='center'><tt><input class='right' type='text' name='f_pay_value[]' value='".number_format($tac_f_pay_value[$i])."' size='6'>&nbsp;ｺｲﾝ</tt></td>\n";
		$dsp_tbl .= "<td class='center'><tt>$link_reg</tt></td>\n";
		$dsp_tbl .= "<input type='hidden' name='fk_adcon_id[]' value='$tac_fk_adcon_id[$i]'>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	//▼追加用にもう一行
	
	//▼状態
	$f_status_id[0] = "0";
	$f_status_name[0] = "有効";
	$f_status_id[1] = "9";
	$f_status_name[1] = "無効";
	$f_status_cnt = 2;
	$name = "ins_f_status";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);
	
	//▼タイプ
	$f_type_id[0] = "0";
	$f_type_name[0] = "会員紹介・会員ﾊﾞﾅｰ";
	$f_type_id[1] = "1";
	$f_type_name[1] = "広告ｱﾌｨﾘ";
	$f_type_cnt = 2;
	$name = "ins_f_type";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_type_id, $f_type_name, $f_type_cnt, $select_num, $f_type_select);
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td class='center'><tt><input class='right' type='text' name='ins_f_adpricode' value='' size='5'></tt></td>\n";
	$dsp_tbl .= "<td class='center'><tt>$f_status_select</tt></td>\n";
	$dsp_tbl .= "<td class='center'><tt>$f_type_select</tt></td>\n";
	$dsp_tbl .= "<td class='center'><tt><input class='right' type='text' name='ins_f_spend_value' value='' size='6'>&nbsp;円(会員登録時0円)</tt></td>\n";
	$dsp_tbl .= "<td class='center'><tt><input class='right' type='text' name='ins_f_pay_value' value='' size='6'>&nbsp;ｺｲﾝ</tt></td>\n";
	$dsp_tbl .= "<td class='center'><tt></tt></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "</table><br>\n";
	
	$dsp_tbl .= "<input type='hidden' name='mode' value='update'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_cnt' value='$tac_data_cnt'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px'>更新確認</button>\n";
	$dsp_tbl .= "</FORM>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("サービスコイン設定",$dsp_tbl);
?>
