<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];	//AREQ-77 G.Chin 2010-08-05 add
	$inp_adcode = $_REQUEST["inp_adcode"];
	
	$f_adcode = $_REQUEST["f_adcode"];
	$k_adname = $_REQUEST["k_adname"];
	$f_agent_name = $_REQUEST["f_agent_name"];
	$f_pay_value = $_REQUEST["f_pay_value"];
	$f_login_id = $_REQUEST["f_login_id"];
	$f_login_pass = $_REQUEST["f_login_pass"];
	$f_mail_address = $_REQUEST["f_mail_address"];
	$f_memo = $_REQUEST["f_memo"];
	$f_mem_reg_urls = isset($_REQUEST["f_mem_reg_urls"]) ? $_REQUEST["f_mem_reg_urls"]:NULL;
	
//G.Chin 2010-10-28 del sta
/*
	$afflt_status = $_REQUEST["afflt_status"];
	$fk_program_id = $_REQUEST["fk_program_id"];
	$f_regist = $_REQUEST["f_regist"];
	$f_regist_value = $_REQUEST["f_regist_value"];
	$f_first_buy = $_REQUEST["f_first_buy"];
	$f_first_buy_price = $_REQUEST["f_first_buy_price"];
	$f_first_buy_value = $_REQUEST["f_first_buy_value"];
//G.Chin 2010-08-06 chg sta
//	$f_img_url = $_REQUEST["f_img_url"];	//G.Chin 2010-07-28 add
	$f_terminal = $_REQUEST["f_terminal"];
	$f_imgurl_pc = $_REQUEST["f_imgurl_pc"];
	$f_imgurl_mb = $_REQUEST["f_imgurl_mb"];
//G.Chin 2010-08-06 chg end
	$f_param1 = $_REQUEST["f_param1"];
	$f_param2 = $_REQUEST["f_param2"];
	$afflt_type = $_REQUEST["f_type"];
	$f_bank_pay = $_REQUEST["f_bank_pay"];
	$f_report = $_REQUEST["f_report"];
	$f_listing = $_REQUEST["f_listing"];	//G.Chin 2010-08-02 add
	$f_listing2 = $_REQUEST["f_listing2"];	//G.Chin 2010-08-27 add
*/
//G.Chin 2010-10-28 del end
	$afflt_status = "";	//G.Chin 2010-11-05 add
//G.Chin 2010-07-27 add sta
	$f_disp_hour = $_REQUEST["f_disp_hour"];
	$f_disp_pay = $_REQUEST["f_disp_pay"];
	$f_disp_today = $_REQUEST["f_disp_today"];
//G.Chin 2010-07-27 add end
//	$f_add_tag = $_REQUEST["f_add_tag"];	//G.Chin 2010-10-06 add	//G.Chin 2010-10-28 del
	
	//▼支払タイプ("金銭"固定)
	$f_pay_type = 1;
	
	//▼広告タイプ("運営者広告"固定)
	$f_type = 1;
	
	//▼リスティング広告
//	$f_listing = str_replace("'", "\'", $f_listing);	//G.Chin 2010-08-17 add	//G.Chin 2010-10-28 del
	
	$dsp_string  = "";
	$back_string = "";
	
	if($aid == "")
	{
		PrintAdminPage("広告媒体編集完了","<P>不正な処理です。</P>");
		print "不正な処理です。<br>\n";
		exit;
	}
	else if($f_adcode =="")
	{
		PrintAdminPage("広告媒体編集完了","<P>不正な処理です。</P>");
		print "広告コードなしでは登録できません。<br>\n";
		exit;
	}
	
	else if($aid == "0")
	{
		//広告コードID取得関数
		GetTAdcodeId($f_adcode, $tac_fk_adcode_id);
		if($tac_fk_adcode_id != "")
		{
			$dsp_string = "広告コードが重複しています。<br>\n";
			$back_string .= "<font><b><A href='adcode_regist.php?sid=$sid&aid=$aid&limit=$limit&offset=$offset&page=$page&sort=$sort'>編集に戻る</A></b></font>\n";
			$back_string .= "<br><br>\n";
		}
		else
		{
			//広告主登録関数
			$ret = RegistTAdmaster($k_adname,$f_agent_name,$f_mail_address,$f_login_id,$f_login_pass,$f_pay_type,$f_pay_value,$f_memo,$f_adcode,$f_type,$fk_admaster_id,$f_mem_reg_urls);
			if($ret == false)
			{
				$dsp_string = "登録処理に失敗しました。<br>\n";
			}
			else
			{
//G.Chin 2010-07-27 add sta
				//▼広告管理画面許可権を判定
				if(ADMASTER_PERMISSION == 1)
				{
					//広告主表示情報更新関数
					UpdateTAdmasterDispInfo($aid,$f_disp_hour,$f_disp_pay,$f_disp_today);
				}
//G.Chin 2010-07-27 add end
				
				//▼アフィリエイト有効の場合
				if($afflt_status == 1)
				{
					//広告主検索広告コード取得関数
					SrcAdmasterGetTAdcode($fk_admaster_id,$ta_fk_adcode_id,$ta_f_adcode,$ta_f_type,$ta_f_tm_stamp);
					//アフィリエイト広告登録関数
//G.Chin 2010-07-28 chg sta
//					$ret = RegistTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
//G.Chin 2010-08-06 chg sta
//					$ret = RegistTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_img_url,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
//					$ret = RegistTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_imgurl_pc,$f_imgurl_mb,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);	//G.Chin 2010-10-28 del
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
					if($ret == false)
					{
						$dsp_string = "アフィリエイト登録処理に失敗しました。<br>\n";
					}
					else
					{
						$dsp_string = "広告主の新規登録処理に成功しました。<br>\n";
						
						//アフィリエイト・リスティング広告更新関数
//G.Chin 2010-08-27 chg sta
//						UpdateTAffiliateFListing($ta_fk_adcode_id,$f_listing);	//G.Chin 2010-08-02 add
//						UpdateTAffiliateFListing($ta_fk_adcode_id,$f_listing,$f_listing2);	//G.Chin 2010-10-28 del
//G.Chin 2010-08-27 chg end
//G.Chin 2010-10-06 add sta
						//アフィリエイト追加タグ更新関数
//						UpdateTAffiliateAddTag($ta_fk_adcode_id,$f_add_tag);	//G.Chin 2010-10-28 del
//G.Chin 2010-10-06 add end
					}
				}
				else
				{
					$dsp_string = "広告主の新規登録処理に成功しました。<br>\n";
				}
			}
		}
	}
	else
	{
		//広告コードID重複チェック関数
		CheckTAdcodeId($aid, $f_adcode, $tac_data_cnt);
		if($tac_data_cnt > 0)
		{
			$dsp_string = "広告コードが重複しています。<br>\n";
			$back_string .= "<font><b><A href='adcode_regist.php?sid=$sid&aid=$aid&limit=$limit&offset=$offset&page=$page&sort=$sort'>編集に戻る</A></b></font>\n";
			$back_string .= "<br><br>\n";
		}
		else
		{
			//広告主情報更新関数
			$ret = UpdateTAdmasterInfo($aid,$k_adname,$f_agent_name,$f_mail_address,$f_login_id,$f_login_pass,$f_pay_type,$f_pay_value,$f_memo,$f_adcode,$f_type,$f_mem_reg_urls);
			if($ret == false)
			{
				$dsp_string = "更新処理に失敗しました。<br>\n";
			}
			else
			{
//G.Chin 2010-07-27 add sta
				//▼広告管理画面許可権を判定
				if(ADMASTER_PERMISSION == 1)
				{
					//広告主表示情報更新関数
					UpdateTAdmasterDispInfo($aid,$f_disp_hour,$f_disp_pay,$f_disp_today);
				}
//G.Chin 2010-07-27 add end
				
				//広告主検索広告コード取得関数
				SrcAdmasterGetTAdcode($aid,$ta_fk_adcode_id,$ta_f_adcode,$ta_f_type,$ta_f_tm_stamp);
//G.Chin 2010-10-28 chg sta
/*
				//アフィリエイト広告件数取得関数
				GetTAffiliateCount($ta_fk_adcode_id,$afflt_cnt);
				if($afflt_cnt > 0)
				{
					//アフィリエイト広告更新関数
//G.Chin 2010-07-28 chg sta
//					$ret = UpdateTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
//G.Chin 2010-08-06 chg sta
//					$ret = UpdateTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_img_url,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
					$ret = UpdateTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_imgurl_pc,$f_imgurl_mb,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
					if($ret == false)
					{
						$dsp_string = "アフィリエイト更新処理に失敗しました。<br>\n";
					}
					else
					{
						$dsp_string = "広告主情報の更新処理に成功しました。<br>\n";
						
						//アフィリエイト・リスティング広告更新関数
//G.Chin 2010-08-27 chg sta
//						UpdateTAffiliateFListing($ta_fk_adcode_id,$f_listing);	//G.Chin 2010-08-02 add
						UpdateTAffiliateFListing($ta_fk_adcode_id,$f_listing,$f_listing2);
//G.Chin 2010-08-27 chg end
//G.Chin 2010-10-06 add sta
						//アフィリエイト追加タグ更新関数
						UpdateTAffiliateAddTag($ta_fk_adcode_id,$f_add_tag);
//G.Chin 2010-10-06 add end
					}
				}
				else
				{
//G.Chin 2010-08-03 chg sta
//					$dsp_string = "広告主情報の更新処理に成功しました。<br>\n";
					//▼アフィリエイト有効の場合
					if($afflt_status == 1)
					{
						//アフィリエイト広告登録関数
//G.Chin 2010-08-06 chg sta
//						$ret = RegistTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_img_url,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
						$ret = RegistTAffiliate($ta_fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_imgurl_pc,$f_imgurl_mb,$f_param1,$f_param2,$afflt_type,$afflt_status,$f_bank_pay,$f_report);
//G.Chin 2010-08-06 chg end
						if($ret == false)
						{
							$dsp_string = "アフィリエイト登録処理に失敗しました。<br>\n";
						}
						else
						{
							$dsp_string = "広告主情報の更新処理に成功しました。<br>\n";
							
							//アフィリエイト・リスティング広告更新関数
//G.Chin 2010-08-27 chg sta
//							UpdateTAffiliateFListing($ta_fk_adcode_id,$f_listing);
							UpdateTAffiliateFListing($ta_fk_adcode_id,$f_listing,$f_listing2);
//G.Chin 2010-08-27 chg end
//G.Chin 2010-10-06 add sta
							//アフィリエイト追加タグ更新関数
							UpdateTAffiliateAddTag($ta_fk_adcode_id,$f_add_tag);
//G.Chin 2010-10-06 add end
						}
					}
				}
//G.Chin 2010-08-03 chg end
*/
				$dsp_string = "広告主情報の更新処理に成功しました。<br>\n";
//G.Chin 2010-10-28 chg end
			}
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= $back_string;
	$dsp_tbl .= "<font><b><A href='adcode_list.php?sid=$sid&limit=$limit&offset=$offset&page=$page&sort=$sort&inp_adcode=$inp_adcode'>リストに戻る</A></b></font>\n";
/*
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
*/
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告媒体編集完了",$dsp_tbl);

?>

