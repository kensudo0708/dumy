<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];	//AREQ-77 G.Chin 2010-08-05 add
	$inp_adcode = $_REQUEST["inp_adcode"];
	
	if(($aid != "") && ($aid != 0))
	{
		//広告主情報取得関数
		GetTAdmasterInfo($aid,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($aid,$tac_fk_adcode_id,$tac_f_adcode,$tac_f_type,$tac_f_tm_stamp,$tac_f_mem_reg_urls);
//G.Chin 2010-10-27 del sta
/*
		//アフィリエイト広告情報取得関数
//G.Chin 2010-07-28 chg sta
//		GetTAffiliateInfo($tac_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
//G.Chin 2010-08-06 chg sta
//		GetTAffiliateInfo($tac_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_img_url,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
		GetTAffiliateInfo($tac_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_imgurl_pc,$taf_f_imgurl_mb,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
		//アフィリエイト広告報告回数取得関数
		GetTAffiliateReportCount($tac_fk_adcode_id,$taf_f_reg_report,$taf_f_buy_report);
		//アフィリエイト・リスティング広告取得関数
//G.Chin 2010-08-27 chg sta
//		GetTAffiliateFListing($tac_fk_adcode_id,$taf_f_listing);	//G.Chin 2010-08-02 add
		GetTAffiliateFListing($tac_fk_adcode_id,$taf_f_listing,$taf_f_listing2);
//G.Chin 2010-08-27 chg end
//G.Chin 2010-10-06 add sta
		//アフィリエイト追加タグ取得関数
		GetTAffiliateAddTag($tac_fk_adcode_id,$taf_f_add_tag);
//G.Chin 2010-10-06 add end
*/
//G.Chin 2010-10-27 del end
		//広告主表示情報取得関数
		GetTAdmasterDispInfo($aid,$tam_f_disp_hour,$tam_f_disp_pay,$tam_f_disp_today);	//G.Chin 2010-07-27 add
	}
	else
	{
		$tam_k_adname		= "";
		$tam_f_agent_name	= "";
		$tam_f_tel_no		= "";
		$tam_f_mail_address	= "";
		$tam_f_login_id		= "";
		$tam_f_login_pass	= "";
		$tam_f_kickback_url	= "";
		$tam_f_pay_type		= "0";
		$tam_f_pay_value	= "0";
		$tam_f_memo			= "";
		$tam_f_counter		= "0";
		$tam_f_tm_stamp		= "";
		
		$tac_f_adcode		= "";
		$tac_f_mem_reg_urls		= "";
		
//G.Chin 2010-10-27 del sta
/*
		$taf_fk_program_id		= "";
		$taf_f_regist			= 0;
		$taf_f_regist_value		= 0;
		$taf_f_first_buy		= 0;
		$taf_f_first_buy_price	= 0;
		$taf_f_first_buy_value	= 0;
//G.Chin 2010-08-06 chg sta
//		$taf_f_img_url			= "";	//G.Chin 2010-07-28 add
		$taf_f_terminal			= "";
		$taf_f_imgurl_pc		= "";
		$taf_f_imgurl_mb		= "";
//G.Chin 2010-08-06 chg end
		$taf_f_param1			= "";
		$taf_f_param2			= "";
		$taf_f_type				= "";
		$taf_f_status			= 0;
		$taf_f_bank_pay			= 0;
		$taf_f_report			= 0;
		$taf_f_buy_count		= 0;
		$taf_f_reg_report		= 0;
		$taf_f_buy_report		= 0;
		$taf_f_listing			= "";	//G.Chin 2010-08-02 add
		$taf_f_listing2			= "";	//G.Chin 2010-08-27 add
		$taf_f_add_tag			= "";	//G.Chin 2010-10-06 add
*/
//G.Chin 2010-10-27 del end
		
//G.Chin 2010-07-27 add sta
		$tam_f_disp_hour		= 0;
		$tam_f_disp_pay			= 0;
		$tam_f_disp_today		= 0;
//G.Chin 2010-07-27 add end
	}
	
	
//G.Chin AWKT-937 2010-12-20 add sta
	if($tac_f_adcode == "0")
	{
		print "NOPコードは編集できません。<br>\n";
		exit;
	}
//G.Chin AWKT-937 2010-12-20 add end
	
	//▼登録URL
//G.Chin 2010-08-13 chg sta
/*
	$regist_url = SITE_URL."index.php/a/".$tac_f_adcode."<br>"
					.SITE_URL."index.php/adcode/".$tac_f_adcode."<br>"
					.SITE_URL."index.php/index/execute/a/".$tac_f_adcode."<br>"
					.SITE_URL."index.php/index/execute/adcode/".$tac_f_adcode."<br>";
*/
	$regist_url = SITE_URL."index.php/a/".$tac_f_adcode;
	$category_url = SITE_URL."index.php/a/".$tac_f_adcode."/category/id/(入力値)";
	$product_url = SITE_URL."index.php/a/".$tac_f_adcode."/product/pro_id/(入力値)";
	$free_url = SITE_URL."index.php/free?p=(ﾍﾟｰｼﾞNo)&a=".$tac_f_adcode;			//G.Chin AWKC-199 2010-10-20 add
	$ar_url = SITE_URL."index.php/ar/".$tac_f_adcode;							//G.Chin AWKC-198 2010-10-22 add
//G.Chin 2010-08-13 chg end
//G.Chin AWKC-199 2010-10-20 add sta
	$afl_regist_url = SITE_URL."index.php/a/".$tac_f_adcode."?aflkey=(ﾕﾆｰｸｺｰﾄﾞ)";
	$afl_category_url = SITE_URL."index.php/a/".$tac_f_adcode."/category/id/(入力値)?aflkey=(ﾕﾆｰｸｺｰﾄﾞ)";
	$afl_product_url = SITE_URL."index.php/a/".$tac_f_adcode."/product/pro_id/(入力値)?aflkey=(ﾕﾆｰｸｺｰﾄﾞ)";
	$afl_free_url = SITE_URL."index.php/free?p=(ﾍﾟｰｼﾞNo)&a=".$tac_f_adcode."?aflkey=(ﾕﾆｰｸｺｰﾄﾞ)";
	$afl_ar_url = SITE_URL."index.php/ar/".$tac_f_adcode."?aflkey=(ﾕﾆｰｸｺｰﾄﾞ)";	//G.Chin AWKC-198 2010-10-22 add
//G.Chin AWKC-199 2010-10-20 add end
	
	//▼種類(アフィリエイト)
//G.Chin 2010-07-28 chg sta
/*
	$f_type_id[0] = "0";
	$f_type_name[0] = "A8";
	$f_type_id[1] = "1";
	$f_type_name[1] = "M8";
	$f_type_id[2] = "2";
	$f_type_name[2] = "その他";
	$f_type_cnt = 3;
*/
//G.Chin 2010-10-27 del sta
/*
	//アフィリエイト種別一覧取得関数
	GetTAfflTypeList($f_type_id,$f_type_name,$f_type_cnt);
//G.Chin 2010-07-28 chg end
	$name = "f_type";
	$select_num = $taf_f_type;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_type_id, $f_type_name, $f_type_cnt, $select_num, $type_select);
	
//G.Chin 2010-08-06 add sta
	//▼端末種別
	$f_terminal_id[0] = "0";
	$f_terminal_name[0] = "PC";
	$f_terminal_id[1] = "1";
	$f_terminal_name[1] = "MB";
	$f_terminal_id[2] = "2";
	$f_terminal_name[2] = "両方";
	$f_terminal_cnt = 3;
	$name = "f_terminal";
	$select_num = $taf_f_terminal;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_terminal_id, $f_terminal_name, $f_terminal_cnt, $select_num, $terminal_select);
//G.Chin 2010-08-06 add end
	
	//▼銀行振込フラグ(アフィリエイト)
	$f_bank_pay_id[0] = "0";
	$f_bank_pay_name[0] = "含まない";
	$f_bank_pay_id[1] = "1";
	$f_bank_pay_name[1] = "含む";
	$f_bank_pay_cnt = 2;
	$name = "f_bank_pay";
	$select_num = $taf_f_bank_pay;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_bank_pay_id, $f_bank_pay_name, $f_bank_pay_cnt, $select_num, $bank_select);
*/
//G.Chin 2010-10-27 del end
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='adcode_regist_result.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table class='form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$aid</font>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin AWKC-199 2010-10-20 chg sta
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> 登録URL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$regist_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> ｶﾃｺﾞﾘﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$category_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> 商品ﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$product_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin AWKC-199 2010-10-20 add sta
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> ﾌﾘｰﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$free_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin AWKC-199 2010-10-20 add end
*/
//G.Chin 2010-12-15 chg sta
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 登録URL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$regist_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 登録URL<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_regist_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> TOPﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$regist_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> TOPﾍﾟｰｼﾞURL<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_regist_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-12-15 chg end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ｶﾃｺﾞﾘﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$category_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ｶﾃｺﾞﾘﾍﾟｰｼﾞURL<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_category_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 商品ﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$product_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 商品ﾍﾟｰｼﾞURL<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_product_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ﾌﾘｰﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$free_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ﾌﾘｰﾍﾟｰｼﾞURL<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_free_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin AWKC-199 2010-10-20 chg end
	
//G.Chin AWKC-198 2010-10-22 add sta
//G.Chin 2010-12-15 chg sta
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 外部リンク </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$ar_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 外部リンク<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_ar_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 登録ﾍﾟｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$ar_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 登録ﾍﾟｰｼﾞURL<BR>(ｱﾌｨﾘｴｲﾄｷｰ付) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$afl_ar_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-12-15 chg end
//G.Chin AWKC-198 2010-10-22 add end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 広告コード </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_adcode' value='$tac_f_adcode' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 広告主名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='k_adname' value='$tam_k_adname' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 代理店名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_agent_name' value='$tam_f_agent_name' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> 出稿価格 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_pay_value' value='$tam_f_pay_value' size='15'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ログインID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_login_id' value='$tam_f_login_id' size='15'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ﾛｸﾞｲﾝﾊﾟｽﾜｰﾄﾞ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_login_pass' value='$tam_f_login_pass' size='15'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> ﾒｰﾙｱﾄﾞﾚｽ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_mail_address' value='$tam_f_mail_address' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><tt> メモ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_memo' cols='50' rows='5'>$tam_f_memo</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin 2010-07-27 add sta
	//▼広告管理画面許可権を判定
	if(ADMASTER_PERMISSION == 1)
	{
		//▼時刻表示フラグ
		$f_disp_hour_id[0] = "0";
		$f_disp_hour_name[0] = "可";
		$f_disp_hour_id[1] = "1";
		$f_disp_hour_name[1] = "不可";
		$f_disp_hour_cnt = 2;
		$name = "f_disp_hour";
		$select_num = $tam_f_disp_hour;
		//選択オブジェクト作成関数
		MakeSelectObject($name, $f_disp_hour_id, $f_disp_hour_name, $f_disp_hour_cnt, $select_num, $hour_select);
		
		//▼購入表示フラグ
		$f_disp_pay_id[0] = "0";
		$f_disp_pay_name[0] = "表示";
		$f_disp_pay_id[1] = "1";
		$f_disp_pay_name[1] = "非表示";
		$f_disp_pay_cnt = 2;
		$name = "f_disp_pay";
		$select_num = $tam_f_disp_pay;
		//選択オブジェクト作成関数
		MakeSelectObject($name, $f_disp_pay_id, $f_disp_pay_name, $f_disp_pay_cnt, $select_num, $pay_select);
		
		//▼購入表示フラグ
		$f_disp_today_id[0] = "0";
		$f_disp_today_name[0] = "表示";
		$f_disp_today_id[1] = "1";
		$f_disp_today_name[1] = "非表示";
		$f_disp_today_cnt = 2;
		$name = "f_disp_today";
		$select_num = $tam_f_disp_today;
		//選択オブジェクト作成関数
		MakeSelectObject($name, $f_disp_today_id, $f_disp_today_name, $f_disp_today_cnt, $select_num, $today_select);
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th><tt> 時刻表示 </tt></th>\n";
		$dsp_tbl .= "<td colspan=3><font color='#B22222'>$hour_select</font></td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th><tt> 購入表示 </tt></th>\n";
		$dsp_tbl .= "<td colspan=3><font color='#B22222'>$pay_select</font></td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th><tt> 当日集計表示 </tt></th>\n";
		$dsp_tbl .= "<td colspan=3><font color='#B22222'>$today_select</font></td>\n";
		$dsp_tbl .= "</tr>\n";

            if ( defined('MEM_DIRECT_REG_ENABLE') && constant('MEM_DIRECT_REG_ENABLE') ) {
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th><tt> 会員登録受入URL </tt></th>\n";
        $dsp_tbl .= "<td colspan=3>\n";
		$dsp_tbl .= "<input type='text' name='f_mem_reg_urls' value='$tac_f_mem_reg_urls' size='80' style='ime-mode:disabled'><br>\n";
        $dsp_tbl .= "・この項目はダイレクト会員登録機能のための設定項目です。<br>\n";
        $dsp_tbl .= "・本項目に登録フォームを設置するページのURLを設定します。<br>\n";
        $dsp_tbl .= "・設定値は登録フォームを設置したページのURLと部分一致で評価され、半角セミコロン区切りで複数列挙することができます。<br>\n";
        $dsp_tbl .= "・ユーザーから送信されるリファラ情報を元にチェックされるため、意図的にリファラの送信を無効化しているユーザーは拒否されます。\n";
                if ( constant('MEM_DIRECT_REG_ACTIVITY') ) {
        $dsp_tbl .= "<br><b><font color='red'>！！【本登録モード】で運用中！！</font></b><br>\n";
        $dsp_tbl .= "・現在、本機能を【本登録モード】で運用しているため、【会員登録】を成果地点とするアフィリエイトプログラムには利用できません。(システム上、成果報告が不可能であるため)\n";
                }
        $dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
            }
        }
//G.Chin 2010-07-27 add end
	
//G.Chin 2010-10-27 del sta
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ｱﾌｨﾘｴｲﾄ使用 </tt></th>\n";
	if($taf_f_status == 0)
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='afflt_status' value=0 checked>無効 <input type='radio' name='afflt_status' value=1>有効</font></td>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='afflt_status' value=0>無効 <input type='radio' name='afflt_status' value=1 checked>有効</font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ﾌﾟﾛｸﾞﾗﾑID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='fk_program_id' value='$taf_fk_program_id' size='80'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 登録時報酬 </tt></th>\n";
	if($taf_f_regist == 0)
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_regist' value=0 checked>無し <input type='radio' name='f_regist' value=1>有り</font></td>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_regist' value=0>無し <input type='radio' name='f_regist' value=1 checked>有り</font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 登録時報酬額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_regist_value' value='$taf_f_regist_value' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 初回ｺｲﾝ購入報酬 </tt></th>\n";
	if($taf_f_first_buy == 0)
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_first_buy' value=0 checked>無し <input type='radio' name='f_first_buy' value=1>有り</font></td>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_first_buy' value=0>無し <input type='radio' name='f_first_buy' value=1 checked>有り</font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 初回ｺｲﾝ購入最低額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_first_buy_price' value='$taf_f_first_buy_price' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 初回ｺｲﾝ購入報酬額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_first_buy_value' value='$taf_f_first_buy_value' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 種類 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$type_select</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
*/
//G.Chin 2010-07-28 add sta
//G.Chin 2010-08-06 chg sta
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ｲﾒｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_img_url' value='$taf_f_img_url' size='80'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 端末種別 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$terminal_select</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> PC用ｲﾒｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_imgurl_pc' value='$taf_f_imgurl_pc' size='80'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> MB用ｲﾒｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_imgurl_mb' value='$taf_f_imgurl_mb' size='80'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 add end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> SI用ﾊﾟﾗﾒｰﾀ(登録) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_param1' value='$taf_f_param1' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> SI用ﾊﾟﾗﾒｰﾀ(購入) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_param2' value='$taf_f_param2' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 銀行振込ﾌﾗｸﾞ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$bank_select</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 報告ﾊﾟﾗﾒｰﾀ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_report' value='$taf_f_report' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 登録報告数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_reg_report 回 (全登録:$tam_f_counter 回)</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ｺｲﾝ購入報告数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_buy_report 回 (全購入:$taf_f_buy_count 回)</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin 2010-08-02 add sta
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ﾘｽﾃｨﾝｸﾞ広告<br>(通常はTOP用) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_listing' cols='50' rows='20'>$taf_f_listing</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-08-02 add end
	
//G.Chin 2010-08-27 add sta
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ﾘｽﾃｨﾝｸﾞ広告2<br>(通常は登録完了用) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_listing2' cols='50' rows='20'>$taf_f_listing2</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-08-27 add end
	
//G.Chin 2010-10-06 add sta
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 追加タグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_add_tag' cols='50' rows='20'>$taf_f_add_tag</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-10-06 add end
*/
//G.Chin 2010-10-27 del end
	
//G.Chin 2010-10-27 add sta
	if(($aid != "") && ($aid != 0))
	{
		$link_affiliate = "<A href='adcode_affiliate.php?sid=$sid&aid=$aid&limit=$limit&offset=$offset&page=$page&sort=$sort&inp_adcode=$inp_adcode'>登録</A>";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th style='background:#C0C0C0'><tt> ｱﾌｨﾘｴｲﾄ登録 </tt></th>\n";
		$dsp_tbl .= "<td colspan=3>$link_affiliate</font>\n";
		$dsp_tbl .= "</tr>\n";
	}
//G.Chin 2010-11-09 add sta
	else
	{
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th style='background:#C0C0C0'><tt> ｱﾌｨﾘｴｲﾄ登録 </tt></th>\n";
		$dsp_tbl .= "<td colspan=3>ｱﾌｨﾘｴｲﾄの登録は、広告ｺｰﾄﾞの登録が完了したのち可能です。<BR>広告ｺｰﾄﾞを登録後、一覧画面の「編集」ﾘﾝｸをｸﾘｯｸし、再度この編集画面を開くとｱﾌｨﾘｴｲﾄ登録画面へのﾘﾝｸが表示されます。</font>\n";
		$dsp_tbl .= "</tr>\n";
	}
//G.Chin 2010-11-09 add end
//G.Chin 2010-10-27 add end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='aid' value='$aid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value='$page'>\n";
	$dsp_tbl .= "<input type='hidden' name='sort' value='$sort'>";	//AREQ-77 G.Chin 2010-08-05 add
	$dsp_tbl .= "<input type='hidden' name='inp_adcode' value='$inp_adcode'>\n";
	$dsp_tbl .= "<button type='submit' style='width:110px;'>更新</button>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告アフィリエイト編集",$dsp_tbl);

?>
