<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/07/05												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];	//AREQ-77 G.Chin 2010-08-05 add
	
	if(($aid != "") && ($aid != 0))
	{
		//広告主情報取得関数
		GetTAdmasterInfo($aid,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($aid,$tac_fk_adcode_id,$tac_f_adcode,$tac_f_type,$tac_f_tm_stamp);
		//アフィリエイト広告情報取得関数
//G.Chin 2010-07-28 chg sta
//		GetTAffiliateInfo($tac_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
//G.Chin 2010-08-06 chg sta
//		GetTAffiliateInfo($tac_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_img_url,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
		GetTAffiliateInfo($tac_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_imgurl_pc,$taf_f_imgurl_mb,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
	}
	else
	{
		print "削除する広告が選択されていません<br>\n";
		exit;
	}
	
	
//G.Chin AWKT-937 2010-12-20 add sta
	if($tac_f_adcode == "0")
	{
		print "NOPコードは削除できません。<br>\n";
		exit;
	}
//G.Chin AWKT-937 2010-12-20 add end
	
	//▼登録URL
	$regist_url = SITE_URL."index.php/a/".$tac_f_adcode."<br>"
					.SITE_URL."index.php/adcode/".$tac_f_adcode."<br>"
					.SITE_URL."index.php/index/execute/a/".$tac_f_adcode."<br>"
					.SITE_URL."index.php/index/execute/adcode/".$tac_f_adcode."<br>";
	
//G.Chin 2010-08-06 add sta
	//▼端末種別
	switch($taf_f_terminal)
	{
		case 0:	$f_terminal_str = "PC";		break;
		case 1: $f_terminal_str = "MB";		break;
		case 2:	$f_terminal_str = "両方";	break;
	}
//G.Chin 2010-08-06 add end
	
	//▼種類(アフィリエイト)
	switch($taf_f_type)
	{
		case 0:	$f_type_str = "A8";		break;
		case 1: $f_type_str = "M8";		break;
		case 2:	$f_type_str = "その他";	break;
	}
	
	//▼アフィリエイト使用
	if($taf_f_status == 0)
	{
		$f_status_str = "無効";
	}
	else
	{
		$f_status_str = "有効";
	}
	
	//▼登録時報酬
	if($taf_f_regist == 0)
	{
		$f_regist_str = "無し";
	}
	else
	{
		$f_regist_str = "有り";
	}
	
	//▼初回ｺｲﾝ購入報酬
	if($taf_f_first_buy == 0)
	{
		$f_first_buy_str = "無し";
	}
	else
	{
		$f_first_buy_str = "有り";
	}
	
	//▼銀行振込フラグ
	if($taf_f_bank_pay == 0)
	{
		$f_bank_pay_str = "含まない";
	}
	else
	{
		$f_bank_pay_str = "含む";
	}
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='adcode_delete_result.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=650>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$aid</font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> 登録URL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$regist_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> 広告コード </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tac_f_adcode</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> 広告主名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_k_adname</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> 代理店名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_f_agent_name</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> 出稿価格 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_f_pay_value 円</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> ログインID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_f_login_id</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> ﾛｸﾞｲﾝﾊﾟｽﾜｰﾄﾞ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_f_login_pass</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> ﾒｰﾙｱﾄﾞﾚｽ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_f_mail_address</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> メモ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3>$tam_f_memo</font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> ｱﾌｨﾘｴｲﾄ使用 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_status_str</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> ﾌﾟﾛｸﾞﾗﾑID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_fk_program_id</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 登録時報酬 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_regist_str</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 登録時報酬額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_regist_value 円</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 初回ｺｲﾝ購入報酬 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_first_buy_str</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 初回ｺｲﾝ購入最低額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_first_buy_price 円</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 初回ｺｲﾝ購入報酬額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_first_buy_value 円</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 種類 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_type_str</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin 2010-07-28 add sta
//G.Chin 2010-08-06 chg sta
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> ｲﾒｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_img_url</font></td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 端末種別 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_terminal_str</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> PC用ｲﾒｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_imgurl_pc</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> MB用ｲﾒｰｼﾞURL </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_imgurl_mb</font></td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 add end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> SI用ﾊﾟﾗﾒｰﾀ(登録) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_param1</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> SI用ﾊﾟﾗﾒｰﾀ(購入) </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_param2</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 銀行振込ﾌﾗｸﾞ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_bank_pay_str</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 報告ﾊﾟﾗﾒｰﾀ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_report</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> ｺｲﾝ購入回数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_buy_count 回</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	
	$dsp_tbl .= "<input type='hidden' name='aid' value='$aid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value='$page'>\n";
	$dsp_tbl .= "<input type='hidden' name='sort' value='$sort'>";	//AREQ-77 G.Chin 2010-08-05 add
	$dsp_tbl .= "この広告コードを削除します。よろしければ、下のボタンをクリックしてください。<br>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='削除'>\n";
	$dsp_tbl .= "</FORM>\n";
	
	$dsp_tbl .= "<FORM action='adcode_list.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value='$page'>\n";
	$dsp_tbl .= "<input type='hidden' name='sort' value='$sort'>";	//AREQ-77 G.Chin 2010-08-05 add
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='ｷｬﾝｾﾙ'>\n";
	$dsp_tbl .= "</FORM>\n";
	
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告媒体削除",$dsp_tbl);

?>
