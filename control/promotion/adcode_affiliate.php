<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/10/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	$sort = $_REQUEST["sort"];	//AREQ-77 G.Chin 2010-08-05 add
	$inp_adcode = $_REQUEST["inp_adcode"];
	
	if(($aid != "") && ($aid != 0))
	{
		//広告主情報取得関数
		GetTAdmasterInfo($aid,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($aid,$tac_fk_adcode_id,$tac_f_adcode,$tac_f_type,$tac_f_tm_stamp);
		//アフィリエイト広告情報取得関数２
		GetTAffiliateInfo2($tac_fk_adcode_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count,$taf_f_reg_report,$taf_f_buy_report,$taf_f_top_pc,$taf_f_top_mb,$taf_f_reg_pc,$taf_f_reg_mb,$taf_f_buy_pc,$taf_f_buy_mb);
		//アフィリエイト広告報告回数取得関数
		GetTAffiliateReportCount($tac_fk_adcode_id,$taf_f_reg_report,$taf_f_buy_report);
	}
	else
	{
		$tam_k_adname		= "";
		$tam_f_agent_name	= "";
		$tam_f_tel_no		= "";
		$tam_f_mail_address	= "";
		$tam_f_login_id		= "";
		$tam_f_login_pass	= "";
		$tam_f_kickback_url	= "";
		$tam_f_pay_type		= "0";
		$tam_f_pay_value	= "0";
		$tam_f_memo			= "";
		$tam_f_counter		= "0";
		$tam_f_tm_stamp		= "";
		
		$tac_f_adcode		= "";
		
		$taf_fk_program_id		= "";
		$taf_f_regist			= 0;
		$taf_f_regist_value		= 0;
		$taf_f_first_buy		= 0;
		$taf_f_first_buy_price	= 0;
		$taf_f_first_buy_value	= 0;
		$taf_f_terminal			= "";
		$taf_f_imgurl_pc		= "";
		$taf_f_imgurl_mb		= "";
		$taf_f_param1			= "";
		$taf_f_param2			= "";
		$taf_f_type				= "";
		$taf_f_status			= 0;
		$taf_f_bank_pay			= 0;
		$taf_f_report			= 0;
		$taf_f_buy_count		= 0;
		$taf_f_reg_report		= 0;
		$taf_f_buy_report		= 0;
		$taf_f_listing			= "";
		$taf_f_listing2			= "";
	}
	
	
	//▼種類(アフィリエイト)
	//アフィリエイト種別一覧取得関数２
	GetTAfflTypeList2($f_type_id,$f_type_name,$f_type_cnt);
	$name = "f_type";
	$select_num = $taf_f_type;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_type_id, $f_type_name, $f_type_cnt, $select_num, $type_select);
	
//G.Chin 2010-08-06 add sta
	//▼端末種別
	$f_terminal_id[0] = "0";
	$f_terminal_name[0] = "PC";
	$f_terminal_id[1] = "1";
	$f_terminal_name[1] = "MB";
	$f_terminal_id[2] = "2";
	$f_terminal_name[2] = "両方";
	$f_terminal_cnt = 3;
	$name = "f_terminal";
	$select_num = $taf_f_terminal;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_terminal_id, $f_terminal_name, $f_terminal_cnt, $select_num, $terminal_select);
//G.Chin 2010-08-06 add end
	
	//▼銀行振込フラグ(アフィリエイト)
	$f_bank_pay_id[0] = "0";
	$f_bank_pay_name[0] = "含まない";
	$f_bank_pay_id[1] = "1";
	$f_bank_pay_name[1] = "含む";
	$f_bank_pay_cnt = 2;
	$name = "f_bank_pay";
	$select_num = $taf_f_bank_pay;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_bank_pay_id, $f_bank_pay_name, $f_bank_pay_cnt, $select_num, $bank_select);
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='adcode_affiliate_result.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=650>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$aid</font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> 広告コード </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tac_f_adcode</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> 広告主名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_k_adname</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right width=150><tt> 代理店名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$tam_f_agent_name</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ｱﾌｨﾘｴｲﾄ使用 </tt></th>\n";
	if($taf_f_status == 0)
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='afflt_status' value=0 checked>無効 <input type='radio' name='afflt_status' value=1>有効</font></td>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='afflt_status' value=0>無効 <input type='radio' name='afflt_status' value=1 checked>有効</font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 登録時報酬 </tt></th>\n";
	if($taf_f_regist == 0)
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_regist' value=0 checked>無し <input type='radio' name='f_regist' value=1>有り</font></td>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_regist' value=0>無し <input type='radio' name='f_regist' value=1 checked>有り</font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 登録時報酬額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_regist_value' value='$taf_f_regist_value' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 初回ｺｲﾝ購入報酬 </tt></th>\n";
	if($taf_f_first_buy == 0)
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_first_buy' value=0 checked>無し <input type='radio' name='f_first_buy' value=1>有り</font></td>\n";
	}
	else
	{
		$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='radio' name='f_first_buy' value=0>無し <input type='radio' name='f_first_buy' value=1 checked>有り</font></td>\n";
	}
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 初回ｺｲﾝ購入最低額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_first_buy_price' value='$taf_f_first_buy_price' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 初回ｺｲﾝ購入報酬額 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_first_buy_value' value='$taf_f_first_buy_value' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 種類 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$type_select</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 端末種別 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$terminal_select</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 銀行振込ﾌﾗｸﾞ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$bank_select</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 報告ﾊﾟﾗﾒｰﾀ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_report' value='$taf_f_report' size='4'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 登録報告数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_reg_report 回 (全登録:$tam_f_counter 回)</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> ｺｲﾝ購入報告数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>$taf_f_buy_report 回 (全購入:$taf_f_buy_count 回)</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> 置換文字 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'>会員ID：&nbsp;&nbsp&nbsp%%MEMBER_ID%%<BR>日時：&nbsp;&nbsp&nbsp&nbsp&nbsp%%DATE%%<BR>金額：&nbsp;&nbsp&nbsp&nbsp&nbsp%%MONEY%%<BR>ｱﾌｨﾘｴｲﾄｷｰ：&nbsp;%%AFLKEY%%<BR>機種情報：&nbsp;%%USER_AGENT%%</font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> PC用TOPタグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_top_pc' cols='50' rows='20'>$taf_f_top_pc</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> MB用TOPタグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_top_mb' cols='50' rows='20'>$taf_f_top_mb</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> PC用登録タグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_reg_pc' cols='50' rows='20'>$taf_f_reg_pc</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> MB用登録タグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_reg_mb' cols='50' rows='20'>$taf_f_reg_mb</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> PC用購入タグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_buy_pc' cols='50' rows='20'>$taf_f_buy_pc</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right width=150><tt> MB用購入タグ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><textarea name='f_buy_mb' cols='50' rows='20'>$taf_f_buy_mb</textarea></font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='aid' value='$aid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value='$page'>\n";
	$dsp_tbl .= "<input type='hidden' name='sort' value='$sort'>";
	$dsp_tbl .= "<input type='hidden' name='inp_adcode' value='$inp_adcode'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='更新'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("広告媒体編集",$dsp_tbl);

?>
