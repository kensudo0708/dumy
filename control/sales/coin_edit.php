<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../control/css/layout.css" type="text/css" rel="stylesheet">
        <title></title>
    </head>
<script type="text/javascript">
<!--
function disp(mss){
        window.confirm(mss)
}
function del(id,dltid){
    if(window.confirm("削除しますか？")){

		parent.main_r.location.href = "../sales/coin_edit.php?act=del&id="+id+"&dltid="+dltid;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		window.alert('キャンセルされました'); // 警告ダイアログを表示

	}
	// 「キャンセル」時の処理終了
}
</script>
<?PHP
session_start();
if($_SESSION['staff_id']==NULL)
{
?>
<body>
    <P>不正アクセスです</P>

</body>
</html>
<?PHP
    return ;
}
if(empty($_GET['id'])==false)
{
    $id=$_GET['id'];
}
else if(empty($_POST['id'])==false)
{
    $id=$_POST['id'];
}
else {
?>
<body>
   <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>

</body>
</html>
<?PHP
    return ;
}
include '../../lib/define.php';
include '../control/exec_select.php';
include '../control/exec_update.php';
include '../control/exec_insert.php';
include '../control/exec_delete.php';

$mss="";
if(empty($_POST['act'])==FALSE)
{
    if($_POST['act']=='GEDT')
    {
          $rs=update_coin_group($id,$_POST['name'], $_POST['memo']);
          if($rs != TRUE)
          {
                $mss="コイングループの変更に成功しました";
          }
          else
          {
              $mss="コイングループの変更に失敗しました";
          }
    }
    else if($_POST['act']=='CEDT')
    {
        if(empty($_POST["cid"])==FALSE)
        {
            $rs=update_coin_setup($_POST["cid"],$_POST['im'],$_POST['coin'],$_POST['free_coin']);
        }
        $rs=insert_coin_setup($_POST['new_im'],$_POST['new_coin'],$_POST['new_f_coin'],$id);
        if($rs != TRUE)
        {
                $mss="コインの変更に成功しました";
        }
        else
        {
              $mss="コインの変更に失敗しました";
        }
    }
?>
<body onload="disp('<?PHP print($mss);?>')" >
<?PHP
}
elseif(empty($_GET['act'])==FALSE)
{
    $id=$_GET['id'];
    $dltid=$_GET['dltid'];
    $rs=delete_coin_setup($_GET['dltid']);
    if($rs != TRUE)
        {
                $mss="コインの削除に成功しました";
        }
        else
        {
              $mss="コインの削除に失敗しました";
        }
?>
<body onload="disp('<?PHP print($mss);?>')" >
<?PHP
}
else
{
?>
<body>
<?PHP
}
$rs=get_coin_group($id);
$ret=get_result($rs);
?>
        <form action="../sales/coin_edit.php" method="POST">
        <table class="form">
            <tr><nobr>
                <th>グループ名</th>
                <td><input type="text" value="<?PHP print($ret['cgn']); ?>" name="name" style="width:180px"/></td>
            </nobr></tr>
            <tr><nobr>
                <th>メモ</th>
                <td><textarea cols="20"  rows="3" name="memo" style="width:180px"> <?PHP print(trim($ret['memo']));?></textarea></td>
            </nobr></tr>
        </table>
        <input type="hidden" value="<?PHP print($id);?>" name="id" />
        <input type="hidden" value="GEDT" name="act" />
        <button type="submit" class="button2">グループ内容修正</button>
        </form>

        <form action="../sales/coin_edit.php" method="POST">
        <input type="hidden" value="CEDT" name="act" />
        <input type="hidden" value="<?PHP print($id);?>" name="id" />
        <table class="list">
          <tr>
            <th style="width:60px"><nobr>管理番号</nobr></th>
            <th><nobr>販売金額</nobr></th>
            <th><nobr>有料<?PHP print(POINT_NAME);?>発行枚数</nobr></th>
            <th><nobr>無料<?PHP print(POINT_NAME);?>発行枚数</nobr></th>
            <th><nobr>合計発行枚数</nobr></th>
            <th style="width:60px"><nobr>操作</nobr></th>
          </tr>
<?PHP
    $rs=get_coin_setup($id);
    $row=get_row_num($rs);
    for($i=0;$i<$row;$i++)
    {
         $ret=get_result($rs);
?>
        <tr>
            <TD class="center"><NOBR><?PHP print($ret['id']); ?><input type="hidden" value="<?PHP print($ret['id']); ?>" name="cid[]" style="text-align:right;ime-mode:disabled"/></NOBR></TD>
            <TD><NOBR><input type="text" value="<?PHP print($ret['im']); ?>" name="im[]" istyle='4' style="text-align:right;ime-mode:disabled"/>&nbsp;円</NOBR></TD>
            <TD><NOBR><input type="text" value="<?PHP print($ret['coin']); ?>" name="coin[]" istyle='4' style="text-align:right;ime-mode:disabled"/>&nbsp;枚</NOBR></TD>
            <TD><NOBR><input type="text" value="<?PHP print($ret['f_coin_free']); ?>" name="free_coin[]" istyle='4' style="text-align:right;ime-mode:disabled"/>&nbsp;枚</NOBR></TD>
            <TD class="right"><NOBR><?PHP print($ret['coin']+$ret['f_coin_free']); ?>枚</NOBR></TD>
            <TD class="center"><NOBR><a href="#" onclick="del('<?PHP print($id); ?>', '<?PHP print($ret['id']); ?>')">削除</a></NOBR></TD>
        </tr>
<?PHP
        
    }

?>
        <tr>
            <TD class="center"><NOBR>新規</NOBR></TD>
            <TD><NOBR><input type="text" name="new_im" istyle='4' style="text-align:right;ime-mode:disabled"/>&nbsp;円</NOBR></TD>
            <TD><NOBR><input type="text" name="new_coin" istyle='4' style="text-align:right;ime-mode:disabled"/>&nbsp;枚</NOBR></TD>
            <TD><NOBR><input type="text" name="new_f_coin" istyle='4' style="text-align:right;ime-mode:disabled"/>&nbsp;枚</NOBR></TD>
            <TD class="right"><NOBR>-</NOBR></TD>
        </tr>
        </table>
        <button type="submit" class="button2">追加・更新</button>
        </form>
    </body>
</html>
