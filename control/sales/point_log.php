<style TYPE="text/css">
tr,td ,th {font-size: 9pt;}
</style>
<?php
include '../control/exec_select.php';
include '../../mvc/utils/Utils.class.php';
$date =$_REQUEST['date'];
$page=$_REQUEST['page'];

$start=$date."000000";
$end = $date."235959";
$offset=50;
$limit =$page*$offset -$offset;

$rs=get_point_log($start,$end,$all_count,$limit,$offset);

$html ="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
$html .="<html>\n";
$html .="<head>\n";
$html .="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
$html .="<link href=\"../../control/css/layout.css\" type=\"text/css\" rel=\"stylesheet\">\n";
$html .="<title></title>\n";
$html .="</head>\n";
$html .="<body>\n";

$html .=Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/sales/point_log.php?date=$date", 5);

$html .="<table class='list'>\n";
$html .="<tr>\n";
$html .="<th rowspan='2' style='width:120px'><nobr>時間</nobr></th>\n";
$html .="<th rowspan='2' style='width:50px'><nobr>会員ID</nobr></th>\n";
$html .="<th rowspan='2' style='width:120px'><center>タイプ</center></th>\n";
$html .="<th colspan='3'><center>発行コイン</center></th>\n";
$html .="</tr>\n";

$html .="<tr>\n";
$html .="<th style='width:50px'><nobr>課金</nobr></th>\n";
$html .="<th style='width:50px'><nobr>フリー</nobr></th>\n";
$html .="<th style='width:50px'><nobr>合計</nobr></th>\n";
$html .="</tr>\n";

$row=get_row_num($rs);

for($i=0;$i<$row;$i++)
{
    $ret=get_result($rs);
    $html .="<tr>\n";
    $html .="<td><nobr>".$ret['f_tm_stamp']."</nobr></td>\n";
    $html .="<td align='center'><nobr>".$ret['f_member_id']."</nobr></td>\n";

    switch($ret['f_status'])
    {
        case 0:
            $str="コイン購入";
            $a=number_format($ret['f_coin_add']);
            $b=number_format($ret['f_free_coin_add']);
            $sum = number_format($ret['f_coin_add']+$ret['f_free_coin_add']);
            break;
        case 2:
            $str="フリーコイン発行";
            $a="-";
            $b=number_format($ret['f_coin_add']+$ret['f_free_coin_add']);
            $sum=$b;
            break;
        case 3:
            $str="コインパック購入";
            $a=number_format($ret['f_coin_add']);
            $b=number_format($ret['f_free_coin_add']);
            
            $sum = number_format($ret['f_coin_add']+$ret['f_free_coin_add']);
    }
    $html .="<td><nobr>$str</nobr></td>\n";
    $html .="<td align='right' style='text-align:right;padding-right:4px'><nobr>$a</nobr></td>\n";
    $html .="<td align='right' style='text-align:right;padding-right:4px'><nobr>$b</nobr></td>\n";
    $html .="<td align='right' style='text-align:right;padding-right:4px'><nobr>$sum</nobr></td>\n";
    $html .="</tr>\n";
}
$html .="</table>\n";
$html .=Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/sales/point_log.php?date=$date", 5);
$html .="<center><button type=\"button\" class=\"button2\" onclick=\"window.close()\">画面を閉じる</button></center>\n";
$html .="</body>\n";
$html .="</html>\n";

echo $html;
?>
