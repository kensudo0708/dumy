<style TYPE="text/css">
tr,td ,th {font-size: 9pt;}
</style>
<?php
session_start();
include '../control/exec_select.php';
include '../../mvc/utils/Utils.class.php';

    $tmp_start=$_REQUEST['start'];
    $start=$tmp_start."000000";
    $end=$tmp_start."235959";

    $page=$_REQUEST['page'];
    $offset=50;
    $limit =$page*$offset -$offset;
    $rs=get_sales_log($start, $end,$all_count,$limit,$offset);
    $html ="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
    $html .="<html>\n";
    $html .="<head>\n";
    $html .="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
    $html .="<link href=\"../../control/css/layout.css\" type=\"text/css\" rel=\"stylesheet\">\n";
    $html .="<title></title>\n";
    $html .="<style type=\"text/css\">\n";
    $html .="div.label {\n";
    $html .="    overflow: hidden;\n";
    $html .="    text-overflow: ellipsis;\n";
    $html .="    white-space: nowrap;\n";
    $html .="}\n";
    $html .="</style>\n";
    $html .="</head>\n";
    $html .="<body>\n";
    $html .="<P>本ページに表示されている入金履歴は全て売上(売上集計・広告効果測定)に計上されます(二重決済を含む)。<BR>二重決済を売上に計上しない場合システムパラメータのFLG_SECONDPAY_OKを0にしてください</P>";
    $html .=Utils::getPageNumbersII($page, ceil($all_count /$offset), SITE_URL."control/sales/sales_log.php?start=$tmp_start", 5);
    $html .="<table class='list'>\n";
    $html .="<tr>\n";
    $html .="<th style='width:40px'><nobr>時間</nobr></th>\n";
    $html .="<th style='width:60px'><nobr>会員ID</nobr></th>\n";
    $html .="<th style='width:60px'><nobr>紹介者</nobr></th>\n";
    $html .="<th style='width:80px'><nobr><center>入会日</center></nobr></th>\n";
    $html .="<th style='width:80px'><center>入金タイプ</center></th>\n";
    $html .="<th style='width:60px'><center>商品ID</center></th>\n";
    $html .="<th style='width:80px'><nobr>入金種別</nobr></th>\n";
    $html .="<th style='width:80px'><nobr>認証先</nobr></th>\n";
    $html .="<th style='width:60px'><nobr>金額</nobr></th>\n";
    $html .="<th style='width:60px'><nobr>備考</nobr></th>\n";
    $html .="</tr>\n";

    $row=mysql_num_rows($rs);

    for($i=0;$i<$row;$i++)
    {
        $ret=get_result($rs);
        $html .="<tr style='height:20px'>\n";
        $html .="<td><nobr>".$ret['dat']."</nobr></td>\n";
        if($ret['m_id'] =="" || $ret['m_id'] ==0)
        {
            $html .="<td><nobr>--(--)</nobr></td>\n";
            $html .="<td><nobr>---</nobr></td>\n";
            $html .="<td><nobr>---</nobr></td>\n";
        }
        else
        {
            $html .="<td><nobr>".$ret['m_id']."(".$ret['handle'].")</nobr></td>\n";
            if($ret['fk_adcode_id'] == 0)
            {
                $result2 = GetPerAdId($ret['m_id']);
                $ret3=get_result($result2);
                $ret2=GetPerName($ret3['fk_parent_ad'],&$p_name,&$p_mem_id);

               if($ret2==-1)
               {
                   $html .="<td><nobr>NOP</nobr></td>\n";
               }
               else if($ret2 == 1)
               {
                   $html .="<td><nobr>$p_name</nobr></td>\n";
               }
               else if($ret2 == 2)
               {
                   $html .="<td><nobr>$p_name($p_mem_id)</nobr></td>\n";
               }
               // $html .="<td><nobr>--</nobr></td>\n";
            }
            else
            {
               $ret2=GetPerName($ret['fk_adcode_id'],&$p_name,&$p_mem_id);

               if($ret2==-1)
               {
                   $html .="<td><nobr>NOP</nobr></td>\n";
               }
               else if($ret2 == 1)
               {
                   $html .="<td><nobr>$p_name</nobr></td>\n";
               }
               else if($ret2 == 2)
               {
                   $html .="<td><nobr>$p_name($p_mem_id)</nobr></td>\n";
               }
            }
            $html .="<td><nobr>".$ret['reg_d']."</nobr></td>\n";
        }
        //$html .="<td><nobr>".$ret['reg_d']."</nobr></td>\n";

        switch($ret['stat'])
        {
            case 0:
                $str="コイン購入";
                $a=$ret['f_shiharai_name'];
                $b=$ret['f_name'];
                $c=number_format($ret['pay']);
                $d="-";
                break;
            case 1:
                //$cate_ret=getItemCategory_sales($ret['fk_products_id']);
                //$cate_ret2=mysql_fetch_array($cate_ret);

                if($ret['f_coin_pack_flag'] == 1)
                {
                    $str="商品購入(コインパック)";
                }
                else
                {
                    $str="商品購入";
                }
                $a=$ret['f_shiharai_name'];
                $b=$ret['f_name'];
                $c=number_format($ret['pay']);
                $d=$ret['fk_products_id'];
                break;
            case 2:
                $str="フリーコイン加算";
                $a="-";
                $b="運営者";
                $c="-";
                $d="-";
        }
        $html .="<td><nobr>$str</nobr></td>\n";
        $html .="<td><nobr>$d</nobr></td>\n";
        $html .="<td><nobr>$a</nobr></td>\n";
        $html .="<td><nobr>$b</nobr></td>\n";
        $html .="<td style='text-align:right;padding-right:4px'><nobr>$c</nobr></td>\n";
        if($ret['f_double'] == 0)
        {
            $html .="<td><nobr>---</nobr></td>\n";
        }
        else
        {
            $html .="<td><nobr>二重決済</nobr></td>\n";
        }
        $html .="</tr>\n";
    }
    
$html .="</table>\n";
$html .=Utils::getPageNumbersII($page, ceil($all_count /$offset), SITE_URL."control/sales/sales_log.php?start=$tmp_start", 5);
$html .="<form>\n";
$html .="<center><button class='button2' type=\"button\" onclick=\"window.close()\">画面を閉じる</button></center>\n";
$html .="</form>\n";
$html .="</body>\n";
$html .="</html>\n";

print($html);
?>
