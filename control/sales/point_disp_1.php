<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
        <style TYPE="text/css">
tr,td ,th {font-size: 9pt;}
</style>
<script type="text/javascript">
<!--
function day(start){
            window.open("../sales/point_disp.php?type=1&start="+start+"&end="+(parseInt(start)+1), "window_name", "width=1015,height=900,scrollbars=yes,resizable = no,fullscreen = yes");
           // 設定終了
}
function hour(start){
            window.open("../sales/point_disp.php?type=0&start="+start+"000000&end="+start+"235959", "window_name", "width=1015,height=900,scrollbars=yes,resizable = no,fullscreen = yes");
           // 設定終了
}
// -->
</script>
<?php
session_start();
include '../control/exec_select.php';

if($_SESSION['staff_id']==NULL )
{
?>
<body>
    <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>
</body>
</html>
<?PHP
    return ;
}
   
include '../control/check_date.php';
$start=check_date($_GET["start"]);
$end=check_date($_GET["end"]);
$type=0;
if(empty($_GET["type"])==FALSE)
{
    $type=$_GET["type"];
}
if($start > $end)
{
    $start = $end;
}
if($start == $end)
{
    if(strlen($end)==8)
    {
        $end=computeday($end);
    }
    $end=computemonth($end);
}
if(strlen($end)==6)
{
    $start.="01";
    $end.="02";
}
else if(strlen($end)==8)
{
    $start.="000000";
    $end.="000000";
}
?>
  <body>
      <table border="1">
          <tr bgcolor='#CCCCFF'>
              <td rowspan="2"><nobr>日付</nobr></td>
<?php
            if($type==1)
            {
                printf("<td colspan=4><nobr><center>発行枚数</center></nobr></td>\n");
            }
            else
            {
                printf("<td colspan=3><nobr><center>発行枚数</center></nobr></td>\n");
            }
?>
              <td colspan=4><nobr><center>消費枚数</center></nobr></td>
      　　　　 <td colspan=3><nobr><center>入札タイプ</center></nobr></td>
<?PHP
        $sql=sprintf("SELECT COUNT(*) count FROM auction.t_auction_type");
        $count =get_count($sql);
?>
      　　　　 <td colspan=<?PHP print($count); ?>><nobr><center>オークションタイプ</center></nobr></td>
          </tr>
          <tr bgcolor='#CCCCFF'>
<?PHP
            if($type==1)
            {
                printf("<td>履歴</td>\n");
            }
?>
              <td>合計</td>
              <td>課金<?PHP print(POINT_NAME);?></td>
              <td>フリー<?PHP print(POINT_NAME);?></td>
              <td>合計</td>
              <td>課金<?PHP print(POINT_NAME);?></td>
              <td>フリー<?PHP print(POINT_NAME);?>(会員)</td>
              <td>フリー<?PHP print(POINT_NAME);?>(支援)</td>
              <td>手動1</td>
              <td>手動2</td>
              <td>自動</td>        
<?PHP
    $rs=get_auction_type();
    for($i=0;$i<$count;$i++)
    {
        $ret=get_result($rs);
?>
              <td><?PHP print($ret["f_auction_name"]);?></td>
<?PHP
    }
?>
          </tr>
<?PHP

    $rs=get_sal_point($type, $start, $end);
    $row=get_row_num($rs);
    for($i=0;$i<$row;$i++)
    {
        $ret=get_result($rs);
?>
          <tr>
<?PHP
          if($type==0)
          {
?>
            <td><nobr><?PHP print($ret['dat']);?>～</nobr></td>
<?PHP
          }
          else if($type==1)
          {
?>
          <td><a href="" onclick="hour('<?PHP print(substr($ret['dat2'],0,8));?>')"><nobr><?PHP print(substr($ret['dat'],0,10)); ?></a></nobr></td>
          
          <!--
          <td><a href="" onclick="day('<?PHP print(substr($ret['dat2'],0,8));?>')"><nobr><?PHP print(substr($ret['dat'],0,10)); ?></a></nobr></td>
          -->
<?PHP
            printf("<td align='right'><nobr><a href='point_log.php?page=1&date=".substr($ret['dat2'],0,8)."' target='_blank'>履歴</a></nobr></td>");
          }
          elseif($type==2)
          {
?>
          <td><a href="" onclick="day('<?PHP print(substr($ret['dat2'],0,6));?>')"><nobr><?PHP print(substr($ret['dat'],0,7)); ?></a></nobr></td>
<?PHP
          }
?>
          <td align="right"><nobr><?PHP print(number_format($ret['total_gen'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_coin_gen'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_scoin_gen'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['total_use'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_coin_use'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_scoin_use']-$ret['f_bid_hand2'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_hand2'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_hand'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_hand2'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_auto'])); ?></nobr></td>
<?PHP
          for($j=1;$j<=$count;$j++)
          {
              $tmp=sprintf("totalbid%d",$j);
?>
              <td align="right"><nobr><?PHP print(number_format($ret[$tmp])); ?></nobr></td>
<?PHP
           }
?>
      </tr>
<?PHP
    }
    $rs=get_sal_point_goukei($type, $start, $end);
    $row=get_row_num($rs);
    for($i=0;$i<$row;$i++)
    {
        $ret=get_result($rs);
?>
          <Tr bgcolor='#FFFFE0'>
          <td align="right"><nobr>合計</nobr></td>
          <!-- <td align="center"><nobr>-</nobr></td> -->
          <td align="right"><nobr><?PHP print(number_format($ret['total_gen'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_coin_gen'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_scoin_gen'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['total_use'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_coin_use'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_scoin_use']-$ret['f_bid_hand2'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_hand2'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_hand'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_hand2'])); ?></nobr></td>
          <td align="right"><nobr><?PHP print(number_format($ret['f_bid_auto'])); ?></nobr></td>
<?PHP
          for($j=1;$j<=$count;$j++)
          {
              $tmp=sprintf("totalbid%d",$j);
              
?>
              <td align="right"><nobr><?PHP print(number_format($ret[$tmp])); ?></nobr></td>
<?PHP
           }
    }
?>
      </Tr>
      </table>
  </body>
</html>
