<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../control/css/layout.css" type="text/css" rel="stylesheet">
        <title></title>
    <style TYPE="text/css">
    div.label {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    </style>
    </head>
<?php
session_start();

$html ="";
if($_SESSION['staff_id']==NULL )
{
    $html .=    "<body>\n
                <P>セッション切れです。</BR>不具合が発生することが考えられますので更新してください</P>\n
                </body>\n
                </html>\n";
    echo $html;
    return ;
}

include '../control/exec_select.php';
include '../control/check_date.php';
$type=0;
$page = 1;

if(empty($_GET["page"]) === FALSE)
{
    $page = $_GET["page"];
}

if(empty($_GET["type"])==FALSE)
{
    $type=$_GET["type"];
}
$start=check_date($_GET["start"]);
$end ="";
if($type == 2)
{
    $end=check_date($_GET["end"]);
}

$spend_coin=array();
$no=array();
for($i=0;$i<20;$i++)
{
    $spend_coin[$i]=0;
    $no[$i]=0;
}

$rs=get_auction_type();
$auction_num=mysql_num_rows($rs);
$tmp=$auction_num;

$html .="<body>\n";
$table_header ="<table class='list'>\n";
$table_header .="<tr>\n";
$table_header .="<th rowspan='3' style='width:80px'><nobr>日付</nobr></th>\n";

$disp_num=1;
$auction_start = $page * $disp_num -$disp_num;
$auction_end = $page * $disp_num;

$tmp=$auction_num;
$tmp_page=0;

$link ="<table><tr>";
for($i =0;$i < $auction_num ;$i++)
{
    $ret=mysql_fetch_array($rs);
    $spend_coin[$i]=$ret['f_spend_coin'];
    $no[$i] = $ret['f_statistics_no'];

    $tmp_page=$i+1;
    if($type ==2)
    {
        $link .="<td align='center'><font size ='1'><a href ='../sales/auction_disp.php?type=$type&start=$start&end=$end&page=$tmp_page'>".$ret['f_auction_name']."</a></font></td>";
    }
    else
    {
        $link .="<td align='center'><font size ='1'><a href ='../sales/auction_disp.php?type=$type&start=$start&page=$tmp_page'>".$ret['f_auction_name']."</a></font></td>";
    }
    if($i <$auction_end && $i>= $auction_start)
    {
        $table_header .="<th colspan='10'><nobr>".$ret['f_auction_name']."</nobr></th>\n";
    }
}
$link .="</tr></table>";
$table_header .="</tr>\n";

$table_header .="<tr>\n";
for($i =0;$i<$disp_num ;$i++)
{
    $table_header .="<th colspan='4'><nobr>手動入札回数</nobr></th>\n";
    $table_header .="<th colspan='4'><nobr>自動入札回数</nobr></th>\n";
    $table_header .="<th colspan='2'><nobr>コイン消費枚数</nobr></th>\n";

}
$table_header .="</tr>\n";

$table_header .="<tr>\n";
for($i =0;$i<$disp_num ;$i++)
{
    $table_header .="<th style='width:50px'><nobr>課金</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>フリー</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>支援</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>合計</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>課金</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>フリー</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>支援</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>合計</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>課金</nobr></th>\n";
    $table_header .="<th style='width:50px'><nobr>フリー</nobr></th>\n";
}
$table_header .="</tr>\n";

$rs = GetPointDateTotal($type, $start, $end);
$num=mysql_num_rows($rs);
$table_body ="";

for($i=0;$i<$num;$i++)
{
    $ret = mysql_fetch_array($rs);
    $table_body .="<tr>\n";
    switch($type)
    {
        case 0:
            $table_body .="<td align='center'><nobr>".$ret['data5']."</nobr></td>\n";
            break;
        case 1:
            $table_body .="<td align='center'><nobr>".$ret['data3']."</nobr></td>\n";
            break;
        case 2:
            $table_body .="<td align='center'><nobr>".$ret['data1']."</nobr></td>\n";
            break;
    }

    for($j =$auction_start;$j<$auction_end && $j < $auction_num ;$j++)
    {
        $auctype = "f_auctype".$no[$j];
        $sauctype= "f_sauctype".$no[$j];
        $aucauto ="f_aucauto".$no[$j];
        $aucautof ="f_aucautof".$no[$j];
        $rhand ="f_rhand".$no[$j];
        $rauto ="f_rauto".$no[$j];

        $f_auctype = $ret[$auctype];
        $f_sauctype= $ret[$sauctype];
        $f_aucauto =$ret[$aucauto];
        $f_aucautof =$ret[$aucautof];
        $f_rhand =$ret[$rhand];
        $f_rauto =$ret[$rauto];

        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_auctype)."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_sauctype)."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_rhand)."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_auctype+$f_sauctype)."</nobr></td>\n";

        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_aucauto)."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_aucautof)."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_rauto)."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format($f_aucauto+$f_aucautof)."</nobr></td>\n";

        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format(($f_auctype+$f_aucauto)*$spend_coin[$j])."</nobr></td>\n";
        $table_body .="<td style='text-align:right;padding-right:4px'><nobr>".number_format(($f_sauctype+$f_aucautof)*$spend_coin[$j])."</nobr></td>\n";
    }
    $table_body .="</tr>\n";
}
$html = $html.$link.$table_header .$table_body."</table></body>\n";
echo $html;
?>

