<html lang="utf-8">
<head>
<title>管理画面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../control/css/layout.css">
</head>
<body class="logout" scroll="no">
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	$dsp_tbl  = "";
	$dsp_tbl .= "<div align='center'>\n";
	$dsp_tbl .= "<A href='logout_jump.php' target='_top'>ログアウト</A>\n";
	$dsp_tbl .= "</div>\n";

	print $dsp_tbl;

?>
</body>
</html>