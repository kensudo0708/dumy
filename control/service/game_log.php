<?php
/******************************
 *
 * ■ゲーム履歴一覧画面
 *
 *
 */

/**
 * ■共通ライブラリ読込
 */
include "../../lib/define.php";
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;
require_once( realpath(COMMON_LIB.'game_lib.php') );

// ページャーの生成ファンクションが定義されている
$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
include $utils_class;

$game_suffix = 'A';

/**
 * ■権限チェック
 */
session_start();
if ( isset( $_SESSION['staff_id'] ) ) {
    if ( !CheckStaffTAuthority( $_SESSION['staff_id'], '31') ) {
        PrintAdminPage( 'ゲーム設定変更', '操作権限がありません。' );
        exit;
    }
} else {
    PrintAdminPage( 'ゲーム設定変更', 'セッションが切れました。' );
    exit;
}

/**
 * ■システムパラメータ設定チェック
 */
if ( !defined( 'GAME_ID_'.$game_suffix ) ) {
    PrintAdminPage( 'ゲーム設定変更', '"GAME_ID_'.$game_suffix.'" is undefined.' );
    exit;
}

/**
 * ■変数初期化
 */
$const_game_id = constant( 'GAME_ID_'.$game_suffix );
$const_item_count = 4;
if ( defined( 'GAME_ITEMS_'.$game_suffix ) ) {
    $const_item_count = constant( 'GAME_ITEMS_'.$game_suffix );
}
if ( $const_item_count > GameModel::PRIZE_ITEMS_LIMIT ) {
    $const_item_count = GameModel::PRIZE_ITEMS_LIMIT;
}
$data = array();
$total_count = 0;
$last_page_no = 1;
$limit = 50;
$offset = 0;
$popup_message = array();

/**
 * ■リクエストパラメータ取得
 */
$page_no = 1;
$search_type = 0;
$member_id = NULL;
$do_refund  = 0;
$game_log_id  = NULL;
if ( isset( $_REQUEST['search_type'] ) ) {
    $search_type = intval( $_REQUEST['search_type'] );
}
if ( isset( $_REQUEST['page'] ) ) {
    $page_no = intval( $_REQUEST['page'] );
}
if ( $page_no < 1 ) {
    $page_no = 1;
}
if ( isset( $_REQUEST['member_id'] ) ) {
    $member_id = intval( $_REQUEST['member_id'] );
}
if ( isset( $_POST['do_refund'] ) ) {
    $do_refund = intval( $_POST['do_refund'] );
}
if ( isset( $_POST['game_log_id'] ) ) {
    $game_log_id = intval( $_POST['game_log_id'] );
}

/**
 * ■主処理
 */
$db = db_connect();
if ( !$db ) {
   PrintAdminPage( 'ゲームプレイ履歴一覧', 'データベース接続エラー<br>'.mysql_error() );
   exit;
}

$options = array(
    'const.prize_items'=>$const_item_count,
);
$model = new GameModel( $db, $const_game_id, $options );

/**
 * ■手動払戻
 */
if ( $do_refund ) {
    if ( mysql_query( 'START TRANSACTION' ) ) {
        if ( $model->doRefund( $db, $game_log_id, $_SESSION['staff_id'] ) ) {
            mysql_query( 'COMMIT' );
            $popup_message = '手動払戻処理：○成功';
        } else {
            mysql_query( 'ROLLBACK' );
            $popup_message = '手動払戻処理：×失敗...';
        }
    }
}
/**
 * ■データ取得処理
 */
$where = '';
switch ( $search_type ) {
// 未プレイのみ
case 1:
    $where .= " AND f_play_status=0";
    break;
// プレイ済のみ
case 2:
    $where .= " AND f_play_status=1";
    break;
// プレイ済＋未払戻
case 3:
    $where .= " AND f_play_status=1 AND f_refund_status=0";
    break;
}
if ( $member_id ) {
    $where .= " AND f_member_id=".$member_id;
}

$sql = "SELECT COUNT(f_game_log_id) cnt FROM auction.t_game_log WHERE f_game_id=".$const_game_id;
$sql = $sql.$where;
$res = mysql_query( $sql, $db );
if ( $res !== FALSE ) {
    $data = mysql_fetch_assoc( $res );
    $total_count = $data['cnt'];
    $last_page_no = ceil( $total_count / $limit );
    if ( $last_page_no < 1 ) {
        $last_page_no = 1;
    }
    if ( $page_no > $last_page_no ) {
        $page_no = $last_page_no;
    }
    $offset = $limit * ( $page_no - 1 );
    mysql_free_result( $res );
} else {
   PrintAdminPage( 'ゲーム履歴一覧', 'データ件数取得エラー<br>'.$sql.'<br>'.mysql_error() );
   exit;
}
$data = array();
$sql = "
SELECT
  f_game_log_id
  ,f_member_id
  ,f_game_id
  ,f_play_status
  ,f_play_type
  ,f_result_no
  ,f_value
  ,f_name
  ,f_message
  ,f_item_a
  ,f_item_b
  ,f_item_c
  ,f_refund_status
  ,f_refund_type
  ,f_refund_request
  ,f_refund_time
  ,f_pay_log_id
  ,f_expire_time
  ,f_create_time
  ,f_update_time
FROM
  auction.t_game_log
WHERE
  f_game_id=".$const_game_id.$where."
ORDER BY
  f_game_log_id DESC
LIMIT ".$offset.", ".$limit;
$res = mysql_query( $sql, $db );
if ( $res !== FALSE ) {
    $tmp = NULL;
    while ( $tmp = mysql_fetch_assoc( $res ) ) {
        $data[] = $tmp;
    }
    mysql_free_result( $res );
} else {
   PrintAdminPage( 'ゲーム履歴一覧', 'データ取得エラー<br>'.$sql.'<br>'.mysql_error() );
   exit;
}

db_close($db);

/**
 * ■ページャーHTMLの生成
 */
$pager = '';
if ( $last_page_no > 1 ) {
    $pager .= '
<div class="pager">
';
    $pager .= Utils::getPageNumbersII( $page_no, $last_page_no, 'game_log.php?search_type='.$search_type, ($last_page_no < 5 ? $last_page_no:5) );
    $pager .= '
  <span class="mypagenum_box4">
    <form action="game_log.php" method="get" style="display: inline">
      <input type="hidden" name="search_type" value="'.$search_type.'"/>
      <input type="text" name="page" style="width:30px;ime-mode:disabled"/>
      <input type="submit" value="Jump"/>
    </form>
  </span>
</div>
';
}

/**
 * ■一覧HTML生成
 */
$html = '
<style type="text/css">
div.pager {
    /* text-align: center; 大抵の表で右にはみ出す */
    color: #666;
    vertical-align: middle;
    height: 32px;
    margin: 1px;
}
div.pager form {
    display: inline;
}
</style>
<script type="text/javascript">
function doRefund( game_log_id, play_status, refund_status, coin, free_coin ) {
    var msg = \'手動払戻処理を実行します。\' + "\n";
    msg += \'（この処理は取り消すことができません！）\' + "\n\n";
    msg += \'履歴ＩＤ：\'+ game_log_id + "\n";
    msg += \'ﾌﾟﾚｲ状態：\'+ (play_status ? \'済\':\'未\') + \' -> 済\' + "\n";
    msg += \'払戻状態：\'+ (refund_status ? \'済\':\'未\') + \' -> 済\' + "\n";
    msg += \'払戻ｺｲﾝ ：\'+ coin + \' / \' + free_coin + "\n";
    if ( confirm( msg ) ) {
        document.forms[\'refund\'].elements[\'game_log_id\'].value = game_log_id;
        document.forms[\'refund\'].submit();
    }
}
';
if ( $do_refund && $popup_message ) {
    $html .= '
alert(\''.$popup_message.'\');
';
}
$html .= '
</script>
<form name="refund" action="game_log.php" method="POST" style="display:none">
<input type="hidden" name="page" value="'.$page_no.'">
<input type="hidden" name="search_type" value="'.$search_type.'">
<input type="hidden" name="do_refund" value="1">
<input type="hidden" name="game_log_id" value="">
</form>
<div class="center">
'.$pager. '
</div>
<div class="center" style="width:100%">
<table class="data">
  <tr><th style="width:100px">データ件数</th><td class="center" style="width:100px">'.$total_count.'</td></tr>
</table>
</div>
<div style="color:#666;padding:2px">
'.($total_count ? ($offset + 1):0).'&nbsp件目&nbsp～&nbsp;'.($total_count - $offset).'&nbsp件目
</div>
<table class="list">
  <tr>
    <th title="履歴ID" rowspan="2" style="width:80px">履歴ID</th>
    <th title="会員ID" rowspan="2" style="width:60px">会員ID</th>
    <th title="ﾌﾟﾚｲ" colspan="2">プレイ</th>
    <th title="結果" colspan="2">結果</th>
    <th title="配当ｺｲﾝ(購入)" rowspan="2">配当コイン<br>（購入）</th>
    <th title="配当ｺｲﾝ(ﾌﾘｰ)" rowspan="2">配当コイン<br>（フリー）</th>
    <th title="払戻" colspan="6">払戻</th>
    <th title="作成日時" rowspan="2" style="width:120px">作成日時</th>
    <th title="更新日時" rowspan="2" style="width:120px">更新日時</th>
  </tr>
  <tr>
    <th title="未=未ﾌﾟﾚｲ、1=ﾌﾟﾚｲ済">状態</th>
    <th title="権利の種別(1=毎日, 2=ｺｲﾝ購入, 3=ｺｲﾝﾊﾟｯｸ購入)">種別</th>
    <th title="0=ﾊｽﾞﾚ, 0≠ｱﾀﾘ">No</th>
    <th title="結果名称">名称</th>
    <th title="未=未払戻、済=払戻済">状態</th>
    <th title="0=ﾘｱﾙﾀｲﾑ、1=手動払戻">種別</th>
    <th title="対応する支払履歴ID">支払ID</th>
    <th title="処理日時" style="width:120px">処理日時</th>
    <th title="払戻要求回数 = ｹﾞｰﾑが再生された回数">要求</th>
    <th title="手動払戻">手動</th>
  </tr>
';
foreach ( $data as $row ) {
    $html .= '
  <tr style="height:25px">
    <td class="center">'.$row['f_game_log_id'].'</td>
    <td class="center">'.$row['f_member_id'].'</td>
    <td class="center">'.($row['f_play_status'] == 0 ? '未':'済').'</td>
    <td class="center">'.$row['f_play_type'].'</td>
    <td class="center">'.$row['f_result_no'].'</td>
    <td><div title="'.$row['f_name'].'" style="width:60px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis">'.$row['f_name'].'</div></td>
    <td class="right">'.$row['f_item_a'].'</td>
    <td class="right">'.$row['f_item_b'].'</td>
    <td class="center"'.(($row['f_play_status'] == 1 && $row['f_refund_status'] == 0) ? ' style="background:#FFBFCF"':'').'>'.($row['f_refund_status'] == 0 ? '未':'済').'</td>
    <td class="center">'.$row['f_refund_type'].'</td>
    <td class="center">'.$row['f_pay_log_id'].'</td>
    <td class="center">'.$row['f_refund_time'].'</td>
    <td class="center">'.$row['f_refund_request'].'</td>
    <td class="center">'.(
    ($row['f_play_status'] == 0 || $row['f_refund_status'] == 0)
      ?
        '<button type="button" onClick="doRefund('.$row['f_game_log_id'].','.$row['f_play_status'].','.$row['f_refund_status'].','.$row['f_item_a'].','.$row['f_item_b'].' )" style="width:20px">＄</button>'
      :
        ''
    ).'</td>
    <td class="center">'.$row['f_create_time'].'</td>
    <td class="center">'.$row['f_update_time'].'</td>
  </tr>
';
}
$html .= '
</table>
<div class="center">
'.$pager.'
</div>
<ul>
  <li>項目値の意味については見出しをマウスオーバーすることで参照することができます。</li>
  <li>【払戻/状態】が<span style="background:#FFBFCF">&nbsp;赤い&nbsp;</span>
    <ul>
        <li>ゲームがプレイ済であるにも関わらず、払戻処理が行われていないことを表します。</li>
        <li>システム上のタイムラグである可能性もありますが、長時間この状態にあるデータが存在する場合は【手動払戻（＄）】ボタンの使用を検討してください。</li>
    </ul>
  </li>
  <li>【払戻/要求】が1回ではないデータがある</span>
    <ul>
        <li>通常この項目は0 or 1ですがプレイヤーがブラウザの戻るボタンを利用したり、あるいはプレイ前状態にある同一ラウンドのゲームを<br>
複数のタブやウインドウで同時に立ち上げた場合には、複数回のプレイ完了信号(払戻要求信号)が受信される場合があります。<br>
こういった場合でも払戻処理は1度しか行われず、信号が受信されたことのみをカウントアップします。</li>
    </ul>
  <li>【手動払戻（＄）】ボタンについて
    <ul>
      <li>ゲーム配当の払戻処理はゲーム終了のタイミングでリアルタイムに行われますので定常運用においてこのボタンを使用することはありません。</li>
      <li>ただし、何からのシステム障害でこの処理が行われなかった場合のリカバリのために用意されています。</li>
    </ul>
　</li>
  </li>
</ul>
';
PrintAdminPage( "ゲーム履歴一覧", $html );
?>
