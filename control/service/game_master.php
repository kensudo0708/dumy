<?php
/******************************
 *
 * ■ゲームマスタ設定の編集画面
 *
 *
 */

/**
 * ■共通ライブラリ読込
 */
include "../../lib/define.php";
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;
require_once( realpath(COMMON_LIB.'game_lib.php') );

$game_suffix = 'A';

/**
 * ■権限チェック
 */
session_start();
if ( isset( $_SESSION['staff_id'] ) ) {
    if ( !CheckStaffTAuthority( $_SESSION['staff_id'], '31') ) {
        PrintAdminPage( 'ゲーム設定変更', '操作権限がありません。' );
        exit;
    }
} else {
    PrintAdminPage( 'ゲーム設定変更', 'セッションが切れました。' );
    exit;
}

/**
 * ■システムパラメータ設定チェック
 */
if ( !defined( 'GAME_ID_'.$game_suffix ) ) {
    PrintAdminPage( 'ゲーム設定変更', 'システムパラメータ("GAME_ID_'.$game_suffix.'")が定義されていません。' );
    exit;
}

/**
 * ■変数初期化
 */
$const_game_id = constant( 'GAME_ID_'.$game_suffix );
$const_item_count = 4;
if ( defined( 'GAME_ITEMS_'.$game_suffix ) ) {
    $const_item_count = constant( 'GAME_ITEMS_'.$game_suffix );
}
if ( $const_item_count > GameModel::PRIZE_ITEMS_LIMIT ) {
    $const_item_count = GameModel::PRIZE_ITEMS_LIMIT;
}
$use_play_count_index = array(
    1=>'毎日',
    2=>'コイン購入',
    3=>'コインパック購入',
//    4=>'友達紹介',
);

$t_game = array();
$t_game_bonus = array();

/**
 * ■主処理
 */
$db = db_connect();
if ( !$db ) {
   PrintAdminPage( 'ゲーム設定変更', 'データベース接続エラー' );
   exit;
}

$options = array(
    'const.prize_items'=>$const_item_count,
);
$model = new GameModel( $db, $const_game_id, $options );

/**
 * ■データ更新処理
 */
if ( isset( $_POST['do_update'] ) && $_POST['do_update'] ) {
    /**
     * ■t_gameの更新 
     */
    if ( isset( $_POST['t_game'] ) ) {
        $t_game = $model->validateGame( $_POST['t_game'] );
        $t_game['f_game_id'] = $const_game_id;
        if ( !$model->updateGame( $db, $t_game, $_SESSION['staff_id'] ) ) {
           PrintAdminPage( 'ゲーム設定変更', 'ゲームマスタ更新エラー<br>' );
           exit;
        }
    }

    /**
     * ■t_game_bonusの更新
     */
    $tmp = "UPDATE auction.t_game_bonus SET ";
    if ( isset( $_POST['t_game_bonus'] ) ) {
        $t_game_bonus = $model->validateGameBonus( $_POST['t_game_bonus'] );
        foreach ( $t_game_bonus as $f_bonus_id=>$row ) {
            $row['f_game_id'] = $const_game_id;
            $row['f_bonus_id'] = $f_bonus_id;
            if ( !$model->updateGameBonus( $db, $row, $_SESSION['staff_id'] ) ) {
                PrintAdminPage( 'ゲーム設定変更', 'ゲームボーナスマスタ更新エラー' );
                exit;
            }
        }
    }
}

/**
 * ■データ取得処理
 */
$t_game = array();
$t_game_bonus = array();

// t_game
$t_game = $model->getGameRow( $db, $const_game_id );
if ( !$t_game ) {
    PrintAdminPage( 'ゲーム設定変更', 'ゲームマスタ取得エラー' );
    exit;
}

// t_game_bonus
$t_game_bonus = $model->getGameBonusList( $db, $const_game_id );
if ( !$t_game_bonus ) {
   PrintAdminPage( 'ゲーム設定変更', 'ゲームボーナスマスタ取得エラー' );
   exit;
}

db_close($db);

/**
 * ■HTML生成
 */

$html = '
<script type="text/javascript">
function updatePrepareCheck() {
    // nop
    return true;
}
</script>
<form action="game_master.php" method="POST">
<fieldset>
<legend>■基本設定</legend>

<table class="list">
<tr>
  <th>開催状態</th>
  <th>休止中に表示するメッセージ</th>
</tr>
<tr>
  <td class="center'.($t_game['f_status'] == 1 ? ' alert':'').'" style="width:366px">
    <select name="t_game[f_status]" style="width:100px">
      <option value="0"'.($t_game['f_status'] == 0 ? ' selected':'').'>○開催中</option>
      <option value="1"'.($t_game['f_status'] == 1 ? ' selected':'').'>×休止中</option>
    </select>
  </td>
  <td>
    <input type="text" name="t_game[f_info1]" value="'.$t_game['f_info1'].'" maxlength="50" style="width:400px;ime-mode:active">
  </td>
</tr>
</table>
<ul>
  <li>開催状態を『×休止中』に切り替えた場合、会員がプレイ権利を所有している状況であっても即時プレイできない状態になります。</li>
  <li>また、コイン購入時などに行われるプレイ権利の付与、ボーナス効果の付与についても停止されます。</li>
  <li>ただし、すでにユーザのブラウザに読み込まれた状態で待機している状態のゲームについてはそのままプレイされます。</li>
</ul>
<div style="height:10px"></div>
<table class="list">
<tr>
  <th colspan="3">付与されるプレイ権利（回数）</th>
  <th rowspan="2">権利がない場合に表示するメッセージ</th>
</tr>
<tr>
';
foreach ( $use_play_count_index as $i=>$caption ) {
    $html .= '
  <th>'.$caption.'</th>
';
}
$html .= '
</tr>
<tr>
';
foreach ( $use_play_count_index as $i=>$caption ) {
    $html .= '
  <td class="center" style="width:120px">
        <input class="right" type="text" name="t_game[f_play_count'.$i.']" value="'.$t_game[('f_play_count'.$i)].'" maxlength="2" style="width:30px;ime-mode:disabled">&nbsp;回
  </td>
';
}
$html .= '
  <td>
    <input type="text" name="t_game[f_info2]" value="'.$t_game['f_info2'].'" maxlength="50" style="width:400px;ime-mode:active">
  </td>
</tr>
</table>
<ul>
  <li>『毎日』に設定された権利は日付を跨いで持ち越すことはできませんが、それ以外のケースで発生した権利は永続的に所有されます。</li>
  <!--li>プレイ権利は『毎日』の権利が他の権利より優先して消費されます。←ボーナス効果取得～当選ゲーム獲得という流れを優先するため廃止</li-->
</ul>
</fieldset>
';

/**
 * ■懸賞設定
 */
$html .= '
<fieldset>
<legend>■懸賞設定</legend>
<table class="list">
<tr>
  <th rowspan="2" style="width:20px"></th>
  <th rowspan="2" style="width:80px">懸賞</th>
  <th rowspan="2" style="width:100px">有効/無効</th>
  <th colspan="2">配当コイン（枚数）</th>
  <th rowspan="2" style="width:120px">[基準]当選確率（％）</th>
  <th rowspan="2" style="width:100px">当選本数上限<br>（１日あたり）</th>
  <th rowspan="2" style="width:300px">当選した場合に表示するメッセージ</th>
</tr>
<tr>
  <th style="width:80px">購入</th>
  <th style="width:80px">サービス</th>
</tr>
';
for ( $i = 1; $i <= $const_item_count; $i++ ) {
    $html .= '<tr>
  <th class="center">'.$i.'</th>
  <td class="center">
    <input type="text" name="t_game[f_name'.$i.']" value="'.$t_game[('f_name'.$i)].'" maxlength="10" style="width:70px;ime-mode:enable"></input>
  </td>
  <td class="center">
     <select name="t_game[f_status'.$i.']" style="width:80px">
       <option value="0"'.($t_game[('f_status'.$i)] == 0 ? ' selected':'').'>○有効</option>
       <option value="1"'.($t_game[('f_status'.$i)] == 1 ? ' selected':'').'>×無効</option>
     </select>
  </td>
  <td class="center">
    <input class="right'.($t_game[('f_item_a'.$i)] < 0 ? ' alert':'').'" type="text" name="t_game[f_item_a'.$i.']" value="'.($t_game[('f_item_a'.$i)]).'" maxlength="7" style="width:60px;ime-mode:disabled">
  </td>
  <td class="center">
    <input class="right'.($t_game[('f_item_b'.$i)] < 0 ? ' alert':'').'" type="text" name="t_game[f_item_b'.$i.']" value="'.($t_game[('f_item_b'.$i)]).'" maxlength="7" style="width:60px;ime-mode:disabled">
  </td>
  <td class="center">
    <input class="right" type="text" name="t_game[f_value'.$i.']" value="'.($t_game[('f_value'.$i)]).'" maxlength="16" style="width:120px;ime-mode:disabled">
  </td>
  <td class="center">
    <input class="right" type="text" name="t_game[f_limit'.$i.']" value="'.($t_game[('f_limit'.$i)]).'" maxlength="7" style="width:60px;ime-mode:disabled">
  </td>
  <td>
    <input type="text" name="t_game[f_message'.$i.']" value="'.($t_game[('f_message'.$i)]).'" maxlength="30" style="width:300px;ime-mode:active">
  </td>
</tr>
';
}
$html .= '<tr>
  <th></th>
  <td class="center">
    <input type="text" name="t_game[f_blank_name]" value="'.$t_game['f_blank_name'].'" maxlength="10" style="width:70px;ime-mode:enable"></input>
  </td>
  <td class="center">常に有効</td>
  <td class="center">
    <input class="right'.($t_game['f_blank_item_a'] < 0 ? ' alert':'').'" type="text" name="t_game[f_blank_item_a]" value="'.$t_game['f_blank_item_a'].'" maxlength="7" style="width:60px;ime-mode:disabled">
  </td>
  <td class="center">
    <input class="right'.($t_game['f_blank_item_b'] < 0 ? ' alert':'').'" type="text" name="t_game[f_blank_item_b]" value="'.$t_game['f_blank_item_b'].'" maxlength="7" style="width:60px;ime-mode:disabled">
  </td>
  <td class="center">
    設定なし
  </td>
  <td class="center">
    0（無制限）
  </td>
  <td>
    <input type="text" name="t_game[f_blank_message]" value="'.$t_game['f_blank_message'].'" maxlength="30" style="width:300px;ime-mode:active">
  </td>
</tr>

</table>
<ul>
  <li>抽選処理の優先順位
    <ul>
      <li>抽選処理は<u>当選確率（％）が低いものから順</u>に行われます。（配当が高いものからではありません。）</li>
      <li>この抽選順序決定手続きにボーナス効果は考慮されません。抽選順序が決定された後にボーナス効果が適用されます。</li>
    </ul>
  </li>
  <li>抽選処理のイメージ
    <ul><u>タイプＡ：当選確率50%超( n > 50 )の抽選</u>
      <li>1～100の目をもつ100面ダイスを振り、出た目が設定した当選確率より小さければ当選です。</li>
      <li>1%未満の当選確率は設定しても考慮されません。</li>
      <li>例）当選確率70%の場合・・・出た目が1～70なら当選、71～100ならハズレ</li>
    </ul>
    <ul><u>タイプＢ：当選確率50%以下( n <= 50 )の抽選</u>
      <li>(100 / 当選確率)を小数点以下四捨五入した個数のクジの中に1本だけ存在する当選クジを引き当てます。</li>
      <li>例）当選確率0.01%の場合・・・100 / 0.01 = 10000本のクジから1本しか存在しない当選クジに挑戦します。</li>
    </ul>
  </li>
  <li>無効化されている懸賞は抽選対象となりません。しかし、当選確率0%の懸賞は抽選対象になります。ボーナス効果が加算される可能性があるからです。</li>
  <li>【当選本数上限】に達した段階で、その日の間のその懸賞は打ち止めになります。0（ゼロ）と設定した場合は『本数無制限』を意味します。</li>
  <!--li><u>本機能はゲームという性質上、マイナス配当の設定を許容します。</u></liいかにもbugりそうな仕様なため廃止-->
</ul>
</fieldset>

<fieldset>
<legend>■ボーナス効果の共通ルール</legend>
<ul>
  <li>ボーナス効果とは？
    <ul>
      <li>ボーナス効果を所有するプレイヤーがゲームをプレイする場合にはその時点の『■懸賞設定』の当選確率に所有する<u>ボーナス効果が<b>加算</b>されます。</u></li>
      <li>ボーナス効果が加算された結果、当選確率が0%を下回る場合は0%に、100%を上回る場合は100%に丸められます。</u></li>
      <!--加算される仕様とする理由⇒"ボーナス効果を所有している状態＝所有していない状態より有利"という概念を明確化するためです。『場合によってボーナス効果を所有していることは意味がない』という仕様はいかにも分かりにくい。-->
    </ul>
  </li>
  <li>ボーナス効果のライフサイクル
    <ul>
      <li>ボーナス効果はプレイヤーがその効果を獲得してから次に当選を体験するまで所有され、<u>当選したことをもって消費されます。</u>当選を体験しない間はその効果は消費されません。</li>
      <li>ただし、上記規則は『ボーナス効果が有効に作用したか？』までは考慮しません。『A賞が当たり易いよう設定されたボーナス効果を使用したもののB賞に当選した』といった場合も消費されます。</li>
    </ul>
  </li>
  <li>ボーナス効果の優先順位
    <ul>
      <li>プレイヤーがボーナス効果を複数所有している場合でも、使用される効果はそのうちの１つだけになります。</li>
      <li>プレイヤーが複数の効果を所有している場合、<u>優先順位が高いもの</u>から使用され、優先順位で絞りきれない場合は、その中から<u>最も長い期間所有しているもの</u>を使用します。</li>
    </ul>
  </li>
</ul>
</fieldset>
';

/**
 * ■ボーナス効果設定フォームの生成
 */
$bonus_caption_array = array(
    '1'=>'ボーナス効果設定（コイン購入）',
    '2'=>'ボーナス効果設定（コインパック購入）',
//    '3'=>'ボーナス効果設定（友達紹介）',
);
$description_array = array(
    // ボーナス効果（コイン購入）の説明
    '1'=>'
<ul>
  <li>コイン購入時、会員は条件に該当する中で<u>最も発生条件が高いボーナス効果を１つ獲得します。</u></li>
</ul>
',
    // ボーナス効果（コインパック購入）の説明
    '2'=>'
<ul>
  <li>コインパック購入時、会員は条件に該当する中で<u>最も発生条件が高いボーナス効果を１つ獲得します。</u></li>
  <li>このとき、その<u>コインパックの額面コイン枚数</u>がボーナス発生条件の評価対象となります。</li>
</ul>
',
    // ボーナス効果（友達紹介）の説明
    '3'=>'
<ul>
  <li>このボーナス効果は【コイン購入】を行った会員に紹介者がいた場合に、その<u>紹介者に対して付与される</u>ボーナス効果です。(【コインパック購入】は対象外です。)</li>
</ul>
',
);
foreach ( $bonus_caption_array as $f_bonus_type=>$bonus_caption ) {
    if ( isset( $t_game_bonus[($f_bonus_type)] ) && is_array( $t_game_bonus[($f_bonus_type)] ) ) {
        $html .= '
<fieldset>
<legend>■'.$bonus_caption.'</legend>
<table class="list">
<tr>
  <th rowspan="2" style="width:80px">ボーナス効果</th>
  <th rowspan="2" style="width:100px">有効/無効</th>
  <th rowspan="2" style="width:120px">発生条件<br>（コイン購入枚数）</th>
  <th colspan="'.$const_item_count.'">[+]当選確率（％）</th>
  <th rowspan="2" style="width:60px">優先順位</th>
</tr>
<tr>
';
        for ( $i = 1; $i <= $const_item_count; $i++ ) {
            $html .= '
  <th>'.$i.'<br>[&nbsp;'.$t_game[('f_name'.$i)].'&nbsp;]</th>
';
        }
        $html .= '
</tr>
<!--パラメータの第一インデックス＝t_game_bonus.f_bonus_id-->
';
        foreach ( $t_game_bonus[($f_bonus_type)] as $row ) {
            $html .= '
<tr>
  <td class="center">
    <input type="text" name="t_game_bonus['.$row['f_bonus_id'].'][f_bonus_name]" value="'.$row['f_bonus_name'].'" maxlength="10" style="width:70px;ime-mode:enable"></input>
    <input type="hidden" name="t_game_bonus['.$row['f_bonus_id'].'][f_bonus_type]" value="'.$row['f_bonus_type'].'"><!--1=コイン購入,2=コインパック購入,3=友達紹介-->
  </td>
  <td class="center">
     <select name="t_game_bonus['.$row['f_bonus_id'].'][f_status]" style="width:80px">
        <option value="0"'.($row['f_status'] == '0' ? ' selected':'').'>○有効</option>
        <option value="1"'.($row['f_status'] == '1' ? ' selected':'').'>×無効</option>
     </select>
  </td>
  <td class="center">
    <input class="right" type="text" name="t_game_bonus['.$row['f_bonus_id'].'][f_trigger_value]" value="'.$row['f_trigger_value'].'" maxlength="16" style="width:120px;ime-mode:disabled">
  </td>
';
            for ( $i = 1; $i <= $const_item_count; $i++ ) {
            $html .= '
  <td class="center">
    <input class="right" type="text" name="t_game_bonus['.$row['f_bonus_id'].'][f_value'.$i.']" value="'.$row[('f_value'.$i)].'" maxlength="16" style="width:120px;ime-mode:disabled">
  </td>
  ';
            }
            $html .= '
  <td class="center">
    <input class="right" type="text" name="t_game_bonus['.$row['f_bonus_id'].'][f_priority_level]" value="'.$row['f_priority_level'].'" maxlength="2" style="width:40px;ime-mode:disabled">
  </td>
</tr>
';
        }
        $html .= '
</table>
'.$description_array[($f_bonus_type)].'
</fieldset>
';
    }
}

$html .= '
<div style="text-align: center">
  <button type="reset" onClick="if (confirm(\'リセットしますか？\')){return true}else{return false}">リセット</button>
  <button type="submit" onClick="if (updatePrepareCheck() && confirm(\'データを更新します。\')){return true}else{return false}">更新</button>
</div>
<input type="hidden" name="do_update" value="1">
</form>
<div style="text-align: right;color: #666">
last-modified: '.$t_game['f_update_time'].'( '.$t_game['f_update_staff_id'].' )
</div>
';

PrintAdminPage( 'ゲーム設定変更', $html );
?>
