<?php
/******************************
 *
 * ■ボーナス効果履歴一覧画面
 *
 *
 */

// TODO:1-5のキャプションに景品名を入れる

// 共通ライブラリ読込
include "../../lib/define.php";
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;
require_once( realpath(COMMON_LIB.'game_lib.php') );

// ページャーの生成ファンクションが定義されている
$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
include $utils_class;

$game_suffix = 'A';

/**
 * ■権限チェック
 */
session_start();
if ( isset( $_SESSION['staff_id'] ) ) {
    if ( !CheckStaffTAuthority( $_SESSION['staff_id'], '31') ) {
        PrintAdminPage( 'ボーナス履歴一覧', '操作権限がありません。' );
        exit;
    }
} else {
    PrintAdminPage( 'ボーナス履歴一覧', 'セッションが切れました。' );
    exit;
}

/**
 * ■システムパラメータ設定チェック
 */
if ( !defined( 'GAME_ID_'.$game_suffix ) ) {
    PrintAdminPage( 'ゲーム設定変更', '"GAME_ID_'.$game_suffix.'" is undefined.' );
    exit;
}

/**
 * ■変数初期化
 */
$const_game_id = constant( 'GAME_ID_'.$game_suffix );
$const_item_count = 4;
if ( defined( 'GAME_ITEMS_'.$game_suffix ) ) {
    $const_item_count = constant( 'GAME_ITEMS_'.$game_suffix );
}
if ( $const_item_count > GameModel::PRIZE_ITEMS_LIMIT ) {
    $const_item_count = GameModel::PRIZE_ITEMS_LIMIT;
}
$data = array();
$total_count = 0;
$page_no = 1;
$last_page_no = 1;
$limit = 50;
$offset = 0;
$search_type = 0;
$member_id = NULL;

/**
 * ■リクエストパラメータ取得
 */
if ( isset( $_REQUEST['search_type'] ) ) {
    $search_type = intval( $_REQUEST['search_type'] );
}
if ( isset( $_REQUEST['page'] ) ) {
    $page_no = intval( $_REQUEST['page'] );
}
if ( $page_no < 1 ) {
    $page_no = 1;
}
if ( isset( $_REQUEST['member_id'] ) ) {
    $member_id = intval( $_REQUEST['member_id'] );
}

/**
 * ■主処理
 */
$db = db_connect();
if ( !$db ) {
   PrintAdminPage( 'ボーナス履歴一覧', 'データベース接続エラー<br>'.mysql_error() );
   exit;
}

/**
 * ■データ取得処理
 */

// t_game
$t_game = array();
$sql = "SELECT * FROM auction.t_game WHERE f_game_id=".$const_game_id;
$res = mysql_query( $sql, $db );
if ( $res === FALSE ) {
   PrintAdminPage( 'ゲーム設定変更', 'ゲームマスタ取得エラー<br>'.mysql_error() );
   exit;
}
$t_game = mysql_fetch_assoc( $res );
if ( $t_game !== FALSE ) {
    mysql_free_result( $res );
} else {
   PrintAdminPage( 'ゲーム設定変更', 'ゲームマスタフェッチエラー<br>'.mysql_error() );
   exit;
}

// t_game_bonus_log
$where = '';
switch ( $search_type ) {
case 1:
    // 所持中のみ
    $where .= " AND f_status=0";
    break;
case 2:
    // 消費済のみ
    $where .= " AND f_status=1";
    break;
}
if ( $member_id ) {
    $where .= " AND f_member_id=".$member_id;
}

$sql = "SELECT COUNT(f_game_bonus_log_id) cnt FROM auction.t_game_bonus_log WHERE f_game_id=".$const_game_id;
$sql = $sql.$where;
$res = mysql_query( $sql, $db );
if ( $res !== FALSE ) {
    $data = mysql_fetch_assoc( $res );
    $total_count = $data['cnt'];
    $last_page_no = ceil( $total_count / $limit );
    if ( $last_page_no < 1 ) {
        $last_page_no = 1;
    }
    if ( $page_no > $last_page_no ) {
        $page_no = $last_page_no;
    }
    $offset = $limit * ( $page_no - 1 );
    mysql_free_result( $res );
} else {
   PrintAdminPage( 'ボーナス履歴一覧', 'データ件数取得エラー<br>'.$sql.'<br>'.mysql_error() );
   exit;
}
$data = array();
$sql = "
SELECT
  f_game_bonus_log_id
  ,f_member_id
  ,f_game_id
  ,f_status
  ,f_priority_level
  ,f_bonus_type
  ,f_bonus_name
  ,f_trigger_value
  ,f_value1
  ,f_value2
  ,f_value3
  ,f_value4
  ,f_value5
  ,f_value6
  ,f_value7
  ,f_value8
  ,f_value9
  ,f_value10
  ,f_expire_time
  ,f_create_time
  ,f_update_time
FROM
  auction.t_game_bonus_log
WHERE
  f_game_id=".$const_game_id.$where."
ORDER BY f_game_bonus_log_id DESC
LIMIT ".$offset.", ".$limit;
$res = mysql_query( $sql, $db );
if ( $res !== FALSE ) {
    $tmp = NULL;
    while ( $tmp = mysql_fetch_assoc( $res ) ) {
        $data[] = $tmp;
    }
    mysql_free_result( $res );
} else {
   PrintAdminPage( 'ボーナス履歴一覧', 'データ取得エラー<br>'.$sql.'<br>'.mysql_error() );
   exit;
}

db_close($db);

/**
 * ■ページャーHTMLの生成
 */
$pager = '';
if ( $last_page_no > 1 ) {
    $pager .= '
<div class="pager">
';
    $pager .= Utils::getPageNumbersII( $page_no, $last_page_no, 'game_bonus_log.php', ($last_page_no < 5 ? $last_page_no:5) );
    $pager .= '
  <span class="mypagenum_box4">
    <form action="game_bonus_log.php" method="get" style="display: inline">
      <input type="text" size="2" name="page" style="ime-mode:disabled"/>
      <input type="submit" value="Jump"/>
    </form>
  </span>
</div>
';
}

/**
 * ■一覧HTML生成
 */

$html = '
<style type="text/css">
div.pager {
    /* text-align: center; 大抵の表で右にはみ出す */
    color: #666;
    vertical-align: middle;
    height: 32px;
    margin: 1px;
}
div.pager form {
    display: inline;
}
</style>
<div class="center">
'.$pager.'
</div>
<div class="center" style="width:100%">
<table class="data">
  <tr><th style="width:100px">データ件数</th><td class="center" style="width:100px">'.$total_count.'</td></tr>
</table>
</div>
<div style="color:#666;padding:2px">
'.($total_count ? ($offset + 1):0).'&nbsp件目&nbsp～&nbsp;'.($total_count - $offset).'&nbsp件目
</div>
<table class="list">
  <tr>
    <th title="履歴ID" rowspan="2" style="width:80px">履歴ID</th>
    <th title="会員ID" rowspan="2" style="width:60px">会員ID</th>
    <th title="ボーナス情報" colspan="'.(5 + $const_item_count).'">ボーナス</th>
    <th title="作成日時" rowspan="2" style="width:120px">作成日時</th>
    <th title="更新日時" rowspan="2" style="width:120px">更新日時</th>
  </tr>
  <tr>
    <th title="0=所持中、1=消費済み">状態</th>
    <th title="優先順位">優先</th>
    <th title="1=ｺｲﾝ購入,2=ｺｲﾝﾊﾟｯｸ購入">種別</th>
    <th title="名称" style="width:80px">名称</th>
    <th title="発生条件値" style="width:100px">発生条件値</th>
';
for ( $i = 1; $i <= $const_item_count; $i++ ) {
    $html .= '
<th title="ボーナス変数" style="width:80px">'.$i.'<br>[&nbsp;'.$t_game[('f_name'.$i)].'&nbsp;]</th>
';
}
$html .= '
  </tr>
';
foreach ( $data as $row ) {
    $html .= '
  <tr>
    <td class="center">'.$row['f_game_bonus_log_id'].'</td>
    <td class="center">'.$row['f_member_id'].'</td>
    <td class="center">'.$row['f_status'].'</td>
    <td class="center">'.$row['f_priority_level'].'</td>
    <td class="center">'.$row['f_bonus_type'].'</td>
    <td><div title="'.$row['f_bonus_name'].'" style="width:80px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis">'.$row['f_bonus_name'].'</div></td>
    <td class="right">'.$row['f_trigger_value'].'</td>
';
    for ( $i = 1; $i <= $const_item_count; $i++ ) {
    $html .= '
    <td class="right">'.$row[('f_value'.$i)].'</td>
';
    }
$html .= '
    <td class="center">'.$row['f_create_time'].'</td>
    <td class="center">'.$row['f_update_time'].'</td>
  </tr>
';
}
$html .= '
</table>

<div class="center">
'.$pager.'
</div>

<ul>
  <li>項目値の意味については見出しをマウスオーバーすることで参照することができます。</li>
</ul>
';
PrintAdminPage( "ボーナス効果履歴一覧", $html );
?>
