<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/layout.css" type="text/css" rel="stylesheet">
<title></title>
</head>
<body class="sub_category">
<script type="text/javascript">
<!--
function jump( l_url, r_url ) {
	// 設定開始（表示するフレーム名とリンク先URLを設定してください）
	parent.main_r.location.href = r_url;
        parent.main_l.location.href = l_url;
	// 設定終了
}
// -->
</script>
<?php
session_start();

if( !isset( $_SESSION['staff_id'] ) )
{
?>
    <P>不正アクセスです</P>
</body>
</html>
<?PHP
    exit;
}

include "../control/exec_select.php";

//そのスタッフの持っている権限からカテゴリを表示
$rs = get_menu( $_SESSION["staff_id"], $_GET["cat_id"], $_SESSION['op_level'] );
if($rs == -1)
{
?>
<P>このカテゴリの実行権はありません</P>
</body>
</html>
<?php
    exit;
}

$row=mysql_num_rows($rs);
$html="<table class=\"main_menu\"><tr>";

for($i = 0 ; $i <$row;$i++)
{
    $ret = mysql_fetch_array($rs);

    //表示メニュー設定
    // ①：&#x2460;
    // ②：&#x2461;
    switch($ret["au_id"])
    {
        case 31:
            if ( defined( 'GAME_ENABLE' ) && constant( 'GAME_ENABLE' ) ) {
                $html .= "<td><a href=\"#\" onClick=\"jump('../service/game_menu.php','../service/blank.php')\">&#x2460;".$ret["au_name"]."</td>";
            } else {
                $html .= "<td><a href=\"#\" onClick=\"alert('この機能は未実装です。')\">&#x2460;".$ret["au_name"]."</td>";
            }
            break;
        case 32:
            $html .= "<td><a href=\"#\" onClick=\"alert('この機能は未実装です。')\">&#x2461;".$ret["au_name"]."</td>";
            break;
    }
}
$html .="</tr></table>";
print($html);
?>
</body>
</html>
