<?php
// 共通ライブラリ読込
include "../../lib/define.php";
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;

session_start();

$html = '
<table class="main_l">
<caption>&#x2460;フラッシュゲーム</caption>
  <tr><th>ゲーム設定</th></tr>
  <tr><td><a href="../service/game_master.php" style="font-size:9pt; text-align:left;" target="main_r">ゲーム設定</a></td></tr>
  <tr><td></td></tr>
  <form action="game_log.php" method="POST" target="main_r">
  <tr><th>ゲームプレイ履歴</th></tr>
  <tr><td>
  会員ID：<input type="text" name="member_id" maxlength="7" style="width:80px;ime-mode:disabled">
  </td></tr>
  <tr><td>
    <input type="radio" name="search_type" value="0">全て</input><br>
    <input type="radio" name="search_type" value="1" checked>未プレイのみ</input><br>
    <input type="radio" name="search_type" value="2">プレイ済のみ</input><br>
    <input type="radio" name="search_type" value="3">プレイ済＋未払戻</input>
  </td></tr>
  <tr><td style="text-align:center"><button type="submit" style="background-color:#FFFBEC; color:#C84B00; width:80px; border-color:#FFFAFA;">検索</button>
  </form>
  <form action="game_bonus_log.php" method="POST" target="main_r">
  <tr><th>ボーナス効果履歴</th></tr>
  <tr><td>
  会員ID：<input type="text" name="member_id" maxlength="7" style="width:80px;ime-mode:disabled">
  </td></tr>
  <tr><td>
    <input type="radio" name="search_type" value="0">全て</input><br>
    <input type="radio" name="search_type" value="1" checked>所持中のみ</input><br>
    <input type="radio" name="search_type" value="2">消費済のみ</input>
  </td></tr>
  <tr><td style="text-align:center"><button type="submit" style="background-color:#FFFBEC; color:#C84B00; width:80px; border-color:#FFFAFA;">検索</button>
  </form>
</table>
';

//管理画面入力ページ表示関数
PrintAdminInputPage( $html );
?>
