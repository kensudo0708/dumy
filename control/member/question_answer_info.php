<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/08/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$qid = $_REQUEST["qid"];
	$qaid = $_REQUEST["qaid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	$inp_flag = $_REQUEST["inp_flag"];
	$inp_category = $_REQUEST["inp_category"];
	
	//質問回答情報取得関数
	GetTQuestionAnswerInfo($qaid,$tqa_fk_question_id,$tqa_f_subject,$tqa_f_main,$tqa_f_name,$tqa_f_staff_id,$tqa_f_tm_stamp);
	
	//▼ﾀｲﾑｽﾀﾝﾌﾟ
	$tm_stamp_str = substr($tqa_f_tm_stamp, 0, 10);
	
	//▼本文改行処理
	$tqa_f_main = str_replace("\r", "<BR>", $tqa_f_main);
	
	//スタッフ管理者権限判定関数
	//▼会員メアド表示権限を取得
	$staff_mail = CheckStaffTAuthority($sid,"24");
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:100px;'>質問No</th>\n";
	$dsp_tbl .= "<td style = 'width:350px; height:25px; text-align:left; padding-left:5px;'>$qid</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>回答No</th>\n";
	$dsp_tbl .= "<td style = 'height:25px; text-align:left; padding-left:5px;'>$qaid</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>回答者</th>\n";
	$dsp_tbl .= "<td style = 'height:25px; text-align:left; padding-left:5px;'>$tqa_f_name</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ﾀｲﾑｽﾀﾝﾌﾟ</th>\n";
	$dsp_tbl .= "<td style = 'height:25px; text-align:left; padding-left:5px;'>$tm_stamp_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>タイトル</th>\n";
	$dsp_tbl .= "<td style = 'height:25px; text-align:left; padding-left:5px;'>$tqa_f_subject</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>本文</th>\n";
	$dsp_tbl .= "<td style = 'height:25px; text-align:left; padding-left:5px;'>$tqa_f_main</td>\n";
	$dsp_tbl .= "</tr>\n";
	
/*
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=6 align=center>\n";
	$dsp_tbl .= "<FORM action='question_answer_send.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qid' value='$qid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qaid' value='$qaid'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_flag' value='$inp_flag'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_category' value='$inp_category'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value='$page'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='送信'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='question_answer_list.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qid' value='$qid'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_flag' value='$inp_flag'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_category' value='$inp_category'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value='$page'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='戻る'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
*/
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "<div style = 'text-align:center;'>\n";
        $dsp_tbl .= "<input type='button' value='戻る' class = 'button1' onClick='history.back()'>\n";
	$dsp_tbl .= "</div>\n";
	//管理画面入力ページ表示関数
	PrintAdminPage("問合せ回答",$dsp_tbl);

?>
