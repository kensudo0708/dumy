<script type="text/javascript">
    /**
     * 確認ダイアログの返り値によりフォーム送信
    */
<!--
	function submitChk ()
	{
		/* 確認ダイアログ表示 */
		var flag = confirm ('指定した写真を削除します。よろしいですか？');
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Li													*/
/*		作成日		:	2010/09/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;


	$fk_item_category = $_REQUEST["pid"];
	$mode = $_REQUEST["mode"];
	$photo_num = $_REQUEST["photo_num"];

	$newFlg=false;
//	//写真削除モード
//	if($mode == "dltphoto")
//	{
//		//商品テンプレート写真削除関数
//		DeleteTProductsTemplatePhoto($pid, $photo_num);
//	}

	if(isset($fk_item_category) && $fk_item_category != "")
	{
		//商品カテゴリ取得関数
		//GetTCategoryInfo($fk_item_category,$f_category_name,$f_mail_category_val,$f_tm_stamp, $f_rbstp, $f_rbedp, $f_status,$f_dspno);
//////////////////////////////////////////////////////////////////////
            //■ＤＢ接続
            $db=db_connect();
            if($db == false) {
                exit;
            }

            $sql  = "select fk_item_category,f_category_name,f_mail_category_val,f_tm_stamp,f_rbstp,f_rbedp,f_status,f_dspno ";
            $sql .= "from auction.t_item_category where fk_item_category='$fk_item_category' ";

            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ＤＢ切断
                db_close($db);
                return false;
            }
            //■データ件数
            $rows = mysql_num_rows($result);

            if($rows > 0) {
                $fk_item_category	= mysql_result($result, 0, "fk_item_category");
                $f_category_name	= mysql_result($result, 0, "f_category_name");
                $f_mail_category_val	= mysql_result($result, 0, "f_mail_category_val");
                $f_tm_stamp	= mysql_result($result, 0, "f_tm_stamp");
                $f_rbstp	= mysql_result($result, 0, "f_rbstp");
                $f_rbedp	= mysql_result($result, 0, "f_rbedp");
                $f_status	= mysql_result($result, 0, "f_status");
                $f_dspno	= mysql_result($result, 0, "f_dspno");
            }
            else {
                $fk_item_category	= "";
                $f_category_name	= "";
                $f_mail_category_val	= "";
                $f_tm_stamp	= "";
                $f_rbstp	= "";
                $f_rbedp	= "";
                $f_status	= "";
                $f_dspno	= "";
            }

            //■ＤＢ切断
            db_close($db);
/////////////////////////////////////////////////////////////////////


        }
	else
	{
            $newFlg = true;

            $fk_item_category	= "";
            $f_category_name	= "";
            $f_mail_category_val	= "";
            $f_tm_stamp	= "";
            $f_rbstp	= "";
            $f_rbedp	= "";
            $f_status	= "";
            $f_dspno	= "";

	}

        $f_status_id[0] = "0";
	$f_status_name[0] = "使用中";
	$f_status_id[1] = "1";
	$f_status_name[1] = "未使用";
	$f_status_cnt = 2;
	$name = "f_status";



        if($f_status == "")
	{
		$f_status = 0;
	}
	else if($f_status > 2)
	{
		$f_status = 1;
	}
//G.Chin 2010-07-01 add end
	$select_num = $f_status;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);

//	//▼写真
//	if($tpt_f_photo1 != "")
//	{
//		$f_photo_1_path = SITE_URL."images/".$tpt_f_photo1;
//		$f_photo_1_dsp = "<img src='$f_photo_1_path'>";
//		$link_dltphoto1 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=1' onClick=\"return submitChk()\">削除</A>";
//	}
//	else
//	{
//		$f_photo_1_dsp = "";
//		$link_dltphoto1 = "";
//	}
//
//	if($tpt_f_photo2 != "")
//	{
//		$f_photo_2_path = SITE_URL."images/".$tpt_f_photo2;
//		$f_photo_2_dsp = "<img src='$f_photo_2_path'>";
//		$link_dltphoto2 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=2' onClick=\"return submitChk()\">削除</A>";
//	}
//	else
//	{
//		$f_photo_2_dsp = "";
//		$link_dltphoto2 = "";
//	}
//
//	if($tpt_f_photo3 != "")
//	{
//		$f_photo_3_path = SITE_URL."images/".$tpt_f_photo3;
//		$f_photo_3_dsp = "<img src='$f_photo_3_path'>";
//		$link_dltphoto3 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=3' onClick=\"return submitChk()\">削除</A>";
//	}
//	else
//	{
//		$f_photo_3_dsp = "";
//		$link_dltphoto3 = "";
//	}
//
//	if($tpt_f_photo4 != "")
//	{
//		$f_photo_4_path = SITE_URL."images/".$tpt_f_photo4;
//		$f_photo_4_dsp = "<img src='$f_photo_4_path'>";
//		$link_dltphoto4 = "<A href='product_temp_regist.php?pid=$pid&mode=dltphoto&photo_num=4' onClick=\"return submitChk()\">削除</A>";
//	}
//	else
//	{
//		$f_photo_4_dsp = "";
//		$link_dltphoto4 = "";
//	}
//


	$dsp_tbl  = "";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 商品カテゴリID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$fk_item_category</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<FORM action='product_category_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 商品カテゴリ名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_category_name' value='$f_category_name' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";

//        $dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> メルマガ用カテゴリ値 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_mail_category_val' value='$f_mail_category_val' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 作業用① </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_rbstp' value='$f_rbstp' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 作業用② </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><input type='text' name='f_rbedp' value='$f_rbedp' size='50'></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file1' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto1;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 状態 </tt></th>\n";
        if(isset($fk_item_category) && $fk_item_category>0){
            $dsp_tbl .= "<td colspan=3><font color='#B22222'>$f_status_select</font></td>\n";
        }else{
            $dsp_tbl .= "<td colspan=3><font color='#B22222'>使用中</font><INPUT TYPE='hidden' NAME='f_status' value='0'></td>\n";
        }

	$dsp_tbl .= "</tr>\n";

//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 写真② </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_2_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file2' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto2;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";

//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 写真③ </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_3_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file3' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto3;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> 写真④ </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$f_photo_4_dsp</tt></font>\n";
//	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' SIZE='65536'>\n";
//	$dsp_tbl .= "<INPUT TYPE='file' NAME='file4' SIZE='25'>\n";
//	$dsp_tbl .= $link_dltphoto4;
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> PC用(見出し) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_top_description_pc' value='$tpt_f_top_description_pc' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> PC用(詳細) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><textarea name='f_main_description_pc' cols='50' rows='5'>$tpt_f_main_description_pc</textarea></font>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> MB用(見出し) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_top_description_mb' value='$tpt_f_top_description_mb' size='50'></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> MB用(詳細) </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><textarea name='f_main_description_mb' cols='50' rows='5'>$tpt_f_main_description_mb</textarea></font>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=center colspan=4><tt><font color='#FF0000'>料金設定</font></tt></th>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'>一般市場価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_market_price' value='$tpt_f_market_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'>仕入価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_min_price' value='$tpt_f_min_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FF0000' align=right><tt> <font color='#FFFFFF'>開始価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_start_price' value='$tpt_f_start_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'>増額時の最大価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_r_max_price' value='$tpt_f_r_max_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'>減額時の最小価格</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_r_min_price' value='$tpt_f_r_min_price'> 円</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";
//
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'>ポイントオークションでのポイント数</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><input type='text' name='f_plus_coins' value='$tpt_f_plus_coins'> 枚</tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";

//	if($tpt_f_address_flag == 0)
//	{
//		$selected0="selected";
//		$selected1="";
//	}
//	else if($tpt_f_address_flag == 1)
//	{
//		$selected0="";
//		$selected1="selected";
//	}

//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> <font color='#FF0000'>配送先設定</font> </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt><SELECT name='f_address_flag'><OPTION value='0' ".$selected0.">配送先必要</OPTION><OPTION value='1' ".$selected1.">配送先不要</OPTION></SELECT></tt></font></td>\n";
//	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
        if($newFlg){
            $dsp_tbl .= "<input type='hidden' name='pid' value='-1'>\n";
        }else{
            $dsp_tbl .= "<input type='hidden' name='pid' value='$fk_item_category'>\n";
        }
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='更新'>\n";
	$dsp_tbl .= "</FORM>\n";
        if(isset($fk_item_category) && $fk_item_category>0 && $f_status==0){
            $dsp_tbl .= "<FORM action='product_category_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
            $dsp_tbl .= "<input type='hidden' name='fk_item_category' value='$fk_item_category'>\n";
            $dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='削除'>\n";
            $dsp_tbl .= "</FORM>\n";
        }
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("商品カテゴリ編集",$dsp_tbl);

?>
