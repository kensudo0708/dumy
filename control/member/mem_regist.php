<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/10												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	$op_level = $_SESSION['op_level'];
	$mid = $_REQUEST["mid"];
	$js="";
	if(($mid != "") && ($mid != 0))
	{
		//会員情報取得関数
		GetTMemberInfo($mid,$tmm_f_login_id,$tmm_f_login_pass,$tmm_f_handle,$tmm_fk_area_id,$tmm_fk_job_id,$tmm_f_activity,
                        $tmm_fk_member_group_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb,$tmm_f_ser_no,$tmm_f_mailmagazein_pc,
                        $tmm_f_mailmagazein_mb,$tmm_fk_adcode_id,$tmm_fk_parent_ad,$tmm_f_regist_date,$tmm_f_regist_pos,$tmm_f_name,
                        $tmm_f_tel_no,$tmm_f_sex,$tmm_f_birthday,$tmm_fk_address_flg,$tmm_f_over_money,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_last_entry,$ip,$f_unpain_num,$f_cancel_num);
		//会員メモ取得関数
		GetTMemberMemo($mid,$tmm_f_memo);
		//会員住所件数取得関数
		GetTAddressCount($mid,$t_address_cnt);
		//会員機種情報取得関数
		GetTMemberUserAgent($mid,$tmm_f_user_agent);	//G.Chin 2010-10-01 add
	}
	else
	{
		//現在日時
		$now = time();
		
		$tmm_f_login_id			= "";
		$tmm_f_login_pass		= "";
		$tmm_f_handle			= "";
		$tmm_fk_area_id			= "";
		$tmm_fk_job_id			= "";
		$tmm_f_activity			= "";
		$tmm_fk_member_group_id	= "";
		$tmm_f_mail_address_pc	= "";
		$tmm_f_mail_address_mb	= "";
		$tmm_f_ser_no			= "";
		$tmm_f_mailmagazein_pc	= "";
		$tmm_f_mailmagazein_mb	= "";
		$tmm_fk_adcode_id		= "";
		$tmm_fk_parent_ad		= "";
		$tmm_f_regist_date		= date("Y-m-d H:i:00", $now);
		$tmm_f_regist_pos		= "2";
		$tmm_f_name				= "";
		$tmm_f_tel_no			= "";
		$tmm_f_sex				= "";
		$tmm_f_birthday			= "";
		$tmm_fk_address_flg		= "0";
		$tmm_f_over_money		= "0";
		$tm_f_coin				= "0";
		$tm_f_free_coin			= "0";
		$tm_f_total_pay			= "0";
		$tm_f_last_entry		= "";
		$tmm_f_memo				= "";
		$t_address_cnt			= "0";
		$ip						= "0.0.0.0";
		$tmm_f_user_agent		= "";	//G.Chin 2010-10-01 add
                $f_unpain_num                   =0;
                $f_cancel_num                   =0;

	}
	
	//▼状態
	$f_activity_id[0] = "0";
	$f_activity_name[0] = "仮登録";
	$f_activity_id[1] = "1";
	$f_activity_name[1] = "会員";
	$f_activity_id[2] = "2";
	$f_activity_name[2] = "退会者";
	$f_activity_cnt = 3;
	$name = "f_activity";
	$select_num = $tmm_f_activity;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_activity_id, $f_activity_name, $f_activity_cnt, $select_num, $activity_select);
	
	//▼会員グループ選択作成
	//会員グループ名一覧取得関数
	GetFMGroupNameList($fmg_fk_member_group_id, $fmg_f_member_group_name, $fmg_data_cnt);
	$name = "fk_member_group_id";
	$select_num = $tmm_fk_member_group_id;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $fmg_fk_member_group_id, $fmg_f_member_group_name, $fmg_data_cnt, $select_num, $group_select);
	//地域一覧取得関数
	GetAreaNameList($fmg_fk_area_id, $fmg_area_name, $fmg_data_cnt);
	
	//地域選択オブジェクト作成関数
	$name = "fk_area_id";
	$select_num = $tmm_fk_area_id;
	MakeSelectObject($name, $fmg_fk_area_id, $fmg_area_name, $fmg_data_cnt, $select_num, $area_select);
	
	//職業名一覧取得関数
	GetJobNameList($fmg_fk_Job_id, $fmg_Job_name, $fmg_data_cnt);
	//職業選択オブジェクト作成関数
	$name = "fk_job_id";
	$select_num = $tmm_fk_job_id;
	MakeSelectObject($name, $fmg_fk_Job_id, $fmg_Job_name, $fmg_data_cnt, $select_num, $job_select);
	
	//▼メルマガ設定(PC)
	if($tmm_f_mailmagazein_pc == 1)
	{
		$check_str_pc = "checked";
	}
	else
	{
		$check_str_pc = "";
	}
	//▼メルマガ設定(PC)
	if($tmm_f_mailmagazein_mb == 1)
	{
		$check_str_mb = "checked";
	}
	else
	{
		$check_str_mb = "";
	}
	
	//▼紹介者
	//▼広告コード
	GetTAdcodeInfo($tmm_fk_parent_ad,$ta_f_adcode,$ta_fk_admaster_id,$ta_f_type,$ta_f_tm_stamp);
	if($ta_f_type == "")		//紹介者が存在しない場合
	{
		$parent_str = "";
	}
	else if($ta_f_type == 0)	//紹介者が会員の場合
	{
		//広告コード検索会員ID取得関数
		SrcFAdcodeIdGetTMemberId($tmm_fk_parent_ad,$parent_id,$parent_login_id);
		
		$parent_str = "会員：".$parent_login_id;
	}
	else						//紹介者が広告主の場合
	{
		//広告マスタ情報取得関数
		GetTAdmasterInfo($ta_fk_admaster_id,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
		$parent_str = $tam_k_adname;
	}
	
	//▼登録日
	$regist_date_str = substr($tmm_f_regist_date, 0, 16);
	if($mid == 0)
	{
		//新規登録の場合、登録日は入力不可能
		$readonly_regd = "readonly";
	}
	else
	{
		//会員情報編集の場合、登録日は入力可能
		$readonly_regd = "";
	}
	
	//▼登録元
	switch($tmm_f_regist_pos)
	{
		case 0:		$regist_pos_str = "PC";			break;
		case 1:		$regist_pos_str = "MB";			break;
		case 2:		$regist_pos_str = "管理画面";	break;
		default:	$regist_pos_str = "";			break;
	}
	
	//▼性別
	$f_sex_id[0] = "0";
	$f_sex_name[0] = "男性";
	$f_sex_id[1] = "1";
	$f_sex_name[1] = "女性";
	$f_sex_cnt = 2;
	$name = "f_sex";
	$select_num = $tmm_f_sex;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_sex_id, $f_sex_name, $f_sex_cnt, $select_num, $sex_select);
	
	//▼誕生日
	if($tmm_f_birthday == "")
	{
		$birthday_str = "";
	}
	else
	{
		$birth_date = substr($tmm_f_birthday, 0, 10);
		
		//日付文字列変更関数
		ChangeDateString($birth_date, $birthday_str);
	}
	
	//▼住所リスト画面へのリンク
	$link_address = "<A href='mem_address.php?sid=$sid&mid=$mid&aid=&up_mode' target='_blank'>編集</A>";
	
	//▼ラストログイン
	$lastentry_str = $tm_f_last_entry;
	$now = time();
	$year = substr($tm_f_last_entry, 0, 4);
	$month = substr($tm_f_last_entry, 5, 2);
	$day = substr($tm_f_last_entry, 8, 2);
	$hour = substr($tm_f_last_entry, 11, 2);
	$minute = substr($tm_f_last_entry, 14, 2);
	$second = substr($tm_f_last_entry, 17, 2);
	$last_tm = mktime($hour,$minute,$second,$month,$day,$year);
	$time_diff = $now - $last_tm;
	if($time_diff <= 3600)
	{
		$lastentry_color = "#FF0000";
		$passage = floor($time_diff/60);
		$lastentry_str .= " (";
		$lastentry_str .= $passage;
		$lastentry_str .= "分前)";
	}
	else
	{
		$lastentry_color = "#000000";
	}
	
	//スタッフ管理者権限判定関数
	//▼会員メアド表示権限を取得
	$staff_mail = CheckStaffTAuthority($sid,"24");
	//▼配送先表示権限を取得
	$staff_address = CheckStaffTAuthority($sid,"25");
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<FORM action='mem_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	
	//会員メアド表示権限,配送先表示権限を会員情報アップ関数に伝える
        $dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .= "<legend>会員情報</legend>\n";
        $dsp_tbl .= "<table class = 'form'>\n";



        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員ID</th>\n";
	$dsp_tbl .= "<td>$mid</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<input type='hidden' name='staff_mail' value='$staff_mail'>\n";
	$dsp_tbl .= "<input type='hidden' name='staff_address' value='$staff_address'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ログインID</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_login_id' value='$tmm_f_login_id' style = 'width:400px;'></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>パスワード</th>\n";
	$dsp_tbl .= "<td><input type='text' name='inp_pass' value='$tmm_f_login_pass' style = 'width:400px;'>&nbsp;<input type='checkbox' name='up_login_pass' value='1'>&nbsp;※&nbsp;パスワードを変更する</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ハンドル名</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_handle' value='$tmm_f_handle' style = 'width:400px;'</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ラストログイン</th>\n";
	$dsp_tbl .= "<td>$lastentry_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>状態</th>\n";
	$dsp_tbl .= "<td>$activity_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員グループ</th>\n";
	$dsp_tbl .= "<td>$group_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>メモ</th>\n";
	$dsp_tbl .= "<td><textarea name='f_memo' style = 'width:400px;' rows='5'>$tmm_f_memo</textarea></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	//▼会員メアド表示権限を判定
	if(($staff_mail == true) || ($mid == "") || ($mid == 0))
	{
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>PCﾒｰﾙｱﾄﾞﾚｽ</th>\n";
		$dsp_tbl .= "<td><input type='text' name='f_mail_address_pc' value='$tmm_f_mail_address_pc' style = 'width:400px;'></td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>MBﾒｰﾙｱﾄﾞﾚｽ</th>\n";
		$dsp_tbl .= "<td><input type='text' name='f_mail_address_mb' value='$tmm_f_mail_address_mb' style = 'width:400px;'></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
//	else
//	{
//		$dsp_tbl .= "<tr>\n";
//		$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> PCﾒｰﾙｱﾄﾞﾚｽ </tt></th>\n";
//		$dsp_tbl .= "<td colspan=3></td>\n";
//		$dsp_tbl .= "</tr>\n";
////		$dsp_tbl .= "<input type='hidden' name='f_mail_address_pc' value='$tmm_f_mail_address_pc'>\n";
//
//		$dsp_tbl .= "<tr>\n";
//		$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> MBﾒｰﾙｱﾄﾞﾚｽ </tt></th>\n";
//		$dsp_tbl .= "<td colspan=3></td>\n";
//		$dsp_tbl .= "</tr>\n";
////		$dsp_tbl .= "<input type='hidden' name='f_mail_address_mb' value='$tmm_f_mail_address_mb'>\n";
//	}
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>携帯固体番号</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_ser_no' value='$tmm_f_ser_no' style = 'width:400px;' readonly></td>\n";
	$dsp_tbl .= "</tr>\n";
	
//G.Chin 2010-10-01 add sta
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>機種情報</th>\n";
	$dsp_tbl .= "<td>$tmm_f_user_agent</td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-10-01 add end
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ﾒﾙﾏｶﾞ設定</th>\n";
	$dsp_tbl .= "<td><input type='checkbox' name='f_mailmagazein_pc' value='1' $check_str_pc>PCﾒﾙﾏｶﾞ配信&nbsp;<input type='checkbox' name='f_mailmagazein_mb' value='1' $check_str_mb>MBﾒﾙﾏｶﾞ配信</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	GetTAdcodeInfo($tmm_fk_adcode_id, $tmp_f_adcode, $tmp_fk_admaster_id, $tmp_f_type, $tmp_f_tm_stamp);
	
	$link_ad="<a href='mem_friend_list.php?id=$mid&page=1' target='_blank'>$tmp_f_adcode</a>";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>本人広告ｺｰﾄﾞ</th>\n";
	$dsp_tbl .= "<td>$link_ad</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>紹介者</th>\n";
	$dsp_tbl .= "<td>$parent_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>登録日</th>\n";
	$dsp_tbl .= "<td><input type='text' name='inp_regist' value='$regist_date_str' style = 'width:400px;' $readonly_regd></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>登録元</th>\n";
	$dsp_tbl .= "<td>$regist_pos_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>登録元IP</th>\n";
	$dsp_tbl .= "<td>$ip</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "</table>\n";
        $dsp_tbl .= "</fieldset>\n";





        $dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .= "<legend>コイン設定</legend>\n";
        if($op_level == 8){
        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>購入ｺｲﾝ</th>\n";
	$dsp_tbl .= "<td><input type ='text' name = 'tm_f_coin' value='$tm_f_coin'>&nbsp;コイン</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ｻｰﾋﾞｽｺｲﾝ</th>\n";
	$dsp_tbl .= "<td><input type ='text' name = 'tm_f_free_coin' value='$tm_f_free_coin'>&nbsp;コイン\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>購入合計金額</th>\n";
	$dsp_tbl .= "<td><input type ='text' name = 'tm_f_total_pay' value='$tm_f_total_pay'>&nbsp;円</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>未処理金額</th>\n";
	$dsp_tbl .= "<td><input type ='text' name = 'tmm_f_over_money' value='$tmm_f_over_money'>&nbsp;円</td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "</fieldset>\n";
        $dsp_tbl .= "</table>\n";
        }else{
        $dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>購入ｺｲﾝ</th>\n";
	$dsp_tbl .= "<td>$tm_f_coin&nbsp;&nbsp;コイン</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ｻｰﾋﾞｽｺｲﾝ</th>\n";
	$dsp_tbl .= "<td>$tm_f_free_coin&nbsp;&nbsp;コイン\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>購入合計金額</th>\n";
	$dsp_tbl .= "<td>$tm_f_total_pay&nbsp;&nbsp;円</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>未処理金額</th>\n";
	$dsp_tbl .= "<td>$tmm_f_over_money&nbsp;&nbsp;円</td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "</fieldset>\n";
        $dsp_tbl .= "</table>\n";

        }
        $dsp_tbl .= "</fieldset>\n";




        $dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .= "<legend>商品落札</legend>\n";

        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>未払い商品数</th>\n";
	$dsp_tbl .= "<td><input type ='text' name = 'unpain_num' value='$f_unpain_num'>&nbsp;個</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>キャンセル商品数</th>\n";
	$dsp_tbl .= "<td><input type ='text' name = 'cancel_num' value='$f_cancel_num'>&nbsp;回</td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "</table>\n";
        $dsp_tbl .= "</fieldset>\n";

	//▼配送先表示権限を判定
	if(($staff_address == true) && ($mid != "") && ($mid != 0))
	{
                $dsp_tbl .= "<fieldset>\n";
                $dsp_tbl .= "<legend>会員プロフィール</legend>\n";

                $dsp_tbl .= "<table class = 'form'>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>名前</th>\n";
		$dsp_tbl .= "<td><input type='text' name='f_name' value='$tmm_f_name' style = 'width:150px;'></td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>電話番号</th>\n";
		$dsp_tbl .= "<td><input type='text' name='f_tel_no' value='$tmm_f_tel_no' style = 'width:150px;'></td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>性別</th>\n";
		$dsp_tbl .= "<td>$sex_select</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>誕生日</th>\n";
		$dsp_tbl .= "<td><input type='text' name='inp_birthday' value='$birthday_str' style = 'width:150px;'></td>\n";
		$dsp_tbl .= "</tr>\n";

                $dsp_tbl .= "<tr>\n";
                $dsp_tbl .= "<th>地域</th>\n";
                $dsp_tbl .= "<td>$area_select</td>\n";
                $dsp_tbl .= "</tr>\n";

                $dsp_tbl .= "<tr>\n";
                $dsp_tbl .= "<th>職業</th>\n";
                $dsp_tbl .= "<td>$job_select</td>\n";
                $dsp_tbl .= "</tr>\n";
                
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>配送先住所</th>\n";
		$dsp_tbl .= "<td>&nbsp;$link_address\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
                $dsp_tbl .= "</table>\n";
                $dsp_tbl .= "</fieldset>\n";
	}
                $dsp_tbl .= "</fieldset>\n";

	$dsp_tbl .= "<table border = '0'>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='fk_adcode_id' value='$tmm_fk_adcode_id'>\n";
        $member_delete_authority = member_delete_authority($sid);
	if($member_delete_authority>0){
    	$dsp_tbl .= "<td colspan=2 align=center width = '90%'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='更新'>\n";
	$dsp_tbl .= "</FORM>\n";
//	$member_delete_authority = member_delete_authority($sid);
//	if($member_delete_authority>0){
	$dsp_tbl .= "<FORM action='mem_log.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='ltype' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='履歴'>\n";
	$dsp_tbl .= "</FORM>\n";

	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mem_payment.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";;
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='入金'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
        $dsp_tbl .= "<td align=center width = '10%'>\n";

        $setting = 9999;
        $setting = get_setting($mid);

        if( ($op_level == 8 || $op_level ==2) && $setting == 0 && $mid != 0)
        {
            $js.="<script laguage=\"JavaScript\">
            <!--
            function setting(id,staff)
            {
                myWin = window.open(\"mem_setting.php?id=\"+id+\"&staff=\"+staff, \"_blank\",\"menubar=no,toolbar=no,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=500,height=300\");
            }
            -->
            </script>";
            $dsp_tbl .= "<input type='button' class = 'button1' value='設定' onClick=\"setting(".$mid.",".$sid.")\">\n";
        }
        else if(($op_level == 8 || $op_level ==2) && $setting == 1 && $mid != 0)
        {
            $js.="<script laguage=\"JavaScript\">
            <!--
            function setting_cancel(id,staff)
            {
                myWin = window.open(\"mem_setting_cancel.php?id=\"+id+\"&staff=\"+staff, \"_blank\",\"menubar=no,toolbar=no,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=500,height=300\");
            }
            -->
            </script>";
            $dsp_tbl .= "<input type='button' class = 'button1' value='確認' onClick=\"setting_cancel(".$mid.",".$sid.")\">\n";
        }
        $dsp_tbl .= "</td>\n";
        $dsp_tbl .= "<td align=center width = '10%'>\n";
        $dsp_tbl .= "<FORM action='mem_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
        $dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
        $dsp_tbl .= "<input type='submit' class = 'button1' value='削除'>\n";

        $dsp_tbl .= "</FORM>\n";
        $dsp_tbl .= "</td>\n";
        $dsp_tbl .= "<td><input type='button' class = 'button1' value='閉じる' onClick='window.close();'></td>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        }else{
        $dsp_tbl .= "<td colspan=2 align=center width = '100%'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='更新'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mem_log.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='ltype' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='履歴'>\n";
	$dsp_tbl .= "</FORM>\n";

	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mem_payment.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";;
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='入金'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";

        $setting = 9999;
        $setting = get_setting($mid);

        if( ($op_level == 8 || $op_level ==2) && $setting == 0 && $mid != 0)
        {
            $dsp_tbl .= "<td align=center width = '10%'>\n";
            $js.="<script laguage=\"JavaScript\">
            <!--
            function setting(id,staff)
            {
                myWin = window.open(\"mem_setting.php?id=\"+id+\"&staff=\"+staff, \"_blank\",\"menubar=no,toolbar=no,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=500,height=300\");
            }
            -->
            </script>";
            $dsp_tbl .= "<input type='button' class = 'button1' value='設定' onClick=\"setting(".$mid.",".$sid.")\">\n";
        }
        else if(($op_level == 8 || $op_level ==2) && $setting == 1 && $mid != 0)
        {
            $dsp_tbl .= "<td align=center width = '10%'>\n";
            $js.="<script laguage=\"JavaScript\">
            <!--
            function setting_cancel(id,staff)
            {
                myWin = window.open(\"mem_setting_cancel.php?id=\"+id+\"&staff=\"+staff, \"_blank\",\"menubar=no,toolbar=no,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=500,height=300\");
            }
            -->
            </script>";
            $dsp_tbl .= "<input type='button' class = 'button1' value='確認' onClick=\"setting_cancel(".$mid.",".$sid.")\">\n";
        }

//        $dsp_tbl .= "<td align=center width = '10%'>\n";
//        $dsp_tbl .= "<FORM action='mem_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
//        $dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
//        $dsp_tbl .= "<input type='submit' class = 'button1' value='削除'>\n";
//
//        $dsp_tbl .= "</FORM>\n";
//        $dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</td>\n";

        $dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        }

        $dsp_tbl .= "<br>\n";
	//管理画面入力ページ表示関数
	PrintAdminPage("会員編集",$js.$dsp_tbl);

?>
