<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	//▼質問カテゴリ選択作成
	//質問カテゴリ一覧取得関数
	GetTQuestionCategoryList($tqc_fk_question_category_id,$tqc_f_category_name,$tqc_f_tm_stamp,$tqc_data_cnt);
	$inp_category_id[0] = "";
	$inp_category_name[0] = "(指定しない)";
	for($i=0; $i<$tqc_data_cnt; $i++)
	{
		$inp_category_id[$i+1] = $tqc_fk_question_category_id[$i];
		$inp_category_name[$i+1] = $tqc_f_category_name[$i];
	}
	$inp_category_cnt = $tqc_data_cnt + 1;
	$name = "inp_category";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_category_id, $inp_category_name, $inp_category_cnt, $select_num, $category_select);
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class = 'main_l'>\n";
	$dsp_tbl .= "<caption>&#x2463;問合せ管理</caption>\n";

        $dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='question_list.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>状態</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_flag'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>未読\n";
	$dsp_tbl .= "<OPTION value='1'>既読\n";
	$dsp_tbl .= "<OPTION value='2'>返信済み\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>カテゴリ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$category_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='width:80px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='表示'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</form>\n";

        $dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='que_cate_list.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='width:80px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='ｶﾃｺﾞﾘﾘｽﾄ'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</form>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='que_cate_regist.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qcid' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='width:80px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='ｶﾃｺﾞﾘ追加'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
