<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$qcid = $_REQUEST["qcid"];
	
	if(($qcid != "") && ($qcid != 0))
	{
		//質問カテゴリ名取得関数
		GetTQuestionCategoryName($qcid, $tqc_f_category_name);
		$up_mode = "update";
	}
	else
	{
		$tqc_f_category_name = "";
		$up_mode = "insert";
	}
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='que_cate_list.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> カテゴリ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_category_name' value='$tqc_f_category_name' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='update'>\n";
	$dsp_tbl .= "<input type='hidden' name='qcid' value='$qcid'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='登録'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("グループ編集",$dsp_tbl);

?>
