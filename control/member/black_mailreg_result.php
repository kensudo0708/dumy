<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$bmid = $_REQUEST["bmid"];
//print "sid = [".$sid."]<br>\n";
//print "bmid = [".$bmid."]<br>\n";
	
	$f_mail_address = $_REQUEST["f_mail_address"];
	$f_status = $_REQUEST["f_status"];
	$f_base = $_REQUEST["f_base"];
/*
print "f_mail_address = [".$f_mail_address."]<br>\n";
print "f_status = [".$f_status."]<br>\n";
print "f_base = [".$f_base."]<br>\n";
*/
	
	if($bmid == "")
	{
		print "不正な処理です。<br>\n";
		exit;
	}
	else if($bmid == "0")
	{
		//ブラックメールアドレス登録関数
		$ret = RegistTBMail($f_mail_address,$f_base,$f_status);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "ブラックメールアドレスの新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//ブラックメールアドレス情報更新関数
		$ret = UpdateTBMailInfo($bmid,$f_mail_address,$f_base,$f_status);
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "ブラックメールアドレス情報の更新処理に成功しました。<br>\n";
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("ブラックメールアドレス編集完了",$dsp_tbl);

?>

