<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/05/08												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	
	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
	
	$ltype = $_REQUEST["ltype"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	//会員情報取得関数
	GetTMemberInfo($mid,$tmm_f_login_id,$tmm_f_login_pass,$tmm_f_handle,$tmm_fk_area_id,$tmm_fk_job_id,$tmm_f_activity,$tmm_fk_member_group_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb,$tmm_f_ser_no,$tmm_f_mailmagazein_pc,$tmm_f_mailmagazein_mb,$tmm_fk_adcode_id,$tmm_fk_parent_ad,$tmm_f_regist_date,$tmm_f_regist_pos,$tmm_f_name,$tmm_f_tel_no,$tmm_f_sex,$tmm_f_birthday,$tmm_fk_address_flg,$tmm_f_over_money,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_last_entry,$ip,$a,$b);
	
	//▼名前
	$name_str  = "";
	$name_str .= $tmm_f_login_id;
	$name_str .= "(";
	$name_str .= $tmm_f_handle;
	$name_str .= ")";
	
	//▼会員グループ
	//会員グループ情報取得関数
	GetTMemberGroupInfo($tmm_fk_member_group_id,$tmg_f_member_group_name,$tmg_fk_coin_group_id,$tmg_f_memo,$tmg_f_tm_stamp);
	
	
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	GetSumBuyProd($mid,$sum_prod_pay);
	//▼検索種別
	if($ltype == 0)
	{
		//支払履歴一覧取得関数
//G.Chin 2010-07-12 chg sta
//		GetTPayLogList($mid,$limit,$offset,$tpl_fk_pay_log_id,$tpl_f_status,$tpl_f_staff_id,$tpl_f_pay_money,$tpl_f_coin_add,$tpl_fk_shiharai_type_id,$tpl_f_coin_result,$tpl_f_min_price,$tpl_f_cert_status,$tpl_f_cert_err,$tpl_f_pay_pos,$tpl_f_tm_stamp,$tpl_fk_adcode_id,$tpl_fk_products_id,$tpl_all_count,$tpl_data_cnt);
		GetTPayLogList($mid,$limit,$offset,$tpl_fk_pay_log_id,$tpl_f_status,$tpl_f_staff_id,$tpl_f_pay_money,$tpl_f_coin_add,$tpl_f_free_coin_add,$tpl_fk_shiharai_type_id,$tpl_f_coin_result,$tpl_f_min_price,$tpl_f_cert_status,$tpl_f_cert_err,$tpl_f_pay_pos,$tpl_f_tm_stamp,$tpl_fk_adcode_id,$tpl_fk_products_id,$tpl_all_count,$tpl_data_cnt);
//G.Chin 2010-07-12 chg end
		$all_count = $tpl_all_count;
		$data_cnt = $tpl_data_cnt;
		$submit0 = "<input type='submit' class = 'button1' value='支払記録'>\n";
		$submit1 = "<input type='submit' class = 'button1' value='入札記録'>\n";
	}
	else
	{
		//入札履歴一覧取得関数
		GetTBidLogList($mid,$limit,$offset,$tbl_fk_bid_log_id,$tbl_f_products_id,$tbl_f_price,$tbl_f_bid_type,$tbl_f_coin_type,$tbl_f_tm_stamp,$tbl_all_count,$tbl_data_cnt);
		$all_count = $tbl_all_count;
		$data_cnt = $tbl_data_cnt;
		$submit0 = "<input type='submit' class = 'button1' value='支払記録'>\n";
		$submit1 = "<input type='submit' class = 'button1' value='入札記録'>\n";
	}
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='mem_log.php?offset=$page_offset[$i]&limit=$limit&sid=$sid&mid=$mid&ltype=$ltype'>$num</A> ";
			}
		}
	}
	
	if($all_count > $limit)
	{
		$page_first = "<A href='mem_log.php?offset=0&limit=$limit&sid=$sid&mid=$mid&ltype=$ltype'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='mem_log.php?offset=$page_offset[$max_page]&limit=$limit&sid=$sid&mid=$mid&ltype=$ltype'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='mem_log.php?offset=$next_page&limit=$limit&sid=$sid&mid=$mid&ltype=$ltype'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='mem_log.php?offset=$before_page&limit=$limit&sid=$sid&mid=$mid&ltype=$ltype'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "mem_log.php?sid=$sid&mid=$mid&ltype=$ltype&limit=$limit&offset=";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='mem_log.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='sid' value='$sid'>";
		$form_str .= "<input type='hidden' name='mid' value='$mid'>";
		$form_str .= "<input type='hidden' name='ltype' value='$ltype'>";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
//	$dsp_tbl .= "</center>\n";
	
	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>履歴表示</legend>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>会員</th>\n";
	$dsp_tbl .= "<td style = 'width:120px;'>$name_str</td>\n";
	$dsp_tbl .= "<td style = 'width:200px;'>$tmg_f_member_group_name</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>コイン購入支払合計</th>\n";
	$dsp_tbl .= "<td>".number_format($tm_f_total_pay)."円</td>\n";
	$dsp_tbl .= "<td>購入コイン($tm_f_coin 枚)&nbsp;フリーコイン($tm_f_free_coin 枚)</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	//2010-06-28 shoji end
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>商品購入支払合計</th>\n";
	$dsp_tbl .= "<td>".number_format($sum_prod_pay)."円</td>\n";
	$dsp_tbl .= "<td colspan=2></td>\n";
	$dsp_tbl .= "</tr>\n";
	//2010-06-28 shoji start
	
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "<br>\n";

	$dsp_tbl .= "<table border = '0' style = 'margin-left:0px;'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mem_log.php' method='GET'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='ltype' value='0'>\n";
	$dsp_tbl .= $submit0;
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "</td>\n";
	
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mem_log.php' method='GET'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='ltype' value='1'>\n";
	$dsp_tbl .= $submit1;
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</table>\n";
        $dsp_tbl .= "</div>\n";
	$dsp_tbl .= "<br>\n";
	
	
	//▼表示開始～終了番号
	if($all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $data_cnt;
	
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$all_count</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<p style = 'margin-left:7px;'>$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目</p>\n";
//	$dsp_tbl .= "<br>\n";
	
	if($ltype == 0)
	{
		//■支払履歴
		$dsp_tbl .= "<table class = 'list'>\n";
		$dsp_tbl .= "<tr bgcolor='#CCCCFF' style = 'height:25px;'>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>日時</th>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>操作</th>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>支払方法</th>\n";
		$dsp_tbl .= "<th style = 'width:80px;'>支払額</th>\n";
		$dsp_tbl .= "<th style = 'width:70px;'>コイン枚数</th>\n";
		$dsp_tbl .= "<th style = 'width:90px;'>ﾌﾘｰｺｲﾝ枚数</th>\n";
		$dsp_tbl .= "<th style = 'width:70px;'>購入後枚数</th>\n";
		$dsp_tbl .= "<th style = 'width:120px;'>対象商品</th>\n";
		$dsp_tbl .= "</tr>\n";
		
		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$data_cnt;$i++)
		{
			//▼日時
			$tm_stamp_str = substr($tpl_f_tm_stamp[$i], 0, 19);
			
			//▼操作
			switch($tpl_f_status[$i])
			{
				case 0:
						$status_str = "ｺｲﾝ購入";
						$money_str = number_format($tpl_f_pay_money[$i])."円";
						$coin_str = "$tpl_f_coin_add[$i] 枚";
						$free_str = "$tpl_f_free_coin_add[$i] 枚";	//G.Chin 2010-07-12 add
						$buy_str = "$tpl_f_coin_result[$i] 枚";
						break;
				case 1:
						$status_str = "商品購入";
						$money_str = number_format($tpl_f_pay_money[$i])."円";
						$coin_str = "";
						$free_str = "";	//G.Chin 2010-07-12 add
						$buy_str = "";
						break;
				case 2:
						$status_str = "ﾌﾘｰｺｲﾝ追加";
						$money_str = "";
						$coin_str = "$tpl_f_coin_add[$i] 枚";
						$free_str = "$tpl_f_free_coin_add[$i] 枚";	//G.Chin 2010-07-12 add
						$buy_str = "$tpl_f_coin_result[$i] 枚";
						break;
//G.Chin 2010-07-26 add sta
				case 3:
						$status_str = "ｺｲﾝﾊﾟｯｸ購入";
						$money_str = "";
						$coin_str = "$tpl_f_coin_add[$i] 枚";
						$free_str = "$tpl_f_free_coin_add[$i] 枚";
						$buy_str = "$tpl_f_coin_result[$i] 枚";
						break;
//G.Chin 2010-07-26 add end
				case 5:
						$status_str = "ゲーム配当";
						$money_str = "";
						$coin_str = "$tpl_f_coin_add[$i] 枚";
						$free_str = "$tpl_f_free_coin_add[$i] 枚";
						$buy_str = "$tpl_f_coin_result[$i] 枚";
						break;
			}
			
//G.Chin AWKT-462 2010-09-07 chg sta
/*
			//支払履歴検索支払方法名取得関数
			SrcPayLogGetFShiharaiName($tpl_fk_shiharai_type_id[$i], $tsp_f_shiharai_name);
*/
			//▼支払方法
			if($tpl_f_status[$i] == 3)
			{
				//"コインパック購入"の場合、支払方法は不要
				$tsp_f_shiharai_name = "";
			}
			else
			{
				//支払履歴検索支払方法名取得関数
				SrcPayLogGetFShiharaiName($tpl_fk_shiharai_type_id[$i], $tsp_f_shiharai_name);
			}
//G.Chin AWKT-462 2010-09-07 chg end
			
			//オークション終了商品名取得関数
			GetTEndProductsName($tpl_fk_products_id[$i], $tep_f_products_name);
			
			//テンプレート写真番号
			$tmppht_next = "";
			for($j=0; $j<5; $j++)
			{
				$tmppht_next .= "&tmppht[]=";
			}
			//▼各種リンク
			$link_product = "<A href='../product/product_regist.php?pid=$tpl_fk_products_id[$i]&mode=&photo_num=&tmpid=$tmppht_next' target='_blank'>$tep_f_products_name</A>";
			
			$dsp_tbl .= "<tr bgcolor='#FFFFFF' style = 'height:25px;'>\n";
			$dsp_tbl .= "<td align='center'>$tm_stamp_str</td>\n";
			$dsp_tbl .= "<td align='center'>$status_str</td>\n";
			$dsp_tbl .= "<td align='center'>$tsp_f_shiharai_name</td>\n";
			$dsp_tbl .= "<td align='right'>$money_str</td>\n";
			$dsp_tbl .= "<td align='center'>$coin_str</td>\n";
			$dsp_tbl .= "<td align='center'>$free_str</td>\n";
			$dsp_tbl .= "<td align='center'>$buy_str</td>\n";
			$dsp_tbl .= "<td align='left'>$link_product</td>\n";
			$dsp_tbl .= "</tr>\n";
		}
		
		$dsp_tbl .= "</table><br>\n";
	}
	else
	{
		//■入札履歴
		$dsp_tbl .= "<table class = 'list' style = 'height:25px;'>\n";
		$dsp_tbl .= "<tr bgcolor='#CCCCFF'>\n";
		$dsp_tbl .= "<th style = 'width:100px;'>日時</th>\n";
		$dsp_tbl .= "<th style = 'width:300px;'>商品</th>\n";
		$dsp_tbl .= "<th style = 'width:80px;'>価格</th>\n";
		$dsp_tbl .= "<th style = 'width:70px;'>入札方法</th>\n";
		$dsp_tbl .= "<th style = 'width:80px;'>コインタイプ</th>\n";
		$dsp_tbl .= "</tr>\n";
		
		//取得データより表示データを１件ずつ取り出す
		for($i=0;$i<$data_cnt;$i++)
		{
			//▼日時
			$tm_stamp_str = substr($tbl_f_tm_stamp[$i], 0, 19);
			
			//▼入札方法
			switch($tbl_f_bid_type[$i])
			{
				case 0:	$bid_type_str = "手動";	break;
				case 1:	$bid_type_str = "自動";	break;
				case 2:	$bid_type_str = "手動 2";	break;
			}
			
			//▼コインタイプ
			if($tbl_f_coin_type[$i] == 0)
			{
				$coin_type_str = "フリーコイン";
			}
			else
			{
				$coin_type_str = "購入コイン";
			}
			
			//入札商品名取得関数
			GetBidProductsName($tbl_f_products_id[$i], $tep_f_products_name);
			
                        $link_product ="";
			//▼各種リンク
                        if( DISPLAY_BIT_LOG == 0)
                        {
                            $link_product = "<A href='../product/product_regist.php?pid=$tbl_f_products_id[$i]&mode=&photo_num=&tmpid=' target='_blank'>$tep_f_products_name</A>";
                        }
                        else
                        {
                            $link_product = "<A href='../product/product_bit_log.php?pid=$tbl_f_products_id[$i]&page=1' target='_blank'>$tep_f_products_name</A>";
                        }
			$dsp_tbl .= "<tr bgcolor='#FFFFFF'>\n";
			$dsp_tbl .= "<td align='center'>$tm_stamp_str</td>\n";
			$dsp_tbl .= "<td align='left'>$link_product</td>\n";
			$dsp_tbl .= "<td align='center'>$tbl_f_price[$i] 円</td>\n";
			$dsp_tbl .= "<td align='center'>$bid_type_str</td>\n";
			$dsp_tbl .= "<td align='center'>$coin_type_str</td>\n";
			$dsp_tbl .= "</tr>\n";
		}
		
		$dsp_tbl .= "</table><br>\n";
	}

        $dsp_tbl .= "</fieldset>\n";

	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "<span style = 'margin-left:5px;'><input type='button' class = 'button1' value='閉じる' onClick='window.close();'></span>\n";
        $dsp_tbl .= "<br>\n";
	//管理画面入力ページ表示関数
	PrintAdminPage("履歴表示",$dsp_tbl);

?>
