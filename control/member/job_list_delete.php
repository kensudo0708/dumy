<?php
    /*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
    /*																				*/
    /*		作成者		:	G.Li													*/
    /*		作成日		:	2010/09/14												*/
    /*		修正日		:															*/
    /*																				*/
    /*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
    //☆★	ライブラリ読込み	★☆
    include "../../lib/define.php";
    $all_include_path = COMMON_LIB."all_include_lib.php";
    include $all_include_path;

    $utils_class = HOME_PATH."mvc/utils/Utils.class.php";
    include $utils_class;

    session_start();
    $sid = $_SESSION["staff_id"];

    $status = $_REQUEST["status"];
    GetMemJobList($fk_job_id, $f_name, $f_status,$fk_all_cnt,$status,$fk_data_cnt);

    $dsp_tbl="";
    $dsp_tbl .= "<table class = 'data'>\n";
    $dsp_tbl .= "<tr>\n";
    $dsp_tbl .= "<th>データ件数</th>\n";
    $dsp_tbl .= "<td>$fk_all_cnt</td>\n";
    $dsp_tbl .= "</tr>\n";
    $dsp_tbl .= "</table>\n<br>\n";

    $dsp_tbl .= "<table class = 'list'>\n";
    $dsp_tbl .= "<tr style = 'height:25px;'>\n";
    $dsp_tbl .= "<th style = 'width:70px;'>職業ID</th>\n";
    $dsp_tbl .= "<th style = 'width:150px;'>職業名前</th>\n";
    $dsp_tbl .= "<th style = 'width:70px;'>状態</th>\n";
    $dsp_tbl .= "<th style = 'width:70px;'>操作</th>\n";
    $dsp_tbl .= "</tr>\n";
    //取得データより表示データを１件ずつ取り出す
    for($i=0;$i<$fk_data_cnt;$i++) {
        switch($f_status[$i]) {
            case 0:		$status_str = "使用中";
                break;
            case 9:		$status_str = "未使用";
                break;
        }

        //▼登録・編集画面へのリンク
        $link_reg = "<A href='mem_job_regist.php?job_id=$fk_job_id[$i]' target='_blank'>編集</A>";
        $link_reg2 = " ";

        $dsp_tbl .= "<tr style = 'height:25px;'>\n";
        $dsp_tbl .= "<td align='center'>$fk_job_id[$i]</td>\n";
        $dsp_tbl .= "<td align='center'>$f_name[$i]</td>\n";
        $dsp_tbl .= "<td align='center'>$status_str</td>\n";
        $dsp_tbl .= "<td align='center'>$link_reg</td>\n";
        $dsp_tbl .= "</tr>\n";
    }

    $dsp_tbl .= "</table><br>\n";

    //管理画面入力ページ表示関数
    PrintAdminPage("職業編集",$dsp_tbl);

    ?>
