<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;

	session_start();
	$sid = $_SESSION["staff_id"];

        if($sid == NULL)
        {
           $dsp_tbl = "セッションが終了しています。再読み込みしてください。";

            //管理画面入力ページ表示関数
            PrintAdminPage("会員管理",$dsp_tbl);
            return ;
        }

        $mid=$_REQUEST['mid'];
        $mode=$_REQUEST['mode'];
        $sex = $_REQUEST['sex'];
        if($mode =='check')
        {
            $dsp_tbl="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
            $dsp_tbl .= "<html lang='utf-8'>\n";
            $dsp_tbl .="<head>\n";
            $dsp_tbl .="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
            $dsp_tbl .="<title>親コードデータ削除</title>\n";
            $dsp_tbl .="</head>\n";
            $dsp_tbl .="<body bgcolor='FF3300'>\n";
            $dsp_tbl.='<h3>本操作は一度行うと復旧できません。<BR>';
            $dsp_tbl .='実行してよろしいですか？<h3>';
            $dsp_tbl .="<table>\n
            <tr>\n
            <td>\n
            <form method='POST' action='parent_delete.php'>\n
            <input type='hidden' name='mid' value='$mid'>\n
            <input type='hidden' name='sex' value='$sex'>\n
            <input type ='hidden' name='mode' value='del'>\n
            <input type=\"submit\" style=\"background-color:#FFFFFF; color:#0000FF; font-size:9pt; width:100; border:1 solid #A4FFA4;\" value='はい'>\n
            </form>\n
            </td>\n
            <td>\n<form>
            <input type=\"button\" style=\"background-color:#FFFFFF; color:#0000FF; font-size:9pt; width:100; border:1 solid #A4FFA4;\" value='いいえ' onclick='window.close()'>\n
            </form></td>\n
            </tr></table>";
            $dsp_tbl .= "</BODY>\n";
            $dsp_tbl .= "</HTML>\n";
            echo $dsp_tbl;
            return;
        }
        else
        {
            $ret =0;
            $ret=DelParAd($mid,$sex);
            if($ret == 0)
            {
                $dsp_tbl='<P>削除に成功しました。</P>';
            }
            else if($ret == 1)
            {
                $dsp_tbl='<P>削除に失敗しました。</P>';
            }
            else if($ret == 2)
            {
                $dsp_tbl='<P>親コードは既に削除済みまたは最初からありません</P>';
            }
            $dsp_tbl .= "<br><br><br>\n";
            $dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
            $dsp_tbl .= "<br><br>\n";
            $dsp_tbl .= "<form>\n";
            $dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
            $dsp_tbl .= "</form>\n";
        }
        PrintAdminPage("親コードデータ削除",$dsp_tbl);
?>
