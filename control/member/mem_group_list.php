<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<script type="text/javascript">
    /**
     * 確認ダイアログの返り値によりフォーム送信
    */
<!--
	function submitChk ()
	{
		/* 確認ダイアログ表示 */
		var flag = confirm ('指定したグループを削除します。このグループに属する会員はすべて基本グループになります。よろしいですか？');
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	$up_mode = $_REQUEST["up_mode"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	//更新モード
	if($up_mode == "insert")
	{
		$f_member_group_name = $_REQUEST["f_member_group_name"];
		$fk_coin_group_id = $_REQUEST["fk_coin_group_id"];
		$f_memo = $_REQUEST["f_memo"];
		
		//会員グループ情報登録関数
		RegistTMGroupInfo($f_member_group_name,$fk_coin_group_id,$f_memo);
	}
	else if($up_mode == "delete")
	{
		$gid = $_REQUEST["gid"];
		
		//会員グループ情報削除関数
		DeleteTMGroupInfo($gid);
	}
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//会員グループ一覧取得関数
	GetTMGroupList($limit,$offset,$tmg_fk_member_group_id,$tmg_f_member_group_name,$tmg_fk_coin_group_id,$tmg_f_memo,$tmg_f_tm_stamp,$tmg_all_count,$tmg_data_cnt);
	
	//▼表示開始～終了番号
	if($tmg_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tmg_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tmg_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='mem_group_list.php?offset=$page_offset[$i]&limit=$limit&up_mode='>$num</A> ";
			}
		}
	}
	
	if($tmg_all_count > $limit)
	{
		$page_first = "<A href='mem_group_list.php?offset=0&limit=$limit&up_mode='>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='mem_group_list.php?offset=$page_offset[$max_page]&limit=$limit&up_mode='>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tmg_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='mem_group_list.php?offset=$next_page&limit=$limit&up_mode='>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='mem_group_list.php?offset=$before_page&limit=$limit&up_mode='>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "mem_group_list.php?limit=$limit&offset=&up_mode=";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='mem_group_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "<input type='hidden' name='up_mode' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
//	$dsp_tbl .= "</center>\n";
	
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$tmg_all_count</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:50px;'>ID</th>\n";
	$dsp_tbl .= "<th style = 'width:180px;'>グループ名</th>\n";
	$dsp_tbl .= "<th style = 'width:150px;'>コイン設定状態</th>\n";
	$dsp_tbl .= "<th style = 'width:150px;'>メモ</th>\n";
	$dsp_tbl .= "<th style = 'width:80px;'>操作</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tmg_data_cnt;$i++)
	{
		//▼基本グループ名
		if($i == 0)
		{
			$base_group = $tmg_f_member_group_name[$i];
		}
		
		//コイン売却価格グループ情報取得関数
		GetTCoinGroupInfo($tmg_fk_coin_group_id[$i],$tcg_f_coin_group_name,$tcg_f_memo,$tcg_f_status,$tcg_f_tm_stamp);
		
		//▼各種リンク
		$link_edit = "<A href='mem_group_edit.php?sid=$sid&gid=$tmg_fk_member_group_id[$i]' target='_blank'>編集</A>";
		$link_delete = "<A href='mem_group_list.php?sid=$sid&gid=$tmg_fk_member_group_id[$i]&limit=$limit&offset=$offset&page=$page&up_mode=delete' onClick=\"return submitChk()\">削除</A>";
		
		$link_str  = "";
		$link_str .= $link_edit;
		//基本グループには"削除"リンク無し
		if($i == 0)
		{
			$link_str .= "&nbsp;";
			$link_str .= "&nbsp;";
			$link_str .= "&nbsp;";
			$link_str .= "&nbsp;";
			$link_str .= "&nbsp;";
		}
		else
		{
			$link_str .= "&nbsp;";
			$link_str .= $link_delete;
		}
		
		$dsp_tbl .= "<tr style = 'height:25px;'>\n";
		$dsp_tbl .= "<td>$tmg_fk_member_group_id[$i]</td>\n";
		$dsp_tbl .= "<td><tt>$tmg_f_member_group_name[$i]</td>\n";
		$dsp_tbl .= "<td>$tcg_f_coin_group_name</td>\n";
		$dsp_tbl .= "<td>$tmg_f_memo[$i]</td>\n";
		$dsp_tbl .= "<td>$link_str</td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "<br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("会員管理",$dsp_tbl);

?>
