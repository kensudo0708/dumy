<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/10												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$strbuilder_class = HOME_PATH."mvc/utils/StringBuilder.class.php";
	include $strbuilder_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	$mid = $_REQUEST["mid"];
	$op_level = $_SESSION['op_level'];
//print "mid = [".$mid."]<br>\n";
	
	$f_login_id = $_REQUEST["f_login_id"];
	$inp_pass = $_REQUEST["inp_pass"];
	$up_login_pass = $_REQUEST["up_login_pass"];
	$f_handle = $_REQUEST["f_handle"];
	$fk_area_id = $_REQUEST["fk_area_id"];
	$fk_job_id = $_REQUEST["fk_job_id"];
	$f_activity = $_REQUEST["f_activity"];
	$fk_member_group_id = $_REQUEST["fk_member_group_id"];
	$f_mail_address_pc = $_REQUEST["f_mail_address_pc"];
	$f_mail_address_mb = $_REQUEST["f_mail_address_mb"];
	$f_mailmagazein_pc = $_REQUEST["f_mailmagazein_pc"];
	$f_mailmagazein_mb = $_REQUEST["f_mailmagazein_mb"];
	$inp_regist = $_REQUEST["inp_regist"];
	$f_name = $_REQUEST["f_name"];
	$f_tel_no = $_REQUEST["f_tel_no"];
	$f_sex = $_REQUEST["f_sex"];
	$inp_birthday = $_REQUEST["inp_birthday"];
	$f_memo = $_REQUEST["f_memo"];
	$fk_adcode_id = $_REQUEST["fk_adcode_id"];
	$staff_mail = $_REQUEST["staff_mail"];
	$staff_address = $_REQUEST["staff_address"];
	$f_unpain_num = $_REQUEST['unpain_num'];
	$f_cancel_num = $_REQUEST['cancel_num'];
	$tm_f_coin = $_REQUEST['tm_f_coin'];
	$tm_f_free_coin = $_REQUEST['tm_f_free_coin'];
	$tm_f_total_pay = $_REQUEST['tm_f_total_pay'];
	$tmm_f_over_money = $_REQUEST['tmm_f_over_money'];
	
	
	if($f_unpain_num < 0 || $f_cancel_num < 0)
	{
	    PrintAdminPage("会員編集失敗","<P>商品落札関連の値が不正です</P>");
	    exit;
	}
	
	if(ereg("'",$f_handle) || ereg("\'",$f_handle) || ereg(";",$f_handle))
	{
		$dsp_tbl="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>";
		$dsp_tbl .="<P>ハンドル名に『'』『;』は入力できません";
		$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<form>\n";
		$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
		$dsp_tbl .= "</form>\n";
		$dsp_tbl .="</body></html>";
		PrintAdminPage("会員編集失敗",$dsp_tbl);
		return;
	}
	
	//ハンドル名が存在するかどうかチェック関数
//G.Chin AWKT-875 2010-12-15 chg sta
/*
	$rows='';
	isExistedHandle($f_handle,$mid,$rows);
	if($rows!=0){
*/
	$rows = 0;
	CheckHandleExist($f_handle,$mid,$rows);
	if($rows > 0)
	{
//G.Chin AWKT-875 2010-12-15 chg end
		$dsp_tbl  = "<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body　bgcolor='#FF0000'>";
		$dsp_tbl .= "<P>入力されたハンドル名が存在しています。ほかのハンドル名を入力してください。";
		$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<form>\n";
		$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
		$dsp_tbl .= "</form>\n";
		$dsp_tbl .= "</body></html>";
		PrintAdminPage("会員編集失敗",$dsp_tbl);
		return;
	}
/*
print "f_login_id = [".$f_login_id."]<br>\n";
print "inp_pass = [".$inp_pass."]<br>\n";
print "up_login_pass = [".$up_login_pass."]<br>\n";
print "f_handle = [".$f_handle."]<br>\n";
print "f_activity = [".$f_activity."]<br>\n";
print "fk_member_group_id = [".$fk_member_group_id."]<br>\n";
print "f_mail_address_pc = [".$f_mail_address_pc."]<br>\n";
print "f_mail_address_mb = [".$f_mail_address_mb."]<br>\n";
print "f_mailmagazein_pc = [".$f_mailmagazein_pc."]<br>\n";
print "f_mailmagazein_mb = [".$f_mailmagazein_mb."]<br>\n";
print "inp_regist = [".$inp_regist."]<br>\n";
print "f_name = [".$f_name."]<br>\n";
print "f_tel_no = [".$f_tel_no."]<br>\n";
print "f_sex = [".$f_sex."]<br>\n";
print "inp_birthday = [".$inp_birthday."]<br>\n";
*/
	//▼登録日
	$f_regist_date = $inp_regist.":00";
	
	//▼誕生日
	//日付文字列作成関数
	MakeDateString($inp_birthday, $w_birthday);
	$f_birthday = $w_birthday." 00:00:00";
	
	if($mid == "")
	{
		PrintAdminPage("会員編集失敗","<P>不正な処理です</P>");
		//print "不正な処理です。<br>\n";
		exit;
	}
	else if($mid == "0")
	{
		//▼登録元(STAFF)
		$f_regist_pos = 2;
		
		//▼会員広告コードを作成
		$w_adcode = StringBuilder::uuid();
		//会員本人広告コード登録関数
		$ret = RegistMemSelfTAdcode($w_adcode,$fk_adcode_id);
		if($ret == false)
		{
			print "会員本人広告コード登録に失敗しました。<br>\n";
			exit;
		}
		
		//会員情報登録関数
		$ret = RegistTMemberData($f_login_id,$inp_pass,$f_handle,$f_activity,$fk_member_group_id,$f_mail_address_pc,$f_mail_address_mb,$f_mailmagazein_pc,$f_mailmagazein_mb,$f_name,$f_tel_no,$f_sex,$f_birthday,$fk_adcode_id,$f_regist_pos,$f_memo);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "会員の新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		$dsp_string  = "";
		
		//会員情報更新関数
		$ret = UpdateTMemberInfo($staff_mail,$staff_address,$mid,$f_login_id,$inp_pass,$f_handle,$fk_area_id,$fk_job_id,$f_activity,$fk_member_group_id,$f_mail_address_pc,$f_mail_address_mb,$f_mailmagazein_pc,$f_mailmagazein_mb,$f_regist_date,$f_name,$f_tel_no,$f_sex,$f_birthday,$f_unpain_num,$f_cancel_num,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tmm_f_over_money,$op_level);
		if($ret == false)
		{
			$dsp_string .= "更新処理に失敗しました。<br>\n";
		}
		else
		{
			//会員メモ更新関数
			UpdateTMemberMemo($mid,$f_memo);
			$dsp_string .= "会員情報の更新処理に成功しました。<br>\n";
		}
		
		//▼パスワード変更判定
		if($up_login_pass == 1)
		{
			//会員パスワード設定関数
			$ret = SetTMemberLoginPass($mid, $inp_pass);
			if($ret == false)
			{
				$dsp_string .= "パスワード更新処理に失敗しました。<br>\n";
			}
			else
			{
				$dsp_string .= "パスワードの更新処理に成功しました。<br>\n";
			}
		}
		
		//▼会員広告コードの有無を判定
//print "fk_adcode_id = ".$fk_adcode_id."<br>\n";
		if($fk_adcode_id == "")
		{
			//▼会員広告コードを作成
			$w_adcode = StringBuilder::uuid();
			//会員本人広告コード登録関数
			RegistMemSelfTAdcode($w_adcode,$fk_adcode_id);
			if($fk_adcode_id != "")
			{
				//会員広告コード設定関数
				SetMemberFkAdcodeId($mid,$fk_adcode_id);
			}
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("会員編集完了",$dsp_tbl);

?>
