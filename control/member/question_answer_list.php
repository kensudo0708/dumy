<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/08/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	$sid = $_REQUEST["sid"];
	$qid = $_REQUEST["qid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	$inp_flag = $_REQUEST["inp_flag"];
	$inp_category = $_REQUEST["inp_category"];
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//質問回答一覧取得関数
	GetTQuestionAnswerList($limit,$offset,$qid,$tqa_fk_question_answer_id,$tqa_f_subject,$tqa_f_main,$tqa_f_name,$tqa_f_staff_id,$tqa_f_tm_stamp,$tqa_all_count,$tqa_data_cnt);
	
	//▼表示開始～終了番号
	if($tqa_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tqa_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tqa_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
			}
		}
	}
	
	if($tqa_all_count > $limit)
	{
		$page_first = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=0&limit=$limit'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$page_offset[$max_page]&limit=$limit'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tqa_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$next_page&limit=$limit'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$before_page&limit=$limit'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&up_mode=&qid=&f_flag=&limit=$limit&offset=";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='question_answer_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='sid' value='$sid'>";
		$form_str .= "<input type='hidden' name='mid' value='$mid'>";
		$form_str .= "<input type='hidden' name='ltype' value='$ltype'>";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
//	$dsp_tbl .= "</center>\n";
	
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style= 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style= 'width:50px;'>$tqa_all_count</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style= 'width:50px;'>No</th>\n";
	$dsp_tbl .= "<th style= 'width:50px;'>質問ID</th>\n";
	$dsp_tbl .= "<th style= 'width:150px;'>タイムスタンプ</th>\n";
	$dsp_tbl .= "<th style= 'width:150px;'>回答者</th>\n";
	$dsp_tbl .= "<th style= 'width:150px;'>タイトル</th>\n";
        $dsp_tbl .= "<th style= 'width:250px;'>本文</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tqa_data_cnt;$i++)
	{
		//▼ﾀｲﾑｽﾀﾝﾌﾟ
		//$tm_stamp_str = substr($tqa_f_tm_stamp[$i], 0, 10);
		$tm_stamp_str = $tqa_f_tm_stamp[$i];
		
		//▼各種リンク
		$link_info = "<A href='question_answer_info.php?sid=$sid&qid=$qid&qaid=$tqa_fk_question_answer_id[$i]&inp_flag=$inp_flag&inp_category=$inp_category&offset=$offset&limit=$limit&page=$page'>".mb_substr($tqa_f_main[$i],0,19)."</A>";
		
		$bgcolor = "#FFFFFF";
		
		$dsp_tbl .= "<tr bgcolor='$bgcolor' style = 'height:25px;'>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tqa_fk_question_answer_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$qid</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tm_stamp_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left'><NOBR><tt>$tqa_f_name[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left'><NOBR><tt>$tqa_f_subject[$i]</tt></NOBR></td>\n";
                $dsp_tbl .= "<td align='center'><NOBR><tt>$link_info</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "<br>\n";

	$dsp_tbl .= "<div style = 'text-align:left; padding-left:5px;'>\n";
        $dsp_tbl .= "<input type='button' value='戻る' class = 'button1' onClick='history.back()'>\n";
	$dsp_tbl .= "</div><br>\n";
	//管理画面入力ページ表示関数
	PrintAdminPage("問合せ回答",$dsp_tbl);

?>
