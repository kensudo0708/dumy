<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/11												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;


	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];

	if(($mid != "") && ($mid != 0))
	{
		//更新モード
		$aid = $_REQUEST["aid"];
		$up_mode = $_REQUEST["up_mode"];
		if($up_mode == "insert")
		{
			$f_name = $_REQUEST["f_name"];
			$f_tel_no = $_REQUEST["f_tel_no"];
			$f_post_code = $_REQUEST["f_post_code"];
			$fk_perf_id = $_REQUEST["fk_perf_id"];
			$f_address1 = $_REQUEST["f_address1"];
			$f_address2 = $_REQUEST["f_address2"];
			$f_address3 = $_REQUEST["f_address3"];

			//会員住所情報登録関数
			RegistTAddressInfo($mid,$f_name,$f_tel_no,$f_post_code,$fk_perf_id,$f_address1,$f_address2,$f_address3);
		}
		else if($up_mode == "update")
		{
			$f_name = $_REQUEST["f_name"];
			$f_tel_no = $_REQUEST["f_tel_no"];
			$f_post_code = $_REQUEST["f_post_code"];
			$fk_perf_id = $_REQUEST["fk_perf_id"];
			$f_address1 = $_REQUEST["f_address1"];
			$f_address2 = $_REQUEST["f_address2"];
			$f_address3 = $_REQUEST["f_address3"];

			//会員住所情報更新関数
			UpdateTAddressInfo($aid,$f_name,$f_tel_no,$f_post_code,$fk_perf_id,$f_address1,$f_address2,$f_address3);
		}
		else if($up_mode == "delete")
		{
			//会員住所情報削除関数
			DeleteTAddressInfo($aid);
		}

		//会員情報取得関数
		GetTMemberInfo($mid,$tmm_f_login_id,$tmm_f_login_pass,$tmm_f_handle,$tmm_fk_area_id,$tmm_fk_job_id,$tmm_f_activity,$tmm_fk_member_group_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb,$tmm_f_ser_no,$tmm_f_mailmagazein_pc,$tmm_f_mailmagazein_mb,$tmm_fk_adcode_id,$tmm_fk_parent_ad,$tmm_f_regist_date,$tmm_f_regist_pos,$tmm_f_name,$tmm_f_tel_no,$tmm_f_sex,$tmm_f_birthday,$tmm_fk_address_flg,$tmm_f_over_money,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_last_entry,$ip,$a,$b);

		//会員住所一覧取得関数
		GetTAddressList($mid,$ta_fk_address_id,$ta_f_status,$ta_f_last_send,$ta_f_name,$ta_f_tel_no,$ta_f_post_code,$ta_fk_perf_id,$ta_f_address1,$ta_f_address2,$ta_f_address3,$ta_f_tm_stamp,$ta_data_cnt);
	}
	else
	{
		//現在日時
		$now = time();

		$tmm_f_login_id			= "";
		$tmm_f_login_pass		= "";
		$tmm_f_handle			= "";
		$tmm_f_activity			= "";
		$tmm_fk_member_group_id	= "";
		$tmm_f_mail_address_pc	= "";
		$tmm_f_mail_address_mb	= "";
		$tmm_f_ser_no			= "";
		$tmm_f_mailmagazein_pc	= "";
		$tmm_f_mailmagazein_mb	= "";
		$tmm_fk_adcode_id		= "";
		$tmm_fk_parent_ad		= "";
		$tmm_f_regist_date		= date("Y-m-d H:i:00", $now);
		$tmm_f_regist_pos		= "2";
		$tmm_f_name				= "";
		$tmm_f_tel_no			= "";
		$tmm_f_sex				= "";
		$tmm_f_birthday			= "";
		$tmm_fk_address_flg		= "0";
		$tmm_f_over_money		= "0";
		$tm_f_coin				= "0";
		$tm_f_free_coin			= "0";
		$tm_f_total_pay			= "0";

		$ta_data_cnt			= 0;
	}

	//▼会員IDハンドル名
	$member_id_str  = "";
	$member_id_str .= $tmm_f_login_id;
	$member_id_str .= "(";
	$member_id_str .= $tmm_f_handle;
	$member_id_str .= ")";


	$dsp_tbl  = "";
	$dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .= "<legend>配送先住所</legend>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>会員IDハンドル名</th>\n";
	$dsp_tbl .= "<td style = 'width:120px;'>$member_id_str</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>会員電話番号</th>\n";
	$dsp_tbl .= "<td>$tmm_f_tel_no</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";

//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th>配送先住所</th>\n";
//	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "<br>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	for($i=0; $i<$ta_data_cnt; $i++)
	{
		$num = $i + 1;
		$address_num = "住所".$num;

		$post_before = substr($ta_f_post_code[$i], 0, 3);
		$post_after = substr($ta_f_post_code[$i], 3, 4);
		$post_str  = "";
		$post_str .= "〒";
		$post_str .= $post_before;
		$post_str .= "-";
		$post_str .= $post_after;

		//都道府県名取得関数
		GetTPrefName($ta_fk_perf_id[$i], $tp_t_pref_name);

		$address_str  = "";
		$address_str .= $post_str;
		$address_str .= " ";
		$address_str .= $tp_t_pref_name;
		$address_str .= $ta_f_address1[$i];
		$address_str .= $ta_f_address2[$i];
		$address_str .= " ";
		$address_str .= $ta_f_address3[$i];
 


		$dsp_tbl .= "<tr style = 'height:25px;'>\n";
		$dsp_tbl .= "<th style = 'width:80px;'>$address_num</th>\n";
      		$dsp_tbl .= "<td style = 'width:350px;'>$address_str</td>\n";
		$dsp_tbl .= "<FORM action='mem_address_regist.php' method='POST' ENCTYPE='multipart/form-data'>\n";
		$dsp_tbl .= "<input type='hidden' name='aid' value='$ta_fk_address_id[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
		$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
		$dsp_tbl .= "<td style = 'width:50px;'><input type='submit' class = 'button2' style = 'width:40px;' value='編集'></td>\n";
		$dsp_tbl .= "</FORM>\n";
		$dsp_tbl .= "<FORM action='mem_address_delete.php' method='POST' ENCTYPE='multipart/form-data'>\n";
		$dsp_tbl .= "<input type='hidden' name='aid' value='$ta_fk_address_id[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
		$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
		$dsp_tbl .= "<td style = 'width:50px;'><input type='submit' class = 'button2' style = 'width:40px;' value='削除'></td>\n";
		$dsp_tbl .= "</FORM>\n";
		$dsp_tbl .= "</tr>\n";
	}
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";

//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<td colspan=4 align=center>\n";
       	$dsp_tbl .= "<div style = 'text-align:center;'>\n";
	$dsp_tbl .= "<FORM action='mem_address_regist.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<input type='hidden' name='aid' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='新規追加'>\n";
        $dsp_tbl .= "<input type='button' class = 'button1' value='閉じる' onClick='window.close();'>\n";
	$dsp_tbl .= "</FORM>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "</div>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";


	//管理画面入力ページ表示関数
	PrintAdminPage("配送先住所",$dsp_tbl);

?>
