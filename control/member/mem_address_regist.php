<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/11												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	$mid = $_REQUEST["mid"];
	
	if(($mid != "") && ($mid != 0))
	{
		//会員情報取得関数
		GetTMemberInfo($mid,$tmm_f_login_id,$tmm_f_login_pass,$tmm_f_handle,$tmm_fk_area_id,$tmm_fk_job_id,$tmm_f_activity,$tmm_fk_member_group_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb,$tmm_f_ser_no,$tmm_f_mailmagazein_pc,$tmm_f_mailmagazein_mb,$tmm_fk_adcode_id,$tmm_fk_parent_ad,$tmm_f_regist_date,$tmm_f_regist_pos,$tmm_f_name,$tmm_f_tel_no,$tmm_f_sex,$tmm_f_birthday,$tmm_fk_address_flg,$tmm_f_over_money,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_last_entry,$ip,$a,$b);
	}
	else
	{
		//現在日時
		$now = time();
		
		$tmm_f_login_id			= "";
		$tmm_f_login_pass		= "";
		$tmm_f_handle			= "";
		$tmm_f_activity			= "";
		$tmm_fk_member_group_id	= "";
		$tmm_f_mail_address_pc	= "";
		$tmm_f_mail_address_mb	= "";
		$tmm_f_ser_no			= "";
		$tmm_f_mailmagazein_pc	= "";
		$tmm_f_mailmagazein_mb	= "";
		$tmm_fk_adcode_id		= "";
		$tmm_fk_parent_ad		= "";
		$tmm_f_regist_date		= date("Y-m-d H:i:00", $now);
		$tmm_f_regist_pos		= "2";
		$tmm_f_name				= "";
		$tmm_f_tel_no			= "";
		$tmm_f_sex				= "";
		$tmm_f_birthday			= "";
		$tmm_fk_address_flg		= "0";
		$tmm_f_over_money		= "0";
		$tm_f_coin				= "0";
		$tm_f_free_coin			= "0";
		$tm_f_total_pay			= "0";
		$tm_f_last_entry		= "";
	}
	
	//▼会員IDハンドル名
	$member_id_str  = "";
	$member_id_str .= $tmm_f_login_id;
	$member_id_str .= "(";
	$member_id_str .= $tmm_f_handle;
	$member_id_str .= ")";
	
	//▼住所ID
	if(($aid != "") && ($aid != 0))
	{
		//更新モード
		$up_mode = "update";
		
		//会員住所取得関数
		GetTAddressInfo($aid,$ta_f_status,$ta_fk_member_id,$ta_f_last_send,$ta_f_name,$ta_f_tel_no,$ta_f_post_code,$ta_fk_perf_id,$ta_f_address1,$ta_f_address2,$ta_f_address3,$ta_f_tm_stamp);
	}
	else
	{
		//登録モード
		$up_mode = "insert";
		
		$ta_f_status		= "";
		$ta_fk_member_id	= "";
		$ta_f_last_send		= "";
		$ta_f_name			= "";
		$ta_f_tel_no		= "";
		$ta_f_post_code		= "";
		$ta_fk_perf_id		= "";
		$ta_f_address1		= "";
		$ta_f_address2		= "";
		$ta_f_address3		= "";
		$ta_f_tm_stamp		= "";
	}
	
	//▼都道府県選択作成
	//都道府県名一覧取得関数
	GetTPrefNameList($tp_t_pref_id, $tp_t_pref_name, $tp_data_cnt);
	$name = "fk_perf_id";
	$select_num = $ta_fk_perf_id;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $tp_t_pref_id, $tp_t_pref_name, $tp_data_cnt, $select_num, $pref_select);
	
	
	$dsp_tbl  = "";
	

	$dsp_tbl .= "<FORM action='mem_address.php' method='GET' ENCTYPE='multipart/form-data'>\n";

       	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>新規配送先住所追加</legend>\n";$dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員IDハンドル名</th>\n";
	$dsp_tbl .= "<td>$member_id_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員電話番号</th>\n";
	$dsp_tbl .= "<td>$tmm_f_tel_no</tr>\n";
	
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>配送先住所</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>配送先名</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_name' value='$ta_f_name' size='30'></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>連絡電話番号</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_tel_no' value='$ta_f_tel_no' size='30'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>郵便番号 </tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_post_code' value='$ta_f_post_code' size='10'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>都道府県 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3>$pref_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>市区町村 </tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_address1' value='$ta_f_address1' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>番地 </tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_address2' value='$ta_f_address2' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>建物・部屋番 </tt></th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_address3' value='$ta_f_address3' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
       	$dsp_tbl .= "</fieldset>\n";
	
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<td colspan=4 align=center>\n";
       	$dsp_tbl .= "<div style = 'text-align:center;'>\n";
        $dsp_tbl .= "<input type='hidden' name='up_mode' value='$up_mode'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='aid' value='$aid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='登録'>\n";
        $dsp_tbl .= "<input type='button' value='戻る' class = 'button1' onClick='history.back()'>\n";
        $dsp_tbl .= "</div>\n";

        $dsp_tbl .= "</FORM>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("配送先住所",$dsp_tbl);

?>
