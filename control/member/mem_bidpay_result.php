<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/05/04												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
	$pid = $_REQUEST["pid"];
	
	$address_flag = $_REQUEST["address_flag"];
	$shiharai = $_REQUEST["shiharai"];
	$carriage = $_REQUEST["carriage"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];

        //共通のDB連続を取得



//        $db=DbCon();
//        $db=DbCon();
//        Transaction();
//        $dbcon=$dbconect[0];
//        $Transaction=$dbconect[1];
	
//G.Chin AWKT-518 2010-10-13 chg sta
/*
	if(($pid != "") && ($pid != 0))
	{
		//オークション終了商品取得関数
		GetTEndProductsInfo($pid,$tep_f_products_name,$tep_fk_item_category_id,$tep_f_photo1,$tep_f_photo2,$tep_f_photo3,$tep_f_photo4,$tep_f_status,$tep_f_stock,$tep_fk_address_id,$tep_f_last_bidder,$tep_f_pickup_flg,$tep_f_end_time,$tep_f_address_flag);
		//オークション終了商品金額取得関数
		GetTEndProductsPrice($pid,$tep_f_start_price,$tep_f_end_price,$tep_f_market_price);
		//商品アドレスフラグ取得関数
		GetTProductsAddressFlag($pid,$tpm_f_address_flag);	//G.Chin 2010-07-19 add
	}
	else
	{
		$tep_f_products_name		= "";
		$tep_fk_item_category_id	= "";
		$tep_f_photo1				= "";
		$tep_f_photo2				= "";
		$tep_f_photo3				= "";
		$tep_f_photo4				= "";
		$tep_f_status				= "";
		$tep_f_stock				= "";
		$tep_f_last_bidder			= "";
		$tep_fk_address_id			= "";
		$tep_f_end_time				= "";
		$tep_f_address_flag			= "";
		
		$tep_f_start_price			= "";
		$tep_f_end_price			= "";
		$tep_f_market_price			= "";
		
		$tpm_f_address_flag			= "";	//G.Chin 2010-07-19 add
	}
	
	
//G.Chin 2010-07-19 add sta
	//▼状態設定
	if((($tpm_f_address_flag == 0) && ($tep_fk_address_id != "")) && (COIN_CATEGORY != $tep_fk_item_category_id))
	{
		//配送先が決定している場合、"配送済み"に設定
		$f_status = 3;
		$tep_f_status = 3;
	}
	else
	{
		//"支払済み"に設定
		$f_status = 2;
		$tep_f_status = 2;
	}
//G.Chin 2010-07-19 add end
	
	//オークション終了商品状態更新関数
//	$f_status = 2;	//G.Chin 2010-07-19 del

        //■ＤＢ接続
        $db=db_connect();

        mysql_query("set autocommit = 0",$db);


	$f_stock = $tep_f_stock;
	$f_pickup_flg = $tep_f_pickup_flg;
	$ret = SetTEndProductsStatusStock($pid, $f_status, $f_stock, $f_pickup_flg, $db);

       if(!$ret){
            PrintAdminPage("入金処理","<P>オークション終了商品状態更新処理に失敗しました。</P>");
            mysql_query("rollback",$db);
            db_close($db);
            exit;

        }


//        if(!$ret)
//	{
//		PrintAdminPage("入金処理","<P>オークション終了商品状態更新処理に失敗しました。</P>");
//		//print "オークション終了商品状態更新処理に失敗しました。<br>\n";
//		exit;
//	}
	
	//▼終了状態
	switch($tep_f_status)
	{
		case 0:
				$status_str = "落札のみ";
				$bgcolor = "#FFFFFF";
				break;
		case 1:
				$status_str = "購入拒否ｷｬﾝｾﾙ";
				$bgcolor = "#FFFFFF";
				break;
		case 2:
				$status_str = "支払済み";
				$bgcolor = "#FFFFFF";
				break;
		case 3:
				$status_str = "配送先決定";
				$bgcolor = "#FFC8C8";
				break;
		case 4:
				$status_str = "配送済み(完了)";
				$bgcolor = "#C0C0C0";
				break;
		default:
				$status_str = "";
				$bgcolor = "#FFFFFF";
				break;
	}
	
	//▼在庫・発注
	switch($tep_f_stock)
	{
		case 0:		$stock_str = "未指定";		break;
		case 1:		$stock_str = "在庫有り";	break;
		case 2:		$stock_str = "発注済み";	break;
		default:	$stock_str = "";			break;
	}
	
	//▼写真
	if($tep_f_photo1 != "")
	{
		$f_photo_1_path = SITE_URL."images/".$tep_f_photo1;
		$f_photo_1_dsp = "<img src='$f_photo_1_path'>";
	}
	else
	{
		$f_photo_1_dsp = "";
	}
	
	//商品金額取得関数
	GetTProductsPrice($pid,$tpm_f_start_price,$tpm_f_min_price,$tpm_f_r_min_price,$tpm_f_r_max_price,$tpm_f_market_price,$tp_f_now_price,$db);
	
	//送料
	if($carriage == "")
	{
		$carriage = 0;
	}
	
	//▼アドレス要不要フラグ判定
	if($address_flag == 0)
	{
		$f_pay_money = $tep_f_end_price;
	}
	else
	{
		$f_pay_money = $tep_f_end_price + $carriage;
	}
	
	$pay_log_id=-1;

	ChkProdBuy($pid, $pay_log_id, $db);
	if($pay_log_id !=-1 && $pay_log_id !='')
	{
		$dsp_tbl  = "";
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<font><b>この商品への支払は完了しています。</b></font>\n";
		$dsp_tbl .= "<br><br><br>\n";
		$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
		$dsp_tbl .= "<br><br>\n";
		$dsp_tbl .= "<form>\n";
		$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
		$dsp_tbl .= "</form>\n";
		
		PrintAdminPage("入金処理",$dsp_tbl);
		return ;
	}
	
	//支払履歴検索支払方法名取得関数
	SrcPayLogGetFShiharaiName_money($shiharai, $f_shiharai_name,$db);
	
	//商品支払記録関数
	$f_status = 1;
	$f_coin_add = 0;
	$fk_shiharai_type_id = $shiharai;
	$f_coin_result = 0;
	$f_min_price = $tpm_f_min_price;
	$f_cert_status = 0;
	$f_cert_err = "";
	$f_pay_pos = 2;
	$fk_adcode_id = 0;

	$ret = InsertProductsTPayLog($mid,$f_status,$sid,$f_pay_money,$f_coin_add,$fk_shiharai_type_id,$f_coin_result,$f_min_price,$f_cert_status,$f_cert_err,$f_pay_pos,$fk_adcode_id,$pid,$db);

       if(!$ret){
            PrintAdminPage("入金処理","<P>入金処理に失敗しました。</P>");
            mysql_query("rollback",$db);
            db_close($db);
            exit;
       }     

//	if($ret == false)
//	{
//		PrintAdminPage("入金処理","<P>入金処理に失敗しました。</P>");
//		//print "入金処理に失敗しました。<br>\n";
//		exit;
//	}
	
	//売上集計商品支払更新関数
	$f_type = 0;
	$f_prd_lastprice = $f_pay_money;
        if($address_flag == 0)
        {
            $ret=UpdateTStatSalesPayProduct($f_type,$f_prd_lastprice,$f_prd_lastprice,$db);

           if(!$ret){

                mysql_query("rollback",$db);
                db_close($db);
            }
        }
        else
        {
            $ret=UpdateTStatSalesPayProduct($f_type,$f_prd_lastprice,0,$db);

            if(!$ret){

                mysql_query("rollback",$db);
                db_close($db);

            }
        }

	
//G.Chin 2010-07-26 add sta
	//▼コインパック判定
	if($tep_fk_item_category_id == COIN_CATEGORY)
	{
		//会員コイン情報取得関数
		GetTMemberCoinData_money($mid,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_over_money,$db);
		$total_coin = $tm_f_coin + $tm_f_free_coin;
		//商品ポイントオークション時加算枚数取得関数
		GetTProductsPlusCoins_money($pid,$tmm_plus_coins,$db);
		$coin_result = $tmm_plus_coins + $total_coin;
		
		//仕入価格集計関数２
		$ret = SetCostPay2_money($pid,$sid,$mid,0,$tmm_plus_coins,$coin_result,$f_prd_lastprice,$db);
                if(!$ret){
                    
                    PrintAdminPage("入金処理","<P>コインの加算に失敗しました。</P>");
                    mysql_query("rollback",$db);
                    db_close($db);

                    exit;
                }

	}

        $sql ="update auction.t_member set f_unpain_num = f_unpain_num -1 where fk_member_id =$mid and f_unpain_num > 0";
        mysql_query($sql,$db);
        
        mysql_query("commit",$db);
        mysql_query("END",$db);
        db_close($db);

//G.Chin 2010-07-26 add end
*/
	if(($pid != "") && ($pid != 0))
	{
		//終了商品入金処理関数
		$ret = PaymentEndProduct($sid,$mid,$pid,$shiharai,$address_flag,$carriage,$status_str,$bgcolor,$stock_str,$f_photo_1_dsp,$f_shiharai_name,$errmsg);
		if($ret == false)
		{
			PrintAdminPage("入金処理",$errmsg);
			exit;
		}
		//オークション終了商品取得関数
		GetTEndProductsInfo($pid,$tep_f_products_name,$tep_fk_item_category_id,$tep_f_photo1,$tep_f_photo2,$tep_f_photo3,$tep_f_photo4,$tep_f_status,$tep_f_stock,$tep_fk_address_id,$tep_f_last_bidder,$tep_f_pickup_flg,$tep_f_end_time,$tep_f_address_flag);
		//オークション終了商品金額取得関数
		GetTEndProductsPrice($pid,$tep_f_start_price,$tep_f_end_price,$tep_f_market_price);
		//商品アドレスフラグ取得関数
		GetTProductsAddressFlag($pid,$tpm_f_address_flag);
	}
	else
	{
		$tep_f_products_name		= "";
		$tep_fk_item_category_id	= "";
		$tep_f_photo1				= "";
		$tep_f_photo2				= "";
		$tep_f_photo3				= "";
		$tep_f_photo4				= "";
		$tep_f_status				= "";
		$tep_f_stock				= "";
		$tep_f_last_bidder			= "";
		$tep_fk_address_id			= "";
		$tep_f_end_time				= "";
		$tep_f_address_flag			= "";
		
		$tep_f_start_price			= "";
		$tep_f_end_price			= "";
		$tep_f_market_price			= "";
		
		$tpm_f_address_flag			= "";
	}
//G.Chin AWKT-518 2010-10-13 chg end
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FF0000' align=center colspan=4><tt><font color='#000000'>入金処理画面</font></tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#CCCCFF' align=righth><tt> 会員ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$mid</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "<br>\n";
	
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=center colspan=4><tt><font color='#0000FF'>落札商品</font></tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 商品ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$pid</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 商品名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$tep_f_products_name</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 写真 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$f_photo_1_dsp</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 状態 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$status_str</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 在庫・発注状態 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$stock_str</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 落札価格 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$tep_f_end_price 円</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 送料 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$carriage 円</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 入金処理タイプ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$f_shiharai_name</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<div align=left>\n";
	$dsp_tbl .= "<font color='#FF0000'>※入金処理を行いました。</font>\n";
	$dsp_tbl .= "</div>\n";
	$dsp_tbl .= "<FORM action='mem_bidlist.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='戻る'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("入金処理",$dsp_tbl);

//        function SetTEndProductsStatusStock1( $dbaaa) {
//
//
//            //■ＳＱＬ実行
//
//            $result = mysql_query("SELECT * FROM auction.t_member_master WHERE f_login_id='li'", $dbaaa);
//         if (!$result) {
//            echo ' n result false';
//
//            return false;
//        }
//
//            return true;
//
//    }


?>
