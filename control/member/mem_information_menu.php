<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Li													*/
/*		作成日		:	2010/09/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;


	$dsp_tbl  = "";

	$dsp_tbl .= "<table class = 'main_l'>\n";
        $dsp_tbl .= "<caption>&#x2464;会員入力情報編集</caption>";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員地域編集</th>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input class='submit' style='width:120px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='新規地域登録' type='button' onclick=\"OpenWin('mem_area_regist.php?area_id=&photo_num=')\">\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";

        $dsp_tbl .= "<form name='form_area' enctype='multipart/form-data' action='area_list.php' method='GET' target='main_r'>\n";
//	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";


	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
//        $dsp_tbl .= "<input type='hidden' name='tic_fk_area_id' value=''>\n";
        $dsp_tbl .= "<input type='hidden' name='status' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='width:120px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='使用中地域表示'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";

        $dsp_tbl .= "<form name='form_area_del' enctype='multipart/form-data' action='area_list_delete.php' method='GET' target='main_r'>\n";
//	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";


	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
        $dsp_tbl .= "<input type='hidden' name='status' value='9'>\n";
	$dsp_tbl .= "<input type='submit' style='width:120px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='未使用地域表示'>\n";
//      $dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='未使用カテゴリ検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "<br />\n";

	$dsp_tbl .= "<table class = 'main_l'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員職業編集</th>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input class='submit' style='width:120px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='新規職業登録' type='button' onclick=\"OpenWin('mem_job_regist.php?job_id=')\">\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";

        $dsp_tbl .= "<form name='form_job' enctype='multipart/form-data' action='job_list.php' method='GET' target='main_r'>\n";
//	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";


	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
        $dsp_tbl .= "<input type='hidden' name='sort' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
        $dsp_tbl .= "<input type='hidden' name='status' value='0'>\n";

	$dsp_tbl .= "<input type='submit' style='width:120px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='使用中職業表示'>\n";
//      $dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='未使用カテゴリ検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";

        $dsp_tbl .= "<form name='form_job_del' enctype='multipart/form-data' action='job_list_delete.php' method='GET' target='main_r'>\n";
//	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' class = 'main_l'>\n";


	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
        $dsp_tbl .= "<input type='hidden' name='sort' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
        $dsp_tbl .= "<input type='hidden' name='status' value='9'>\n";
	$dsp_tbl .= "<input type='submit' style='width:120px; background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='未使用職業表示'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";


	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
