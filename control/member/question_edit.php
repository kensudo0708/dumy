<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$qid = $_REQUEST["qid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	$inp_flag = $_REQUEST["inp_flag"];
	$inp_category = $_REQUEST["inp_category"];
	
	//質問情報取得関数
	GetTQuestionInfo($qid,$tq_fk_question_category_id,$tq_f_subject,$tq_f_main,$tq_f_name,$tq_fk_member_id,$tq_f_mail_address,$tq_f_flag,$tq_f_recv_dt,$tq_f_check_dt,$tq_f_tm_stamp);
	
	//▼状態
	switch($tq_f_flag)
	{
		case 0:	$flag_str = "未読";		break;
		case 1:	$flag_str = "既読";		break;
		case 2:	$flag_str = "返信済み";	break;
	}
	
	//質問カテゴリ名取得関数
	GetTQuestionCategoryName($tq_fk_question_category_id, $f_category_name);
	
	//会員ログインIDハンドル取得関数
	GetTMemberLoginIdHandle($tq_fk_member_id, $tmm_f_login_id, $tmm_f_handle);
	
	//▼受付日
	$recv_dt_str = substr($tq_f_recv_dt, 0, 10);
	
	//▼各種リンク
	$link_member = "<A href='mem_regist.php?sid=$sid&mid=$tq_fk_member_id' target='_blank'>$tmm_f_handle</A>";
	
	//スタッフ管理者権限判定関数
	//▼会員メアド表示権限を取得
	$staff_mail = CheckStaffTAuthority($sid,"24");
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>No</th>\n";
	$dsp_tbl .= "<td style = 'width:380px; text-align:left;'>$qid</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>状態</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$flag_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>カテゴリ</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$f_category_name</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>会員</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$link_member</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	//▼会員メアド表示権限を判定
	if($staff_mail == true)
	{
		$dsp_tbl .= "<tr style = 'height:25px;'>\n";
		$dsp_tbl .= "<th>ﾒｰﾙｱﾄﾞﾚｽ</th>\n";
		$dsp_tbl .= "<td style = 'text-align:left;'>$tq_f_mail_address</td>\n";
		$dsp_tbl .= "</tr>\n";
		
//G.Chin 2010-09-30 chg sta
/*
                $mail_str="";
                if(ereg("vodafone",$tq_f_mail_address) || ereg("softbank",$tq_f_mail_address))
                {
                    $mail_str="MB(softbank)";
                }
                elseif(ereg("docomo",$tq_f_mail_address))
                {
                    $mail_str="MB(docomo)";
                }
                elseif(ereg("ezweb",$tq_f_mail_address))
                {
                    $mail_str="MB(AU)";
                }
                else
                {
                    $mail_str="PC";
                }
                $dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th bgcolor='#C0C0C0' align=left><tt>返信対象</tt></th>\n";
		$dsp_tbl .= "<td colspan=5 align=left><font color='#B22222'>$mail_str</font></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
*/
	}
	
	//▼返信対象
	$mail_str = "";
	if(ereg("vodafone",$tq_f_mail_address) || ereg("softbank",$tq_f_mail_address))
	{
		$mail_str = "MB(softbank)";
	}
	else if(ereg("docomo",$tq_f_mail_address))
	{
		$mail_str = "MB(docomo)";
	}
	elseif(ereg("ezweb",$tq_f_mail_address))
	{
		$mail_str = "MB(AU)";
	}
	else
	{
		$mail_str = "PC";
	}
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>返信対象</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$mail_str</td>\n";
	$dsp_tbl .= "</tr>\n";
//G.Chin 2010-09-30 chg end
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>受付日</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$tq_f_recv_dt</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>タイトル</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$tq_f_subject</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>本文</th>\n";
	$dsp_tbl .= "<td style = 'text-align:left;'>$tq_f_main</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        $dsp_tbl .= "<br>\n";
        
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<td colspan=6 align=center>\n";
        $dsp_tbl .= "<div style = 'text-align:center;'>\n";
	$dsp_tbl .= "<FORM action='question_list.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qid' value='$qid'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='update'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_flag' value='1'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_flag' value='$inp_flag'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_category' value='$inp_category'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='確認'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='question_list.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qid' value='$qid'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='update'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_flag' value='2'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_flag' value='$inp_flag'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_category' value='$inp_category'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='返信済'>\n";
        $dsp_tbl .= "<input type='button' value='戻る' class = 'button1' onClick='history.back()'>\n";
        $dsp_tbl .= "</FORM>\n";
        $dsp_tbl .= "</div>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("問合せ管理",$dsp_tbl);

?>
