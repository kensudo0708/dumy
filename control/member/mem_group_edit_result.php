<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$gid = $_REQUEST["gid"];
//print "gid = [".$gid."]<br>\n";
	
	$f_member_group_name = $_REQUEST["f_member_group_name"];
	$fk_coin_group_id = $_REQUEST["fk_coin_group_id"];
	$f_memo = $_REQUEST["f_memo"];
	
	$dsp_string  = "";
	
	//会員グループ情報更新関数
	$ret = UpdateTMGroupInfo($gid,$f_member_group_name,$fk_coin_group_id,$f_memo);
	if($ret == false)
	{
		$dsp_string = "更新処理に失敗しました。<br>\n";
	}
	else
	{
		$dsp_string .= "会員グループ情報の更新処理に成功しました。<br>\n";
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("グループ編集完了",$dsp_tbl);

?>

