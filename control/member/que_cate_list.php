<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	$up_mode = $_REQUEST["up_mode"];
	
	//更新モード
	if($up_mode == "insert")
	{
		$f_category_name = $_REQUEST["f_category_name"];
		
		//質問カテゴリ登録関数
		RegistTQuestionCategory($f_category_name);
	}
	else if($up_mode == "update")
	{
		$qcid = $_REQUEST["qcid"];
		$f_category_name = $_REQUEST["f_category_name"];
		
		//質問カテゴリ更新関数
		UpdateTQuestionCategory($qcid, $f_category_name);
	}
	else if($up_mode == "delete")
	{
		$qcid = $_REQUEST["qcid"];
		
		//質問カテゴリ削除関数
		DeleteTQuestionCategory($qcid);
	}
	
	//質問カテゴリ一覧取得関数
	GetTQuestionCategoryList($tqc_fk_question_category_id,$tqc_f_category_name,$tqc_f_tm_stamp,$tqc_data_cnt);
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$tqc_data_cnt</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";


	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:50px;'>No</th>\n";
	$dsp_tbl .= "<th style = 'width:400px;'>カテゴリ</th>\n";
	$dsp_tbl .= "<th colspan = '2' style = 'width:120px;'>操作</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tqc_data_cnt;$i++)
	{
		//▼各種リンク
		$link_edit = "<A href='que_cate_regist.php?sid=$sid&qcid=$tqc_fk_question_category_id[$i]'>編集</A>";
		$link_delete = "<A href='que_cate_list.php?qcid=$tqc_fk_question_category_id[$i]&up_mode=delete'>削除</A>";
		
		$dsp_tbl .= "<tr style = 'height:25px;'>\n";
		$dsp_tbl .= "<td align='center'>$tqc_fk_question_category_id[$i]</td>\n";
		$dsp_tbl .= "<td style = 'text-align:left; padding-left:5px;'>$tqc_f_category_name[$i]</td>\n";
		$dsp_tbl .= "<td align='center'>$link_edit</td>\n";
		$dsp_tbl .= "<td align='center'>$link_delete</td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("問合せ管理",$dsp_tbl);

?>
