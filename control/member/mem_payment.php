<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/10												*/
/*		修正日		:	2010/05/04	落札商品の表示追加							*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
	
	$mode = $_REQUEST["mode"];
	
	//▼更新モード
	if($mode == "memo")
	{
		$f_memo = $_REQUEST["f_memo"];
		
		//会員メモ更新関数
		UpdateTMemberMemo($mid,$f_memo);
	}
	
	if(($mid != "") && ($mid != 0))
	{
		//会員情報取得関数
		GetTMemberInfo($mid,$tmm_f_login_id,$tmm_f_login_pass,$tmm_f_handle,$tmm_fk_area_id,$tmm_fk_job_id,$tmm_f_activity,$tmm_fk_member_group_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb,$tmm_f_ser_no,$tmm_f_mailmagazein_pc,$tmm_f_mailmagazein_mb,$tmm_fk_adcode_id,$tmm_fk_parent_ad,$tmm_f_regist_date,$tmm_f_regist_pos,$tmm_f_name,$tmm_f_tel_no,$tmm_f_sex,$tmm_f_birthday,$tmm_fk_address_flg,$tmm_f_over_money,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_last_entry,$ip,$a,$b);
		//会員メモ取得関数
		GetTMemberMemo($mid,$tmm_f_memo);
		//オークション終了商品未払い件数取得関数
		GetTEndProductsNotPayCount($mid, $tep_all_count);
	}
	else
	{
		//現在日時
		$now = time();
		
		$tmm_f_login_id			= "";
		$tmm_f_login_pass		= "";
		$tmm_f_handle			= "";
		$tmm_f_activity			= "";
		$tmm_fk_member_group_id	= "";
		$tmm_f_mail_address_pc	= "";
		$tmm_f_mail_address_mb	= "";
		$tmm_f_ser_no			= "";
		$tmm_f_mailmagazein_pc	= "";
		$tmm_f_mailmagazein_mb	= "";
		$tmm_fk_adcode_id		= "";
		$tmm_fk_parent_ad		= "";
		$tmm_f_regist_date		= date("Y-m-d H:i:00", $now);
		$tmm_f_regist_pos		= "2";
		$tmm_f_name				= "";
		$tmm_f_tel_no			= "";
		$tmm_f_sex				= "";
		$tmm_f_birthday			= "";
		$tmm_fk_address_flg		= "0";
		$tmm_f_over_money		= "0";
		$tm_f_coin				= "0";
		$tm_f_free_coin			= "0";
		$tm_f_total_pay			= "0";
		$tm_f_last_entry		= "";
		$tmm_f_memo				= "";
		$tep_all_count			= "0";
	}
	
	//▼会員グループ
	//会員グループ情報取得関数
	GetTMemberGroupInfo($tmm_fk_member_group_id,$tmg_f_member_group_name,$tmg_fk_coin_group_id,$tmg_f_memo,$tmg_f_tm_stamp);
	
	//コイン売却価格一覧取得関数
	GetTCoinSetupList($tmg_fk_coin_group_id,$tcs_fk_coin_id,$tcs_f_coin,$tcs_fk_shiharai_type_id,$tcs_f_inp_money,$tcs_f_tm_stamp,$tcs_f_free_coin,$tcs_data_cnt);
	
	
	$dsp_tbl  = "";

        $dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .="<legend>会員情報</legend>\n";
        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員ID</th>\n";
	$dsp_tbl .= "<td>$mid</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>会員グループ</th>\n";
	$dsp_tbl .= "<td>$tmg_f_member_group_name</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<FORM action='mem_payment.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<th rowspan = '2'>メモ</th>\n";
	$dsp_tbl .= "<td><textarea name='f_memo' cols='40' rows='5'>$tmm_f_memo</textarea>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value='memo'>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td><input type='submit' class = 'button2' style = 'width:80px;' value='ﾒﾓ更新'></td>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";


	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>落札商品</legend>\n";
        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th rowspan = '2'>未払い商品</th>\n";
	$dsp_tbl .= "<td>$tep_all_count&nbsp;件</td>\n";
	$dsp_tbl .= "<FORM action='mem_bidlist.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
      	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td><input type='submit' class = 'button2' style = 'width:80px;' value='ﾘｽﾄ表示'></td>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";

	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>コイン設定</legend>\n";
       	$dsp_tbl .= "<table class = 'form'>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>購入ｺｲﾝ</th>\n";
	$dsp_tbl .= "<td>$tm_f_coin&nbsp;&nbsp;コイン</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ｻｰﾋﾞｽｺｲﾝ</th>\n";
	$dsp_tbl .= "<td>$tm_f_free_coin&nbsp;&nbsp;コイン</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>購入合計金額</th>\n";
	$dsp_tbl .= "<td>$tm_f_total_pay&nbsp;&nbsp;円</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>未処理金額</th>\n";
	$dsp_tbl .= "<td>$tmm_f_over_money&nbsp;&nbsp;円</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";
	


	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>入金処理</legend>\n";

       	$dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>銀行振込入金額</th>\n";
	$dsp_tbl .= "<FORM action='mem_payconfirm.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='inp_bank' value='0' size='10'>&nbsp;&nbsp;円入金</td>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value='bank'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<td><input type='submit' class = 'button2' style = 'width:80px;' value='入金処理'></td>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>ｻｰﾋﾞｽｺｲﾝ追加</th>\n";
	$dsp_tbl .= "<FORM action='mem_payconfirm.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='inp_free' value='0' size='10'>&nbsp;&nbsp;コイン追加</td>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value='free'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<td><input type='submit' class = 'button2' style = 'width:80px;' value='追加処理'></td>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>未処理金ｸﾘｱ</th>\n";
	$dsp_tbl .= "<FORM action='mem_payconfirm.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<td colspan=2><input type='text' name='inp_over' value='0' size='10'>&nbsp;&nbsp;コイン追加で未処理金額を0円にする</td>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value='over'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<td><input type='submit' class = 'button2' style = 'width:80px;' value='追加処理'></td>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";


        $dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>料金テーブル(基本コイン)</legend>\n";

	$dsp_tbl .= "<table class='list'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:40px;'></th>\n";
	$dsp_tbl .= "<th style = 'width:80px;'>コイン数</td>\n";
	$dsp_tbl .= "<th style = 'width:80px;'>料金</td>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>操作</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	for($i=0; $i<$tcs_data_cnt; $i++)
	{
		$num = $i + 1;
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th>$num</th>\n";
		$dsp_tbl .= "<td class='right' style='right-padding:2px'>".number_format($tcs_f_coin[$i])."</td>\n";
		$dsp_tbl .= "<td class='right' style='right-padding:2px'>".number_format($tcs_f_inp_money[$i])."</td>\n";
		$dsp_tbl .= "<FORM action='mem_payconfirm.php' method='POST' ENCTYPE='multipart/form-data'>\n";
		$dsp_tbl .= "<input type='hidden' name='mode' value='coin'>\n";
		$dsp_tbl .= "<input type='hidden' name='cid' value='$tcs_fk_coin_id[$i]'>\n";
		$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
		$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
		$dsp_tbl .= "<td><input type='submit' class = 'button2' style = 'width:80px;' value='追加処理'></td>\n";
		$dsp_tbl .= "</FORM>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        $dsp_tbl .= "</fieldset>\n";
        $dsp_tbl .= "<div style = 'text-align:center; margin-top:5px;'>\n";
        $dsp_tbl .= "<input type='button' class = 'button1' value='閉じる' onClick='window.close();'>\n";
        $dsp_tbl .= "</div><br>\n";

//管理画面入力ページ表示関数
	PrintAdminPage("入金処理",$dsp_tbl);

?>
