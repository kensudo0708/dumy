<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	//▼会員グループ選択作成
	//会員グループ名一覧取得関数
	GetFMGroupNameList($fmg_fk_member_group_id, $fmg_f_member_group_name, $fmg_data_cnt);
	$inp_group_id[0] = "";
	$inp_group_name[0] = "(指定しない)";
	for($i=0; $i<$fmg_data_cnt; $i++)
	{
		$inp_group_id[$i+1] = $fmg_fk_member_group_id[$i];
		$inp_group_name[$i+1] = $fmg_f_member_group_name[$i];
	}
	$inp_group_cnt = $fmg_data_cnt + 1;
	$name = "inp_group";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_group_id, $inp_group_name, $inp_group_cnt, $select_num, $group_select);
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table class = 'main_l'>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='black_maillist.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "メールアドレス";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_mail' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>登録状態</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_status'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>配信不可\n";
	$dsp_tbl .= "<OPTION value='1'>入会不可\n";
	$dsp_tbl .= "<OPTION value='9'>ﾌﾞﾗｯｸ保留\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>ﾌﾞﾗｯｸ登録元</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_base'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>番組ﾌﾞﾗｯｸ\n";
	$dsp_tbl .= "<OPTION value='1'>共有ﾌﾞﾗｯｸ\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:80px; border-color:#FFFAFA' value='検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</form>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input class='submit' style='background-color:#FFFBEC; color:#C84B00; width:80px; border-color:#FFFAFA' value='新規追加' type='button' onclick=\"OpenWin('black_mailreg.php?sid=$sid&bmid=0')\">\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
