<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/10												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$mid = $_REQUEST["mid"];
	
	//会員情報取得関数
	GetTMemberInfo($mid,$tmm_f_login_id,$tmm_f_login_pass,$tmm_f_handle,$tmm_fk_area_id,$tmm_fk_job_id,$tmm_f_activity,$tmm_fk_member_group_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb,$tmm_f_ser_no,$tmm_f_mailmagazein_pc,$tmm_f_mailmagazein_mb,$tmm_fk_adcode_id,$tmm_fk_parent_ad,$tmm_f_regist_date,$tmm_f_regist_pos,$tmm_f_name,$tmm_f_tel_no,$tmm_f_sex,$tmm_f_birthday,$tmm_fk_address_flg,$tmm_f_over_money,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_last_entry,$ip,$a,$b);
	
	$dsp_tbl="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
	$dsp_tbl .= "<html lang='utf-8'>\n";
	$dsp_tbl .="<head>\n";
	$dsp_tbl .="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
	$dsp_tbl .="<title>会員削除</title>\n";
	$dsp_tbl .="</head>\n";
	$dsp_tbl .="<body bgcolor='FF3300'>\n";
	
	//$dsp_tbl .= "<B><font size='2'>[";
	$dsp_tbl .= "<h3>[";
	$dsp_tbl .= $mid;
	$dsp_tbl .= ":";
	$dsp_tbl .= $tmm_f_login_id;
	$dsp_tbl .= "(";
	$dsp_tbl .= $tmm_f_handle;
	//$dsp_tbl .= ")]の会員を削除します。よろしければ、下のボタンをクリックしてください。</font></B>\n";
	$dsp_tbl .= ")]の会員を削除します。よろしければ、下のボタンをクリックしてください。</h3>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<FORM action='mem_delete_result.php' method='POST' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='削除実行'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</BODY>\n";
	$dsp_tbl .= "</HTML>\n";
	
	echo $dsp_tbl
	//管理画面入力ページ表示関数
	//PrintAdminPage("会員削除",$dsp_tbl);

?>
