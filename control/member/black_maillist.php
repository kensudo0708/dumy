<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<script type="text/javascript">
    /**
     * 確認ダイアログの返り値によりフォーム送信
    */
<!--
	function submitChk ()
	{
		/* 確認ダイアログ表示 */
		var flag = confirm ('ブラックリストより削除します。よろしいですか？');
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	$up_mode = $_REQUEST["up_mode"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	$inp_mail = $_REQUEST["inp_mail"];
	$inp_status = $_REQUEST["inp_status"];
	$inp_base = $_REQUEST["inp_base"];
	
	//更新モード
	if($up_mode == "delete")
	{
		$bmid = $_REQUEST["bmid"];
		
		//ブラックメールアドレス削除関数
		DeleteTBMail($bmid);
	}
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//ブラックメールアドレス一覧取得関数
	GetTBMailList($inp_mail,$inp_status,$inp_base,$limit,$offset,$tbm_fk_black_member_ser_id,$tbm_f_mail_address,$tbm_f_base,$tbm_f_status,$tbm_f_tm_stamp,$tbm_all_count,$tbm_data_cnt);
	
	//▼表示開始～終了番号
	if($tbm_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tbm_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tbm_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='black_maillist.php?inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&offset=$page_offset[$i]&limit=$limit&up_mode='>$num</A> ";
			}
		}
	}
	
	if($tbm_all_count > $limit)
	{
		$page_first = "<A href='black_maillist.php?inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&offset=0&limit=$limit&up_mode='>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='black_maillist.php?inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&offset=$page_offset[$max_page]&limit=$limit&up_mode='>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tbm_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='black_maillist.php?inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&offset=$next_page&limit=$limit&up_mode='>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='black_maillist.php?inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&offset=$before_page&limit=$limit&up_mode='>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "black_maillist.php?inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&limit=$limit&offset=&up_mode=";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='black_maillist.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='inp_mail' value='$inp_mail'>";
		$form_str .= "<input type='hidden' name='inp_status' value='$inp_status'>";
		$form_str .= "<input type='hidden' name='inp_base' value='$inp_base'>";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "<input type='hidden' name='up_mode' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
//	$dsp_tbl .= "</center>\n";
	
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$tbm_all_count</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:60px;'>ID</th>\n";
	$dsp_tbl .= "<th style = 'width:250px;'>メールアドレス</th>\n";
	$dsp_tbl .= "<th style = 'width:80px;'>状態</th>\n";
	$dsp_tbl .= "<th style = 'width:80px;'>登録元</th>\n";
	$dsp_tbl .= "<th style = 'width:100px;'>操作</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tbm_data_cnt;$i++)
	{
		//▼状態
		switch($tbm_f_status[$i])
		{
			case 0:		$status_str = "配信不可";	break;
			case 1:		$status_str = "入会不可";	break;
			case 9:		$status_str = "ﾌﾞﾗｯｸ保留";	break;
			default:	$status_str = "";			break;
		}
		
		//▼ブラック登録元
		switch($tbm_f_base[$i])
		{
			case 0:		$base_str = "番組ﾌﾞﾗｯｸ";	break;
			case 1:		$base_str = "共有ﾌﾞﾗｯｸ";	break;
			default:	$base_str = "";				break;
		}
		
		//▼各種リンク
		$link_edit = "<A href='black_mailreg.php?sid=$sid&bmid=$tbm_fk_black_member_ser_id[$i]' target='_blank'>編集</A>";
		$link_delete = "<A href='black_maillist.php?sid=$sid&bmid=$tbm_fk_black_member_ser_id[$i]&inp_mail=$inp_mail&inp_status=$inp_status&inp_base=$inp_base&limit=$limit&offset=$offset&page=$page&up_mode=delete' onClick=\"return submitChk()\">削除</A>";
		
		$link_str  = "";
		$link_str .= $link_edit;
		$link_str .= "&nbsp;";
		$link_str .= $link_delete;
		
		$dsp_tbl .= "<tr style = 'height:25px;'>\n";
		$dsp_tbl .= "<td align='center'>$tbm_fk_black_member_ser_id[$i]</td>\n";
		$dsp_tbl .= "<td align='center'>$tbm_f_mail_address[$i]</td>\n";
		$dsp_tbl .= "<td align='center'>$status_str</td>\n";
		$dsp_tbl .= "<td align='center'>$base_str</td>\n";
		$dsp_tbl .= "<td align='center'>$link_str</td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	
	//管理画面入力ページ表示関数
	PrintAdminPage("ブラックメールアドレス管理",$dsp_tbl);

?>
