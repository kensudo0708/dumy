<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/05/04												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
	$pid = $_REQUEST["pid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	if(($pid != "") && ($pid != 0))
	{
		//オークション終了商品取得関数
		GetTEndProductsInfo($pid,$tep_f_products_name,$tep_fk_item_category_id,$tep_f_photo1,$tep_f_photo2,$tep_f_photo3,$tep_f_photo4,$tep_f_status,$tep_f_stock,$tep_fk_address_id,$tep_f_last_bidder,$tep_f_pickup_flg,$tep_f_end_time,$tep_f_address_flag);
		//オークション終了商品金額取得関数
		GetTEndProductsPrice($pid,$tep_f_start_price,$tep_f_end_price,$tep_f_market_price);
		//オークション終了商品アドレスフラグ取得関数
		GetTEndProductsAddressFlag($pid,$tep_f_address_flag);
	}
	else
	{
		$tep_f_products_name		= "";
		$tep_fk_item_category_id	= "";
		$tep_f_photo1				= "";
		$tep_f_photo2				= "";
		$tep_f_photo3				= "";
		$tep_f_photo4				= "";
		$tep_f_status				= "";
		$tep_f_stock				= "";
		$tep_f_last_bidder			= "";
		$tep_fk_address_id			= "";
		$tep_f_end_time				= "";
		$tep_f_address_flag			= "";
		
		$tep_f_start_price			= "";
		$tep_f_end_price			= "";
		$tep_f_market_price			= "";
		
		$tep_f_address_flag			= 0;
	}
	
	//▼終了状態
	switch($tep_f_status)
	{
		case 0:
				$status_str = "落札のみ";
				$bgcolor = "#FFFFFF";
				break;
		case 1:
				$status_str = "購入拒否ｷｬﾝｾﾙ";
				$bgcolor = "#FFFFFF";
				break;
		case 2:
				$status_str = "支払済み";
				$bgcolor = "#FFFFFF";
				break;
		case 3:
				$status_str = "配送先決定";
				$bgcolor = "#FFC8C8";
				break;
		case 4:
				$status_str = "配送済み(完了)";
				$bgcolor = "#C0C0C0";
				break;
		default:
				$status_str = "";
				$bgcolor = "#FFFFFF";
				break;
	}
	
	//▼在庫・発注
	switch($tep_f_stock)
	{
		case 0:		$stock_str = "未指定";		break;
		case 1:		$stock_str = "在庫有り";	break;
		case 2:		$stock_str = "発注済み";	break;
		default:	$stock_str = "";			break;
	}
	
	//▼写真
	if($tep_f_photo1 != "")
	{
		$f_photo_1_path = SITE_URL."images/".$tep_f_photo1;
		$f_photo_1_dsp = "<img src='$f_photo_1_path'>";
	}
	else
	{
		$f_photo_1_dsp = "";
	}
	
	//▼送料
	$carriage = CARRIAGE;
	
	//▼入金処理タイプ選択作成
	//会員支払方法一覧取得関数
	GetMemShiharaiList($fst_fk_shiharai_type_id,$fsp_f_shiharai_name,$fst_data_cnt);
	$name = "shiharai";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $fst_fk_shiharai_type_id, $fsp_f_shiharai_name, $fst_data_cnt, $select_num, $shiharai_select);
	
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FF0000' align=center colspan=4><tt><font color='#000000'>入金処理画面</font></tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#CCCCFF' align=righth><tt> 会員ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$mid</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "<br>\n";
	
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=center colspan=4><tt><font color='#0000FF'>落札商品</font></tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 商品ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$pid</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 商品名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$tep_f_products_name</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 写真 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$f_photo_1_dsp</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 状態 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$status_str</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 在庫・発注状態 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'><tt>$stock_str</tt></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<FORM action='mem_bidpay_result.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 落札価格 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$tep_f_end_price 円</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 送料 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>";
	//▼アドレス要不要フラグ判定
	if($tep_f_address_flag == 0)
	{
		$dsp_tbl .= "<input type='checkbox' name='address_flag' value='1' checked>";
	}
	else
	{
		$dsp_tbl .= "<input type='checkbox' name='address_flag' value='1'>";
	}
	$dsp_tbl .= "<input type='text' name='carriage' value='$carriage' size='5'> 円含める(含めない場合、ﾁｪｯｸを外す)";
	$dsp_tbl .= "</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#C0C0C0' align=right><tt> 入金処理タイプ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'>$shiharai_select\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<font color='#FF0000'>入金処理を行います。<BR>取り消しはできませんのでご確認ください。</font><BR>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='pid' value='$pid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='確定'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mem_bidlist.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='戻る'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("入金処理",$dsp_tbl);

?>
