<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	session_start();
	if($_SESSION['staff_id']==null)
        {
            $dsp_tbl ="<P>ログインの有効期限が切れました。不具合がおきる可能性があるので再度ログインしてください。</P>";
            $dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
            $dsp_tbl .= "<br><br>\n";
            $dsp_tbl .= "<form>\n";
            $dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
            $dsp_tbl .= "</form>\n";
            PrintAdminPage("セッション切れ",$dsp_tbl);
        }
	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
//print "mid = [".$mid."]<br>\n";
	$up_mode = $_REQUEST["up_mode"];
//print "up_mode = ".$up_mode."<br>\n";
	
	//▼入金モード
	if($up_mode != "")
	{
		$inp_money = $_REQUEST["inp_money"];
		$ope_coin = $_REQUEST["ope_coin"];
		$ope_money = $_REQUEST["ope_money"];
		$ope_diff = $_REQUEST["ope_diff"];
		$fk_shiharai_type_id = $_REQUEST["fk_shiharai_type_id"];
		$ope_f_coin = $_REQUEST["ope_f_coin"];
/*
print "inp_money = ".$inp_money."<br>\n";
print "ope_coin = ".$ope_coin."<br>\n";
print "ope_money = ".$ope_money."<br>\n";
print "ope_diff = ".$ope_diff."<br>\n";
*/
		
		//▼処理モード
		switch($up_mode)
		{
			case "bank":
						$f_coin = $ope_coin;
						$f_free_coin = $ope_f_coin;
						$f_total_pay = $ope_money;
						$f_over_money = $ope_diff;
						$f_status = 0;
						$f_coin_add = $f_coin;
						break;
			case "free":
						$f_coin = 0;
						$f_free_coin = $ope_coin;
						$f_total_pay = $ope_money;
						$f_over_money = $ope_diff;
						$f_status = 2;
						$f_coin_add = $f_free_coin;
						break;
			case "over":
						$f_coin = $ope_coin;
						$f_free_coin = $ope_f_coin;
						$f_total_pay = $ope_money;
						$f_over_money = $ope_diff;
						$f_status = 0;
						$f_coin_add = $f_coin;
						break;
			case "coin":
						$f_coin = $ope_coin;
						$f_free_coin = $ope_f_coin;
						$f_total_pay = $ope_money;
						$f_over_money = $ope_diff;
						$f_status = 0;
						$f_coin_add = $f_coin;
						break;
			default:
						$f_coin = 0;
						$f_free_coin = 0;
						$f_total_pay = 0;
						$f_over_money = 0;
						$f_status = 0;
						$f_coin_add = $f_coin;
						break;
		}
		
/*
		//会員コイン情報加算関数
		AddTMemberCoinData($mid, $f_coin, $f_free_coin, $f_total_pay, $f_over_money);
		//会員親紹介コード取得関数
		GetTMemberFKParentAd($mid,$tmm_fk_parent_ad);
		//会員コイン情報取得関数
		GetTMemberCoinData($mid,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_over_money);
		//▼現在コインの合計を取得
		$f_coin_result = $tm_f_coin + $tm_f_free_coin;
		//支払履歴記録関数
		$f_pay_money = $ope_money;
//		$fk_shiharai_type_id = 1;
		//$ret = InsertTPayLog($mid,$f_status,$sid,$f_pay_money,$f_coin_add,$fk_shiharai_type_id,$f_coin_result,$tmm_fk_parent_ad);
		$ret = InsertTPayLog($mid,$f_status,$sid,$f_pay_money,$f_coin,$fk_shiharai_type_id,$f_coin_result,$tmm_fk_parent_ad,$f_free_coin);
		if($ret == false)
		{
			$dsp_string = "入金処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "会員の入金処理に成功しました。<br>\n";
		}
*/
		//会員親紹介コード取得関数
		GetTMemberFKParentAd($mid,$tmm_fk_parent_ad);
		//会員コイン情報取得関数
		GetTMemberCoinData($mid,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tm_f_over_money);
		//▼現在コインの合計を取得
//G.Chin 2010-07-12 chg sta
//		$f_coin_result = $tm_f_coin + $tm_f_free_coin;
		$f_coin_result = $tm_f_coin + $tm_f_free_coin + $f_coin + $f_free_coin;
//G.Chin 2010-07-12 chg end
		//会員入金処理関数
		$f_pay_money = $ope_money;
		$ret = PaymentTMemberCoin($mid,$f_coin,$f_free_coin,$f_total_pay,$f_over_money,$f_status,$sid,$f_pay_money,$f_coin,$fk_shiharai_type_id,$f_coin_result,$tmm_fk_parent_ad);
		if($ret == false)
		{
			$dsp_string = "会員の入金処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "会員の入金処理に成功しました。<br>\n";

                        // ゲーム関係の処理
                        if (
                            ( $up_mode == 'coin' || $up_mode == 'bank' || $up_mode == 'over' )
                            && defined( 'GAME_ENABLE' ) && constant( 'GAME_ENABLE' )
                            && defined( 'GAME_ID_A' )
                            && is_readable( realpath(COMMON_LIB.'game_lib.php') )
                        ) {
                            $msg = 'game option is effective.';
                            syslog( LOG_INFO, $msg );
                            error_log( 'success message:'.$msg );
                            require_once( realpath(COMMON_LIB.'game_lib.php') );
                            $db = db_connect();
                            $options = array();
                            if ( defined( 'GAME_ITEMS_A' ) ) {
                                $options['const.prize_items'] = GAME_ITEMS_A;
                            }
                            $game = new GameModel( $db, GAME_ID_A, $options );
                            if ( $game ) {

                                // プレイ権利付与と当時に抽選処理を行うため、ボーナス効果を先に付与してください
                                // ボーナス効果を即時利用するために、プレイ権利の付与に先駆けてボーナス効果の付与処理をコミットする必要があります。

                                // ボーナス効果
                                mysql_query("START TRANSACTION", $db);
                                $game->putGameBonusOfCoin( $db, $mid, ($f_coin + $f_free_coin) );
                                mysql_query("COMMIT", $db);

                                // プレイ権利付与
                                mysql_query("START TRANSACTION", $db);
                                $game->putPlayTicketOfCoin( $db, $mid );
                                mysql_query("COMMIT", $db);

                            }
                        } else {
                            $msg = 'game option is invalid.';
                            syslog( LOG_INFO, $msg );
                            error_log('success message:'.$msg);
                        }
		}
		
		if($up_mode != "free")
		{
			//会員広告コード取得関数
			//GetTMemberFKAdcodeId($mid,$tmm_fk_adcode_id);
			GetTMemberFKParentAd($mid,$tmm_fk_adcode_id);
			//広告コード取得関数
			GetTAdcodeInfo($tmm_fk_adcode_id,$tac_f_adcode,$tac_fk_admaster_id,$tac_f_type,$tac_f_tm_stamp);
			//広告集計更新関数
			$f_type = 0;
			$f_pc_paycoin = $ope_money;
			$f_pc_memreg = 0;
			$f_mb_memreg = 0;
			$f_pc_access = 0;
			$f_mb_access = 0;
			$f_mb_paycoin = 0;
			UpdateTStatAd($tac_fk_admaster_id,$f_type,$f_pc_memreg,$f_mb_memreg,$f_pc_access,$f_mb_access,$f_pc_paycoin,$f_mb_paycoin);
		}
		GetShiharaiId($fk_shiharai_type_id,$fk_shiharai_id);
		//売上集計コイン購入更新関数
		$f_type = 0;
		$f_buycoin = $ope_money;
		$buycoin_value = $ope_money;
		$coin_num = $fk_shiharai_id;
		UpdateTStatSalesBuyCoin($f_type,$f_buycoin,$buycoin_value,$coin_num);
		
		//▼アフィリエイト表示タグ初期化
		$affiliate_tag = "";	//G.Chin 2010-07-23 add
		
		//▼親紹介コード判定
		if(($up_mode != "free") && ($up_mode != "over"))
		{
			if($tmm_fk_parent_ad != "")
			{
				//広告コード取得関数
				GetTAdcodeInfo($tmm_fk_parent_ad, $pa_f_adcode, $pa_fk_admaster_id, $pa_f_type, $pa_f_tm_stamp);
				if($pa_f_type == 0)
				{
					//▼親紹介コードが会員広告コードの場合
					//親会員無料コイン加算関数
					AddParentMemberFreeCoin($mid, $tmm_fk_parent_ad);
				}
//G.Chin 2010-07-23 add sta
//G.Chin 2010-07-28 chg sta
/*
				//▼アフィリエイト判定
				//会員メールアドレス取得関数
				GetTMemberMailAddress($mid,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb);
				if(($tmm_f_mail_address_pc != "") && ($tmm_f_mail_address_mb == ""))
				{
					$mail_type = 0;
					$affiliate_tag = "<!--A8.net:アフィリエイトのタグ--><br>\n";
				}
				else if(($tmm_f_mail_address_pc == "") && ($tmm_f_mail_address_mb != ""))
				{
					$mail_type = 1;
					$affiliate_tag = "<!--MOBA8.net:アフィリエイトのタグ--><br>\n";
				}
				else
				{
					$mail_type = 99;
					$affiliate_tag = "";
				}
				
				//アフィリエイト広告情報取得関数
				GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
				//アフィリエイト広告購入報告判定関数
				CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
				if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($taf_f_type == $mail_type) && ($not_report == 0))
				{
					//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
					//▼会員アドレスがPCなら種類が"A8"、MBなら種類が"MOBA8"の場合
					$so = $mid;
					//アフィリエイト用支払履歴情報取得関数
					GetAffiliateTPayLogInfo($so,$recent_money,$post_money,$all_count);
					if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
					{
						//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
						$pid = $taf_fk_program_id;
						//▼SIパラメータの有無を判定
						if($taf_f_param2 == "") {
							if($mail_type == 0)
							{
								$affiliate_tag .= "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=1.1.1.coin' width='1' height='1'><br>\n";
							}
							else if($mail_type == 1)
							{
								$affiliate_tag .= "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=1.1.1.coin' width='1' height='1'><br>\n";
							}
						}
						else {
							$si = $taf_f_param2;
							if($mail_type == 0)
							{
								$affiliate_tag .= "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=$si' width='1' height='1'><br>\n";
							}
							else if($mail_type == 1)
							{
								$affiliate_tag .= "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=$si' width='1' height='1'><br>\n";
							}
						}
						
						//アフィリエイト広告購入報告数加算関数
						CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);	//G.Chin 2010-07-26 add
					}
					else {
						$affiliate_tag .= "";
					}
				}
				else {
					//▼状態が"無効"の場合
					$affiliate_tag .= "";
				}
*/
//G.Chin 2010-09-16 chg sta
/*
				//アフィリエイトタグ作成関数
				MakeAffiliateTag($mid,1,2,$affiliate_tag);
*/
				//アフィリエイト用支払履歴情報取得関数
				GetAffiliateTPayLogInfo($mid,$recent_money,$post_money,$all_count);
				if($post_money == 0)
				{
					//初回銀行振込フラグ更新関数
					UpdateMemberFirstBank($mid, 1);
				}
//G.Chin 2010-09-16 chg end
//G.Chin 2010-07-28 chg end
				
				//アフィリエイト広告コイン購入回数加算関数
				CountUpTAffiliateFBuyCount($tmm_fk_parent_ad);
				//アフィリエイト広告コイン購入回数加算関数２
				CountUpTAffiliateFBuyCount2($tmm_fk_parent_ad);	//G.Chin 2010-11-27 add
//G.Chin 2010-07-23 add end
			}
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>　　$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= $affiliate_tag;	//G.Chin 2010-07-23 add
	
	//管理画面入力ページ表示関数
	PrintAdminPage("入金処理完了",$dsp_tbl);

?>
