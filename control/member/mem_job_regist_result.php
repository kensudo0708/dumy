<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Li												*/
/*		作成日		:	2010/09/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	$file_class = HOME_PATH."mvc/utils/File.class.php";
	$string_class = HOME_PATH."mvc/utils/StringBuilder.class.php";
	$constkeys_class = HOME_PATH."mvc/config/ConstKeys.class.php";

	$fk_job_id = $_REQUEST["job_id"];
	$fk_job_name = $_REQUEST["f_name"];
        $f_status = $_REQUEST["f_status"];
        $now = date("Y-m-d H:i:s", time());
	if($fk_job_id == "")
	{
		//print "不正な処理です。<br>\n";
		PrintAdminPage("会員職業登録完了","<P>不正な処理です</P>");
		exit;
	}
	else if($fk_job_id == "-1")
	{       
		//商品テンプレート登録関数
		$ret = RegistMemJob($fk_job_name,$now,$f_status);
		if($ret == false)
		{
			$dsp_string = "登録処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "会員職業新規登録処理に成功しました。<br>\n";
		}
	}
	else
	{
		//商品テンプレート更新関数
		$ret = UpdatetMemJob($fk_job_id,$fk_job_name,$f_status);
		if($ret == false)
		{
			$dsp_string = "更新処理に失敗しました。<br>\n";
		}
		else
		{
			$dsp_string = "職業の更新処理に成功しました。<br>\n";
		}
	}

	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
	$dsp_tbl .= "<font size='-1'>下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("職業登録完了",$dsp_tbl);

?>
