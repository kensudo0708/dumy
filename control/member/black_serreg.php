<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$bsid = $_REQUEST["bsid"];
	
	if(($bsid != "") && ($bsid != 0))
	{
		//携帯識別番号情報取得関数
		GetTBSerInfo($bsid,$tbm_f_ser,$tbm_f_base,$tbm_f_status,$tbm_f_tm_stamp);
	}
	else
	{
		$tbm_fk_black_member_ser_id	= "";
		$tbm_f_ser					= "";
		$tbm_f_base					= 0;
		$tbm_f_status				= "";
		$tbm_f_tm_stamp				= "";
	}
	
	//▼状態
	$f_status_id[0] = "0";
	$f_status_name[0] = "配信不可";
	$f_status_id[1] = "1";
	$f_status_name[1] = "入会不可";
	$f_status_id[2] = "9";
	$f_status_name[2] = "ﾌﾞﾗｯｸ保留";
	$f_status_cnt = 3;
	$name = "f_status";
	$select_num = $tbm_f_status;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $status_select);
	
	//▼ブラック登録元
	switch($tbm_f_base)
	{
		case 0:		$base_str = "番組ﾌﾞﾗｯｸ";	break;
		case 1:		$base_str = "共有ﾌﾞﾗｯｸ";	break;
		default:	$base_str = "";				break;
	}
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='black_serreg_result.php' method='GET' ENCTYPE='multipart/form-data'>\n";

        $dsp_tbl .= "<fieldset>\n";
        $dsp_tbl .= "<legend>携帯識別番号編集</legend>\n";
        $dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>ID</th>\n";
	$dsp_tbl .= "<td>$bsid</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>識別番号</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_ser' value='$tbm_f_ser' size='50'></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>状態</th>\n";
	$dsp_tbl .= "<td>$status_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>登録元</th>\n";
	$dsp_tbl .= "<td>$base_str</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
        $dsp_tbl .= "</fieldset>\n";


//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<div style = 'text-align:center;'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='bsid' value='$bsid'>\n";
	$dsp_tbl .= "<input type='hidden' name='f_base' value='$tbm_f_base'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='登録'>&nbsp;&nbsp;\n";
	$dsp_tbl .= "<input type='button' class = 'button1' value='閉じる' onClick='window.close();'>\n";
        $dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</div><br>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("携帯識別番号編集",$dsp_tbl);

?>
