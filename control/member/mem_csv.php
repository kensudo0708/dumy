<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/09/06												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
	
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	$inp_type = $_REQUEST["inp_type"];
	
	//▼種別判定
	if($inp_type == 0)
	{
		//仮登録
		$dsp_tbl  = "";
		$dsp_tbl .= "<CENTER>生成ファイル一覧</CENTER>\n";
		$dsp_tbl .= "<HR>\n";
		$dsp_tbl .= "今までに生成されたファイルを表示します。<BR>\n";
		$dsp_tbl .= "ダウンロードしたいファイル名を右クリックして「対象をファイルに保存」でご使用のPCに保存して下さい。\n";
		$dsp_tbl .= "<HR>\n";
		$FileDir = KARI_DEL_PATH;
print "FileDir = [".$FileDir."]<br>\n";
		$FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
//		system("`ssh root@116.58.177.7 rm -f /home/auction/script/command/log/*`", $FileList);
		for($i=0; $FileList[$i]!=""; $i++)
		{
			$filename = str_replace("$FileDir", "", $FileList[$i]);
			
			$url = KARI_DEL_URL.$filename;
print "url = ".$url."<br>\n";
			$dsp_tbl .= "<A HREF='$url' target=_blank>$filename</A><BR>\n";
		}
		$dsp_tbl .= "<HR>\n";
	}
	else
	{
		//会員
		
		$mode = $_REQUEST["mode"];
		
		//▼会員グループ選択作成
		//会員グループ名一覧取得関数
		GetFMGroupNameList($fmg_fk_member_group_id, $fmg_f_member_group_name, $fmg_data_cnt);
		$inp_group_id[0] = "";
		$inp_group_name[0] = "(指定しない)";
		for($i=0; $i<$fmg_data_cnt; $i++)
		{
			$inp_group_id[$i+1] = $fmg_fk_member_group_id[$i];
			$inp_group_name[$i+1] = $fmg_f_member_group_name[$i];
		}
		$inp_group_cnt = $fmg_data_cnt + 1;
		$name = "inp_group";
		$select_num = "";
		//選択オブジェクト作成関数
		MakeSelectObject($name, $inp_group_id, $inp_group_name, $inp_group_cnt, $select_num, $group_select);
		
		$dsp_tbl  = "";
		
		$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mem_csv.php' method='GET' target='main_r'>\n";
		$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=90%>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><tt>状態</tt></td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<SELECT name='inp_activity'>\n";
		$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
		$dsp_tbl .= "<OPTION value='0'>仮登録\n";
		$dsp_tbl .= "<OPTION value='1'>会員\n";
		$dsp_tbl .= "<OPTION value='2'>退会者\n";
		$dsp_tbl .= "</SELECT>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "会員ID";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_mem_id' istyle='4'></input>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "ﾛｸﾞｲﾝID/ﾊﾝﾄﾞﾙ名";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_id_name' size='15' value=''>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "氏名";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_name' size='15' value='' maxlength='256'>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "広告/紹介ｺｰﾄﾞ";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_adcode' size='15' value='' maxlength='256'>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><tt>会員グループ</tt></td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>$group_select</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "メールアドレス";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_mail' size='15' value=''>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "登録日(yyyymmdd)";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_s_regdate' size='10' value=''>から\n";
		$dsp_tbl .= "<input type='text' name='inp_e_regdate' size='10' value=''>まで\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><tt>登録媒体</tt></td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<SELECT name='inp_regpos'>\n";
		$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
		$dsp_tbl .= "<OPTION value='0'>PC\n";
		$dsp_tbl .= "<OPTION value='1'>MB\n";
		$dsp_tbl .= "<OPTION value='2'>STAFF\n";
		$dsp_tbl .= "</SELECT>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "残り購入ｺｲﾝ数";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_s_coin' size='3' value=''> ～ \n";
		$dsp_tbl .= "<input type='text' name='inp_e_coin' size='3' value=''>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "残りｻｰﾋﾞｽｺｲﾝ数";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_s_free' size='3' value=''> ～ \n";
		$dsp_tbl .= "<input type='text' name='inp_e_free' size='3' value=''>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "合計ｺｲﾝ購入金額";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<input type='text' name='inp_s_totalpay' size='3' value=''>円 ～ \n";
		$dsp_tbl .= "<input type='text' name='inp_e_totalpay' size='3' value=''>円\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><tt>性別</tt></td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td>\n";
		$dsp_tbl .= "<SELECT name='inp_sex'>\n";
		$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
		$dsp_tbl .= "<OPTION value='0'>男性\n";
		$dsp_tbl .= "<OPTION value='1'>女性\n";
		$dsp_tbl .= "</SELECT>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><tt>ソート</tt></td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='left'><tt><input type='radio' name='sort' value='0' >会員ID(昇順)</input><BR>";
		$dsp_tbl .= "<input type='radio' name='sort' value='1' checked>会員ID(降順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='2' >コイン購入金額(昇順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='3' >コイン購入金額(降順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='6' >商品購入金額(昇順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='7' >商品購入金額(降順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='4' >ﾛｸﾞｲﾝ日時(昇順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='5' >ﾛｸﾞｲﾝ日時(降順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='10' >最終購入日(昇順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='11' >最終購入日(降順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='12' >購入回数(昇順)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='sort' value='13' >購入回数(降順)</input><BR></tt></td>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<td align='center' colspan=2>\n";
		$dsp_tbl .= "<input type='hidden' name='inp_type' value='1'>\n";
		$dsp_tbl .= "<input type='hidden' name='mode' value='exec'>\n";
		$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='会員検索'>\n";
		$dsp_tbl .= "</td>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "</table>\n";
		$dsp_tbl .= "</form>\n";
		
		//▼検索モード
		if($mode == "exec")
		{
			$inp_activity = $_REQUEST["inp_activity"];
			$inp_id_name = $_REQUEST["inp_id_name"];
			$inp_name = $_REQUEST["inp_name"];
			$inp_adcode = $_REQUEST["inp_adcode"];
			$inp_group = $_REQUEST["inp_group"];
			$inp_mail = $_REQUEST["inp_mail"];
			$inp_s_regdate = $_REQUEST["inp_s_regdate"];
			$inp_e_regdate = $_REQUEST["inp_e_regdate"];
			$inp_regpos = $_REQUEST["inp_regpos"];
			$inp_s_coin = $_REQUEST["inp_s_coin"];
			$inp_e_coin = $_REQUEST["inp_e_coin"];
			$inp_s_free = $_REQUEST["inp_s_free"];
			$inp_e_free = $_REQUEST["inp_e_free"];
			$inp_s_totalpay = $_REQUEST["inp_s_totalpay"];
			$inp_e_totalpay = $_REQUEST["inp_e_totalpay"];
			$inp_sex = $_REQUEST["inp_sex"];
			$inp_mem_id = $_REQUEST["inp_mem_id"];
			$sort = $_REQUEST["sort"];
			
			//▼登録日
			if($inp_s_regdate != "")
			{
				//日付文字列作成関数
				MakeDateString($inp_s_regdate, $inp_s_regist);
			}
			else
			{
				$inp_s_regist = "";
			}
			if($inp_e_regdate != "")
			{
				//日付文字列作成関数
				MakeDateString($inp_e_regdate, $inp_e_regist);
			}
			else
			{
				$inp_e_regist = "";
			}
		    
			//広告コードID取得関数
			GetTAdcodeId($inp_adcode, $inp_parent_ad);
			
			$dist_path = MNTSTAFF_PATH;
			
			//会員CSV作成関数
			MakeMemberCSV($dist_path,$inp_activity,$inp_id_name,$inp_parent_ad,$inp_group,$inp_mail,$inp_s_regist,$inp_e_regist,$inp_regpos,$inp_s_coin,$inp_e_coin,$inp_s_free,$inp_e_free,$inp_s_totalpay,$inp_e_totalpay,$inp_sex,$inp_mem_id,$inp_name,$sort,$csv_file_name,$csv_all_count);
			
			$dsp_tbl .= "<HR>\n";
			$dsp_tbl .= "<CENTER>生成ファイル一覧</CENTER>\n";
			$dsp_tbl .= "<HR>\n";
			$dsp_tbl .= "今までに生成されたファイルを表示します。<BR>\n";
			$dsp_tbl .= "ダウンロードしたいファイル名を右クリックして「対象をファイルに保存」でご使用のPCに保存して下さい。\n";
			$dsp_tbl .= "<HR>\n";
			
			$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='3' cellpadding='3'>\n";
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<th><font size='-1'>データ件数</font></th>\n";
			$dsp_tbl .= "<th><font color='#5D478B' size='-1'>$csv_all_count</font></th>\n";
			$dsp_tbl .= "</tr>\n";
			$dsp_tbl .= "</table>\n<br>\n";
			
			$FileDir = MNTSTAFF_PATH;
			$FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
			for($i=0; $FileList[$i]!=""; $i++)
			{
				$filename = str_replace("$FileDir", "", $FileList[$i]);
				
				$url = MNTSTAFF_URL.$filename;
				$dsp_tbl .= "<A HREF='$url' target=_blank>$filename</A><BR>\n";
			}
			$dsp_tbl .= "<HR>\n";
		}
	}
	
	//管理画面入力ページ表示関数
	PrintAdminPage("CSV出力",$dsp_tbl);

?>
