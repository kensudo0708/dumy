<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$qcid = $_REQUEST["qcid"];
	
	if(($qcid != "") && ($qcid != 0))
	{
		//質問カテゴリ名取得関数
		GetTQuestionCategoryName($qcid, $tqc_f_category_name);
		$up_mode = "update";
	}
	else
	{
		$tqc_f_category_name = "";
		$up_mode = "insert";
	}
	
	$link_back = "<A href='que_cate_list.php?up_mode='>カテゴリリストに戻る</A>\n";
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='que_cate_list.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table class = 'list' style = 'margin-top:10px;'>\n";
	
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:100px;'>カテゴリ</th>\n";
	$dsp_tbl .= "<td style = 'width:250px;'><input type='text' name='f_category_name' value='$tqc_f_category_name' size='50'></td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "<br>\n";
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='$up_mode'>\n";
	$dsp_tbl .= "<input type='hidden' name='qcid' value='$qcid'>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='登録'>\n";
	$dsp_tbl .= "</FORM>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<br>\n";
	$dsp_tbl .= $link_back;
	
	//管理画面入力ページ表示関数
	PrintAdminPage("問合せ管理",$dsp_tbl);

?>
