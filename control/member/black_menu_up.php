<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	
	$dsp_tbl  = "";

        $dsp_tbl .= "<table class = 'main_l'>";
	$dsp_tbl .= "<caption>&#x2462;ブラックリスト管理</caption>\n";
	
        $dsp_tbl .= "<tr>";
        $dsp_tbl .= "<td style = 'text-align:center;'>";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='black_srcmail.php' method='GET' target='menu_d'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='メール管理'>\n";
	$dsp_tbl .= "</form>\n";
        $dsp_tbl .= "</td>";
        $dsp_tbl .= "</tr>";
	
        $dsp_tbl .= "<tr>";
        $dsp_tbl .= "<td style = 'text-align:center;'>";
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='black_srcser.php' method='GET' target='menu_d'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:120; border-color:#FFFAFA' value='携帯識別番号管理'>\n";
	$dsp_tbl .= "</form>\n";
        $dsp_tbl .= "</td>";
        $dsp_tbl .= "</tr>";
        $dsp_tbl .= "</table>";
	
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
