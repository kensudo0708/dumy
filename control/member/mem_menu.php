<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/09												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	session_start();
	$sid = $_SESSION["staff_id"];

	//▼会員グループ選択作成
	//会員グループ名一覧取得関数
	GetFMGroupNameList($fmg_fk_member_group_id, $fmg_f_member_group_name, $fmg_data_cnt);
	$inp_group_id[0] = "";
	$inp_group_name[0] = "(指定しない)";
	for($i=0; $i<$fmg_data_cnt; $i++)
	{
		$inp_group_id[$i+1] = $fmg_fk_member_group_id[$i];
		$inp_group_name[$i+1] = $fmg_f_member_group_name[$i];
	}
	$inp_group_cnt = $fmg_data_cnt + 1;
	$name = "inp_group";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_group_id, $inp_group_name, $inp_group_cnt, $select_num, $group_select);

	//地域一覧取得関数
	GetAreaNameList_search($fmg_fk_area_id, $fmg_area_name, $fmg_data_cnt);

	//地域選択オブジェクト作成関数
	$name = "inp_area";
	MakeSelectObject($name, $fmg_fk_area_id, $fmg_area_name, $fmg_data_cnt, $select_num, $area_select);

	//職業名一覧取得関数
	GetJobNameList_search($fmg_fk_Job_id, $fmg_Job_name, $fmg_data_cnt);
	//職業選択オブジェクト作成関数
	$name = "inp_job";
	MakeSelectObject($name, $fmg_fk_Job_id, $fmg_Job_name, $fmg_data_cnt, $select_num, $job_select);

	$dsp_tbl  = "";

	$dsp_tbl .= "<table class='main_l'>\n";
//	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<caption>&#x2460;会員管理</caption>\n";
//	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='新規登録' type='button' onclick=\"OpenWin('mem_regist.php?sid=$sid&mid=0')\">\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
//	$dsp_tbl .= "</table>\n";

	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mem_list.php' method='GET' target='main_r'>\n";
//	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width='90%' class='main_l'>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>状態</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_activity'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>仮登録\n";
	$dsp_tbl .= "<OPTION value='1'>会員\n";
	$dsp_tbl .= "<OPTION value='2'>退会者\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "会員ID";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_mem_id' istyle='4'></input>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "ﾛｸﾞｲﾝID/ﾊﾝﾄﾞﾙ名";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_id_name' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "氏名";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_name' size='15' value='' maxlength='256'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "広告/紹介ｺｰﾄﾞ";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_adcode' size='15' value='' maxlength='256'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='left'><tt>会員グループ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$group_select</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "メールアドレス";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_mail' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "IPアドレス";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_ip' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "未払い商品数";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_unpain' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "キャンセル商品数";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_cancel' size='15' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "登録日(yyyymmdd)";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_s_regdate' size='10' value=''>から\n";
	$dsp_tbl .= "<input type='text' name='inp_e_regdate' size='10' value=''>まで\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "最終ログイン日(yyyymmdd)";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_s_last_entry' size='10' value=''>から\n";
	$dsp_tbl .= "<input type='text' name='inp_e_last_entry' size='10' value=''>まで\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='left'><tt>登録媒体</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_regpos'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>PC\n";
	$dsp_tbl .= "<OPTION value='1'>MB\n";
	$dsp_tbl .= "<OPTION value='2'>STAFF\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "残り購入ｺｲﾝ数";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_s_coin' size='3' value=''> ～ \n";
	$dsp_tbl .= "<input type='text' name='inp_e_coin' size='3' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "残りｻｰﾋﾞｽｺｲﾝ数";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_s_free' size='3' value=''> ～ \n";
	$dsp_tbl .= "<input type='text' name='inp_e_free' size='3' value=''>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>\n";
	$dsp_tbl .= "合計ｺｲﾝ購入金額";
	$dsp_tbl .= "</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='text' name='inp_s_totalpay' size='3' value=''>円 ～ \n";
	$dsp_tbl .= "<input type='text' name='inp_e_totalpay' size='3' value=''>円\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='left'><tt>性別</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<SELECT name='inp_sex'>\n";
	$dsp_tbl .= "<OPTION value=''>(指定しない)\n";
	$dsp_tbl .= "<OPTION value='0'>男性\n";
	$dsp_tbl .= "<OPTION value='1'>女性\n";
	$dsp_tbl .= "</SELECT>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='left'><tt>地域</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$area_select</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='left'><tt>職業</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>$job_select</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>ソート</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='left'><tt><input type='radio' name='sort' value='0' >会員ID(昇順)</input><BR>";
	$dsp_tbl .= "<input type='radio' name='sort' value='1' checked>会員ID(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='2' >コイン購入金額(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='3' >コイン購入金額(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='6' >商品購入金額(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='7' >商品購入金額(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='4' >ﾛｸﾞｲﾝ日時(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='5' >ﾛｸﾞｲﾝ日時(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='10' >最終購入日(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='11' >最終購入日(降順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='12' >購入回数(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='13' >購入回数(降順)</input><BR>\n";
        $dsp_tbl .= "<input type='radio' name='sort' value='14' >未払い商品数(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='15' >未払い商品数(降順)</input><BR>\n";
        $dsp_tbl .= "<input type='radio' name='sort' value='16' >キャンセル商品数(昇順)</input><BR>\n";
	$dsp_tbl .= "<input type='radio' name='sort' value='17' >キャンセル商品数(降順)</input><BR>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>表示ﾀｲﾌﾟ</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='left'>\n";
	$dsp_tbl .= "<tt>\n";
	$dsp_tbl .= "<input type='radio' name='inp_type' value='0' checked>一覧</input><BR>\n";

//G.Chin 2010-09-28 chg sta
/*
	//人事情報取得関数
	GetTStaffInfo($sid,$ts_f_login_id,$ts_f_login_pass,$ts_f_entry_time,$ts_f_status,$ts_f_tm_stamp);
	//▼最高権限と開発者のみ表示
	if(($ts_f_status == 2) || ($ts_f_status == 8))
	{
*/
	//スタッフ管理者権限判定関数
	$staff_csv = CheckStaffTAuthority($sid,"30");
	//▼会員CSV出力権限
	if($staff_csv == true)
	{
		$dsp_tbl .= "<input type='radio' name='inp_type' value='1' >CSV(会員)</input><BR>\n";
		$dsp_tbl .= "<input type='radio' name='inp_type' value='2' >CSV(仮登録)</input><BR>\n";
	}

	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "</tt>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='50'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='order' value=''>\n";
	//$dsp_tbl .= "<input type='hidden' name='sort' value='desc'>\n";
	$dsp_tbl .= "<input type='hidden' name='page' value=''>\n";
//G.Chin AWKC-218 2010-11-12 add sta
	$dsp_tbl .= "<input type='hidden' name='save_flg' value='0'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_file_num' value='0'>\n";
//G.Chin AWKC-218 2010-11-12 add end
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='会員検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";

/*
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mem_robot.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='ﾛﾎﾞｯﾄ名修正'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";



	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='mem_robot2.php' method='GET' target='main_r'>\n";
	$dsp_tbl .= "<table border='1' bordercolor='#CCCCCC' cellspacing='1' cellpadding='1' width=80%>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='ﾛﾎﾞｯﾄ名重複修正'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</form>\n";
*/

	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
