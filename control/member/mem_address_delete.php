<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/11												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$aid = $_REQUEST["aid"];
	$mid = $_REQUEST["mid"];
	
	//会員住所取得関数
	GetTAddressInfo($aid,$ta_f_status,$ta_fk_member_id,$ta_f_last_send,$ta_f_name,$ta_f_tel_no,$ta_f_post_code,$ta_fk_perf_id,$ta_f_address1,$ta_f_address2,$ta_f_address3,$ta_f_tm_stamp);
	
	$post_before = substr($ta_f_post_code, 0, 3);
	$post_after = substr($ta_f_post_code, 3, 4);
	$post_str  = "";
	$post_str .= "〒";
	$post_str .= $post_before;
	$post_str .= "-";
	$post_str .= $post_after;
	
	//都道府県名取得関数
	GetTPrefName($ta_fk_perf_id, $tp_t_pref_name);
	
	$address_str  = "";
	$address_str .= $post_str;
	$address_str .= " ";
	$address_str .= $tp_t_pref_name;
	$address_str .= $ta_f_address1;
	$address_str .= $ta_f_address2;
	$address_str .= " ";
	$address_str .= $ta_f_address3;
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<font size='-1'>[";
	$dsp_tbl .= $aid;
	$dsp_tbl .= ": [";
	$dsp_tbl .= $address_str;
	$dsp_tbl .= "] の住所を削除します。よろしければ、下のボタンをクリックしてください。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<FORM action='mem_address.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='delete'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='aid' value='$aid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='削除実行'>\n";
	$dsp_tbl .= "</FORM>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("配送先住所削除",$dsp_tbl);

?>
