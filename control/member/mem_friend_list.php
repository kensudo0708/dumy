<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	Shoji													*/
/*		作成日		:	2010/06/26												*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	include '../control/exec_select.php';
	
	session_start();
	$sid = $_SESSION['staff_id'];
	
	$mid = $_REQUEST["id"];
	$page = $_REQUEST["page"];
	$offset=50;
	$limit=$offset*($page-1);
	
	$rs=getmem($mid);

        $ret=mysql_fetch_array($rs);
        $adcode_id=$ret['fk_adcode_id'];
        $f_handle=$ret['f_handle'];

	$disp_tab  = "<BR><BR>\n";
	
	$disp_tab .= "<table class=\"form\">\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<th>会員ID</th>\n";
	$disp_tab .= "<td>$mid</td>\n";
	$disp_tab .= "</tr>\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<th>ハンドル</th>\n";
	$disp_tab .= "<td>$f_handle</td>\n";
	$disp_tab .= "</table>\n";
	$disp_tab .= "<BR><BR>\n";
        $disp_tab .= "<center>紹介者</center>\n";
        $rs=Getchildmem_all($adcode_id);
	$all_count=mysql_num_rows($rs);

        $rs=Getchildmem($adcode_id,$limit,$offset);
	$data_cnt=mysql_num_rows($rs);
	$disp_tab .= "<center>\n";
	$disp_tab .= Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/member/mem_frend_list.php?id=$mid", 5);
	$disp_tab .= "</center>\n";
	
	$disp_tab .= "<table class=\"list\">\n";
	$disp_tab .= "<tr>\n";
	$disp_tab .= "<th style=\"width:60px\">ID</th>\n";
	$disp_tab .= "<th style=\"width:180px\">ハンドル名</th>\n";
	$disp_tab .= "<th style=\"width:240px\">メールアドレス(PC)</th>\n";
        $disp_tab .= "<th style=\"width:240px\">メールアドレス(MB)</th>\n";
	$disp_tab .= "</tr>\n";
	
	for($i=0;$i<$data_cnt;$i++)
	{
            $ret=mysql_fetch_array($rs);
            $mem_id=$ret['fk_member_id'];
            $hand=$ret['f_handle'];
            $mail_pc=$ret['f_mail_address_pc'];
            $mail_mb=$ret['f_mail_address_mb'];
            $disp_tab .= "<tr>\n";
            $disp_tab .= "<td>$mem_id</td>\n";
            $disp_tab .= "<td>$hand</td>\n";
            $disp_tab .= "<td>$mail_pc</td>\n";
            $disp_tab .= "<td>$mail_mb</td>\n";
            $disp_tab .= "</tr>\n";
        }
	$disp_tab .= "</table>\n";
	$disp_tab .= "<center>\n";
	$disp_tab .= Utils::getPageNumbersII($page, ($offset ? ceil($all_count /$offset):1), "/control/member/mem_frend_list.php?id=$mid", 5);
	$disp_tab .= "</center>\n";
	$disp_tab .= "<BR><BR>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("会員ID:".$mid."による紹介リスト",$disp_tab);

?>
