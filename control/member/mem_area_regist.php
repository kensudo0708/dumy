<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Li													*/
/*		作成日		:	2010/09/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;


	$fk_area_id = $_REQUEST["area_id"];
	$newFlg=false;

	if(isset($fk_area_id) && $fk_area_id != "")
	{
            GetAreaByID($fk_area_id,$f_name,$f_status);

        }
	else
	{
            $newFlg = true;

            $fk_area_id	= "";
            $f_name	= "";
            $f_status	= "";

	}

        $f_status_id[0] = "0";
	$f_status_name[0] = "使用中";
	$f_status_id[1] = "9";
	$f_status_name[1] = "未使用";
	$f_status_cnt = 2;
	$name = "f_status";

	$select_num = $f_status;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $f_status_id, $f_status_name, $f_status_cnt, $select_num, $f_status_select);


	$dsp_tbl  = "";
	$dsp_tbl .= "<fieldset>\n";
	$dsp_tbl .= "<legend>地域編集</legend>\n";
	$dsp_tbl .= "<table class = 'form'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:100px;'>地域ID</th>\n";
	$dsp_tbl .= "<td style = 'width:200px;'>$fk_area_id</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<FORM action='mem_area_regist_result.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>地域名前</th>\n";
	$dsp_tbl .= "<td><input type='text' name='f_name' value='$f_name' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>状態 </tt></th>\n";
        if(isset($fk_area_id) && !empty ($fk_area_id) ){
            $dsp_tbl .= "<td>$f_status_select</font></td>\n";
        }else{
            $dsp_tbl .= "<td>使用中<INPUT TYPE='hidden' NAME='f_status' value='0'></td>\n";
        }

	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";
//	$dsp_tbl .= "<br>\n";

	$dsp_tbl .= "<div style = 'text-align:center;'>\n";

//	$dsp_tbl .= "<tr>\n";
        if($newFlg){
            $dsp_tbl .= "<input type='hidden' name='area_id' value='-1'>\n";
        }else{
            $dsp_tbl .= "<input type='hidden' name='area_id' value='$fk_area_id'>\n";
        }
//	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='更新'>\n";
	$dsp_tbl .= "</FORM>\n";
        if(isset($fk_area_id) && !empty ($fk_area_id)  && $f_status==0){
            $dsp_tbl .= "<FORM action='mem_area_delete.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
            $dsp_tbl .= "<input type='hidden' name='fk_area_id' value='$fk_area_id'>\n";
            $dsp_tbl .= "<input type='submit' class = 'button1' value='削除'>\n";
            $dsp_tbl .= "</FORM>\n";
        }
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
        $dsp_tbl .= "<input type='button' class = 'button1' value='閉じる' onClick='window.close();'>\n";
        $dsp_tbl .= "</div>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("地域編集",$dsp_tbl);

?>
