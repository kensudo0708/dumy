<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/10												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;


	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
	$mode = $_REQUEST["mode"];

	if(($mid != "") && ($mid != 0))
	{
		//会員コイン情報取得関数
		GetTMemberCoinData($mid,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tmm_f_over_money);
	}
	else
	{
		//現在日時
		$now = time();

		$tmm_f_over_money		= "0";
		$tm_f_coin				= "0";
		$tm_f_free_coin			= "0";
		$tm_f_total_pay			= "0";
	}

	//▼会員グループ
	//会員グループID取得関数
	GetTMemberGroupId($mid,$tmm_fk_member_group_id);
	//会員グループ情報取得関数
	GetTMemberGroupInfo($tmm_fk_member_group_id,$tmg_f_member_group_name,$tmg_fk_coin_group_id,$tmg_f_memo,$tmg_f_tm_stamp);

	//コイン売却価格一覧取得関数
	GetTCoinSetupList($tmg_fk_coin_group_id,$tcs_fk_coin_id,$tcs_f_coin,$tcs_fk_shiharai_type_id,$tcs_f_inp_money,$tcs_f_tm_stamp,$tcs_f_free_coin,$tcs_data_cnt);

	$mode_string = "";
	$ope_f_coin = 0;

	//▼処理モード
	switch($mode)
	{
		case "bank":
					$inp_bank = $_REQUEST["inp_bank"];
					$inp_money = $inp_bank;

					//▼購入可能コイン数
					for($i=0; $i<$tcs_data_cnt; $i++)
					{
						if($inp_money < $tcs_f_inp_money[$i])
						{
							if($i == 0)
							{
								$ope_money = 0;
								$ope_coin = 0;
								$ope_f_coin = 0;
							}
							else
							{
								$ope_money = $tcs_f_inp_money[$i-1];
								$ope_coin = $tcs_f_coin[$i-1];
								$ope_f_coin = $tcs_f_free_coin[$i-1];
							}
							break;
						}
					}
					if($i == $tcs_data_cnt)
					{
//G.Chin 2010-08-11 chg sta
/*
						$ope_money = $tcs_f_inp_money[$i];
						$ope_coin = $tcs_f_coin[$i];
						$ope_f_coin = $tcs_f_free_coin[$i];
*/
						$ope_money = $tcs_f_inp_money[$i-1];
						$ope_coin = $tcs_f_coin[$i-1];
						$ope_f_coin = $tcs_f_free_coin[$i-1];
//G.Chin 2010-08-11 chg end
					}
					$total_ad_coin =$ope_coin + $ope_f_coin;
					//▼差額
					$ope_diff = $inp_money - $ope_money;

					//▼表示文字列
					$mode_string .= $inp_money;
					$mode_string .= "円の入金<br><br>\n";
					$mode_string .= "適用料金テーブルは、";
					//$mode_string .= $ope_coin;
                                        $mode_string .= $total_ad_coin;
					$mode_string .= "コイン(";
					$mode_string .= $ope_money;
					$mode_string .= ")<br>\n";
					$mode_string .= "未処理金額は、";
					$mode_string .= $ope_diff;
					$mode_string .= "円<br>\n";

					//▼処理後データ算出
					$after_coin = $tm_f_coin + $ope_coin;
					$after_free_coin = $tm_f_free_coin + $ope_f_coin;
					$after_total_pay = $tm_f_total_pay + $inp_money;
					$after_over_money = $tmm_f_over_money + $ope_diff;

					break;
		case "free":
					$inp_free = $_REQUEST["inp_free"];
					$inp_money = 0;
					$ope_money = 0;
					$ope_coin = $inp_free;

					//▼差額
					$ope_diff = $inp_money - $ope_money;

					//▼表示文字列
					$mode_string .= $ope_coin;
					$mode_string .= "コインのサービスコイン追加<br>\n";

					//▼処理後データ算出
					$after_coin = $tm_f_coin;
					$after_free_coin = $tm_f_free_coin + $ope_coin;
					$after_total_pay = $tm_f_total_pay;
					$after_over_money = $tmm_f_over_money;

					break;
		case "over":
					$inp_over = $_REQUEST["inp_over"];
					$inp_money = 0;
					$ope_money = $tmm_f_over_money;
					$ope_coin = $inp_over;

					//▼差額
					$ope_diff = 0 - $tmm_f_over_money;

					//▼表示文字列
					$mode_string .= $ope_coin;
					$mode_string .= "コイン追加で未処理金額を0円にしました。<br>\n";

					//▼処理後データ算出
					$after_coin = $tm_f_coin + $ope_coin;
					$after_free_coin = $tm_f_free_coin;
					$after_total_pay = $tm_f_total_pay;
					$after_over_money = 0;

					break;
		case "coin":
					$cid = $_REQUEST["cid"];

					//コイン売却価格情報取得関数
					GetTCoinSetupInfo($cid,$tcs_fk_coin_group_id,$tcs_f_coin,$tcs_fk_shiharai_type_id,$tcs_f_inp_money,$tcs_f_tm_stamp,$tcs_f_free_coin);
					$inp_money = $tcs_f_inp_money;
					$ope_money = $tcs_f_inp_money;
					$ope_coin = $tcs_f_coin;
					$ope_f_coin = $tcs_f_free_coin;
					$total_ad_coin = $ope_coin + $ope_f_coin;
					//▼差額
					$ope_diff = $inp_money - $ope_money;

					//▼表示文字列
					$mode_string .= $total_ad_coin;
					$mode_string .= "コイン";
					$mode_string .= $ope_money;
					$mode_string .= "円の料金テーブルを適用しました。<br>\n";

					//▼処理後データ算出
					$after_coin = $tm_f_coin + $ope_coin;
					$after_free_coin = $tm_f_free_coin + $ope_f_coin;
					$after_total_pay = $tm_f_total_pay + $ope_money;
					$after_over_money = $tmm_f_over_money;

					break;
	}

	//▼処理後コイン数のマイナス判定
	if(($after_coin < 0) || ($after_free_coin < 0))
	{
		$dsp_tbl = "この処理は保有コイン数がマイナスになり、不正です。<br>\n";

		//管理画面入力ページ表示関数
		PrintAdminPage("入金処理",$dsp_tbl);
		exit;
	}

	//▼更新画面のモード
	$up_mode = $mode;

	//▼入金処理タイプ選択作成
	//会員支払方法一覧取得関数
	GetMemShiharaiList($fst_fk_shiharai_type_id,$fsp_f_shiharai_name,$fst_data_cnt);
	$name = "fk_shiharai_type_id";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $fst_fk_shiharai_type_id, $fsp_f_shiharai_name, $fst_data_cnt, $select_num, $shiharai_select);


	$dsp_tbl  = "";

	$dsp_tbl .= "<fieldset>";
	$dsp_tbl .= "<legend>入金処理画面</legend>";
	$dsp_tbl .= "<table class = 'list'>\n";

	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>会員</th>\n";
	$dsp_tbl .= "<td colspan = '2' style = 'width:250px;'>$mid</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<FORM action='mem_payresult.php' method='POST' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<tr style = 'height:60px;'>\n";
	$dsp_tbl .= "<th>確認内容</th>\n";
	$dsp_tbl .= "<td colspan = '2'>$mode_string</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'height:25px;'>入金処理タイプ</th>\n";
	$dsp_tbl .= "<td colspan = '2' style = 'text-align:left;'>$shiharai_select</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th></th>\n";
	$dsp_tbl .= "<th>処理前</td>\n";
	$dsp_tbl .= "<th>処理後</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>購入ｺｲﾝ</th>\n";
	$dsp_tbl .= "<td>$tm_f_coin&nbsp;&nbsp;コイン</td>\n";
	$dsp_tbl .= "<td>$after_coin&nbsp;&nbsp;コイン</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>ｻｰﾋﾞｽｺｲﾝ</th>\n";
	$dsp_tbl .= "<td>$tm_f_free_coin&nbsp;&nbsp;コイン</td>\n";
	$dsp_tbl .= "<td>$after_free_coin&nbsp;&nbsp;コイン</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th>購入合計金額</th>\n";
	$dsp_tbl .= "<td>$tm_f_total_pay&nbsp;&nbsp;円</td>\n";
	$dsp_tbl .= "<td>$after_total_pay&nbsp;&nbsp;円</td>\n";
	$dsp_tbl .= "</tr>\n";

	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'height:25px;'>未処理金額</th>\n";
	$dsp_tbl .= "<td>$tmm_f_over_money&nbsp;&nbsp;円</td>\n";
	$dsp_tbl .= "<td>$after_over_money&nbsp;&nbsp;円</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	$dsp_tbl .= "</fieldset>\n";


	$dsp_tbl .= "<div style = 'text-align:center;'>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='$up_mode'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_money' value='$inp_money'>\n";
	$dsp_tbl .= "<input type='hidden' name='ope_coin' value='$ope_coin'>\n";
	$dsp_tbl .= "<input type='hidden' name='ope_f_coin' value='$ope_f_coin'>\n";
	$dsp_tbl .= "<input type='hidden' name='ope_money' value='$ope_money'>\n";
	$dsp_tbl .= "<input type='hidden' name='ope_diff' value='$ope_diff'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";

	$dsp_tbl .= "<input type='submit' class = 'button1' value='確定'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "<FORM action='mem_payment.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='submit' class = 'button1' value='戻る'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</div><br>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("入金処理",$dsp_tbl);

?>
