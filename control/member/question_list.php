<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$utils_class = HOME_PATH."mvc/utils/Utils.class.php";
	include $utils_class;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	$up_mode = $_REQUEST["up_mode"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	$page = $_REQUEST["page"];
	
	//更新モード
	if($up_mode == "update")
	{
		$qid = $_REQUEST["qid"];
		$f_flag = $_REQUEST["f_flag"];
		
		//質問状態更新関数
		UpdateTQuestionFlag($qid, $f_flag);
	}
	
	$inp_flag = $_REQUEST["inp_flag"];
	$inp_category = $_REQUEST["inp_category"];
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	if($page != "")
	{
		$offset = ($page - 1) * $limit;
	}
	
	//質問一覧取得関数
	GetTQuestionList($inp_flag,$inp_category,$limit,$offset,$tq_fk_question_id,$tq_fk_question_category_id,$tq_f_subject,$tq_f_main,$tq_f_name,$tq_fk_member_id,$tq_f_mail_address,$tq_f_flag,$tq_f_recv_dt,$tq_f_check_dt,$tq_f_tm_stamp,$tq_all_count,$tq_data_cnt);
	
	//▼表示開始～終了番号
	if($tq_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tq_data_cnt;
	
	//■表示一覧
	$dsp_tbl  = "";
	
	//■ページ移行リンク
	$current_page = 1;
	$page_link_str = "";
	$page_count = ceil($tq_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
				$current_page = $num;
			}
			else
			{
				$page_link_str .= "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
			}
		}
	}
	
	if($tq_all_count > $limit)
	{
		$page_first = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=0&limit=$limit'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$page_offset[$max_page]&limit=$limit'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tq_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$next_page&limit=$limit'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&offset=$before_page&limit=$limit'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
/*
	$dsp_tbl .= $page_link_str;
*/
	$url = "question_list.php?inp_flag=$inp_flag&inp_category=$inp_category&up_mode=&qid=&f_flag=&limit=$limit&offset=";
	$pageNumber = Utils::getPageNumbersII($current_page,$page_count,$url,5);
	
	$dsp_tbl .= $pageNumber;
	
	$form_str = "";
	if($pageNumber != "")
	{
		$form_str .= "<span class='mypagenum_box4'>\n";
		$form_str .= "<form action='question_list.php' method='get' style='display: inline'>\n";
		$form_str .= "<input type='text' size='2' name='page'/>\n";
		$form_str .= "<input type='submit' value='Jump'/>\n";
		$form_str .= "<input type='hidden' name='sid' value='$sid'>";
		$form_str .= "<input type='hidden' name='mid' value='$mid'>";
		$form_str .= "<input type='hidden' name='ltype' value='$ltype'>";
		$form_str .= "<input type='hidden' name='limit' value='$limit'>";
		$form_str .= "<input type='hidden' name='offset' value=''>";
		$form_str .= "</form>\n";
		$form_str .= "</span>\n";
	}
	$dsp_tbl .= $form_str;
//	$dsp_tbl .= "</center>\n";
	
	$dsp_tbl .= "<table class = 'data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>データ件数</th>\n";
	$dsp_tbl .= "<td style = 'width:50px;'>$tq_all_count</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "$start_num\n";
	$dsp_tbl .= "件目&nbsp;～&nbsp;\n";
	$dsp_tbl .= "$end_num\n";
	$dsp_tbl .= "件目\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table class = 'list'>\n";
	$dsp_tbl .= "<tr style = 'height:25px;'>\n";
	$dsp_tbl .= "<th style = 'width:50px;'>No</th>\n";
	$dsp_tbl .= "<th style = 'width:70px;'>操作</th>\n";	//G.Chin 2010-08-19 chg
	$dsp_tbl .= "<th style = 'width:70px;'>状態</th>\n";
	$dsp_tbl .= "<th style = 'width:100px;'>受付日</th>\n";
	$dsp_tbl .= "<th style = 'width:120px;'>カテゴリ</th>\n";
	$dsp_tbl .= "<th style = 'width:150px;'>会員</th>\n";
	$dsp_tbl .= "<th style = 'width:200px;'>タイトル</th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tq_data_cnt;$i++)
	{
		$bgcolor = "#FFFFFF";
		
		//▼状態
//G.Chin 2010-08-19 chg sta
/*
		switch($tq_f_flag[$i])
		{
			case 0:	$flag_str = "未読"		break;
			case 1:	$flag_str = "既読";		break;
			case 2:	$flag_str = "返信済み";	break;
		}
*/
		switch($tq_f_flag[$i])
		{
			case 0:
					$flag_str = "未読";
					$bgcolor = "#DDA0DD";
					break;
			case 1:
					$flag_str = "既読";
					break;
			case 2:
//G.Chin AWKT-515 chg sta
//					$link_answer = "<A href='question_answer_list.php?sid=$sid&qid=$tq_fk_question_id[$i]&inp_flag=$inp_flag&inp_category=$inp_category&offset=$offset&limit=$limit&page=$page'>返信済み</A>";
					$link_answer = "<A href='question_answer_list.php?sid=$sid&qid=$tq_fk_question_id[$i]&inp_flag=$inp_flag&inp_category=$inp_category&offset=&limit=&page='>返信済み</A>";
//G.Chin AWKT-515 chg end
					$flag_str = $link_answer;
					break;
		}
//G.Chin 2010-08-19 chg end
		
		//▼受付日
		$recv_dt_str = substr($tq_f_recv_dt[$i], 0, 10);
		
		//質問カテゴリ名取得関数
		GetTQuestionCategoryName($tq_fk_question_category_id[$i], $f_category_name);
		
		//会員ログインIDハンドル取得関数
		GetTMemberLoginIdHandle($tq_fk_member_id[$i], $tmm_f_login_id, $tmm_f_handle);
		
		//▼各種リンク
//G.Chin 2010-08-19 chg sta
//		$link_edit = "<A href='question_edit.php?sid=$sid&qid=$tq_fk_question_id[$i]&inp_flag=$inp_flag&inp_category=$inp_category&offset=$offset&limit=$limit'>変更</A>";
		$link_edit = "<A href='question_edit.php?sid=$sid&qid=$tq_fk_question_id[$i]&inp_flag=$inp_flag&inp_category=$inp_category&offset=$offset&limit=$limit'>確認</A>";
		$link_info = "<A href='question_info.php?sid=$sid&qid=$tq_fk_question_id[$i]&inp_flag=$inp_flag&inp_category=$inp_category&offset=$offset&limit=$limit'>返信</A>";
//G.Chin 2010-08-19 chg end
		$link_member = "<A href='mem_regist.php?sid=$sid&mid=$tq_fk_member_id[$i]' target='_blank'>$tmm_f_handle</A>";
		
//G.Chin 2010-08-19 chg sta
//		$dsp_tbl .= "<tr bgcolor='#FFFFFF'>\n";
		$dsp_tbl .= "<tr bgcolor='$bgcolor' style = 'height:25px;'>\n";
//G.Chin 2010-08-19 chg end
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tq_fk_question_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$link_info/$link_edit</tt></NOBR></td>\n";	//G.Chin 2010-08-19 add
		$dsp_tbl .= "<td align='center'><NOBR><tt>$flag_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left'><NOBR><tt>$recv_dt_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left'><NOBR><tt>$f_category_name</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$link_member</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left'><NOBR><tt>$tq_f_subject[$i]</tt></NOBR></td>\n";
//		$dsp_tbl .= "<td align='center'><NOBR><tt>$link_edit</tt></NOBR></td>\n";				//G.Chin 2010-08-19 del
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//▼ページ移動リンク
	$dsp_tbl .= $pageNumber;
	$dsp_tbl .= $form_str;
	$dsp_tbl .= "<br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("問合せ管理",$dsp_tbl);

?>
