<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$gid = $_REQUEST["gid"];
	
	if(($gid != "") && ($gid != 0))
	{
		//会員グループ情報取得関数
		GetTMemberGroupInfo($gid, $tmg_f_member_group_name, $tmg_fk_coin_group_id, $tmg_f_memo, $tmg_f_tm_stamp);
	}
	else
	{
		$tmg_f_member_group_name	= "";
		$tmg_fk_coin_group_id		= "";
		$tmg_f_memo					= "";
		$tmg_f_tm_stamp				= "";
	}
	
	//▼コイン売却価格グループ選択作成
	//コイン売却価格グループ名一覧取得関数
	GetTCoinGroupNameList($tcg_fk_coin_group_id, $tcg_f_coin_group_name, $tcg_data_cnt);
	$name = "fk_coin_group_id";
	$select_num = $tmg_fk_coin_group_id;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $tcg_fk_coin_group_id, $tcg_f_coin_group_name, $tcg_data_cnt, $select_num, $coin_select);
	
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='mem_group_edit_result.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$gid</font>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width=600>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=right><tt> ｸﾞﾙｰﾌﾟ名 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_member_group_name' value='' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> ｺｲﾝ設定 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3>$coin_select</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFC8C8' align=right><tt> メモ </tt></th>\n";
	$dsp_tbl .= "<td colspan=3><font color='#B22222'><input type='text' name='f_memo' value='' size='50'></font></td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 align=center>\n";
	$dsp_tbl .= "<input type='hidden' name='up_mode' value='update'>\n";
	$dsp_tbl .= "<input type='hidden' name='gid' value='$gid'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='登録'>\n";
	$dsp_tbl .= "</FORM>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("グループ登録",$dsp_tbl);

?>
