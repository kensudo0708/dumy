<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
<script type="text/javascript">
<!--

function edit(url){

	window.open(url, "window_name", "width=1015,height=900,scrollbars=yes,resizable = no,fullscreen = no");

}

function del(mes,url){
    if(window.confirm(mes)){

		parent.main_r.location.href = url;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		window.alert('キャンセルされました'); // 警告ダイアログを表示

	}
	// 「キャンセル」時の処理終了
}

// -->
</script>


    <body>
<?PHP
include '../control/exec_delete.php';
    if(empty($_GET["act"])==FALSE)
    {
        delete_draw_ser($_GET["id"]);
    }
?>
        <input type="button" onclick="edit('../member/draw_ser_edit.php')" value="新規登録"/>

        <form method="POST" action="../member/draw_ser_disp.php">
        <table border="1">
            <TR>
                <TD nowrap>端末識別番号</TD>
                <TD nowrap><input type="text" name="ser" size="30"></TD>
            </TR>
            <TR>
                <TD nowrap>状態</TD>
                <TD nowrap><SELECT name="status">
                            <option value="-1">特定しない</option>
                            <option value="0">送信禁止</option>
                            <option value="1">入会禁止</option>
                    </SELECT>
                </TD>
            </TR>
            <TR>
                <TD nowrap>登録元</TD>
                <TD nowrap><SELECT name="base">
                            <option value="-1">特定しない</option>
                            <option value="0">番組ブラック</option>
                            <option value="1">共有ブラック</option>
                    </SELECT>
                </TD>
            </TR>
        </table>
        <input type="hidden" name="act" value="SEC">
        <input type="submit" value="検索">
        </form>
<?PHP
include '../control/exec_select.php';
    if(empty($_POST["act"])==FALSE)
    {
        if(empty($_POST["ser"])==FALSE)
        {
            $ser=$_POST["ser"];
        }
        else
        {
            $ser="";
        }
        $rs=get_draw_ser($ser,$_POST["status"],$_POST["base"],-1);
        if($rs !=0)
        {
            $row=get_row_num($rs);
        }
        else
        {
            $row=0;
        }
?>
        <P><?PHP print($row); ?>件</P>
        <table border="1">
            <TH><NOBR>ID</NOBR></TH>
            <TH><NOBR>端末識別番号</NOBR></TH>
            <TH><NOBR>状態</NOBR></TH>
            <TH><NOBR>登録元</NOBR></TH>
            <TH><NOBR>操作</NOBR></TH>
<?PHP
        for($i =0;$i<$row;$i++)
        {
            $ret=get_result($rs);

?>
        <TR>
            <TD><NOBR><?PHP print($ret["id"]); ?></NOBR></TD>
            <TD><NOBR><?PHP print($ret["ser"]); ?></NOBR></TD>
            <TD><NOBR><?PHP $ret["stat"]==0?print("送信禁止"):print("入会禁止"); ?></NOBR></TD>
            <TD><NOBR><?PHP $ret["base"]==0?print("番組ブラック"):print("共有ブラック"); ?></NOBR></TD>
            
            <TD><a href="#" onclick="del('ブラックリスト番号<?PHP print($ret["id"])?>を削除します','../member/draw_ser_disp.php?id=<?PHP print($ret["id"])?>&act=del')">削除</a>
            </TD>
        </TR>
<?PHP
        }
?>
        </table>
<?PHP
    }
?>
    </body>
</html>
