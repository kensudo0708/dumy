<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>

<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/05/04												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$sid = $_REQUEST["sid"];
	$mid = $_REQUEST["mid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	if ($limit == "")
	{
		$limit = 50;
	}
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
	//最終入札者指定オークション終了商品一覧検索関数
	SrcLstBidderTEndProductsList($mid,$limit,$offset,$tep_fk_end_products_id,$tep_f_products_name,$tep_f_photo1,$tep_f_status,$tep_f_stock,$tep_f_end_price,$tep_f_bit_buy_count,$tep_f_bit_free_count,$tep_f_last_bidder,$tep_f_end_time,$tep_all_count,$tep_data_cnt);
	
	//▼表示開始～終了番号
	if($tep_all_count == 0)
	{
		$start_num = 0;
	}
	else
	{
		$start_num = $offset + 1;
	}
	$end_num = $offset + $tep_data_cnt;
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width='100%'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FF0000' align=center colspan=4><tt><font color='#000000'>入金確認画面</font></tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#CCCCFF' align=righth><tt> 会員ID </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0'><font color='#B22222'>$mid</font>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	
	$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='3' cellpadding='3'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th><font size='-1'>データ件数</font></th>\n";
	$dsp_tbl .= "<th><font color='#5D478B' size='-1'>$tep_all_count</font></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<font color='#000000'><tt>$start_num</tt></font>\n";
	$dsp_tbl .= "<font color='#000000'><tt>件目 ～ </tt></font>\n";
	$dsp_tbl .= "<font color='#000000'><tt>$end_num</tt></font>\n";
	$dsp_tbl .= "<font color='#000000'><tt>件目</tt></font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='1' cellpadding='1' width='100%'>\n";
	$dsp_tbl .= "<tr bgcolor='#CCCCFF'>\n";
	$dsp_tbl .= "<th><NOBR><tt>商品ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>商品名</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>写真</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>状態</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>在庫・発注</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>落札価格</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>操作</tt></NOBR></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tep_data_cnt;$i++)
	{
		//▼終了状態
		switch($tep_f_status[$i])
		{
			case 0:
					$status_str = "落札のみ";
					$bgcolor = "#FFFFFF";
					break;
			case 1:
					$status_str = "購入拒否ｷｬﾝｾﾙ";
					$bgcolor = "#FFFFFF";
					break;
			case 2:
					$status_str = "支払済み";
					$bgcolor = "#FFFFFF";
					break;
			case 3:
					$status_str = "配送先決定";
					$bgcolor = "#FFC8C8";
					break;
			case 4:
					$status_str = "配送済み(完了)";
					$bgcolor = "#C0C0C0";
					break;
			default:
					$status_str = "";
					$bgcolor = "#FFFFFF";
					break;
		}
		
		//▼在庫・発注
		switch($tep_f_stock[$i])
		{
			case 0:		$stock_str = "未指定";		break;
			case 1:		$stock_str = "在庫有り";	break;
			case 2:		$stock_str = "発注済み";	break;
			default:	$stock_str = "";			break;
		}
		
		//▼写真
		if($tep_f_photo1[$i] == "")
		{
			$picture_path_dsp = "";
		}
		else
		{
			$photo_path = SITE_URL."images/".$tep_f_photo1[$i];
			$picture_path_dsp = "<img src='$photo_path' width=50 height=40>";
		}
		
		//▼各種リンク
		$link_reg = "<A href='../product/order_regist.php?pid=$tep_fk_end_products_id[$i]&sid=$sid&mid=$mid' target='_blank'>$tep_f_products_name[$i]</A>";
		if($tep_f_status[$i] == 0)
		{
			//"落札のみ"リンク表示
			$link_pay = "<A href='mem_bidpay.php?sid=$sid&mid=$mid&pid=$tep_fk_end_products_id[$i]&limit=$limit&offset=$offset'>入金処理</A>";
		}
		else
		{
			$link_pay = "";
		}
		
		$dsp_tbl .= "<tr bgcolor='$bgcolor'>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$tep_fk_end_products_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$link_reg</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$picture_path_dsp</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$status_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$stock_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><tt>$tep_f_end_price[$i] 円</tt></td>\n";
		$dsp_tbl .= "<td align='center'><tt>$link_pay</tt></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
//G.Chin 2010-10-13 chg sta
/*
	//■ページ移行リンク
	$page_link_str = "";
	$page_count = ceil($tep_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
			}
			else
			{
				$page_link_str .= "<A href='mem_bidlist.php?inp_activity=$inp_activity&inp_id_name=$inp_id_name&inp_adcode=$inp_adcode&inp_group=$inp_group&inp_mail=$inp_mail&inp_s_regdate=$inp_s_regdate&inp_e_regdate=$inp_e_regdate&inp_regpos=$inp_regpos&inp_s_coin=$inp_s_coin&inp_e_coin=$inp_e_coin&inp_s_free=$inp_s_free&inp_e_free=$inp_e_free&inp_s_totalpay=$inp_s_totalpay&inp_e_totalpay=$inp_e_totalpay&inp_sex=$inp_sex&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
			}
		}
	}
	
	if($tep_all_count > $limit)
	{
		$page_first = "<A href='mem_bidlist.php?inp_activity=$inp_activity&inp_id_name=$inp_id_name&inp_adcode=$inp_adcode&inp_group=$inp_group&inp_mail=$inp_mail&inp_s_regdate=$inp_s_regdate&inp_e_regdate=$inp_e_regdate&inp_regpos=$inp_regpos&inp_s_coin=$inp_s_coin&inp_e_coin=$inp_e_coin&inp_s_free=$inp_s_free&inp_e_free=$inp_e_free&inp_s_totalpay=$inp_s_totalpay&inp_e_totalpay=$inp_e_totalpay&inp_sex=$inp_sex&offset=0&limit=$limit'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='mem_bidlist.php?inp_activity=$inp_activity&inp_id_name=$inp_id_name&inp_adcode=$inp_adcode&inp_group=$inp_group&inp_mail=$inp_mail&inp_s_regdate=$inp_s_regdate&inp_e_regdate=$inp_e_regdate&inp_regpos=$inp_regpos&inp_s_coin=$inp_s_coin&inp_e_coin=$inp_e_coin&inp_s_free=$inp_s_free&inp_e_free=$inp_e_free&inp_s_totalpay=$inp_s_totalpay&inp_e_totalpay=$inp_e_totalpay&inp_sex=$inp_sex&offset=$page_offset[$max_page]&limit=$limit'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tep_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='mem_bidlist.php?inp_activity=$inp_activity&inp_id_name=$inp_id_name&inp_adcode=$inp_adcode&inp_group=$inp_group&inp_mail=$inp_mail&inp_s_regdate=$inp_s_regdate&inp_e_regdate=$inp_e_regdate&inp_regpos=$inp_regpos&inp_s_coin=$inp_s_coin&inp_e_coin=$inp_e_coin&inp_s_free=$inp_s_free&inp_e_free=$inp_e_free&inp_s_totalpay=$inp_s_totalpay&inp_e_totalpay=$inp_e_totalpay&inp_sex=$inp_sex&offset=$next_page&limit=$limit'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='mem_bidlist.php?inp_activity=$inp_activity&inp_id_name=$inp_id_name&inp_adcode=$inp_adcode&inp_group=$inp_group&inp_mail=$inp_mail&inp_s_regdate=$inp_s_regdate&inp_e_regdate=$inp_e_regdate&inp_regpos=$inp_regpos&inp_s_coin=$inp_s_coin&inp_e_coin=$inp_e_coin&inp_s_free=$inp_s_free&inp_e_free=$inp_e_free&inp_s_totalpay=$inp_s_totalpay&inp_e_totalpay=$inp_e_totalpay&inp_sex=$inp_sex&offset=$before_page&limit=$limit'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
*/
	//■ページ移行リンク
	$page_link_str = "";
	$page_count = ceil($tep_all_count / $limit);
	if($page_count > 1)
	{
		for($i=0; $i<$page_count; $i++)
		{
			$page_offset[$i] = $limit * $i;
			$num = $i + 1;
			if($page_offset[$i] == $offset)
			{
				$page_link_str .= "$num</A> ";
			}
			else
			{
				$page_link_str .= "<A href='mem_bidlist.php?sid=$sid&mid=$mid&offset=$page_offset[$i]&limit=$limit'>$num</A> ";
			}
		}
	}
	
	if($tep_all_count > $limit)
	{
		$page_first = "<A href='mem_bidlist.php?sid=$sid&mid=$mid&offset=0&limit=$limit'>";
		$page_first .= "|<";
		$page_first .= "</A> ";
		
		$max_page = $page_count - 1;
		$page_last  = "<A href='mem_bidlist.php?sid=$sid&mid=$mid&offset=$page_offset[$max_page]&limit=$limit'>";
		$page_last .= ">|";
		$page_last .= "</A> ";
		
		$next_num = $tep_all_count - ($offset + $limit);
		$next_page = $offset + $limit;
		if($next_num > 0)
		{
			$page_next  = "<A href='mem_bidlist.php?sid=$sid&mid=$mid&offset=$next_page&limit=$limit'>";
			$page_next .= ">";
			$page_next .= "</A> ";
		}
		else
		{
			$page_next = "";
		}
		
		$before_page = $offset - $limit;
		if($offset > 0)
		{
			$page_before  = "<A href='mem_bidlist.php?sid=$sid&mid=$mid&offset=$before_page&limit=$limit'>";
			$page_before .= "<";
			$page_before .= "</A> ";
		}
		else
		{
			$page_before = "";
		}
		
		$page_link_str = $page_first.$page_before.$page_link_str.$page_next.$page_last;
	}
//G.Chin 2010-10-13 chg end
	$dsp_tbl .= $page_link_str;
	$dsp_tbl .= "<br>\n";
	
	$dsp_tbl .= "<FORM action='mem_payment.php' method='GET' ENCTYPE='multipart/form-data'>\n";
	$dsp_tbl .= "<center>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mid' value='$mid'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value=''>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='戻る'>\n";
	$dsp_tbl .= "</center>\n";
	$dsp_tbl .= "</FORM>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("入金処理",$dsp_tbl);

?>
