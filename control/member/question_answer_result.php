<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/08/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	
	$sid = $_REQUEST["sid"];
	$qid = $_REQUEST["qid"];
	
	$limit = $_REQUEST["limit"];
	$offset = $_REQUEST["offset"];
	
	$inp_flag = $_REQUEST["inp_flag"];
	$inp_category = $_REQUEST["inp_category"];
	
	
	$f_subject = $_REQUEST["f_subject"];
	$f_main = $_REQUEST["f_main"];
	
	if($qid == "")
	{
		PrintAdminPage("質問回答失敗","<P>不正な処理です</P>");
		exit;
	}
	else
	{
		//人事情報取得関数
		GetTStaffInfo($sid,$ts_f_login_id,$ts_f_login_pass,$ts_f_entry_time,$ts_f_status,$ts_f_tm_stamp);
		
		//質問回答登録関数
		$f_name = $ts_f_login_id;
		$f_staff_id = $sid;
		$ret = RegistTQuestionAnswer($qid,$f_subject,$f_main,$f_name,$f_staff_id);
		if($ret == false)
		{
			$dsp_string = "質問への回答登録に失敗しました。<br>\n";
		}
		else
		{
			//質問状態更新関数
			UpdateTQuestionFlag($qid, 2);
			
			$dsp_string = "質問への回答登録に成功しました。<br>\n";
			
			//▼送信処理
			//質問情報取得関数
			GetTQuestionInfo($qid,$tq_fk_question_category_id,$tq_f_subject,$tq_f_main,$tq_f_name,$tq_fk_member_id,$tq_f_mail_address,$tq_f_flag,$tq_f_recv_dt,$tq_f_check_dt,$tq_f_tm_stamp);
			
			//会員メールアドレス取得関数
			GetTMemberMailAddress($tq_fk_member_id,$tmm_f_mail_address_pc,$tmm_f_mail_address_mb);
			
			//送信元
			$sendfrom_str  = "";
			$sendfrom_str .= "FROM: ";
			$sendfrom_str .= INFO_MAIL;
			$sendfrom_str .= "\n";
			$sendfrom_str .= "RETURN-Path: ";
			$sendfrom_str .= INFO_MAIL;
			
			$f_type = 0;
			$f_body = $f_main;
			$f_sendfrom = $sendfrom_str;
			$f_sendto = $tq_f_mail_address;
			
			//メール要求登録関数
			$ret = RegistTMailRequest($f_type,$f_subject,$f_body,$f_sendfrom,$f_sendto);
			if($ret == false)
			{
				$dsp_string .= "回答の送信登録に失敗しました。<br>\n";
			}
			else
			{
				$dsp_string .= "回答の送信登録に成功しました。<br>\n";
			}
		}
	}
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<font><b>$dsp_string</b></font>\n";
	$dsp_tbl .= "<br><br><br>\n";
/*
	$dsp_tbl .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
	$dsp_tbl .= "<br><br>\n";
	$dsp_tbl .= "<form>\n";
	$dsp_tbl .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
	$dsp_tbl .= "</form>\n";
*/
	$dsp_tbl .= "<FORM action='question_list.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<input type='hidden' name='sid' value='$sid'>\n";
	$dsp_tbl .= "<input type='hidden' name='qid' value='$qid'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_flag' value='$inp_flag'>\n";
	$dsp_tbl .= "<input type='hidden' name='inp_category' value='$inp_category'>\n";
	$dsp_tbl .= "<input type='hidden' name='limit' value='$limit'>\n";
	$dsp_tbl .= "<input type='hidden' name='offset' value='$offset'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='戻る'>\n";
	$dsp_tbl .= "</FORM>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("質問回答完了",$dsp_tbl);

?>

