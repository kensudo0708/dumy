<?php
	//☆★	ライブラリ読込み	★☆
	include "../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

	session_start();
	$sid = $_SESSION["staff_id"];

	$inp_hour = $_REQUEST["inp_hour"];


	//起動中自動入札一覧取得関数
	GetActiveAutoBidList($inp_hour,$tab_fk_auto_id,$tab_fk_member_id,$tab_fk_products_id,$tab_f_min_price,$tab_f_max_price,$tab_f_free_coin,$tab_f_buy_coin,$tab_f_set_free_coin,$tab_f_set_buy_coin,$tab_f_tm_stamp,$tab_data_cnt,$f_status);

	$dsp_tbl  = "";

	$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='1' cellpadding='1' width='100%' class = 'main_r'>\n";
	$dsp_tbl .= "<tr bgcolor='#CCCCFF'>\n";
	$dsp_tbl .= "<th><NOBR><tt>時刻</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>会員ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>ハンドル名</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>開始金額</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>終了金額</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>回数</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>商品ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>商品名</tt></NOBR></th>\n";
	$dsp_tbl .= "</tr>\n";

	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tab_data_cnt;$i++)
	{
		//▼時刻
		$time_str = substr($tab_f_tm_stamp[$i], 11, 5);

		//会員ログインIDハンドル取得関数
		GetTMemberLoginIdHandle($tab_fk_member_id[$i], $tmm_f_login_id, $tmm_f_handle);

		//商品名取得関数
		GetTProductsName($tab_fk_products_id[$i],$tpm_f_products_name);
		$count = $tab_f_set_buy_coin[$i] + $tab_f_free_coin[$i];

		if ($f_status[$i] == '0'){
            $bgcolor="";
        }else{
            $bgcolor="bgcolor='#e1e1e1'";
        }

		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$time_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$tab_fk_member_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$tmm_f_handle</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$tab_f_min_price[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$tab_f_max_price[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$count</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center' $bgcolor><NOBR><tt>$tab_fk_products_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='left' $bgcolor><NOBR><tt>$tpm_f_products_name</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}

	$dsp_tbl .= "</table><br>\n";

	//管理画面入力ページ表示関数
	PrintAdminPage("TOP自動入札履歴画面",$dsp_tbl);
?>
