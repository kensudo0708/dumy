<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/09/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
/*
	$inp_hour = $_SESSION["inp_hour"];
	$inp_minutes = $_SESSION["inp_minutes"];
*/
	$mem_show_cnt = $_REQUEST["inp_show_cnt"];
	
	//最後ログインメンバーリスト取得
//G.Chin AWKT-683 2010-11-17 chg sta
//	GetMemLastLoginList($fk_member_id,$f_handle,$f_regist_date,$mem_show_cnt,$data_cnt);
	GetMemLastLoginList($fk_member_id,$f_handle,$f_last_entry,$mem_show_cnt,$data_cnt);
//G.Chin AWKT-683 2010-11-17 chg end
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='1' cellpadding='1' width='100%' class = 'main_r'>\n";
	$dsp_tbl .= "<tr bgcolor='#CCCCFF'>\n";
	$dsp_tbl .= "<th><NOBR><tt>ログイン時刻</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>会員ID</tt></NOBR></th>\n";
	$dsp_tbl .= "<th><NOBR><tt>ハンドル名</tt></NOBR></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$data_cnt;$i++)
	{
		//▼時刻
//G.Chin AWKT-683 2010-11-17 chg sta
//		$time_str = substr($f_regist_date[$i], 11, 5);
		$time_str = substr($f_last_entry[$i], 11, 5);
//G.Chin AWKT-683 2010-11-17 chg end
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$time_str</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$fk_member_id[$i]</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>$f_handle[$i]</tt></NOBR></td>\n";
		
		$dsp_tbl .= "</tr>\n";
	}
	
	$dsp_tbl .= "</table><br>\n";
	
	//管理画面入力ページ表示関数
	PrintAdminPage("最後ログイン会員リスト",$dsp_tbl);

?>
