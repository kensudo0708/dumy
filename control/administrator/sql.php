<SCRIPT language="php">
	//if( strcmp($GLOBALS[PHP_AUTH_PW],"UkiUkiKibun_55") )
	//{
	//	Header("WWW-authenticate: basic realm=\"認証\"");
	//	Header("HTTP/1.0 401 Unauthorixed");
	//	print "認証に失敗しました";
	//	exit();
	//}
	
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	$site_name = SITE_NAME;
	
	print "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n";
	print "<html lang='utf-8'>\n";
	print "<HEAD>\n";
	print "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n";
	print "<TITLE>${site_name}[SQL]</TITLE>\n";
	print "</HEAD>\n";
	print "<BODY>\n";
	
	$sql = $_REQUEST["sql"];
	$sql = ereg_replace("[\\]+","\\",$sql);
	
	//■ SQL文入力フォーム表示
	print "<FORM method=post action=\"sql.php\">\n";
	print "<TABLE border=0>\n";
	print "<TR>\n";
	print "<TD><INPUT type=text name=sql size=80 maxlength=102400 value=\"$sql\"></TD>\n";
	print "<TD><INPUT type=submit value=\"EXEC\"></TD>\n";
	print "</TR>\n";
	print "</TABLE>\n";
	print "</FORM>\n";
//print "sql = [".$sql."]<br>\n";
	
	//■ ＤＢ名、ＳＱＬ文が未入力か
	if( $sql==false )
	{
		exit();
	}
	print "<HR size=1>\n";
	
	//■ＤＢ接続
	$db=db_connect();
//print "db = [".$db."]<br>\n";
	if($db == false)
	{
		exit;
	}
	
	//■ ＳＱＬ文により処理分岐
	if( ereg("^select",$sql) )
	{
		//■ select文の場合は、内容表示
		$result = mysql_query($sql, $db);				// sqlを実行
//print "result = [".$result."]<br>\n";
		if( $result==false )
		{
			print "ERROR: mysql_query: $db , $sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			exit();
		}
		
		$columns = mysql_num_fields($result);			// 列数を取得
		$rows    = mysql_num_rows($result);				// 行数を取得
		
		print "$rows 件該当<BR>\n";
		print "<TABLE border=1 bordercolor='#CCCCCC' cellspacing='1' cellpadding='1'>\n";
		print "<TR>\n";
		for( $i=0; $i<$columns; $i++ )
		{
			$str = mysql_field_name($result,$i);		// 列名の取り出し
			print "<TH><NOBR>$str</NOBR>\n";			// 列名の表示
		}
		print "</TR>\n";
		for( $i=0; $i<$rows; $i++ )
		{
			print("<TR>");
			for( $j=0; $j<$columns; $j++ )
			{
				$str = mysql_result($result,$i,$j);		// データの取り出し
				$str = ereg_replace("\n","<BR>",$str);
				$str = ($str==false) ? "-" : $str;
				
				print "<TD><NOBR>$str</NOBR>\n";		// データの表示
			}
			print "</TR>\n";
		}
		print "</TABLE>\n";
		mysql_free_result($result);						// 検索結果の解放
	}
	else if( ereg("^update",$sql) )
	{
		//■ update文の場合、該当件数出力後に実行。変更後の内容表示
		//■ update文からselect文を作成
		$sql2 = ereg_replace("^update (.+) set .+$", "select * from \\1 ", $sql);
		if( ereg(" where ",$sql) )
		{
			// 条件(where)あり
			$sql2 .= ereg_replace("^.+ (where .+)$", "\\1", $sql);
		}
		//■ sql2を実行して該当件数を表示
		$result = mysql_query($sql2, $db);				// sql2を実行
		if( $result==false )
		{
			print "ERROR: mysql_query: $db , $sql2<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			exit();
		}
		$rows = mysql_num_rows($result);				// 行数を取得
		print "$sql2 >>> $rows 件該当<BR>\n";
		mysql_free_result($result);						// 検索結果の解放
		
		//■ update文の実行
		$sql = iemoji_encode($sql);
		$sql = i18n_convert($sql, 'EUC', 'SJIS');
		$result = mysql_query($sql, $db);				// sqlを実行
		if( $result==false )
		{
			print "ERROR: mysql_query: $db , $sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			exit();
		}
//		mysql_free_result($result);						// 検索結果の解放
		
		//■ update文を実行後、再度sql2を実行して該当件数の内容表示
		$result = mysql_query($sql2, $db);				// sql2を実行
		if( $result==false )
		{
			print "ERROR: mysql_query: $db , $sql2<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			exit();
		}
		$columns = mysql_num_fields($result);			// 列数を取得
		$rows    = mysql_num_rows($result);				// 行数を取得
		print "変更後 $rows 件該当<BR>\n";
		print "<TABLE border>\n";
		print "<TR>\n";
		for( $i=0; $i<$columns; $i++ )
		{
			$str = mysql_field_name($result,$i);		// 列名の取り出し
			print "<TH><NOBR>$str</NOBR>\n";			// 列名の表示
		}
		print "</TR>\n";
		for( $i=0; $i<$rows; $i++ )
		{
			print("<TR>");
			for( $j=0; $j<$columns; $j++ )
			{
				$str = mysql_result($result,$i,$j);		// データの取り出し
				$str = ereg_replace("\n","<BR>",$str);
				$str = ($str==false) ? "-" : $str;
				
				$str = i18n_convert($str, 'SJIS' ,'EUC');
				
				print "<TD><NOBR>$str</NOBR>\n";		// データの表示
			}
			print "</TR>\n";
		}
		print "</TABLE>\n";
		mysql_free_result($result);						// 検索結果の解放
	}
	else
	{
		$sql = iemoji_encode($sql);
		$sql = i18n_convert($sql, 'EUC', 'SJIS');
		
		//■ そのまま実行
		$result = mysql_query($sql, $db);				// sqlを実行
		if( $result==false )
		{
			print "ERROR: mysql_query: $db , $sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			exit();
		}
		print "success!!<BR>\n";
	}
	//■ＤＢ切断
    db_close($db);
	print "</BODY>\n";
	print "</HTML>\n";

</SCRIPT>
