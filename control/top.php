<html lang="utf-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/control/css/layout.css" type="text/css" rel="stylesheet">
<title>自動入札状況閲覧画面</title>
</head>
<script laguage="JavaScript">
<!--
function changePage()
{
	location.reload();
}
// -->
</script>

<body onLoad="setInterval('changePage()',1000*60*1)">
<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/08/26												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	//☆★	ライブラリ読込み	★☆
	include "../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
/*
	$inp_hour = $_SESSION["inp_hour"];
	$inp_minutes = $_SESSION["inp_minutes"];
*/
	//トップ画面で表示するログイン人数
	$mem_show_cnt=MEMBER_LAST_LOGIN;
	
	$mode = $_REQUEST["mode"];
	if($mode == "change")
	{
		$inp_hour = $_REQUEST["inp_hour"];
		$inp_minutes = $_REQUEST["inp_minutes"];
		
		$_COOKIE["c_hour"] = "";
		$_COOKIE["c_minutes"] = "";
		
		$cookie_limit = time() + (86400 * 7);
		
		setcookie("c_hour", $inp_hour, $cookie_limit);
		setcookie("c_minutes", $inp_minutes, $cookie_limit);
	}
	else
	{
		$inp_hour = @$_COOKIE["c_hour"];
		$inp_minutes = @$_COOKIE["c_minutes"];
	}
	
	
	//▼時間（自動入札履歴）
	for($i=0; $i<12; $i++)
	{
		$num = $i + 1;
		$inp_hour_id[$i] = $num;
		$inp_hour_name[$i] = $num."時間前から";
	}
	$inp_hour_cnt = 12;
	$name = "inp_hour";
	$select_num = $inp_hour;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_hour_id, $inp_hour_name, $inp_hour_cnt, $select_num, $inp_hour_select);
	
	//▼時間（ログイン数）
	$inp_minutes_id[0] = 10;
	$inp_minutes_name[0] = "10分前から";
	$inp_minutes_id[1] = 20;
	$inp_minutes_name[1] = "20分前から";
	$inp_minutes_id[2] = 30;
	$inp_minutes_name[2] = "30分前から";
	$inp_minutes_id[3] = 60;
	$inp_minutes_name[3] = "60分前から";
	$inp_minutes_id[4] = 120;
	$inp_minutes_name[4] = "120分前から";
	$inp_minutes_id[5] = 180;
	$inp_minutes_name[5] = "180分前から";
	$inp_minutes_cnt = 6;
	$name = "inp_minutes";
	$select_num = $inp_minutes;
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_minutes_id, $inp_minutes_name, $inp_minutes_cnt, $select_num, $inp_minutes_select);
	
	//会員数取得関数
	GetTMemberCount($inp_minutes, $tm_login_cnt, $tmm_all_count);
	
	//全会員コイン数取得関数
//	GetAllMemberCoin($tm_sum_f_coin, $tm_sum_f_free_coin);
//	if($tm_sum_f_coin == "")
//	{
//		$tm_sum_f_coin = 0;
//	}
//	
//	if($tm_sum_f_free_coin == "")
//	{
//		$tm_sum_f_free_coin = 0;
//	}
	
	$dsp_tbl  = "";
	
	$dsp_tbl .= "<FORM action='top.php' method='GET' ENCTYPE='multipart/form-data' style='display: inline'>\n";
	$dsp_tbl .= "<table cellspacing='1' cellpadding='1' border='1' bordercolor='#C0C0C0' width='100%' class = 'main_r'>\n";
	$dsp_tbl .= "<input type='hidden' name='mode' value='change'>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#828282; width=110px; color:#FFFFFF; border-color:#FFFAFA' value='更新'>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=left colspan=4><tt>ニュース情報</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td colspan=4 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= "<IFRAME name='OutNews' width='100%' height='70' Marginwidth='4' Marginheight='1' Scrolling='auto' src='top_news.php?mode=&upid='></IFRAME>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=left colspan=4><tt>入札情報</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> 更新設定 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= $inp_hour_select;
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> 自動入札履歴 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= "<IFRAME name='OutAutBid' width='100%' height='150' Marginwidth='4' Marginheight='1' Scrolling='auto' src='top_autobid.php?inp_hour=$inp_hour'></IFRAME>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> 会員入札履歴 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= "<IFRAME name='OutMemBid' width='100%' height='150' Marginwidth='4' Marginheight='1' Scrolling='auto' src='top_membid.php?inp_hour=$inp_hour'></IFRAME>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=left colspan=4><tt>会員情報</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> 全会員数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= "&nbsp $tmm_all_count 人<br>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> ログイン数 </tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= $inp_minutes_select;
	$dsp_tbl .= "&nbsp $tm_login_cnt 人<br>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	
	
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#A4DBFF' align=left colspan=4><tt>コイン情報</tt></th>\n";
//	$dsp_tbl .= "</tr>\n";
//	
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> 購入コイン数 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
//	$dsp_tbl .= "&nbsp $tm_sum_f_coin コイン<br>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//	
//	$dsp_tbl .= "<tr>\n";
//	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> フリーコイン数 </tt></th>\n";
//	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
//	$dsp_tbl .= "&nbsp $tm_sum_f_free_coin コイン<br>\n";
//	$dsp_tbl .= "</td>\n";
//	$dsp_tbl .= "</tr>\n";
//	
//	$dsp_tbl .= "</table>\n";
//	$dsp_tbl .= "</FORM>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th bgcolor='#FFFFFF' align=left width=100><tt> ログイン会員：<br>(最大".$mem_show_cnt."人)</tt></th>\n";
	$dsp_tbl .= "<td colspan=3 bordercolor='#C0C0C0' align='left'>\n";
	$dsp_tbl .= "<IFRAME name='MemLoginCnt' width='100%' height='150' Marginwidth='4' Marginheight='1' Scrolling='auto' src='top_last_login.php?inp_show_cnt=$mem_show_cnt'></IFRAME>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "</tr>\n";
	
	//管理画面入力ページ表示関数
	//PrintAdminPage("TOP画面",$dsp_tbl);
	print $dsp_tbl;

?>
</body>
</html>
