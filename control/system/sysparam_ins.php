<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="../../control/css/layout.css">
        <title></title>
    </head>
<script type="text/javascript">
<!--

function exe_ins(ret){

	if(ret !=0)
        {
            mes="登録に失敗しました";
        }
        else
        {
            mes="登録しました";
        }
        if(window.confirm(mes))
        {
            window.close();
            window.opener.location.reload();
        }
}
// -->
</script>
<?PHP
include '../control/exec_insert.php';
if(empty($_POST['act'])==NULL)
{

    $rs=insert_sysparam($_POST['name'],$_POST['type'],$_POST['val'],$_POST['cat'],$_POST['com'],$_POST['main'],$_POST['staff']);

?>
    <body onload="exe_ins('<?PHP print($rs)?>')">
    </body>
</html>
<?PHP
}
?>
<body>
<div>
開発者用画面です。未入力項目はないようにしてください。<br>
入力内容を定数ファイルに反映するには編集ページの確定ボタンを押下してください。
</div>
    <form action="../system/sysparam_ins.php" method="POST">
        <table class="form">
            <tr>
                <th><nobr>定数名</nobr></th>
                <td><nobr><input type="text" name="name" style="width:180px"/></nobr></td>
            </tr>
            <tr>
                <th><nobr>定数タイプ</nobr></th>
            <td><nobr><select name="type" style="width:180px">
                    <option value="0">出力しない</option>
                    <option value="1">INT</option>
                    <option value="2">TEXT</option>
                </select></nobr></td>
            </tr>
            <tr>
                <th><nobr>定数値</nobr></th>
                <td><nobr><input type="text" name="val" style="width:180px"/></nobr></td>
            </tr>
            <tr>
                <th><nobr>カテゴリ</nobr></th>
            <td><nobr><select name="cat" style="width:180px">
                    <option value="0">C</option>
                    <option value="1">PHP</option>
                    <option value="2">C/PHP</option>
                </select></nobr></td>
            </tr>
            <tr>
                <th><nobr>コメント</nobr></th>
                <td><nobr><input type="text" name="com" style="width:180px"/></nobr></td>
            </tr>
            <tr>
                <th><nobr>運営中変更</nobr></th>
            <td><nobr><select name="main" style="width:180px">
                    <option value="0">可</option>
                    <option value="1">不可</option>
                </select></nobr></td>
            </tr>
            <tr>
                <th><nobr>スタッフ変更</nobr></th>
            <td><nobr><select name="staff" style="width:180px">
                    <option value="0">不可</option>
                    <option value="1">可</option>
                </select></nobr></td>
            </tr>
        </table>
        <input type="hidden" name="act" value="ins" />
        <div class="center"><button type="submit" style="width:120px">追加</button></div>
    </form>
</body>
</html>
