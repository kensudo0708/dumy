<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="../../control/css/layout.css">
        <title></title>
    </head>
<script type="text/javascript">
<!--

function rel(ret){

	if(ret !=0)
        {
            mes="更新・確定に失敗しました";
        }
        else
        {
            mes="更新・確定しました";
        }
        window.confirm(mes);
        parent.main_r.location.href = "../system/sysparam_edit.php?stat=1";
}
// -->
</script>
<?PHP
include '../control/exec_update.php';
include '../control/exec_select.php';
session_start();
if($_SESSION['staff_id']==NULL )
{
?>
<body>
    <P>セッション切れです。<BR>不具合が発生することが考えられますので更新してください</P>
</body>
</html>
<?PHP
    return ;
}

if(empty($_POST["act"])==FALSE)
{
    if($_SESSION['op_level']==8)
    {
        $rs=update_sysparam($_POST['name'],$_POST['type'],$_POST['val'],$_POST['cat'],$_POST['com'],$_POST['main'],$_POST['staff'],$_POST['id']);
    }
    else
    {
        $rs=update_sysparam2($_POST['val'],$_POST['com'],$_POST['id']);
    }
    include '../system/edit_define.php';
    edit_define();
    edit_c_conf();
    if(SERVER_NUM > 0)
    {
//      `/home/auction/script/php/data_sync/sync_prod_lib.sh`;
        `/home/auction/script/php/date_sync/sync_prod_lib.sh`;
    }
?>
<body onclick="rel('<?PHP print($rs);?>')" >
<?PHP
}
else
{
?>
    <body>
<?PHP
}

$rs=get_sysparam("-1",$_SESSION['op_level']);
$row=get_row_num($rs);

?>
        <form action="../system/sysparam_edit.php" method="POST">  
            <input type="hidden" name="act" value="up">
            <button type="submit" style="width:180px">更新・確定</button>
            <table class="list">
                <th><nobr>定数名</nobr></th>
                <th><nobr>定数カテゴリ</nobr></th>
                <th><nobr>定数値</nobr></th>
                <th><nobr>カテゴリ</nobr></th>
                <th><nobr>コメント</nobr></th>
                <th><nobr>メンテ中変更</nobr></th>
                <th><nobr>スタッフ変更</nobr></th>
<?PHP
        for($i=0;$i<$row;$i++)
        {
            $ret=get_result($rs);

            $type0="";$type1="";$type2="";
            switch($ret['type'])
            {
                case 0:
                    $type0="SELECTED";
                    break;
                case 1:
                    $type1="SELECTED";
                    break;
                case 2:
                    $type2="SELECTED";
                    break;
            }
            $cat0="";$cat1="";$cat2="";
            switch($ret['cat'])
            {
                case 0:
                    $cat0="SELECTED";
                    break;
                case 1:
                    $cat1="SELECTED";
                    break;
                case 2:
                    $cat2="SELECTED";
                    break;
            }
            $main0="";$main1="";
            $ret['main']==0?$main0="SELECTED":$main1="SELECTED";
            $staff0="";$staff1="";
            $ret['staff']==0?$staff0="SELECTED":$staff1="SELECTED";

            $level="";
            if($_SESSION['op_level']!="8")
            {
                $level="disabled";
            }
?>
            <TR>
                <input type="hidden" value="<?PHP print($ret['id']);?>" name="id[]">
                <td><NOBR><input type="text" size="40" name="name[]" value="<?PHP print($ret['NAME']);?>" <?PHP printf($level);?> ></NOBR></td>
                <td><nobr><select name="type[]" <?PHP printf($level);?> >
                    <option value="0" <?PHP print($type0);?>>出力しない</option>
                    <option value="1" <?PHP print($type1);?>>INT</option>
                    <option value="2" <?PHP print($type2);?>>TEXT</option>
                </select></nobr></td>
                <td><NOBR><input type="text" size="40" name="val[]" value="<?PHP print($ret['val']);?>" ></NOBR></td>
                <td><nobr><select name="cat[]" <?PHP printf($level);?> >
                    <option value="0" <?PHP print($cat0);?>>C</option>
                    <option value="1" <?PHP print($cat1);?>>PHP</option>
                    <option value="2" <?PHP print($cat2);?>>C/PHP</option>
                </select></nobr></td>
                <td><NOBR><input type="text" size="40" name="com[]" value="<?PHP print($ret['com']);?>" ></NOBR></td>
                <td><nobr><select name="main[]" <?PHP printf($level);?> >
                    <option value="0" <?PHP print($main0);?>>可</option>
                    <option value="1" <?PHP print($main1);?>>不可</option>
                </select></nobr></td>
                <td><nobr><select name="staff[]" <?PHP printf($level);?> >
                    <option value="0" <?PHP print($staff0);?>>不可</option>
                    <option value="1" <?PHP print($staff1);?>>可</option>
                </select></nobr></td>
            </TR>
<?PHP
        }
?>
            </table>
            <button type="submit" style="width:180px">更新・確定</button>
        </form>
    </body>
</html>
