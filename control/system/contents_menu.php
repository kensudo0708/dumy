<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◇◆*/
/*																		*/
/*		作成者		:	Kaz Tanaka                                          */
/*		作成日		:	2010/04/12											*/
/*		修正日		:														*/
/*																			*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	include "../../lib/define.php";
	$all_include_path = HOME_PATH."config/all_include_lib.php";
	include $all_include_path;
        include "edit_define.php";


    //-----------------------------------------//
    //     ﾒﾝﾃﾅﾝｽﾌﾗｸﾞ更新関数                   //
    //-----------------------------------------//
    function UpdateGetSystemMainteFlag($f_mainte_flg)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");

        $sql = "update t_system set f_mainte_flg='$f_mainte_flg'";
        mysql_query($sql, $db);
        mysql_close ($db);
        if(SERVER_NUM > 0)
        {
            `/home/auction/script/php/date_sync/sync_prod_lib.sh`;
        }
        return true;

    }


    //-----------------------------------------//
    //     ﾒﾝﾃﾅﾝｽﾌﾗｸﾞ読み出し関数               //
    //-----------------------------------------//
    function GetSystemMainteFlag()
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }

        $ret = mysql_select_db("auction");

        $sql = "SELECT f_mainte_flg FROM t_system";
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";

            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        if ($data_cnt >= 1)
        {
            $f_mainte_flg = mysql_result($result, 0, "f_mainte_flg");
        }
        else
        {
            //ﾃﾞｰﾀが存在しない場合は停止中として扱う
            $f_mainte_flg = SYS_CLOSE;
        }

        return $f_mainte_flg;

        mysql_close ($db);
    }

    //開始中の表示
    function DispOpen(&$dsp_tbl)
    {
        //開始中
        $dsp_tbl = "<div style=\"height:150px;width:250px;left:50%;margin-left:-200px;margin-top:-90px;position:absolute;top:50%;\">\n";
        $dsp_tbl .= "<font color='red'>只今、運営しています。</font>";
        $dsp_tbl .= "<form action='contents_menu.php' method='POST'>\n";
        $dsp_tbl .= "<input type='hidden' name='mode' value='UPDATE'>\n";
        $dsp_tbl .= "<input type='submit' value='メンテ中にする' onclick='return confirm(\"ﾒﾝﾃﾅﾝｽ中にします。よろしいですか？\");'>\n";
        $dsp_tbl .=  "</form>\n";
        $dsp_tbl .=  "</div>\n";
    }

    //終了中の表示
    function DispClose(&$dsp_tbl)
    {
        //停止中状態
        $dsp_tbl = "<div style=\"height:150px;width:250px;left:50%;margin-left:-200px;margin-top:-90px;position:absolute;top:50%;\">\n";
        $dsp_tbl .= "<font color='red'>只今、メンテナンス中です。</font>";
        $dsp_tbl .= "<form action='contents_menu.php' method='POST'>\n";
        $dsp_tbl .= "<input type='hidden' name='mode' value='UPDATE'>\n";
        $dsp_tbl .= "<input type='submit' value='開始する' onclick='return confirm(\"番組運営を開始します。よろしいですか？\");'>\n";
        $dsp_tbl .= "</form>\n";
        $dsp_tbl .= "</div>\n";
    }

    //-----------メイン処理開始-----------
    //ｾｯｼｮﾝ開始（スタッフ情報読み出し）
	session_start();
	$sid = $_SESSION["staff_id"];

    if (array_key_exists("mode", $_POST))
    {
        $mode = $_POST['mode'];
    }
    else
    {
        $mode = "";
    }

    if (($mode == "") || ($mode == "DISP"))
    {
        //現在状況表示
        $f_mainte_flg = GetSystemMainteFlag();
        if ($f_mainte_flg == SYS_CLOSE)
        {
            DispClose($dsp_tbl);
        }
        else if ($f_mainte_flg == SYS_OPEN)
        {
            DispOpen($dsp_tbl);
        }
    }
    else if ($mode == "UPDATE")
    {
        //ここに更新処理がはいる
        //①変更前の現在地取得
        $bf_f_mainte_flg = GetSystemMainteFlag();
        
        //②変更後の値は現在値と逆にする
        if ($bf_f_mainte_flg == SYS_OPEN)
        {
            $new_mainte_flg = SYS_CLOSE;
        }
        else
        {
            $new_mainte_flg = SYS_OPEN;
        }

        //③実際にDB更新を試みる
        UpdateGetSystemMainteFlag($new_mainte_flg);
        //メンテナンスflagの設定
        edit_define();
        if($bf_f_mainte_flg == SYS_OPEN)
        {
            unlink("/home/auction/script/command/tmp/one_sec_run");
        }
        //更新後のﾃﾞｰﾀで画面を再表示
        $f_mainte_flg = GetSystemMainteFlag();
        if ($f_mainte_flg == SYS_CLOSE)
        {
            DispClose($dsp_tbl);
        }
        else if ($f_mainte_flg == SYS_OPEN)
        {
            DispOpen($dsp_tbl);
        }
                    if(SERVER_NUM > 0)
    {
//      `/home/auction/script/php/data_sync/sync_prod_lib.sh`;
        `/home/auction/script/php/date_sync/sync_prod_lib.sh`;
    }
    }


	//管理画面入力ページ表示関数
	PrintAdminPage("システム設定（番組開始・停止）",$dsp_tbl);

?>
