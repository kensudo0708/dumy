<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◇◆*/
/*																			*/
/*		作成者		:	Kaz Tanaka                                          */
/*		作成日		:	2010/04/12											*/
/*		修正日		:	2010/04/15(G.Chin)									*/
/*																			*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	include "../../lib/define.php";
	$all_include_path = HOME_PATH."config/all_include_lib.php";
	include $all_include_path;
	
    //-----------メイン処理開始-----------
    //ｾｯｼｮﾝ開始（スタッフ情報読み出し）
	session_start();
	$sid = $_SESSION["staff_id"];
	
    if (array_key_exists("mode", $_POST))
    {
        $mode = $_POST['mode'];
    }
    else
    {
        $mode = "";
    }

    $dsp_tbl = "<table class = 'main_l'><caption>&#x2460;スタッフ権限設定</caption>";
    $dsp_tbl .= "<FORM action='staff_entry.php' method='GET' target='main_r' style = 'padding:5px;'>";
    $dsp_tbl .= "<tr><td><input type='submit' style='background-color:#FFFBEC; color:#C84B00; width:100px; border-color:#FFFAFA;' value='新規ｽﾀｯﾌ追加'></td></tr>";
    $dsp_tbl .= "</FORM>";
    $dsp_tbl .= "</table>";
	//管理画面入力ページ表示関数
	PrintAdminInputPage($dsp_tbl);

?>
