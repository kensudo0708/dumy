<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◇◆*/
/*																		*/
/*		作成者		:	Kaz Tanaka                                          */
/*		作成日		:	2010/04/12											*/
/*		修正日		:														*/
/*																			*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	include "../../lib/define.php";
	$all_include_path = HOME_PATH."config/all_include_lib.php";
	include $all_include_path;

    //-----------------------------------------//
    //     STAFF取得関数                       //
    //-----------------------------------------//
    function GetStaffOne($staff_id, &$fk_staff_id, &$f_login_id, &$f_login_pass, &$f_status, &$f_entry_time, &$f_tm_stamp)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }

        $ret = mysql_select_db("auction");

        $sql = "SELECT * FROM t_staff where fk_staff_id='$staff_id'";
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";

            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        if ($data_cnt >= 1)
        {
            $fk_staff_id    = mysql_result($result, 0, 'fk_staff_id');
            $f_login_id     = mysql_result($result, 0, 'f_login_id');
            $f_login_pass   = mysql_result($result, 0, 'f_login_pass');
            $f_status       = mysql_result($result, 0, 'f_status');
            $f_entry_time   = mysql_result($result, 0, 'f_entry_time');
            $f_tm_stamp     = mysql_result($result, 0, 'f_tm_stamp');
        }
        else
        {
            //ﾃﾞｰﾀが存在しない場合はなにもしない。
        }
        mysql_close ($db);
        return true;
    }

    //-----------------------------------------//
    //     権限取得関数                         //
    //     この関数は他の画面でも重複してるので　 //
    //     共通関数としてまとめるときは要注意です //
    //-----------------------------------------//
    function GetAuthorityList(&$fk_authority_id, &$f_authority_name,$level)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");

        if($level==8)
        {
            $sql = "select * from t_authority order by fk_authority_id";
        }
        else
        {
            $sql = "select * from t_authority where f_op_level=0 order by fk_authority_id";
        }
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";

            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        if ($data_cnt >= 1)
        {
            for ($i=0; $i<$data_cnt; $i++)
            {
                $fk_authority_id[$i]  = mysql_result($result, $i, 'fk_authority_id');
                $f_authority_name[$i] = mysql_result($result, $i, 'f_authority_name');
            }
        }
        else
        {
            //ﾃﾞｰﾀが存在しない場合はなにもしない。
        }
        mysql_close ($db);

        return true;
    }

    //-----------------------------------------//
    //     権限設定取得関数                     //
    //-----------------------------------------//
    function GetAuthorityConfig($staff_id, $authority_id)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");
        
        $sql = "select * from t_staff_authority where fk_staff_id='$staff_id' and fk_authority_id='$authority_id'";
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";

            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        mysql_close ($db);
        return $data_cnt;
    }


    //-----------メイン処理開始-----------
    //ｾｯｼｮﾝ開始（スタッフ情報読み出し）
	session_start();
	$sid = $_SESSION["staff_id"];
        $level= $_SESSION["op_level"];

    if (array_key_exists("mode", $_POST))
    {
        $mode = $_POST['mode'];
    }
    else
    {
        $mode = "";
    }

    $template = "staff_edit.html";
    $fp = fopen($template, "r");
    if ($fp == false)
    {
        //
        print "FILE OPEN ERROR!<BR>\n";
        exit;
    }
    else
    {
        $html = fread($fp, 1024000);
        fclose($fp);
    }

    //ページ間情報読み出し
    if (array_key_exists("staff_id", $_GET))
    {
        $staff_id = $_GET['staff_id'];
    }
    else
    {
        $staff_id = "";
    }

    //指定されたSTAFF情報読み込み
    GetStaffOne($staff_id, $fk_staff_id, $f_login_id, $f_login_pass, $f_status, $f_entry_time, $f_tm_stamp);
    $html = str_replace("%%LOGIN_ID%%",   $f_login_id,   $html);
    $html = str_replace("%%LOGIN_PASS%%", $f_login_pass, $html);
//G.Chin 2010-08-17 chg sta
/*
    if ($f_status == 0)
    {
        $html = str_replace("%%CHECKED_ENABLE%%",  "selected",  $html);
        $html = str_replace("%%CHECKED_DISABLE%%", "",          $html);
    }
    else
    {
        $html = str_replace("%%CHECKED_ENABLE%%",  "",          $html);
        $html = str_replace("%%CHECKED_DISABLE%%", "selected",  $html);
    }
*/
    //現管理者のSTAFF情報読み込み
    GetStaffOne($sid, $ts_fk_staff_id, $ts_f_login_id, $ts_f_login_pass, $ts_f_status, $ts_f_entry_time, $ts_f_tm_stamp);
//G.Chin 2010-08-31 chg sta
/*
    switch($ts_f_status)
    {
		case 0:
		        $html = str_replace("%%OPTION_0%%", "<OPTION value='0' selected>一般管理者", $html);
		        $html = str_replace("%%OPTION_2%%", "",                                      $html);
		        $html = str_replace("%%OPTION_3%%", "",                                      $html);
		        $html = str_replace("%%OPTION_8%%", "",                                      $html);
		        $html = str_replace("%%OPTION_9%%", "<OPTION value='9' >無効",               $html);
				break;
		case 2:
		        $html = str_replace("%%OPTION_0%%", "<OPTION value='0' >一般管理者",         $html);
		        $html = str_replace("%%OPTION_2%%", "<OPTION value='2' selected>最高権限",   $html);
		        $html = str_replace("%%OPTION_3%%", "<OPTION value='3' >R管理",              $html);
		        $html = str_replace("%%OPTION_8%%", "",                                      $html);
		        $html = str_replace("%%OPTION_9%%", "<OPTION value='9' >無効",               $html);
				break;
		case 8:
		        $html = str_replace("%%OPTION_0%%", "<OPTION value='0' >一般管理者",         $html);
		        $html = str_replace("%%OPTION_2%%", "<OPTION value='2' >最高権限",           $html);
		        $html = str_replace("%%OPTION_3%%", "<OPTION value='3' >R管理",              $html);
		        $html = str_replace("%%OPTION_8%%", "<OPTION value='8' selected>開発者",     $html);
		        $html = str_replace("%%OPTION_9%%", "<OPTION value='9' >無効",               $html);
				break;
    }
*/
    switch($ts_f_status)
    {
		case 0:
		        $html = str_replace("%%OPTION_0%%", "<OPTION value='0' %%SELECTED_0%%>一般管理者", $html);
		        $html = str_replace("%%OPTION_2%%", "",                                            $html);
		        $html = str_replace("%%OPTION_3%%", "",                                            $html);
		        $html = str_replace("%%OPTION_8%%", "",                                            $html);
		        $html = str_replace("%%OPTION_9%%", "<OPTION value='9' %%SELECTED_9%%>無効",       $html);
				break;
		case 2:
		        $html = str_replace("%%OPTION_0%%", "<OPTION value='0' %%SELECTED_0%%>一般管理者", $html);
		        $html = str_replace("%%OPTION_2%%", "<OPTION value='2' %%SELECTED_2%%>最高権限",   $html);
		        $html = str_replace("%%OPTION_3%%", "<OPTION value='3' %%SELECTED_3%%>R管理",      $html);
		        $html = str_replace("%%OPTION_8%%", "",                                            $html);
		        $html = str_replace("%%OPTION_9%%", "<OPTION value='9' %%SELECTED_9%%>無効",       $html);
				break;
		case 8:
		        $html = str_replace("%%OPTION_0%%", "<OPTION value='0' %%SELECTED_0%%>一般管理者", $html);
		        $html = str_replace("%%OPTION_2%%", "<OPTION value='2' %%SELECTED_2%%>最高権限",   $html);
		        $html = str_replace("%%OPTION_3%%", "<OPTION value='3' %%SELECTED_3%%>R管理",      $html);
		        $html = str_replace("%%OPTION_8%%", "<OPTION value='8' %%SELECTED_8%%>開発者",     $html);
		        $html = str_replace("%%OPTION_9%%", "<OPTION value='9' %%SELECTED_9%%>無効",       $html);
				break;
    }
	
	//設定対象管理者の権限状態
	$selected_0 = "";
	$selected_2 = "";
	$selected_3 = "";
	$selected_8 = "";
	$selected_9 = "";
	
    switch($f_status)
    {
		case 0:	$selected_0 = "selected";	break;
		case 2:	$selected_2 = "selected";	break;
		case 3:	$selected_3 = "selected";	break;
		case 8:	$selected_8 = "selected";	break;
		case 9:	$selected_9 = "selected";	break;
	}
	
    $html = str_replace("%%SELECTED_0%%", $selected_0, $html);
    $html = str_replace("%%SELECTED_2%%", $selected_2, $html);
    $html = str_replace("%%SELECTED_3%%", $selected_3, $html);
    $html = str_replace("%%SELECTED_8%%", $selected_8, $html);
    $html = str_replace("%%SELECTED_9%%", $selected_9, $html);
//G.Chin 2010-08-31 chg end
//G.Chin 2010-08-17 chg end

    //権限設定マスタを読み出す
    GetAuthorityList($fk_authority_id, $f_authority_name,$level);
    $auth_count = count($fk_authority_id);

    $auth_list = "";
    for ($i=0; $i<$auth_count; $i++)
    {
        $ret = GetAuthorityConfig($staff_id, $fk_authority_id[$i]);
        if ($ret >= 1)
        {
            $auth_list .= "<input type='checkbox' name='authority[]' value='$fk_authority_id[$i]' checked>$f_authority_name[$i]<BR>\n";
        }
        else
        {
            $auth_list .= "<input type='checkbox' name='authority[]' value='$fk_authority_id[$i]'>$f_authority_name[$i]<BR>\n";
        }
    }
    $html = str_replace("%%AUTHORITY_LIST%%", $auth_list, $html);

    $html = str_replace("%%STAFF_ID%%", $staff_id, $html);

    print "$html";

?>
