<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◇◆*/
/*																		*/
/*		作成者		:	Kaz Tanaka                                          */
/*		作成日		:	2010/04/12											*/
/*		修正日		:														*/
/*																			*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	include "../../lib/define.php";
	$all_include_path = HOME_PATH."config/all_include_lib.php";
	include $all_include_path;

    //-----------------------------------------//
    //     許可IP更新関数                       //
    //-----------------------------------------//
    function UpdateGetSystemMainteFlag($fk_globalip)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");

        //めんどくさいんで一回削除（笑）大雑把でごめんね。
        $sql = "delete from t_staff_ip";
        mysql_query($sql, $db);

        //渡された配列に従い１個ずつINSERT
        $ip_count = count($fk_globalip);
        for ($i=0; $i<$ip_count; $i++)
        {
            $ip_address = $fk_globalip[$i];
            if ($ip_address != "")
            {
                $sql = "insert into t_staff_ip (fk_grobalip) values ('$ip_address')";
                mysql_query($sql, $db);
            }

        }

        mysql_close ($db);

        return true;
    }

    //-----------------------------------------//
    //     許可IP取得関数                       //
    //-----------------------------------------//
    function GetStaffIP(&$fk_ip_sno, &$fk_grobalip)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }

        $ret = mysql_select_db("auction");

        $sql = "SELECT * FROM t_staff_ip";
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";

            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        if ($data_cnt >= 1)
        {
            for ($i=0; $i<$data_cnt; $i++)
            {
                $fk_ip_sno[$i]   = mysql_result($result, $i, 'fk_ip_sno');
                $fk_grobalip[$i] = mysql_result($result, $i, 'fk_grobalip');
            }
        }
        else
        {
            //ﾃﾞｰﾀが存在しない場合はなにもしない。
        }
        mysql_close ($db);
        return true;
    }

    //-----------メイン処理開始-----------
    //ｾｯｼｮﾝ開始（スタッフ情報読み出し）
	session_start();
	$sid = $_SESSION["staff_id"];

    if (array_key_exists("mode", $_POST))
    {
        $mode = $_POST['mode'];
    }
    else
    {
        $mode = "";
    }

    if (array_key_exists("fk_globalip", $_POST))
    {
        $fk_globalip = $_POST['fk_globalip'];
    }
    else
    {
        $fk_globalip = "";
    }

    //更新ﾓｰﾄﾞの時は更新処理をしてから一覧表示を行う
    if ($mode == "UPDATE")
    {
        UpdateGetSystemMainteFlag($fk_globalip);
    }

    GetStaffIP($fk_ip_sno, $fk_grobalip);
    $ip_count = count($fk_ip_sno);

    $dsp_tbl = "<FORM action='staff_ip.php' method='POST'>\n";
    $dsp_tbl .= "<table class='list'>\n";

    $remote_ip = $_SERVER['REMOTE_ADDR'];

    $dsp_tbl .= "<TR>\n";
    $dsp_tbl .= "<th style='width:262px'>現在ｱｸｾｽしているIPｱﾄﾞﾚｽ</th>\n";
    $dsp_tbl .= "</TR>\n";
    $dsp_tbl .= "<TR>\n";
    $dsp_tbl .= "<TD class='center'>$remote_ip</TD>\n";
    $dsp_tbl .= "</TR>\n";
    $dsp_tbl .= "</TABLE>\n";

    $dsp_tbl .= "<BR>\n";

    $dsp_tbl .= "<table class='list'>\n";
    $dsp_tbl .= "<TR>\n";
    $dsp_tbl .= "<th style='width:60px'>No.</th>\n";
    $dsp_tbl .= "<th style='width:200px'>現在許可されてるIPｱﾄﾞﾚｽ</th>\n";
    $dsp_tbl .= "</TR>\n";

    for ($i=0; $i<$ip_count; $i++)
    {
        $dsp_tbl .= "<TR>\n";
        $dsp_tbl .= "<TD class='center'>$fk_ip_sno[$i]</TD>\n";
        $dsp_tbl .= "<TD><input type='text' name='fk_globalip[]' value='$fk_grobalip[$i]'></TD>\n";
        $dsp_tbl .= "</TR>\n";
    }

    $dsp_tbl .= "<TR>\n";
    $dsp_tbl .= "<TD class='center'>NEW</TD>\n";
    $dsp_tbl .= "<TD><input type='text' name='fk_globalip[]' value=''></TD>\n";
    $dsp_tbl .= "</TR>\n";
    $dsp_tbl .= "<TR>\n";
    $dsp_tbl .= "<TD colspan='2' class='center'>\n";
    $dsp_tbl .= "<input type='hidden' name='mode' value='UPDATE'>\n";
    $dsp_tbl .= "<button type='submit' onclick='return confirm(\"更新します。よろしいですか？\");' style='width:120px'>更新実行</button>\n";
    $dsp_tbl .= "</TD>\n";
    $dsp_tbl .= "</TR>\n";
    $dsp_tbl .= "</table>\n";

    $dsp_tbl .= "</FORM>\n";

    $dsp_tbl .= "<font color='red'>設定を間違えると接続できなくなる可能性がありますのでご注意下さい。</font>\n";

    //管理画面入力ページ表示関数
	PrintAdminPage("許可IP設定",$dsp_tbl);

?>
