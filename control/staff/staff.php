<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	Kaz Tanaka												*/
/*		作成日		:	2010/04/12												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	include "../../lib/define.php";
	$all_include_path = HOME_PATH."config/all_include_lib.php";
	include $all_include_path;
	
    //-----------------------------------------//
    //     STAFF新規追加関数                    //
    //-----------------------------------------//
    function EntryStaff($f_login_id, $f_login_pass, $f_status, $f_authority)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");
		
        //①まずはSTAFF登録
        $sql = "insert into t_staff (f_login_id,f_login_pass,f_status) values ('$f_login_id', '$f_login_pass', '$f_status')";
        mysql_query($sql, $db);
        //追加したてのSTAFF_ID（自動採番）を取得する
        $sql = "SELECT LAST_INSERT_ID() as last_id";
        $result = mysql_query($sql, $db);
        $rows   = mysql_num_rows($result);
        if ($rows >= 1)
        {
            $new_staff_id = mysql_result($result, 0, 'last_id');
        }
		
        //②権限設定を行う（→ t_staff_authority)
        $auth_count = count($f_authority);
        for ($i=0; $i<$auth_count; $i++)
        {
            $sql = "insert into t_staff_authority (fk_authority_id,fk_staff_id) values ('$f_authority[$i]','$new_staff_id')";
            mysql_query($sql, $db);
        }
        mysql_close ($db);
        return true;
    }
	
    //-----------------------------------------//
    //     STAFF削除関数                       //
    //-----------------------------------------//
    function DeleteStaff($staff_id)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");
		
        //①消すときは先に権限を消す
        $sql = "delete from t_staff_authority where fk_staff_id='$staff_id'";
        mysql_query($sql, $db);
		
        //②続いてSTAFF本体を消す
        $sql = "delete from t_staff where fk_staff_id='$staff_id'";
        mysql_query($sql, $db);
		
        mysql_close ($db);
        return true;
    }
	
    //-----------------------------------------//
    //     STAFF存在確認関数                    //
    //-----------------------------------------//
    function CheckStaffExist($login_id, $login_pass)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
		
        $ret = mysql_select_db("auction");
		
        $sql = "select * from t_staff where f_login_id='$login_id' and f_login_pass='$login_pass'";
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";
			
            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        mysql_close ($db);
		
        return $data_cnt;
    }
	
    //-----------------------------------------//
    //     STAFF取得関数                       //
    //-----------------------------------------//
    function GetStaffs(&$fk_staff_id, &$f_login_id, &$f_login_pass, &$f_status, &$f_entry_time, &$f_tm_stamp, $level)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
		
        $ret = mysql_select_db("auction");
		
//G.Chin 2010-08-17 chg sta
/*
        if($level==8)
        {
            $sql = "SELECT * FROM t_staff order by fk_staff_id";
        }
        else
        {
            $sql = "SELECT * FROM t_staff where f_status = 0 order by fk_staff_id";
        }
*/
		switch($level)
		{
			case 0:
		            $sql  = "select * from t_staff ";
					$sql .= "where f_status=0 or f_status=9 ";
					$sql .= "order by fk_staff_id";
					break;
			case 2:
		            $sql  = "select * from t_staff ";
					$sql .= "where f_status!=8 ";
					$sql .= "order by fk_staff_id";
					break;
			case 8:
		            $sql  = "select * from t_staff ";
					$sql .= "order by fk_staff_id";
					break;
		}
//G.Chin 2010-08-17 chg end
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";
			
            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        if ($data_cnt >= 1)
        {
            for ($i=0; $i<$data_cnt; $i++)
            {
                $fk_staff_id[$i]    = mysql_result($result, $i, 'fk_staff_id');
                $f_login_id[$i]     = mysql_result($result, $i, 'f_login_id');
                $f_login_pass[$i]   = mysql_result($result, $i, 'f_login_pass');
                $f_status[$i]       = mysql_result($result, $i, 'f_status');
                $f_entry_time[$i]   = mysql_result($result, $i, 'f_entry_time');
                $f_tm_stamp[$i]     = mysql_result($result, $i, 'f_tm_stamp');
            }
        }
        else
        {
            //ﾃﾞｰﾀが存在しない場合はなにもしない。
        }
        mysql_close ($db);
        return true;
    }
	
    //-----------メイン処理開始-----------
    //ｾｯｼｮﾝ開始（スタッフ情報読み出し）
	session_start();
	$sid = $_SESSION["staff_id"];
        $level = $_SESSION["op_level"];
	
    if (array_key_exists("mode", $_GET))
    {
        $mode = $_GET['mode'];
    }
    else
    {
        $mode = "";
    }

    if (array_key_exists("f_login_id", $_GET))
    {
        $login_id = $_GET['f_login_id'];
    }
    else
    {
        $login_id = "";
    }

    if (array_key_exists("f_login_pass", $_GET))
    {
        $login_pass = $_GET['f_login_pass'];
    }
    else
    {
        $login_pass = "";
    }

    if (array_key_exists("f_status", $_GET))
    {
        $status = $_GET['f_status'];
    }
    else
    {
        $status = "";
    }

    if (array_key_exists("authority", $_GET))
    {
        $authority = $_GET['authority'];
    }
    else
    {
        $authority = "";
    }

//↓新規追加・更新・削除処理ﾌﾞﾛｯｸ↓
    if ($mode == "INSERT")
    {
        //新規追加ﾓｰﾄﾞ処理
        //

        //入力値ﾁｪｯｸ
        if (($login_id == "") || ($login_pass == ""))
        {
            //
        }
        else
        {
            //重複ﾁｪｯｸを行う
            $ret = CheckStaffExist($login_id, $login_pass);
            if ($ret == 0)
            {
                //入力されたID・PASSが存在しなければ新規追加処理実行
                EntryStaff($login_id, $login_pass, $status, $authority);
            }
        }
    }
    else if ($mode == "UPDATE")
    {
        //更新ﾓｰﾄﾞ
        //一旦削除する
        if (array_key_exists("staff_id", $_GET))
        {
            $staff_id = $_GET['staff_id'];
        }
        else
        {
            $staff_id = "";
        }
        DeleteStaff($staff_id);

        //改めて追加実行
        $ret = CheckStaffExist($login_id, $login_pass);
        if ($ret == 0)
        {
            //入力されたID・PASSが存在しなければ新規追加処理実行
            EntryStaff($login_id, $login_pass, $status, $authority);
        }
    }
    else if ($mode == "DELETE")
    {
        //削除ﾓｰﾄﾞ
        if (array_key_exists("staff_id", $_GET))
        {
            $staff_id = $_GET['staff_id'];
        }
        else
        {
            $staff_id = "";
        }
        DeleteStaff($staff_id);
    }
    //↑新規追加・更新・削除処理ﾌﾞﾛｯｸ↑


    //STAFF情報をDBより取得
    GetStaffs($fk_staff_id, $f_login_id, $f_login_pass, $f_status, $f_entry_time, $f_tm_stamp, $level);

    $staff_count = count($fk_staff_id);
    $dsp_tbl = "<table class='list' width='80%'>\n";
    $dsp_tbl .= "<TR>\n";
    $dsp_tbl .= "<th>loginid</th>\n";
    $dsp_tbl .= "<th>状態</th>\n";
    $dsp_tbl .= "<th>権限操作</th>\n";
    $dsp_tbl .= "<th>削除</th>\n";
    $dsp_tbl .= "</TR>\n";

    for ($i=0; $i<$staff_count; $i++)
    {
        $link_authority = "<A HREF='staff_edit.php?staff_id=$fk_staff_id[$i]'>設定</A>";
        $link_delete    = "<A HREF='staff.php?mode=DELETE&staff_id=$fk_staff_id[$i]' onclick='return confirm(\"削除します。よろしいですか？\");'>削除</A>";

//G.Chin 2010-08-16 chg sta
/*
        if ($f_status[$i] == 0)
        {
            $status_str = "有効";
        }
        else if ($f_status[$i] == 9)
        {
            $status_str = "無効";
        }
        else
        {
            $status_str = "　";
        }
*/
		switch($f_status[$i])
		{
			case 0:	$status_str = "一般管理者";		break;
			case 2:	$status_str = "最高権限";		break;
			case 3:	$status_str = "R管理";			break;
			case 8:	$status_str = "開発者";			break;
			case 9:	$status_str = "無効";			break;
		}
//G.Chin 2010-08-16 chg end

        $dsp_tbl .= "<TR>\n";
        $dsp_tbl .= "<TD class='center'>$f_login_id[$i]</TD>\n";
        $dsp_tbl .= "<TD class='center'>$status_str</TD>\n";
        $dsp_tbl .= "<TD class='center'>$link_authority</TD>\n";
        $dsp_tbl .= "<TD class='center'>$link_delete</TD>\n";
        $dsp_tbl .= "</TR>\n";
    }
    $dsp_tbl .= "</TABLE>";

    //管理画面入力ページ表示関数
	PrintAdminPage("スタッフ一覧",$dsp_tbl);

?>
