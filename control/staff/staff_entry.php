<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◇◆*/
/*																		*/
/*		作成者		:	Kaz Tanaka                                          */
/*		作成日		:	2010/04/12											*/
/*		修正日		:														*/
/*																			*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

	include "../../lib/define.php";
	$all_include_path = HOME_PATH."config/all_include_lib.php";
	include $all_include_path;

    //-----------------------------------------//
    //     権限取得関数                         //
    //     この関数は他の画面でも重複してるので　 //
    //     共通関数としてまとめるときは要注意です //
    //-----------------------------------------//
    function GetAuthorityList(&$fk_authority_id, &$f_authority_name,$level)
    {
        $db = db_connect();
        if($db == false)
        {
            print "DB Connect Error!<BR>\n";
            exit;
        }
        $ret = mysql_select_db("auction");

        if($level==8)
        {
            $sql = "select * from t_authority order by fk_authority_id";
        }
        else
        {
            $sql = "select * from t_authority where f_op_level=0 order by fk_authority_id";
        }
        $result = mysql_query($sql, $db);
        if( $result == false )
        {
            $errmsg = mysql_error ($db);
            print "$errmsg<BR>\n";

            print "$sql<BR>\n";
            db_close($db);
            return false;
        }
        $data_cnt = mysql_num_rows($result);
        if ($data_cnt >= 1)
        {
            for ($i=0; $i<$data_cnt; $i++)
            {
                $fk_authority_id[$i]  = mysql_result($result, $i, 'fk_authority_id');
                $f_authority_name[$i] = mysql_result($result, $i, 'f_authority_name');
            }
        }
        else
        {
            //ﾃﾞｰﾀが存在しない場合はなにもしない。
        }
        mysql_close ($db);

        return true;
    }

    //-----------メイン処理開始-----------
    //ｾｯｼｮﾝ開始（スタッフ情報読み出し）
	session_start();
	$sid = $_SESSION["staff_id"];
        $level = $_SESSION["op_level"];

    if (array_key_exists("mode", $_POST))
    {
        $mode = $_POST['mode'];
    }
    else
    {
        $mode = "";
    }

    $template = "staff_entry.html";
    $fp = fopen($template, "r");
    if ($fp == false)
    {
        //
        print "FILE OPEN ERROR!<BR>\n";
        exit;
    }
    else
    {
        $html = fread($fp, 1024000);
        fclose($fp);
    }

    //権限設定マスタを読み出す
    GetAuthorityList($fk_authority_id, $f_authority_name,$level);
    $auth_count = count($fk_authority_id);

    $auth_list = "";
    for ($i=0; $i<$auth_count; $i++)
    {
        $auth_list .= "<input type='checkbox' name='authority[]' value='$fk_authority_id[$i]'>$f_authority_name[$i]<BR>\n";
    }
    $html = str_replace("%%AUTHORITY_LIST%%", $auth_list, $html);

    $dsp_tbl = "";

    print "$html";

?>
