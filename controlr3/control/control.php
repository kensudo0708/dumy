<?php

function file_read($file)
{
    $fp=0;
    $i=0;
    $fp=fopen($file,"r");
    while( ! feof($fp) )
    {
        $buffer = fgets( $fp);
        $date[$i] = trim($buffer);
        $i++;
    }

    fclose($fp);
    return $date;
}

function file_write($file,$date)
{
    $row=count($date);
    $fp=fopen($file,"w");
    for($i=0;$i<$row ;$i++)
    {
        $tmp=trim($date[$i])."\n";
        fputs($fp, $tmp, strlen($tmp));
    }
    fclose($fp);
}

function check_input($str)
{
    if(isset($str))
    {
        if (is_numeric($str) == false)
        {
            return 1;
        }
    }
    else
    {
        return 2;
    }
    return 0;
}

function check_value($min,$max)
{
    if($min < $max)
    {
        return 0;
    }
    else if($max< $min)
    {
        return 1;
    }
}

?>
