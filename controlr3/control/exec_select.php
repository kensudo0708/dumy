<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function get_row_num($rs)
{
    return mysql_num_rows($rs);
}
//スタッフログイン情報取得
function exec_loginr($id,$pass,$op_level)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    //SQL文生成
    $sql = "SELECT fk_staff_id ,f_status FROM auction.t_staff WHERE f_status in(2,8,3) and f_login_id = '$id' and f_login_pass ='$pass'";

    //SQL文実行
    $rs = mysql_query($sql,$db);
    
    if(mysql_num_rows($rs) != 1)
    {
        //該当スタッフなし 又は　一名ではない
        return -1;
    }

    db_close($db);

    $ret = mysql_fetch_array($rs);
    $op_level=$ret['f_status'];
    return $ret['fk_staff_id'];
}

function get_r_para(&$param)
{
    $sql ="select f_id,f_prm1 from auction.t_rparam order by f_id";

    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    $num = mysql_num_rows($rs);

    for($i=0;$i<$num;$i++)
    {
        $ret = mysql_fetch_array($rs);
        $param[$ret['f_id']] =$ret['f_prm1'];
    }
    db_close($db);
    return true;
}

function GetCategoryList()
{
    $sql ="select fk_item_category,f_category_name from auction.t_item_category where f_status=0  order by fk_item_category";

    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
function GetAuctionType()
{
    $sql="select fk_auction_type_id,f_auction_name from auction.t_auction_type order by fk_auction_type_id  ";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
function GetCategoryLimit($cat)
{
    $sql= "select f_rbstp,f_rbedp from auction.t_item_category where fk_item_category='$cat'";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
function GetCategoryR($cat)
{
    $sql= "select f_stp,f_edp,f_num from auction.t_rmatrix where f_cno='$cat' order by f_stp";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function GetSystem()
{
    $sql= "select f_rupdate_flg from auction.t_system limit 0,1";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function GetRParam()
{
    $sql= "select f_id,f_prm1 from auction.t_rparam  order by f_id  ";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function GetRnum($id=0)
{
    if($id==0)
    {
        $sql= "SELECT COUNT(f_temp1) as count FROM auction.t_member2 WHERE f_temp1 <> 0";
    }
    else
    {
        $sql= "SELECT COUNT(mm.f_temp1) as count FROM auction.t_member_master mm,auction.t_products_master pm
            WHERE mm.f_temp1 <> 0 and mm.f_temp1 = pm.fk_products_id and pm.fk_item_category_id=$id ";
    }
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function GetRobProd($star,$end,$id=0,$day,$pid=0)
{
    $sec_day=$day;
    if($sec_day < 0)
    {
        $sec_day=3;
    }
    $where_day="";
    if($sec_day >0)
    {
        $where_day =" and f_start_time > DATE_SUB(NOW() ,INTERVAL $sec_day DAY)";
    }

    $where="";
    if( $id>0)
    {
        $where=" and fk_item_category_id = $id ";
    }

    $where_id="";
    $where_temp1="";
    if( $pid>0)
    {
        $where_id=" and fk_products_id= $pid ";
	$where_temp1 = " and f_temp1 = $pid ";
    }
	$sql="SELECT pm.fk_products_id,pm.f_photo1,pm.f_products_name,ic.f_category_name, mm.cnt,
            pm.f_r,pm.f_p,pm.f_min_price,pm.f_now_price,pm.f_bit_buy_count, att.f_spend_coin,pm.f_market_price,
            att.f_auction_name ,att.f_auto_flag ,pm.f_after_cnt ,pm.f_none_flag
FROM
(SELECT fk_products_id,f_photo1,f_products_name,fk_auction_type_id,f_p,f_r,f_min_price,f_now_price,f_bid_buy_count f_bit_buy_count,f_market_price,f_after_cnt,f_none_flag,fk_item_category_id,TIME_TO_SEC(TIMEDIFF(DATE_ADD(f_start_time,INTERVAL f_auction_time  SECOND ),NOW())) auction_time
FROM auction.t_products2 WHERE f_status > 0 AND f_end_status = 99 $where_id $where_day $where ) pm,
(SELECT COUNT(fk_member_id) cnt,f_temp1 FROM auction.t_member2 WHERE f_temp1 > 0 AND f_activity > 89 $where_temp1 group by f_temp1 ) mm,auction.t_item_category ic,auction.t_auction_type att
WHERE pm.fk_products_id = mm.f_temp1 AND ic.fk_item_category = pm.fk_item_category_id AND pm.fk_auction_type_id =att.fk_auction_type_id order by pm.auction_time limit $star,$end";

//          echo $sql;
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}
function GetRobProdAuto($star,$end,$id=0,$day,$pid=0)
{
    $where="";
    if( $id>0)
    {
        $where=" and fk_item_category = $id ";
    }
        $sec_day=$day;
    if($sec_day < 0)
    {
        $sec_day=3;
    }
    $where_day="";
    if($sec_day >0)
    {
        $where_day =" and f_start_time > DATE_SUB(NOW() ,INTERVAL $sec_day DAY)";
    }
    $where_id="";
    if($pid>0)
    {
        $where_id=" and fk_products_id = $pid";
    }

    $sql ="select pm.fk_products_id,pm.f_photo1,pm.f_products_name,ic.f_category_name,att.f_auction_name,att.f_spend_coin,pm.f_market_price,pm.f_now_price,att.f_auto_flag  
	from
	(select fk_products_id,f_photo1,f_products_name,fk_item_category_id,fk_auction_type_id,f_market_price,f_now_price,TIME_TO_SEC(TIMEDIFF(DATE_ADD(f_start_time,INTERVAL f_auction_time  SECOND ),NOW())) auction_time from auction.t_products2 where f_status > 0 and f_end_status =99 $where_id $where_day  ) pm,
	auction.t_auction_type att,auction.t_item_category ic,
	(select distinct f_temp1 from auction.t_member2 where f_temp1 > 0 and f_activity >89) m
	where pm.fk_item_category_id = ic.fk_item_category and pm.fk_auction_type_id=att.fk_auction_type_id and pm.fk_products_id =m.f_temp1 order by pm.auction_time limit $star,$end";
//echo $sql;

    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function GetRobProdNum($id,$day,$pid=0)
{
    $where ="";
    $sec_day=$day;
    if($day < 0)
    {
        $sec_day=3;
    }
    if($id > 0)
    {
        $where ="and fk_item_category_id = $id ";
    }
    $where_day="";
    if($sec_day >0)
    {
        $where_day =" and f_start_time > DATE_SUB(NOW() ,INTERVAL $sec_day DAY)";
    }
    $where_id="";
    if($pid > 0)
    {
        $where_day="and fk_products_id = $pid";
    }
    $sql="SELECT pm.fk_products_id
FROM
(SELECT fk_products_id
FROM auction.t_products2 
WHERE f_status > 0 AND f_end_status =99  $where_day $where 
) pm,
(SELECT distinct f_temp1 FROM auction.t_member2 WHERE f_temp1 > 0 AND f_activity > 89 ) m
WHERE pm.fk_products_id = m.f_temp1";
    //echo $sql;
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}
function GetRobMember($id)
{
    $sql ="SELECT fk_member_id ,f_handle,f_age1,f_age2 FROM auction.t_member2 WHERE f_temp1 =$id ORDER BY f_age1,fk_member_id";

    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}
function GetRobProdSession($id)
{
    $sql ="SELECT f_p,f_r
            FROM auction.t_products2
            WHERE fk_products_id=$id";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function getRobAuto($prod_id)
{
    $sql="SELECT ab.fk_auto_id ,ab.fk_member_id ,ab.f_min_price,ab.f_max_price , ab.f_free_coin ,ab.f_set_free_coin,mm.f_handle from auction.t_auto_bidder ab ,
        auction.t_member2 mm WHERE ab.fk_member_id = mm.fk_member_id and ab.fk_products_id = $prod_id and (mm.f_activity = 99 or mm.f_activity = 98) and ab.f_status = 0 order by fk_auto_id";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function getRobAuto2($prod_id)
{
    $sql="SELECT ab.fk_auto_id ,ab.fk_member_id ,ab.f_min_price,ab.f_max_price , ab.f_free_coin ,ab.f_set_free_coin,ab.f_buy_coin,ab.f_set_buy_coin,mm.f_handle from auction.t_auto_bidder ab ,
        auction.t_member2 mm WHERE ab.fk_member_id = mm.fk_member_id and ab.fk_products_id = $prod_id and mm.f_activity = 1 and ab.f_status = 0 order by fk_auto_id";

    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function getUpPrice($prod_id)
{
    $sql="SELECT a.f_up_price FROM auction.t_auction_type a,auction.t_products2 pm
            WHERE a.fk_auction_type_id=pm.fk_auction_type_id
            AND pm.fk_products_id = $prod_id";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function getAutoMem($prod_id,$type,$num)
{
    if($type==0)
    {
        $sql="SELECT fk_member_id FROM auction.t_member2 WHERE f_activity = 99 and f_temp1 = $prod_id order by RAND()
                LIMIT 0 ,$num";
    }
    else if($type==1)
    {
        $sql="SELECT fk_member_id FROM auction.t_member2 WHERE f_activity = 99 order by RAND()  LIMIT 0 ,$num";
    }
    else if($type == 2)
    {
         $sql="SELECT fk_member_id,f_handle,f_age1,f_age2 FROM auction.t_member2 WHERE f_activity = 99 and f_temp1 = $prod_id order by f_age1";
    }
    //echo "$sql<BR>";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}
function GetNowPrice($prod_id)
{
    $sql="SELECT f_now_price , f_min_price,f_r,f_p
            FROM auction.t_products2 
            WHERE fk_products_id =$prod_id";
//echo "$sql<BR>";
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function GetRVersion($db)
{
    $sql = "select f_prm1 from auction.t_rparam where f_id =1";
    $rs = mysql_query($sql,$db);
    db_close($db);
    return $rs;
}

function GetKnock($start,$end,$stat,$order)
{
    $sql ="select kn.fk_knocked_id ,mm.fk_member_id,kn.f_knocked_price,mm.f_handle,kn.f_tm_stamp from
            (select fk_knocked_id,f_knocked_price,fk_member_id,f_tm_stamp from auction.t_knock where f_status = $stat ) kn,auction.t_member2 mm
            where mm.fk_member_id = kn.fk_member_id ";

    switch($order)
    {
        case 1:
            $sql.="order by kn.fk_knocked_id ";
            break;
        case 2:
            $sql.="order by kn.fk_knocked_id desc";
            break;
        case 3:
            $sql.="order by mm.fk_member_id ";
            break;
        case 4:
            $sql.="order by mm.fk_member_id desc";
            break;
        case 5:
            $sql.="order by kn.f_knocked_price ";
            break;
        case 6:
            $sql.="order by kn.f_knocked_price desc";
            break;
    }
    $sql.=" limit $start,$end";
    //echo $sql;
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);

    return $rs;
}

function GetKnock_end($start,$end,$order)
{
    $sql ="select kn.fk_knocked_id ,mm.fk_member_id,kn.f_knocked_price,mm.f_handle,ep.f_products_name,ep.f_end_time,ep.f_end_price from
            (select fk_knocked_id,f_knocked_price,fk_member_id,fk_products_id from auction.t_knock where f_status = 2 ) kn,auction.t_member_master mm,auction.t_end_products ep
            where mm.fk_member_id = kn.fk_member_id and kn.fk_products_id = ep.fk_end_products_id ";

    switch($order)
    {
        case 1:
            $sql.="order by kn.fk_knocked_id ";
            break;
        case 2:
            $sql.="order by kn.fk_knocked_id desc";
            break;
        case 3:
            $sql.="order by mm.fk_member_id ";
            break;
        case 4:
            $sql.="order by mm.fk_member_id desc";
            break;
        case 5:
            $sql.="order by kn.f_knocked_price ";
            break;
        case 6:
            $sql.="order by kn.f_knocked_price desc";
            break;
    }
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);
    echo $sql;
    return $rs;
}
function GetKnocknum($status)
{
    $sql ="select fk_knocked_id from auction.t_knock where f_status = $status";
    
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    db_close($db);
    return mysql_num_rows($rs);
}
?>
