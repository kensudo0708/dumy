<html lang="utf-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/control/css/layout.css" type="text/css" rel="stylesheet">
<title>割引確認メニュー</title>

<style TYPE="text/css">
body{
background-color:#FFFFFF;
font-size:9pt;
height:600;
width:500;
font-family:"メイリオ",Meiryo,"MS P Gothic","ＭＳ Ｐゴシック",Verdana,Arial,sans-serif;
}

table{
font-size:9pt;
}

table.list{
text-align:center;
border: solid 1px #999999;
border-collapse:collapse;
}

table.list th{
text-align:center;
color:#555;
background-color:#D3D3D3;
border: solid 1px #999999;
border-collapse:collapse;
height:25px;
}

table.list td{
text-align:center;
border: solid 1px #999999;
border-collapse:collapse;
height:20px;}

.button{
background-color:#828282;
color:#FFFFFF;
border-color:#FFFAFA;
margin-top:10px;
text-align: center;
}

</style>

</head>
<script laguage="JavaScript">
<!--
function change_status(id,stat,order,page)
{
	myWin = window.open("change_status.php?id="+id+"&stat="+stat+"&order="+order +"&page="+page,"_blank","menubar=no,toolbar=no,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=500,height=300");
}
-->
</script>
<body>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../../mvc/utils/Utils.class.php';

$status=$_REQUEST['status'];
$sort = $_REQUEST['order'];
$page = $_REQUEST['page'];
$result ="";
$num = "";

$html="";
$all_num =GetKnocknum($status);
$disp_num=20;

if($page==0)
{
    $start=$disp_num*($page);
}
else
{
    $start=$disp_num*($page-1);
}

$html.="<table class = 'list'>\n";
$html.="<tr>\n";
$html.="<td style = 'width:30px; height:25px;'>\n";
$html.="件数\n";
$html.="</td>\n";
$html.="<td style = 'width:40px;'>\n";
$html.="$all_num\n";
$html.="</td>\n";
$html.="</tr>\n";
$html.="</table>\n";

$disp=$start+$disp_num;
$html.="<P>".$start."～".$disp."件目</P>\n";


if($status == 0 || $status == 1 || $status == 2)
{
    $pageId=Utils::getInt($page,1);
    $pageCount=ceil($all_num/ $disp_num);

    $result = GetKnock($start,$disp_num,$status,$sort);
    $num=mysql_num_rows($result);

    if($num==0)
    {
        $html="該当データはありません";
        echo $html;
        return;
    }

    $html .=Utils::getPageNumbersII($pageId, $pageCount, SITE_URL."controlr/knock/disp.php?status=$status&order=$sort", 5);
    $html .="<table class  = 'list'>\n";
    $html .="<tr>\n";
    $html .="<th style = 'width:70px'>会員ID</th>\n";
    $html .="<th style = 'width:120px'>ハンドル名</th>\n";
    $html .="<th style = 'width:70px'>割引額(円)</th>\n";

    if( $status == 0)
    {
        $html .="<th style = 'width:70px'>操作</th>\n";
        $html .="<th style = 'width:70px'>設定時間</th>\n";
    }
    else if( $status == 1)
    {
        $html .="<th style = 'width:200px'>キャンセル時間</th>\n";
    }

    $html .="</tr>\n";

    for($i=0;$i<$num;$i++)
    {
        $ret=mysql_fetch_array($result);
        $id=$ret['fk_knocked_id'];
        $mid=$ret['fk_member_id'];
        $kid=$ret['fk_knocked_id'];
        $handle=$ret['f_handle'];
        $price =number_format($ret['f_knocked_price']);
        $time =$ret['f_tm_stamp'];

        $html .="<tr>\n";
        $html .="<td align='right' >$mid</td>\n";
        $html .="<td align='center'>$handle</td>\n";
        $html .="<td align='right'>$price</td>\n";
        if( $status == 0)
        {
            $html .="<td align =right><NOBR><input type=\"button\" style=\"background-color:#FFFFFF; color:#000000; font-size:9pt; width:100; border:1 solid #A4FFA4;\" value=\"キャンセル\"  onClick=\"change_status(".$id.",".$status.",".$sort.",".$page.")\">";
        }
        if($status !=2)
        {
            $html .="<td>$time</td>\n";
        }
        $html .="</tr>\n";
    }
    $html .="</table>\n";
    $html .=Utils::getPageNumbersII($pageId, $pageCount, SITE_URL."controlr/knock/disp.php?status=$status&order=$sort", 5);
}
else if($status==3)
{
    $pageId=Utils::getInt($page,1);
    $pageCount=ceil($all_num/ $disp_num);
    $result = GetKnock_end($start,$disp_num,$status,$sort);
    $num=mysql_num_rows($result);

    if($num==0)
    {
        $html="該当データはありません";
        echo $html;
        return;
    }
    $html .=Utils::getPageNumbersII($pageId, $pageCount, SITE_URL."controlr/knock/disp.php?status=$status&order=$sort", 5);
    $html .="<table class = 'list'>\n";
    $html .="<tr>\n";
    $html .="<th>会員ID</th>\n";
    $html .="<th>ハンドル名</th>\n";
    $html .="<th>割引額(円)</th>\n";
    $html .="<th>落札商品名</th>\n";
    $html .="<th>落札額(円)</th>\n";
    $html .="<th>落札日時</th>\n";
    $html .="</tr>\n";

    for($i=0;$i<$num;$i++)
    {
        $ret=mysql_fetch_array($result);
        $mid=$ret['fk_member_id'];
        $kid=$ret['fk_knocked_id'];
        $handle=$ret['f_handle'];
        $price =$ret['f_knocked_price'];
        $prod_name=mb_substr($ret['f_products_name'],20);
        $prod_price=number_format($ret['f_end_price']);
        $prod_time=$ret['f_end_time'];

        $html .="<tr>\n";
        $html .="<td align='right'>$mid</td>\n";
        $html .="<td align='center'>$handle</td>\n";
        $html .="<td align='right'>$price</td>\n";
        $html .="<td>$prod_name</td>\n";
        $html .="<td align='right'>$prod_price</td>\n";
        $html .="<td>$prod_time</td>\n";
        $html .="</tr>\n";
    }
    $html .="</table>\n";
    $html .=Utils::getPageNumbersII($pageId, $pageCount, SITE_URL."controlr/knock/disp.php?status=$status&order=$sort", 5);
}
echo $html;
?>
</body>
</html>
