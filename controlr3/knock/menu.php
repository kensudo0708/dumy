<html lang="utf-8">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/control/css/layout.css" type="text/css" rel="stylesheet">
<title>割引確認メニュー</title>
<style TYPE='text/css'>

body{
background-color:#FFFFC8;
font-size:9pt;
font-family:'メイリオ',Meiryo,'MS P Gothic','ＭＳ Ｐゴシック',Verdana,Arial,sans-serif;
}

table{
font-size:9pt;
}

.button{
background-color:#828282;
color:#FFFFFF;
border-color:#FFFAFA;
margin-top:0px;
text-align: center;
}

</style>


</head>
<body>
<center>
    <table style = "width:100%">
        <form method ="POST" action="disp.php" target='main_r'>
            <input type ="hidden" name="page" value="0">
            <tr>
                <td style = "height:20px; background-color: #EAEA00;">状態</td>
            </tr>
            <tr>
            <td>

            <P><select name="status">
                <option value="0">作動中</option>
                <option value="2">終了済</option>
                <option value="1">キャンセル</option>
            </select></P>

            </td>
            </tr>

            <tr>
                <td style = "height:20px; background-color: #EAEA00;">ソート</td>
            <tr>
            <td>
            <P><input type ="radio" name="order" value="1">設定番号(昇順)<BR>
            <input type ="radio" name="order" value="2" checked>設定番号(降順)<BR>
            <input type ="radio" name="order" value="3">会員番号(昇順)<BR>
            <input type ="radio" name="order" value="4">会員番号(降順)<BR>
            <input type ="radio" name="order" value="5">割引額(昇順)<BR>
            <input type ="radio" name="order" value="6">割引額(降順)</P>
            </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" class = "button" value="検索">
                </td>
            </tr>
        </form>
    </table>
</center>
</body>
</html>