<html lang="utf-8">
<head>
<title>管理画面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
function isIE() {
    var ua = navigator.userAgent.toLowerCase();
    if ( ua.indexOf("msie") != -1 ) {
        return true;
    } else {
        return false;
    }
}
function reinitIframe() {
    var l_frame = document.getElementById( 'l_frame' );
    var r_frame = document.getElementById( 'r_frame' );
    var i = 1;
    var header_height = 77;
    var l_column_width = 180;
    var resize_height = 0;
    var resize_width = 0;
    try{
        // ie
        if ( isIE() ) {
            resize_height = document.body.clientHeight - header_height - 10;
            resize_width = document.body.clientWidth - l_column_width - 5;
        // ie以外
        } else {
            resize_height = window.innerHeight - header_height - 10;
            resize_width = window.innerWidth - l_column_width - 15;
        }
	l_frame.height = resize_height + 'px';
	r_frame.height = resize_height + 'px';
	l_frame.width = l_column_width  + 'px';
	r_frame.width = resize_width + 'px';
    } catch ( ex ) {
        alert('window resize error.');
    }
}
</script>
</head>
<?PHP
include 'control/DB_connect.php';
include 'control/exec_select.php';

$ret = -2;
session_start();
if(empty($_POST["id"]) ==FALSE && empty ($_POST["pass"]) ==FALSE)
{
    $_SESSION['staff_id']=NULL;
    //ログインチェック処理
    $ret = exec_loginr($_POST["id"],$_POST["pass"],&$op_level);
    $_SESSION['staff_id']=$_POST["id"];
}

if($ret == -1)
{
    //ログインチェックで該当する管理者がいない場合
?>
    <P>入力されたID・PASSの管理者は存在しません。</P>
    <a href="mainte.html">戻る</a>
</body>
</html>
<?PHP
    exit;
}
elseif ($ret ==-2)
{
    //ログイン画面(mainte.html)空の遷移ではない場合

    //staff_idがセッションに格納されていない場合は不正アクセス
    if($_SESSION['staff_id']==NULL)
    {
?>
    <P>不正アクセスです。</P>
    <a href="mainte.html">戻る</a>
</body>
</html>
<?PHP
        exit;
    }
}
else
{
    //ログイン成功ならスタッフのIDをセッションに格納
    $_SESSION['staff']=$ret;
    $_SESSION['f_login_id']=$_POST["id"];
    $_SESSION['op_level']=$op_level;
}
?>
<body onResize="reinitIframe()" style="margin:0px;">
<table id="header" style="width:100%; border:0px; margin:0px; padding:0px; border-collapse:collapse;">
<tr>
<td width="80%"><iframe height="40px" width="100%" src="main/head.php"  frameborder="no" scrolling="no"></iframe></td>
<td width="20%"><iframe height="40px" width="100%" src="main/logout.php" frameborder="no" scrolling="no"></iframe></td>
</tr>

<tr>
<td colspan="2">
<iframe height="40px" width="100%" src="main/category_menu.php" frameborder="no" scrolling="auto"></iframe>
</td>
</tr>

</table>
<table id="main_contents" style="width:100%; border:0px; margin:0px; padding:0px; border-collapse:collapse;">
<tr>
<td><iframe id="l_frame" width="200px" name="main_l" frameborder="no" src="none_water2.html" scrolling="auto"></iframe></td>
<td><iframe id="r_frame" width="500px" name="main_r" frameborder="no" scrolling="auto"></iframe></td>
</tr>
</table>
</body>
</html>
<script type="text/javascript">
reinitIframe();
</script>
