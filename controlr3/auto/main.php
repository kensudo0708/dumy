<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/exec_update.php';
include '../control/control.php';
include '../../mvc/utils/Utils.class.php';

$display_num=30;//ページあたりの表示件数
$html ="<html lang='utf-8'>\n";
$html .="<head>\n";
$html .="<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";

$html .="<title></title>\n";
$html .="<style TYPE='text/css'>\n";
$html .="body{\n";
$html .="background-color:#FFFFFF;\n";
$html .="font-size:9pt;\n";
$html .="height:600;\n";
$html .="width:800;\n";
$html .="font-family:'メイリオ',Meiryo,'MS P Gothic','ＭＳ Ｐゴシック',Verdana,Arial,sans-serif;\n";
$html .="}\n";

$html .="table{\n";
$html .="font-size:9pt;\n";
$html .="}\n";

$html .="table.list{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="}\n";

$html .="table.list th{\n";
$html .="text-align:center;\n";
$html .="color:#555;\n";
$html .="background-color:#D3D3D3;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="height:25px;\n";
$html .="}\n";

$html .="table.list td{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="}\n";

$html .=".button{\n";
$html .="background-color:#828282;\n";
$html .="color:#FFFFFF;\n";
$html .="border-color:#FFFAFA;\n";
$html .="margin-top:10px;\n";
$html .="text-align: center;\n";
$html .="}\n";

$html .="</style>\n";

$html .="</head>\n";
$html .="<body>\n";

$day=$_REQUEST['day'];
$pid=$_REQUEST['pid'];
if($pid==NULL)
{
    $pid=0;
}
if(is_numeric($pid)==false)
{
    $html ="<html lang='utf-8'>\n";
    $html .="<head>\n";
    $html .="<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
    $html ="</head><body><P>商品IDは数値のみです</P></body>\n";
    print($html);
    return;
}
$rs=GetRnum();
$row = mysql_num_rows($rs);
$num=0;
if($row == 1)
{
    $ret=mysql_fetch_array($rs);
    $num=$ret['count'];
}
$size=-1;
$num_before=10000-$num;
$html .="<table class = 'list'>\n";
$html .="<tr>\n";
$html .="<td style = 'width:80px; height:25px; background-color:#D3D3D3;'>割当済み</td>\n";
$html .="<td style = 'width:80px;'>".$num."人</td>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$html .="<td style = 'height:25px; background-color:#D3D3D3;'>未割当</td>\n";
$html .="<td>".$num_before."人</td>\n";
$html .="</tr>\n";
$html .="</table></font>\n";
$html .="<BR><BR>\n";

if($num_before < 1000)
{
    $html .="<p><Font Color='#ff00007' >※割当可能な援会員が支援会員総数の一割未満になっています!!</Font><P><br>\n";
}

$row=-1;
$num=0;

$rs=GetRobProdNum(0,$day,$pid);
$row = mysql_num_rows($rs);
$num +=$row;

if(empty($_GET['page'])==TRUE)
{
    $disp_count=0;
}
else
{
    $disp_count=$_GET['page']-1;
}

$start_disp=$display_num*$disp_count+1;
if($num < $display_num*($disp_count+1))
{
    $end_disp=$num;
}
else
{
    $end_disp=$display_num*($disp_count+1);
}

if($num == 0)
{
    $html ="<html lang='utf-8'>\n";
    $html .="<head>\n";
    $html .="<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
    $html ="</head><body><P>現在割当中の商品はありません</P></body>\n";
    print($html);
    return;
}
$rs=GetRobProdAuto($disp_count*$display_num,$display_num,0,$day,$pid);

$row = mysql_num_rows($rs);

$html.="<P>".$start_disp."件目～".$end_disp."件目</P>";
$html.= "<table class = 'list'>\n";
$html.= "<tr style = 'height:25px;'>\n";
$html.= "<th style = 'width:70px'>ID</th>\n";
$html.= "<th style = 'width:70px'>画像</th>\n";
$html.= "<th style = 'width:200px'>商品名</th>\n";
$html.= "<th style = 'width:160px'>カテゴリ</th>\n";
$html.= "<th style = 'width:160px'>オークションタイプ</th>\n";
$html.= "<th style = 'width:100px'>自動入札設定</th>\n";
$html.= "<th style = 'width:100px'>現在価格</th>\n";
$html.= "</tr>\n";

$pageId=Utils::getInt($disp_count,1);
$pageCount=Utils::getInt(($num + $display_num -1)/ $display_num );
for($i=0;$i<$row;$i++)
{
    $ret=mysql_fetch_array($rs);
    $html.= "<tr>\n";
    $html.= "<td><NOBR>".$ret['fk_products_id']."</td>\n";
    $html.= "<td><NOBR><img height =30 width=30 src = '".SITE_URL."images/".$ret['f_photo1']."'></img></th>\n";
    if(mb_strlen($ret['f_products_name']) >= 20)
    {
        $html.= "<td align =left><NOBR>".mb_strcut($ret['f_products_name'],0,20)."…</td>\n";
    }
    else
    {
        $html.= "<td align =left><NOBR>".mb_strcut($ret['f_products_name'],0,20)."</td>\n";
    }
    $html.= "<td align =left><NOBR>".$ret['f_category_name']."</td>\n";
    $html.= "<td align =left><NOBR>".$ret['f_auction_name']."</td>\n";
    if($ret['f_auto_flag'] == 1)
    {
        $html.= "<td><NOBR><a href='set_auto.php?id=".$ret['fk_products_id']."&s_coin=".$ret['f_spend_coin']."&market=".$ret['f_market_price']."' target='_blank' >設定・確認</a></td>\n";
    }
    else
    {
         $html.= "<td><NOBR>-</td>\n";
    }
    $html.= "<td align =right><NOBR>".number_format($ret['f_now_price'])."円</td>\n";


    $html.= "</tr>\n";
}
$html .="</table>\n";
$html .=Utils::getPageNumbersII($pageId, $pageCount, SITE_URL."controlr/auto/main.php?day=$day", 5);
$html .="<input type='button' style = 'width:120px;' class = 'button' value='更新' onClick=\"location.reload(true);\">";
$html .= "<br><br></body>\n";
$html .="</html>\n";
printf($html);
?>
