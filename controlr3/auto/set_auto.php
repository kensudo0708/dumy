<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '../control/DB_connect.php';
include '../control/exec_select.php';

if(empty($_GET['id']))
{
    $html ="<html lang='utf-8'>\n";
    $html .="<head><title>設定・確認</title><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
    $html .="</head>\n";
    $html .="<body>\n";
    $html .="<P>不正アクセスです</P>";
    $html .="</body></html>";
    printf($html);
    return;
}
$id=$_GET['id'];
$market =$_REQUEST['market'];
$spend_coin=$_REQUEST['s_coin'];

$rs=getRobAuto2($id);
$row=mysql_num_rows($rs);

$tmp_html="";
if($row==0)
{
    $tmp_html .="<P>現在この商品に割り当てられている会員はいません</P>\n";
}
else
{
    $tmp_html .="<P>現在この商品には以下の自動入札が設定されています。</P>\n";
    $tmp_html .="<table class = 'list'>";
    $tmp_html .="<tr>\n";
    $tmp_html .="<TH style = 'width:70px;'>会員ID</th>\n";
    $tmp_html .="<TH style = 'width:120px;'>ハンドル名</th>\n";
    $tmp_html .="<TH style = 'width:70px;'>開始価格</th>\n";
    $tmp_html .="<TH style = 'width:70px;'>終了価格</th>\n";
    $tmp_html .="<TH style = 'width:70px;'>残り回数</th>\n";
    $tmp_html .="<TH style = 'width:70px;'>設定回数</th>\n";
    $tmp_html .="<TH style = 'width:70px;'>操作</th>\n";
    $tmp_html .="</tr>\n";
    for($i=0;$i<$row;$i++)
    {
        $ret=mysql_fetch_array($rs);
        $tmp_coin=$ret['f_free_coin']+$ret['f_buy_coin'];
        $tmp_set=$ret['f_set_free_coin']+$ret['f_set_buy_coin'];
        $tmp_html .="<tr>\n";
        $tmp_html .="<Td style = 'height:20px;'>".$ret['fk_member_id']."</td>\n";
        $tmp_html .="<Td>".$ret['f_handle']."</td>\n";
        $tmp_html .="<Td>".$ret['f_min_price']."</td>\n";
        $tmp_html .="<Td>".$ret['f_max_price']."</td>\n";
        $tmp_html .="<Td>".$tmp_coin."</td>\n";
        $tmp_html .="<Td>".$tmp_set."</td>\n";
        $tmp_html .="<Td>--</td>\n";
        $tmp_html .="</tr>\n";
    }
    $tmp_html .="</table>";
}

$rs=getRobAuto($id);
$row=mysql_num_rows($rs);

if($row==0)
{
    $tmp_html .="<P>現在この商品に割り当てられている支援会員はいません</P>\n";
}
else
{
    $tmp_html .="<P>現在この商品には以下の自動入札が設定されています。</P>\n";
    $tmp_html .="<table class = 'list'>";
    $tmp_html .="<tr>\n";
    $tmp_html .="<th style = 'width:70px;'>会員ID</th>\n";
    $tmp_html .="<th style = 'width:120px;'>ハンドル名</th>\n";
    $tmp_html .="<th style = 'width:70px;'>開始価格</th>\n";
    $tmp_html .="<th style = 'width:70px;'>終了価格</th>\n";
    $tmp_html .="<th style = 'width:70px;'>残り回数</th>\n";
    $tmp_html .="<th style = 'width:70px;'>設定回数</th>\n";
    $tmp_html .="<th style = 'width:70px;'>操作</th>\n";
    $tmp_html .="</tr>\n";
    for($i=0;$i<$row;$i++)
    {
        $ret=mysql_fetch_array($rs);
        $tmp_html .="<tr>\n";
        $tmp_html .="<td>".$ret['fk_member_id']."</td>\n";
        $tmp_html .="<td>".$ret['f_handle']."</td>\n";
        $tmp_html .="<td>".$ret['f_min_price']."</td>\n";
        $tmp_html .="<td>".$ret['f_max_price']."</td>\n";
        $tmp_html .="<td>".$ret['f_free_coin']."</td>\n";
        $tmp_html .="<td>".$ret['f_set_free_coin']."</td>\n";
        $tmp_html .="<td><form method='POST' action='set_auto_check.php' target='_blank'>\n";
        $tmp_html .="<input type='hidden' name='id' value='".$ret['fk_auto_id']."'>\n";
        $tmp_html .="<input type = 'hidden' name='mod' value='2'>\n";
        $tmp_html .="<input type='submit' class = 'button' value='キャンセル' style = 'width:60px;'>\n";
        $tmp_html .="</form>\n";
        $tmp_html .="</td>\n";
        $tmp_html .="</tr>\n";
    }
    $tmp_html .="</table>";
}
$html ="<html lang='utf-8'>\n";
$html .="<head>\n";
$html .="<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
$html .="<title>設定・確認</title>\n";

$html .="<style TYPE='text/css'>\n";

$html .="body{\n";
$html .="background-color:#FFFFFF;\n";
$html .="font-size:9pt;\n";
$html .="height:600;\n";
$html .="width:550;\n";
$html .="font-family:'メイリオ',Meiryo,'MS P Gothic','ＭＳ Ｐゴシック',Verdana,Arial,sans-serif;\n";
$html .="}\n";

$html .="table{\n";
$html .="font-size:9pt;\n";
$html .="}\n";

$html .="table.list{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="}\n";

$html .="table.list th{\n";
$html .="text-align:center;\n";
$html .="color:#555;\n";
$html .="background-color:#D3D3D3;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="height:25px;\n";
$html .="}\n";

$html .="table.list td{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="height:20px;\n";
$html .="}\n";

$html .=".button{\n";
$html .="background-color:#828282;\n";
$html .="color:#FFFFFF;\n";
$html .="border-color:#FFFAFA;\n";
$html .="margin-top:0px;\n";
$html .="text-align: center;\n";
$html .="}\n";

$html .="</style>\n";

$html .="</head>\n";
$html .="<body>\n";

$html .="<fieldset>\n";
$html .="<legend>割当設定</legend>\n";

$html .=$tmp_html;

$html .="<form method='POST' action='set_auto_check.php'>\n";
$html .="<table class = 'list'>\n";
$html .="<tr>\n";
$html .="<th style = 'width:100px;'>商品ID</th>\n";
$html .="<td style = 'width:50px;'>$id<input type='hidden' name='prod_id' value='$id'></input></td><BR>\n";
$html .="</tr>\n";
$rs=GetNowPrice($id);
$ret=mysql_fetch_array($rs);
$html .="<tr>\n";
$html .="<th>現在価格</th>\n";
$html .="<td>".$ret['f_now_price']."</td><BR>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$r_param = $ret['f_r'];
$r_end = ($ret['f_min_price']* $r_param/100)/100 ;
$html .="<th>対支終了額</td>\n";
$html .="<td>".number_format($r_end)."</td><BR>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$p_param = $ret['f_p'];
$p_end = ($ret['f_min_price']* $p_param/100)/100 ;
$html .="<th>対人終了額</th>\n";
$html .="<td>".number_format($p_end)."</td><BR>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$html .="<th>開始価格</th>\n";
$html .="<td><input type='text' name='min_price' istyle='4' size='5' value='0'></input></td><BR>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$html .="<th>終了価格</th>\n";
$html .="<td><input type='text' name='max_price' istyle='4' size='5' value='0'></input></td><BR>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$html .="<th>人数</th>\n";
$html .="<td><input type='text' name='num' istyle='4' size='5' value='2'></input></td><BR>\n";
$html .="</tr>\n";
$html .="</table>\n";
$html .="<br>\n";
$html .="<select name='type'><OPTION value='0'>割当済み会員からランダムで設定する</OPTION>\n";
$html .="<OPTION value='1'>全ての会員からランダムで設定する</OPTION>\n";
$html .="<OPTION value='2'>割当済み会員から選択して設定する</OPTION>\n";
$html .="</SELECT>\n";
$html .="</fieldset>";
$html .="<BR>\n";

$html .="<input type='hidden' name='spend_coin' value='$spend_coin'>\n";
$html .="<input type='hidden' name='market' value='$market'>\n";
$html .="<div style = 'text-align:center;'>\n";
$html .="<input type='submit' class = 'button' style = 'width:120px; margin-buttom:100px;' value='設定確認'>\n";
$html .="<input type='button' class = 'button' style = 'width:120px; margin-buttom:100px;' value=\"閉じる\" onClick=\"window.close(); return false;\">\n";
$html .="<input type = 'hidden' name='mod' value='1'>";
$html .="<input type = 'hidden' name='f_min_price' value='".$ret['f_min_price']."'>";
$html .="</div>\n";
$html .="</form>\n";
$html .="<br>\n";
$html .="</html>\n";
echo $html;

?>
