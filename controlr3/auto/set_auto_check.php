<style TYPE="text/css">
body{
background-color:#FFFFFF;
font-size:9pt;
height:600;
width:500;
font-family:"メイリオ",Meiryo,"MS P Gothic","ＭＳ Ｐゴシック",Verdana,Arial,sans-serif;
}

table{
font-size:9pt;
}

table.list{
text-align:center;
border: solid 1px #999999;
border-collapse:collapse;
}

table.list th{
text-align:center;
background-color:#D3D3D3;
border: solid 1px #999999;
border-collapse:collapse;
height:25px;
}

table.list td{
text-align:center;
border: solid 1px #999999;
border-collapse:collapse;
height:20px;}

.button{
background-color:#828282;
color:#FFFFFF;
border-color:#FFFAFA;
margin-top:10px;
text-align: center;
}

</style>

<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/exec_update.php';
$mod=$_REQUEST['mod'];
$html="";
if($mod == 1)
{
    $prod_id = $_REQUEST['prod_id'];
    $min_price=$_REQUEST['min_price'];
    $max_price=$_REQUEST['max_price'];
    $num=$_REQUEST['num'];
    $type=$_REQUEST['type'];
    $f_min_price =$_REQUEST['f_min_price'];
    $spend_coin=$_REQUEST['spend_coin'];
    $market = $_REQUEST['market'];

    $html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>";
    if($prod_id =="" ||$min_price=="" || $max_price == "" || $num =="" || $type =="")
    {
        $html.="<P>不正アクセスです</P></body></html>";
        //$html.="$prod_id,$min_price,$max_price,$num,$type";
        printf($html);
        return;
    }

    if(is_numeric($min_price)==false || is_numeric($max_price) == false || is_numeric($num) == false )
    {
        $html.="<P>開始価格・終了価格・人数には数値を入力してください</P></body></html>";
        //$html.="$prod_id,$min_price,$max_price,$num,$type";
        printf($html);
        return;
    }

    if($spend_coin < 1 && $market < $max_price)
    {
        $html.="<P>このオークションは上限金額を市場価格より大きく設定はできません</P></body></html>";
        printf($html);
        return;
    }

    $rs=getUpPrice($prod_id);
    $ret=mysql_fetch_array($rs);
    $up_price=$ret['f_up_price'];

    if($type == 0 || $type== 1)
    {
        //会員取得
        $rs=getAutoMem($prod_id,$type,$num);
        $result_num=mysql_num_rows($rs);
        $mem_id;
        for($i=0;$i<$result_num ;$i++)
        {
            $ret=mysql_fetch_array($rs);
            $mem_id[$i]=$ret['fk_member_id'];
        }
        //回数計算
        $tmp_count=floor(($max_price-$min_price)/$up_price) +1;
        $execed_count=0;
        $tmp_num=$num;
        $tmp_table ="<P>以下で設定を行います。(会員を変えたい場合F5を押してください)</P>";
        $tmp_table.="<form method ='POST' action ='set_auto_result.php'>";
        $tmp_table.="<input type = 'hidden' name ='num' value='$num'>";
        $tmp_table.="<input type = 'hidden' name ='prod_id' value='$prod_id'>";
        $tmp_table.="<table class = 'list'>\n";
        $tmp_table.="<tr>\n";
        $tmp_table.="<th style = 'width:70px;'>会員ID</th>\n";
        $tmp_table.="<th style = 'width:70px;'>開始価格</th>\n";
        $tmp_table.="<th style = 'width:70px;'>終了価格</th>\n";
        $tmp_table.="<th style = 'width:70px;'>回数</th>\n";
        $tmp_table.="</tr>\n";

        for($i=0;$i<$num;$i++)
        {
            //$ins_count=round(($tmp_count-$execed_count)/$tmp_num);
            //$execed_count +=$ins_count;
            //$tmp_num--;
            $ins_count = $tmp_count;
            $tmp_table.="<tr>\n";
            $tmp_table.="<td>$mem_id[$i]<input type ='hidden' name='mem_id[]' value='$mem_id[$i]'></td>\n";
            $tmp_table.="<td>".number_format($min_price)."<input type ='hidden' name='min_price[]' value='$min_price'></td>\n";
            $tmp_table.="<td>".number_format($max_price)."<input type ='hidden' name='max_price[]' value='$max_price'></td>\n";
            $tmp_table.="<td>$ins_count<input type ='hidden' name='ins_count[]' value='$ins_count'></td>\n";
            $tmp_table.="</tr>\n";
        //printf($i);
            //insRobAuto($prod_id,$mem_id[$i],$min_price,$max_price,$ins_count);
        }
        $tmp_table.="<tr>\n";
        $tmp_table .="<table>\n";
        $tmp_table .="<tr>\n";
        $tmp_table .="<td><input type='submit' style = 'width:80px;' class = 'button' value='確認'></td><td><input type='button' class = 'button' value=\"閉じる\" onClick=\"window.close(); return false;\"></td>\n";
        $tmp_table .="</tr>\n";
        $tmp_table.="</table>\n";
        $tmp_table.="</tr>\n";
        $tmp_table.="</table>\n";
        $tmp_table.="</form>\n";
    }
    else if($type == 2)
    {
        //会員取得
        $rs=getAutoMem($prod_id,$type,$num);
        $result_num=mysql_num_rows($rs);
        $mem_id;
        
        $selection="<SELECT name='mem_id[]'>";

        $mem_list ="<P>以下に本商品に割り当てられた会員を表示します</p>";
        $mem_list .="<table border = '1' >";
        $mem_list .="<th>会員ID</th>";
        $mem_list .="<th>ハンドル名</th>";
        $mem_list .="<th>入札下限</th>";
        $mem_list .="<th>入札上限</th>";
        for($i=0;$i < $result_num ;$i++)
        {
            $ret = mysql_fetch_array($rs);
            $member_id=$ret['fk_member_id'];
            $low=$ret['f_age1']/100;
            $upper=$ret['f_age2']/100;
            $handle=$ret['f_handle'];
            //echo $member_id ."<BR>";
            $selection .="<OPTION value='$member_id' >$member_id</OPTION>";

            $mem_list .="<tr>\n";
            $mem_list .="<td>$member_id</td>\n";
            $mem_list .="<td>$handle</td>\n";
            $mem_list .="<td>$low</td>\n";
            $mem_list .="<td>$upper</td>\n";
            $mem_list .="</tr>\n";
        }
        $selection .="</SELECT>";
        $mem_list .="</table>";
        $f_min_price = $f_min_price == 0?1:$f_min_price;
        
        $start_param = $min_price /$f_min_price * 100;
        $end_param = $max_price /$f_min_price*100;

        $param_table ="<table border ='1'>";
        $param_table .="<th>開始(%)</th>";
        $param_table .="<th>終了(%)</th>";
        $param_table .="<tr>\n";
        $param_table .="<td>".number_format($start_param,2,'.','')."%</td>\n";
        $param_table .="<td>".number_format($end_param,2,'.','')."%</td>\n";
        $param_table .="</tr>\n";
        $param_table .="</table>\n";
        $param_table .="<P>設定した価格は仕入値に対して上記の割合になります。<BR>上記を参考にして以下で割り当てる会員を選択してください</P>";

        $tmp_table="以下で設定を行います。";
        $tmp_table .="<form method ='POST' action ='set_auto_result.php'>";
        $tmp_table.="<input type = 'hidden' name ='num' value='$num'>";
        $tmp_table.="<input type = 'hidden' name ='prod_id' value='$prod_id'>";
        $tmp_table .="<table border ='1'>";
        $tmp_table .="<th>会員ID</th>";
        $tmp_table .="<th>下限価格</th>";
        $tmp_table .="<th>上限価格</th>";
        $tmp_table .="<th>入札回数</th>";

        $execed_count=0;
        $tmp_num=$num;
        $tmp_count=floor(($max_price-$min_price)/$up_price) +1;
            
        for($i=0;$i<$num ;$i++)
        {
            $ins_count=round(($tmp_count-$execed_count)/$tmp_num);
            $execed_count +=$ins_count;
            $tmp_num--;
            $tmp_table .="<tr>\n";
            $tmp_table .="<td>$selection</td>\n";
            $tmp_table.="<td>".number_format($min_price)."<input type ='hidden' name='min_price[]' value='$min_price'></td>\n";
            $tmp_table.="<td>".number_format($max_price)."<input type ='hidden' name='max_price[]' value='$max_price'></td>\n";
            $tmp_table.="<td>$ins_count<input type ='hidden' name='ins_count[]' value='$tmp_count'></td>\n";
            $tmp_table .="</tr>\n";
        }
        $tmp_table .="<table>\n";
        $tmp_table .="<tr>\n";
        $tmp_table .="<td><input type='submit' value='確認' class = 'button'></td><td><input type='button' class = 'button' value=\"閉じる\" onClick=\"window.close(); return false;\"></td>\n";
        $tmp_table .="</tr>\n";
        $tmp_table.="</table>\n";
        $tmp_table .="</table>\n<BR>";
        $tmp_table .=$mem_list;
        $tmp_table = $param_table.$tmp_table;
    }
    $html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";

    $html .="</head><body>";
    if($spend_coin ==0)
    {
        $html .="<P><font color ='RED'>支援会員が落札者となる場合余波が発生します。それを勘案して上限価格を設定しているか再度確認してください</font></P>";
    }
    $html .=$tmp_table;
    $html.= "</body></html>\n";
}
else if($mod == 2)
{
    $auto_id=$_REQUEST['id'];
    UpRobAuto($auto_id);
    $html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>";
    $html .="<p>設定をキャンセルしました<p><br>\n";
    $html .= "<input type=\"button\" style=\"width:80px;\"class = 'button' value=\"閉じる\" onClick=\"window.close(); return false;\">";
    $html.= "</body></html>\n";
}
echo $html;
?>
