<?php
include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/exec_update.php';
include '../control/control.php';

if(empty($_POST['check'])==TRUE || $_POST['check'] != 1)
{
    $html='<p>不正アクセスです</p>';
    printf($html);
    return;
}

$worm="";
if(check_input($_POST['min_time']) != 0)
{
    $worm .="<p>支援起動下限時間に不正な値があります(数値を入力してください)</p>\n";
}
if($_POST['min_time'] < 0)
{
    $worm .="<p>支援起動下限時間に0未満は設定できません。1以上を設定してください</p>\n";
}

if(check_input($_POST['max_time']) != 0)
{
    $worm .="<p>支援起動上限時間に不正な値があります(数値を入力してください)</p>\n";
}
if(check_input($_POST['exec_num']) != 0)
{
    $worm .="<p>支援同時実行数に不正な値があります(数値を入力してください)</p>\n";
}
if($_POST['exec_num'] < 1)
{
    $worm .="<p>支援同時実行数に0以下は設定できません。1以上を設定してください</p>\n";
}
if(check_input($_POST['up_time']) != 0)
{
    $worm .="<p>上昇時間数に不正な値があります(数値を入力してください)</p>\n";
}
if($_POST['up_time'] < 1)
{
    $worm .="<p>上昇時間数に0以下は設定できません。1以上を設定してください</p>\n";
}
if(check_input($_POST['limit_time']) != 0)
{
    $worm .="<p>上昇時間数上限に不正な値があります(数値を入力してください)</p>\n";
}
if(check_input($_POST['end_rob']) != 0)
{
    $worm .="<p>支援終了率(保険)に不正な値があります(数値を入力してください)</p>\n";
}
if($_POST['end_rob'] < 1)
{
    $worm .="<p>支援終了率(保険)に0以下は設定できません。1以上を設定してください</p>\n";
}
if(check_input($_POST['end_pc']) != 0)
{
    $worm .="<p>対人終了率(保険)に不正な値があります(数値を入力してください)</p>\n";
}
if($_POST['end_pc'] < 1)
{
    $worm .="<p>対人終了率(保険)に0以下は設定できません。1以上を設定してください</p>\n";
}
if($_POST['after_min'] < 0)
{
    $worm .="<p>余波(最小)に0未満は設定できません。0以上を設定してください</p>\n";
}
if($_POST['after_max'] < 0)
{
    $worm .="<p>余波(最大)に0未満は設定できません。0以上を設定してください</p>\n";
}

if($_POST['after_min'] > 0 && $_POST['after_max'] > 0)
{
    if($_POST['after_min'] == $_POST['after_max'])
    {
        $worm .="<p>余波(最小)と余波(最大)が同じ数にすることはできません(余波無しのときのみ可能にしています)</p>\n";
    }
}

if($_POST['after_min'] > $_POST['after_max'])
{
    $worm .="<p>余波(最小)を余波(最大)より大きくすることはできません</p>\n";
}

if(!($_POST['check_price'] == 0 || $_POST['check_price'] == 1))
{
    $worm .="<p>監視対象価格が不正です。選択内容を確認してください</p>\n";
}
$check_box=array();
for($i=0;$i<$_POST['r_param_num'];$i++)
{
    if(empty($_POST['r_flag'][$i]))
    {
        $check_box[$i]=0;
    }
    else
    {
        $check_box[$i]="no";
    }
}

$r_start=$_POST['r_start'];
$r_end=$_POST['r_end'];
$kakuritsu1=$_POST['kakuritsu1'];
$kakuritsu2=$_POST['kakuritsu2'];
$num=0;
for($i=0;$i<$_POST['r_param_num'];$i++)
{
    if($check_box[$i] == "0")
    {
        if(check_input($r_start[$i]) != 0)
        {
             $worm .="<p>支援起動時間(下限".($i+1).")に不正な値があります(数値を入力してください)</p>\n";
        }
        if(check_input($r_end[$i]) != 0)
        {
             $worm .="<p>支援起動時間(上限".($i+1).")に不正な値があります(数値を入力してください)</p>\n";
        }
        if(check_input($kakuritsu1[$i]) != 0)
        {
             $worm .="<p>支援起動確率(分子)".($i+1)."に不正な値があります(数値を入力してください)</p>\n";
        }
	if(check_input($kakuritsu2[$i]) != 0)
        {
             $worm .="<p>支援起動確率(分母)".($i+1)."に不正な値があります(数値を入力してください)</p>\n";
        }
    }
    else
    {
        $r_start[$i]=0;
        $r_end[$i]=0;
        $kakuritsu1[$i]=0;
	$kakuritsu2[$i]=0;
        $num++;
    }
}
$rnum=$_POST['r_param_num'];
if($num==10)
{
    $rnum=0;
}

if($worm =="")
{
    if(check_value($_POST['min_time'], $_POST['max_time']) !=0)
    {
        $worm .="<p>支援起動最小秒数が最大秒数と等しいか大きいです。最小秒数&lt;最大秒数となる値を設定してください</p>\n";
    }
    if(check_value($_POST['up_time'], $_POST['limit_time']) !=0)
    {
        $worm .="<p>入札後の上昇秒数が最大上昇秒数と等しいか大きいです。上昇秒数&lt;最大上昇秒数となる値を設定してください</p>\n";
    }
	
}
if($worm =="")
{
    for($i=0;$i<$_POST['r_param_num'];$i++)
    {
        if($r_end[$i] < 1 || $r_start[$i] < 1 || $kakuritsu1 < 1 || $kakuritsu2 < 1)
        {
            $check_box[$i]="no";
            continue;
        }
        if(check_value($r_start[$i], $r_end[$i]) !=0)
        {
            $worm .="<p>支援起動時間(下限)が支援起動時間(上限)と等しいか大きいです。最小秒数&lt;最大秒数となる値を設定してください</p>\n";
        }
	if($kakuritsu1[$i] > $kakuritsu2[$i])
        {
            $worm .="<p>支援起動確率は仮分数形式にはできません(最大で1/1です)。帯分数形式になるように設定してください</p>\n";
        }
    }
}

if($worm !="")
{
    $html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>\n";
    $html .= $worm;
    $html .= "<A href='def.php'>戻る</a>\n";
    printf($html);
    return;
}

function faultUpdate()
{
    $html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>\n";
    $html .="<P>設定を変更に失敗しました</P>";
    $html .= "<A href='def.php'>戻る</a>\n";
    $html .="</body></html>\n";

    print($html);
}
$param=array($_POST['version'],$_POST['min_time'],$_POST['max_time'],
            $_POST['exec_num'],$_POST['mim_price'],$_POST['max_price'],
            $_POST['up_time'],$_POST['limit_time'],$_POST['point_val'],
            $_POST['end_rob'],$_POST['end_pc'],$_POST['free'],$_POST['after_min'],$_POST['after_max'],$_POST['check_price'],$_POST['end_none'],$_POST['r_param_num']);
$db=db_connect();
if($db == false)
{
    faultUpdate();
    exit;
}
$rs=GetRVersion($db);
$ret = mysql_fetch_array($rs);
$ver=$ret['f_prm1'];
db_close($db);

$db=db_connect();
if($db == false)
{
    faultUpdate();
    exit;
}
if(mysql_query("begin",$db)==false)
{
    mysql_error();
    db_close($db);
    faultUpdate();
    exit;
}
if($ver==3)
{
    if(param3to4($db) == 1)
    {
        faultUpdate();
        exit;
    }
}
if(SetRSystem($param,$db) == 1)
{
    faultUpdate();
    exit;
}

for($i=0;$i<$_POST['r_param_num'];$i++)
{
    $param=array($r_start[$i],$r_end[$i],$kakuritsu1[$i],$kakuritsu2[$i]);
    
    if(SetRSystem2($param,(17+$i*4),$db) == 1)
    {
        faultUpdate();
        exit;
    }
}
if(mysql_query("commit",$db)==false)
{
    mysql_error();
    faultUpdate();
    exit;
}
db_close($db);
$html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>\n";
$html .="<P>設定を変更しました</P>";
$html .= "<A href='def.php'>戻る</a>\n";
$html .="</body></html>\n";

print($html);
?>
