<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/exec_update.php';
include '../control/control.php';
$head="<html lang='ja'>\n<head>\n<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n</head>\n<body>";
$foot="</body></html>";
if(empty($_POST['flag'])==TRUE)
{
    $html ="<P>不正アクセスです</P>";
    print($html);
    return;
}

if($_POST['flag']==1)
{
    $worm="";
    //更新処理

    //入力値チェック
    if(check_input($_POST['battle_min'])!=0 )
    {
        $worm .="<p>支援達成率下限に不正な値があります(数値を入力してください)</p>\n";
    }
    if($_POST['battle_min'] < 0.01 || $_POST['battle_min'] > 999.99)
    {
        $worm .="<p>支援達成率下限に0.01未満又は999.99以上は設定できません。</p>\n";
    }

    if(check_input($_POST['battle_max'])!=0 )
    {
        $worm .="<p>支援達成率上限に不正な値があります(数値を入力してください)</p>\n";
    }
    if($_POST['battle_max'] > 999.99)
    {
        $worm .="<p>支援達成率上限に999.99以上は設定できません。</p>\n";
    }

    if(check_input($_POST['pc_battle_min'])!=0 )
    {
        $worm .="<p>対人達成率下限に不正な値があります(数値を入力してください)</p>\n";
    }
    if($_POST['pc_battle_min'] < 0.01  || $_POST['pc_battle_min'] > 999.99)
    {
        $worm .="<p>対人達成率下限に0未満又は999.99以上は設定できません。</p>\n";
    }
    if(check_input($_POST['pc_battle_max'])!=0 )
    {
        $worm .="<p>対人達成率上限に不正な値があります(数値を入力してください)</p>\n";
    }
    if($_POST['pc_battle_max'] > 999.99)
    {
        $worm .="<p>対人達成率上限に999.99以上は設定できません。</p>\n";
    }
    $in_f=1;
    if(empty($_POST['id'])==false)
    {
        $in_f=0;
        $id=$_POST['id'];
        $count =count($id);
        $min=$_POST['min'];
        $max=$_POST['max'];
        $num=$_POST['num'];
        for($i=0;$i<$count;$i++)
        {
            if(check_input($min[$i]) !=0 )
            {
                $worm .="<p>NO:".$id[$i]."の支援入札下限値に不正な値があります(数値を入力してください)</p>\n";
            }
            if($min[$i] < 0 )
            {
                $worm .="<p>NO:".$id[$i]."の支援入札下限値に0未満は設定できません。1以上を設定してください</p>\n";
            }
            if(check_input($max[$i])!=0 )
            {
                $worm .="<p>NO:".$id[$i]."の支援入札上限値に不正な値があります(数値を入力してください)</p>\n";
            }
            if($max[$i] >= 1000 )
            {
                $worm .="<p>NO:".$id[$i]."の支援入札上限値に1001以上は設定できません。</p>\n";
            }
            if(check_input($num[$i])!=0 )
            {
                $worm .="<p>NO:".$id[$i]."の支援割り当て数に不正な値があります(数値を入力してください)</p>\n";
            }
            if($num[$i] < 1 )
            {
                $worm .="<p>NO:".$id[$i]."の支援割り当て数に0以下は設定できません。1以上を設定してください</p>\n";
            }
        }
    }
    
    $f=0;
    if(empty($_POST['ins_max']))
    {
        //print("bbb");
        $f=1;
    }
    else if( $_POST['ins_min'] ==0 ||$_POST['ins_max'] != "")
    {
        if($_POST['ins_min'] == '' || $_POST['ins_max'] =='' || $_POST['ins_max']=='')
        {
            //print($_POST['ins_min']);
            $f=1;
        }
        if($f == 0 && check_input($_POST['ins_min']) !=0 )
        {
            $worm .="<p>新規追加の支援入札下限値に不正な値があります(数値を入力してください)</p>\n";
        }
        if($f == 0 && $_POST['ins_min'] < 0 )
        {
            $worm .="<p>新規追加の支援入札下限値に0未満は設定できません。1以上を設定してください</p>\n";
        }
        if($f == 0 && check_input($_POST['ins_max'])!=0 )
        {
            $worm .="<p>新規追加の支援入札上限値に不正な値があります(数値を入力してください)</p>\n";
        }
        if($f == 0 && $_POST['ins_max']>=1000 )
        {
            $worm .="<p>新規追加の支援入札上限値に1000以上は設定できません。</p>\n";
        }
        if($f == 0 && check_input($_POST['ins_num'])!=0 )
        {
            $worm .="<p>新規追加の支援割り当て数に不正な値があります(数値を入力してください)</p>\n";
        }
        if($f == 0 && $_POST['ins_num'] < 1  )
        {
            $worm .="<p>NO:".$id[$i]."の支援割り当て数に0以下は設定できません。1以上を設定してください</p>\n";
        }
    }


    if($worm == "")
    {
        if(check_value($_POST['battle_min'], $_POST['battle_max']))
        {
            $worm .="<p>支援達成率下限が達成率上限と等しいか大きいです。達成率下限&lt;達成率上限となる値を設定してください</p>\n";
        }
        if(check_value($_POST['pc_battle_min'], $_POST['pc_battle_max']))
        {
            $worm .="<p>対人達成率下限が達成率上限と等しいか大きいです。達成率下限&lt;達成率上限となる値を設定してください</p>\n";
        }
        if($in_f==0)
        {
            for($i=0;$i<$count;$i++)
            {
                if(check_value($min[$i], $max[$i]) !=0 )
                {
                    $worm .="<p>NO:".$id[$i]."の支援入札下限値が上限値と等しいか大きいです。下限値&lt;上限値となる値を設定してください</p>\n";
                }
            }
        }
        if($f == 0)
        {
            if(check_value($_POST['ins_min'],$_POST['ins_max']))
            {
                $worm .="<p>新規追加の支援入札下限値が上限値と等しいか大きいです。下限値&lt;上限値となる値を設定してください</p>\n";
            }
        }
    }
    if($worm !="")
    {
        //不正値が合ったためメッセージ表記後終了
        $html=$worm;
        $html .="<a href='cat.php?cat=".$_POST['cat'] . "'>戻る</a>\n";
        $html =$head.$html.$foot;
        print($html);
        return;
    }
    $battle_min=$_POST['battle_min'];
    $battle_max=$_POST['battle_max'];
    if(strlen($battle_min) !=6)
    {
        $tmp=strpos($battle_min,".");
        if((strlen($battle_min) == 4 && $tmp ==1) || (strlen($battle_min) == 5 && $tmp ==2))
        {}
        else if($tmp === false)
        {
            $battle_min =$battle_min.".00";
        }
        else if((strlen($battle_min) == 3 && $tmp ==1) || (strlen($battle_min) == 4 && $tmp ==2) || (strlen($battle_min) == 5 && $tmp ==3) )
        {
            $battle_min =$battle_min."0";
        }
        else
        {
             $worm .="<p>支援達成率下限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    else
    {
        $tmp=strpos($battle_min,".");
        if($tmp != 3)
        {
            $worm .="<p>支援達成率下限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    if(strlen($battle_max) !=6)
    {
        $tmp=strpos($battle_max,".");
        if((strlen($battle_max) == 4 && $tmp ==1) || (strlen($battle_max) == 5 && $tmp ==2)){}
        else if($tmp === false)
        {
            $battle_max =$battle_max.".00";
        }
        else if((strlen($battle_max) == 3 && $tmp ==1) || (strlen($battle_max) == 4 && $tmp ==2) || (strlen($battle_max) == 5 && $tmp ==3) )
        {
            $battle_max =$battle_max."0";
        }
        else
        {
             $worm .="<p>支援達成率上限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    else
    {
        $tmp=strpos($battle_max,".");
        if($tmp != 3)
        {
            $worm .="<p>支援達成率上限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    $pc_battle_min=$_POST['pc_battle_min'];
    $pc_battle_max=$_POST['pc_battle_max'];
    if(strlen($pc_battle_min) !=6)
    {
        $tmp=strpos($pc_battle_min,".");
        if((strlen($pc_battle_min) == 4 && $tmp ==1) || (strlen($pc_battle_min) == 5 && $tmp ==2)){}
        else if($tmp === false)
        {
            $pc_battle_min =$pc_battle_min.".00";
        }
        else if((strlen($pc_battle_min) == 3 && $tmp ==1) || (strlen($pc_battle_min) == 4 && $tmp ==2) || (strlen($pc_battle_min) == 5 && $tmp ==3) )
        {
            $pc_battle_min =$pc_battle_min."0";
        }
        else
        {
             $worm .="<p>対人達成率下限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    else
    {
        $tmp=strpos($pc_battle_min,".");
        if($tmp != 3)
        {
            $worm .="<p>人達成率下限が不正です。少数二桁で指定してください</p>\n";
        }
    }

    if(strlen($pc_battle_max) !=6)
    {
        $tmp=strpos($pc_battle_max,".");
        if((strlen($pc_battle_max) == 4 && $tmp ==1) || (strlen($pc_battle_max) == 5 && $tmp ==2)){}
        else if($tmp === false)
        {
            $pc_battle_max =$pc_battle_max.".00";
        }
        else if((strlen($pc_battle_max) == 3 && $tmp ==1) || (strlen($pc_battle_max) == 4 && $tmp ==2) || (strlen($pc_battle_max) == 5 && $tmp ==3) )
        {
            //echo $r_battle;
            $pc_battle_max =$pc_battle_max."0";
        }
        else
        {
             $worm .="<p>対人達成率上限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    else
    {
        $tmp=strpos($pc_battle_max,".");
        if($tmp != 3)
        {
            $worm .="<p>対人達成率上限が不正です。少数二桁で指定してください</p>\n";
        }
    }
    if($worm !="")
    {
        //不正値が合ったためメッセージ表記後終了
        $html=$worm;
        $html .="<a href='cat.php?cat=".$_POST['cat'] . "'>戻る</a>\n";
        $html =$head.$html.$foot;
        print($html);
        return;
    }
    $cat=$_POST['cat'];
    $r=$battle_min*100000*100 + $battle_max*100;
    $p=$_POST['pc_battle_min']*100000*100 + $_POST['pc_battle_max']*100;
    UpItemCategory($cat, $r, $p);

    DelCatMat($cat);
    if($in_f==0)
    {
        for($i=0;$i<$count;$i++)
        {
            InsCatMat($cat, $min[$i]*100, $max[$i]*100, $num[$i]);
        }
    }
    if($f==0)
    {
        InsCatMat($cat, $_POST['ins_min']*100, $_POST['ins_max']*100, $_POST['ins_num']);
    }
    $html="<P>完了しました<P><BR>\n";
    $html .="<a href='cat.php?cat=".$_POST['cat'] . "'>戻る</a>\n";
    $html =$head.$html.$foot;
    print($html);
    return;

}
?>
