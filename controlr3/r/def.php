<?php

include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/control.php';

$param1=0;
$param=array();

$rs=GetSystem();
$ret = mysql_fetch_array($rs);

$param1=$ret['f_rupdate_flg'];

$rs=GetRParam();
$num=mysql_num_rows($rs);

for($i=0;$i<$num;$i++)
{
    $ret = mysql_fetch_array($rs);
    $param[$i]=$ret['f_prm1'];
}

$html ="<html lang='utf-8'>\n";
$html .="<head>\n";
$html .="<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
$html .="<style type='text/css'>\n";
$html .="body{\n";
$html .="background-color:#FFFFFF;\n";
$html .="font-size:9pt;\n";
$html .="font-family:'メイリオ',Meiryo,'MS P Gothic','ＭＳ Ｐゴシック',Verdana,Arial,sans-serif;\n";
$html .="}\n";

$html .="table{\n";
$html .="color:#555;\n";
$html .="font-size:9pt;\n";
$html .="}\n";

$html .="table.form td{\n";
$html .="height:25px;\n";
$html .="padding-left:5px;\n";
$html .="}\n";

$html .="table.form th{\n";
$html .="text-align:right;\n";
$html .="color:#555;\n";
$html .="background-color:#D3D3D3;\n";
$html .="width:150px;\n";
$html .="height:25px;\n";
$html .="}\n";

$html .="table.list{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999;\n";
$html .="border-collapse: collapse;\n";
$html .="}\n";

$html .="table.list td{\n";
$html .="height:25px;\n";
$html .="border:solid 1px #999;\n";
$html .="}\n";

$html .="table.list th{\n";
$html .="height:25px;\n";
$html .="border:solid 1px #999;\n";
$html .="background-color:#D3D3D3;\n";
$html .="}\n";

$html .=".button{\n";
$html .="background-color:#828282;\n";
$html .="width:120px;\n";
$html .="color:#FFFFFF;\n";
$html .="border-color:#FFFAFA; \n";
$html .="}\n";
$html .="</style>\n";
$html .="</head>\n";

$html .="<body>\n";
$html .="<fieldset>\n";
$html .="<legend>基本設定</legend>\n";
$html .="<form action = 'def_result.php' method = 'POST'>\n";
$html .="<table class = 'form'>\n";
if($param1==0)
{
    $html .="<tr>\n";
    $html .="<th>反映状態</th>\n";
    $html .="<td>適用されています</td>\n";
    $html .="</tr>\n";
}
elseif($param1==1)
{
    $html .="<tr>\n";
    $html .="<th>反映状態</th>\n";
    $html .="<td>更新中です</td>\n";
    $html .="</tr>\n";
}
$html .="<tr>\n";
$html .="<th>バージョン</th>\n";
$html .="<td>4<input type = 'hidden' size = '4' maxlength = '4' value = '4' name = 'version'></td>\n";
$html .="</tr>\n";

$html .="<input type='hidden' size='4' maxlength='4' istayle='4' value='$param[1]' name='min_time'><input type='hidden' size='4' maxlength='4' istayle='4' value='$param[2]' name='max_time'>";
$html .="<input type='hidden' size='4' maxlength='4' istayle='4' value='$param[4]' name='mim_price'><input type='hidden' size='4' maxlength='4' istayle='4' value='$param[5]' name='max_price'>\n";

$html .="<th>上昇秒数</th>\n";
$html .="<td><input type = 'text' size = '7' maxlength = '4' value = '$param[6]' name = 'up_time' style = 'ime-mode: disabled;'>&nbsp;秒</td>\n";

$uptimelimit=LIMIT_PLUS_TIME;
$html .="<tr>\n";
$html .="<th>上限上昇秒数</th>\n";
$html .="<td><input type = 'hidden' value = '$uptimelimit' name = 'limit_time' style = 'ime-mode: disabled;'>".$uptimelimit."秒</td>\n";
$html .="</tr>\n";

$html .="<tr>\n";
$html .="<th>ポイント価格</th>\n";
$html .="<td>\n";
$html .="<input type='hidden' size='4' maxlength='4' istayle='4' value='".POINT_VALUE."' name='point_val'>$param[8]円<br>\n";
$html .="(番組内でのポイント価格:".POINT_VALUE."円)\n";
$html .="</td>\n";
$html .="</tr>\n";

$html .="<tr>\n";
$html .="<th>支援終了率(予備用)</th>\n";
$html .="<td><input type='text' size='4' maxlength='4' value='$param[9]' name='end_rob' style = 'ime-mode: disabled;'>&nbsp;%</td>\n";
$html .="</tr>\n";

$html .="<tr>\n";
$html .="<th>対人終了率(予備用)</th>\n";
$html .="<td><input type='text' size='4' maxlength='4' value='$param[10]' name='end_pc' style = 'ime-mode: disabled;'>&nbsp;%</td>\n";
$html .="</tr>\n";

if($param[11]==0)
{
    $selected0 ="selected";
    $selected1 ="";
}
else
{
    $selected1 ="selected";
    $selected0 ="";
}

$html .="<tr>\n";
$html .="<th>フリー落札</th>\n";
$html .="<td><select name='free'>\n";
$html .="<option value='0' $selected0>不許可</option>\n";
$html .="<option value='1' $selected1>許可</option>\n";
$html .="</td>\n";
$html .="</tr>\n";

$html .="<tr>\n";
$html .="<th>対支余剰入札回数</th>\n";
$html .="<td>\n";
$html .="<input type='text' size='4' maxlength='4' value='$param[12]' name='after_min' style = 'ime-mode: disabled;'>&nbsp;回～\n";
$html .="<input type='text' size='4' maxlength='4' value='$param[13]' name='after_max' style = 'ime-mode: disabled;'>&nbsp;回\n";
$html .="</td>\n";
$html .="</tr>\n";

if($param[14]==0)
{
    $selected0 ="selected";
    $selected1 ="";
}
else
{
    $selected1 ="selected";
    $selected0 ="";
}

$html .="<tr>\n";
$html .="<th>対支開始・終了基準価格</th>\n";
$html .="<td>\n";
$html .="<select name='check_price' >\n";
$html .="<option value='0' $selected0>監視価格</option>\n";
$html .="<option value='1' $selected1>現在価格</option>\n";
$html .="</td>\n";
$html .="</tr>\n";

if($param[15]==0)
{
    $selected_0 ="selected";
    $selected_1 ="";
}
else
{
    $selected_1 ="selected";
    $selected_0 ="";
}

$html .="<tr>\n";
$html .="<th>入札者なしでの終了</th>\n";
$html .="<td>\n";
$html .="<select name='end_none' >\n";
$html .="<option value='0' $selected_0>なし</option>\n";
$html .="<option value='1' $selected_1>あり</option>\n";
$html .="</td>\n";
$html .="</tr>\n";

$html .="</table>\n";
$html .="</fieldset>\n";

$html .="<br>\n";

$html .="<fieldset>\n";
$html .="<legend>自動入札<b style = 'color:#EE0000;'>※参考にしてください</b></legend>\n";
$html .="<table class = 'form'>\n";
$html .="<tr>\n";
$html .="<th>起動下限秒数</th>\n";
$html .="<td>".AUTO_MIN_TIME."</td>\n";
$html .="</tr>\n";

$html .="<tr>\n";
$html .="<th>起動上限秒数</th>\n";
$html .="<td>".AUTO_MAX_TIME."</td>\n";
$html .="</tr>\n";

$html .="</table>\n";
$html .="</fieldset>\n";

$html .="<br>\n";

$html .="<fieldset>\n";
$html .="<legend>支援起動タイミング設定</legend>\n";

$html .="<table class = 'list'>\n";
$html .="<tr>\n";
$html .="<th style = 'width:40px;'>NO</th>\n";
$html .="<th style = 'width:100px;'>起動下限秒数</th>\n";
$html .="<th style = 'width:100px;'>起動上限秒数</th>\n";
$html .="<th style = 'width:100px;'>起動確率</th>\n";
$html .="<th style = 'width:100px;'>未使用フラグ</th>\n";
$html .="</tr>\n";
$j=17;
for($i=0;$j<count($param) ; $i++)
{
    $check="";
    $html .="<td>".($i+1)."</td>\n";
    $html .="<td><input type='TEXT' name='r_start[]' value='$param[$j]' istyle='4' size='7' maxlength='7' >秒</td>\n";
    if($param[$j]==0)
    {
        $check="checked";
    }
    $j++;
    $html .="<td><input type='TEXT' name='r_end[]' value='$param[$j]' istyle='4' size='7' maxlength='7' >秒</td>\n";
    $j++;
    $html .="<td><nobr><input type='TEXT' name='kakuritsu1[]' value='$param[$j]' istyle='4' size='9' maxlength='9'>";
    $j++;
    $html .="/<input type='TEXT' name='kakuritsu2[]' value='$param[$j]' istyle='4' size='9' maxlength='9'></nobr></td>\n";
    $html .="<td><input type='checkbox' name='r_flag[$i]' value='no' $check ></td>\n";
    $html .="</tr>\n";

    $j++;
}
$html .="<p>・未使用フラグをチェックすると未使用になります。</p>\n";
$html .="<p>・未使用フラグ全てチェックすると支援は動作しません。</p>\n";


$html .="</table>\n";
$html .="</fieldset>\n";
$html .="<input type='hidden' value='$i' name='r_param_num'>\n";
$html .="<input type='hidden' value='1' name='exec_num'>\n";
$html .="<input type='hidden' name='check' value='1' >\n";
$html .="<div style = 'text-align:center; margin-top:5px;'>";
$html .="<input type='submit' class = 'button' value='更新処理'>";
$html .="</div>";
$html .="</form>";
$html .="</body>\n";
$html .="</html>\n";

print($html);
?>

