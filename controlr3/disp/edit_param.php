<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/exec_update.php';

if(empty($_GET['id']))
{
    $html="<P>不正アクセスです</P>";
    printf($html);
    return;
}

$rs=GetRobProdSession($_GET['id']);
$row=mysql_num_rows($rs);

if($row!=1)
{
    $html ="<P>不正なデータが混ざっています。問い合わせをお願いします</P>\n";
    print($html);
    return;
}
$ret=mysql_fetch_array($rs);
$html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
$html .="<head>\n";
$html .="<title>".$_GET['id']."（達成率編集）</title>\n";

$html .="<style TYPE='text/css'>\n";
$html .="body{\n";
$html .="background-color:#FFFFFF;\n";
$html .="font-size:9pt;\n";
$html .="height:600;\n";
$html .="width:500;\n";
$html .="font-family:'メイリオ',Meiryo,'MS P Gothic','ＭＳ Ｐゴシック',Verdana,Arial,sans-serif;\n";
$html .="}\n";

$html .="table{\n";
$html .="font-size:9pt;\n";
$html .="}\n";

$html .="table.list{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="}\n";

$html .="table.list th{\n";
$html .="text-align:center;\n";
$html .="color:#555;\n";
$html .="background-color:#D3D3D3;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="height:25px;\n";
$html .="}\n";

$html .="table.list td{\n";
$html .="text-align:center;\n";
$html .="border: solid 1px #999999;\n";
$html .="border-collapse:collapse;\n";
$html .="height:20px;\n";
$html .="}\n";

$html .=".button{\n";
$html .="background-color:#828282;\n";
$html .="color:#FFFFFF;\n";
$html .="border-color:#FFFAFA;\n";
$html .="margin-top:10px;\n";
$html .="text-align: center;\n";
$html .="}\n";

$html .="</style>\n";

$html .="</head>\n";
$html .="<body>\n";

$html .="<fieldset>\n";
$html .="<legend>".$_GET['id']."（達成率編集）</legend>\n";

$html .="<P>達成率修正(0.01～999.99%)で指定してください<br>\n";
$html .="小数点第三以下は四捨五入されます</p><br>\n";
$html .="<form action='edit_param_result.php' method='POST'>\n";
$html .="<table class = 'list'>\n";
$html .="<tr>\n";
$r =$ret['f_r']/100;
$p =$ret['f_p']/100;
$html .="<th style = 'width:70px;'>対支達成率</th>\n";
$html .="<td style = 'width:100px;'><input type='text' istyle='4' value='$r' name='r_battle' size='7'>&nbsp;%</td>\n";
$html .="</tr>\n";
$html .="<tr>\n";
$html .="<th style = 'width:70px;'>対人達成率</th>\n";
$html .="<td style = 'width:100px;'><input type='text' istyle='4' value='".$p."' name='p_battle' size='7'>&nbsp;%</td>\n";
$html .="</tr>\n";
$html .="</table>\n";
$html .="</fieldset>\n";

$html .="<div style = 'text-align:center;'>\n";
$html .="<input type='hidden' name='id' value='".$_GET['id']."' >\n";
$html .="<input type='submit' class = 'button' style = 'width: 120px;' value='更新'>\n";
$html .="<input type='button' class = 'button' style = 'width: 120px;' onClick='window.close()' value='閉じる'>\n";
$html .="</form>\n";
$html .="</div>\n";

$html.= "</body></html>\n";
echo $html;
?>
