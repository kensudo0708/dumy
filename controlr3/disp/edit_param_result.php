<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '../control/DB_connect.php';
include '../control/exec_select.php';
include '../control/exec_update.php';
$html ="<html lang='utf-8'><head><META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'></head><body>";
if(empty($_POST['id']) || empty($_POST['r_battle']) || empty($_POST['p_battle']))
{
    $html.="<P>不正アクセスです</P>";
    printf($html);
    return;
}
$r_battle=doubleval($_POST['r_battle']);
$p_battle=doubleval($_POST['p_battle']);
if($r_battle > 999.99 || $p_battle >999.99)
{
    $html .= "達成率の上限は対人・対支ともに999.99です。";
    $html .= "<input type=\"button\" style=\"background-color:#FFFFFF; color:#0000FF; font-size:9pt; width:100; border:1 solid #A4FFA4;\" value=\"閉じる\" onClick=\"window.close(); return false;\">";
    $html.= "</body></html>\n";
    printf($html);
    return;
}
if($r_battle< 0.01 || $p_battle <0.01)
{
    $html .= "達成率の下限は対人・対支ともに0.01です。";
    $html .= "<input type=\"button\" style=\"background-color:#FFFFFF; color:#0000FF; font-size:9pt; width:100; border:1 solid #A4FFA4;\" value=\"閉じる\" onClick=\"window.close(); return false;\">";
    $html.= "</body></html>\n";
    printf($html);
    return;
}

$r_battle=$r_battle*100;
$p_battle=$p_battle*100;
$j=0;

UpdateSession($p_battle,$r_battle,$_POST['id']);
$html ="<html lang='utf-8'>\n";
$html .="<head>\n";
$html .="<META HTTP-EQUIV='Content-type' CONTENT='text/html; charset=UTF-8'>\n";
$html .="<title>達成率編集</title>";
$html .="<style type='text/css'>\n";
$html .="body{\n";
$html .="background-color:#FFFFFF;\n";
$html .="font-size:9pt;\n";
$html .="height:600;\n";
$html .="width:500;\n";
$html .="font-family:'メイリオ',Meiryo,'MS P Gothic','ＭＳ Ｐゴシック',Verdana,Arial,sans-serif;\n";
$html .="}\n";
$html .=".button{\n";
$html .="background-color:#828282;\n";
$html .="color:#FFFFFF;\n";
$html .="border-color:#FFFAFA;\n";
$html .="margin-top:10px;\n";
$html .="text-align: center;\n";
$html .="}\n";
$html .="</style>\n";

$html .="</head>\n";
$html .="<body>\n";
$html .="<p>更新完了しました<p><br>\n";
$html .= "<input type=\"button\" class = 'button' style='width:120;' value=\"閉じる\" onClick=\"window.close(); return false;\">";
$html.= "</body></html>\n";
printf($html);
?>
