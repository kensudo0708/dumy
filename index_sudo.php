<?php
$bench=false;
if($bench){
  $s=microtime(true);
}

ob_start();
define('FRAMEWORK_DIR', 'mvc');
require(FRAMEWORK_DIR."/load.php");
ActionService::init();
ActionService::service();
ob_end_flush();

if($bench){
  $e=microtime(true);
  error_log('bench:'.($e-$s).' '.$_SERVER['REQUEST_URI'].' '.$_SERVER['HTTP_USER_AGENT']);
}
?>
