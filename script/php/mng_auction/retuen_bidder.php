<?php
include '/home/auction/script/php/delete_date/common.php';
//自動入札設定のコイン返却処理
function WriteLog($str)
{
    $file2="/home/auction/control/data/kari/kari_del.log.".date("Ymd").".csv";
    //echo $str;
    $fp=fopen($file2,"a+");
    $str2=mb_convert_encoding($str, "SJIS", "UTF-8");
    fputs($fp, $str2, strlen($str2));
    fclose($fp);
    return;
}
function WriteLog2($str)
{
    $file="/home/auction/script/command/log/return_batch.log";
    //echo $str;
    $str =date("H:i:s").":".$str;
    $fp=fopen($file,"a+");
    fputs($fp, $str, strlen($str));
    fclose($fp);
    return;
}

//ウォッチリストの消去
function close_watch()
{
    write_log(SQL_LOG,"ウォッチリストの消去開始");
    $sql="DELETE FROM auction.t_watch_list WHERE  EXISTS (SELECT p.fk_products_id FROM auction.t_products2 p WHERE p.f_status = 2 or f_delete_flag <> 0 )";
    
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        print($sql."\n");
        write_log(SQL_LOG, $sql);
        write_log(SQL_LOG,"ウォッチリストの消去に失敗しました");
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"ウォッチリストの消去終了");
}

function cancel_prod($type)
{
    write_log(SQL_LOG,"商品キャンセル開始");

    if($type==0)
    {
	//ロボット
	$day = CANCEL_TIME;
    }
    else
    {
	$day = PRODUCT_PAYLIMIT_DAY;
    }

    $sql ="SELECT fk_products_id,fk_member_id from auction.t_products2 where f_status = 2 and f_end_status = 0 and f_delete_flag =0 and f_end_time < date_sub(now(),interval $day day)";

    $db=DB_CONNECT();
    $result = mysql_query($sql,$db);
    DB_CLOSE($db);
    $num = mysql_num_rows($result);

    for($i=0;$i<$num;$i++)
    {
	$ret=mysql_fetch_array($result);
	$mid=$ret['fk_member_id'];
	$pid=$ret['fk_products_id'];
	$sql="select f_activity from auction.t_member2 where fk_member_id=$mid";
	//echo "$sql\n";
	$db=DB_CONNECT();
        $result2 = mysql_query($sql,$db);
        DB_CLOSE($db);

	$ret2=mysql_fetch_array($result2);

	if($type==0)
	{
		if($ret2['f_activity'] >=90 )
		{
			$sql="update auction.t_products2 set f_end_status =1 where fk_products_id=$pid";
			$db=DB_CONNECT();
			mysql_query($sql,$db);
			DB_CLOSE($db);
		}
	}
	else 
	{
		if($ret2['f_activity'] <=10 )
		{
			$sql="update auction.t_products2 set f_end_status =1 where fk_products_id=$pid";
			$db=DB_CONNECT();
                        mysql_query($sql,$db);
                        DB_CLOSE($db);
		}
	}
	
    }
    write_log(SQL_LOG,"商品キャンセルの消去終了");
}
function del_req()
{
    write_log(SQL_LOG,"リクエスト削除");
    $sql=" delete from auction.t_reqhist where DATE_ADD(f_updatetm,interval f_timelimit second ) < now()";
    $db=DB_CONNECT();
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        write_log(SQL_LOG,"リクエスト削除に失敗しました");
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"リクエスト削除の消去終了");
}
function del_kari()
{
    $dat=date('YmdHis');

    $sql="SELECT m.fk_member_id,m.f_login_id,m.f_login_pass,m.f_handle,m.f_ser_no,m.f_user_agent,m.f_name,m.f_tel_no,m.f_sex,m.f_birthday,m.f_ip,m.f_mail_address_pc,m.f_mail_address_mb,ad.f_adcode,ar.f_name area_name,job.f_name job_name,m.f_regist_pos,m.f_regist_date,m.fk_adcode_id
        FROM (SELECT fk_member_id,f_login_id,f_login_pass,f_handle,f_ser_no,f_user_agent,f_name,f_tel_no,f_sex,f_birthday,f_ip,f_mail_address_pc,f_mail_address_mb,f_regist_pos,f_regist_date,fk_adcode_id,fk_area_id,fk_job_id FROM auction.t_member2 WHERE f_activity = 0 AND f_regist_date < DATE_SUB($dat , INTERVAL ". TEMP_REGIST_LIMIT ." SECOND)) m,
        (SELECT fk_adcode_id ,f_adcode FROM auction.t_adcode WHERE f_type =0 ) ad,auction.t_member_area ar,auction.t_member_job job
        WHERE m.fk_adcode_id = ad.fk_adcode_id AND ar.fk_area_id = m.fk_area_id AND job.fk_job_id = m.fk_job_id";
    //echo $sql;

    $db=DB_CONNECT();
    $rs_main=mysql_query($sql,$db);

    $num_main = mysql_num_rows($rs_main);
    DB_CLOSE($db);

    for($i=0;$i<$num_main;$i++)
    {
        $ret=mysql_fetch_array($rs_main);
        $id=$ret['fk_member_id'];
        $logid =$ret['f_login_id'];
        $logposs=$ret['f_login_pass'];
        $handle=$ret['f_handle'];
        $ser=$ret['f_ser_no'];
        $ua=$ret['f_user_agent'];
        $mem_name =$ret['f_name'];
        $tel=$ret['f_tel_no'];
        $sex=$ret['f_sex'];
        $birth=$ret['f_birthday'];
        $ip=$ret['f_ip'];
        
        $mail="";
        if($ret['f_mail_address_pc'] !="")
        {
            $mail = $ret['f_mail_address_pc'];
        }
        else if ($ret['f_mail_address_mb'] !="")
        {
            $mail = $ret['f_mail_address_mb'];
        }

        $par=$ret['f_adcode'];
	if($ret['f_regist_pos']==0)
	{
        	$pos="PC";
	}
	else
	{
		 $pos="MB";
	}
        $reg=$ret['f_regist_date'];
        $area=$ret['area_name'];
        $job=$ret['job_name'];
        $str="$id,$logid,$logposs,$handle,$ser,$ua,$mem_name,$tel,$sex,$birth,$ip,$mail,$par,$pos,$reg,$area,$job";
        $str .="\n";
        WriteLog($str);

        $db=DB_CONNECT();
        $sql="DELETE FROM auction.t_adcode where t_adcode.fk_adcode_id =".$ret['fk_adcode_id'];
        //echo $sql;
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"リクエスト削除に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        $sql="delete from auction.t_member2 where fk_member_id =$id";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"リクエスト削除に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
    }
}
function clear()
{
    $sql="SELECT m.f_temp1 FROM (SELECT DISTINCT f_temp1 FROM auction.t_member2 WHERE f_temp1 > 0) m,(SELECT fk_products_id FROM auction.t_products2 WHERE f_status = 2 OR f_delete_flag > 0) p WHERE m.f_temp1 = p.fk_products_id";
    $db=DB_CONNECT();

    $result = mysql_query($sql,$db);
    $num=mysql_num_rows($result);

    for($i=0;$i<$num;$i++)
    {
	$ret=mysql_fetch_array($result);
	$prod=$ret['f_temp1'];

	$sql="update auction.t_member2 set f_temp1 = 0 where f_temp1 = $prod ";
	mysql_query("BEGIN",$db);
	if(mysql_query($sql,$db)==FALSE)
	{
	    write_log(SQL_LOG, $sql);
	    write_log(SQL_LOG,"リクエスト削除に失敗しました");
	    mysql_query("ROLLBACK",$db);
	    DB_CLOSE($db);
	    return;
	}
	mysql_query("COMMIT",$db);
    }
    DB_CLOSE($db);
}

function reset_prod()
{
        $sql ="update auction.t_products2 set f_status=1 where f_status=7";

        $db=DB_CONNECT();
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
}

    close_watch();
    cancel_prod(0);
    cancel_prod(1);
    del_req();
    del_kari();
    reset_prod();
    clear();
?>
