<?PHP

include '/home/auction/script/php/mng_auction/common.php';

function close_auto()
{
    write_log(PROC_LOG,"自動入札での預かりコインの返却を開始します");
    $db=DB_CONNECT();
    //返却対象コインを取得
	$sql="select ab.fk_auto_id,ab.fk_member_id,ab.f_free_coin,ab.f_buy_coin,ab.fk_products_id ,ab.f_status,auc.f_spend_coin 
	FROM
	(select fk_auto_id , fk_member_id ,f_free_coin,f_buy_coin,fk_products_id,f_status from auction.t_auto_bidder where f_status =0 or f_status =1) ab,
	(select fk_products_id,fk_auction_type_id from auction.t_products2 where f_status = 2 or f_delete_flag <> 0 ) p,auction.t_auction_type auc
	where ab.fk_products_id = p.fk_products_id and p.fk_auction_type_id = auc.fk_auction_type_id ";
	
    //echo "$sql\n";
    //write_log(SQL_LOG, $sql);
    $rs=mysql_query($sql,$db);
    $row=mysql_num_rows($rs);

    for($i=0;$i<$row;$i++)
    {
        $ret=mysql_fetch_array($rs);

        $sql = "select f_coin ,f_free_coin from auction.t_member2 where fk_member_id =".$ret['fk_member_id'];
        //echo $sql."\n";
        $rs2=mysql_query($sql,$db);
        if(mysql_num_rows($rs2)==0)
        {
            continue;
        }
        $ret2=mysql_fetch_array($rs2);

        $ret_f_coin = $ret['f_free_coin'] ;
        $ret_coin=$ret['f_buy_coin'] ;
        WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚数(free):$ret_f_coin 返却枚数(buy):$ret_coin \n");
        
        $f_coins=$ret_f_coin+$ret2['f_free_coin'];
        $coins=$ret_coin+$ret2['f_coin'];
        
        if($ret['f_spend_coin'] > 0)
        {
            WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚後数(free) :".$f_coins." 返却後枚数(buy) :".$coins."\n");

        }
        else
        {
            WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚後数(free) :".$f_coins." 返却後枚数(buy) :".$coins." フリー\n");
        }
        
        $sql  = "UPDATE auction.t_member2 set f_free_coin=f_free_coin+$ret_f_coin,f_coin = f_coin+$ret_coin ";
        $sql .= "WHERE fk_member_id=".$ret['fk_member_id'];

        //echo $sql."\n";
        
        
        mysql_query("BEGIN",$db);
        if($ret['f_spend_coin'] > 0)
        {
            if(mysql_query($sql,$db)==FALSE)
            {
                write_log(SQL_LOG, $sql);
                write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(返却処理失敗)");
                mysql_query("ROLLBACK",$db);
                DB_CLOSE($db);
                return;
            }
        }
        $sql ="UPDATE auction.t_auto_bidder  set f_status = 9 WHERE fk_auto_id=".$ret['fk_auto_id'];
        //echo $sql."\n";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(auto_bidder削除処理失敗)");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"自動入札での預かりコインの返却を終了します");
}

function close_auto2()
{
    write_log(PROC_LOG,"自動入札での預かりコインの返却を開始します");
    $db=DB_CONNECT();
    //返却対象コインを取得
    $sql="select ab.fk_auto_id,ab.fk_member_id,ab.f_free_coin,ab.f_buy_coin,ab.fk_products_id ,ab.f_status,auc.f_spend_coin
        FROM
        (select fk_auto_id , fk_member_id ,f_free_coin,f_buy_coin,fk_products_id,f_status from auction.t_auto_bidder where f_status =2) ab,
        (select fk_products_id,fk_auction_type_id from auction.t_products2 ) p,auction.t_auction_type auc
        where ab.fk_products_id = p.fk_products_id and p.fk_auction_type_id = auc.fk_auction_type_id ";

    //echo $sql;
    //write_log(SQL_LOG, $sql);
    $rs=mysql_query($sql,$db);
    $row=mysql_num_rows($rs);

    for($i=0;$i<$row;$i++)
    {
        $ret=mysql_fetch_array($rs);

        $sql = "select f_coin ,f_free_coin from auction.t_member2 where fk_member_id =".$ret['fk_member_id'];
        //echo $sql."\n";
        $rs2=mysql_query($sql,$db);
        if(mysql_num_rows($rs2)==0)
        {
            continue;
        }
        $ret2=mysql_fetch_array($rs2);

        $ret_f_coin = $ret['f_free_coin'] ;
        $ret_coin=$ret['f_buy_coin'] ;
        
        WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚数(free):$ret_f_coin 返却枚数(buy):$ret_coin キャンセル\n");

        $f_coins=$ret_f_coin+$ret2['f_free_coin'];
        $coins=$ret_coin+$ret2['f_coin'];
        $flag =0;
        if($ret['f_spend_coin'] > 0)
        {
            WriteLog2("キャンセル 返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚後数(free) :".$f_coins." 返却後枚数(buy) :".$coins."\n");
            $flag = 1;
        }
        else
        {
            WriteLog2("キャンセル 返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚後数(free) :".$f_coins." 返却後枚数(buy) :".$coins." フリー\n");
        }
        $sql  = "UPDATE auction.t_member2 set f_free_coin=f_free_coin+$ret_f_coin,f_coin = f_coin+$ret_coin ";
        $sql .= "WHERE fk_member_id=".$ret['fk_member_id'];

//        echo $sql."\n";
        mysql_query("BEGIN",$db);

        if($flag == 1)
        {
            if(mysql_query($sql,$db)==FALSE)
            {
                write_log(SQL_LOG, $sql);
                write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(返却処理失敗)");
                mysql_query("ROLLBACK",$db);
                DB_CLOSE($db);
                return;
            }
        }
        $sql ="UPDATE auction.t_auto_bidder  set f_status = 9 WHERE fk_auto_id=".$ret['fk_auto_id'];
        //echo $sql."\n";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(auto_bidder削除処理失敗)");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
 //       echo "aaaa";
        mysql_query("COMMIT",$db);
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"自動入札での預かりコインの返却を終了します");
}

while(1)
{
    close_auto();
    sleep(1);
    close_auto2();
    sleep(5);
}
?>

