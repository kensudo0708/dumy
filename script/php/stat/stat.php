<?php
//時間毎の集計処理
include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';
class COMMON
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function GetStartDate($str)
    {
        $sql="SELECT f_bidst_start_dt FROM auction.t_tempvalue WHERE f_bidst_start_dt < date_sub('$str' , interval 1 hour)";
        return $sql;
    }

    function GetDateDiff2($date)
    {
        $sql ="select datediff(f_bidst_start_dt,'$date') as days from auction.t_tempvalue";
        //echo $sql;
        return $sql;
    }

    function GetLastDate()
    {
        $sql="SELECT f_laststat_dt FROM auction.t_tempvalue";
        return $sql;
    }
    function SetLastDate($str)
    {
        $sql="UPDATE auction.t_tempvalue set f_laststat_dt='$str'";
        return $sql;
    }
    function SetStartDate($str)
    {
        $sql="UPDATE auction.t_tempvalue set f_bidst_start_dt='$str' ";
        return $sql;
    }
    function SetStartDate2($str)
    {
        $sql="UPDATE auction.t_tempvalue set f_bidst_start_dt=$str ";
        return $sql;
    }
    function GetHourNum($str)
    {
        $sql="SELECT DISTINCT DATE_FORMAT(f_tm_stamp ,'%Y-%m-%d %H:00:00') as dat FROM auction.t_bid_log where f_tm_stamp > '$str'";
        return $sql;
    }

    function GetTimeDiff($str1,$str2)
    {
        $sql ="select timediff('$str1','$str2') as hours";
        return $sql;
    }

    function GetDateDiff($str1,$str2)
    {
        $sql ="select datediff($str1,$str2) as dates";
        return $sql;
    }

    function GetTimes($str,$i)
    {
        $sql ="SELECT DATE_FORMAT(DATE_ADD('$str',INTERVAL $i HOUR) ,'%Y%m%d') AS days ,DATE_FORMAT(DATE_ADD('$str',INTERVAL $i HOUR),'%k') AS hours";
        return $sql;
    }
}
function WriteLog($date)
{
    $file="/home/auction/script/command/log/time.log";
    $str = "$date\n";
    //echo $str;
    $fp=fopen($file,"a+");
    fputs($fp, $str, strlen($str));
    fclose($fp);
    return;
}

function stat_coin_command($date)
{
    $COMMON =   new COMMON();
    if($date =="")
    {
        return 1;
    }
    WriteLog($date);
    //開始時間判定
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->GetStartDate($date),$db);
    $COMMON->db_close($db);

    if(mysql_num_rows($result) == 0)
    {
        //実施時間が前回から一時間たっていない
        $db=$COMMON->db_connect();
        $result=mysql_query($COMMON->GetDateDiff2($date),$db);
        $COMMON->db_close($db);

        $ret = mysql_fetch_array($result);
        if($date =="")
        {
            return 1;
        }
        if(!($ret['days'] < 2 && $ret['days'] > -2 ))
        {
            ini_set("mbstring.internal_encoding","UTF8");
            mb_language("ja");
            mb_internal_encoding("UTF8");

            $subject ="集計処理エラー:".SITE_NAME;
            $from = INFO_MAIL;
            $body = "集計処理でエラーが発生しました";
            $to = "auction@ark-com.jp";
            //$to = "shoji@ark-com.jp";
            $header = "From: $from\n";
            $header .= "Reply-To: $from\n";

            mb_send_mail($to, $subject,$body, $header);
        }
        return 1;
    }
    
    $ret=mysql_fetch_array($result);
    $f_bidst_start_dt = $ret['f_bidst_start_dt'];
    
    //開始時間更新
    $db=$COMMON->db_connect();
    mysql_query("BEGIN",$db);
    $result=mysql_query($COMMON->SetStartDate($date),$db);
    if($result == false)
    {
        mysql_query("ROLLBACK",$db);
        $COMMON->db_close($db);
        return 1;
    }
    
    mysql_query("COMMIT",$db);
    $COMMON->db_close($db);

    //最終集計終了時刻取得
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->GetLastDate(),$db);
    if($result == false)
    {
        $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
        $COMMON->db_close($db);
        return 1;
    }
    $COMMON->db_close($db);
    $ret=mysql_fetch_array($result);
    $f_laststat_dt=$ret['f_laststat_dt'];

    //時間差分取得
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->GetTimeDiff($date,$f_laststat_dt),$db);
    if($result == false)
    {
        $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
        $COMMON->db_close($db);
        return 1;
    }
    $COMMON->db_close($db);
    $ret=mysql_fetch_array($result);
    $time_diff=$ret['hours'];

    $count=$time_diff[0]*10+$time_diff[1];
    for($i=0;$i<$count+1 ;$i++)
    {
        $db=$COMMON->db_connect();
        $result=mysql_query($COMMON->GetTimes($f_laststat_dt,$i),$db);
        if($result == false)
        {
            $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
            $COMMON->db_close($db);
            return 1;
        }
        $COMMON->db_close($db);
        $ret=mysql_fetch_array($result);

        $days=$ret['days'];
        $hours=$ret['hours'];
        $command = "/home/auction/script/command/day_cron/cron_batch $days $hours 0";
	`$command`;
        $retruen[$i][2] =`echo $?`;
        $retruen[$i][1] =$hours;
        $retruen[$i][0] =$days;
    }
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->SetLastDate($date),$db);
    if($result == false)
    {
        $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
        $COMMON->db_close($db);
        return 1;
    }
    $date_end="date_sub('$date',interval 1 hour)";
    //echo $COMMON->SetStartDate2($date_end);
    $result=mysql_query($COMMON->SetStartDate2($date_end),$db);
    if($result == false)
    {
        mysql_query("ROLLBACK",$db);
        $COMMON->db_close($db);
        return 1;
    }
    mysql_query("COMMIT",$db);
    $COMMON->db_close($db);
    
    for($i=0;$i<$count+1 ;$i++)
    {
        if($retruen[$i][2] == 0)
        {
            continue;
        }

        $return2=$retuen[$i][2];

        while($return2 !=0)
        {
            $day = $return[$i][0];
            $hour = $return[$i][1];
            switch($return2)
            {
                case 1:
                    $command = "/home/auction/script/command/day_cron/cron_batch $day $hour 1";
                    echo $command;
                    break;
                case 2:
                    $command = "/home/auction/script/command/day_cron/cron_batch $day $hour 2";
                    echo $command;
                    break;
                case 4:
                    $command = "/home/auction/script/command/day_cron/cron_batch $day $hour 3";
                    echo $command;
                    break;
            }
            $return2 =`$command`;
        }
    }
    return 0;
}

$ret=stat_coin_command($argv[1]);
if($ret==0)
{
	echo "修正処理が終了しました";
}
else
{
	echo "二重起動のため終了します。<BR>現在起動中の処理は10分ほどで終了しますのでお待ちください";
}
?>
