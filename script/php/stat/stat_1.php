<?php
//時間毎の集計処理
include '/home/auction/lib/define.php';

class STAT
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function GetStartDate($db,$str)
    {
        $sql="SELECT f_bidst_start_dt FROM auction.t_tempvalue WHERE f_bidst_start_dt < date_sub('$str' , interval 1 hour)";
        //printf($sql."\n");
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function GetLastDate($db)
    {
        $sql="SELECT f_laststat_dt FROM auction.t_tempvalue";
        //printf($sql."\n");
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function SetLastDate($db,$str)
    {
        $sql="UPDATE auction.t_tempvalue set f_laststat_dt=$str";
        //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        return $rs;
    }
    function SetStartDate($db,$str)
    {
        $sql="UPDATE auction.t_tempvalue set f_bidst_start_dt=$str ";
        //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function GetHourNum($db,$str)
    {
        $sql="SELECT DISTINCT DATE_FORMAT(f_tm_stamp ,'%Y-%m-%d %H:00:00') as dat FROM auction.t_bid_log where f_tm_stamp > '$str'";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function GetTimeDiff($db,$str1,$str2)
    {
        $sql ="select timediff($str1,$str2) as hours";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;
    }
    function GetDateDiff($db,$str1,$str2)
    {
        $sql ="select datediff($str1,$str2) as dates";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function InsStatSales($db,$i,$date)
    {
        $sql ="insert into auction.t_stat_sales (f_stat_dt,f_type) values($date,$i)";
        $rs=mysql_query($sql,$db);
        //echo $sql."\n";
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function GetBidData($db,$date,$coin_flag)
    {
        $sql="SELECT COUNT(*)*tat.f_spend_coin*1 as coin,tat.f_statistics_no
            FROM auction.t_auction_type tat,auction.t_bid_log bl,auction.t_products_master pm 
            WHERE bl.f_products_id = pm.fk_products_id AND pm.fk_auction_type_id = tat.fk_auction_type_id AND bl.f_coin_type=$coin_flag
            AND bl.f_tm_stamp BETWEEN $date GROUP BY tat.fk_auction_type_id ORDER BY tat.f_statistics_no";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;

    }

    function GetBidType($db,$date)
    {
        $sql="SELECT COUNT(bl.fk_bid_log_id) as count,tat.f_spend_coin ,bl.f_bid_type,pm.fk_auction_type_id
            FROM auction.t_auction_type tat,auction.t_bid_log bl,auction.t_products_master pm
            WHERE bl.f_products_id = pm.fk_products_id AND pm.fk_auction_type_id = tat.fk_auction_type_id
            AND bl.f_tm_stamp BETWEEN $date GROUP BY bl.f_bid_type,tat.f_spend_coin";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function GetFreeCoinGen($db,$date)
    {
        $total;
        $sql="SELECT SUM(p.f_coin_add) as coin,p.f_status FROM auction.t_pay_log p WHERE p.f_status=2 AND p.f_tm_stamp BETWEEN $date and p.f_cert_status=0 group by p.f_status";
        echo $sql;
        $rs=mysql_query($sql,$db);
        $ret=mysql_fetch_array($rs);
        $total=$ret['coin'];

        $sql="SELECT SUM(p.f_free_coin_add) as coin,p.f_status FROM auction.t_pay_log p WHERE p.f_status=0 AND p.f_tm_stamp BETWEEN $date and p.f_cert_status=0 group by p.f_status";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        $ret=mysql_fetch_array($rs);
        $total +=$ret['coin'];

        return $total;
    }

    function GetCoinGen($db,$date)
    {
        $sql="SELECT SUM(p.f_coin_add) as coin,p.f_status FROM auction.t_pay_log p WHERE p.f_status=0 AND p.f_tm_stamp BETWEEN $date and p.f_cert_status=0 group by p.f_status";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        $ret=mysql_fetch_array($rs);
        $total =$ret['coin'];

        return $total;
    }

    function SetHourData($db,$date,$flag,$f_coin_use,$f_scoin_use,$f_bid_hand,$f_bid_hand2,$f_bid_auto,$f_auctype,$f_sauctype,$f_total,$total)
    {
        $sql="UPDATE auction.t_stat_sales
            set f_coin_use = f_coin_use+$f_coin_use ,
            f_scoin_use = f_scoin_use+$f_scoin_use,
            f_bid_hand = f_bid_hand+$f_bid_hand,
            f_bid_hand2 = f_bid_hand2+$f_bid_hand2,
            f_bid_auto = f_bid_auto+$f_bid_auto,
            f_coin_gen = f_coin_gen + $total,
            f_scoin_gen = f_scoin_gen +$f_total,
            f_auctype1 = f_auctype1+$f_auctype[0],f_auctype2 = f_auctype2+$f_auctype[1],f_auctype3 = f_auctype3+$f_auctype[2],f_auctype4 = f_auctype4+$f_auctype[3],
            f_auctype5 = f_auctype5+$f_auctype[4],f_auctype6 = f_auctype6+$f_auctype[5],f_auctype7 = f_auctype7+$f_auctype[6],f_auctype8 = f_auctype8+$f_auctype[7],
            f_auctype9 = f_auctype9+$f_auctype[8],f_auctype10 = f_auctype10+$f_auctype[9],f_auctype11 = f_auctype11+$f_auctype[10],f_auctype12 = f_auctype12+$f_auctype[11],
            f_auctype13 = f_auctype13+$f_auctype[12],f_auctype14 = f_auctype14+$f_auctype[13],f_auctype15 = f_auctype15+$f_auctype[14],f_auctype16 = f_auctype16+$f_auctype[15],
            f_auctype17 = f_auctype17+$f_auctype[16],f_auctype18 = f_auctype18+$f_auctype[17],f_auctype19 = f_auctype19+$f_auctype[18],f_auctype20 = f_auctype20+$f_auctype[19],
        
            f_sauctype1 = f_sauctype1+$f_sauctype[0],f_sauctype2 = f_sauctype2+$f_sauctype[1],f_sauctype3 = f_sauctype3+$f_sauctype[2],f_sauctype4 = f_sauctype4+$f_sauctype[3],
            f_sauctype5 = f_sauctype5+$f_sauctype[4],f_sauctype6 = f_sauctype6+$f_sauctype[5],f_sauctype7 = f_sauctype7+$f_sauctype[6],f_sauctype8 = f_sauctype8+$f_sauctype[7],
            f_sauctype9 = f_sauctype9+$f_sauctype[8],f_sauctype10 = f_sauctype10+$f_sauctype[9],f_sauctype11 = f_sauctype11+$f_sauctype[10],f_sauctype12 = f_sauctype12+$f_sauctype[11],
            f_sauctype13 = f_sauctype13+$f_sauctype[12],f_sauctype14 = f_sauctype14+$f_sauctype[13],f_sauctype15 = f_sauctype15+$f_sauctype[14],f_sauctype16 = f_sauctype16+$f_sauctype[15],
            f_sauctype17 = f_sauctype17+$f_sauctype[16],f_sauctype18 = f_sauctype18+$f_sauctype[17],f_sauctype19 = f_sauctype19+$f_sauctype[18],f_sauctype20 = f_sauctype20+$f_sauctype[19]
            where f_type=$flag and f_stat_dt=$date";
            //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        //echo $sql."\n";
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function GetSumDeta($db,$flag,$start,$end)
    {
        $sql ="SELECT SUM(f_memreg) as sum1, SUM(f_buycoin)  as sum2, SUM(f_prd_lastprice) as sum3, SUM(f_prd_cost) as sum4,SUM(f_sale_total) as sum5,
            SUM(f_buycoin1) as sum6, SUM(f_buycoin2) as sum7, SUM(f_buycoin3) as sum8, SUM(f_buycoin4)  as sum9, SUM(f_buycoin5) as sum10,
            SUM(f_buycoin6) as sum11, SUM(f_buycoin7) as sum12, SUM(f_buycoin8) as sum13, SUM(f_buycoin9) as sum14, SUM(f_buycoin10) as sum15,
            SUM(f_buycoin11) as sum16, SUM(f_buycoin12) as sum17, SUM(f_buycoin13) as sum18, SUM(f_buycoin14) as sum19, SUM(f_buycoin15) as sum20,
            SUM(f_buycoin16) as sum21, SUM(f_buycoin17) as sum22, SUM(f_buycoin18) as sum23, SUM(f_buycoin19) as sum24, SUM(f_buycoin20) as sum25,
            SUM(f_coin_gen) as sum26, SUM(f_scoin_gen) as sum27, SUM(f_coin_use) as sum28, SUM(f_scoin_use) as sum29, SUM(f_bid_hand) as sum30, SUM(f_bid_hand2) as sum31, SUM(f_bid_auto) as sum32,
            SUM(f_auctype1) as sum33, SUM(f_auctype2) as sum34, SUM(f_auctype3) as sum35, SUM(f_auctype4) as sum36, SUM(f_auctype5) as sum37,
            SUM(f_auctype6) as sum38, SUM(f_auctype7) as sum39, SUM(f_auctype8) as sum40, SUM(f_auctype9) as sum41, SUM(f_auctype10) as sum42,
            SUM(f_auctype11) as sum43, SUM(f_auctype12) as sum44, SUM(f_auctype13) as sum45, SUM(f_auctype14) as sum46, SUM(f_auctype15) as sum47,
            SUM(f_auctype16) as sum48, SUM(f_auctype17) as sum49, SUM(f_auctype18) as sum50, SUM(f_auctype19) as sum51, SUM(f_auctype20) as sum52,
            SUM(f_sauctype1) as sum53, SUM(f_sauctype2) as sum54, SUM(f_sauctype3) as sum55, SUM(f_sauctype4) as sum56, SUM(f_sauctype5) as sum57,
            SUM(f_sauctype6) as sum58, SUM(f_sauctype7) as sum59, SUM(f_sauctype8) as sum60,SUM(f_sauctype9) as sum61, SUM(f_sauctype10) as sum62,
            SUM(f_sauctype11) as sum63, SUM(f_sauctype12) as sum64, SUM(f_sauctype13) as sum65, SUM(f_sauctype14) as sum66,SUM(f_sauctype15) as sum67,
            SUM(f_sauctype16) as sum68, SUM(f_sauctype17) as sum69, SUM(f_sauctype18) as sum70,SUM(f_sauctype19) as sum71, SUM(f_sauctype20) as sum72
            FROM auction.t_stat_sales WHERE f_type=$flag and f_stat_dt between $start and $end";

        $rs=mysql_query($sql,$db);

        return $rs;
    }
    function SetSumDate($db,$flag,$date,$sum)
    {
        $sql="UPDATE auction.t_stat_sales
            set f_memreg = $sum[0],f_buycoin = $sum[1],f_prd_lastprice = $sum[2],f_prd_cost = $sum[3],f_sale_total = $sum[4],
            f_buycoin1 = $sum[5],f_buycoin2 = $sum[6],f_buycoin3 = $sum[7],f_buycoin4 = $sum[8],f_buycoin5 = $sum[9],
            f_buycoin6 = $sum[10],f_buycoin7 = $sum[11],f_buycoin8 = $sum[12],f_buycoin9 = $sum[13],f_buycoin10 = $sum[14],
            f_buycoin11 = $sum[15],f_buycoin12 = $sum[16],f_buycoin13 = $sum[17],f_buycoin14 = $sum[18],f_buycoin15 = $sum[19],
            f_buycoin16 =$sum[20],f_buycoin17 = $sum[21],f_buycoin18 = $sum[22],f_buycoin19 = $sum[23],f_buycoin20 = $sum[24],
            f_coin_gen = $sum[25],f_scoin_gen = $sum[26],f_coin_use = $sum[27],f_scoin_use = $sum[28],f_bid_hand = $sum[29],f_bid_hand2 = $sum[30],f_bid_auto = $sum[31],
            f_auctype1 = $sum[32],f_auctype2 = $sum[33],f_auctype3 = $sum[34],f_auctype4 = $sum[35],f_auctype5 = $sum[36],
            f_auctype6 = $sum[37],f_auctype7 = $sum[38],f_auctype8 = $sum[39],f_auctype9 = $sum[40],f_auctype10 = $sum[41],
            f_auctype11 = $sum[42],f_auctype12 = $sum[43],f_auctype13 = $sum[44],f_auctype14 = $sum[45],f_auctype15 = $sum[46],
            f_auctype16 = $sum[47],f_auctype17 = $sum[48],f_auctype18 = $sum[49],f_auctype19 = $sum[50],f_auctype20 = $sum[51],
            f_sauctype1 = $sum[52],f_sauctype2 = $sum[53],f_sauctype3 = $sum[54],f_sauctype4 = $sum[55],f_sauctype5 = $sum[56],
            f_sauctype6 = $sum[57],f_sauctype7 = $sum[58],f_sauctype8 = $sum[59],f_sauctype9 = $sum[60],f_sauctype10 = $sum[61],
            f_sauctype11 = $sum[62],f_sauctype12 = $sum[63],f_sauctype13 = $sum[64],f_sauctype14 = $sum[65],f_sauctype15 = $sum[66],
            f_sauctype16 = $sum[67],f_sauctype17 = $sum[68],f_sauctype18 = $sum[69],f_sauctype19 = $sum[70],f_sauctype20 = $sum[71] 
            where f_type=$flag and f_stat_dt=$date";

        $rs=mysql_query($sql,$db);
        //echo $sql."\n";
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;

    }
}

class AD
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function GetAdmasterId($db)
    {
        $sql="select fk_admaster_id from auction.t_admaster";
        //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        return $rs;
    }
    function GetLastDate($db)
    {
        $sql="SELECT f_laststat_dt FROM auction.t_tempvalue";
        //printf($sql."\n");
        $rs=mysql_query($sql,$db);
        return $rs;
    }
    function GetTimeDiff($db,$str1,$str2)
    {
        $sql ="select timediff('$str1','$str2') as hours";
        //echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;
    }
    function CreateAdDate($db,$id,$str,$flag)
    {
        $sql="insert into auction.t_stat_ad (f_stat_dt,fk_admaster_id,f_type) values($str,$id,$flag)";
        //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        return $rs;
    }
    function GetAdDate($db,$id,$str,$flag)
    {
        $sql="SELECT SUM(f_pc_memreg) AS sum1 ,SUM(f_mb_memreg) AS sum2,SUM(f_pc_access) AS sum3,
            SUM(f_mb_access) AS sum4,SUM(f_pc_paycoin) AS sum5,SUM(f_mb_paycoin) AS sum6
            FROM auction.t_stat_ad
            WHERE f_type =$flag
            and fk_admaster_id = $id
            AND f_stat_dt BETWEEN $str";
        //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function SetAdDate($db,$id,$str,$flag,$f1,$f2,$f3,$f4,$f5,$f6)
    {
        $sql="update auction.t_stat_ad Set 
        f_pc_memreg = $f1 ,f_mb_memreg = $f2 ,
	f_pc_access = $f3 ,f_mb_access = $f4 ,
	f_pc_paycoin =$f5 ,f_mb_paycoin = $f6
	where f_stat_dt = $str and fk_admaster_id = $id and f_type = $flag ;";
        $rs=mysql_query($sql,$db);
        //echo $sql."\n";
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

}
function a_stat_ad($date)
{
    $ad = new AD();
    $db=$ad->db_connect();
    $date = trim($date);

    $rs=$ad->GetLastDate($db);
    $ret=mysql_fetch_array($rs);
    $last=$ret['f_laststat_dt'];

    $rs=$ad->GetTimeDiff($db, $date,$last);
    $ret=mysql_fetch_array($rs);
    $hour=$ret['hours'];
    //広告主ID取得
    $rs=$ad->GetAdmasterId($db);
    $ad_num=mysql_num_rows($rs);
    for($i=0;$i<$ad_num;$i++)
    {
        $ret=mysql_fetch_array($rs);
        $ad_id=$ret['fk_admaster_id'];

        for($j=0;$j<=$hour;$j++)
        {
            $str="date_format(date_add('$last',interval $j hour),'%Y-%m-%d %H:00:00')";
            $ad->CreateAdDate($db,$ad_id,$str,0);
        }
        $str="date_format('$date','%Y-%m-%d 23:00:00')";
        //空の時間別日別・月別フィールド作成(出来上がっていた場合は失敗する)
        $ad->CreateAdDate($db,$ad_id,$str,1);
        $str="date_format('$date','%Y-%m-01 23:00:00')";
        $ad->CreateAdDate($db,$ad_id,$str,2);

        //日別の合計作成
        $str="date_format('$date','%Y-%m-%d 00:00:00') and date_format('$date','%Y-%m-%d 23:59:59')";
        $rs_day=$ad->GetAdDate($db,$ad_id,$str,0);
        $ret_day=mysql_fetch_array($rs_day);
        $str="date_format('$date','%Y-%m-%d 23:00:00')";
        $f1= $ret_day['sum1']==""?0:$ret_day['sum1'];
        $f2= $ret_day['sum2']==""?0:$ret_day['sum2'];
        $f3= $ret_day['sum3']==""?0:$ret_day['sum3'];
        $f4= $ret_day['sum4']==""?0:$ret_day['sum4'];
        $f5= $ret_day['sum5']==""?0:$ret_day['sum5'];
        $f6= $ret_day['sum6']==""?0:$ret_day['sum6'];
        $ad->SetAdDate($db, $ad_id, $str, 1, $f1, $f2, $f3, $f4, $f5, $f6);

        //月別合計作成
        $str="date_format('$date','%Y-%m-01 00:00:00') and date_format(LAST_DAY('$date'),'%Y-%m-%d 23:59:59')";
        $rs_mon=$ad->GetAdDate($db,$ad_id,$str,1);
        $ret_mon=mysql_fetch_array($rs_mon);
        $str="date_format('$date','%Y-%m-01 23:00:00')";
        $f1= $ret_day['sum1']==""?0:$ret_day['sum1'];
        $f2= $ret_day['sum2']==""?0:$ret_day['sum2'];
        $f3= $ret_day['sum3']==""?0:$ret_day['sum3'];
        $f4= $ret_day['sum4']==""?0:$ret_day['sum4'];
        $f5= $ret_day['sum5']==""?0:$ret_day['sum5'];
        $f6= $ret_day['sum6']==""?0:$ret_day['sum6'];
        $ad->SetAdDate($db, $ad_id, $str, 2, $f1, $f2, $f3, $f4, $f5, $f6);
    }
}
function a_stat_coin_command($date)
{
    $stat=new STAT();

    $date = trim($date);
    $db=$stat->db_connect();

    //前回実行時間を取得
    $rs=$stat->GetStartDate($db,$date);
    $row=mysql_num_rows($rs);
    if($row !=1 )
    {
        //前回実施時間が一時間以内
        $db=$stat->db_close($db);
        return;
    }
 
    //時刻を現在に設定
    $str="date_format($date,'%Y-%m-%d %H:%i:%s')";
    $stat->SetStartDate($db,$str);

    //前回取得時刻を取得
    $rs=$stat->GetLastDate($db);
    $row=mysql_num_rows($rs);
    $ret=mysql_fetch_array($rs);
    $last_date=$ret['f_laststat_dt'];
    $last_date=trim($last_date);
    $str2="date_format('$last_date' ,'%Y%m%d%H%i%s')";

    $rs = $stat->GetTimeDiff($db,$date,$str2);
    $ret=mysql_fetch_array($rs);
    $hour=$ret['hours'];
    $count=$hour[0]*10+$hour[1];
    for($i=0;$i<$count+1 ;$i++)
    {
        //空の時間ごと集計結果を作成する
        $str="date_format(date_sub('$date',interval $i hour),'%Y-%m-%d %H:00:00')";
        $stat->InsStatSales($db,0,$str);
    }

    $f_coin_use=0;
    $f_scoin_use=0;
    $f_bid_hand=0;
    $f_bid_hand2=0;
    $f_bid_auto=0;
    $f_auctype=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    $f_sauctype=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    
    //前回からの経過時間数とデータの有無を確認

    if($count==0)
    {
        $str="'$last_date' and '$date' ";
        //課金コイン消費集計
        $rs=$stat->GetBidData($db,$str,1);
        $row=mysql_num_rows($rs);
        for($i=0;$i<$row;$i++)
        {
            $ret=mysql_fetch_array($rs);
            $tmp=$ret['f_statistics_no']-1;
            $f_auctype[$tmp]=$ret['coin'];
            $f_coin_use+=$ret['coin'];
        }

        //フリーコイン集計
        $rs=$stat->GetBidData($db,$str,0);
        $row=mysql_num_rows($rs);
        for($i=0;$i<$row;$i++)
        {
            $ret=mysql_fetch_array($rs);
            $tmp=$ret['f_statistics_no']-1;
            $f_sauctype[$tmp]=$ret['coin'];
            $f_scoin_use+=$ret['coin'];
        }

        $rs=$stat->GetBidType($db,$str);
        $row=mysql_num_rows($rs);
        for($i=0;$i<$row;$i++)
        {
            $ret=mysql_fetch_array($rs);
            switch ($ret['f_bid_type'])
            {
                case 0:
                    $f_bid_hand += $ret['count'] * $ret['f_spend_coin'];
                    break;
                case 1:
                    $f_bid_auto += $ret['count'] * $ret['f_spend_coin'];
                    break;
                case 2:
                    $f_bid_hand2 += $ret['count'] * $ret['f_spend_coin'];
                    break;
            }
        }

        $f_total = $stat->GetFreeCoinGen($db, $str);
        $total = $stat->GetCoinGen($db, $str);
        $str ="date_format('$date', '%Y-%m-%d %H:00:00')";
        $stat->SetHourData($db,$str,0,$f_coin_use,$f_scoin_use,$f_bid_hand,$f_bid_hand2,$f_bid_auto,$f_auctype,$f_sauctype,$f_total,$total);
    }
    else if($count>0)
    {
        for($i=0;$i<= $count;$i++)
        {
            $f_coin_use=0;
            $f_scoin_use=0;
            $f_bid_hand=0;
            $f_bid_hand2=0;
            $f_bid_auto=0;
            $f_auctype=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            $f_sauctype=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            if($i==0)
            {
                $str="'$last_date' and date_format('$last_date','%Y-%m-%d %H:59:59') ";
            }
            else
            {
                $str="date_format(date_add('$last_date',interval $i hour),'%Y-%m-%d %H:00:00')  and date_format(date_add('$last_date',interval $i hour),'%Y-%m-%d %H:59:59') ";
            }
            //課金ーコイン集計
            $rs=$stat->GetBidData($db,$str,1);
            $row=mysql_num_rows($rs);
            for($j=0;$j<$row;$j++)
            {
                $ret=mysql_fetch_array($rs);
                $tmp=$ret['f_statistics_no']-1;
                $f_auctype[$tmp]=$ret['coin'];
                $f_coin_use+=$ret['coin'];
            }

            //フリーコイン集計
            $rs=$stat->GetBidData($db,$str,0);
            $row=mysql_num_rows($rs);
            for($j=0;$j<$row;$j++)
            {
                $ret=mysql_fetch_array($rs);
                $tmp=$ret['f_statistics_no']-1;
                $f_sauctype[$tmp]=$ret['coin'];
                $f_scoin_use+=$ret['coin'];
            }

            $rs=$stat->GetBidType($db,$str);
            $row=mysql_num_rows($rs);
            for($j=0;$j<$row;$j++)
            {
                $ret=mysql_fetch_array($rs);
                switch ($ret['f_bid_type'])
                {
                    case 0:
                        $f_bid_hand += $ret['count'] * $ret['f_spend_coin'];
                        break;
                    case 1:
                        $f_bid_auto += $ret['count'] * $ret['f_spend_coin'];
                        break;
                    case 2:
                        $f_bid_hand2 += $ret['count'] * $ret['f_spend_coin'];
                        break;
                }
            }
            $f_total = $stat->GetFreeCoinGen($db, $str);
            $total = $stat->GetCoinGen($db, $str);
            $str="date_format(date_add('$last_date',interval $i hour),'%Y-%m-%d %H:00:00')";
            $stat->SetHourData($db,$str,0,$f_coin_use,$f_scoin_use,$f_bid_hand,$f_bid_hand2,$f_bid_auto,$f_auctype,$f_sauctype,$f_total,$total);
        }
    }
    $str2="date_format('$last_date' ,'%Y%m%d%H%i%s')";
    $rs = $stat->GetDateDiff($db,$date,$str2);
    $ret=mysql_fetch_array($rs);
    $dates=$ret['dates'];

    //空の月別・日別領域を作成する
    for($i=0;$i <= $dates;$i++)
    {
        $str="date_format(date_add('$last_date',interval $i day),'%Y-%m-%d 23:00:00')";
        $stat->InsStatSales($db,1,$str);

        $str1="date_format(date_add('$last_date',interval $i day),'%Y-%m-%d 00:00:00')";
        $str2="date_format(date_add('$last_date',interval $i day),'%Y-%m-%d 23:59:00')";
        $rs=$stat->GetSumDeta($db,0,$str1,$str2);
        $ret=mysql_fetch_array($rs);
        $sumdate=array($ret['sum1'],$ret['sum2'],$ret['sum3'],$ret['sum4'],$ret['sum5'],$ret['sum6'],$ret['sum7'],$ret['sum8'],
                        $ret['sum9'],$ret['sum10'],$ret['sum11'],$ret['sum12'],$ret['sum13'],$ret['sum14'],$ret['sum15'],$ret['sum16'],
                        $ret['sum17'],$ret['sum18'],$ret['sum19'],$ret['sum20'],$ret['sum21'],$ret['sum22'],$ret['sum23'],$ret['sum24'],
                        $ret['sum25'],$ret['sum26'],$ret['sum27'],$ret['sum28'],$ret['sum29'],$ret['sum30'],$ret['sum31'],$ret['sum32'],
                        $ret['sum33'],$ret['sum34'],$ret['sum35'],$ret['sum36'],$ret['sum37'],$ret['sum38'],$ret['sum39'],$ret['sum40'],
                        $ret['sum41'],$ret['sum42'],$ret['sum43'],$ret['sum44'],$ret['sum45'],$ret['sum46'],$ret['sum47'],$ret['sum48'],
                        $ret['sum49'],$ret['sum50'],$ret['sum51'],$ret['sum52'],$ret['sum53'],$ret['sum54'],$ret['sum55'],$ret['sum56'],
                        $ret['sum57'],$ret['sum58'],$ret['sum59'],$ret['sum60'],$ret['sum61'],$ret['sum62'],$ret['sum63'],$ret['sum64'],
                        $ret['sum65'],$ret['sum66'],$ret['sum67'],$ret['sum68'],$ret['sum69'],$ret['sum70'],$ret['sum71'],$ret['sum72'],);
        $str1="date_format(date_add('$last_date',interval $i day),'%Y-%m-%d 23:00:00')";
        $stat->SetSumDate($db,1,$str1,$sumdate);
    }
    $str="date_format('$last_date',interval $i day),'%Y-%m-01 23:00:00')";
    $stat->InsStatSales($db,2,$str);

    $str1="date_format('$last_date','%Y-%m-01 00:00:00')";
    $str2="date_format(LAST_DAY('$last_date'),'%Y-%m-%d 23:59:00')";
    $rs=$stat->GetSumDeta($db,1,$str1,$str2);
    $ret=mysql_fetch_array($rs);
    $sumdate=array($ret['sum1'],$ret['sum2'],$ret['sum3'],$ret['sum4'],$ret['sum5'],$ret['sum6'],$ret['sum7'],$ret['sum8'],
                    $ret['sum9'],$ret['sum10'],$ret['sum11'],$ret['sum12'],$ret['sum13'],$ret['sum14'],$ret['sum15'],$ret['sum16'],
                    $ret['sum17'],$ret['sum18'],$ret['sum19'],$ret['sum20'],$ret['sum21'],$ret['sum22'],$ret['sum23'],$ret['sum24'],
                    $ret['sum25'],$ret['sum26'],$ret['sum27'],$ret['sum28'],$ret['sum29'],$ret['sum30'],$ret['sum31'],$ret['sum32'],
                    $ret['sum33'],$ret['sum34'],$ret['sum35'],$ret['sum36'],$ret['sum37'],$ret['sum38'],$ret['sum39'],$ret['sum40'],
                    $ret['sum41'],$ret['sum42'],$ret['sum43'],$ret['sum44'],$ret['sum45'],$ret['sum46'],$ret['sum47'],$ret['sum48'],
                    $ret['sum49'],$ret['sum50'],$ret['sum51'],$ret['sum52'],$ret['sum53'],$ret['sum54'],$ret['sum55'],$ret['sum56'],
                    $ret['sum57'],$ret['sum58'],$ret['sum59'],$ret['sum60'],$ret['sum61'],$ret['sum62'],$ret['sum63'],$ret['sum64'],
                    $ret['sum65'],$ret['sum66'],$ret['sum67'],$ret['sum68'],$ret['sum69'],$ret['sum70'],$ret['sum71'],$ret['sum72'],);
    $str1="date_format('$last_date','%Y-%m-01 00:00:00')";
    $stat->SetSumDate($db,2,$str1,$sumdate);

    stat_ad($date);
    
    //最終取得時刻を設定
    $str="date_format($date,'%Y-%m-%d %H:%i:%s')";
    $stat->SetLastDate($db,$str);
    
    //開始時刻を修正
    $str="date_format(date_sub($date,interval 1 hour),'%Y-%m-%d %H:%i:%s')";
    $stat->SetStartDate($db,$str);
    $stat->db_close($db);
    
}
stat_coin_command($argv[1]);



?>
