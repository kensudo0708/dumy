<?php
//時間毎の集計処理
include '/home/auction/lib/define.php';

class COMMON
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function GetStartDate($str)
    {
        $sql="SELECT f_bidst_start_dt FROM auction.t_tempvalue WHERE f_bidst_start_dt < date_sub('$str' , interval 1 hour)";
        return $sql;
    }

    function GetDateDiff2($date)
    {
        $sql ="select datediff(f_bidst_start_dt,'$date') as days from auction.t_tempvalue";
        echo $sql;
        return $sql;
    }

    function GetLastDate()
    {
        $sql="SELECT f_laststat_dt FROM auction.t_tempvalue";
        return $sql;
    }
    function SetLastDate($str)
    {
        $sql="UPDATE auction.t_tempvalue set f_laststat_dt='$str'";
        return $sql;
    }
    function SetStartDate($str)
    {
        $sql="UPDATE auction.t_tempvalue set f_bidst_start_dt='$str' ";
        return $sql;
    }
    function SetStartDate2($str)
    {
        $sql="UPDATE auction.t_tempvalue set f_bidst_start_dt=$str ";
        return $sql;
    }
    function GetHourNum($str)
    {
        $sql="SELECT DISTINCT DATE_FORMAT(f_tm_stamp ,'%Y-%m-%d %H:00:00') as dat FROM auction.t_bid_log where f_tm_stamp > '$str'";
        return $sql;
    }

    function GetTimeDiff($str1,$str2)
    {
        $sql ="select timediff('$str1','$str2') as hours";
        return $sql;
    }

    function GetDateDiff($str1,$str2)
    {
        $sql ="select datediff($str1,$str2) as dates";
        return $sql;
    }
}

class SALSE
{
    function InsStatSales($i,$date)
    {
        $sql ="insert into auction.t_stat_sales (f_stat_dt,f_type) values($date,$i)";
        return $sql;
    }

    //コイン消費取得
    function GetBidData($date,$coin_flag)
    {
        $sql="SELECT COUNT(*)*tat.f_spend_coin*1 as spend,tat.f_statistics_no
              FROM auction.t_auction_type tat,auction.t_bid_log bl,auction.t_products_master pm
              WHERE bl.f_products_id = pm.fk_products_id AND pm.fk_auction_type_id = tat.fk_auction_type_id AND bl.f_coin_type=$coin_flag AND
              bl.f_tm_stamp BETWEEN $date GROUP BY tat.fk_auction_type_id ORDER BY tat.f_statistics_no";
        return $sql;
    }


    //入札種別別コイン消費枚数
    function GetBidType($date)
    {
        $sql="SELECT COUNT(bl.fk_bid_log_id) as count,tat.f_spend_coin ,bl.f_bid_type,pm.fk_auction_type_id
            FROM auction.t_auction_type tat,auction.t_bid_log bl,auction.t_products_master pm
            WHERE bl.f_products_id = pm.fk_products_id AND pm.fk_auction_type_id = tat.fk_auction_type_id
            AND bl.f_tm_stamp BETWEEN $date GROUP BY bl.f_bid_type,tat.f_spend_coin";
        return $sql;
    }

    //有料コイン発行枚数取得
    function GetCoinGen($date)
    {
        $sql="SELECT p.f_coin_add FROM auction.t_pay_log p WHERE (p.f_status=0 OR p.f_status=3)  AND p.f_cert_status=0 AND p.f_tm_stamp BETWEEN $date";
        return $sql;
    }

    function GetFreeCoinGen($date)
    {
        $sql="SELECT p.f_coin_add+p.f_free_coin_add as total FROM auction.t_pay_log p WHERE p.f_status=2 AND p.f_cert_status=0 and p.f_tm_stamp BETWEEN $date UNION ALL SELECT p.f_free_coin_add FROM auction.t_pay_log p WHERE (p.f_status=0 OR p.f_status=3) AND p.f_cert_status=0 and p.f_tm_stamp BETWEEN $date";
        return $sql;
    }

    function SetHourData($date,$flag,$f_coin_use,$f_scoin_use,$f_bid_hand,$f_bid_hand2,$f_bid_auto,$f_auctype,$f_sauctype,$f_total,$total)
    {
        $sql="UPDATE auction.t_stat_sales
            set f_coin_use = $f_coin_use ,
            f_scoin_use = $f_scoin_use,
            f_bid_hand = $f_bid_hand,
            f_bid_hand2 = $f_bid_hand2,
            f_bid_auto = $f_bid_auto,
            f_coin_gen = $total,
            f_scoin_gen = $f_total,
            f_auctype1 = $f_auctype[0],f_auctype2 = $f_auctype[1],f_auctype3 = $f_auctype[2],f_auctype4 = $f_auctype[3],
            f_auctype5 = $f_auctype[4],f_auctype6 = $f_auctype[5],f_auctype7 = $f_auctype[6],f_auctype8 = $f_auctype[7],
            f_auctype9 = $f_auctype[8],f_auctype10 = $f_auctype[9],f_auctype11 = $f_auctype[10],f_auctype12 = $f_auctype[11],
            f_auctype13 = $f_auctype[12],f_auctype14 = $f_auctype[13],f_auctype15 = $f_auctype[14],f_auctype16 = $f_auctype[15],
            f_auctype17 = $f_auctype[16],f_auctype18 = $f_auctype[17],f_auctype19 = $f_auctype[18],f_auctype20 = $f_auctype[19],

            f_sauctype1 = $f_sauctype[0],f_sauctype2 = $f_sauctype[1],f_sauctype3 = $f_sauctype[2],f_sauctype4 = $f_sauctype[3],
            f_sauctype5 = $f_sauctype[4],f_sauctype6 = $f_sauctype[5],f_sauctype7 = $f_sauctype[6],f_sauctype8 = $f_sauctype[7],
            f_sauctype9 = $f_sauctype[8],f_sauctype10 = $f_sauctype[9],f_sauctype11 = $f_sauctype[10],f_sauctype12 = $f_sauctype[11],
            f_sauctype13 = $f_sauctype[12],f_sauctype14 = $f_sauctype[13],f_sauctype15 = $f_sauctype[14],f_sauctype16 = $f_sauctype[15],
            f_sauctype17 = $f_sauctype[16],f_sauctype18 = $f_sauctype[17],f_sauctype19 = $f_sauctype[18],f_sauctype20 = $f_sauctype[19]
            where f_type=$flag and f_stat_dt=$date";
        return $sql;
    }
    
    function SetDayData($date,$flag,$f_coin_use,$f_scoin_use,$f_bid_hand,$f_bid_hand2,$f_bid_auto,$f_auctype,$f_sauctype,$f_total,$total,$last_price,$cost,$f_package_sales,$f_buycoin,$f_buycoin_total,$f_memreg)
    {
        $sql="UPDATE auction.t_stat_sales
            set f_coin_use = $f_coin_use ,
            f_prd_lastprice =$last_price,
            f_prd_cost = $cost,
            f_scoin_use = $f_scoin_use,
            f_bid_hand = $f_bid_hand,
            f_bid_hand2 = $f_bid_hand2,
            f_bid_auto = $f_bid_auto,
            f_coin_gen = $total,
            f_scoin_gen = $f_total,
            f_package_sales =$f_package_sales,
            f_buycoin = $f_buycoin_total ,
            f_memreg =$f_memreg,
            f_buycoin1 = $f_buycoin[0] ,f_buycoin2 = $f_buycoin[1] ,f_buycoin3 = $f_buycoin[2] ,f_buycoin4 = $f_buycoin[3] ,
            f_buycoin5 = $f_buycoin[4],f_buycoin6 = $f_buycoin[5] ,f_buycoin7 = $f_buycoin[6] ,f_buycoin8 = $f_buycoin[7] ,
            f_buycoin9 = $f_buycoin[8] ,f_buycoin10 = $f_buycoin[9] ,f_buycoin11 = $f_buycoin[10] ,f_buycoin12 = $f_buycoin[11] ,
            f_buycoin13 = $f_buycoin[12] ,f_buycoin14 = $f_buycoin[13] ,f_buycoin15 = $f_buycoin[14] ,f_buycoin16 = $f_buycoin[15] ,
            f_buycoin17 = $f_buycoin[16] ,f_buycoin18 = $f_buycoin[17] ,f_buycoin19 = $f_buycoin[18] ,f_buycoin20 = $f_buycoin[19] ,
            f_auctype1 = $f_auctype[0],f_auctype2 = $f_auctype[1],f_auctype3 = $f_auctype[2],f_auctype4 = $f_auctype[3],
            f_auctype5 = $f_auctype[4],f_auctype6 = $f_auctype[5],f_auctype7 = $f_auctype[6],f_auctype8 = $f_auctype[7],
            f_auctype9 = $f_auctype[8],f_auctype10 = $f_auctype[9],f_auctype11 = $f_auctype[10],f_auctype12 = $f_auctype[11],
            f_auctype13 = $f_auctype[12],f_auctype14 = $f_auctype[13],f_auctype15 = $f_auctype[14],f_auctype16 = $f_auctype[15],
            f_auctype17 = $f_auctype[16],f_auctype18 = $f_auctype[17],f_auctype19 = $f_auctype[18],f_auctype20 = $f_auctype[19],

            f_sauctype1 = $f_sauctype[0],f_sauctype2 = $f_sauctype[1],f_sauctype3 = $f_sauctype[2],f_sauctype4 = $f_sauctype[3],
            f_sauctype5 = $f_sauctype[4],f_sauctype6 = $f_sauctype[5],f_sauctype7 = $f_sauctype[6],f_sauctype8 = $f_sauctype[7],
            f_sauctype9 = $f_sauctype[8],f_sauctype10 = $f_sauctype[9],f_sauctype11 = $f_sauctype[10],f_sauctype12 = $f_sauctype[11],
            f_sauctype13 = $f_sauctype[12],f_sauctype14 = $f_sauctype[13],f_sauctype15 = $f_sauctype[14],f_sauctype16 = $f_sauctype[15],
            f_sauctype17 = $f_sauctype[16],f_sauctype18 = $f_sauctype[17],f_sauctype19 = $f_sauctype[18],f_sauctype20 = $f_sauctype[19]
            where f_type=$flag and f_stat_dt=$date";
        return $sql;
    }

    function GetSumDeta($flag,$start,$end)
    {
        $sql ="SELECT SUM(f_memreg) as sum1, SUM(f_buycoin)  as sum2, SUM(f_prd_lastprice) as sum3, SUM(f_prd_cost) as sum4,SUM(f_sale_total) as sum5,
            SUM(f_buycoin1) as sum6, SUM(f_buycoin2) as sum7, SUM(f_buycoin3) as sum8, SUM(f_buycoin4)  as sum9, SUM(f_buycoin5) as sum10,
            SUM(f_buycoin6) as sum11, SUM(f_buycoin7) as sum12, SUM(f_buycoin8) as sum13, SUM(f_buycoin9) as sum14, SUM(f_buycoin10) as sum15,
            SUM(f_buycoin11) as sum16, SUM(f_buycoin12) as sum17, SUM(f_buycoin13) as sum18, SUM(f_buycoin14) as sum19, SUM(f_buycoin15) as sum20,
            SUM(f_buycoin16) as sum21, SUM(f_buycoin17) as sum22, SUM(f_buycoin18) as sum23, SUM(f_buycoin19) as sum24, SUM(f_buycoin20) as sum25,
            SUM(f_coin_gen) as sum26, SUM(f_scoin_gen) as sum27, SUM(f_coin_use) as sum28, SUM(f_scoin_use) as sum29, SUM(f_bid_hand) as sum30, SUM(f_bid_hand2) as sum31, SUM(f_bid_auto) as sum32,
            SUM(f_auctype1) as sum33, SUM(f_auctype2) as sum34, SUM(f_auctype3) as sum35, SUM(f_auctype4) as sum36, SUM(f_auctype5) as sum37,
            SUM(f_auctype6) as sum38, SUM(f_auctype7) as sum39, SUM(f_auctype8) as sum40, SUM(f_auctype9) as sum41, SUM(f_auctype10) as sum42,
            SUM(f_auctype11) as sum43, SUM(f_auctype12) as sum44, SUM(f_auctype13) as sum45, SUM(f_auctype14) as sum46, SUM(f_auctype15) as sum47,
            SUM(f_auctype16) as sum48, SUM(f_auctype17) as sum49, SUM(f_auctype18) as sum50, SUM(f_auctype19) as sum51, SUM(f_auctype20) as sum52,
            SUM(f_sauctype1) as sum53, SUM(f_sauctype2) as sum54, SUM(f_sauctype3) as sum55, SUM(f_sauctype4) as sum56, SUM(f_sauctype5) as sum57,
            SUM(f_sauctype6) as sum58, SUM(f_sauctype7) as sum59, SUM(f_sauctype8) as sum60,SUM(f_sauctype9) as sum61, SUM(f_sauctype10) as sum62,
            SUM(f_sauctype11) as sum63, SUM(f_sauctype12) as sum64, SUM(f_sauctype13) as sum65, SUM(f_sauctype14) as sum66,SUM(f_sauctype15) as sum67,
            SUM(f_sauctype16) as sum68, SUM(f_sauctype17) as sum69, SUM(f_sauctype18) as sum70,SUM(f_sauctype19) as sum71, SUM(f_sauctype20) as sum72,sum(f_package_sales) as SUM72 
            FROM auction.t_stat_sales WHERE f_type=$flag and f_stat_dt between $start and $end";
        return $sql;
    }
}

class AD
{
    function GetAdmasterId()
    {
        $sql="select fk_admaster_id from auction.t_admaster order by fk_admaster_id";
        return $sql;
    }

    function CreateAdDate($id,$str,$flag)
    {
        $sql="insert into auction.t_stat_ad (f_stat_dt,fk_admaster_id,f_type) values($str,$id,$flag)";
        return $sql;
    }

    function GetAdDate($id,$str,$flag)
    {
        $sql="SELECT SUM(f_pc_memreg) AS sum1 ,SUM(f_mb_memreg) AS sum2,SUM(f_pc_access) AS sum3,
            SUM(f_mb_access) AS sum4,SUM(f_pc_paycoin) AS sum5,SUM(f_mb_paycoin) AS sum6
            FROM auction.t_stat_ad
            WHERE f_type =$flag
            and fk_admaster_id = $id
            AND f_stat_dt BETWEEN $str";
        return $sql;
    }
    function SetAdDate($id,$str,$flag,$f1,$f2,$f3,$f4,$f5,$f6)
    {
        $sql="update auction.t_stat_ad Set
        f_pc_memreg = $f1 ,f_mb_memreg = $f2 ,
	f_pc_access = $f3 ,f_mb_access = $f4 ,
	f_pc_paycoin =$f5 ,f_mb_paycoin = $f6
	where f_stat_dt = $str and fk_admaster_id = $id and f_type = $flag ;";
        return $sql;
    }
}
function WriteLog($date)
{
    $file="/home/auction/script/command/log/time.log";
    $str = "$date\n";
    //echo $str;
    $fp=fopen($file,"a+");
    fputs($fp, $str, strlen($str));
    fclose($fp);
    return;
}

function stat_coin_command($date)
{
    $COMMON =   new COMMON();
    $SALSE  =   new SALSE();
    $AD     =   new AD();
    if($date =="")
    {
        return;
    }
    WriteLog($date);
    //開始時間判定
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->GetStartDate($date),$db);
    $COMMON->db_close($db);

    if(mysql_num_rows($result) == 0)
    {
        //実施時間が前回から一時間たっていない
        $db=$COMMON->db_connect();
        $result=mysql_query($COMMON->GetDateDiff2($date),$db);
        $COMMON->db_close($db);

        $ret = mysql_fetch_array($result);
        if($date =="")
        {
            return;
        }
        if(!($ret['days'] < 2 && $ret['days'] > -2 ))
        {
            ini_set("mbstring.internal_encoding","UTF8");
            mb_language("ja");
            mb_internal_encoding("UTF8");

            $subject ="集計処理エラー:".SITE_NAME;
            $from = INFO_MAIL;
            $body = "集計処理でエラーが発生しました";
            $to = "auction@ark-com.jp";
            //$to = "shoji@ark-com.jp";
            $header = "From: $from\n";
            $header .= "Reply-To: $from\n";

            mb_send_mail($to, $subject,$body, $header);
        }


        return;
    }
    
    $ret=mysql_fetch_array($result);
    $f_bidst_start_dt = $ret['f_bidst_start_dt'];
    
    //開始時間更新
    $db=$COMMON->db_connect();
    mysql_query("BEGIN",$db);
    $result=mysql_query($COMMON->SetStartDate($date),$db);
    if($result == false)
    {
        mysql_query("ROLLBACK",$db);
        $COMMON->db_close($db);
        return 1;
    }
    
    mysql_query("COMMIT",$db);
    $COMMON->db_close($db);

    //最終集計終了時刻取得
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->GetLastDate(),$db);
    if($result == false)
    {
        $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
        $COMMON->db_close($db);
        return 1;
    }
    $COMMON->db_close($db);
    $ret=mysql_fetch_array($result);
    $f_laststat_dt=$ret['f_laststat_dt'];

    //時間差分取得
    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->GetTimeDiff($date,$f_laststat_dt),$db);
    if($result == false)
    {
        $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
        $COMMON->db_close($db);
        return 1;
    }
    $COMMON->db_close($db);
    $ret=mysql_fetch_array($result);
    $time_diff=$ret['hours'];

    $count=$time_diff[0]*10+$time_diff[1];

    //集計実施
    for($i=0;$i<$count+1 ;$i++)
    {
        $f_coin_use=0;
        $f_scoin_use=0;
        $f_bid_hand=0;
        $f_bid_hand2=0;
        $f_bid_auto=0;
        $f_auctype=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        $f_sauctype=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        $f_coin_gen=0;
        $f_free_coin_gen=0;
        
        //空の時間ごと集計結果を作成する
        $str_date="date_format(date_sub('$date',interval $i hour),'%Y-%m-%d %H:00:00') and date_format(date_sub('$date',interval $i hour),'%Y-%m-%d %H:59:59')";

        //コイン消費と発行の集計
        //課金コイン消費枚数総数取得
        $sql=$SALSE->GetBidData($str_date, 1);
        $db=$COMMON->db_connect();
        $result=mysql_query($sql,$db);
        $COMMON->db_close($db);
        if($result != false)
        {
            $num=mysql_num_rows($result);
            
            if($num != 0)
            {
                for($j=0;$j<$num;$j++)
                {
                    $ret=mysql_fetch_array($result);
                    $f_coin_use += $ret['spend'];
                    $f_auctype[$ret['f_statistics_no']] = $ret['spend'];
                }
            }
        }

        //フリーコイン消費枚数総数取得
        $sql=$SALSE->GetBidData($str_date, 0);
        $db=$COMMON->db_connect();
        $result=mysql_query($sql,$db);
        $COMMON->db_close($db);
        if( $result != false)
        {
            $num=mysql_num_rows($result);
            if($num != 0)
            {
                for($j=0;$j<$num;$j++)
                {
                    $ret=mysql_fetch_array($result);
                    $f_scoin_use += $ret['spend'];
                    $f_sauctype[$ret['f_statistics_no']] = $ret['spend'];
                }
            }
        }
        
        //入札種別ごとの使用枚数取得
        $sql = $SALSE->GetBidType($str_date);
        $db=$COMMON->db_connect();
        $result=mysql_query($sql,$db);
        $COMMON->db_close($db);
        if($result != false)
        {
            for($j=0;$j<$num;$j++)
            {
                $ret=mysql_fetch_array($result);
                switch($ret['f_bid_type'])
                {
                    case 0:
                        $f_bid_hand += $ret['f_bid_type'] * $ret['f_spend_coin'];
                        break;
                    case 1:
                        $f_bid_auto += $ret['f_bid_type'] * $ret['f_spend_coin'];
                        break;
                    case 2:
                        $f_bid_hand2 += $ret['f_bid_type'] * $ret['f_spend_coin'];
                        break;
                }
            }
        }

        //有料コイン発行枚数取得
        $sql = $SALSE->GetCoinGen($str_date);
        $db=$COMMON->db_connect();
        $result=mysql_query($sql,$db);
        $COMMON->db_close($db);

        if($result != false)
        {
            $num=mysql_num_rows($result);
            if($num != 0)
            {
                for($j=0;$j<$num;$j++)
                {
                    $ret = mysql_fetch_array($result);
                    $f_coin_gen += $ret['f_coin_add'];
                }
            }
        }

        //無料コイン発行枚数取得
        $sql = $SALSE->GetFreeCoinGen($str_date);
        $db=$COMMON->db_connect();
        $result=mysql_query($sql,$db);
        $COMMON->db_close($db);

        if($result != false)
        {
            $num=mysql_num_rows($result);
            if($num != 0)
            {
                for($j=0;$j<$num;$j++)
                {
                    $ret = mysql_fetch_array($result);
                    $f_free_coin_gen += $ret['total'];
                }
            }
        }

        //広告系データ取得
        //広告主取得
        $db=$COMMON->db_connect();
        $result=mysql_query($AD->GetAdmasterId(),$db);
        if($result == false)
        {
            $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
            $COMMON->db_close($db);
            return;
        }
        $COMMON->db_close($db);

        $admaster_id;
        
        $admaster_num=mysql_num_rows($result);

        if($admaster_num > 0)
        {
            for($j=0;$j<$admaster_num;$j++)
            {
                $ret = mysql_fetch_array($result);
                $admaster_id[$j]=$ret['fk_admaster_id'];
            }
        }
        
        $date_sum=$str_date="date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 00:00:00') and date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 23:59:59')";
        for($j=0;$j<$admaster_num;$j++)
        {
            $db=$COMMON->db_connect();
            $result=mysql_query($AD->GetAdDate($admaster_id[$j], $date_sum, 0),$db);
            $COMMON->db_close($db);

            $f1= 0;
            $f2= 0;
            $f3= 0;
            $f4= 0;
            $f5= 0;
            $f6= 0;
            if($result !=FALSE)
            {
                $ret_day=mysql_fetch_array($result);
                $f1= $ret_day['sum1']==""?0:$ret_day['sum1'];
                $f2= $ret_day['sum2']==""?0:$ret_day['sum2'];
                $f3= $ret_day['sum3']==""?0:$ret_day['sum3'];
                $f4= $ret_day['sum4']==""?0:$ret_day['sum4'];
                $f5= $ret_day['sum5']==""?0:$ret_day['sum5'];
                $f6= $ret_day['sum6']==""?0:$ret_day['sum6'];
            }
            $db=$COMMON->db_connect();
            $result=mysql_query($AD->CreateAdDate($admaster_id[$j], "date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 23:00:00')", 1),$db);
            $COMMON->db_close($db);

            $db=$COMMON->db_connect();
            $sql = $AD->SetAdDate($admaster_id[$j], "date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 23:00:00')", 1, $f1, $f2, $f3, $f4, $f5, $f6);
            $result=mysql_query($sql,$db);
            $COMMON->db_close($db);
        }

        //売上集計結果をデータに反映(時間別)
        $db=$COMMON->db_connect();
        mysql_query($SALSE->InsStatSales(0,"date_format(date_sub('$date',interval $i hour),'%Y-%m-%d %H:00:00')"),$db);
        mysql_query("BEGIN",$db);
        $result =mysql_query($SALSE->SetHourData("date_format(date_sub('$date',interval $i hour),'%Y-%m-%d %H:00:00')", 0, $f_coin_use, $f_scoin_use, $f_bid_hand, $f_bid_hand2, $f_bid_auto, $f_auctype, $f_sauctype, $f_free_coin_gen, $f_coin_gen),$db);
        if($result == false)
        {
            mysql_query("ROLLBACK",$db);
            $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
            $COMMON->db_close($db);
            return;
        }
        mysql_query("COMMIT",$db);
        $COMMON->db_close($db);

        //売上集計結果をデータに反映(日別)
        $db=$COMMON->db_connect();
        mysql_query($SALSE->InsStatSales(1,"date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 23:00:00')"),$db);

        //集計結果取得
        $result =mysql_query($SALSE->GetSumDeta(0,"date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 00:00:00')","date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 23:59:59')"));
        $ret_day=mysql_fetch_array($result);

        $f_coin_use = $ret_day['sum28'];
        $f_scoin_use = $ret_day['sum29'];
        $f_bid_hand =$ret_day['sum30'];
        $f_bid_hand2 =$ret_day['sum31'];
        $f_bid_auto = $ret_day['sum32'];
        $f_auctype =array($ret_day['sum33'],$ret_day['sum34'],$ret_day['sum35'],$ret_day['sum36'],$ret_day['sum37'],$ret_day['sum38'],$ret_day['sum39'],$ret_day['sum40'],$ret_day['sum41'],
            $ret_day['sum42'],$ret_day['sum43'],$ret_day['sum44'],$ret_day['sum45'],$ret_day['sum46'],$ret_day['sum47'],$ret_day['sum48'],$ret_day['sum49'],$ret_day['sum50'],$ret_day['sum51'],$ret_day['sum52']);
        $f_sauctype =array($ret_day['sum53'],$ret_day['sum54'],$ret_day['sum55'],$ret_day['sum56'],$ret_day['sum57'],$ret_day['sum58'],$ret_day['sum59'],$ret_day['sum60'],$ret_day['sum61'],
            $ret_day['sum62'],$ret_day['sum63'],$ret_day['sum64'],$ret_day['sum65'],$ret_day['sum66'],$ret_day['sum67'],$ret_day['sum68'],$ret_day['sum69'],$ret_day['sum70'],$ret_day['sum71'],$ret_day['sum72']);

        $f_total=$ret_day['sum27'];
        $total=$ret_day['sum26'];
        $last_price=$ret_day['sum3'];
        $cost=$ret_day['sum4'];
        $f_package_sales=$ret_day['SUM72'];

        $f_buycoin =array($ret_day['sum6'],$ret_day['sum7'],$ret_day['sum8'],$ret_day['sum9'],$ret_day['sum10'],$ret_day['sum11'],$ret_day['sum12'],$ret_day['sum13'],$ret_day['sum14'],
            $ret_day['sum15'],$ret_day['sum16'],$ret_day['sum17'],$ret_day['sum18'],$ret_day['sum19'],$ret_day['sum20'],$ret_day['sum21'],$ret_day['sum22'],$ret_day['sum23'],$ret_day['sum24'],$ret_day['sum25']);

        
        $f_buycoin_total=$ret_day['sum2'];
        $f_memreg =$ret_day['sum1'];
        $sql =$SALSE->SetDayData("date_format(date_sub('$date',interval $i hour),'%Y-%m-%d 23:00:00')", 1, $f_coin_use, $f_scoin_use, $f_bid_hand, $f_bid_hand2, $f_bid_auto, $f_auctype, $f_sauctype, $f_total, $total, $last_price, $cost, $f_package_sales, $f_buycoin, $f_buycoin_total,$f_memreg);
        
        $result =mysql_query($sql,$db);
        if($result == false)
        {
            mysql_query("ROLLBACK",$db);
            $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
            $COMMON->db_close($db);
            return;
        }
        mysql_query("COMMIT",$db);
        $COMMON->db_close($db);
   }

    $db=$COMMON->db_connect();
    $result=mysql_query($COMMON->SetLastDate($date),$db);
    if($result == false)
    {
        $result=mysql_query($COMMON->SetStartDate($f_bidst_start_dt),$db);
        $COMMON->db_close($db);
        return 1;
    }
    $date_end="date_sub('$date',interval 1 hour)";
    //echo $COMMON->SetStartDate2($date_end);
    $result=mysql_query($COMMON->SetStartDate2($date_end),$db);
    if($result == false)
    {
        mysql_query("ROLLBACK",$db);
        $COMMON->db_close($db);
        return 1;
    }
    mysql_query("COMMIT",$db);
    $COMMON->db_close($db);
}

stat_coin_command($argv[1]);

?>
