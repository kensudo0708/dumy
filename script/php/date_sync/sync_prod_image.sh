#!/bin/sh
echo echo `date` PROD_IMAGE START >> /home/auction/script/command/log/sync.log
/home/auction/script/php/date_sync/date_sync.sh /home/auction/script/php/date_sync/dir_list_prod /home/auction/script/php/date_sync/ip_list & >> /home/auction/script/command/log/sync.log

if [ $? -eq 0 ]
then
    echo echo 正常終了 >> /home/auction/script/command/log/sync.log
else
    echo echo 異常終了 >> /home/auction/script/command/log/sync.log
fi
echo echo `date` PROD_IMAGE END >> /home/auction/script/command/log/sync.log
