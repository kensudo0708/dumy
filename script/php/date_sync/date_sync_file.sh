#!/bin/sh

while read a
do
        IP=`echo $a | cut -d',' -f1`
        PASS=`echo $a | cut -d',' -f2`
        #PORT=`echo $a | cut -d',' -f3`

        while read dir
        do
                echo $dir
                expect -c"
                set timeout 1000
                spawn rsync -avz $dir root@$IP:$dir
                expect \"yes\"
                send \"yes\r\"
                expect \"password:\"
                send \"$PASS\r\"
                expect eof"
                sleep 1
        done<$1
echo $IP,$PASS
done<$2
