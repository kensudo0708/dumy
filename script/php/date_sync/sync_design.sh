#!/bin/sh
echo `date` デザインSTART >> /home/auction/script/command/log/sync.log

if [ $? -eq 0 ]
then
/home/auction/script/php/date_sync/date_sync.sh /home/auction/script/php/date_sync/dir_list_page /home/auction/script/php/date_sync/ip_list >> /home/auction/script/command/log/sync.log &
elif [ $? -eq 1 ]
then
/home/auction/script/php/date_sync/date_sync.sh /home/auction/script/php/date_sync/dir_list_image /home/auction/script/php/date_sync/ip_list >> /home/auction/script/command/log/sync.log &
elif [ $? -eq 2 ]
then
/home/auction/script/php/date_sync/date_sync.sh /home/auction/script/php/date_sync/dir_list_all /home/auction/script/php/date_sync/ip_list >> /home/auction/script/command/log/sync.log &
fi

if [ $? -eq 0 ]
then
    echo echo 正常終了 >> /home/auction/script/command/log/sync.log
else
    echo echo 異常終了 >> /home/auction/script/command/log/sync.log
fi

echo `date` デザインEND >> /home/auction/script/command/log/sync.log
