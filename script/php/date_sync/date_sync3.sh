#!/bin/sh

while read a
do
        IP=`echo $a | cut -d',' -f1`
        PASS=`echo $a | cut -d',' -f2`
        #PORT=`echo $a | cut -d',' -f3`

        while read DIR
        do

expect -c"
set timeout 1000
spawn rsync -avz /home/auction/$DIR root@$IP:/home/auction/
expect \"password:\"
send \"$PASS\r\"
expect eof"
sleep 0
        done<$1
        expect -c"
        set timeout 1000
        spawn ssh root@$IP rm -f /home/auction/mvc/smarty/templates_c/*
        expect \"password:\"
        send \"$PASS\r\"
        expect eof"
done<$2
