<?php
include '/home/auction/lib/define.php';
$files = scandir("/home/auction/images/");

function DB_CONNECT()
{
    //MYSQL DBへの接続データの取得
    $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
    mysql_query("SET NAMES utf8",$db);
    return $db;
}

function DB_CLOSE($db)
{
    mysql_close($db);
}

function GetImage()
{
    $sql ="SELECT DISTINCT image.f_photo1 FROM (
            SELECT f_photo1 FROM auction.t_products_template
            UNION ALL
            SELECT f_photo1 FROM auction.t_products_master
            UNION ALL
            SELECT f_photo1 FROM auction.t_end_products
            ) AS image WHERE image.f_photo1 <>''";
    $db=DB_CONNECT();
    $rs = mysql_query($sql,$db);
    $row=mysql_num_rows($rs);
    if($rs==0)
    {
        return 0;
    }
    
    $db=DB_CONNECT();
    return $rs;
}
function DispFileName($rs)
{
    $row=mysql_num_rows($rs);
    for($i=0;$i<$row ;$i++)
    {
        $ret=mysql_fetch_array($rs);
        echo $ret["f_photo1"]."\n";
        $f="/home/auction/images/".$ret["f_photo1"];
        echo $f."\n";
        echo "/home/auction/images/".PRODUCT_LITTLE_PREFIX.$ret["f_photo1"]."\n";
        thumb($f,"/home/auction/images/".PRODUCT_LITTLE_PREFIX.$ret["f_photo1"],'',PRODUCT_LITTLE_WIDTH,PRODUCT_LITTLE_HEIGHT,true);
    }
}

//var_dump(file_exists("d:\\images\\".$files[2]));
//var_dump(getExt("d:\\images\\".$files[2]));
//for($i=2;$i<count($files);$i++){
//$f="d:\\images\\".$files[$i];
//$fName=str_replace("pc_", "", $files[$i]);
//
//
//thumb($f,"d:\\mb\\mb_$fName",'',100,100,true);
//print("mb_$fName<br>");
//}


function getExt($filename) {
    $pathinfo=pathinfo($filename);
    return $pathinfo['extension'];
}
function getImageInfo($img) {
    $imageInfo = getimagesize($img);
    if( $imageInfo!== false) {
        if ( function_exists('image_type_to_extension') ) {
            $imageType = strtolower(substr(image_type_to_extension($imageInfo[2]),1));
        } else {
            $imageType = 'jpeg';
            $tmp = pathinfo($img);
            if ( $tmp['extension'] ) {
                $tmp['extension'] = strtolower( $tmp['extension'] );
                switch ( $tmp['extension'] ) {
                case 'png':
                    $imageType = 'png';
                    break;
                case 'jpeg':
                case 'jpg':
                default:
                    $imageType = 'jpeg';
                    break;
                }
            }
        }
        $imageSize = filesize($img);
        $info = array(
                "width"=>$imageInfo[0],
                "height"=>$imageInfo[1],
                "type"=>$imageType,
                "size"=>$imageSize,
                "mime"=>$imageInfo['mime']
        );
        return $info;
    }else {
        return false;
    }
}
function thumb($image,$thumbname,$type='',$maxWidth=200,$maxHeight=50,$interlace=true) {
    $info  = getImageInfo($image);
    if($info !== false) {
        $srcWidth  = $info['width'];
        $srcHeight = $info['height'];
        $type = empty($type)?$info['type']:$type;
        $type = strtolower($type);
        $interlace  =  $interlace? 1:0;

        $scale = min($maxWidth/$srcWidth, $maxHeight/$srcHeight);
        if($scale>=1) {
            $width   =  $srcWidth;
            $height  =  $srcHeight;
        }else {
            $width  = (int)($srcWidth*$scale);
            $height = (int)($srcHeight*$scale);
        }

        //元のImageロード
        $createFun = 'ImageCreateFrom'.($type=='jpg'?'jpeg':$type);
        $srcImg     = $createFun($image);

        //画像タイプより、処理する
        if($type!='gif' && function_exists('imagecreatetruecolor'))
            $thumbImg = imagecreatetruecolor($width, $height);
        else
            $thumbImg = imagecreate($width, $height);

        if(function_exists("ImageCopyResampled"))
            imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth,$srcHeight);
        else
            imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height,  $srcWidth,$srcHeight);
        if('gif'==$type || 'png'==$type) {
            $background_color=imagecolorallocate($thumbImg,  0,255,0);
            imagecolortransparent($thumbImg,$background_color);
        }

        //jpg,jpegの画像がimageinterlaceより処理されたら、一部の携帯端末が表示されないことがありますので、無効にする
//            if('jpg'==$type || 'jpeg'==$type)
//                imageinterlace($thumbImg,$interlace);

        $imageFun = 'image'.($type=='jpg'?'jpeg':$type);
        $imageFun($thumbImg,$thumbname);
        imagedestroy($thumbImg);
        imagedestroy($srcImg);
        return $thumbname;
    }
    return false;
}

$rs=GetImage();
DispFileName($rs);
?>