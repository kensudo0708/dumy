<?php
define("DOCOMO_DEF","/home/auction/config/def_docomo.php");
define("AU_DEF","/home/auction/config/def_au.php");
define("YAHOO_DEF","/home/auction/config/def_softbank.php");
define("AU_MAIL_DEF","/home/auction/config/def_au_mail.php");
//define("DOCOMO_DEF","./def_docomo.php");
//define("AU_DEF","./def_au.php");
//define("YAHOO_DEF","./softbank.php");

include '/home/auction/lib/define.php';
function DB_CONNECT()
{
    //MYSQL DBへの接続データの取得
    $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
    mysql_query("SET NAMES utf8",$db);
    return $db;
}

function DB_CLOSE($db)
{
    mysql_close($db);
}

function edit_define($carrier)
{
    //キャリア判別
    if($carrier=="docomo")
    {
        $sql="SELECT fk_picture_id as id,f_define_name as name,f_docomo_code as code FROM auction.t_mb_picture";
        $file=DOCOMO_DEF;
    }
    else if($carrier=="au")
    {
        $sql="SELECT fk_picture_id as id,f_define_name as name ,f_au_code as code FROM auction.t_mb_picture";
        $file=AU_DEF;
    }
    else if($carrier=="yahoo")
    {
        $sql="SELECT fk_picture_id as id ,f_define_name as name,f_sb_code as code FROM auction.t_mb_picture";
        $file=YAHOO_DEF;
    }
    else if($carrier=="au_mail")
    {
        $sql="SELECT fk_picture_id as id ,f_define_name as name,f_au_mail_code as code FROM auction.t_mb_picture";
        $file=AU_MAIL_DEF;
    }
    $db=DB_CONNECT();

    $rs = mysql_query($sql,$db);
    $row=mysql_num_rows($rs);

    if($row == 0)
    {
        return 1;
    }

    $fp=fopen($file,"w+");
    $tmp="<?PHP\nreturn array\n(\n";
    fputs($fp, $tmp, strlen($tmp));

    for($i=0;$i<$row ;$i++)
    {
        $ret=mysql_fetch_array($rs);
        $tmp="'".$ret['name']."'=>'".$ret['code']."',\n";
        fputs($fp, $tmp, strlen($tmp));
    }

    $tmp=");\n?>\n";
    fputs($fp, $tmp, strlen($tmp));
    fclose($fp);
    DB_CLOSE($db);
}
edit_define("docomo");
edit_define("au");
edit_define("yahoo");
edit_define("au_mail");
?>