<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '/home/auction/script/php/delete_date/common.php';

class UpMember
{
    //未清算商品数取得
    function GetUnpainNum()
    {
        $sql="SELECT COUNT(ep.fk_end_products_id) as cnt ,ep.f_last_bidder FROM auction.t_end_products ep ,auction.t_member_master mm
                WHERE ep.f_status = 0 AND ep.f_last_bidder = mm.fk_member_id AND mm.f_activity =1
                GROUP BY f_last_bidder
                ORDER BY f_last_bidder";

        //echo $sql;
        $db=DB_CONNECT();
        $result=mysql_query($sql,$db);
        DB_CLOSE($db);

        return $result;
    }
    
    //未清算商品数更新
    function UpUnpainNum($mid,$num)
    {
        $sql ="update auction.t_member set f_unpain_num = $num where fk_member_id =$mid";
        $db=DB_CONNECT();
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            print($sql."\n");
            write_log(PROC_LOG, $sql);
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return false;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
        return true;
    }

    //キャンセル数取得
    function GetCancelNum()
    {
        $sql="SELECT COUNT(ep.fk_end_products_id) as cnt,ep.f_last_bidder FROM auction.t_end_products ep ,auction.t_member_master mm
                WHERE ep.f_status = 1 AND ep.f_last_bidder = mm.fk_member_id AND mm.f_activity =1
                AND ep.f_end_time BETWEEN '20100901' AND '20100930'
                GROUP BY f_last_bidder
                ORDER BY f_last_bidder";

        //echo $sql;
        $db=DB_CONNECT();
        $result=mysql_query($sql,$db);
        DB_CLOSE($db);

        return $result;
    }

    //キャンセル数取得更新
    function UpCancelNum($mid,$num)
    {
        $sql ="update auction.t_member set f_cancel_num = $num where fk_member_id =$mid";
        $db=DB_CONNECT();
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            print($sql."\n");
            write_log(PROC_LOG, $sql);
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return false;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
        return true;
    }    
}

$UpMember = new UpMember();
$result = $UpMember->GetUnpainNum();

$result_num=mysql_num_rows($result);
for($i=0;$i<$result_num;$i++)
{
    $ret=mysql_fetch_array($result);
    $UpMember->UpUnpainNum($ret['f_last_bidder'], $ret['cnt']);
}
$result = $UpMember->GetCancelNum();

$result_num=mysql_num_rows($result);
for($i=0;$i<$result_num;$i++)
{
    $ret=mysql_fetch_array($result);
    $UpMember->UpCancelNum($ret['f_last_bidder'], $ret['cnt']);
}

?>
