<?php

include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';

/*
 * input
 * arg[1] :商品ID
 * arg[2] :会員ID
 * arg[3] :自動入札ID
 * arg[4] :必要コイン枚数
 * arg[5] :価格
 */

class mail_Reserver
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function loadTemplateContent($tplFile,$params="")
    {
        $handle=fopen($tplFile, "r");
        $body=@fread($handle, filesize($tplFile));
        fclose($handle);
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(stripos($body,$key)!==FALSE)
                {
                    $body=str_ireplace("{%".$key."%}", $value, $body);
                }
            }
        }
        return $body;
    }

    function send($f_subject,$f_body,$f_sendfrom,$f_sendto,$db,$id)
    {
        $from = "From: ".$f_sendfrom."\nReturn-Path:".$f_sendfrom;
        $sql="INSERT INTO auction.t_mail_request(f_type,f_subject,f_body,f_sendfrom,f_sendto) VALUES(80,'$f_subject','$f_body','$from','$f_sendto')";
        $sql2="UPDATE auction.t_auto_bidder set f_mail_status = 1 where fk_auto_id =$id";

        $rs=mysql_query("begin",$db);
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }

        $rs=mysql_query($sql2,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function GetMailAddress($mem_id,$db)
    {
        $sql="select f_handle,f_login_id,f_mail_address_pc,f_mail_address_mb ,f_activity from auction.t_member2 where fk_member_id='$mem_id'";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function CheckAutoMail($id,$db)
    {
        $sql ="select f_free_coin,f_buy_coin,f_mail_status from auction.t_auto_bidder where f_status = 0 and fk_auto_id =$id";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function GetProdDate($id,$db)
    {
        $sql = "select f_products_name from auction.t_products2 where fk_products_id =$id";
        $rs=mysql_query($sql,$db);
        return $rs;
    }
}

$reserve=new mail_Reserver();
$db=$reserve->db_connect();
$result=$reserve->CheckAutoMail($argv[3],$db);

$num = mysql_num_rows($result);
if($num == 0)
{
    $reserve->db_close($db);
    return;
}
$ret =mysql_fetch_array($result);

if($ret['f_mail_status'] == 1)
{
    $reserve->db_close($db);
    return;
}
if($argv[4] == 0)
{
    $auto_count = ($ret['f_free_coin']+$ret['f_buy_coin']);
}
else
{
    $auto_count = ($ret['f_free_coin']+$ret['f_buy_coin'])/$argv[4];
}

if(AUTO_MAIL_COUNT == 0 || AUTO_MAIL_COUNT < $auto_count)
{
    $reserve->db_close($db);
    return;
}

$result=$reserve->GetMailAddress($argv[2], $db);
$num=mysql_num_rows($result);
if($num == 0)
{
    $reserve->db_close($db);
    return;
}
$ret =mysql_fetch_array($result);
if($ret['f_activity'] != 1)
{
    $reserve->db_close($db);
    return;
}

$handle = $ret['f_handle'];
$login_id = $ret['f_login_id'];
$mail_pc= $ret['f_mail_address_pc'];
$mail_mb=$ret['f_mail_address_mb'];

$result = $reserve->GetProdDate($argv[1],$db);
$num=mysql_num_rows($result);
if($num == 0)
{
    $reserve->db_close($db);
    return;
}
$ret =mysql_fetch_array($result);

$f_products_name = $ret['f_products_name'];

$param = array(
    "date"=>date('Y年m月d日'),
    "time"=>date('G時i分'),
    "product_name"=>$f_products_name,
    "product_url"=>SITE_URL."index.php/product?pro_id=".$argv[1],
    "member_name"=>$handle,
    "login_id"=>$login_id,
    "site_name"=>SITE_NAME,
    "question_url"=>SITE_URL."index.php/guide/contact",
    "auto_list" =>SITE_URL."index.php/myproduct/autobid",
    "url_home"=>SITE_URL,
    "mail_count"=>AUTO_MAIL_COUNT,
    "price"=>$argv[5],
);

if($mail_pc != NULL && $mail_pc !="")
{
    $body=$reserve->loadTemplateContent("/home/auction/templates/pc/parts/mail_auto.html", $param);
    $reserve->send(AUTO_MAIL_TITLE, $body, INFO_MAIL, $mail_pc, $db,$argv[3]);
}

if($mail_mb!= NULL && $mail_mb !="")
{
    $body=$reserve->loadTemplateContent("/home/auction/templates/mobile/parts/mail_auto.html", $param);
    $reserve->send(AUTO_MAIL_TITLE, $body, INFO_MAIL, $mail_mb, $db,$argv[3]);
}

?>
