<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utilsclass
 *
 * @author mw
 */
final class Utils {
    //put your code here

    /**
     *一意な ID を生成する
     * @return <string> 一意な識別子を文字列で返します。
     */
    public static function uuid() {
        $charid = md5(uniqid(mt_rand(), true));
        $uuid = substr($charid, 0, 8)
                .substr($charid, 8, 4)
                .substr($charid,12, 4)
                .substr($charid,16, 4)
                .substr($charid,20,12);
        return $uuid;
    }
    
    /**
     *メール送信
     * @param <string> $to
     * @param <string> $subject
     * @param <type> $body
     * @param <type> $headers
     * @param <type> $params
     * @return <type>
     */
    public static function sendMail($to,$subject,$body,$headers) {
        mb_language("japanese");
        mb_internal_encoding("UTF-8");
        return mb_send_mail($to,$subject,$body,$headers);
    }


    /**
     *テンプレートからbodyをロード、メール送信する
     * @param <string> $to
     * @param <string> $subject
     * @param <type> $tpl
     * @param <type> $headers
     * @param <type> $params
     * @return <type>
     */
    public static function sendTemplateMail($to,$subject,$tpl,$headers,$params="") {
        $message=self::loadTemplateContent($tpl,$params);
        return self::sendMail($to,$subject,$message,$headers);
    }


    /**
     *テンプレートの引数を引き換え、bodyを戻る
     * @param <type> $tplFile
     * @param <array> $params キーと値の配列
     * @return <string> body
     */
    public static function loadTemplateContent($tplFile,$params="") {
        $handle=fopen($tplFile, "r");
        $body=fread($handle, filesize($tplFile));
        fclose($handle);
        if(is_array($params)) {
            foreach ($params as $key=>$value) {
                if(strpos($body,$key)!==FALSE)
                    $body=str_replace("{%".$key."%}", $value, $body);
            }
        }
        return $body;
    }
}
?>
