<?php

include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';
class mail_Reserver
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function loadTemplateContent($tplFile,$params="")
    {
        $handle=fopen($tplFile, "r");
        $body=@fread($handle, filesize($tplFile));
        fclose($handle);
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(stripos($body,$key)!==FALSE)
                {
                    $body=str_ireplace("{%".$key."%}", $value, $body);
                }
            }
        }
        return $body;
    }

    function send($f_subject,$f_body,$f_sendfrom,$f_sendto,$db)
    {
        $from = "From: ".$f_sendfrom."\nReturn-Path:".$f_sendfrom;
        $sql="INSERT INTO auction.t_mail_request(f_type,f_subject,f_body,f_sendfrom,f_sendto) VALUES(90,'$f_subject','$f_body','$from','$f_sendto')";
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function GetMailAddress($mem_id,$db)
    {
//        $sql="select f_handle,f_login_id,f_mail_address_pc,f_mail_address_mb ,f_activity from auction.t_member_master where fk_member_id='$mem_id'";
        $sql="select f_handle,f_login_id,f_mail_address_pc,f_mail_address_mb ,f_activity from auction.t_member2 where fk_member_id='$mem_id'";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function GetEndProduct($pro_id,$db)
    {
        $sql="SELECT f_products_name,f_address_flag,fk_item_category_id FROM auction.t_end_products WHERE fk_end_products_id=$pro_id";
        $rs=mysql_query($sql,$db);
        if(mysql_num_rows($rs)!=1){
            $pro = new stdClass();
            $pro->f_products_name ="";
            $pro->f_address_flag ="";
            $pro->fk_item_category_id ="0";
            return $pro;
        }
        return mysql_fetch_object($rs);
    }
    function SetWinner($item,$mem_id,$db)
    {
//        $sql="UPDATE auction.t_member set t_hummer_count = t_hummer_count+1 ,f_unpain_num = f_unpain_num +1,f_last_bid_item_id ='$item' where fk_member_id = '$mem_id'";
        $sql="UPDATE auction.t_member2 set f_hummer_count = f_hummer_count+1 ,f_unpain_num = f_unpain_num +1,f_last_bid_item_id ='$item' where fk_member_id = '$mem_id'";
//        printf($sql);
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function set_knock($id,$db)
    {
        $sql="SELECT fk_knocked_id FROM auction.t_knock WHERE fk_member_id =$id AND f_status =0
                ORDER BY fk_knocked_id LIMIT 1";
        echo $sql;
        $rs=mysql_query($sql,$db);

        if(mysql_num_rows($rs)!=1)
        {
            return 0;
        }

        $ret2=mysql_fetch_array($rs);
        $knock_id=$ret2['fk_knocked_id'];
        
        $sql="UPDATE auction.t_knock SET f_status = 2 WHERE fk_knocked_id = $knock_id AND f_status =0 AND fk_member_id = $id";
        echo $sql;
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 1;
        }
        return 2;
    }
}

$reserve=new mail_Reserver();
$db=$reserve->db_connect();
$rs=$reserve->GetMailAddress($argv[2],$db);
$pro=$reserve->GetEndProduct($argv[1],$db);
$row=mysql_num_rows($rs);
if($row != 1)
{
    return 0;
}
$ret=mysql_fetch_array($rs);
if($ret['f_activity'] != 1)
{
    return 0;
}
$param = array(
    "date"=>date('Y年m月d日'),
    "time"=>date('G時i分'),
    "product_name"=>$pro->f_products_name,
    "product_url"=>SITE_URL."index.php/product?pro_id=".$argv[1],
    "product_pay_url"=>SITE_URL."index.php/mypay/pay?pro_id=".$argv[1],
    "member_name"=>$ret['f_handle'],
    "login_id"=>$ret['f_login_id'],
    "price"=>number_format($argv[3]),
    "point_name"=>POINT_NAME,
    "site_name"=>SITE_NAME,
    "question_url"=>SITE_URL."index.php/guide/contact",
    "url_home"=>SITE_URL,
    "carriage" =>"",
);
if($pro->f_address_flag == 0)
{
    $param['carriage']=number_format(CARRIAGE);
}
elseif($pro->f_address_flag == 1)
{
    $param['carriage']=0;
}
$rs=$reserve->SetWinner($argv[1],$argv[2], $db);
//PC用メール設定
if($ret['f_mail_address_pc'] != NULL && $ret['f_mail_address_pc'] !="")
{
    if($pro->fk_item_category_id == COIN_CATEGORY)
    {
        $body=$reserve->loadTemplateContent("/home/auction/templates/pc/parts/mail_winner_coin.html", $param);
    }
    else
    {
        $body=$reserve->loadTemplateContent("/home/auction/templates/pc/parts/mail_winner.html", $param);
    }
    $reserve->send(WINNER_MAIL_TITLE, $body, INFO_MAIL, $ret['f_mail_address_pc'], $db);
}
//MB用メール設定
if($ret['f_mail_address_mb'] != NULL && $ret['f_mail_address_mb'] !="")
{
    if($pro->fk_item_category_id == COIN_CATEGORY)
    {
        $body=$reserve->loadTemplateContent("/home/auction/templates/mobile/parts/mail_winner_coin.html", $param);
    }
    else
    {
        $body=$reserve->loadTemplateContent("/home/auction/templates/mobile/parts/mail_winner.html", $param);
    }
    //$body=$reserve->loadTemplateContent("/home/auction/templates/mobile/parts/mail_winner.html", $param);
    $reserve->send(WINNER_MAIL_TITLE, $body, INFO_MAIL, $ret['f_mail_address_mb'], $db);
}
$reserve->set_knock($argv[2], $db);

return ;
?>
