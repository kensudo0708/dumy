<?php
include '/home/auction/lib/define.php';      //お客さんの環境に合わせて、[config/define.php]のフルパスを設定する
include '/home/auction/lib/server.php';
return array(

'DIR_PATH'=>HOME_PATH.'script/php/email',
/*   DB接続   */
'host'=>DB_SERVER,
'port'=>DB_PORT,
'user'=>DB_USER,
'pwd'=>DB_PASS,
'dbname'=>DB_NAME,
'charset'=>DB_CHARSET,

/*   メール送信情報   */
"tpl"=>"/home/auction/templates/mobile/parts/memreg_premail.html",      //送信メールのbodyのテンプレートの絶対的なPATH
"failed_tpl"=>"/home/auction/templates/mobile/parts/memreg_premail_failed.html",      //送信メールのbodyのテンプレートの絶対的なPATH(失敗の場合）
"confirm_url"=>SITE_URL."index.php/member/register?code=",     //bodyテンプレートの中の置換文字（ユーザ正式登録のURL）
"question_url"=>SITE_URL."index.php/guide/contact",            //bodyテンプレートの中の置換文字（サイドのガイドのURL)
"url_home"=>SITE_URL,                                        //bodyテンプレートの中の置換文字（サイドのホーム)
"from_email"=>INFO_MAIL,                                         //送信のFROM
"subject"=>REGIST_MAIL_SUBJECT,                                                  //送信メールのsubject
"site_name"=>SITE_NAME,


/*   メールアドレス変更送信情報   */
"email_success_tpl"=>"/home/auction/templates/mobile/parts/mail_change_success.html",      //メール変更成功
"email_failed_tpl"=>"/home/auction/templates/mobile/parts/mail_change_failed.html",      //メール変更失敗
"email_subject"=>EMAIL_CHANGE_SUBJECT,                                                  //送信メールのsubject
);
?>
