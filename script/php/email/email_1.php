<?php
include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';
class send_mail
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function getSendMail($db)
    {
        mysql_query("SET NAMES utf8",$db);
        $sql="SELECT t_mail_request,t_mail_request,f_body,f_sendfrom,f_sendto , f_subject FROM auction.t_mail_request WHERE f_status = 0 order by f_type desc LIMIT 0,".MAIL_SEND_NUM;
        $rs=mysql_query($sql,$db);
        $i=mysql_num_rows($rs);
        if($i < 1)
        {
            //該当スタッフなし 又は　一名ではない
            return 0;
        }
        return $rs;
    }

    function setSendMail($db,$t_mail_request,$status)
    {
        $sql="update auction.t_mail_request set f_status ='$status',f_send_dt = now() where t_mail_request='$t_mail_request'";
        mysql_query("begin",$db);
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }
    
    function SendMail_php($to,$subject,$body,$from)
    {
        $subject=mb_convert_kana($subject,"K","UTF8" );
        $body=mb_convert_kana($body,"K","UTF8" );
        if(ereg("vodafone",$to) || ereg("softbank",$to))
        {
            ini_set("mbstring.internal_encoding","JIS");
            mb_language("JIS");
            mb_internal_encoding("JIS");
            $body=mb_convert_encoding($body, "JIS", "UTF8");
            $subject=mb_convert_encoding($subject, "JIS", "UTF8");

        }
        else if(ereg("docomo",$to) || ereg("ezweb", $to))
        {
            ini_set("mbstring.internal_encoding","SJIS");
            mb_language("SJIS");
            mb_internal_encoding("SJIS");
            $body=mb_convert_encoding($body, "SJIS", "UTF8");
            $subject=mb_convert_encoding($subject, "SJIS", "UTF8");

        }
        else
        {
            ini_set("mbstring.internal_encoding","UTF-8");
            mb_language("UTF-8");
            mb_internal_encoding("UTF-8");
            //$body=base64_encode($body);
        }


        if (mb_send_mail($to, $subject,$body, $from))
        {
            return 0;
        } 
        else 
        {
            return 1;
        }
    }

    function SendMail_sh($to,$subject,$body,$from,$request)
    {
        $mail_main="
            FROM:'$from'\n
            TO:'$to'\n
            SUBJECT:'$subject'\n
            '$body'\n";
        `echo -e $mail_main | /usr/sbin/sendmail -t`;

        if (`echo $?` == 0)
        {
            echo "メールが送信されました。";
        }
        else
        {
            echo "メールの送信に失敗しました。";
        }
    }
}

$mail = new send_mail();
$db = $mail->db_connect();
$cnt=0;
$start=time();
$end=time();
while(1)
{
    if($end-$start > 50)
    {
        break;
    }
    $rs=$mail->getSendMail($db);

    if($rs==0 )
    {
        //送信予定が0の場合処理なし
        //sleep(MAIL_SEND_WAIT);
        sleep(5);
        $end=time();
        continue;
    }
    if( $cnt == 0)
    {
        //二通送信しないように
        usleep(MAIL_SEND_WAIT);
        $end=time();
        $cnt++;
        continue;
    }

    for($i=0;$i<mysql_num_rows($rs);$i++)
    {
        $ret=mysql_fetch_array($rs);
        if($mail->SendMail_php($ret['f_sendto'], $ret['f_subject'], $ret['f_body'],$ret['f_sendfrom'])==0)
        {
            $mail->setSendMail($db,$ret['t_mail_request'],1);
        }
        else
        {
            $mail->setSendMail($db,$ret['t_mail_request'],2);
        }
        sleep(0);
    }
    usleep(MAIL_SEND_WAIT);
    $end=time();
}
?>
