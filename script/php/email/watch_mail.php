<?php

include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';

/*
 * input
 * arg[1] :商品ID
 * 
 */

class mail_Reserver
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function loadTemplateContent($tplFile,$params="")
    {
        $handle=fopen($tplFile, "r");
        $body=@fread($handle, filesize($tplFile));
        fclose($handle);
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(stripos($body,$key)!==FALSE)
                {
                    $body=str_ireplace("{%".$key."%}", $value, $body);
                }
            }
        }
        return $body;
    }

    function send($f_subject,$f_body,$f_sendfrom,$f_sendto,$db,$id)
    {
        $from = "From: ".$f_sendfrom."\nReturn-Path:".$f_sendfrom;
        $sql="INSERT INTO auction.t_mail_request(f_type,f_subject,f_body,f_sendfrom,f_sendto) VALUES(70,'$f_subject','$f_body','$from','$f_sendto')";
        $sql2="UPDATE auction.t_watch_list set f_mail_status = 1 where fk_watch_id =$id";

        $rs=mysql_query("begin",$db);
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }

        $rs=mysql_query($sql2,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    function GetMailAddress($mem_id,$db)
    {
        $sql="select f_handle,f_login_id,f_mail_address_pc,f_mail_address_mb ,f_activity from auction.t_member_master where fk_member_id='$mem_id'";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function CheckwatchMail($id,$db)
    {
        $sql ="select fk_watch_id,fk_member_id,f_mail_status from auction.t_watch_list where fk_products_id = $id";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    function GetProdDate($id,$db)
    {
        $sql = "SELECT pm.f_products_name,p.f_now_price FROM auction.t_products_master pm,auction.t_products p WHERE pm.fk_products_id =$id AND pm.fk_products_id = p.fk_products_id";
        echo $sql;
        $rs=mysql_query($sql,$db);
        return $rs;
    }
}

$reserve=new mail_Reserver();
$db=$reserve->db_connect();
$result=$reserve->GetProdDate($argv[1],$db);
$num= mysql_num_rows($result);
if($num == 0)
{
    $reserve->db_close($db);
    return;
}
$ret =mysql_fetch_array($result);
$f_products_name=$ret['f_products_name'];
$f_now_price=$ret['f_now_price'];

$result=$reserve->CheckWatchMail($argv[1],$db);
$num = mysql_num_rows($result);
if($num == 0)
{
    $reserve->db_close($db);
    return;
}

for($i=0;$i< $num ;$i++)
{
    $ret =mysql_fetch_array($result);
    $watch_id=$ret['fk_watch_id'];
    if($ret['f_mail_status'] == 1)
    {
        continue;
    }
    
    $result2=$reserve->GetMailAddress($ret['fk_member_id'], $db);
    $num2=mysql_num_rows($result2);
    if($num == 0)
    {
        continue;
    }
    $ret2 =mysql_fetch_array($result2);
    if($ret2['f_activity'] != 1)
    {
        continue;
    }

    $handle = $ret2['f_handle'];
    $login_id = $ret2['f_login_id'];
    $mail_pc= $ret2['f_mail_address_pc'];
    $mail_mb=$ret2['f_mail_address_mb'];
    $sec = CALL_WATCH_SECOND>60?CALL_WATCH_SECOND:60;
    $param = array(
        "date"=>date('Y年m月d日'),
        "time"=>date('G時i分'),
        "product_name"=>$f_products_name,
        "product_url"=>SITE_URL."index.php/product?pro_id=".$argv[1],
        "member_name"=>$handle,
        "login_id"=>$login_id,
        "site_name"=>SITE_NAME,
        "question_url"=>SITE_URL."index.php/guide/contact",
        "watch_list" =>SITE_URL."index.php/myproduct/watch",
        "url_home"=>SITE_URL,
        "last_second"=>$sec,
        "price" =>$f_now_price,
    );

    if($mail_pc != NULL && $mail_pc !="")
    {
        $body=$reserve->loadTemplateContent("/home/auction/templates/pc/parts/mail_watch.html", $param);
        $reserve->send(CALL_WATCH_MTITLE, $body, INFO_MAIL, $mail_pc, $db,$watch_id);
    }

    if($mail_mb!= NULL && $mail_mb !="")
    {
        $body=$reserve->loadTemplateContent("/home/auction/templates/mobile/parts/mail_watch.html", $param);
        $reserve->send(CALL_WATCH_MTITLE, $body, INFO_MAIL, $mail_mb, $db,$watch_id);
    }
}
?>
