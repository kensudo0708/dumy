<?php
include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';
class send_mail_maga
{
    function db_connect()
    {
        //MYSQL DBへの接続データの取得
        $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
        mysql_query("SET NAMES utf8",$db);
        return $db;
    }

    function db_close($db)
    {
        mysql_close($db);
    }

    function GetMailMaga($db)
    {
        mysql_query("SET NAMES utf8",$db);
        $sql="SELECT fk_reserve_id,f_subject,f_body,f_target,f_sql,f_req_stop from auction.t_mail_reserve where f_reserve_time < now() and f_status = 0 limit 0,1 ";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    //配信開始処理
    function GetMailMaga_Start($db,$fk_reserve_id)
    {
        mysql_query("SET NAMES utf8",$db);
        $sql="update auction.t_mail_reserve set f_start_time = now() ,f_status=1 where fk_reserve_id='$fk_reserve_id'";
        mysql_query("begin",$db);
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }

    //送信対象者チェック
    function get_to($db,$str,$count)
    {
        if($count==0)
        {
            $start = 0;
        }
        else if( $count * MAIL_SEND_NUM > 0 )
        {
            $start = $count * MAIL_SEND_NUM ;
        }
        else
        {
            return false;
        }

        $limit = " limit ". $start.",".MAIL_SEND_NUM;
        $sql ="SELECT mail.f_login_id,mail.f_handle,mail.mail_to FROM ($str) mail where mail.mail_to <> '' $limit";
        //echo $sql."\n";
        $rs=mysql_query($sql,$db);
        return $rs;
    }

    //優先するメールがあるかのチェック
    function CheckMail($db)
    {
        $sql="SELECT t_mail_request as count from auction.t_mail_request where f_status=0";
        $rs=mysql_query($sql,$db);
        $num=0;
        $num = mysql_num_rows($rs);
        return $num;
    }

    function CheckStop($db,$fk_reserve_id)
    {
        $sql="SELECT f_req_stop  from auction.t_mail_reserve where f_req_stop = 1 and fk_reserve_id='$fk_reserve_id'";
        $rs=mysql_query($sql,$db);
        $num=0;
        $num = mysql_num_rows($rs);
        return $num;
    }
    //メール送信
    function SendMail_php($to,$subject,$body,$from)
    {

        $mb_subject=mb_convert_kana($subject,"K","UTF8" );
        //$mb_body="<HTML><HEAD>".$mb_subject."</HEAD><BODY><P>".mb_convert_kana($body,'K','UTF8') ."</P></BODY></HTML>";
        $mb_body=mb_convert_kana($body,"K","UTF8" );
        if(ereg("vodafone",$to) || ereg("softbank",$to))
        {
            echo $to."\n";
            ini_set("mbstring.internal_encoding","Shift_JIS");
            mb_language("ja");
            mb_internal_encoding("Shift_JIS");
            $mb_body=$this->replace_emoji(1,$mb_body);
            $mb_body=mb_convert_encoding($mb_body, "Shift_JIS", "UTF-8");

            $mb_body="<HTML><BODY><P>".$mb_body."</P></BODY></HTML>";
            $mb_subject=mb_convert_encoding($subject, "Shift_JIS", "auto");
            $mb_subject = "=?shift_jis?B?" . base64_encode($mb_subject) . "?=";
            $char="JIS";

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html;charset=Shift_JIS' . "\r\n";
            //$headers .= 'Content-type: text/html;charset=JIS' . "\r\n";
            // 追加のヘッダ
            $headers .= "To : $to" . "\r\n";
            $headers .= $from . "\r\n";
            //$mb_body="<HTML><BODY><P>".$mb_body."</P></BODY></HTML>";            
        }
        else if(ereg("docomo",$to))
        {
            ini_set("mbstring.internal_encoding","SJIS");
            mb_language("ja");
            mb_internal_encoding("Shift_JIS");
            $mb_body=$this->replace_emoji(2,$mb_body);
            $mb_body=mb_convert_encoding($mb_body, "Shift_JIS", "UTF8");
            $mb_subject=mb_convert_encoding($mb_subject, "Shift_JIS", "UTF8");
            $char="SJIS";
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html;charset=iso-8859-1' . "\r\n";
            // 追加のヘッダ
            $headers .= "To : $to" . "\r\n";
            $headers .= $from . "\r\n";
            $mb_body="<HTML><BODY><P>".$mb_body."</P></BODY></HTML>";
        }
        else if(ereg("ezweb", $to))
        {
            ini_set("mbstring.internal_encoding","SJIS");
            mb_language("ja");
            mb_internal_encoding("Shift_JIS");
            $mb_body=$this->replace_emoji(3, $mb_body);
            $mb_body=mb_convert_encoding($mb_body, "Shift_JIS", "UTF8");
            $mb_subject=mb_convert_encoding($mb_subject, "Shift_JIS", "UTF8");
            $char="SJIS";

            
            $headers = $from . "\r\n";
            $headers  .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: multipart/alternative;boundary="abcdefgh";\r\n';
            // 追加のヘッダ
            //$headers .= "To:$to" . "\r\n";
            //echo $mb_body;
            
$mb_body =<<<END

--abcdefgh
Content-Type: text/html; charset=\"iso-2022-jp\"
Content-Transfer-Encoding: 7bit

<HTML>
<BODY>
<P>$mb_body</P><BR>
</BODY>
</HTML>

--abcdefgh
Content-Type: text/plain; charset="iso-2022-jp"
Content-Transfer-Encoding: 7bit

$mb_body

--abcdefgh--

END;
/*
<P>NUM0:\x1b$@\x78\x4b\x1b(B;</P><BR>
<P>\x1b$@\x78\x4a\x1b(B;</P><BR>
<P>NUM0:\x1b$@\xF7\xC9\x1b(B;</P><BR>
<P>NUM0:\x1b$@\xE5\xAC\x1b(B;</P><BR>
*/
            //$mb_body=mb_convert_encoding($mb_body, "SJIS", "UTF8");

            //echo $mb_body;
        }
        else
        {
            ini_set("mbstring.internal_encoding","UTF8");
            mb_language("uni");
            mb_internal_encoding("UTF8");
            if (mb_send_mail($to, $subject,$body, $from))
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        //$mb_body="<HTML><BODY><P>".$mb_body."</P></BODY></HTML>";
        //echo $mb_body ."\n";
        // HTML メールを送信するには Content-type ヘッダが必須
//        $headers  = "MIME-Version: 1.0\n";
//        $headers .= "Content-type: text/html; charset=$char\n";
//
//        // 追加のヘッダ
//        $headers .= "To:$to\n";
//        $headers .= "From:$from\n";

        // HTML メールを送信するには Content-type ヘッダが必須

        mail($to,$mb_subject,$mb_body,$headers);
    }
    function replace_emoji($type,$mess)
    {
        if($type ==1)
        {
            $emoji=include( '/home/auction/config/def_softbank.php');
        }
        else if ($type==2)
        {
            $emoji=include('/home/auction/config/def_docomo.php');
        }
        elseif($type==3)
        {
            $emoji=include ('/home/auction/config/def_au_mail.php');
        }
        //$temp="<em c=";
        //echo $emoji['SHARP'];
        if(ereg("{%",$mess))
        {
            foreach ($emoji as $key=>$value)
            {
                //echo $key ."\n";
                if(stripos($mess,$key)!==FALSE)
                {
                    $tmp_key="{%".$key."%}";
                    if($type ==3){
                        $emoji="";
                        //$tmp="\\x1b$@\x$value[0]$value[1]\x$value[2]$value[3]\\x1b(B";
                        for($i =0 ;$i<strlen($value) ;$i=$i+2)
                        {
                            $emoji .= sprintf('%c', hexdec(substr($value, $i, 2)));
                        }
                        $emoji = "\x1b$@$emoji\x1b(B";
                        $mess=str_ireplace($tmp_key, $emoji, $mess);
                    }
                    else {
                        echo $tmp_key ."\n";
                        $mess=str_ireplace($tmp_key, $value, $mess);
                    }
                    
                }
            }
        }
        return $mess;
    }
//    function SendMail_sh($to,$subject,$body,$from,$request)
//    {
//        $mail_main="
//            FROM:'$from'\n
//            TO:'$to'\n
//            SUBJECT:'$subject'\n
//            '$body'\n";
//        `echo -e $mail_main | /usr/sbin/sendmail -t`;
//
//        if (`echo $?` == 0)
//        {
//            echo "メールが送信されました。";
//        }
//        else
//        {
//            echo "メールの送信に失敗しました。";
//        }
//    }
//
    //終了処理
    function SetReservStatus($db,$fk_reserve_id,$status,$ok,$ng)
    {
        mysql_query("SET NAMES utf8",$db);
        $sql="update auction.t_mail_reserve set f_start_time = now() ,f_status=$status,f_ok_counts =$ok ,f_ng_counts=$ng,f_end_time=now() where fk_reserve_id='$fk_reserve_id'";
        mysql_query("begin",$db);
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        mysql_query("commit",$db);
        return $rs;
    }
//
    function get_pickup_prod($db,$term)
    {
        if($term==FALSE)
        {
            $terms="f_top_description_mb as top_description";
        }
        else
        {
            $terms="f_top_description_pc as top_description";
        }
        mysql_query("SET NAMES utf8",$db);
        $sql="SELECT pm.fk_products_id,pm.f_products_name,p.f_now_price, $terms FROM auction.t_products p,auction.t_products_master pm
                WHERE pm.f_recmmend =1 AND p.f_status = 1 AND pm.fk_products_id= p.fk_products_id ORDER BY p.f_auction_time LIMIT 0,1";
        $rs=mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return 0;
        }
        return $rs;
    }
    //共通置換処理
    function replace_body_common($f_body,$db,$term)
    {
        $tmp_body=$f_body;

        $str=array(
                "site_name"=>SITE_NAME,
                "question_url"=>SITE_URL."index.php/guide/contact",
                "site_url" =>SITE_URL,
        );

        $rs=$this->get_pickup_prod($db,$term);
        $row=mysql_num_rows($rs);
        if($row> 0)
        {
            $ret=mysql_fetch_array($rs);
            $str["n_product_url"]=SITE_URL."index.php/product?pro_id=".$ret['fk_products_id'];
            $str["n_product_name"]=$ret['f_products_name'];
            $str["n_now_price"]=number_format($ret['f_now_price']);
            $str["n_top_description"]=$ret["top_description"];
        }
        else
        {
            $str["n_product_url"]=SITE_URL;
            $str["n_product_name"]="なし";
            $str["n_now_price"]="0";
            $str["n_top_description"]="なし";
        }

        if(is_array($str))
        {
            foreach ($str as $key=>$value)
            {
                if(stripos($tmp_body,$key)!==FALSE)
                {
                    $tmp_body=str_ireplace("{%".$key."%}", $value, $tmp_body);
                }
            }
        }
        return $tmp_body;
    }

    //個別置換処理
    function replace_body_mem($str,$f_body)
    {
        $tmp_body=$f_body;

        if(is_array($str))
        {
            foreach ($str as $key=>$value)
            {
                if(stripos($tmp_body,$key)!==FALSE)
                {
                    $tmp_body=str_ireplace("{%".$key."%}", $value, $tmp_body);
                }
            }
        }
        return $tmp_body;
    }
}



$mail = new send_mail_maga();
$db = $mail->db_connect();
$rs=$mail->GetMailMaga($db);
if($rs==mysql_num_rows($rs))
{
    //送信予定が0の場合処理なし
    //sleep(MAIL_SEND_WAIT);
    sleep(5);
    continue;
}
$ret=mysql_fetch_array($rs);
$fk_reserve_id=$ret['fk_reserve_id'];
$f_subject=$ret['f_subject'];
$f_body=$ret['f_body'];
$f_target=$ret['f_target'];
$f_sql=$ret['f_sql'];

$term=stripos($f_sql,"f_mail_address_pc");

//print($f_body);
$f_body=$mail->replace_body_common($f_body,$db,$term);
//print($f_body);
$ok=0;
$ng=0;
$cnt=0;
if($ret['f_req_stop']==1)
{
    //中断フラグが有効なら終了
    $mail->SetReservStatus($db,$fk_reserve_id,2,$ok,$ng);
}
$from="From: ".$f_target."\nReturn-Path:".$f_target;
$mail->GetMailMaga_Start($db,$fk_reserve_id);
//print($f_sql."1\n");
$start = time();
$end  = time();
$mail->db_close($db);
while(1)
{
    $db = $mail->db_connect();
    if($end -$start > 5)
    {
        //優先順の高いメールがある場合　送信を一時中断
        if($mail->CheckMail($db) != 0)
        {
            //print("一時中断\n");
            sleep(5);
            $star = time();
            $end  = time();
            continue;
        }
    }
   // print("起動中1\n");
    //中断要求が出てる場合、処理終了
    if($mail->CheckStop($db, $fk_reserve_id) !=0)
    {
        //print("中断受付\n");
        break;
    }
    //print("起動中2\n");
    //print($f_sql."\n");
    $result=$mail->get_to($db,$f_sql,$cnt);
    if($result == false)
    {
        $mail->SetReservStatus($db,$fk_reserve_id,2,$ok,$ng);
        break;
    }
    $tmp_send=mysql_num_rows($result);

    if($tmp_send==0)
    {
        $mail->SetReservStatus($db,$fk_reserve_id,2,$ok,$ng);
        break;
    }

    for($i=0;$i<$tmp_send;$i++)
    {
        $ret = mysql_fetch_array($result);
        $send_to = $ret['mail_to'];
        $str_mem=array(
                "member_name"=>$ret['f_handle'],
                "login_id"=>$ret['f_login_id'],
        );
        $main_body=$mail->replace_body_mem($str_mem,$f_body);
        if($mail->SendMail_php($send_to, $f_subject, $main_body,$from)==0)
        {
            $ok++;
        }
        else
        {
            sleep(1);
            if($mail->SendMail_php($pic_data,$send_to, $f_subject, $f_body,$from)==0)
            {
                //$mail->setSendMail($db,$ret['t_mail_request'],2);
                $ok++;
            }
            $ng++;
        }
    }
    $cnt++;
    $end  = time();
    sleep(1);
    $mail->db_close($db);
}
$db = $mail->db_connect();
$mail->SetReservStatus($db,$fk_reserve_id,2,$ok,$ng);
$mail->db_close($db);

?>
