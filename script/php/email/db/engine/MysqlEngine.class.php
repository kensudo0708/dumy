<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Mysqlデータベースのアクセスエンジン
 *
 * @author mw
 */
class MysqlEngine implements DataAccessEngine {
    //put your code here

    private $conn;//="";
    private $host;
    private $dbname;
    private $user;
    private $pwd;
    private $charset;
    
    /**
     * トランザクション処理かどうかを示す
     * @var boolean
     */
    private $isTran;

    /**
     *DataAccessエンジンが初期化する
     * @param <type> $host
     * @param <type> $dbname
     * @param <type> $user
     * @param <type> $pwd
     * @param <type> $charset
     */
    public function startup($host,$port, $dbname, $user, $pwd, $charset){
        $this->host=$host.":".$port;
        $this->dbname=$dbname;
        $this->user=$user;
        $this->pwd=$pwd;
        $this->charset=$charset;

    }

    /**
     *接続しているかどうか、
     * @return <boolean>
     */
    public function isOpen(){
        return !is_null($this->conn);
    }

    /**
     *DB接続をオープンする
     */
    public function openConnection(){
        $this->conn=mysql_connect($this->host, $this->user, $this->pwd);
        mysql_select_db($this->dbname, $this->conn);
        mysql_set_charset($this->charset,$this->conn);
    }
    
    /**
     * DB接続をクローズする
     */
    public function closeConnection(){
        if(mysql_close($this->conn)){
            $this->conn=null;
            if($this->isTran)
                $this->isTran=false;
        }
    }

    /**
     *指定されたクエリを実行し、Objectの配列を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return Array() Object配列
     */
    public function getObjectByQuery($sql,$params=false){
        $objList=array();
        $query=$this->query($sql,$params);
        if($query===FALSE)
            return $query;
        while($obj=mysql_fetch_object($query))
            array_push($objList, $obj);
       
        return $objList;
    }

    /**
     *指定されたクエリを実行し、配列を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return Array()　実行結果
     */
    public function getArrayByQuery($sql,$params=false){
        $objList=array();
        $query=$this->query($sql,$params);
        if($query===FALSE)
            return $query;
        while($obj=mysql_fetch_array($query))
            array_push($objList, $obj);
        
        return $objList;
    }

//    /**
//     *指定されたクエリを実行し、一行目のデータをObjectとしてを戻り（SELECT用)
//     * @param <Array> $params　クエリの引数の値をセットした配列
//     * @return (Object)　唯一の結果をもどり、結果が複数行の場合でも、一行目のデータだけ、後のデータ無視
//     */
//    public function uniqueObject($sql,$params=false) {
//        $result=$this->getObjectByQuery($sql,$params);
//        return count($result)==1 ? $result : $result[0];
//    }
//
//    /**
//     *指定されたクエリを実行し、一行目のデータをArrayとしてを戻り（SELECT用)
//     * @param <Array> $params　クエリの引数の値をセットした配列
//     * @return (Array)　唯一の結果をもどり、結果が複数行の場合でも、一行目のデータだけ、後のデータ無視
//     */
//    public function uniqueArray($sql,$params=false){
//        $result=$this->getArrayByQuery($sql,$params);
//        return count($result)==1 ? $result : $result[0];
//    }

    /**
     *指定されたクエリを実行し、成功した場合に TRUE 、エラー時に FALSE を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return 実行結果
     */
    public function query($sql,$params=false){
        if ($params) {
            foreach ($params as &$v)
                $v = mysql_real_escape_string($v);
            $sql = vsprintf( str_replace(DB::PARAM_STR,"'%s'",$sql), $params );
            $result = mysql_query($sql,$this->conn);
        }
        else
            $result = mysql_query($sql,$this->conn);
//        Logger::debug($sql);
        return $result;
    }

    /**
     *指定されたクエリを実行し、影響を受けた行数を返します
     * @param <String> $resource クエリ
     * @return <int> 影響を受けた行数
     */
    public function affectedRows($resource){
        return mysql_affected_rows($this->conn);
    }
    
    /**
     *指定されたクエリを実行し、結果における行の数を戻り
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return <int> 行数
     */
    public function resultRows($sql,$params=false){
        return mysql_num_rows($this->query($sql,$params));
    }
    /**
     * トランザクションの開始
     */
    public function beginTransaction(){
        if($this->query("START TRANSACTION")){
            $this->isTran=true;
            return true;
        }
        return false;
    }

    /**
     * トランザクションのコミット
     */
    public function commitTransaction(){
        if(!$this->isTran)
                return;
        if($this->query("COMMIT")){
            $this->isTran=false;
            return true;
        }
        return false;
    }

    /**
     *トランザクションのロールバック
     */
    public function rollbackTransaction(){
        if($this->query("ROLLBACK")){
            $this->isTran=false;
            return true;
        }
        return false;
    }

    /**
     *データベース操作のエラーメッセージを返す
     * @param <type> $link MySQLの接続。
     * @return <type>
     */
    public function error() {
        return mysql_error($this->conn);
    }
    

}
?>
