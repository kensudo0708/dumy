<?php


/**
 * データアクセスクラス
 *
 * @author mw
 */
final class DB {

    /**
     * クエリのパラメータの文字表現
     */
    const PARAM_STR="?";

    private static $host;
    private static $port;
    private static $dbname;
    private static $user;
    private static $pwd;
    private static $charset;
    /**
     * ＤＢアクセスエンジンのインスタンス
     * @var DataAcessEngine [DataAcessEngine]インタフェースに実装したクラスのインスタンス
     */
    private static $engine;

    /**
     * コンストラクタ、外部から、インスタンスの作成できないよう
     */
    private function  __construct() {

    }

    /**
     * ＤＢアクセスエンジンを設定する
     * @param DataAcessEngine $engine
     */
    public static function setDataAccessEngine($engine) {
        //$engineは[DataAcessEngine]インタフェースに実装したクラスのインスタンス
        if($engine instanceof DataAccessEngine)
            DB::$engine=$engine;
        else
            throw new AppException(ConstKeys::DATA_ENGINE_NO_IMPLEMENT);
        DB::$engine->startup(DB::$host,DB::$port,DB::$dbname,DB::$user,DB::$pwd,DB::$charset);
    }

    /**
     * 初期化処理
     * @param String $host
     * @param String $port
     * @param String $user
     * @param String $pwd
     * @param String $dbname
     * @param String $charset
     */
    public static function init($host,$port,$user,$pwd,$dbname,$charset="utf8") {
        DB::$host=$host;
        DB::$port=$port;
        DB::$dbname=$dbname;
        DB::$user=$user;
        DB::$pwd=$pwd;
        DB::$charset=$charset;
    }

    /**
     * DB接続オープンする
     */
    public static function openConnection() {
        if(!DB::$engine->isOpen())
            DB::$engine->openConnection();
    }

    /**
     * DB接続をクローズする
     */
    public static function closeConnection() {
        if(DB::$engine->isOpen())
            DB::$engine->closeConnection();
    }

    /**
     * クエリを実行した結果をオブジェクトとして取得する
     * openConnectionをコールする必要がある
     * @param String $sql
     * @param <Array> $params 引数$sqlはパラメータ化したSQL文の場合、パラメータの値をセットした配列
     * @return Array() Object配列
     */
    public static function getObjectByQuery($sql,$params=false) {
        $objList=DB::$engine->getObjectByQuery($sql, $params);
        if($objList===FALSE)
            throw new AppException(DB::$engine->error());
        return $objList;
    }

    /**
     * クエリを実行した結果をＡｒｒａｙとして取得する
     * openConnectionをコールする必要がある
     * @param String $sql
     * @param <Array> $params 引数$sqlはパラメータ化したSQL文の場合、パラメータの値をセットした配列
     * @return Array()　実行結果
     */
    public static function getArrayByQuery($sql,$params=false) {
        $objList=DB::$engine->getArrayByQuery($sql, $params);
        if($objList===FALSE)
            throw new AppException(DB::$engine->error());
        return $objList;
    }

    /**
     *指定されたクエリを実行し、影響を受けた行数を返します
     * @param <String> $sql クエリ
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return <int> 影響を受けた行数、実行失敗の場合、-1を返し
     */
    public static function executeNonQuery($sql,$params=false) {
        $resource=DB::$engine->query($sql, $params);
        if($resource===FALSE)
            throw new AppException(DB::$engine->error());
        return DB::$engine->affectedRows($resource);
    }

    /**
     *指定されたクエリを実行し、一行目のデータをObjectとしてを戻り（SELECT用)
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return (Object)　唯一の結果をもどり、結果が複数行の場合でも、一行目のデータだけ、後のデータ無視
     */
    public static function uniqueObject($sql,$params=false) {
        $objList=self::getObjectByQuery($sql, $params);
        return count($objList)>0 ? $objList[0] : NULL;
    }

    /**
     *指定されたクエリを実行し、一行目のデータをArrayとしてを戻り（SELECT用)
     * @param <Array> $params　クエリの引数の値をセットした配列
     * @return (Array)　唯一の結果をもどり、結果が複数行の場合でも、一行目のデータだけ、後のデータ無視
     */
    public static function uniqueArray($sql,$params=false) {
        $objList=self::getArrayByQuery($sql, $params);
        return count($objList)>0 ? $objList[0] : NULL;
    }

    /**
     * クエリを実行する、
     * @param String $sql
     * @param <Array> $params 引数$sqlはパラメータ化したSQL文の場合、パラメータの値をセットした配列
     * @return　<int> 結果における行の数
     */
    public static function result_rows($sql,$params=false) {
        $result=DB::$engine->resultRows($sql, $params);
        if($result===FALSE)
            throw new AppException(DB::$engine->error());

        return $result;
    }

    /**
     * トランザクション開始処理、openConnectionをコールする必要がない
     * 接続がオープンされない場合、自動にオープンする
     */
    public static function begin() {
        if(!DB::$engine->isOpen())
            DB::$engine->openConnection();
        if(!DB::$engine->beginTransaction())
            throw new AppException(DB::$engine->error());
    }

    /**
     * コミット処理
     */
    public static function commit() {
        if(!DB::$engine->commitTransaction())
            throw new AppException(DB::$engine->error());
    }

    /**
     *  トランザクションをロールバックする
     */
    public static function rollback() {
        if(!DB::$engine->rollbackTransaction())
            throw new AppException(DB::$engine->error());

    }

    /**
     * オーバーライドさせない
     */
    private final function  __clone() {

    }
}

?>
