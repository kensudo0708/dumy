<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
include 'AbstractRepository.class.php';
/**
 * Description of ProductRepository
 *
 * @author mw
 */
class ProductRepository extends AbstractRepository {
    //put your code here

    private static $product_open_area_a_prefix="product_open_area_a";
    private static $product_open_area_b_prefix="product_open_area_b";

    private static $in_area_prefix="in_area";
    private static $out_area_prefix="out_area";

    private static $out_area_a_flag="out_area_a";
    private static $out_area_b_flag="out_area_b";

    /**
     * アプリケーション起動する時、実行される、抽象的なメソッド、サブクラスがオーバーライド
     */
    public function startup() {
        parent::createEngine();
    }


    /**
     *開催中商品をキャッシュに追加する
     * @param <array> $products
     */
    public static function addOpenProductCache($products) {
        $area="";
        $out="";
        $sleepTotal=0;
        $in = self::getInFlagValue();
        if($in=="a"){
            $area=self::$product_open_area_a_prefix;
            $out=self::$out_area_a_flag;
        }
        else{
            $area=self::$product_open_area_b_prefix;
            $out=self::$out_area_b_flag;
        }

        while(true) {
            $out = self::getOutFlayValue($out);
            if(empty($out) || $out===FALSE || $out==0) {
                parent::$cache->removeAllElement($area);
                foreach ($products as $p)
                    parent::$cache->addElement($area,$p->id,$p);
                $in = $in=="a" ? "b" : ($in=="b" ? "a" : "b");
                parent::$cache->addVariable(self::$in_area_prefix,$in);
                break;
            }

            else{
                if($sleepTotal>=300000){
                    self::addOpenProductCache($products);
                    return;
                }
                usleep(1000);
                $sleepTotal+=1000;
            }

        }
    }

    public static function getInFlagValue() {
        $in = parent::$cache->getVariable(self::$in_area_prefix);
        if(empty ($in)) {
            $in ="a";
            parent::$cache->addVariable(self::$in_area_prefix,$in);
        }
        return $in;
    }

    public static function getOutFlayValue($out) {
        return parent::$cache->getVariable($out);
    }

    /**
     *開催中商品のログをキャッシュに追加する
     * @param <array> $logs
     */
    public static function addOpenProductLogCache($logs) {
        parent::createEngine();
        foreach ($logs as $log) {
            parent::$cache->removeAllElement(self::$product_open_log_prefix);
            parent::$cache->addElement(self::$product_open_log_prefix,$log->f_products_id,$log);
        }
    }
}
?>
