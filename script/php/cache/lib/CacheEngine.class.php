<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *キャッシュエンジン
 * @author mw
 */
interface CacheEngine {
    //put your code here
    public function addElement($cacheName, $key, $element);

    public function addVariable($varName,$value);

    public function getElement($cacheName, $key);

    public function getVariable($varName);

    public function getCache($cacheName);

    public function getElements($cacheName);

    public function getCacheSize($cacheName);

    public function getKeys($cacheName);

    public function removeElement($cacheName, $key);

    public function removeAllElement($cacheName);

    public function removeCache($cacheName);

    public function isKeyInCache($cacheName, $key);
}
?>
