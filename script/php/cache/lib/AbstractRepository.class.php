<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include 'Repository.class.php';
/**
 * Description of AbstractRepository
 *
 * @author mw
 */
abstract class AbstractRepository implements Repository {
    //put your code here
    protected static $cache;

    /**
     *キャッシュエンジンを設置する
     * @param <string> $engine CacheEngineを実装したクラスのフルパス
     */
    public function setCacheEngine($engine){
        self::$cache=$engine;
    }

    /**
     * キャッシュエンジンのインスタンスを作る
     */
    public static function createEngine(){
        self::$cache = createInstance(Config::value("CACHE_ENGINE_CLASS"));
    }
}
?>
