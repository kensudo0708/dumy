<?php
include '../../../lib/define.php';
include 'lib/ProductRepository.class.php';
include 'lib/ApcCache.class.php';
$cfg=array(
"micro_seconds"=>200000,
'stopFlag'=>"stop.txt",

/*   DB接続   */
'host'=>DB_SERVER,
'port'=>DB_PORT,
'user'=>DB_USER,
'pwd'=>DB_PASS,
'dbname'=>DB_NAME,
'charset'=>DB_CHARSET,
    
);


/**
 * オークションスタート
 */
function start($cfg) {

    set_time_limit(0);
    ignore_user_abort(true);

    
    while(true) {
        if(file_exists($cfg['stopFlag']))
                break;
        
        /*   cache 処理    start   */
        $conn = openConnection($cfg);
        //商品を取得
        $products=getProducts();
        ProductRepository::addOpenProductCache($products);

        /*   cache 処理    end   */
        
        usleep($cfg['micro_seconds']);
        closeConnection($conn);
    }
    
}

function createProductsCache($products) {
    $text=json_encode($products);
    apc_store(CACHE_NAME,$text);
}

function getProducts() {
    $objList=array();
    $query=mysql_query("SELECT
                          p.fk_products_id       AS id,
                          p.f_now_price          AS price,
			  (CASE WHEN p.f_last_bidder_handle IS NULL THEN '入札者はいません' ELSE p.f_last_bidder_handle END)  AS last_bidder,
                          p.f_auction_time       AS `time`,
                          mast.f_market_price    AS market_price,
                          p.f_status AS `status`
                        FROM t_products p
                        INNER JOIN t_products_master mast ON mast.fk_products_id=p.fk_products_id
                        WHERE p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5");
    while($obj=mysql_fetch_object($query))
        array_push($objList, $obj);
    return $objList;
}

/**
 * 商品の入札ログ処理
 */
function getProductLog(){
    
}

function openConnection($cfg) {
    $conn=mysql_connect($cfg['host'], $cfg['user'], $cfg['pwd']);
    mysql_select_db($cfg['dbname'], $conn);
    mysql_set_charset($cfg['charset'],$conn);
    return $conn;
}

function closeConnection($conn) {
    if(!is_null($conn)) {
        try {
            if(mysql_close($conn)) 
                $conn=null;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

//キャッシュ初期化する
$repositroy = new ProductRepository();
$repositroy->setCacheEngine(new ApcCache());

if(file_exists($cfg['stopFlag'])) {
    unlink($cfg['stopFlag']);
    start($cfg);
}
else
    start($cfg);

?>
