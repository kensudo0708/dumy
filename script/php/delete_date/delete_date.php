<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include '/home/auction/script/php/delete_date/common.php';
include '/home/auction/script/php/delete_date/delete_products_flag.php';
include '/home/auction/script/php/delete_date/delete_member.php';
include '/home/auction/script/php/delete_date/delete_etc.php';

////商品マスターで削除になっている
$DEL_PROD = NEW DELETE_PRODUCT();
$db=DB_CONNECT();
$rs_prod = mysql_query($DEL_PROD->delete_flag_prod(),$db);
$rs_prod_num = mysql_num_rows($rs_prod);
DB_CLOSE($db);
echo "del_prod\n";
for($i=0;$i<$rs_prod_num;$i++)
{
    $ret=mysql_fetch_array($rs_prod);
    $prod_id=$ret['fk_products_id'];
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($DEL_PROD->UpDate_lastbid_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD->UpDate_lastbid_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD->delete_bidlog_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD->delete_bidlog_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }

    if(mysql_query($DEL_PROD->delete_prod_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD->delete_prod_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD->delete_prodmaster_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD->delete_prodmaster_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD->delete_endprod_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD->delete_endprod_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
}

//終了商品で削除になっている
$DEL_PROD_END = NEW DELETE_END_PRODUCT();
$db=DB_CONNECT();
$rs_prod = mysql_query($DEL_PROD_END->delete_flag_prod(),$db);
$rs_prod_num = mysql_num_rows($rs_prod);
DB_CLOSE($db);
echo "del_end\n";
for($i=0;$i<$rs_prod_num;$i++)
{
    $ret=mysql_fetch_array($rs_prod);
    $prod_id=$ret['fk_products_id'];

    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($DEL_PROD_END->UpDate_lastbid_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD_END->UpDate_lastbid_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD_END->delete_bidlog_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD_END->delete_bidlog_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD_END->delete_endprod_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD_END->delete_endprod_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD_END->delete_prod_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD_END->delete_prod_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    if(mysql_query($DEL_PROD_END->delete_prodmaster_sql($prod_id),$db) == false)
    {
        echo $DEL_PROD_END->delete_prodmaster_sql($prod_id)."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }
    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
}

//退会者削除
$MEM = new DELETE_MEMBER();
$db=DB_CONNECT();
$mem_result = mysql_query($MEM->getDropMem(),$db);
//echo $MEM->getDropMem()."\n";
$mem_num = mysql_num_rows($mem_result);
DB_CLOSE($db);
//echo "drop_mem\n";
for($i=0;$i<$mem_num;$i++)
{
    $ret =mysql_fetch_array($mem_result);
    $fk_member_id = $ret['fk_member_id'];
    $fk_adcode_id = $ret['fk_adcode_id'];
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if($fk_adcode_id != "")
    {
        $sql = $MEM->UP_parent($fk_adcode_id);
        if(mysql_query($sql,$db) == false)
        {
            echo $sql."\n";
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            continue;
        }
    }
    $sql = $MEM->Del_member($fk_member_id);
    if(mysql_query($sql,$db) == false)
    {
        echo $sql."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }

    $sql = $MEM->Del_member_master($fk_member_id);
    if(mysql_query($sql,$db) == false)
    {
        echo $sql."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }

    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
}
//削除者削除
$MEM = new DELETE_MEMBER();
$db=DB_CONNECT();
$mem_result = mysql_query($MEM->getDelMem(),$db);
//echo $MEM->getDelMem()."\n";
$mem_num = mysql_num_rows($mem_result);
DB_CLOSE($db);
//echo "del_mem\n";
for($i=0;$i<$mem_num;$i++)
{
    $ret =mysql_fetch_array($mem_result);
    $fk_member_id = $ret['fk_member_id'];
    $fk_adcode_id = $ret['fk_adcode_id'];

    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if($fk_adcode_id !="" )
    {
        $sql = $MEM->UP_parent($fk_adcode_id);
        if(mysql_query($sql,$db) == false)
        {
            echo $sql."\n";
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            continue;
        }
    }

    $sql = $MEM->Del_member($fk_member_id);
    if(mysql_query($sql,$db) == false)
    {
        echo $sql."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }

    $sql = $MEM->Del_member_master($fk_member_id);
    if(mysql_query($sql,$db) == false)
    {
        echo $sql."\n";
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        continue;
    }

    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
}

$ETC = New ETC_DEL();

$sql = $ETC->Del_auto();
$db=DB_CONNECT();
mysql_query("BEGIN",$db);
if(mysql_query($sql,$db) == false)
{
    mysql_query("ROLLBACK",$db);
    DB_CLOSE($db);
    continue;
}
mysql_query("COMMIT",$db);
DB_CLOSE($db);

$sql = $ETC->Del_mail_req();
$db=DB_CONNECT();
mysql_query("BEGIN",$db);
if(mysql_query($sql,$db) == false)
{
    mysql_query("ROLLBACK",$db);
    DB_CLOSE($db);
    continue;
}
mysql_query("COMMIT",$db);
DB_CLOSE($db);

$sql = $ETC->Del_mail_res();
$db=DB_CONNECT();
mysql_query("BEGIN",$db);
if(mysql_query($sql,$db) == false)
{
    mysql_query("ROLLBACK",$db);
    DB_CLOSE($db);
    continue;
}
mysql_query("COMMIT",$db);
DB_CLOSE($db);

?>
