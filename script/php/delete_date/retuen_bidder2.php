<?php
include '/home/auction/script/php/delete_date/common.php';
//include '/home/auction/lib/define.php';
//自動入札設定のコイン返却処理
function WriteLog($str)
{
    $file="/home/auction/script/command/log/kari_del.log";
    $file2="/home/auction/script/php/log/kari_del.log.".date("Ymd").".csv";
    //$str = "$mid,$pc_mail,$mb_mail\n";
    //echo $str;
    $fp=fopen($file,"a+");
    fputs($fp, $str, strlen($str));
    fclose($fp);
    $fp=fopen($file2,"a+");
    fputs($fp, $str, strlen($str));
    fclose($fp);
    return;
}
function WriteLog2($str)
{
    $file="/home/auction/script/command/log/return_batch.log";
    //$str = "$mid,$pc_mail,$mb_mail\n";
    //echo $str;
    $str =date("H:i:s").":".$str;
    $fp=fopen($file,"a+");
    fputs($fp, $str, strlen($str));
    fclose($fp);
    return;
}

function close_auto()
{
    write_log(PROC_LOG,"自動入札での預かりコインの返却を開始します");
    $db=DB_CONNECT();
    //返却対象コインを取得
    $sql ="SELECT ab.fk_auto_id,ab.fk_member_id,ab.f_free_coin,ab.f_buy_coin,ab.fk_products_id ,ab.f_status,auc.f_spend_coin FROM auction.t_auto_bidder ab,auction.t_products_master pm,auction.t_auction_type auc ,auction.t_products p WHERE ab.fk_products_id = pm.fk_products_id AND pm.fk_auction_type_id = auc.fk_auction_type_id AND p.fk_products_id =pm.fk_products_id AND (p.f_status =2 AND (ab.f_status =0 OR ab.f_status =1)) ";
    //echo $sql;
    //write_log(SQL_LOG, $sql);
    $rs=mysql_query($sql,$db);
    $row=mysql_num_rows($rs);

    for($i=0;$i<$row;$i++)
    {
        $ret=mysql_fetch_array($rs);

        $sql = "select f_coin ,f_free_coin from auction.t_member where fk_member_id =".$ret['fk_member_id'];
        //echo $sql."\n";
        $rs2=mysql_query($sql,$db);
        if(mysql_num_rows($rs2)==0)
        {
            continue;
        }
        $ret2=mysql_fetch_array($rs2);
        
        WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚数(free)".$ret['f_free_coin']." 返却枚数(buy)".$ret['f_buy_coin']."\n");
        $f_coins=$ret['f_free_coin']+$ret2['f_free_coin'];
        $coins=$ret['f_buy_coin']+$ret2['f_coin'];
        if($ret['f_spend_coin'] > 0)
        {
            WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚後数(free) :".$f_coins." 返却後枚数(buy) :".$coins."\n");
        }
        else
        {
            WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " :フリーオークション\n");
        }
        $sql  = "UPDATE auction.t_member set f_free_coin=f_free_coin+".$ret['f_free_coin'] .",f_coin = f_coin+".$ret['f_buy_coin']." ";
        $sql .= "WHERE fk_member_id=".$ret['fk_member_id'];

        //echo $sql."\n";
        
        
        mysql_query("BEGIN",$db);
        if($ret['f_spend_coin'] > 0)
        {
            if(mysql_query($sql,$db)==FALSE)
            {
                write_log(SQL_LOG, $sql);
                write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(返却処理失敗)");
                mysql_query("ROLLBACK",$db);
                DB_CLOSE($db);
                return;
            }
        }
        $sql ="UPDATE auction.t_auto_bidder  set f_status = 9 WHERE fk_auto_id=".$ret['fk_auto_id'];
        //echo $sql."\n";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(auto_bidder削除処理失敗)");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        echo "aaaa";
        mysql_query("COMMIT",$db);
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"自動入札での預かりコインの返却を終了します");
}

function close_auto2()
{
    write_log(PROC_LOG,"自動入札での預かりコインの返却を開始します");
    $db=DB_CONNECT();
    //返却対象コインを取得
    $sql ="SELECT ab.fk_auto_id,ab.fk_member_id,ab.f_free_coin,ab.f_buy_coin,ab.fk_products_id ,ab.f_status,auc.f_spend_coin FROM auction.t_auto_bidder ab,auction.t_products_master pm,auction.t_auction_type auc ,auction.t_products p WHERE ab.fk_products_id = pm.fk_products_id AND pm.fk_auction_type_id = auc.fk_auction_type_id AND p.fk_products_id =pm.fk_products_id AND ab.f_status =2 ";
    //echo $sql;
    //write_log(SQL_LOG, $sql);
    $rs=mysql_query($sql,$db);
    $row=mysql_num_rows($rs);

    for($i=0;$i<$row;$i++)
    {
        $ret=mysql_fetch_array($rs);

        $sql = "select f_coin ,f_free_coin from auction.t_member where fk_member_id =".$ret['fk_member_id'];
        //echo $sql."\n";
        $rs2=mysql_query($sql,$db);
        if(mysql_num_rows($rs2)==0)
        {
            continue;
        }
        $ret2=mysql_fetch_array($rs2);

        WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚数(free)".$ret['f_free_coin']." 返却枚数(buy)".$ret['f_buy_coin']." キャンセル\n");
        $f_coins=$ret['f_free_coin']+$ret2['f_free_coin'];
        $coins=$ret['f_buy_coin']+$ret2['f_coin'];
        WriteLog2("返却対象会員ID:".$ret['fk_member_id']." 返却対象ID:". $ret['fk_auto_id']. " 返却枚後数(free) :".$f_coins." 返却後枚数(buy) :".$coins."\n");

        $sql  = "UPDATE auction.t_member set f_free_coin=f_free_coin+".$ret['f_free_coin'] .",f_coin = f_coin+".$ret['f_buy_coin']." ";
        $sql .= "WHERE fk_member_id=".$ret['fk_member_id'];

        //echo $sql."\n";
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(返却処理失敗)");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        $sql ="UPDATE auction.t_auto_bidder  set f_status = 9 WHERE fk_auto_id=".$ret['fk_auto_id'];
        //echo $sql."\n";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"自動入札での預かりコインの返却に失敗しました(auto_bidder削除処理失敗)");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        echo "aaaa";
        mysql_query("COMMIT",$db);
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"自動入札での預かりコインの返却を終了します");
}

//ウォッチリストの消去
function close_watch()
{
    write_log(SQL_LOG,"ウォッチリストの消去開始");
    $sql="DELETE FROM auction.t_watch_list WHERE  EXISTS (SELECT p.fk_products_id FROM auction.t_products p WHERE p.f_status = 2 || p.f_status = 9 )";
    
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        print($sql."\n");
        write_log(SQL_LOG, $sql);
        write_log(SQL_LOG,"ウォッチリストの消去に失敗しました");
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"ウォッチリストの消去終了");
}

function cancel_prod()
{
    write_log(SQL_LOG,"商品キャンセル開始");
    $sql=" UPDATE auction.t_end_products ep SET f_status = 1 WHERE ep.f_status = 0 and ep.f_end_time < DATE_SUB(NOW() , INTERVAL ".CANCEL_TIME ." DAY) AND  EXISTS
                  (SELECT fk_member_id FROM auction.t_member_master mm WHERE f_activity > 89 AND ep.f_last_bidder = mm.fk_member_id)";
    $db=DB_CONNECT();
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        write_log(PROC_LOG,"商品キャンセルに失敗しました");
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"商品キャンセルの消去終了");
}
function cancel_prod2()
{
    write_log(SQL_LOG,"商品キャンセル開始");
    $sql="SELECT ep.fk_end_products_id,ep.f_last_bidder from auction.t_end_products ep,auction.t_member_master mm
         where ep.f_last_bidder = mm.fk_member_id and mm.f_activity < 10 and ep.f_end_time < DATE_SUB(now(),interval ".PRODUCT_PAYLIMIT_DAY." DAY )";
    $db=DB_CONNECT();
    $result=mysql_query($sql,$db);
    DB_CLOSE($db);
    //echo $sql;
    $num=mysql_num_fields($result);
    for($i=0;$i<$num;$i++)
    {
        $ret=mysql_fetch_array($result);
        $pid=$ret['fk_end_products_id'];
        $mid=$ret['f_last_bidder'];
        $sql="select f_cancel_num from auction.t_member where fk_member_id = $mid";
        //echo $sql."\n";
        $db=DB_CONNECT();
        $mem_result=mysql_query($sql,$db);
        DB_CLOSE($db);
        $ret2=mysql_fetch_array($mem_result);

        if($ret2['f_cancel_num'] >= PROD_CANCEL_NUM && PROD_CANCEL_NUM != 0)
        {
            continue;
        }

        $sql_mem="update auction.t_member set f_cancel_num = f_cancel_num + 1 where fk_member_id = $mid";
        $sql_prod="UPDATE auction.t_end_products SET f_status = 1 WHERE fk_end_products_id = $pid";

        $db=DB_CONNECT();
        if(mysql_query($sql_mem,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(PROC_LOG,"商品キャンセルに失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            continue;
        }
        if(mysql_query($sql_prod,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(PROC_LOG,"商品キャンセルに失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            continue;
        }
        DB_CLOSE($db);
    }
    write_log(SQL_LOG,"商品キャンセルの消去終了");
}
function del_req()
{
    write_log(SQL_LOG,"リクエスト削除");
    $sql=" delete from auction.t_reqhist where DATE_ADD(f_updatetm,interval f_timelimit second ) < now()";
    $db=DB_CONNECT();
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        write_log(SQL_LOG,"リクエスト削除に失敗しました");
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    DB_CLOSE($db);
    write_log(SQL_LOG,"リクエスト削除の消去終了");
}
function del_kari()
{
    $dat=date('YmdHis');
    $sql="SELECT mm.fk_member_id, mm.f_login_id,mm.f_login_pass,mm.f_handle,mm.f_ser_no,mm.f_user_agent,mm.f_name as mem_name,mm.f_tel_no,mm.f_sex,
            mm.f_birthday,mm.f_ip,mm.f_mail_address_pc,mm.f_mail_address_mb,ad.f_adcode,mm.f_regist_pos,mm.f_regist_date,ar.f_name as area_name, job.f_name as job_name,mm.fk_adcode_id
            FROM auction.t_member_master mm,auction.t_adcode ad,auction.t_member_area ar,auction.t_member_job job
            WHERE mm.fk_parent_ad=ad.fk_adcode_id AND mm.fk_job_id=job.fk_job_id AND mm.fk_area_id = ar.fk_area_id and f_activity = 0 and f_delete_flag = 0 and mm.f_tm_stamp < date_sub($dat , interval ".TEMP_REGIST_LIMIT." SECOND)";
    echo $sql;
    $db=DB_CONNECT();
    $rs_main=mysql_query($sql,$db);

    $num_main = mysql_num_rows($rs_main);
    DB_CLOSE($db);

    for($i=0;$i<$num_main;$i++)
    {
        $ret=mysql_fetch_array($rs_main);
        $id=$ret['fk_member_id'];
        $logid =$ret['f_login_id'];
        $logposs=$ret['f_login_pass'];
        $handle=$ret['f_handle'];
        $ser=$ret['f_ser_no'];
        $ua=$ret['f_user_agent'];
        $mem_name =$ret['mem_name'];
        $tel=$ret['f_tel_no'];
        $sex=$ret['f_sex'];
        $birth=$ret['f_birthday'];
        $ip=$ret['f_ip'];
        
        $mail="";
        if($ret['f_mail_address_pc'] !="")
        {
            $mail = $ret['f_mail_address_pc'];
        }
        else if ($ret['f_mail_address_mb'] !="")
        {
            $mail = $ret['f_mail_address_mb'];
        }

        $par=$ret['f_adcode'];
        $pos=$ret['f_regist_pos'];
        $reg=$ret['f_regist_date'];
        $area=$ret['area_name'];
        $job=$ret['job_name'];
        $str="$id,$logid,$logposs,$handle,$ser,$ua,$mem_name,$tel,$sex,$birth,$ip,$mail,$par,$pos,$reg,$area,$job";
        $str .="\n";
        WriteLog($str);

        $db=DB_CONNECT();
        $sql="DELETE FROM auction.t_adcode where t_adcode.fk_adcode_id =".$ret['fk_adcode_id'];
        echo $sql;
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"リクエスト削除に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        $sql="delete from auction.t_member where fk_member_id =$id";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"リクエスト削除に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }

        $sql ="delete from auction.t_member_master where f_activity = 0 and f_delete_flag = 0 and fk_member_id =$id";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"リクエスト削除に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
    }
}
function clear()
{
    $sql ="UPDATE auction.t_member_master mm,auction.t_products p SET mm.f_temp1 =0 WHERE  mm.f_temp1 = p.fk_products_id AND (p.f_status = 2 or p.f_status=9 )";
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        write_log(SQL_LOG,"リクエスト削除に失敗しました");
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    mysql_query("COMMIT",$db);
}

function setEndProd()
{
    $sql = "SELECT p.fk_products_id FROM auction.t_products p ";
    $sql .="WHERE NOT EXISTS ";
    $sql .="(SELECT p.fk_products_id FROM auction.t_end_products e ";
    $sql .="WHERE e.fk_end_products_id = p.fk_products_id) ";
    $sql .="AND  p.f_status = 2 ";
//    echo $sql."\n";
    $db=DB_CONNECT();
    $rs_main=mysql_query($sql,$db);

    $num_main = mysql_num_rows($rs_main);

    if($num_main == 0)
    {
        DB_CLOSE($db);
    }

    for($i=0;$i<$num_main;$i++)
    {
        $ret_main=mysql_fetch_array($rs_main);
        $sql ="SELECT pm.fk_products_id,pm.f_products_name,pm.fk_item_category_id,
               pm.f_photo1,pm.f_photo2,pm.f_photo3,pm.f_photo4,pm.f_photo5,pm.f_photo1mb,pm.f_photo2mb,
               pm.f_photo3mb,pm.f_photo4mb,pm.f_photo5mb,pm.f_top_description_pc,pm.f_main_description_pc,
               pm.f_top_description_mb,pm.f_main_description_mb,pm.f_start_price,p.f_now_price,pm.f_market_price,
               pm.f_start_time,pm.fk_auction_type_id,p.f_bit_buy_count,p.f_bit_free_count,p.f_bit_r_count,
               pm.f_target_hard,p.f_last_bidder,pm.f_address_flag ,p.f_tm_stamp,pm.f_min_price ";
        $sql.="FROM auction.t_products_master pm,auction.t_products p WHERE pm.fk_products_id=p.fk_products_id AND p.fk_products_id=".$ret_main['fk_products_id'];

  //      echo $sql."\n";

        $rs=mysql_query($sql,$db);
        $num = mysql_num_rows($rs);
        if($num==0)
        {
            continue;
        }
        $ret=mysql_fetch_array($rs);
        $fk_end_products_id=$ret['fk_products_id'];
        $f_products_name=$ret['f_products_name'];
        $fk_item_category_id=$ret['fk_item_category_id'];
        $f_photo1 = $ret['f_photo1'];
        $f_photo2 = $ret['f_photo2'];
        $f_photo3 = $ret['f_photo3'];
        $f_photo4 = $ret['f_photo4'];
        $f_photo5 = $ret['f_photo5'];
        $f_photo1mb=$ret['f_photo1mb'];
        $f_photo2mb=$ret['f_photo2mb'];
        $f_photo3mb=$ret['f_photo3mb'];
        $f_photo4mb=$ret['f_photo4mb'];
        $f_photo5mb=$ret['f_photo5mb'];
        
        $f_top_description_pc=str_replace("'", "\'", $ret['f_top_description_pc']);
        $f_main_description_pc=str_replace("'", "\'", $ret['f_main_description_pc']);
        $f_top_description_mb=str_replace("'", "\'", $ret['f_top_description_mb']);
        $f_main_description_mb=str_replace("'", "\'", $ret['f_main_description_mb']);
        //$f_main_description_pc=$ret['f_main_description_pc'];
        //$f_top_description_mb=$ret['f_top_description_mb'];
        //$f_main_description_mb=$ret['f_main_description_mb'];

        $f_start_price=$ret['f_start_price'];
        $f_now_price=$ret['f_now_price'];
        $f_market_price=$ret['f_market_price'];
        $f_start_time=$ret['f_start_time'];
        $f_tm_stamp=$ret['f_tm_stamp'];
        $fk_auction_type_id=$ret['fk_auction_type_id'];
        $f_bit_buy_count=$ret['f_bit_buy_count'];
        $f_bit_free_count=$ret['f_bit_free_count'];
        $f_bit_r_count=$ret['f_bit_r_count'];
        $f_target_hard=$ret['f_target_hard'];
        $f_last_bidder=$ret['f_last_bidder'];
        $f_address_flag=$ret['f_address_flag'];
        $f_min_price=$ret['f_min_price'];
        $sql ="INSERT INTO auction.t_end_products
            (fk_end_products_id, f_products_name, fk_item_category_id,
            f_photo1, f_photo2, f_photo3, f_photo4, f_photo5,
            f_photo1mb,f_photo2mb, f_photo3mb, f_photo4mb, f_photo5mb,
            f_top_description_pc,f_main_description_pc, f_top_description_mb, f_main_description_mb,
            f_start_price, f_end_price, f_market_price,
            f_start_time, f_end_time,
            fk_auction_type_id, f_bit_buy_count, f_bit_free_count, f_bit_r_count,f_target_hard,
            f_last_bidder,f_address_flag,f_min_price)
            VALUES(
            $fk_end_products_id,'$f_products_name', $fk_item_category_id,
            '$f_photo1', '$f_photo2', '$f_photo3', '$f_photo4', '$f_photo5',
            '$f_photo1mb', '$f_photo2mb', '$f_photo3mb', '$f_photo4mb', '$f_photo5mb',
            '$f_top_description_pc', '$f_main_description_pc', '$f_top_description_mb', '$f_main_description_mb',
            $f_start_price, $f_now_price, $f_market_price,
            '$f_start_time', '$f_tm_stamp',
            $fk_auction_type_id, $f_bit_buy_count,$f_bit_free_count,$f_bit_r_count,$f_target_hard,$f_last_bidder,$f_address_flag,$f_min_price)";
    //        echo $sql."\n";
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"終了商品追加に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        $sql ="UPDATE auction.t_member_master set f_temp1 = 0 where f_temp1 =$fk_end_products_id";
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"終了商品追加に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
    }
}
function reset_prod()
{
        $sql ="update auction.t_products set  f_status=1 where f_status=7";
        $db=DB_CONNECT();
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
}
function alm_mail()
{
    $sql ="select p.fk_products_id from auction.t_products p,auction.t_products_master pm where p.fk_products_id = pm.fk_products_id and p.f_status = 1
        and p.f_now_price > pm.f_market_price and p.f_alm_mail = 0";
    //echo $sql;
    $db=DB_CONNECT();
    $rs_main=mysql_query($sql,$db);

    $num_main = mysql_num_rows($rs_main);
    DB_CLOSE($db);
    $body="現在価格が市場価格を超えました";
    $send_from ="From: ".INFO_MAIL ."\nReturn-Path:".INFO_MAIL;
    //$send_from ="From: shoji@ark-com.jpReturn-Path:shoji@ark-com.jp";
    $send_to ="auction@ark-com.jp";
    
    for($i=0;$i<$num_main;$i++)
    {
        $ret=mysql_fetch_array($rs_main);
        $prod_id=$ret['fk_products_id'];
        $subject = SITE_NAME.":商品ID:$prod_id";
        $ins_sql = "insert into auction.t_mail_request (f_type,f_subject,f_body,f_sendfrom,f_sendto,f_status)
	values(30,'$subject','$body','$send_from','$send_to',0)";
        $up_sql ="update auction.t_products set f_alm_mail =1 where fk_products_id =$prod_id";

        $db=DB_CONNECT();
        mysql_query("BEGIN",$db);
        if(mysql_query($ins_sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $ins_sql);
            write_log(SQL_LOG,"警告送信失敗");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        if(mysql_query($up_sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $ins_sql);
            write_log(SQL_LOG,"警告送信失敗");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
        DB_CLOSE($db);
    }
    
}
//$count=0;
//$start=time();
//$end=time();

//while($count < 50)
//{
//    if($end-$start > 55)
//    {
//        return;
//    }
//    close_auto();
//    close_auto2();
//    close_watch();
//    cancel_prod();
//    del_req();
//    del_kari();
//    cancel_prod2();
//    setEndProd();
//    reset_prod();
    clear();
    //alm_mail();
    sleep(1);
//    $count++;
//}
?>
