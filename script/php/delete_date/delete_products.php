<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//include '/home/auction/script/php/delete_date/common.php';

//商品削除
//入札ログ

//入札ログ削除
function delete_bid_log()
{
    $sql = "delete from auction.t_bid_log where f_products_id in(select fk_products_id from auction.t_products_master where f_delflg =9  and  f_tm_stamp < date_sub(now(), interval ".FLAG_PROD_DEL." day))";
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
    return;
}
function delete_products()
{
    $sql = "delete from auction.t_products where fk_products_id in(select fk_products_id from auction.t_products_master where f_delflg =9  and  f_tm_stamp < date_sub(now(), interval ".FLAG_PROD_DEL." day))";
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
    return;
}
function delete_products_master()
{
    $sql = "delete from auction.t_products_master where f_delflg =9  and  f_tm_stamp < date_sub(now(), interval ".FLAG_PROD_DEL." day)";
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
    return;
}

function delete_end_products()
{
    $sql = "update t_member set f_last_bid_item_id = null where f_last_bid_item_id in(select fk_products_id from auction.t_products_master where f_delflg =9  and  f_tm_stamp < date_sub(now(), interval ".FLAG_PROD_DEL." day)) ";
    $db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    $sql = "delete from auction.t_end_products where fk_end_products_id in(select fk_products_id from auction.t_products_master where f_delflg =9  and  f_tm_stamp < date_sub(now(), interval ".FLAG_PROD_DEL." day)) ";
    //$db=DB_CONNECT();
    mysql_query("BEGIN",$db);
    if(mysql_query($sql,$db)==FALSE)
    {
        write_log(SQL_LOG, $sql);
        mysql_query("ROLLBACK",$db);
        DB_CLOSE($db);
        return;
    }
    mysql_query("COMMIT",$db);
    DB_CLOSE($db);
    return;
}
function setEndProd()
{
    $sql = "SELECT p.fk_products_id FROM auction.t_products p ";
    $sql .="WHERE NOT EXISTS ";
    $sql .="(SELECT p.fk_products_id FROM auction.t_end_products e ";
    $sql .="WHERE e.fk_end_products_id = p.fk_products_id) ";
    $sql .="AND  p.f_status = 2 ";
//    echo $sql."\n";
    $db=DB_CONNECT();
    $rs_main=mysql_query($sql,$db);

    $num_main = mysql_num_rows($rs_main);

    if($num_main == 0)
    {
        DB_CLOSE($db);
    }

    for($i=0;$i<$num_main;$i++)
    {
        $ret_main=mysql_fetch_array($rs_main);
        $sql ="SELECT pm.fk_products_id,pm.f_products_name,pm.fk_item_category_id,
               pm.f_photo1,pm.f_photo2,pm.f_photo3,pm.f_photo4,pm.f_photo5,pm.f_photo1mb,pm.f_photo2mb,
               pm.f_photo3mb,pm.f_photo4mb,pm.f_photo5mb,pm.f_top_description_pc,pm.f_main_description_pc,
               pm.f_top_description_mb,pm.f_main_description_mb,pm.f_start_price,p.f_now_price,pm.f_market_price,
               pm.f_start_time,pm.fk_auction_type_id,p.f_bit_buy_count,p.f_bit_free_count,
               pm.f_target_hard,p.f_last_bidder,pm.f_address_flag ,p.f_tm_stamp,pm.f_min_price ";
        $sql.="FROM auction.t_products_master pm,auction.t_products p WHERE pm.fk_products_id=p.fk_products_id AND p.fk_products_id=".$ret_main['fk_products_id'];

  //      echo $sql."\n";

        $rs=mysql_query($sql,$db);
        $num = mysql_num_rows($rs);
        if($num==0)
        {
            continue;
        }
        $ret=mysql_fetch_array($rs);
        $fk_end_products_id=$ret['fk_products_id'];
        $f_products_name=$ret['f_products_name'];
        $fk_item_category_id=$ret['fk_item_category_id'];
        $f_photo1 = $ret['f_photo1'];
        $f_photo2 = $ret['f_photo2'];
        $f_photo3 = $ret['f_photo3'];
        $f_photo4 = $ret['f_photo4'];
        $f_photo5 = $ret['f_photo5'];
        $f_photo1mb=$ret['f_photo1mb'];
        $f_photo2mb=$ret['f_photo2mb'];
        $f_photo3mb=$ret['f_photo3mb'];
        $f_photo4mb=$ret['f_photo4mb'];
        $f_photo5mb=$ret['f_photo5mb'];

        $f_top_description_pc=str_replace("'", "\'", $ret['f_top_description_pc']);
        $f_main_description_pc=str_replace("'", "\'", $ret['f_main_description_pc']);
        $f_top_description_mb=str_replace("'", "\'", $ret['f_top_description_mb']);
        $f_main_description_mb=str_replace("'", "\'", $ret['f_main_description_mb']);
        //$f_main_description_pc=$ret['f_main_description_pc'];
        //$f_top_description_mb=$ret['f_top_description_mb'];
        //$f_main_description_mb=$ret['f_main_description_mb'];

        $f_start_price=$ret['f_start_price'];
        $f_now_price=$ret['f_now_price'];
        $f_market_price=$ret['f_market_price'];
        $f_start_time=$ret['f_start_time'];
        $f_tm_stamp=$ret['f_tm_stamp'];
        $fk_auction_type_id=$ret['fk_auction_type_id'];
        $f_bit_buy_count=$ret['f_bit_buy_count'];
        $f_bit_free_count=$ret['f_bit_free_count'];
        $f_target_hard=$ret['f_target_hard'];
        $f_last_bidder=$ret['f_last_bidder'];
        $f_address_flag=$ret['f_address_flag'];
        $f_min_price=$ret['f_min_price'];
        $sql ="INSERT INTO auction.t_end_products
            (fk_end_products_id, f_products_name, fk_item_category_id,
            f_photo1, f_photo2, f_photo3, f_photo4, f_photo5,
            f_photo1mb,f_photo2mb, f_photo3mb, f_photo4mb, f_photo5mb,
            f_top_description_pc,f_main_description_pc, f_top_description_mb, f_main_description_mb,
            f_start_price, f_end_price, f_market_price,
            f_start_time, f_end_time,
            fk_auction_type_id, f_bit_buy_count, f_bit_free_count, f_target_hard,
            f_last_bidder,f_address_flag,f_min_price)
            VALUES(
            $fk_end_products_id,'$f_products_name', $fk_item_category_id,
            '$f_photo1', '$f_photo2', '$f_photo3', '$f_photo4', '$f_photo5',
            '$f_photo1mb', '$f_photo2mb', '$f_photo3mb', '$f_photo4mb', '$f_photo5mb',
            '$f_top_description_pc', '$f_main_description_pc', '$f_top_description_mb', '$f_main_description_mb',
            $f_start_price, $f_now_price, $f_market_price,
            '$f_start_time', '$f_tm_stamp',
            $fk_auction_type_id, $f_bit_buy_count,$f_bit_free_count,$f_target_hard,$f_last_bidder,$f_address_flag,$f_min_price)";
    //        echo $sql."\n";
        mysql_query("BEGIN",$db);
        if(mysql_query($sql,$db)==FALSE)
        {
            write_log(SQL_LOG, $sql);
            write_log(SQL_LOG,"終了商品追加に失敗しました");
            mysql_query("ROLLBACK",$db);
            DB_CLOSE($db);
            return;
        }
        mysql_query("COMMIT",$db);
    }
}
?>
