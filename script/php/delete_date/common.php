<?php
include '/home/auction/lib/define.php';
include '/home/auction/lib/server.php';
define("SQL_LOG","/home/auction/log/batch_sql.log");
define("PROC_LOG","/home/auction/log/batch_proc.log");

function DB_CONNECT()
{
    //MYSQL DBへの接続データの取得
    $db = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
    mysql_query("SET NAMES utf8",$db);
    return $db;
}

function DB_CLOSE($db)
{
    mysql_close($db);
}

function write_log($file,$str)
{
    $now_time = date("Y/m/d H:i:s");
    $log_date= $now_time ."  ".$str."\n";

    $fp = fopen($file, "a+");
    flock($fp, LOCK_EX);
    fputs($fp,$log_date);
    flock($fp, LOCK_UN);
    fclose($fp);
}
?>
