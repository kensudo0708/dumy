<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//商品削除(デリートフラグON)
//入札ログ
//商品マスターからの削除
Class DELETE_PRODUCT
{
    function delete_flag_prod()
    {
        $ret ="SELECT pm.fk_products_id FROM
                auction.t_products_master pm
                WHERE pm.f_delflg = 9 and 
                pm.f_tm_stamp < DATE_SUB(NOW() ,INTERVAL ". FLAG_PROD_DEL. " DAY)";
        return $ret;
    }
    //入札ログ削除用SQL生成
    public function delete_bidlog_sql($prod_id)
    {
        $sql ="delete from auction.t_bid_log where f_products_id =$prod_id";
        return $sql;
    }

    //最終入札消去用SQL生成
    public function UpDate_lastbid_sql($prod_id)
    {
        $sql = "Update auction.t_member set f_last_bid_item_id = NULL where f_last_bid_item_id =$prod_id";
        return $sql;
    }

    public function delete_endprod_sql($prod_id)
    {
        $sql="delete from auction.t_end_products where fk_end_products_id =$prod_id";
        return $sql;
    }

    public function delete_prod_sql($prod_id)
    {
        $sql ="delete from auction.t_products where fk_products_id =$prod_id ";
        return $sql;
    }

    public function delete_prodmaster_sql($prod_id)
    {
        $sql ="delete from auction.t_products_master where fk_products_id =$prod_id ";
        return $sql;
    }
}

Class DELETE_END_PRODUCT
{
    function delete_flag_prod()
    {
        $ret ="SELECT pm.fk_end_products_id FROM
                auction.t_end_products pm
                WHERE pm.f_status = 9 and
                pm.f_tm_stamp < DATE_SUB(NOW() ,INTERVAL ". FLAG_PROD_DEL. " DAY)";
        return $ret;
    }
    //入札ログ削除用SQL生成
    public function delete_bidlog_sql($prod_id)
    {
        $sql ="delete from auction.t_bid_log where f_products_id =$prod_id";
        return $sql;
    }

    //最終入札消去用SQL生成
    public function UpDate_lastbid_sql($prod_id)
    {
        $sql = "Update auction.t_member set f_last_bid_item_id = NULL where f_last_bid_item_id =$prod_id";
        return $sql;
    }

    public function delete_endprod_sql($prod_id)
    {
        $sql="delete from auction.t_end_products where fk_end_products_id =$prod_id";
        return $sql;
    }

    public function delete_prod_sql($prod_id)
    {
        $sql ="delete from auction.t_products where fk_products_id =$prod_id ";
        return $sql;
    }

    public function delete_prodmaster_sql($prod_id)
    {
        $sql ="delete from auction.t_products_master where fk_products_id =$prod_id ";
        return $sql;
    }
}

?>
