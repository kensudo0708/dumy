#!/bin/sh

while [ 1 ]
do

	DATE1=""
	DATE2=""

	DATE1=`tail -n 1 /home/auction/script/command/log/main.log`
	echo $DATE1 >>/home/auction/script/command/log/check_process.log

	sleep 2

	DATE2=`tail -n 1 /home/auction/script/command/log/main.log`
	echo $DATE2 >> /home/auction/script/command/log/check_process.log

	PROCESS=0
	if [ "$DATE1" ==  "$DATE2" ]
	then
        	kill `/bin/ps -eaf | /bin/grep 1_sec | /bin/grep -v grep | /bin/awk '{print $2}'`
        	php -f /home/auction/script/php/email/warm_mail.php
	fi
	sleep 3
done

