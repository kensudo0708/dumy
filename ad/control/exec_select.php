<?php
include 'DB_connect.php';
function get_row_num($rs)
{
    return mysql_num_rows($rs);
}
function get_result($rs)
{
    return mysql_fetch_array($rs);
}


function exec_query($sql,$db)
{
    $rs=mysql_query($sql,$db);
    return $rs;
}

//スタッフログイン情報取得
function exec_login($id,$pass)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    //SQL文生成
    $sql = 'select fk_admaster_id from auction.t_admaster where f_login_id = \''.$id .'\' and f_login_pass =\'' .$pass . '\'';

    //SQL文実行
    $rs = mysql_query($sql,$db);
    
    if(get_row_num($rs) == 0)
    {
        //該当スタッフなし
        return -1;
    }

    db_close($db);

    $ret = mysql_fetch_array($rs);
    return $ret['fk_admaster_id'];
}

//管理画面のメニュー用
function get_menu_category($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    //SQL文生成
	$sql  = "SELECT ac.fk_authority_category_id AS cat_id  ";
	$sql .= "FROM auction.t_authority_category ac,auction.t_authority a,auction.t_staff_authority sa ";
	$sql .= "WHERE sa.fk_staff_id=" . $id ." ";
	$sql .= "and ac.fk_authority_category_id = a.fk_authority_category_id ";
	$sql .= "and a.fk_authority_id = sa.fk_authority_id ";
	$sql .= "GROUP BY ac.fk_authority_category_id ";
	$sql .= "ORDER BY ac.fk_authority_category_id ASC ";

    //SQL文実行
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        return -1;
    }

    db_close($db);
    return $rs;
}

//各管理画面生成用処理
function get_menu($staff_id,$cate_id,$op_level)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    //SQL文生成
        $sql = "SELECT sa.fk_authority_id as au_id,a.f_authority_name as au_name FROM auction.t_authority a , auction.t_staff_authority sa
WHERE sa.fk_staff_id=". $staff_id . " AND a.fk_authority_id = sa.fk_authority_id AND a.fk_authority_category_id=" .$cate_id;

    //SQL文実行
    $rs = mysql_query($sql,$db);
    if(get_row_num($rs) == 0)
    {
        //なし　
        return -1;
    }

    db_close($db);
    return $rs;
}

//デザインテンプレートリスト取得
function get_edit_page_list($term,$category)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    //SQL文生成
    $sql = "select p.fk_page_id as id, p.f_page_uri as uri ,p.f_importance_level as lev,p.f_explanation AS exp ,f_open_flag as of from auction.t_page p where p.f_term_category=".$term . " and p.f_category=".$category." order by id" ;

    //SQL文実行
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        return -1;
    }

    db_close($db);
    return $rs;
}

//編集対象テンプレート取得
function get_edit_page($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    //SQL文生成
    $sql = "select p.f_page_uri as uri ,p.f_term_category as term,p.f_category as cat,p.f_explanation AS exp from auction.t_page p where p.fk_page_id=".$id;

    //SQL文実行
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) != 1 )
    {
        //なし　
        return -1;
    }

    db_close($db);
    return $rs;
}

//都道府県情報取得
function get_pref()
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql = "select * from auction.t_pref";

    //SQL文実行
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        db_close($db);
        return -1;
    }
    db_close($db);
    return $rs;
}

//会員リスト表示
function get_mem_disp($mail,$ad,$sex,$r_start,$r_end,$act,$group,$b_coin_min,$b_coin_max,$f_coin_min,$f_coin_max,$pref)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql = "SELECT mm.fk_member_id AS id,mm.f_session AS ses,mm.f_login_id AS l_id,mm.f_login_pass AS l_pass,mm.f_handle AS han,mm.f_mail_address_pc AS mail_pc,mm.f_mail_address_mb AS mail_mb, mm.f_regist_data AS reg ,m.f_free_coin f_c,m.f_coin c,mg.f_member_group_name mgn,mm.f_name NAME,ROUND((TO_DAYS(NOW()) - TO_DAYS(f_birthday))/365) AS ages,p.t_pref_name AS p_name,mm.f_sex AS sex
FROM auction.t_member_master mm ,auction.t_member m,auction.t_member_group mg,auction.t_address ad,auction.t_pref p
WHERE mm.fk_member_id = m.fk_member_id AND mm.fk_member_group_id = mg.fk_member_group_id
AND mm.f_delete_flag=0 ";

    //入力値の確認
    if($mail != "")
    {
        $sql .= "and (mm.f_mail_address_pc ='". $mail ."' || mm.f_mail_address_mb ='" . $mail ."') ";
    }
    if($sex != -1)
    {
        $sql .= "and mm.f_sex=".$sex." ";
    }
    if(mb_strlen($r_start)==8 && mb_strlen($r_end)==8 && $r_start < $r_end)
    {
        $sql .= "and mm.f_regist_data between ".$r_start ." and ".$r_end." ";
    }
    if($act != -1)
    {
        $sql .= "and mm.f_activity=".$act ." ";
    }
    if($group !=-1)
    {
        $sql .="and mm.fk_member_group_id = ".$group ." ";
    }
    if($b_coin_min != -1 && $b_coin_max != -1 && $b_coin_min < $b_coin_max)
    {
        $sql.="and m.f_coin between ".$b_coin_min." and ".$b_coin_max. " ";
    }
    if($f_coin_min != -1 && $f_coin_max != -1 && $f_coin_min < $f_coin_max)
    {
        $sql.="and m.f_free_coin between ".$f_coin_min." and ".$f_coin_max. " ";
    }
    if($pref != -1)
    {
        $sql.="and ad.fk_perf_id = ".$pref ;
    }
    //SQL文実行
    print($sql);
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし
        db_close($db);
        return -1;
    }
    db_close($db);
    return $rs;
}

//メンバーグループ取得
function get_mem_group($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql="SELECT  mg.fk_member_group_id mgi,mg.f_member_group_name mgn ,
        cg.f_coin_group_name cgn ,cg.fk_coin_group_id cgi,mg.f_memo memo FROM auction.t_member_group mg
        ,auction.t_coin_group cg";
    if($id != -1)
    {
        $sql .= " WHERE mg.fk_member_group_id=" . $id;
    }
    
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        db_close($db);
        return -1;
    }
    db_close($db);
    return $rs;
}

//メンバーグループに属する会員人数取得
function get_mem_group_count($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql="SELECT count(*) FROM auction.t_member_master mm
        where mm.fk_member_group_id =".$id;

    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        db_close($db);
        return -1;
    }
    db_close($db);
    return $rs;
}

//コイングループ取得
function get_coin_group($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql="SELECT  cg.fk_coin_group_id cgi,cg.f_coin_group_name cgn ,
        cg.f_memo memo
        FROM auction.t_coin_group cg";
    if($id != -1)
    {
        $sql .= " WHERE cg.fk_coin_group_id=" . $id;
    }

    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        db_close($db);
        return -1;
    }
    db_close($db);
    return $rs;
}

function get_sysparam($cat,$staff)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT sys.f_sno as id,sys.f_paramname as NAME,sys.f_type as type,f_value as val,sys.f_category as cat,
        sys.f_comment com,sys.f_maint_flg main,sys.f_chgstaff_flg staff
        FROM auction.t_sysparam sys ";

    $tmp="";
    if($cat !="-1")
    {
        $tmp=" sys.f_category IN (2,".$cat.")";
    }
    if($staff =="0")
    {
        if($tmp !="")
        {
            $tmp .=" and";
        }
        $tmp .=" f_chgstaff_flg=1";
    }
    if($tmp !="")
    {
        $sql .= "Where ".$tmp;
    }
    $rs = mysql_query($sql,$db);

    if(get_row_num($rs) == 0)
    {
        //なし　
        db_close($db);
        return -1;
    }
    db_close($db);
    return $rs;
}

//NEXT　ID取得
function get_next_id($sql)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);
    db_close($db);
    return $rs;
    
}

//ブラックメールアドレス取得
function get_draw_mail($mail,$status,$base,$id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="select fk_black_member_mail_id as id,f_mail_address as mail,f_status as stat,f_base as base from auction.t_black_member_mail ";

    $tmp_sql="";
    if($mail !="")
    {
        $tmp_sql.="f_mail_address ='".$mail."' ";
    }
    if($status != -1)
    {
        if($tmp_sql !="")
        {
            $tmp_sql .= "and ";
        }
        $tmp_sql.="f_status =".$status." ";
    }
    if($base != -1)
    {
        if($tmp_sql !="")
        {
            $tmp_sql .= "and ";
        }
        $tmp_sql.="f_base =".$base;
    }
    if($tmp_sql !="")
    {
        $tmp_sql="WHERE ".$tmp_sql;
    }
    if($id != -1)
    {
        $tmp_sql = "WHERE fk_black_member_mail_id=".$id;
    }
    $sql = $sql.$tmp_sql;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

//ブラック端末取得
function get_draw_ser($ser,$status,$base,$id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="select fk_black_member_ser_id as id,f_ser as ser,f_status as stat,f_base as base from auction.t_black_member_ser ";

    $tmp_sql="";
    if($ser !="")
    {
        $tmp_sql.="f_ser ='".$ser."' ";
    }
    if($status != -1)
    {
        if($tmp_sql !="")
        {
            $tmp_sql .= "and ";
        }
        $tmp_sql.="f_status =".$status." ";
    }
    if($base != -1)
    {
        if($tmp_sql !="")
        {
            $tmp_sql .= "and ";
        }
        $tmp_sql.="f_base =".$base;
    }
    if($tmp_sql !="")
    {
        $tmp_sql="WHERE ".$tmp_sql;
    }
    if($id != -1)
    {
        $tmp_sql = "WHERE fk_black_member_ser_id=".$id;
    }
    $sql = $sql.$tmp_sql;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_coin_setup($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql="SELECT cs.fk_coin_id id,cs.f_coin AS coin ,cs.f_inp_money AS im ,fk_shiharai_type_id AS sti FROM auction.t_coin_setup cs ";

    if($id !="-1")
    {
        $sql .= " WHERE cs.fk_coin_group_id =".$id;
    }
    
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_count($sql)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $rs = mysql_query($sql,$db);

    $ret=get_result($rs);
    db_close($db);
    return $ret["count"];
}
function get_shiharai_type()
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT DISTINCT sp.f_shiharai_name AS NAME ,sp.f_statistics_no FROM auction.t_shiharai_type st , auction.t_shiharai_plan sp WHERE st.fk_shiharai_id=sp.fk_shiharai_id" ;
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_stat_sales($type,$start,$end)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql="SELECT DATE_FORMAT(f_stat_dt,'%Y/%m/%d %H:00') AS dat, DATE_FORMAT(f_stat_dt,'%Y%m%d') AS dat2,DAYOFWEEK(f_stat_dt) AS dayn,
    f_memreg, f_buycoin, f_prd_lastprice, f_prd_cost,f_sale_total,
    f_buycoin1, f_buycoin2, f_buycoin3, f_buycoin4, f_buycoin5, f_buycoin6, f_buycoin7, f_buycoin8,
    f_buycoin9, f_buycoin10, f_buycoin11, f_buycoin12, f_buycoin13, f_buycoin14, f_buycoin15, f_buycoin16,
    f_buycoin17, f_buycoin18, f_buycoin19, f_buycoin20  from auction.t_stat_sales where f_type=".$type." and f_stat_dt between ".$start." and ".$end;
    //echo $sql."\n";
    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_sales_log($start,$end)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT DATE_FORMAT(pl.f_tm_stamp,' %H:%i') AS dat,mm.fk_member_id AS m_id,mm.f_handle handle,
    DATE_FORMAT(mm.f_regist_date,'%Y/%m/%d') reg_d,pl.f_status stat,sp.f_shiharai_name s_name,
    pl.f_pay_money pay,sa.f_name a_name
    FROM auction.t_pay_log pl,auction.t_member_master mm,auction.t_shiharai_plan sp,auction.t_shiharai_agent sa,auction.t_shiharai_type st
    WHERE pl.f_member_id=mm.fk_member_id
    AND pl.fk_shiharai_type_id =st.fk_shiharai_type_id
    AND st.fk_shiharai_agent_id = sa.fk_shiharai_agent_id
    AND st.fk_shiharai_id=sp.fk_shiharai_id
    AND pl.`f_cert_status`=0
    AND pl.f_tm_stamp BETWEEN ".$start." AND ".$end;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
function get_sal_log_goukei($start,$end)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT sum(pl.f_pay_money) pay
    FROM auction.t_pay_log pl,auction.t_member_master mm,auction.t_shirarai_plan sp,auction.t_siharai_agent sa,auction.t_shirarai_type st
    WHERE pl.f_member_id=mm.fk_member_id
    AND pl.fk_shiharai_type_id =st.fk_shiharai_type_id
    AND st.fk_shiharai_agent_id = sa.fk_shiharai_agent_id
    AND st.fk_shiharai_id=sp.fk_shiharai_id
    AND pl.`f_cert_status`=0
    AND pl.f_tm_stamp BETWEEN ".$start." AND ".$end;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_sal_goukei($type,$start,$end)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT SUM(f_memreg) as reg,
SUM(f_buycoin) AS buycoin,SUM(f_prd_lastprice) AS prd_lastprice,
SUM(f_prd_cost) AS prd_cost,SUM(f_sale_total) AS sale_total,
SUM(f_buycoin1) AS buycoin1,SUM(f_buycoin2) AS buycoin2,SUM(f_buycoin3) AS buycoin3,SUM(f_buycoin4) AS buycoin4,
SUM(f_buycoin5) AS buycoin5,SUM(f_buycoin6) AS buycoin6,SUM(f_buycoin7) AS buycoin7,SUM(f_buycoin8) AS buycoin8,
SUM(f_buycoin9) AS buycoin9,SUM(f_buycoin10) AS buycoin10,SUM(f_buycoin11) AS buycoin11,SUM(f_buycoin12) AS buycoin12,
SUM(f_buycoin13) AS buycoin13,SUM(f_buycoin14) AS buycoin14,SUM(f_buycoin15) AS buycoin15,SUM(f_buycoin16) AS buycoin16,
SUM(f_buycoin17) AS buycoin17,SUM(f_buycoin18) AS buycoin18,SUM(f_buycoin19) AS buycoin19,SUM(f_buycoin20) AS buycoin20
FROM auction.t_stat_sales where f_type=".$type." and f_stat_dt between ".$start." and ".$end;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_auction_type()
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="select * from auction.t_auction_type order by f_statistics_no asc";

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}

function get_sal_point($type,$start,$end)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT DATE_FORMAT(f_stat_dt,'%Y/%m/%d %H:00') AS dat, DATE_FORMAT(f_stat_dt,'%Y%m%d') AS dat2,DAYOFWEEK(f_stat_dt) AS dayn,
    f_coin_gen+f_scoin_gen total_gen,f_coin_gen,f_scoin_gen,
    f_coin_use+f_scoin_use total_use,f_coin_use,f_scoin_use,
    f_bid_hand,f_bid_hand2,f_bid_auto,
    f_auctype1,f_auctype2,f_auctype3,f_auctype4,
    f_auctype5,f_auctype6,f_auctype7,f_auctype8,
    f_auctype9,f_auctype10,f_auctype11,f_auctype12,
    f_auctype13,f_auctype14,f_auctype15,f_auctype16,
    f_auctype17,f_auctype18,f_auctype19,f_auctype20,
    f_sauctype1,f_sauctype2,f_sauctype3,f_sauctype4,
    f_sauctype5,f_sauctype6,f_sauctype7,f_sauctype8,
    f_sauctype9,f_sauctype10,f_sauctype11,f_sauctype12,
    f_sauctype13,f_sauctype14,f_sauctype15,f_sauctype16,
    f_sauctype17,f_sauctype18,f_sauctype19,f_sauctype20,
    	 f_auctype1+f_sauctype1  as totalbid1,
	 f_auctype2+f_sauctype2  as totalbid2,
	 f_auctype3+f_sauctype3  as totalbid3,
	 f_auctype4+f_sauctype4  as totalbid4,
	 f_auctype5+f_sauctype5  as totalbid5,
	 f_auctype6+f_sauctype6  as totalbid6,
	 f_auctype7+f_sauctype7  as totalbid7,
	 f_auctype8+f_sauctype8  as totalbid8,
	 f_auctype9+f_sauctype9  as totalbid9,
	f_auctype10+f_sauctype10 as totalbid10,
	f_auctype11+f_sauctype11 as totalbid11,
	f_auctype12+f_sauctype12 as totalbid12,
	f_auctype13+f_sauctype13 as totalbid13,
	f_auctype14+f_sauctype14 as totalbid14,
	f_auctype15+f_sauctype15 as totalbid15,
	f_auctype16+f_sauctype16 as totalbid16,
	f_auctype17+f_sauctype17 as totalbid17,
	f_auctype18+f_sauctype18 as totalbid18,
	f_auctype19+f_sauctype19 as totalbid19,
	f_auctype20+f_sauctype20 as totalbid20
    from auction.t_stat_sales where f_type=".$type." and f_stat_dt between ".$start." and ".$end;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
function get_sal_point_goukei($type,$start,$end)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="SELECT
        SUM(f_coin_gen+f_scoin_gen) total_gen,
        SUM(f_coin_gen) f_coin_gen,
SUM(f_scoin_gen) f_scoin_gen ,
SUM(f_coin_use+f_scoin_use) total_use,
SUM(f_coin_use) f_coin_use,
SUM(f_scoin_use) f_scoin_use,
SUM(f_bid_hand) f_bid_hand,
SUM(f_bid_hand2) f_bid_hand2,
SUM(f_bid_auto) f_bid_auto,
SUM(f_auctype1) f_auctype1,
SUM(f_auctype2) f_auctype2,
SUM(f_auctype3) f_auctype3,
SUM(f_auctype4) f_auctype4,
SUM(f_auctype5) f_auctype5,
SUM(f_auctype6) f_auctype6,
SUM(f_auctype7) f_auctype7,
SUM(f_auctype8) f_auctype8,
SUM(f_auctype9) f_auctype9,
SUM(f_auctype10) f_auctype10,
SUM(f_auctype11) f_auctype11,
SUM(f_auctype12) f_auctype12,
SUM(f_auctype13) f_auctype13,
SUM(f_auctype14) f_auctype14,
SUM(f_auctype15) f_auctype15,
SUM(f_auctype16) f_auctype16,
SUM(f_auctype17) f_auctype17,
SUM(f_auctype18) f_auctype18,
SUM(f_auctype19) f_auctype19,
SUM(f_auctype20) f_auctype20,
SUM(f_sauctype1) f_sauctype1,
SUM(f_sauctype2) f_sauctype2,
SUM(f_sauctype3) f_sauctype3,
SUM(f_sauctype4) f_sauctype4,
SUM(f_sauctype5) f_sauctype5,
SUM(f_sauctype6) f_sauctype6,
SUM(f_sauctype7) f_sauctype7,
SUM(f_sauctype8) f_sauctype8,
SUM(f_sauctype9) f_sauctype9,
SUM(f_sauctype10) f_sauctype10,
SUM(f_sauctype11) f_sauctype11,
SUM(f_sauctype12) f_sauctype12,
SUM(f_sauctype13) f_sauctype13,
SUM(f_sauctype14) f_sauctype14,
SUM(f_sauctype15) f_sauctype15,
SUM(f_sauctype16) f_sauctype16,
SUM(f_sauctype17) f_sauctype17,
SUM(f_sauctype18) f_sauctype18,
SUM(f_sauctype19) f_sauctype19,
SUM(f_sauctype20) f_sauctype20,
SUM(f_auctype1+f_sauctype1)	as	totalbid1,
SUM(f_auctype2+f_sauctype2)	as	totalbid2,
SUM(f_auctype3+f_sauctype3)	as	totalbid3,
SUM(f_auctype4+f_sauctype4)	as	totalbid4,
SUM(f_auctype5+f_sauctype5)	as	totalbid5,
SUM(f_auctype6+f_sauctype6)	as	totalbid6,
SUM(f_auctype7+f_sauctype7)	as	totalbid7,
SUM(f_auctype8+f_sauctype8)	as	totalbid8,
SUM(f_auctype9+f_sauctype9)	as	totalbid9,
SUM(f_auctype10+f_sauctype10)	as	totalbid10,
SUM(f_auctype11+f_sauctype11)	as	totalbid11,
SUM(f_auctype12+f_sauctype12)	as	totalbid12,
SUM(f_auctype13+f_sauctype13)	as	totalbid13,
SUM(f_auctype14+f_sauctype14)	as	totalbid14,
SUM(f_auctype15+f_sauctype15)	as	totalbid15,
SUM(f_auctype16+f_sauctype16)	as	totalbid16,
SUM(f_auctype17+f_sauctype17)	as	totalbid17,
SUM(f_auctype18+f_sauctype18)	as	totalbid18,
SUM(f_auctype19+f_sauctype19)	as	totalbid19,
SUM(f_auctype20+f_sauctype20)	as	totalbid20
    FROM auction.t_stat_sales where f_type=".$type." and f_stat_dt between ".$start." and ".$end;

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
function get_mainte_flag()
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }
    $sql="select f_mainte_flg from auction.t_system";

    $rs = mysql_query($sql,$db);

    db_close($db);
    return $rs;
}
?>
