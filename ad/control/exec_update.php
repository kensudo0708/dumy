<?php
include 'DB_connect.php';

//編集中にする
function edit_page($id,$flag)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $sql = 'update auction.t_page set f_open_flag ='.$flag.' where fk_page_id='.$id;

    //SQL文実行
    $rs = mysql_query($sql,$db);
    if($rs ==FALSE)
    {
        mysql_query("rollback",$db);
        return 0;
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}

//メンバーグループ情報変更
function edit_mem_group($id,$name,$coin_id,$memo)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $sql = "update auction.t_member_group set f_member_group_name='".$name."' ,fk_coin_group_id=".$coin_id ." ,
        f_memo='".$memo."' where fk_member_group_id=".$id;
    //SQL文実行
    $rs = mysql_query($sql,$db);
    if($rs ==FALSE)
    {
        mysql_query("rollback",$db);
        return 0;
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}

//ブラックメールアドレス情報変更
function edit_draw_mail($id,$mail,$stat,$base)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $sql = "update auction.t_black_member_mail set f_mail_address='".$mail."' ,f_base=".$base ." ,
        f_status='".$stat."' where fk_black_member_mail_id=".$id;
    //SQL文実行
    $rs = mysql_query($sql,$db);
    if($rs ==FALSE)
    {
        mysql_query("rollback",$db);
        return 0;
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}

function exec_del_mem_group_update($id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    $sql = "update auction.t_member_master set fk_member_group_id=1 where fk_member_group_id=".$id;
    $rs = mysql_query($sql,$db);
    if($rs ==FALSE)
    {
        mysql_query("rollback",$db);
        return 0;
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}

function update_coin_group($id,$name,$memo)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $sql = "UPDATE auction.t_coin_group set f_coin_group_name='$name',f_memo='$memo' ,f_tm_stamp=now() where fk_coin_group_id=".$id;
    //SQL文実行
    $rs = mysql_query($sql,$db);
    if($rs ==FALSE)
    {
        mysql_query("rollback",$db);
        return 0;
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}

function update_coin_setup($id,$im,$coin)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $cnt=count($id);

    for($i=0;$i<$cnt;$i++)
    {
        $sql = "UPDATE auction.t_coin_setup set f_coin=".$coin[$i]." ,f_inp_money=".$im[$i] ."
             WHERE fk_coin_id=".$id[$i];
    //SQL文実行
        $rs = mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return FALSE;
        }
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}

function update_sysparam($name,$type,$val,$cat,$com,$main,$staff,$id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $cnt=count($name);

    for($i=0;$i<$cnt;$i++)
    {
        $tmp="UPDATE auction.t_sysparam SET f_paramname = '%s' ,f_type = %d ,
	f_value = '%s'  ,f_category = %d ,f_comment = '%s' ,f_maint_flg = %d ,
	f_chgstaff_flg = %d WHERE f_sno = %d";
        $sql =sprintf($tmp,$name[$i],$type[$i],$val[$i],$cat[$i],$com[$i],$main[$i],$staff[$i],$id[$i]);
    //SQL文実行
        $rs = mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return FALSE;
        }
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}
function update_sysparam2($val,$com,$id)
{
    $db=db_connect();
    if($db == false)
    {
        exit;
    }

    mysql_query("begin",$db);

    $cnt=count($id);
    for($i=0;$i<$cnt;$i++)
    {
        $tmp="UPDATE auction.t_sysparam SET f_value = '%s'  ,f_comment = '%s'
            WHERE f_sno = %d";
        $sql =sprintf($tmp,$val[$i],$com[$i],$id[$i]);

    //SQL文実行
        $rs = mysql_query($sql,$db);
        if($rs ==FALSE)
        {
            mysql_query("rollback",$db);
            return FALSE;
        }
    }
    mysql_query("commit",$db);
    db_close($db);
    return 0;
}
?>
