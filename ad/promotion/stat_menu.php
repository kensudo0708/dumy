<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$f_login_id = $_SESSION["f_login_id"];
	$f_login_pass = $_SESSION["f_login_pass"];
	
	//▼現在日付
	$now = time();
	$yesterday = $now - 86400;
	$inp_s_date = date("Ym01", $now);
	$inp_e_date = date("Ymd", $now);
	$inp_s_month = date("Ym", $now);
	$inp_e_month = date("Ym", $now);
	
	//▼広告主選択作成
	//ID-PASS指定広告主名一覧取得関数
	$order_str = "order by k_adname";
	SrcIdPssGetFAdNameList($order_str,$f_login_id,$f_login_pass,$inp_admaster_id, $inp_admaster_name, $inp_admaster_cnt);
	$name = "inp_ad";
	$select_num = "";
	//選択オブジェクト作成関数
	MakeSelectObject($name, $inp_admaster_id, $inp_admaster_name, $inp_admaster_cnt, $select_num, $admaster_select);
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<table class='main_l'>\n";
	$dsp_tbl .= "<caption>&#x2460;広告効果測定</caption>\n";
	
	$dsp_tbl .= "<form name='form' enctype='multipart/form-data' action='stat_list.php' method='GET' target='main_r'>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='radio' name='inp_type' value=1 checked>日別集計(yyyymmdd指定)";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "開始<input type='text' name='inp_s_date' size='10' value='$inp_s_date'><br>\n";
	$dsp_tbl .= "終了<input type='text' name='inp_e_date' size='10' value='$inp_e_date'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "<input type='radio' name='inp_type' value=2>月別集計(yyyymm指定)";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "開始<input type='text' name='inp_s_month' size='10' value='$inp_s_month'><br>\n";
	$dsp_tbl .= "終了<input type='text' name='inp_e_month' size='10' value='$inp_e_month'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th align='center'><tt>登録媒体</tt></th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td>\n";
	$dsp_tbl .= "$admaster_select";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<td align='center' colspan=2>\n";
	$dsp_tbl .= "<input type='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' value='会員検索'>\n";
	$dsp_tbl .= "</td>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</form>\n";
	$dsp_tbl .= "</table>\n";
	
	//管理画面広告主入力ページ表示関数
	PrintAdminAdInputPage($dsp_tbl);
?>
