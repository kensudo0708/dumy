<script language="JavaScript">
<!--
	function OpenWin(url)
	{
		myWin = window.open(url,"_blank","menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,scrollbars=yes,resizable=yes,width=800,height=600");
	}
-->
</script>
<style TYPE="text/css">
tr,td ,th {font-size: 9pt;}
</style>
<?php
	//☆★	ライブラリ読込み	★☆
	include "../../lib/define.php";
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;
	
	session_start();
	$sid = $_SESSION["staff_id"];
	
	$inp_type = $_REQUEST["inp_type"];
	$inp_ad = $_REQUEST["inp_ad"];
	
	$inp_s_date = $_REQUEST["inp_s_date"];
	$inp_e_date = $_REQUEST["inp_e_date"];
	
	$inp_s_month = $_REQUEST["inp_s_month"];
	$inp_e_month = $_REQUEST["inp_e_month"];
	
	//▼入力判定
	if($inp_type == 0)
	{
		//日付文字列作成関数
		MakeDateString($inp_s_date, $start_date);
		MakeDateString($inp_e_date, $end_date);
		$inp_start = $start_date." 00:00:00";
		$inp_end = $end_date." 23:59:59";
		
		$s_year = substr($inp_s_date, 0, 4);
		$s_month = substr($inp_s_date, 4, 2);
		$s_day = substr($inp_s_date, 6, 2);
		$e_year = substr($inp_e_date, 0, 4);
		$e_month = substr($inp_e_date, 4, 2);
		$e_day = substr($inp_e_date, 6, 2);
		$src_term = "集計期間(".$s_year."年".$s_month."月".$s_day."日 00時 ～ 23時)";
	}
	else if($inp_type == 1)
	{
		$error_msg = "";
		if($inp_s_date == "")
		{
			$error_msg .= "開始日が入力されていません。<br>\n";
		}
		if($inp_e_date == "")
		{
			$error_msg .= "終了日が入力されていません。<br>\n";
		}
		
		if($error_msg != "")
		{
			//管理画面入力ページ表示関数
			PrintAdminPage("広告効果測定",$error_msg);
			exit;
		}
		else
		{
			//日付文字列作成関数
			MakeDateString($inp_s_date, $start_date);
			MakeDateString($inp_e_date, $end_date);
			$inp_start = $start_date." 00:00:00";
			$inp_end = $end_date." 23:59:59";
		}
		
		$s_year = substr($inp_s_date, 0, 4);
		$s_month = substr($inp_s_date, 4, 2);
		$s_day = substr($inp_s_date, 6, 2);
		$e_year = substr($inp_e_date, 0, 4);
		$e_month = substr($inp_e_date, 4, 2);
		$e_day = substr($inp_e_date, 6, 2);
		$src_term = "集計期間(".$s_year."年".$s_month."月".$s_day."日 ～ ".$e_year."年".$e_month."月".$e_day."日)";
	}
	else if($inp_type == 2)
	{
		$error_msg = "";
		if($inp_s_month == "")
		{
			$error_msg .= "開始月が入力されていません。<br>\n";
		}
		if($inp_e_month == "")
		{
			$error_msg .= "終了月が入力されていません。<br>\n";
		}
		
		if($error_msg != "")
		{
			//管理画面入力ページ表示関数
			PrintAdminPage("広告効果測定",$error_msg);
			exit;
		}
		else
		{
			$s_year = substr($inp_s_month, 0, 4);
			$s_month = substr($inp_s_month, 4, 2);
			$e_year = substr($inp_e_month, 0, 4);
			$e_month = substr($inp_e_month, 4, 2);
			//月末日取得関数
			$limit_day = GetMonthlimit($e_year, $e_month);
			$inp_start = $s_year."-".$s_month."-01 00:00:00";
			$inp_end = $e_year."-".$e_month."-".$limit_day." 23:59:59";
		}
	}
	
	//広告集計合計取得関数
	GetTStatAdSum($inp_start,$inp_end,$inp_type,$inp_ad,$tsa_f_stat_dt,$tsa_f_pc_memreg,$tsa_f_mb_memreg,$tsa_f_pc_access,$tsa_f_mb_access,$tsa_f_pc_paycoin,$tsa_f_mb_paycoin,$tsa_data_cnt,$tas_f_pc_prod,$tas_f_mb_prod,$tas_f_pc_coinpack,$tas_f_mb_coinpack,$pc_reg_woman,$mb_reg_woman);
	
	//広告主情報取得関数
	GetTAdmasterInfo($inp_ad,$tam_k_adname,$tam_f_agent_name,$tam_f_tel_no,$tam_f_mail_address,$tam_f_login_id,$tam_f_login_pass,$tam_f_kickback_url,$tam_f_pay_type,$tam_f_pay_value,$tam_f_memo,$tam_f_counter,$tam_f_tm_stamp);
	
//G.Chin 2010-07-27 add sta
	//広告主表示情報取得関数
	GetTAdmasterDispInfo($inp_ad,$tam_f_disp_hour,$tam_f_disp_pay,$tam_f_disp_today);
//G.Chin 2010-07-27 add end
	
	$dsp_tbl  = "";
	$dsp_tbl .= "<table class='data'>\n";
	$dsp_tbl .= "<tr>\n";
	$dsp_tbl .= "<th>データ件数</th>\n";
	$dsp_tbl .= "<td>$tsa_data_cnt</th>\n";
	$dsp_tbl .= "</tr>\n";
	$dsp_tbl .= "</table>\n<br>\n";
	$dsp_tbl .= "<br><br>\n";
	
	//▼購入表示フラグ判定
	if($tam_f_disp_pay == 0)
	{
		$dsp_tbl .= "<table class='list' style='width:1100px'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan=4><NOBR><tt>日付</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan=12><NOBR><tt>広告($tam_k_adname)</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th colspan=4 style='background:#80FFFF'><NOBR><tt>PC</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan=4 style='background:#FFFF80'><NOBR><tt>MB</tt></NOBR></th>\n";
        $dsp_tbl .= "<th colspan=4><NOBR><tt>合計</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｱｸｾｽ数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan='2'><NOBR><tt>登録数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｺｲﾝ購入金額</tt></NOBR></th>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｱｸｾｽ数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan='2'><NOBR><tt>登録数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｺｲﾝ購入金額</tt></NOBR></th>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｱｸｾｽ数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan='2'><NOBR><tt>登録数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｺｲﾝ購入金額</tt></NOBR></th>\n";
        $dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th><NOBR><tt>男</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>女</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>男</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>女</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>男</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>女</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
	}
	else
	{
		$dsp_tbl .= "<table border='1' bordercolor='#BDBDBD' cellspacing='1' cellpadding='1' width='100%'>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan=4><NOBR><tt>日付</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan=9><NOBR><tt>広告($tam_k_adname)</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th colspan=3 style='background:#80FFFF'><NOBR><tt>PC</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan=3 style='background:#FFFF80'><NOBR><tt>MB</tt></NOBR></th>\n";
        $dsp_tbl .= "<th colspan=3><NOBR><tt>合計</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
		
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｱｸｾｽ数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan='2'><NOBR><tt>登録数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｱｸｾｽ数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan='2'><NOBR><tt>登録数</tt></NOBR></th>\n";
        $dsp_tbl .= "<th rowspan='2'><NOBR><tt>ｱｸｾｽ数</tt></NOBR></th>\n";
		$dsp_tbl .= "<th colspan='2'><NOBR><tt>登録数</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<th><NOBR><tt>男</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>女</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>男</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>女</tt></NOBR></th>\n";
        $dsp_tbl .= "<th><NOBR><tt>男</tt></NOBR></th>\n";
		$dsp_tbl .= "<th><NOBR><tt>女</tt></NOBR></th>\n";
		$dsp_tbl .= "</tr>\n";
	}
//G.Chin 2010-07-27 chg end
	
	//▼現在日
	$now = time();
	$today = date("Y-m-d", $now);
	
	$ttl_pc_access = 0;
	$ttl_pc_memreg = 0;
	$ttl_pc_paycoin = 0;
	$ttl_mb_access = 0;
	$ttl_mb_memreg = 0;
	$ttl_mb_paycoin = 0;
	
	$ttl_f_pc_buy_prod = 0;
	$ttl_f_mb_buy_prod = 0;
	$ttl_f_pc_buy_coin = 0;
	$ttl_f_mb_buy_coin = 0;
	$ttl_pc_memreg_woman = 0;
	$ttl_mb_memreg_woman = 0;
	
	//取得データより表示データを１件ずつ取り出す
	for($i=0;$i<$tsa_data_cnt;$i++)
	{
		//▼入力判定
		if($inp_type == 0)		//時間別
		{
			//▼日付
			$hour = substr($tsa_f_stat_dt[$i], 11, 5);
			$link_date = $hour."～";
		}
		else if($inp_type == 1)	//日別
		{
			//▼日付
			$src_date = substr($tsa_f_stat_dt[$i], 0, 10);
			$date_str = str_replace("-", "/", $src_date);
			$w_date = str_replace("-", "", $src_date);
			//曜日文字列生成関数
			MakeWeekDayStr($src_date, $weekstr);
			$date_str = $date_str."(".$weekstr.")";
			//▼時刻表示フラグ判定
			if($tam_f_disp_hour == 0)
			{
				$link_date = "<A href='stat_list.php?inp_type=0&inp_s_date=$w_date&inp_e_date=$w_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad'>$date_str</A>";
			}
			else
			{
				$link_date = $date_str;
			}
		}
		else if($inp_type == 2)	//月別
		{
			//▼日付
			$src_date = substr($tsa_f_stat_dt[$i], 0, 7);
			$date_str = str_replace("-", "/", $src_date);
			$w_date = str_replace("-", "", $src_date);
			$w_year = substr($w_date, 0, 4);
			$w_month = substr($w_date, 5, 2);
			//月末日取得関数
			$w_limit_day = GetMonthlimit($w_year, $w_month);
			$w_s_date = $w_date."01";
			$w_e_date = $w_date.$w_limit_day;
			$link_date = "<A href='stat_list.php?inp_type=1&inp_s_date=$w_s_date&inp_e_date=$w_e_date&inp_s_month=$inp_s_month&inp_e_month=$inp_e_month&inp_ad=$inp_ad'>$date_str</A>";
		}
		
		//▼当日表示フラグ判定
		if($tam_f_disp_today == 1)
		{
			//※．当日は非表示
			if($inp_type != 0)
			{
				if($src_date == $today)
				{
					break;
				}
			}
		}
		
		//▼購入表示フラグ判定
		if($tam_f_disp_pay == 0)
		{
                        $sum_acc=$tsa_f_pc_access[$i]+$tsa_f_mb_access[$i];
                        $sum_reg_mem = $tsa_f_mb_memreg[$i] +$tsa_f_pc_memreg[$i];
                        $sum_reg_womam = $mb_reg_woman[$i] + $pc_reg_woman[$i];
                        $sum_pay = $tsa_f_mb_paycoin[$i] + $tsa_f_pc_paycoin[$i];
                        
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<td align='center'><NOBR><tt>$link_date</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_pc_access[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_pc_memreg[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$pc_reg_woman[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_pc_paycoin[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_mb_access[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_mb_memreg[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$mb_reg_woman[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_mb_paycoin[$i]</tt></NOBR></td>\n";
            $dsp_tbl .= "<td align='right'><NOBR><tt>$sum_acc</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_reg_mem</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_reg_womam</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_pay</tt></NOBR></td>\n";
			$dsp_tbl .= "</tr>\n";
		}
		else
		{
                        $sum_acc=$tsa_f_pc_access[$i]+$tsa_f_mb_access[$i];
                        $sum_reg_mem = $tsa_f_mb_memreg[$i] +$tsa_f_pc_memreg[$i];
                        $sum_reg_womam = $mb_reg_woman[$i] + $pc_reg_woman[$i];
                        $sum_pay = $tsa_f_mb_paycoin[$i] + $tsa_f_pc_paycoin[$i];
			$dsp_tbl .= "<tr>\n";
			$dsp_tbl .= "<td align='center'><NOBR><tt>$link_date</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_pc_access[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_pc_memreg[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$pc_reg_woman[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_mb_access[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$tsa_f_mb_memreg[$i]</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$mb_reg_woman[$i]</tt></NOBR></td>\n";
            $dsp_tbl .= "<td align='right'><NOBR><tt>$sum_acc</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_reg_mem</tt></NOBR></td>\n";
			$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_reg_womam</tt></NOBR></td>\n";
			$dsp_tbl .= "</tr>\n";
		}
		
		//▼合計加算
		$ttl_pc_access = $ttl_pc_access + $tsa_f_pc_access[$i];
		$ttl_pc_memreg = $ttl_pc_memreg + $tsa_f_pc_memreg[$i];
		$ttl_pc_memreg_woman = $ttl_pc_memreg_woman + $pc_reg_woman[$i];
		$ttl_pc_paycoin = $ttl_pc_paycoin + $tsa_f_pc_paycoin[$i];
		$ttl_mb_access = $ttl_mb_access + $tsa_f_mb_access[$i];
		$ttl_mb_memreg = $ttl_mb_memreg + $tsa_f_mb_memreg[$i];
		$ttl_mb_memreg_woman = $ttl_mb_memreg_woman + $mb_reg_woman[$i];
		$ttl_mb_paycoin = $ttl_mb_paycoin + $tsa_f_mb_paycoin[$i];
		
		$ttl_f_pc_buy_prod = $ttl_f_pc_buy_prod+$tas_f_pc_prod[$i];
		$ttl_f_mb_buy_prod = $ttl_f_mb_buy_prod+$tas_f_mb_prod[$i];
		$ttl_f_pc_buy_coin = $ttl_f_pc_buy_coin+$tas_f_pc_coinpack[$i];
		$ttl_f_mb_buy_coin = $ttl_f_mb_buy_coin+$tas_f_mb_coinpack[$i];
	}
	
	//▼購入表示フラグ判定
	if($tam_f_disp_pay == 0)
	{
            $sum_acc=$ttl_pc_access + $ttl_mb_access;
            $sum_mem = $ttl_pc_memreg + $ttl_mb_memreg;
            $sum_woman = $ttl_pc_memreg_woman + $ttl_mb_memreg_woman;
            $sum_pay = $ttl_pc_paycoin + $ttl_mb_paycoin;
            
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>合計</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_access</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_memreg</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_memreg_woman</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_paycoin</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_access</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_memreg</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_memreg_woman</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_paycoin</tt></NOBR></td>\n";
        $dsp_tbl .= "<td align='right'><NOBR><tt>$sum_acc</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_mem</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_woman</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_pay</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	else
	{
            $sum_acc=$ttl_pc_access + $ttl_mb_access;
            $sum_mem = $ttl_pc_memreg + $ttl_mb_memreg;
            $sum_woman = $ttl_pc_memreg_woman + $ttl_mb_memreg_woman;
		$dsp_tbl .= "<tr>\n";
		$dsp_tbl .= "<td align='center'><NOBR><tt>合計</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_access</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_memreg</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_pc_memreg_woman</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_access</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_memreg</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$ttl_mb_memreg_woman</tt></NOBR></td>\n";
        $dsp_tbl .= "<td align='right'><NOBR><tt>$sum_acc</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_mem</tt></NOBR></td>\n";
		$dsp_tbl .= "<td align='right'><NOBR><tt>$sum_woman</tt></NOBR></td>\n";
		$dsp_tbl .= "</tr>\n";
	}
	$dsp_tbl .= "</table><br>\n";
	
	//管理画面広告主ページ表示関数
	PrintAdminAdPage("広告効果測定",$dsp_tbl);

?>
