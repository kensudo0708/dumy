<?php

/* アクション名のマッピングの設定、アクション名は小文字、大文字が区別しない */
return array(


"index"=>"index.IndexAction",
"login"=>"index.LoginAction",
"logout"=>"index.LogoutAction",
"product"=>"product.ProductAction",
"list"=>"product.ProductListAction",
"end"=>"product.ProductEndAction",
"category"=>"product.ProductCategoryAction",
"member"=>"member.MemberAction",
"my"=>"member.MyAction",
"myproduct"=>"member.MyProductAction",
"mypay"=>"member.MyFinishAction",
"coin"=>"member.CoinAction",
"file"=>"index.FileUploadAction",
"guide"=>"GuideAction",
"free"=>"FreeAction",
"pact"=>"pact.PactAction",
"thanks"=>"index.ThanksAction",
//2010-04-08 Kaz Tanaka
//決済ﾘﾀﾞｲﾚｸﾀへのﾏｯﾋﾟﾝｸﾞ
"mbpay"=>"commonview.MBPayment",
"pcpay"=>"commonview.PCPayment",

//IPSからの入金通知受信ﾏｯﾋﾟﾝｸﾞ
"ipscredit"=>"server.IPSCredit",
"ipsebank"=>"server.IPSEbank",
"ipsedy"=>"server.IPSEdy",
"webmoney"=>"server.WebMoney",
//TELECOMからの入金通知受信ﾏｯﾋﾟﾝｸﾞ
"telecredit"=>"server.TelecomCredit",
//ZEROからの入金通知受信ﾏｯﾋﾟﾝｸﾞ
"zerocredit"=>"server.ZeroCredit",
//APROSからの入金通知受信ﾏｯﾋﾟﾝｸﾞ
"apros"=>"server.PSApros",
//ZEROからの入金通知受信ﾏｯﾋﾟﾝｸﾞ
//
//
//
//indexページゲームﾏｯﾋﾟﾝｸﾞ
"game"=>"game.GameAction",
//"login"=>"classes.action.index.LoginAction",
//"ajax"=>"classes.action.index.AjaxAction",

// モバイル版Flash
// 商品詳細
"product_flash"=>"product.ProductFlashAction",
// Ajax通信
"ajax"=>"index.AjaxAction",

// ダイレクト会員登録
"regist"=>"member.MemberDirectRegistAction",

//"auctionstart"=>"classes.action.admin.AuctionAction"

/*        管理画面       */
//"admin"=>"admin.IndexAction",
);
?>
