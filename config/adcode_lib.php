<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/19												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 広告関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主名一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//AREQ-77 G.Chin 2010-08-05 chg sta
//	function GetFAdNameList(&$fk_admaster_id, &$k_adname, &$data_cnt)
	function GetFAdNameList($order_str, &$fk_admaster_id, &$k_adname, &$data_cnt)
//AREQ-77 G.Chin 2010-08-05 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
//AREQ-77 G.Chin 2010-08-05 add sta
		if($order_str == "")
		{
			$order_str = "order by fk_admaster_id";
		}
//AREQ-77 G.Chin 2010-08-05 add end
		
		$sql = "select fk_admaster_id,k_adname from auction.t_admaster ";
		$sql .= "where f_delflg=0 ";
//AREQ-77 G.Chin 2010-08-05 chg sta
//		$sql .= "order by fk_admaster_id";
		$sql .= $order_str;
//AREQ-77 G.Chin 2010-08-05 chg end
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_admaster_id[$i]	= mysql_result($result, $i, "fk_admaster_id");
			$k_adname[$i]		= mysql_result($result, $i, "k_adname");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ID-PASS指定広告主名一覧取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//AREQ-77 G.Chin 2010-08-05 chg sta
//	function SrcIdPssGetFAdNameList($f_login_id,$f_login_pass,&$fk_admaster_id, &$k_adname, &$data_cnt)
	function SrcIdPssGetFAdNameList($order_str,$f_login_id,$f_login_pass,&$fk_admaster_id, &$k_adname, &$data_cnt)
//AREQ-77 G.Chin 2010-08-05 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
//AREQ-77 G.Chin 2010-08-05 add sta
		if($order_str == "")
		{
			$order_str = "order by fk_admaster_id";
		}
//AREQ-77 G.Chin 2010-08-05 add end
		
		$sql  = "select fk_admaster_id,k_adname from auction.t_admaster ";
		$sql .= "where f_login_id='$f_login_id' and f_login_pass='$f_login_pass' and f_delflg=0 ";
//AREQ-77 G.Chin 2010-08-05 chg sta
//		$sql .= "order by fk_admaster_id";
		$sql .= $order_str;
//AREQ-77 G.Chin 2010-08-05 chg end
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_admaster_id[$i]	= mysql_result($result, $i, "fk_admaster_id");
			$k_adname[$i]		= mysql_result($result, $i, "k_adname");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主一覧取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAdmasterList($limit,$offset,&$fk_admaster_id,&$k_adname,&$f_agent_name,&$f_tel_no,&$f_mail_address,&$f_login_id,&$f_login_pass,&$f_kickback_url,&$f_pay_type,&$f_pay_value,&$f_memo,&$f_counter,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql_cnt  = "select count(fk_admaster_id) from auction.t_admaster ";
		$sql_cnt .= "where fk_admaster_id>0 and f_delflg=0 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		$sql  = "select fk_admaster_id,k_adname,f_agent_name,f_tel_no,";
		$sql .= "f_mail_address,f_login_id,f_login_pass,f_kickback_url,";
		$sql .= "f_pay_type,f_pay_value,f_memo,f_counter,f_tm_stamp ";
		$sql .= "from auction.t_admaster ";
		$sql .= "where fk_admaster_id>0 and f_delflg=0 ";
		$sql .= "order by fk_admaster_id ";
		$sql .= "limit $limit offset $offset";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_admaster_id[$i]	= mysql_result($result, $i, "fk_admaster_id");
			$k_adname[$i]		= mysql_result($result, $i, "k_adname");
			$f_agent_name[$i]	= mysql_result($result, $i, "f_agent_name");
			$f_tel_no[$i]		= mysql_result($result, $i, "f_tel_no");
			$f_mail_address[$i]	= mysql_result($result, $i, "f_mail_address");
			$f_login_id[$i]		= mysql_result($result, $i, "f_login_id");
			$f_login_pass[$i]	= mysql_result($result, $i, "f_login_pass");
			$f_kickback_url[$i]	= mysql_result($result, $i, "f_kickback_url");
			$f_pay_type[$i]		= mysql_result($result, $i, "f_pay_type");
			$f_pay_value[$i]	= mysql_result($result, $i, "f_pay_value");
			$f_memo[$i]			= mysql_result($result, $i, "f_memo");
			$f_counter[$i]		= mysql_result($result, $i, "f_counter");
			$f_tm_stamp[$i]		= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主一覧取得関数II																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

//AREQ-77 G.Chin 2010-08-05 chg sta
/*
        function GetTAdmasterListII($limit,$offset,$sort,&$fk_admaster_id,&$k_adname,&$f_agent_name,&$f_tel_no,&$f_mail_address,&$f_login_id,&$f_login_pass,&$f_kickback_url,&$f_pay_type,&$f_pay_value,&$f_memo,&$f_counter,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }

		$sql_cnt  = "select count(*) from auction.t_admaster ";
		$sql_cnt .= "where fk_admaster_id>0 and f_delflg=0 ";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

                $sql  = "select ms.fk_admaster_id,ms.k_adname,ms.f_agent_name,ms.f_tel_no,";
		$sql .= "ms.f_mail_address,ms.f_login_id,ms.f_login_pass,ms.f_kickback_url,";
		$sql .= "ms.f_pay_type,ms.f_pay_value,ms.f_memo,ms.f_counter,ms.f_tm_stamp,ad.f_adcode ";
		$sql .= "from auction.t_admaster ms left outer join auction.t_adcode ad on ms.fk_admaster_id=ad.fk_admaster_id and ad.f_type=1 ";
		$sql .= "where ms.fk_admaster_id>0 and ms.f_delflg=0 ";
                if(empty ($sort) || ($sort!="desc" && $sort!="asc"))
                    $sql .= "order by ms.fk_admaster_id ";
                else
                {
                    $sql .= "order by ad.f_adcode $sort ";
                }
		$sql .= "limit $limit offset $offset";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_admaster_id[$i]	= mysql_result($result, $i, "fk_admaster_id");
			$k_adname[$i]		= mysql_result($result, $i, "k_adname");
			$f_agent_name[$i]	= mysql_result($result, $i, "f_agent_name");
			$f_tel_no[$i]		= mysql_result($result, $i, "f_tel_no");
			$f_mail_address[$i]	= mysql_result($result, $i, "f_mail_address");
			$f_login_id[$i]		= mysql_result($result, $i, "f_login_id");
			$f_login_pass[$i]	= mysql_result($result, $i, "f_login_pass");
			$f_kickback_url[$i]	= mysql_result($result, $i, "f_kickback_url");
			$f_pay_type[$i]		= mysql_result($result, $i, "f_pay_type");
			$f_pay_value[$i]	= mysql_result($result, $i, "f_pay_value");
			$f_memo[$i]			= mysql_result($result, $i, "f_memo");
			$f_counter[$i]		= mysql_result($result, $i, "f_counter");
			$f_tm_stamp[$i]		= mysql_result($result, $i, "f_tm_stamp");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}
*/
	function GetTAdmasterListII($limit,$offset,$sort,&$fk_admaster_id,&$k_adname,&$f_agent_name,&$f_tel_no,&$f_mail_address,&$f_login_id,&$f_login_pass,&$f_kickback_url,&$f_pay_type,&$f_pay_value,&$f_memo,&$f_counter,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
//G.Chin 2010-10-07 chg sta
/*
		$sql_cnt  = "select count(*) from auction.t_admaster ";
		$sql_cnt .= "where fk_admaster_id>0 and f_delflg=0 ";
*/
//                $sql_cnt  = "select count(ms.fk_admaster_id) ";
//		$sql_cnt .= "from auction.t_admaster ms left outer join auction.t_adcode ad on ms.fk_admaster_id=ad.fk_admaster_id and ad.f_type=1 ";
//		$sql_cnt .= "where ms.fk_admaster_id>0 and ms.f_delflg=0 ";
                //shoji 20101126 start
                $sql_cnt ="select count(ms.fk_admaster_id)
                    from (select fk_admaster_id from auction.t_admaster where fk_admaster_id>0 and f_delflg=0 ) ms left outer join
                    (select fk_admaster_id from auction.t_adcode where f_type=1 ) ad on ms.fk_admaster_id=ad.fk_admaster_id ";
                //end
//G.Chin 2010-10-07 chg end
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		//ソートキー
		switch($sort)
		{
			case 0:		$order_str = "order by ms.fk_admaster_id ";			break;
			case 1:		$order_str = "order by ms.fk_admaster_id desc ";	break;
			case 2:		$order_str = "order by ms.k_adname ";				break;
			case 3:		$order_str = "order by ms.k_adname desc ";			break;
			case 4:		$order_str = "order by ad.f_adcode ";				break;
			case 5:		$order_str = "order by ad.f_adcode desc ";			break;
			default:	$order_str = "order by ms.fk_admaster_id ";			break;
		}
		
//		$sql  = "select ms.fk_admaster_id,ms.k_adname,ms.f_agent_name,ms.f_tel_no,";
//		$sql .= "ms.f_mail_address,ms.f_login_id,ms.f_login_pass,ms.f_kickback_url,";
//		$sql .= "ms.f_pay_type,ms.f_pay_value,ms.f_memo,ms.f_counter,ms.f_tm_stamp,ad.f_adcode ";
//		$sql .= "from auction.t_admaster ms left outer join auction.t_adcode ad on ms.fk_admaster_id=ad.fk_admaster_id and ad.f_type=1 ";
//		$sql .= "where ms.fk_admaster_id>0 and ms.f_delflg=0 ";
                $sql ="SELECT ms.fk_admaster_id,ms.k_adname,ms.f_agent_name,ms.f_tel_no,ms.f_mail_address,ms.f_login_id,ms.f_login_pass,ms.f_kickback_url,ms.f_pay_type,ms.f_pay_value,ms.f_memo,ms.f_counter,ms.f_tm_stamp,ad.f_adcode
                        FROM auction.t_admaster ms LEFT OUTER JOIN
                        (SELECT f_adcode,fk_admaster_id FROM auction.t_adcode WHERE f_type=1  )ad ON ms.fk_admaster_id=ad.fk_admaster_id
                        WHERE ms.fk_admaster_id>0 AND ms.f_delflg=0 ";
                usleep(100000);
		$sql .= $order_str;
		$sql .= "limit $limit offset $offset";
		//echo $sql;
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_admaster_id[$i]	= mysql_result($result, $i, "fk_admaster_id");
			$k_adname[$i]		= mysql_result($result, $i, "k_adname");
			$f_agent_name[$i]	= mysql_result($result, $i, "f_agent_name");
			$f_tel_no[$i]		= mysql_result($result, $i, "f_tel_no");
			$f_mail_address[$i]	= mysql_result($result, $i, "f_mail_address");
			$f_login_id[$i]		= mysql_result($result, $i, "f_login_id");
			$f_login_pass[$i]	= mysql_result($result, $i, "f_login_pass");
			$f_kickback_url[$i]	= mysql_result($result, $i, "f_kickback_url");
			$f_pay_type[$i]		= mysql_result($result, $i, "f_pay_type");
			$f_pay_value[$i]	= mysql_result($result, $i, "f_pay_value");
			$f_memo[$i]			= mysql_result($result, $i, "f_memo");
			$f_counter[$i]		= mysql_result($result, $i, "f_counter");
			$f_tm_stamp[$i]		= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//AREQ-77 G.Chin 2010-08-05 chg end

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主情報取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAdmasterInfo($fk_admaster_id,&$k_adname,&$f_agent_name,&$f_tel_no,&$f_mail_address,&$f_login_id,&$f_login_pass,&$f_kickback_url,&$f_pay_type,&$f_pay_value,&$f_memo,&$f_counter,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select k_adname,f_agent_name,f_tel_no,";
		$sql .= "f_mail_address,f_login_id,f_login_pass,f_kickback_url,";
		$sql .= "f_pay_type,f_pay_value,f_memo,f_counter,f_tm_stamp ";
		$sql .= "from auction.t_admaster ";
		$sql .= "where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$k_adname		= mysql_result($result, 0, "k_adname");
			$f_agent_name	= mysql_result($result, 0, "f_agent_name");
			$f_tel_no		= mysql_result($result, 0, "f_tel_no");
			$f_mail_address	= mysql_result($result, 0, "f_mail_address");
			$f_login_id		= mysql_result($result, 0, "f_login_id");
			$f_login_pass	= mysql_result($result, 0, "f_login_pass");
			$f_kickback_url	= mysql_result($result, 0, "f_kickback_url");
			$f_pay_type		= mysql_result($result, 0, "f_pay_type");
			$f_pay_value	= mysql_result($result, 0, "f_pay_value");
			$f_memo			= mysql_result($result, 0, "f_memo");
			$f_counter		= mysql_result($result, 0, "f_counter");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$k_adname		= "";
			$f_agent_name	= "";
			$f_tel_no		= "";
			$f_mail_address	= "";
			$f_login_id		= "";
			$f_login_pass	= "";
			$f_kickback_url	= "";
			$f_pay_type		= "";
			$f_pay_value	= "";
			$f_memo			= "";
			$f_counter		= "";
			$f_tm_stamp		= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主登録関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTAdmaster($k_adname,$f_agent_name,$f_mail_address,$f_login_id,$f_login_pass,$f_pay_type,$f_pay_value,$f_memo,$f_adcode,$f_type,&$fk_admaster_id,$f_mem_reg_urls=NULL)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_admaster
//G.Chin 2010-07-27 chg sta
/*
		$sql  = "insert into auction.t_admaster ";
		$sql .= "(fk_admaster_id,k_adname,f_agent_name,f_mail_address,";
		$sql .= "f_login_id,f_login_pass,f_pay_type,f_pay_value,f_memo,f_tm_stamp) ";
		$sql .= "values (0,'$k_adname','$f_agent_name','$f_mail_address',";
		$sql .= "'$f_login_id','$f_login_pass','$f_pay_type','$f_pay_value','$f_memo',now()) ";
*/
		$sql  = "insert into auction.t_admaster ";
		$sql .= "(fk_admaster_id,k_adname,f_agent_name,f_mail_address,";
		$sql .= "f_login_id,f_login_pass,f_pay_type,f_pay_value,f_memo,f_tm_stamp,f_disp_hour,f_disp_pay,f_disp_today) ";
		$sql .= "values (0,'$k_adname','$f_agent_name','$f_mail_address',";
		$sql .= "'$f_login_id','$f_login_pass','$f_pay_type','$f_pay_value','$f_memo',now(),'0','0','0') ";
//G.Chin 2010-07-27 chg end
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//▼作成した広告主IDを取得
		$sql  = "select fk_admaster_id ";
		$sql .= "from auction.t_admaster ";
		$sql .= "where k_adname='$k_adname' and f_agent_name='$f_agent_name' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$num = $rows - 1;
			$fk_admaster_id = mysql_result($result, $num, "fk_admaster_id");
		}
		else
		{
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//t_adcode
		$sql  = "insert into auction.t_adcode ";
		$sql .= "(fk_adcode_id,f_adcode,fk_admaster_id,f_type,f_tm_stamp,f_mem_reg_urls) ";
		$sql .= "values (0,'$f_adcode','$fk_admaster_id','$f_type',now(),'$f_mem_reg_urls')";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主情報更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTAdmasterInfo($fk_admaster_id,$k_adname,$f_agent_name,$f_mail_address,$f_login_id,$f_login_pass,$f_pay_type,$f_pay_value,$f_memo,$f_adcode,$f_type,$f_mem_reg_urls=NULL)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_admaster
		$sql  = "update auction.t_admaster set ";
		$sql .= "k_adname='$k_adname',";
		$sql .= "f_agent_name='$f_agent_name',";
		$sql .= "f_mail_address='$f_mail_address',";
		$sql .= "f_mail_address='$f_mail_address',";
		$sql .= "f_login_id='$f_login_id',";
		$sql .= "f_login_pass='$f_login_pass',";
		$sql .= "f_pay_type='$f_pay_type',";
		$sql .= "f_pay_value='$f_pay_value',";
		$sql .= "f_memo='$f_memo' ";
		$sql .= "where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//t_adcode
		$sql  = "update auction.t_adcode set ";
		$sql .= "f_adcode='$f_adcode' ,f_mem_reg_urls='$f_mem_reg_urls' ";
		$sql .= "where fk_admaster_id='$fk_admaster_id' and f_type='$f_type' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主削除関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function DeleteTAdmaster($fk_admaster_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_admaster
		$sql = "update auction.t_admaster set f_delflg=1 where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//t_adcode
		$sql = "update auction.t_adcode set f_delflg=1 where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員本人広告コード登録関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistMemSelfTAdcode($f_adcode,&$fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_adcode
		$sql  = "insert into auction.t_adcode ";
		$sql .= "(fk_adcode_id,f_adcode,fk_admaster_id,f_type,f_tm_stamp) ";
		$sql .= "values (0,'$f_adcode','0','0',now()) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//▼作成した広告IDを取得
		$sql  = "select fk_adcode_id ";
		$sql .= "from auction.t_adcode ";
		$sql .= "where f_adcode='$f_adcode' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$fk_adcode_id = mysql_result($result, 0, "fk_adcode_id");
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告集計一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTStatAdList($inp_start,$inp_end,$inp_type,$inp_ad,&$f_stat_dt,&$f_type,&$f_pc_memreg,&$f_mb_memreg,&$f_pc_access,&$f_mb_access,&$f_pc_paycoin,&$f_mb_paycoin,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		if($inp_ad == "")
		{
			$where_admaster = "";
		}
		else
		{
			$where_admaster = "and fk_admaster_id='$inp_ad' ";
		}
		
		$sql  = "select f_stat_dt,f_type,f_pc_memreg,f_mb_memreg,";
		$sql .= "f_pc_access,f_mb_access,f_pc_paycoin,f_mb_paycoin ";
		$sql .= "from auction.t_stat_ad ";
		$sql .= "where f_type='$inp_type' and f_stat_dt>='$inp_start' and f_stat_dt<='$inp_end' ";
		$sql .= $where_admaster;
		$sql .= "order by f_stat_dt";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$f_stat_dt[$i]		= mysql_result($result, $i, "f_stat_dt");
			$f_type[$i]			= mysql_result($result, $i, "f_type");
			$f_pc_memreg[$i]	= mysql_result($result, $i, "f_pc_memreg");
			$f_mb_memreg[$i]	= mysql_result($result, $i, "f_mb_memreg");
			$f_pc_access[$i]	= mysql_result($result, $i, "f_pc_access");
			$f_mb_access[$i]	= mysql_result($result, $i, "f_mb_access");
			$f_pc_paycoin[$i]	= mysql_result($result, $i, "f_pc_paycoin");
			$f_mb_paycoin[$i]	= mysql_result($result, $i, "f_mb_paycoin");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告集計合計取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTStatAdSum($inp_start,$inp_end,$inp_type,$inp_ad,&$f_stat_dt,&$f_pc_memreg,&$f_mb_memreg,&$f_pc_access,&$f_mb_access,&$f_pc_paycoin,&$f_mb_paycoin,&$data_cnt,&$tas_f_pc_prod,&$tas_f_mb_prod,&$tas_f_pc_coinpack,&$tas_f_mb_coinpack,&$pc_reg_woman,&$mb_reg_woman)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		if($inp_ad == "")
		{
			$where_admaster = "";
		}
		else
		{
			$where_admaster = "and fk_admaster_id='$inp_ad' ";
		}
		
		$sql  = "select f_stat_dt,sum(f_pc_memreg),sum(f_mb_memreg),";
		$sql .= "sum(f_pc_access),sum(f_mb_access),sum(f_pc_paycoin),sum(f_mb_paycoin),sum(f_pc_buy_prod),sum(f_mb_buy_prod),sum(f_pc_buy_coin),sum(f_mb_buy_coin) ";
		$sql .= ",sum(f_pc_memreg_woman),sum(f_mb_memreg_woman) from auction.t_stat_ad ";
		$sql .= "where f_type='$inp_type' and f_stat_dt>='$inp_start' and f_stat_dt<='$inp_end' ";
		$sql .= $where_admaster;
		$sql .= "group by f_stat_dt ";
		$sql .= "order by f_stat_dt ";
		//echo $sql ."<br>\n";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$f_stat_dt[$i]		= mysql_result($result, $i, 0);
			$f_pc_memreg[$i]	= mysql_result($result, $i, 1);
			$f_mb_memreg[$i]	= mysql_result($result, $i, 2);
			$f_pc_access[$i]	= mysql_result($result, $i, 3);
			$f_mb_access[$i]	= mysql_result($result, $i, 4);
			$f_pc_paycoin[$i]	= mysql_result($result, $i, 5);
			$f_mb_paycoin[$i]	= mysql_result($result, $i, 6);
                        $tas_f_pc_prod[$i]      = mysql_result($result, $i, 7);
                        $tas_f_mb_prod[$i]      = mysql_result($result, $i, 8);
                        $tas_f_pc_coinpack[$i]  = mysql_result($result, $i, 9);
                        $tas_f_mb_coinpack[$i]  = mysql_result($result, $i, 10);
                        $pc_reg_woman[$i]  = mysql_result($result, $i, 11);
                        $mb_reg_woman[$i] =mysql_result($result, $i, 12);
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告集計広告主別合計取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTStatAdAdmasterSum($inp_start,$inp_end,$inp_type,$inp_ad,&$fk_admaster_id,&$f_pc_memreg,&$f_mb_memreg,&$f_pc_access,&$f_mb_access,&$f_pc_paycoin,&$f_mb_paycoin,&$data_cnt,&$tas_f_pc_prod,&$tas_f_mb_prod,&$tas_f_pc_coinpack,&$tas_f_mb_coinpack,&$pc_reg_woman,&$mb_reg_woman)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		
		$where_admaster = "";
		if($inp_ad != "")
		{
			$where_admaster = "and fk_admaster_id='$inp_ad' ";
		}
		
		$sql  = "select fk_admaster_id,sum(f_pc_memreg),sum(f_mb_memreg),";
		$sql .= "sum(f_pc_access),sum(f_mb_access),sum(f_pc_paycoin),sum(f_mb_paycoin),sum(f_pc_buy_prod),sum(f_mb_buy_prod),sum(f_pc_buy_coin),sum(f_mb_buy_coin) ";
		$sql .= ",sum(f_pc_memreg_woman),sum(f_mb_memreg_woman) from auction.t_stat_ad ";
		$sql .= "where f_type='$inp_type' and f_stat_dt>='$inp_start' and f_stat_dt<='$inp_end' ";
		$sql .= $where_admaster;
		$sql .= "group by fk_admaster_id ";
		$sql .= "order by fk_admaster_id ";
		//echo $sql ."<br>\n";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
//G.Chin 2010-10-07 add sta
		$num = 0;
		$del_cnt = 0;
//G.Chin 2010-10-07 add end
		
		for($i=0; $i<$data_cnt; $i++)
		{
//G.Chin 2010-10-07 chg sta
/*
			$fk_admaster_id[$i]		= mysql_result($result, $i, 0);
			$f_pc_memreg[$i]		= mysql_result($result, $i, 1);
			$f_mb_memreg[$i]		= mysql_result($result, $i, 2);
			$f_pc_access[$i]		= mysql_result($result, $i, 3);
			$f_mb_access[$i]		= mysql_result($result, $i, 4);
			$f_pc_paycoin[$i]		= mysql_result($result, $i, 5);
			$f_mb_paycoin[$i]		= mysql_result($result, $i, 6);
			$tas_f_pc_prod[$i]      = mysql_result($result, $i, 7);
			$tas_f_mb_prod[$i]      = mysql_result($result, $i, 8);
			$tas_f_pc_coinpack[$i]  = mysql_result($result, $i, 9);
			$tas_f_mb_coinpack[$i]  = mysql_result($result, $i, 10);
			$pc_reg_woman[$i]		= mysql_result($result, $i, 11);
			$mb_reg_woman[$i]		= mysql_result($result, $i, 12);
*/
			$w_admaster_id			= mysql_result($result, $i, 0);
			$sql  = "select f_delflg from auction.t_admaster ";
			$sql .= "where fk_admaster_id='$w_admaster_id' ";
			//echo $sql ."<br>\n";
			//■ＳＱＬ実行
			$res2 = mysql_query($sql, $db);
			if( $res2 == false )
			{
				print "$sql<BR>\n";
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
			$f_delflg = mysql_result($res2, 0, 0);
			if($f_delflg == 1)
			{
				$del_cnt++;
			}
			else
			{
				$fk_admaster_id[$num]		= mysql_result($result, $i, 0);
				$f_pc_memreg[$num]			= mysql_result($result, $i, 1);
				$f_mb_memreg[$num]			= mysql_result($result, $i, 2);
				$f_pc_access[$num]			= mysql_result($result, $i, 3);
				$f_mb_access[$num]			= mysql_result($result, $i, 4);
				$f_pc_paycoin[$num]			= mysql_result($result, $i, 5);
				$f_mb_paycoin[$num]			= mysql_result($result, $i, 6);
				$tas_f_pc_prod[$num]		= mysql_result($result, $i, 7);
				$tas_f_mb_prod[$num]		= mysql_result($result, $i, 8);
				$tas_f_pc_coinpack[$num]	= mysql_result($result, $i, 9);
				$tas_f_mb_coinpack[$num]	= mysql_result($result, $i, 10);
				$pc_reg_woman[$num]			= mysql_result($result, $i, 11);
				$mb_reg_woman[$num]			= mysql_result($result, $i, 12);
				
				$num++;
			}
//G.Chin 2010-10-07 chg end
		}
		
		$data_cnt = $data_cnt - $del_cnt;	//G.Chin 2010-10-07 add
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告集計更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTStatAd($fk_admaster_id,$f_type,$f_pc_memreg,$f_mb_memreg,$f_pc_access,$f_mb_access,$f_pc_paycoin,$f_mb_paycoin)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		if($f_pc_memreg == "")
		{
			$f_pc_memreg = 0;
		}
		
		if($f_mb_memreg == "")
		{
			$f_mb_memreg = 0;
		}
		
		if($f_pc_access == "")
		{
			$f_pc_access = 0;
		}
		
		if($f_mb_access == "")
		{
			$f_mb_access = 0;
		}
		
		if($f_pc_paycoin == "")
		{
			$f_pc_paycoin = 0;
		}
		
		if($f_mb_paycoin == "")
		{
			$f_mb_paycoin = 0;
		}
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//▼現在時刻を取得
		$sql = "select current_date ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$c_date = mysql_result($result, 0, 0);
		
		$sql = "select current_time ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$c_time = mysql_result($result, 0, 0);
		$c_hour = substr($c_time, 0, 2);
		$f_stat_dt = $c_date." ".$c_hour.":00:00";
		
		//▼現在日時のレコードの存在チェック
		$sql = "select count(f_stat_dt) from auction.t_stat_ad where f_stat_dt='$f_stat_dt' and fk_admaster_id='$fk_admaster_id' and f_type='$f_type' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$rows = mysql_result($result, 0, 0);
		
		if($rows == 0)
		{
			//t_stat_ad
			$sql  = "insert into auction.t_stat_ad ";
			$sql .= "(f_stat_dt,fk_admaster_id,f_type,f_pc_memreg,f_mb_memreg,f_pc_access,f_mb_access,f_pc_paycoin,f_mb_paycoin) ";
			$sql .= "values ('$f_stat_dt','$fk_admaster_id','$f_type','$f_pc_memreg','$f_mb_memreg','$f_pc_access','$f_mb_access','$f_pc_paycoin','$f_mb_paycoin') ";
			
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
		}
		else
		{
			//t_stat_ad
			$sql  = "update auction.t_stat_ad set ";
			$sql .= "f_pc_memreg=f_pc_memreg+$f_pc_memreg,";
			$sql .= "f_mb_memreg=f_mb_memreg+$f_mb_memreg,";
			$sql .= "f_pc_access=f_pc_access+$f_pc_access,";
			$sql .= "f_mb_access=f_mb_access+$f_mb_access,";
			$sql .= "f_pc_paycoin=f_pc_paycoin+$f_pc_paycoin,";
			$sql .= "f_mb_paycoin=f_mb_paycoin+$f_mb_paycoin ";
			$sql .= "where f_stat_dt='$f_stat_dt' and fk_admaster_id='$fk_admaster_id' and f_type='$f_type' ";
			
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告コード取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAdcodeInfo($fk_adcode_id, &$f_adcode, &$fk_admaster_id, &$f_type, &$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_adcode,fk_admaster_id,f_type,f_tm_stamp ";
		$sql .= "from auction.t_adcode ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		//echo $sql;
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_adcode		= mysql_result($result, 0, "f_adcode");
			$fk_admaster_id	= mysql_result($result, 0, "fk_admaster_id");
			$f_type			= mysql_result($result, 0, "f_type");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_adcode		= "";
			$fk_admaster_id	= "";
			$f_type			= "";
			$f_tm_stamp		= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告コードID取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAdcodeId($f_adcode, &$fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
//G.Chin 2010-12-17 chg sta
//		$sql = "select fk_adcode_id from auction.t_adcode where f_adcode='$f_adcode' ";
		$sql  = "select fk_adcode_id from auction.t_adcode ";
		$sql .= "where f_adcode='$f_adcode' ";
		$sql .= "and f_delflg=0 ";
//G.Chin 2010-12-17 chg end
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_adcode_id = mysql_result($result, 0, "fk_adcode_id");
		}
		else
		{
			$fk_adcode_id = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告コードID重複チェック関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CheckTAdcodeId($fk_admaster_id, $f_adcode, &$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select count(f_adcode) from auction.t_adcode ";
		$sql .= "where fk_admaster_id!='$fk_admaster_id' and f_adcode='$f_adcode' and f_delflg=0 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_result($result, 0, 0);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主検索広告コード取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function SrcAdmasterGetTAdcode($fk_admaster_id,&$fk_adcode_id,&$f_adcode,&$f_type,&$f_tm_stamp,&$f_mem_reg_urls=NULL)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_adcode_id,f_adcode,f_type,f_tm_stamp,f_mem_reg_urls ";
		$sql .= "from auction.t_adcode ";
		$sql .= "where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$fk_adcode_id	= mysql_result($result, 0, "fk_adcode_id");
			$f_adcode		= mysql_result($result, 0, "f_adcode");
			$f_type			= mysql_result($result, 0, "f_type");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
			$f_mem_reg_urls		= mysql_result($result, 0, "f_mem_reg_urls");
		}
		else
		{
			$fk_adcode_id	= "";
			$f_adcode		= "";
			$f_type			= "";
			$f_tm_stamp		= "";
            $f_mem_reg_urls = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告情報取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-07-28 chg sta
//	function GetTAffiliateInfo($fk_adcode_id,&$fk_program_id,&$f_regist,&$f_regist_value,&$f_first_buy,&$f_first_buy_price,&$f_first_buy_value,&$f_param1,&$f_param2,&$f_type,&$f_status,&$f_bank_pay,&$f_report,&$f_buy_count)
//G.Chin 2010-08-06 chg sta
//	function GetTAffiliateInfo($fk_adcode_id,&$fk_program_id,&$f_regist,&$f_regist_value,&$f_first_buy,&$f_first_buy_price,&$f_first_buy_value,&$f_imgurl_pc,&$f_param1,&$f_param2,&$f_type,&$f_status,&$f_bank_pay,&$f_report,&$f_buy_count)
	function GetTAffiliateInfo($fk_adcode_id,&$fk_program_id,&$f_regist,&$f_regist_value,&$f_first_buy,&$f_first_buy_price,&$f_first_buy_value,&$f_terminal,&$f_imgurl_pc,&$f_imgurl_mb,&$f_param1,&$f_param2,&$f_type,&$f_status,&$f_bank_pay,&$f_report,&$f_buy_count)
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_program_id,f_regist,f_regist_value,";
		$sql .= "f_first_buy,f_first_buy_price,f_first_buy_value,";
//G.Chin 2010-07-28 chg sta
//		$sql .= "f_param1,f_param2,f_type,f_status, ";
//G.Chin 2010-08-06 chg sta
//		$sql .= "f_imgurl_pc,f_param1,f_param2,f_type,f_status, ";
		$sql .= "f_terminal,f_imgurl_pc,f_imgurl_mb,";
		$sql .= "f_param1,f_param2,f_type,f_status,";
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
		$sql .= "f_bank_pay,f_report,f_buy_count ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$fk_program_id		= mysql_result($result, 0, "fk_program_id");
			$f_regist			= mysql_result($result, 0, "f_regist");
			$f_regist_value		= mysql_result($result, 0, "f_regist_value");
			$f_first_buy		= mysql_result($result, 0, "f_first_buy");
			$f_first_buy_price	= mysql_result($result, 0, "f_first_buy_price");
			$f_first_buy_value	= mysql_result($result, 0, "f_first_buy_value");
			$f_terminal			= mysql_result($result, 0, "f_terminal");	//G.Chin 2010-08-06 add
			$f_imgurl_pc		= mysql_result($result, 0, "f_imgurl_pc");	//G.Chin 2010-07-28 add
			$f_imgurl_mb		= mysql_result($result, 0, "f_imgurl_mb");	//G.Chin 2010-08-06 add
			$f_param1			= mysql_result($result, 0, "f_param1");
			$f_param2			= mysql_result($result, 0, "f_param2");
			$f_type				= mysql_result($result, 0, "f_type");
			$f_status			= mysql_result($result, 0, "f_status");
			$f_bank_pay			= mysql_result($result, 0, "f_bank_pay");
			$f_report			= mysql_result($result, 0, "f_report");
			$f_buy_count		= mysql_result($result, 0, "f_buy_count");
		}
		else
		{
			$fk_program_id		= "";
			$f_regist			= "";
			$f_regist_value		= "";
			$f_first_buy		= "";
			$f_first_buy_price	= "";
			$f_first_buy_value	= "";
			$f_terminal			= "";	//G.Chin 2010-08-06 add
			$f_imgurl_pc		= "";	//G.Chin 2010-07-28 add
			$f_imgurl_mb		= "";	//G.Chin 2010-08-06 add
			$f_param1			= "";
			$f_param2			= "";
			$f_type				= "";
			$f_status			= "";
			$f_bank_pay			= "";
			$f_report			= "";
			$f_buy_count		= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告プログラムID取得関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetFProgramId($fk_adcode_id,&$fk_program_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_program_id ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$fk_program_id = mysql_result($result, 0, "fk_program_id");
		}
		else
		{
			$fk_program_id = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告件数取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateCount($fk_adcode_id,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select count(fk_adcode_id) ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_result($result, 0, 0);
		if($data_cnt == "")
		{
			$data_cnt = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告報告数取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateReportCount($fk_adcode_id,&$f_reg_report,&$f_buy_report)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_admaster
		$sql  = "select f_reg_report,f_buy_report ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_reg_report	= mysql_result($result, 0, "f_reg_report");
			$f_buy_report	= mysql_result($result, 0, "f_buy_report");
		}
		else
		{
			$f_reg_report	= "";
			$f_buy_report	= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告登録関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-07-28 chg sta
//	function RegistTAffiliate($fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_param1,$f_param2,$f_type,$f_status,$f_bank_pay,$f_report)
//G.Chin 2010-08-06 chg sta
//	function RegistTAffiliate($fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_imgurl_pc,$f_param1,$f_param2,$f_type,$f_status,$f_bank_pay,$f_report)
	function RegistTAffiliate($fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_imgurl_pc,$f_imgurl_mb,$f_param1,$f_param2,$f_type,$f_status,$f_bank_pay,$f_report)
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
//G.Chin 2010-07-28 chg sta
/*
		$sql  = "insert into auction.t_affiliate ";
		$sql .= "(fk_adcode_id,fk_program_id,f_regist,f_regist_value,";
		$sql .= "f_first_buy,f_first_buy_price,f_first_buy_value,";
		$sql .= "f_param1,f_param2,f_type,f_status,";
		$sql .= "f_bank_pay,f_report,f_buy_count,f_reg_report,f_buy_report) ";
		$sql .= "values ('$fk_adcode_id','$fk_program_id','$f_regist','$f_regist_value',";
		$sql .= "'$f_first_buy','$f_first_buy_price','$f_first_buy_value',";
		$sql .= "'$f_param1','$f_param2','$f_type','$f_status',";
		$sql .= "'$f_bank_pay','$f_report','0','0','0') ";
*/
//G.Chin 2010-08-06 chg sta
/*
		$sql  = "insert into auction.t_affiliate ";
		$sql .= "(fk_adcode_id,fk_program_id,f_regist,f_regist_value,";
		$sql .= "f_first_buy,f_first_buy_price,f_first_buy_value,";
		$sql .= "f_imgurl_pc,f_param1,f_param2,f_type,f_status,";
		$sql .= "f_bank_pay,f_report,f_buy_count,f_reg_report,f_buy_report) ";
		$sql .= "values ('$fk_adcode_id','$fk_program_id','$f_regist','$f_regist_value',";
		$sql .= "'$f_first_buy','$f_first_buy_price','$f_first_buy_value',";
		$sql .= "'$f_imgurl_pc','$f_param1','$f_param2','$f_type','$f_status',";
		$sql .= "'$f_bank_pay','$f_report','0','0','0') ";
*/
		$sql  = "insert into auction.t_affiliate ";
		$sql .= "(fk_adcode_id,fk_program_id,f_regist,f_regist_value,";
		$sql .= "f_first_buy,f_first_buy_price,f_first_buy_value,";
		$sql .= "f_terminal,f_imgurl_pc,f_imgurl_mb,";
		$sql .= "f_param1,f_param2,f_type,f_status,";
		$sql .= "f_bank_pay,f_report,f_buy_count,f_reg_report,f_buy_report) ";
		$sql .= "values ('$fk_adcode_id','$fk_program_id','$f_regist','$f_regist_value',";
		$sql .= "'$f_first_buy','$f_first_buy_price','$f_first_buy_value',";
		$sql .= "'$f_terminal','$f_imgurl_pc','$f_imgurl_mb',";
		$sql .= "'$f_param1','$f_param2','$f_type','$f_status',";
		$sql .= "'$f_bank_pay','$f_report','0','0','0') ";
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-07-28 chg sta
//	function UpdateTAffiliate($fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_param1,$f_param2,$f_type,$f_status,$f_bank_pay,$f_report)
//G.Chin 2010-08-06 chg sta
//	function UpdateTAffiliate($fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_imgurl_pc,$f_param1,$f_param2,$f_type,$f_status,$f_bank_pay,$f_report)
	function UpdateTAffiliate($fk_adcode_id,$fk_program_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_imgurl_pc,$f_imgurl_mb,$f_param1,$f_param2,$f_type,$f_status,$f_bank_pay,$f_report)
//G.Chin 2010-08-06 chg end
//G.Chin 2010-07-28 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
		$sql  = "update auction.t_affiliate set ";
		$sql .= "fk_program_id='$fk_program_id',";
		$sql .= "f_regist='$f_regist',";
		$sql .= "f_regist_value='$f_regist_value',";
		$sql .= "f_first_buy='$f_first_buy',";
		$sql .= "f_first_buy_price='$f_first_buy_price',";
		$sql .= "f_first_buy_value='$f_first_buy_value',";
		$sql .= "f_terminal='$f_terminal',";	//G.Chin 2010-08-06 add
		$sql .= "f_imgurl_pc='$f_imgurl_pc',";	//G.Chin 2010-07-28 add
		$sql .= "f_imgurl_mb='$f_imgurl_mb',";	//G.Chin 2010-08-06 add
		$sql .= "f_param1='$f_param1',";
		$sql .= "f_param2='$f_param2',";
		$sql .= "f_type='$f_type',";
		$sql .= "f_status='$f_status',";
		$sql .= "f_bank_pay='$f_bank_pay',";
		$sql .= "f_report='$f_report' ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告銀行振込フラグ取得関数												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetFBankPay($fk_adcode_id,&$f_bank_pay)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_bank_pay ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_bank_pay = mysql_result($result, 0, "f_bank_pay");
		}
		else
		{
			$f_bank_pay = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告コイン購入回数加算関数												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CountUpTAffiliateFBuyCount($fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
		$sql  = "update auction.t_affiliate set ";
		$sql .= "f_buy_count=f_buy_count+1 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告登録報告数加算関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CountUpTAffiliateFRegReport($fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
		$sql  = "update auction.t_affiliate set ";
		$sql .= "f_reg_report=f_reg_report+1 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告購入報告数加算関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CountUpTAffiliateFBuyReport($fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
		$sql  = "update auction.t_affiliate set ";
		$sql .= "f_buy_report=f_buy_report+1 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告登録判定関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CheckAffiliateRegistReport($fk_adcode_id,&$not_report)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_affiliate
		$sql  = "select f_report ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_report = mysql_result($result, 0, "f_report");
		}
		else
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		if($f_report == 0)
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		//t_admaster
		$sql  = "select tam.f_counter ";
		$sql .= "from auction.t_admaster as tam,auction.t_adcode as tad ";
		$sql .= "where tad.fk_adcode_id='$fk_adcode_id' ";
		$sql .= "and tad.fk_admaster_id=tam.fk_admaster_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_counter = mysql_result($result, 0, "f_counter");
		}
		else
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		//剰余計算
		//登録は、この関数が実行される時点で既に加算されている。
		if($f_counter % $f_report == 0)
		{
			$not_report = 1;
		}
		else
		{
			$not_report = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告購入報告判定関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CheckAffiliateBuyReport($fk_adcode_id,&$not_report)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_report,f_buy_count ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_report		= mysql_result($result, 0, "f_report");
			$f_buy_count	= mysql_result($result, 0, "f_buy_count");
		}
		else
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		if(($f_report == 0) || ($f_buy_count == 0))
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		//剰余計算
		//購入は、この関数が実行される時点ではまだ加算されていない。
		if(($f_buy_count + 1) % $f_report == 0)
		{
			$not_report = 1;
		}
		else
		{
			$not_report = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

//G.Chin 2010-07-27 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主表示情報取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAdmasterDispInfo($fk_admaster_id,&$f_disp_hour,&$f_disp_pay,&$f_disp_today)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_disp_hour,f_disp_pay,f_disp_today ";
		$sql .= "from auction.t_admaster ";
		$sql .= "where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_disp_hour	= mysql_result($result, 0, "f_disp_hour");
			$f_disp_pay		= mysql_result($result, 0, "f_disp_pay");
			$f_disp_today	= mysql_result($result, 0, "f_disp_today");
		}
		else
		{
			$f_disp_hour	= "";
			$f_disp_pay		= "";
			$f_disp_today	= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告主表示情報更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTAdmasterDispInfo($fk_admaster_id,$f_disp_hour,$f_disp_pay,$f_disp_today)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_admaster
		$sql  = "update auction.t_admaster set ";
		$sql .= "f_disp_hour='$f_disp_hour',";
		$sql .= "f_disp_pay='$f_disp_pay',";
		$sql .= "f_disp_today='$f_disp_today' ";
		$sql .= "where fk_admaster_id='$fk_admaster_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-07-27 add end

//G.Chin 2010-07-28 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト種別一覧取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAfflTypeList(&$fk_affl_type_id,&$f_type_name,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_affl_type_id,f_type_name ";
		$sql .= "from auction.t_affl_type ";
		$sql .= "where f_stop=0 ";
		$sql .= "order by fk_affl_type_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_affl_type_id[$i]	= mysql_result($result, $i, "fk_affl_type_id");
			$f_type_name[$i]		= mysql_result($result, $i, "f_type_name");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイトタグ作成関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-08-06 chg sta
//	function MakeAffiliateTag($fk_member_id,$ope_type,&$affiliate_tag)
	function MakeAffiliateTag($fk_member_id,$ope_type,$term_type,&$affiliate_tag)
//G.Chin 2010-08-06 chg end
	{
		//会員親紹介コード取得関数
		GetTMemberFKParentAd($fk_member_id,$tmm_fk_parent_ad);
		
		//アフィリエイト広告情報取得関数
//G.Chin 2010-08-06 chg sta
//		GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_imgurl_pc,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
		GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_imgurl_pc,$taf_f_imgurl_mb,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
//G.Chin 2010-08-06 chg end
		
		$working_tag = "";
		
		//▼アフィリエイト種類判定
		switch($taf_f_type)
		{
			case 0:		//A8
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"で、種類が"A8"の場合
								$pid = $taf_fk_program_id;
								$so = $fk_member_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								$img_url = str_replace("%%PID%%", $pid, $img_url);
								$img_url = str_replace("%%SO%%", $so, $img_url);
								//▼SIパラメータの有無を判定
								if($taf_f_param1 == "")
								{
									$img_url = str_replace("%%SI%%", "1.1.1.regist", $img_url);
								}
								else
								{
									$si = $taf_f_param1;
									$img_url = str_replace("%%SI%%", $si, $img_url);
								}
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
							else
							{
								//▼状態が"無効"、もしくは種類が"A8"でない場合
								$working_tag = "";
							}
						}
					}
					else
					{
						//◆コイン購入決済
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//$working_tag = "<A8.net:アフィリエイトのタグ>";
							//アフィリエイト広告購入報告判定関数
							CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、初回コイン購入報酬が"有り"で、種類が"A8"の場合
								//アフィリエイト用支払履歴情報取得関数
								GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
								if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
								{
									//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
						            $pid = $taf_fk_program_id;
									$so = $fk_member_id;
									if($term_type == 1)
									{
										//MB
										$img_url = $taf_f_imgurl_mb;
									}
									else
									{
										//PC/両方
										$img_url = $taf_f_imgurl_pc;
									}
									$img_url = str_replace("%%PID%%", $pid, $img_url);
									$img_url = str_replace("%%SO%%", $so, $img_url);
									//▼SIパラメータの有無を判定
									if($taf_f_param2 == "") {
										$img_url = str_replace("%%SI%%", "1.1.1.coin", $img_url);
									}
									else {
										$si = $taf_f_param2;
										$img_url = str_replace("%%SI%%", $si, $img_url);
									}
									//$working_tag .= "<img src='$img_url' width='1' height='1'><br>";
									$working_tag = "<img src='$img_url' width='1' height='1'><br>";
									
									//アフィリエイト広告購入報告数加算関数
									CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								//▼状態が"無効"の場合
								$working_tag = "";
							}
						}
					}
					break;
			case 1:		//MOBA8
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆MB/管理画面
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"で、種類が"MOBA8"の場合
								$pid = $taf_fk_program_id;
								$so = $fk_member_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								$img_url = str_replace("%%PID%%", $pid, $img_url);
								$img_url = str_replace("%%SO%%", $so, $img_url);
								//▼SIパラメータの有無を判定
								if($taf_f_param1 == "")
								{
									$img_url = str_replace("%%SI%%", "1.1.1.regist", $img_url);
								}
								else
								{
									$si = $taf_f_param1;
									$img_url = str_replace("%%SI%%", $si, $img_url);
								}
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
							else
							{
								//▼状態が"無効"、もしくは種類が"MOBA8"でない場合
								$working_tag = "";
							}
						}
					}
					else
					{
						//◆コイン購入決済
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//$working_tag = "<MOBA8.net:アフィリエイトのタグ>";
							//アフィリエイト広告購入報告判定関数
							CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、初回コイン購入報酬が"有り"で、種類が"MOBA8"の場合
								//アフィリエイト用支払履歴情報取得関数
								GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
								if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
								{
									//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
						            $pid = $taf_fk_program_id;
									$so = $fk_member_id;
									if($term_type == 1)
									{
										//MB
										$img_url = $taf_f_imgurl_mb;
									}
									else
									{
										//PC/両方
										$img_url = $taf_f_imgurl_pc;
									}
									$img_url = str_replace("%%PID%%", $pid, $img_url);
									$img_url = str_replace("%%SO%%", $so, $img_url);
									//▼SIパラメータの有無を判定
									if($taf_f_param2 == "") {
										$img_url = str_replace("%%SI%%", "1.1.1.coin", $img_url);
									}
									else {
										$si = $taf_f_param2;
										$img_url = str_replace("%%SI%%", $si, $img_url);
									}
									//$working_tag .= "<img src='$img_url' width='1' height='1'><br>";
									$working_tag = "<img src='$img_url' width='1' height='1'><br>";
									
									//アフィリエイト広告購入報告数加算関数
									CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								//▼状態が"無効"の場合
								$working_tag = "";
							}
						}
					}
					break;
			case 2:		//モビル
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//会員アフィリエイトキー取得関数
							GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
							$aflkey = $tmm_f_aflkey;
							$uid1 = $fk_member_id;
							if($term_type == 1)
							{
								//MB
								$img_url = $taf_f_imgurl_mb;
							}
							else
							{
								//PC/両方
								$img_url = $taf_f_imgurl_pc;
							}
							$img_url = str_replace("%%AFLKEY%%", $aflkey, $img_url);
							$img_url = str_replace("%%UID1%%", $uid1, $img_url);
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<モビル:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$aflkey = $tmm_f_aflkey;
								$uid1 = $fk_member_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								$img_url = str_replace("%%AFLKEY%%", $aflkey, $img_url);
								$img_url = str_replace("%%UID1%%", $uid1, $img_url);
								//▼SIパラメータの有無を判定
								if($taf_f_param2 != "") {
									$pri = $recent_money;
									$cnt = 1;
									$img_url .= $taf_f_param2;
									$img_url = str_replace("%%PRI%%", $pri, $img_url);
									$img_url = str_replace("%%CNT%%", $cnt, $img_url);
								}
								//$working_tag .= "<img src='$img_url' width='1' height='1'><br>";
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告購入報告数加算関数
								CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			case 3:		//RTS
					//▼処理種別判定
					//▼無し
					//アフィリエイト広告登録判定関数
					CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
					if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
					{
						//▼状態が"有効"で、登録時報酬が"有り"の場合
						$a = $taf_fk_program_id;
						$u = $fk_member_id;
						if($term_type == 1)
						{
							//MB
							$img_url = $taf_f_imgurl_mb;
						}
						else
						{
							//PC/両方
							$img_url = $taf_f_imgurl_pc;
						}
						$img_url = str_replace("%%A%%", $a, $img_url);
						$img_url = str_replace("%%U%%", $u, $img_url);
						$working_tag = "<img src='$img_url' width='1' height='1'><br>";
						
						//アフィリエイト広告登録報告数加算関数
						CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
					}
					else
					{
						//▼状態が"無効"の場合
						$working_tag = "";
					}
					break;
			case 4:		//MicroAd
					//▼処理種別判定
					//※この関数では登録完了のみ
					//▼端末種別判定
					if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
					{
						//◆登録完了
//G.Chin 2010-08-17 chg sta
/*
						//◆PC
						$working_tag  = "<script type='text/javascript'>\n";
						$working_tag .= "<!--\n";
						$working_tag .= "	var mad_client_id='%%MAD_CLIENT_ID%%';\n";
						$working_tag .= "	var mad_group_id='convtrack';\n";
						$working_tag .= "-->\n";
						$working_tag .= "</script>\n";
						$working_tag .= "<script src='";
						$working_tag .= $taf_f_imgurl_pc;
						$working_tag .= "'>\n";
						$working_tag .= "</script>\n";
						$working_tag = str_replace("%%JS%%", $taf_fk_program_id, $working_tag);
						$working_tag = str_replace("%%MAD_CLIENT_ID%%", $taf_f_param1, $working_tag);
						
						//アフィリエイト広告登録報告数加算関数
						CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
*/
						if($term_type == 0)
						{
							//◆PC
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"の場合
								//アフィリエイト・リスティング広告取得関数
								GetTAffiliateFListing($tmm_fk_parent_ad,$taf_f_listing,$taf_f_listing2);
								
								$working_tag = $taf_f_listing;
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//◆MB
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"の場合
//G.Chin 2010-08-27 chg sta

/*
								$working_tag .= "<img alt='' src='";
								$working_tag .= $taf_f_imgurl_mb;
								$working_tag .= "' width='1' height='1' />";
*/
								//アフィリエイト・リスティング広告取得関数
								GetTAffiliateFListing($tmm_fk_parent_ad,$taf_f_listing,$taf_f_listing2);
								
								$working_tag = $taf_f_listing2;
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
//G.Chin 2010-08-27 chg end
							}
						}
//G.Chin 2010-08-17 chg end
					}
					break;
			case 5:		//Pitta!
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							$pid = $taf_fk_program_id;
							if($term_type == 1)
							{
								//MB
								$img_url = $taf_f_imgurl_mb;
							}
							else
							{
								//PC/両方
								$img_url = $taf_f_imgurl_pc;
							}
							$img_url = str_replace("%%PID%%", $pid, $img_url);
							//$working_tag .= "<script type='text/javascript' src='$img_url'></script><br>";
							$working_tag = "<script type='text/javascript' src='$img_url'></script><br>";
							
							//アフィリエイト広告購入報告数加算関数
							CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<Pitta!:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								$pid = $taf_fk_program_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								$img_url = str_replace("%%PID%%", $pid, $img_url);
								//$working_tag .= "<script type='text/javascript' src='$img_url'></script><br>";
								$working_tag = "<script type='text/javascript' src='$img_url'></script><br>";
								
								//アフィリエイト広告購入報告数加算関数
								CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			case 6:		//Google
					//▼処理種別判定
					if($ope_type == 0)
					{
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC
							//◆登録
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"の場合
								//アフィリエイト・リスティング広告取得関数
								GetTAffiliateFListing($tmm_fk_parent_ad,$taf_f_listing,$taf_f_listing2);
								
								$working_tag = $taf_f_listing;
							}
						}
					}
					else
					{
						//◆コイン購入決済
					}
					break;
			case 7:		//PayPerCall
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<PayPerCall:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								$c = $fk_member_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
									//会員アフィリエイトキー取得関数
									GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
									$aflkey = $tmm_f_aflkey;
									$img_url = str_replace("%%AFLKEY%%", $aflkey, $img_url);
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								$img_url = str_replace("%%C%%", $c, $img_url);
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告購入報告数加算関数
								CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			case 8:		//TRUST COMMERCE
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//会員アフィリエイトキー取得関数
							GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
							$af = $tmm_f_aflkey;
							if($term_type == 1)
							{
								//MB
								$img_url = $taf_f_imgurl_mb;
							}
							else
							{
								//PC/両方
								$img_url = $taf_f_imgurl_pc;
							}
							$img_url = str_replace("%%AF%%", $af, $img_url);
							$working_tag = "<img src='$img_url' width='1' height='1'><br>";
							
							//アフィリエイト広告購入報告数加算関数
							CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<TRUST COMMERCE:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$af = $tmm_f_aflkey;
								$amount = $recent_money;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								$img_url = str_replace("%%AF%%", $af, $img_url);
								$img_url = str_replace("%%AMOUNT%%", $amount, $img_url);
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告購入報告数加算関数
								CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			case 9:		//リンクシェア
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"で、種類が"リンクシェア"の場合
								if($term_type == 1)
								{
									//MB
									$img_url = "";
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								//▼SIパラメータ
								$si = $taf_f_param1;
								$img_url = str_replace("%%SI%%", $si, $img_url);
								//▼会員ID
								$oid = $fk_member_id;
								$img_url = str_replace("%%OID%%", $oid, $img_url);
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
							else
							{
								//▼状態が"無効"、もしくは種類が"リンクシェア"でない場合
								$working_tag = "";
							}
						}
					}
					else
					{
						//◆コイン購入決済
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告購入報告判定関数
							CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、初回コイン購入報酬が"有り"で、種類が"リンクシェア"の場合
								//アフィリエイト用支払履歴情報取得関数
								GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
								if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
								{
									//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
									if($term_type == 1)
									{
										//MB
										$img_url = "";
									}
									else
									{
										//PC/両方
										$img_url = $taf_f_imgurl_pc;
									}
									//▼SIパラメータ
									$si = $taf_f_param2;
									$img_url = str_replace("%%SI%%", $si, $img_url);
									//▼会員ID
									$oid = $fk_member_id;
									$img_url = str_replace("%%OID%%", $oid, $img_url);
									$working_tag = "<img src='$img_url' width='1' height='1'><br>";
									
									//アフィリエイト広告購入報告数加算関数
									CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								//▼状態が"無効"の場合
								$working_tag = "";
							}
						}
					}
					break;
			case 10:	//MoBri
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//会員アフィリエイトキー取得関数
							GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
							$mbid = $tmm_f_aflkey;
							$mbo = $fk_member_id;
							$mbf = $_SERVER['HTTP_USER_AGENT'];
							//▼SIパラメータ
							$si = $taf_f_param1;
							$si = str_replace("%%MBID%%", $mbid, $si);
							$si = str_replace("%%MBO%%", $mbo, $si);
							$si = str_replace("%%MBF%%", $mbf, $si);
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								// $mbid : TOPページ呼出し時に指定した値
								// $mbo : 判別する値、申し込み番号等
								// $mbf : 追加判別する値、申し込み番号等（任意）
								$fp = fsockopen("tk.mobri.jp", 80, &$errno, &$errstr);
								# HTTPヘッダ出力
								If (!$fp)
								{
									echo "$errstr ($errno)<br>\n";
								}
								else
								{
									$sock_str = "GET /$si HTTP/1.0 \n\n";
//									fputs($fp,"GET /lead.php?mbid=$mbid&mbo=$mbo&mbf=$mbf HTTP/1.1 \n\n");
									fputs($fp,$sock_str);
								}
								fclose($fp);
								
								$working_tag = "";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<モビル:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$mbid = $tmm_f_aflkey;
								$mbo = $fk_member_id;
								$mbf = $_SERVER['HTTP_USER_AGENT'];
								//▼SIパラメータ
								$si = $taf_f_param2;
								$si = str_replace("%%MBID%%", $mbid, $si);
								$si = str_replace("%%MBO%%", $mbo, $si);
								$si = str_replace("%%MBF%%", $mbf, $si);
								//▼端末種別判定
								if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
								{
									//◆PC/管理画面
									// $mbid : TOPページ呼出し時に指定した値
									// $mbo : 判別する値、申し込み番号等
									// $mbf : 追加判別する値、申し込み番号等（任意）
									$fp = fsockopen("tk.mobri.jp", 80, &$errno, &$errstr);
									# HTTPヘッダ出力
									If (!$fp)
									{
										echo "$errstr ($errno)<br>\n";
									}
									else
									{
										$sock_str = "GET /$si HTTP/1.0 \n\n";
//										fputs($fp,"GET /lead.php?mbid=$mbid&mbo=$mbo&mbf=$mbf HTTP/1.1 \n\n");
										fputs($fp,$sock_str);
									}
									fclose($fp);
									
									$working_tag = "";
									
									//アフィリエイト広告購入報告数加算関数
									CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
								}
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
/*
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//会員アフィリエイトキー取得関数
							GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
							$mbid = $tmm_f_aflkey;
							$mbo = $fk_member_id;
							if($term_type == 1)
							{
								//MB
								$img_url = $taf_f_imgurl_mb;
							}
							else
							{
								//PC/両方
								$img_url = $taf_f_imgurl_pc;
							}
							//▼SIパラメータ
							$si = $taf_f_param1;
							$img_url = str_replace("%%SI%%", $si, $img_url);
							$img_url = str_replace("%%MBID%%", $mbid, $img_url);
							$img_url = str_replace("%%MBO%%", $mbo, $img_url);
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								$working_tag = "<IMG WIDTH='1' HEIGHT='1' SRC='$img_url' /><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<モビル:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$mbid = $tmm_f_aflkey;
								$mbo = $fk_member_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = $taf_f_imgurl_pc;
								}
								//▼SIパラメータ
								$si = $taf_f_param2;
								$img_url = str_replace("%%SI%%", $si, $img_url);
								$img_url = str_replace("%%MBID%%", $mbid, $img_url);
								$img_url = str_replace("%%MBO%%", $mbo, $img_url);
								//▼端末種別判定
								if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
								{
									//◆PC/管理画面
									$working_tag = "<IMG WIDTH='1' HEIGHT='1' SRC='$img_url' /><br>";
									
									//アフィリエイト広告登録報告数加算関数
									CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
								}
								
								//アフィリエイト広告購入報告数加算関数
								CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
*/
					break;
			case 11:	//BannerBridge
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$bbid = $tmm_f_aflkey;
								$orderid = $fk_member_id;
								$proid = $taf_fk_program_id;
								if($term_type == 1)
								{
									//MB
									$img_url = "";
								}
								else
								{
									//PC/両方
									$img_url = "https://track.bannerbridge.net/%%SI%%";
								}
								//▼SIパラメータ
								$si = $taf_f_param1;
								$img_url = str_replace("%%SI%%", $si, $img_url);
								//▼会員ID
								$img_url = str_replace("%%ORDERID%%", $orderid, $img_url);
								//▼プログラムID
								if($proid != "")
								{
									$img_url = str_replace("%%PROID%%", $proid, $img_url);
								}
								//▼アフィリエイトキー
								if($bbid != "")
								{
									$img_url = str_replace("%%BBID%%", $bbid, $img_url);
								}
								//▼現在日時
								$now = time();
								$current_time = date("YmdHis", $now);
								$img_url = str_replace("%%DATE%%", $current_time, $img_url);
								$working_tag = "<img width='1' height='1' SRC='$img_url' /><br>";
/*
								$working_tag = "<img width='1' height='1' SRC='https://track.bannerbridge.net/lead.php?orderid=%%ORDERID%%&proID=%%PROID%%' />";
								$working_tag = str_replace("%%ORDERID%%", $orderid, $working_tag);
								$working_tag = str_replace("%%PROID%%", $proid, $working_tag);
*/
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<モビル:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数
						CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$bbid = $tmm_f_aflkey;
								$orderid = $fk_member_id;
								$proid = $taf_fk_program_id;
								if($term_type == 1)
								{
									//MB
									$img_url = "";
								}
								else
								{
									//PC/両方
									$img_url = "https://track.bannerbridge.net/%%SI%%";
								}
								//▼SIパラメータ
								$si = $taf_f_param2;
								$img_url = str_replace("%%SI%%", $si, $img_url);
								//▼会員ID
								$img_url = str_replace("%%ORDERID%%", $orderid, $img_url);
								//▼プログラムID
								if($proid != "")
								{
									$img_url = str_replace("%%PROID%%", $proid, $img_url);
								}
								//▼アフィリエイトキー
								if($bbid != "")
								{
									$img_url = str_replace("%%BBID%%", $bbid, $img_url);
								}
								//▼現在日時
								$now = time();
								$current_time = date("YmdHis", $now);
								$img_url = str_replace("%%DATE%%", $current_time, $img_url);
								$working_tag = "<img width='1' height='1' SRC='$img_url' /><br>";
/*
								$working_tag = "<img width='1' height='1' SRC='https://track.bannerbridge.net/sale.php?trans_amount=&orderid=%%ORDERID%%&proID=%%PROID%%' />";
								$working_tag = str_replace("%%ORDERID%%", $orderid, $working_tag);
								$working_tag = str_replace("%%PROID%%", $proid, $working_tag);
*/
								
								//アフィリエイト広告購入報告数加算関数
								CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			case 12:	//アクセストレード
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							$verify = $fk_member_id;
							if($term_type == 1)
							{
								//MB
								$img_url = $taf_f_imgurl_mb;
							}
							else
							{
								//PC/両方
								$img_url = $taf_f_imgurl_pc;
							}
							$img_url = str_replace("%%VERIFY%%", $verify, $img_url);
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						$working_tag = "";
					}
					break;
			case 13:	//AG COMMERCE
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告登録判定関数
							CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"の場合
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								$agcmaid = $tmm_f_aflkey;
								$ap = $taf_fk_program_id;
								if($term_type == 1)
								{
									//MB
									$img_url = $taf_f_imgurl_mb;
								}
								else
								{
									//PC/両方
									$img_url = "";
								}
								//▼SIパラメータ
								$si = $taf_f_param1;
								$img_url = str_replace("%%SI%%", $si, $img_url);
								//▼会員ID
								$OrderID = $fk_member_id;
								$img_url = str_replace("%%ORDERID%%", $OrderID, $img_url);
								$img_url = str_replace("%%AP%%", $ap, $img_url);
								$img_url = str_replace("%%AGCMAID%%", $agcmaid, $img_url);
								$working_tag = "<img src='$img_url' width='1' height='1'><br>";
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
							else
							{
								//▼状態が"無効"、もしくは種類が"リンクシェア"でない場合
								$working_tag = "";
							}
						}
					}
					else
					{
						//◆コイン購入決済
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告購入報告判定関数
							CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
								//アフィリエイト用支払履歴情報取得関数
								GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
								if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
								{
									//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
									//会員アフィリエイトキー取得関数
									GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
									$agcmaid = $tmm_f_aflkey;
									$amount = $recent_money;
									$ap = $taf_fk_program_id;
									if($term_type == 1)
									{
										//MB
										$img_url = $taf_f_imgurl_mb;
									}
									else
									{
										//PC/両方
										$img_url = "";
									}
									//▼SIパラメータ
									$si = $taf_f_param2;
									$img_url = str_replace("%%SI%%", $si, $img_url);
									//▼会員ID
									$OrderID = $fk_member_id;
									$img_url = str_replace("%%ORDERID%%", $OrderID, $img_url);
									$img_url = str_replace("%%AP%%", $ap, $img_url);
									$img_url = str_replace("%%AGCMAID%%", $agcmaid, $img_url);
									$img_url = str_replace("%%AMOUNT%%", $amount, $img_url);
									$working_tag = "<img src='$img_url' width='1' height='1'><br>";
									
									//アフィリエイト広告購入報告数加算関数
									CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								//▼状態が"無効"の場合
								$working_tag = "";
							}
						}
					}
					break;
			case 14:	//Cross-A
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							$u1 = $fk_member_id;
							$u2 = "";
							$u3 = "";
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								//アフィリエイト・リスティング広告取得関数
								GetTAffiliateFListing($tmm_fk_parent_ad,$taf_f_listing,$taf_f_listing2);
								
								$taf_f_listing2 = str_replace("%%U1%%", $u1, $taf_f_listing2);
								$taf_f_listing2 = str_replace("%%U2%%", $u2, $taf_f_listing2);
								$taf_f_listing2 = str_replace("%%U3%%", $u3, $taf_f_listing2);
								
								$working_tag = $taf_f_listing2;
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告購入報告判定関数
							CheckAffiliateBuyReport($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
								//アフィリエイト用支払履歴情報取得関数
								GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
								if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
								{
									//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
									$u1 = $fk_member_id;
									$u2 = "";
									$u3 = "";
									//▼端末種別判定
									if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
									{
										//◆PC/管理画面
										//アフィリエイト・リスティング広告取得関数
										GetTAffiliateFListing($tmm_fk_parent_ad,$taf_f_listing,$taf_f_listing2);
										
										$taf_f_listing2 = str_replace("%%U1%%", $u1, $taf_f_listing2);
										$taf_f_listing2 = str_replace("%%U2%%", $u2, $taf_f_listing2);
										$taf_f_listing2 = str_replace("%%U3%%", $u3, $taf_f_listing2);
										
										$working_tag = $taf_f_listing2;
										
										//アフィリエイト広告購入報告数加算関数
										CountUpTAffiliateFBuyReport($tmm_fk_parent_ad);
									}
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								//▼状態が"無効"の場合
								$working_tag = "";
							}
						}
					}
					break;
			case 15:	//モーションリンク
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数
						CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆PC/管理画面
								//アフィリエイト・リスティング広告取得関数
								GetTAffiliateFListing($tmm_fk_parent_ad,$taf_f_listing,$taf_f_listing2);
								
								$taf_f_listing2 = str_replace("%%MEMBER_ID%%", $fk_member_id, $taf_f_listing2);
								
								$working_tag = $taf_f_listing2;
								
								//アフィリエイト広告登録報告数加算関数
								CountUpTAffiliateFRegReport($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						$working_tag = "";
					}
					break;
		}
		
//G.Chin 2010-10-06 add sta
		//アフィリエイト追加タグ取得関数
		GetTAffiliateAddTag($tmm_fk_parent_ad,$taf_f_add_tag);
		$taf_f_add_tag = str_replace("%%MEMBER_ID%%", $fk_member_id, $taf_f_add_tag);
		$working_tag .= $taf_f_add_tag;
//G.Chin 2010-10-06 add end
		
		$affiliate_tag = $working_tag;
	}
//G.Chin 2010-07-28 add end

//G.Chin 2010-07-29 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		リスティングTOPタグ作成関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-08-06 chg sta
//	function MakeListingTopTag($fk_admaster_id,&$listing_tag)
	function MakeListingTopTag($fk_admaster_id,$term_type,&$listing_tag)
//G.Chin 2010-08-06 chg end
	{
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($fk_admaster_id,$tam_fk_adcode_id,$tam_f_adcode,$tam_f_type,$tam_f_tm_stamp);
		//アフィリエイト広告情報取得関数
//G.Chin 2010-08-06 chg sta
//		GetTAffiliateInfo($tam_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_imgurl_pc,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
		GetTAffiliateInfo($tam_fk_adcode_id,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_imgurl_pc,$taf_f_imgurl_mb,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
//G.Chin 2010-08-06 chg end
		
		$listing_tag = "";
		
		//▼判定
		if($taf_f_type == 4)
		{
			//◆MicroAd
			//▼端末種別判定
			if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
			{
//G.Chin 2010-08-17 chg sta
/*
				//◆PC
				$listing_tag  = "<script type='text/javascript'>\n";
				$listing_tag .= "<!--\n";
				$listing_tag .= "	var mad_client_id='%%MAD_CLIENT_ID%%';\n";
				$listing_tag .= "	var mad_group_id='';\n";
				$listing_tag .= "-->\n";
				$listing_tag .= "</script>\n";
				$listing_tag .= "<script src='";
				$listing_tag .= $taf_f_imgurl_pc;
				$listing_tag .= "'>\n";
				$listing_tag .= "</script>\n";
				$listing_tag = str_replace("%%JS%%", $taf_fk_program_id, $listing_tag);
				$listing_tag = str_replace("%%MAD_CLIENT_ID%%", $taf_f_param1, $listing_tag);
*/
				//▼状態判定
				if($taf_f_status == 1)
				{
					//▼状態が"有効"の場合
					if($term_type == 0)
					{
						//◆PC
						//アフィリエイト・リスティング広告取得関数
						GetTAffiliateFListing($tam_fk_adcode_id,$taf_f_listing,$taf_f_listing2);
						
						$listing_tag = $taf_f_listing;
						$listing_tag = str_replace("convtrack", "", $listing_tag);
					}
					else
					{
						//◆MB
//G.Chin 2010-08-27 chg sta
/*
						$listing_tag .= "<img alt='' src='";
						$listing_tag .= $taf_f_imgurl_pc;
						$listing_tag .= "' width='1' height='1' />";
*/
						//アフィリエイト・リスティング広告取得関数
						GetTAffiliateFListing($tam_fk_adcode_id,$taf_f_listing,$taf_f_listing2);
						
						$listing_tag = $taf_f_listing;
//G.Chin 2010-08-27 chg end
					}
				}
//G.Chin 2010-08-17 chg end
			}
		}
//G.Chin 2010-08-20 del sta
/*
		else if($taf_f_type == 6)
		{
			//◆Google
			//▼端末種別判定
			if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
			{
				//▼状態判定
				if($taf_f_status == 1)
				{
					//▼状態が"有効"の場合
					//アフィリエイト・リスティング広告取得関数
					GetTAffiliateFListing($tam_fk_adcode_id,$taf_f_listing);
					
					$listing_tag = $taf_f_listing;
				}
			}
		}
*/
//G.Chin 2010-08-20 del end
		else if($taf_f_type == 11)
		{
			//◆BannerBridge
			//▼端末種別判定
			if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
			{
				//▼状態判定
				if($taf_f_status == 1)
				{
					//▼状態が"有効"の場合
					if($term_type == 0)
					{
						//◆PC
						//アフィリエイト・リスティング広告取得関数
						GetTAffiliateFListing($tam_fk_adcode_id,$taf_f_listing,$taf_f_listing2);
						
						$listing_tag = $taf_f_listing;
					}
					else
					{
						//◆MB
						$listing_tag = "";
					}
				}
			}
		}
		else
		{
			$listing_tag = "";
		}
	}
//G.Chin 2010-07-29 add end

//G.Chin 2010-08-02 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト・リスティング広告取得関数												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-08-27 chg sta
//	function GetTAffiliateFListing($fk_adcode_id,&$f_listing)
	function GetTAffiliateFListing($fk_adcode_id,&$f_listing,&$f_listing2)
//G.Chin 2010-08-27 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
//G.Chin 2010-08-27 chg sta
//		$sql  = "select f_listing ";
		$sql  = "select f_listing,f_listing2 ";
//G.Chin 2010-08-27 chg end
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_listing	= mysql_result($result, 0, "f_listing");
			$f_listing2	= mysql_result($result, 0, "f_listing2");	//G.Chin 2010-08-27 add
		}
		else
		{
			$f_listing	= "";
			$f_listing2	= "";	//G.Chin 2010-08-27 add
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト・リスティング広告更新関数												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-08-27 chg sta
//	function UpdateTAffiliateFListing($fk_adcode_id,$f_listing)
	function UpdateTAffiliateFListing($fk_adcode_id,$f_listing,$f_listing2)
//G.Chin 2010-08-27 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
		$sql  = "update auction.t_affiliate set ";
//G.Chin 2010-08-27 chg sta
//		$sql .= "f_listing='$f_listing' ";
		$sql .= "f_listing='$f_listing',f_listing2='$f_listing2' ";
//G.Chin 2010-08-27 chg end
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-08-02 add end

//G.Chin 2010-08-19 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告コード指定広告主取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function SrcAdcodeGetTAdmaster($limit,$offset,$f_adcode,&$fk_admaster_id,&$k_adname,&$f_agent_name,&$f_tel_no,&$f_mail_address,&$f_login_id,&$f_login_pass,&$f_kickback_url,&$f_pay_type,&$f_pay_value,&$f_memo,&$f_counter,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_adcode
		$sql_cnt  = "select count(f_adcode) from auction.t_adcode ";
		$sql_cnt .= "where f_adcode like '%$f_adcode%' ";
		$sql_cnt .= "and f_type>0 ";							//G.Chin 2010-10-05 add
		$sql_cnt .= "and f_delflg=0 ";							//G.Chin AWKT-726 2010-11-22 add
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		if($all_count == 0)
		{
			$data_cnt = 0;
			
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		$sql  = "select fk_admaster_id ";
		$sql .= "from auction.t_adcode ";
		$sql .= "where f_adcode like '%$f_adcode%' ";
//		$sql .= "where f_adcode='$f_adcode' ";
		$sql .= "and f_type>0 ";							//G.Chin 2010-10-05 add
		$sql .= "and f_delflg=0 ";							//G.Chin AWKT-726 2010-11-22 add
		$sql .= "limit $limit offset $offset";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_admaster_id[$i]	= mysql_result($result, $i, "fk_admaster_id");
			
			//t_admaster
			$sql  = "select k_adname,f_agent_name,f_tel_no,";
			$sql .= "f_mail_address,f_login_id,f_login_pass,f_kickback_url,";
			$sql .= "f_pay_type,f_pay_value,f_memo,f_counter,f_tm_stamp ";
			$sql .= "from auction.t_admaster ";
			$sql .= "where fk_admaster_id='$fk_admaster_id[$i]' ";
			
			//■ＳＱＬ実行
			$result2 = mysql_query($sql, $db);
			if( $result2 == false )
			{
				print "$sql<BR>\n";
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($result2);
			
			$k_adname[$i]		= mysql_result($result2, 0, "k_adname");
			$f_agent_name[$i]	= mysql_result($result2, 0, "f_agent_name");
			$f_tel_no[$i]		= mysql_result($result2, 0, "f_tel_no");
			$f_mail_address[$i]	= mysql_result($result2, 0, "f_mail_address");
			$f_login_id[$i]		= mysql_result($result2, 0, "f_login_id");
			$f_login_pass[$i]	= mysql_result($result2, 0, "f_login_pass");
			$f_kickback_url[$i]	= mysql_result($result2, 0, "f_kickback_url");
			$f_pay_type[$i]		= mysql_result($result2, 0, "f_pay_type");
			$f_pay_value[$i]	= mysql_result($result2, 0, "f_pay_value");
			$f_memo[$i]			= mysql_result($result2, 0, "f_memo");
			$f_counter[$i]		= mysql_result($result2, 0, "f_counter");
			$f_tm_stamp[$i]		= mysql_result($result2, 0, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-08-19 add end

//G.Chin 2010-10-06 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト追加タグ取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateAddTag($fk_adcode_id,&$f_add_tag)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		
		$sql  = "select f_add_tag ";
		$sql .= "from auction.t_affiliate ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_add_tag	= mysql_result($result, 0, "f_add_tag");
		}
		else
		{
			$f_add_tag	= "";
		}
		
		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト追加タグ更新関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTAffiliateAddTag($fk_adcode_id,$f_add_tag)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate
		$sql  = "update auction.t_affiliate set ";
		$sql .= "f_add_tag='$f_add_tag' ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-10-06 add end

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告情報取得関数２														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateInfo2($fk_adcode_id,&$f_regist,&$f_regist_value,&$f_first_buy,&$f_first_buy_price,&$f_first_buy_value,&$f_terminal,&$f_type,&$f_status,&$f_bank_pay,&$f_report,&$f_buy_count,&$f_reg_report,&$f_buy_report,&$f_top_pc,&$f_top_mb,&$f_reg_pc,&$f_reg_mb,&$f_buy_pc,&$f_buy_mb)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_regist,f_regist_value,";
		$sql .= "f_first_buy,f_first_buy_price,f_first_buy_value,";
		$sql .= "f_terminal,f_type,f_status,f_bank_pay,f_report,";
		$sql .= "f_buy_count,f_reg_report,f_buy_report,";
		$sql .= "f_top_pc,f_top_mb,";
		$sql .= "f_reg_pc,f_reg_mb,";
		$sql .= "f_buy_pc,f_buy_mb ";
		$sql .= "from auction.t_affiliate2 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_regist			= mysql_result($result, 0, "f_regist");
			$f_regist_value		= mysql_result($result, 0, "f_regist_value");
			$f_first_buy		= mysql_result($result, 0, "f_first_buy");
			$f_first_buy_price	= mysql_result($result, 0, "f_first_buy_price");
			$f_first_buy_value	= mysql_result($result, 0, "f_first_buy_value");
			$f_terminal			= mysql_result($result, 0, "f_terminal");
			$f_type				= mysql_result($result, 0, "f_type");
			$f_status			= mysql_result($result, 0, "f_status");
			$f_bank_pay			= mysql_result($result, 0, "f_bank_pay");
			$f_report			= mysql_result($result, 0, "f_report");
			$f_buy_count		= mysql_result($result, 0, "f_buy_count");
			$f_reg_report		= mysql_result($result, 0, "f_reg_report");
			$f_buy_report		= mysql_result($result, 0, "f_buy_report");
			$f_top_pc			= mysql_result($result, 0, "f_top_pc");
			$f_top_mb			= mysql_result($result, 0, "f_top_mb");
			$f_reg_pc			= mysql_result($result, 0, "f_reg_pc");
			$f_reg_mb			= mysql_result($result, 0, "f_reg_mb");
			$f_buy_pc			= mysql_result($result, 0, "f_buy_pc");
			$f_buy_mb			= mysql_result($result, 0, "f_buy_mb");
		}
		else
		{
			$f_regist			= "";
			$f_regist_value		= "";
			$f_first_buy		= "";
			$f_first_buy_price	= "";
			$f_first_buy_value	= "";
			$f_terminal			= "";
			$f_type				= "";
			$f_status			= "";
			$f_bank_pay			= "";
			$f_report			= "";
			$f_buy_count		= "";
			$f_reg_report		= "";
			$f_buy_report		= "";
			$f_top_pc			= "";
			$f_top_mb			= "";
			$f_reg_pc			= "";
			$f_reg_mb			= "";
			$f_buy_pc			= "";
			$f_buy_mb			= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告報告数取得関数２														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateReportCount2($fk_adcode_id,&$f_reg_report,&$f_buy_report)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_admaster
		$sql  = "select f_reg_report,f_buy_report ";
		$sql .= "from auction.t_affiliate2 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_reg_report	= mysql_result($result, 0, "f_reg_report");
			$f_buy_report	= mysql_result($result, 0, "f_buy_report");
		}
		else
		{
			$f_reg_report	= "";
			$f_buy_report	= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト種別一覧取得関数２														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAfflTypeList2(&$fk_affl_type_id,&$f_type_name,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_affl_type_id,f_type_name ";
		$sql .= "from auction.t_affl_type2 ";
		$sql .= "where f_stop=0 ";
		$sql .= "order by fk_affl_type_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_affl_type_id[$i]	= mysql_result($result, $i, "fk_affl_type_id");
			$f_type_name[$i]		= mysql_result($result, $i, "f_type_name");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告件数取得関数２														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateCount2($fk_adcode_id,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select count(fk_adcode_id) ";
		$sql .= "from auction.t_affiliate2 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_result($result, 0, 0);
		if($data_cnt == "")
		{
			$data_cnt = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告登録関数２															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTAffiliate2($fk_adcode_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_type,$f_status,$f_bank_pay,$f_report,$f_top_pc,$f_top_mb,$f_reg_pc,$f_reg_mb,$f_buy_pc,$f_buy_mb)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate2
		$sql  = "insert into auction.t_affiliate2 ";
		$sql .= "(fk_adcode_id,f_regist,f_regist_value,";
		$sql .= "f_first_buy,f_first_buy_price,f_first_buy_value,";
		$sql .= "f_terminal,f_type,f_status,f_bank_pay,f_report,";
		$sql .= "f_buy_count,f_reg_report,f_buy_report,";
		$sql .= "f_top_pc,f_top_mb,";
		$sql .= "f_reg_pc,f_reg_mb,";
		$sql .= "f_buy_pc,f_buy_mb) ";
		$sql .= "values ('$fk_adcode_id','$f_regist','$f_regist_value',";
		$sql .= "'$f_first_buy','$f_first_buy_price','$f_first_buy_value',";
		$sql .= "'$f_terminal','$f_type','$f_status','$f_bank_pay','$f_report',";
		$sql .= "'0','0','0',";
		$sql .= "'$f_top_pc','$f_top_mb',";
		$sql .= "'$f_reg_pc','$f_reg_mb',";
		$sql .= "'$f_buy_pc','$f_buy_mb') ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告更新関数２															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTAffiliate2($fk_adcode_id,$f_regist,$f_regist_value,$f_first_buy,$f_first_buy_price,$f_first_buy_value,$f_terminal,$f_type,$f_status,$f_bank_pay,$f_report,$f_top_pc,$f_top_mb,$f_reg_pc,$f_reg_mb,$f_buy_pc,$f_buy_mb)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate2
		$sql  = "update auction.t_affiliate2 set ";
		$sql .= "fk_adcode_id='$fk_adcode_id',";
		$sql .= "f_regist='$f_regist',";
		$sql .= "f_regist_value='$f_regist_value',";
		$sql .= "f_first_buy='$f_first_buy',";
		$sql .= "f_first_buy_price='$f_first_buy_price',";
		$sql .= "f_first_buy_value='$f_first_buy_value',";
		$sql .= "f_terminal='$f_terminal',";
		$sql .= "f_type='$f_type',";
		$sql .= "f_status='$f_status',";
		$sql .= "f_bank_pay='$f_bank_pay',";
		$sql .= "f_report='$f_report',";
		$sql .= "f_top_pc='$f_top_pc',";
		$sql .= "f_top_mb='$f_top_mb',";
		$sql .= "f_reg_pc='$f_reg_pc',";
		$sql .= "f_reg_mb='$f_reg_mb',";
		$sql .= "f_buy_pc='$f_buy_pc',";
		$sql .= "f_buy_mb='$f_buy_mb' ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイトタグ作成関数２															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function MakeAffiliateTag2($fk_member_id,$ope_type,$term_type,&$affiliate_tag)
	{
		//会員親紹介コード取得関数
		GetTMemberFKParentAd($fk_member_id,$tmm_fk_parent_ad);
		
		//アフィリエイト広告情報取得関数２
		GetTAffiliateInfo2($tmm_fk_parent_ad,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count,$taf_f_reg_report,$taf_f_buy_report,$taf_f_top_pc,$taf_f_top_mb,$taf_f_reg_pc,$taf_f_reg_mb,$taf_f_buy_pc,$taf_f_buy_mb);
		
		$working_tag = "";
		
		//▼アフィリエイト種類判定
		switch($taf_f_type)
		{
			case 10:	//MoBri
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数２
						CheckAffiliateRegistReport2($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//▼端末種別判定
							if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
							{
								//◆MB
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								//▼表示タグ
								// $mbid(%%AFLKEY%%)    : TOPページ呼出し時に指定した値
								// $mbo(%%MEMBER_ID%%)  : 判別する値、申し込み番号等
								// $mbf(%%USER_AGENT%%) : 追加判別する値、申し込み番号等（任意）
								$si = $taf_f_reg_mb;
								$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
								$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
								$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
								
								$fp = fsockopen("tk.mobri.jp", 80, &$errno, &$errstr);
								# HTTPヘッダ出力
								If (!$fp)
								{
									echo "$errstr ($errno)<br>\n";
								}
								else
								{
									$sock_str = "GET /$si HTTP/1.0 \n\n";
//									fputs($fp,"GET /lead.php?mbid=$mbid&mbo=$mbo&mbf=$mbf HTTP/1.1 \n\n");
									fputs($fp,$sock_str);
								}
								fclose($fp);
								
								$working_tag = "";
								
								//アフィリエイト広告登録報告数加算関数２
								CountUpTAffiliateFRegReport2($tmm_fk_parent_ad);
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<モビル:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数２
						CheckAffiliateBuyReport2($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//▼端末種別判定
								if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
								{
									//◆MB
									//会員アフィリエイトキー取得関数
									GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
									//▼表示タグ
									// $mbid(%%AFLKEY%%)    : TOPページ呼出し時に指定した値
									// $mbo(%%MEMBER_ID%%)  : 判別する値、申し込み番号等
									// $mbf(%%USER_AGENT%%) : 追加判別する値、申し込み番号等（任意）
									$si = $taf_f_buy_mb;
									$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
									$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
									$si = str_replace("%%MONEY%%", $recent_money, $si);
									$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
									
									$fp = fsockopen("tk.mobri.jp", 80, &$errno, &$errstr);
									# HTTPヘッダ出力
									If (!$fp)
									{
										echo "$errstr ($errno)<br>\n";
									}
									else
									{
										$sock_str = "GET /$si HTTP/1.0 \n\n";
//										fputs($fp,"GET /lead.php?mbid=$mbid&mbo=$mbo&mbf=$mbf HTTP/1.1 \n\n");
										fputs($fp,$sock_str);
									}
									fclose($fp);
									
									$working_tag = "";
									
									//アフィリエイト広告購入報告数加算関数２
									CountUpTAffiliateFBuyReport2($tmm_fk_parent_ad);
								}
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			case 22:	//EQS
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//アフィリエイト広告登録判定関数２
						CheckAffiliateRegistReport2($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、登録時報酬が"有り"の場合
							//▼端末種別判定
							if((($term_type == $taf_f_terminal) && ($term_type == 1)) || (($term_type == 1) && ($taf_f_terminal == 2)))
							{
								//◆MB
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								//▼表示タグ
								// $mbid(%%AFLKEY%%)    : TOPページ呼出し時に指定した値
								// $mbo(%%MEMBER_ID%%)  : 判別する値、申し込み番号等
								// $mbf(%%USER_AGENT%%) : 追加判別する値、申し込み番号等（任意）
								$si = $taf_f_reg_mb;
								$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
								$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
								$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
								
								$fp = fsockopen("ad.eqsv.jp", 80, &$errno, &$errstr);
								# HTTPヘッダ出力
								If (!$fp)
								{
									echo "$errstr ($errno)<br>\n";
								}
								else
								{
									$sock_str = "GET /$si HTTP/1.0 \n\n";
									fputs($fp,$sock_str);
								}
								fclose($fp);
								
								$working_tag = "";
								
								//アフィリエイト広告登録報告数加算関数２
								CountUpTAffiliateFRegReport2($tmm_fk_parent_ad);
							}
							else if((($term_type == $taf_f_terminal) && ($term_type == 0)) || (($term_type == 0) && ($taf_f_terminal == 2)))
							{
								//◆PC/両方
								$si = $taf_f_reg_pc;
								//▼現在日時
								$now = time();
								$date = date("YmdHis", $now);
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								//▼表示タグ
								$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
								$si = str_replace("%%DATE%%", $date, $si);
								$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
								$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
								$working_tag  = $si;
								$working_tag .= "<br>";
								
								//アフィリエイト広告登録報告数加算関数２
								CountUpTAffiliateFRegReport2($tmm_fk_parent_ad);
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					else
					{
						//◆コイン購入決済
						//$working_tag = "<モビル:アフィリエイトのタグ>";
						//アフィリエイト広告購入報告判定関数２
						CheckAffiliateBuyReport2($tmm_fk_parent_ad,$not_report);
						if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
						{
							//▼状態が"有効"で、初回コイン購入報酬が"有り"の場合
							//アフィリエイト用支払履歴情報取得関数
							GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
							if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
							{
								//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
								//▼端末種別判定
								if((($term_type == $taf_f_terminal) && ($term_type == 1)) || (($term_type == 1) && ($taf_f_terminal == 2)))
								{
									//◆MB
									//会員アフィリエイトキー取得関数
									GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
									//▼表示タグ
									// $mbid(%%AFLKEY%%)    : TOPページ呼出し時に指定した値
									// $mbo(%%MEMBER_ID%%)  : 判別する値、申し込み番号等
									// $mbf(%%USER_AGENT%%) : 追加判別する値、申し込み番号等（任意）
									$si = $taf_f_buy_mb;
									$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
									$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
									$si = str_replace("%%MONEY%%", $recent_money, $si);
									$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
									
									$fp = fsockopen("ad.eqsv.jp", 80, &$errno, &$errstr);
									# HTTPヘッダ出力
									If (!$fp)
									{
										echo "$errstr ($errno)<br>\n";
									}
									else
									{
										$sock_str = "GET /$si HTTP/1.0 \n\n";
										fputs($fp,$sock_str);
									}
									fclose($fp);
									
									$working_tag = "";
									
									//アフィリエイト広告購入報告数加算関数２
									CountUpTAffiliateFBuyReport2($tmm_fk_parent_ad);
								}
								else if((($term_type == $taf_f_terminal) && ($term_type == 0)) || (($term_type == 0) && ($taf_f_terminal == 2)))
								{
									//◆PC/両方
									$si = $taf_f_buy_pc;
									//▼現在日時
									$now = time();
									$date = date("YmdHis", $now);
									//会員アフィリエイトキー取得関数
									GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
									//▼表示タグ
									$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
									$si = str_replace("%%DATE%%", $date, $si);
									$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
									$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
									$working_tag  = $si;
									$working_tag .= "<br>";
									
									//アフィリエイト広告登録報告数加算関数２
									CountUpTAffiliateFRegReport2($tmm_fk_parent_ad);
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								$working_tag = "";
							}
						}
						else
						{
							//▼状態が"無効"の場合
							$working_tag = "";
						}
					}
					break;
			default:
					//▼処理種別判定
					if($ope_type == 0)
					{
						//◆登録
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告登録判定関数２
							CheckAffiliateRegistReport2($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、登録時報酬が"有り"で、種類が同じ場合
								if($term_type == 1)
								{
									//MB
									$si = $taf_f_reg_mb;
								}
								else
								{
									//PC/両方
									$si = $taf_f_reg_pc;
								}
								//▼現在日時
								$now = time();
								$date = date("YmdHis", $now);
								//会員アフィリエイトキー取得関数
								GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
								//▼表示タグ
								$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
								$si = str_replace("%%DATE%%", $date, $si);
								$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
								$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
								$working_tag  = $si;
								$working_tag .= "<br>";
								
								//アフィリエイト広告登録報告数加算関数２
								CountUpTAffiliateFRegReport2($tmm_fk_parent_ad);
							}
							else
							{
								//▼状態が"無効"、もしくは種類が"リンクシェア"でない場合
								$working_tag = "";
							}
						}
					}
					else
					{
						//◆コイン購入決済
						//▼端末種別判定
						if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
						{
							//◆PC/管理画面
							//アフィリエイト広告購入報告判定関数２
							CheckAffiliateBuyReport2($tmm_fk_parent_ad,$not_report);
							if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($not_report == 0))
							{
								//▼状態が"有効"で、初回コイン購入報酬が"有り"で、種類が同じ場合
								//アフィリエイト用支払履歴情報取得関数
								GetAffiliateTPayLogInfo($fk_member_id,$recent_money,$post_money,$all_count);
								if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price))
								{
									//▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
									if($term_type == 1)
									{
										//MB
										$si = $taf_f_buy_mb;
									}
									else
									{
										//PC/両方
										$si = $taf_f_buy_pc;
									}
									//▼現在日時
									$now = time();
									$date = date("YmdHis", $now);
									//会員アフィリエイトキー取得関数
									GetTMemberAflkey($fk_member_id,$tmm_f_aflkey);
									//▼表示タグ
									$si = str_replace("%%MEMBER_ID%%", $fk_member_id, $si);
									$si = str_replace("%%DATE%%", $date, $si);
									$si = str_replace("%%MONEY%%", $recent_money, $si);
									$si = str_replace("%%AFLKEY%%", $tmm_f_aflkey, $si);
									$si = str_replace("%%USER_AGENT%%", $_SERVER['HTTP_USER_AGENT'], $si);
									$working_tag  = $si;
									$working_tag .= "<br>";
									
									//アフィリエイト広告購入報告数加算関数２
									CountUpTAffiliateFBuyReport2($tmm_fk_parent_ad);
								}
								else
								{
									$working_tag = "";
								}
							}
							else
							{
								//▼状態が"無効"の場合
								$working_tag = "";
							}
						}
					}
					break;
		}
		
		$affiliate_tag = $working_tag;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		リスティングTOPタグ作成関数２															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function MakeListingTopTag2($fk_admaster_id,$term_type,&$listing_tag)
	{
		//広告主検索広告コード取得関数
		SrcAdmasterGetTAdcode($fk_admaster_id,$tam_fk_adcode_id,$tam_f_adcode,$tam_f_type,$tam_f_tm_stamp);
		//アフィリエイト広告情報取得関数２
		GetTAffiliateInfo2($tam_fk_adcode_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_terminal,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count,$taf_f_reg_report,$taf_f_buy_report,$taf_f_top_pc,$taf_f_top_mb,$taf_f_reg_pc,$taf_f_reg_mb,$taf_f_buy_pc,$taf_f_buy_mb);
		
		$listing_tag = "";
		
//G.Chin AREQ-362 2010-11-22 chg sta
/*
		//▼判定
		if($taf_f_type == 4)
		{
			//◆MicroAd
			//▼端末種別判定
			if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
			{
				//▼状態判定
				if($taf_f_status == 1)
				{
					//▼状態が"有効"の場合
					if($term_type == 0)
					{
						//◆PC
						//アフィリエイトTOPタグ取得関数
						GetTAffiliateFTop($tam_fk_adcode_id,$taf_f_top_pc,$taf_f_top_mb);
						
						$listing_tag = $taf_f_top_pc;
						$listing_tag = str_replace("convtrack", "", $listing_tag);
					}
					else
					{
						//◆MB
						//アフィリエイトTOPタグ取得関数
						GetTAffiliateFTop($tam_fk_adcode_id,$taf_f_top_pc,$taf_f_top_mb);
						
						$listing_tag = $taf_f_top_mb;
					}
				}
			}
		}
		else if($taf_f_type == 11)
		{
			//◆BannerBridge
			//▼端末種別判定
			if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
			{
				//▼状態判定
				if($taf_f_status == 1)
				{
					//▼状態が"有効"の場合
					if($term_type == 0)
					{
						//◆PC
						//アフィリエイトTOPタグ取得関数
						GetTAffiliateFTop($tam_fk_adcode_id,$taf_f_top_pc,$taf_f_top_mb);
						
						$listing_tag = $taf_f_top_pc;
					}
					else
					{
						//◆MB
						$listing_tag = "";
					}
				}
			}
		}
		else
		{
			//▼端末種別判定
			if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
			{
				//▼状態判定
				if($taf_f_status == 1)
				{
					//▼状態が"有効"の場合
					//アフィリエイトTOPタグ取得関数
					GetTAffiliateFTop($tam_fk_adcode_id,$taf_f_top_pc,$taf_f_top_mb);
					
					if($term_type == 0)
					{
						//◆PC
						$listing_tag = $taf_f_top_pc;
					}
					else
					{
						//◆MB
						$listing_tag = $taf_f_top_mb;
					}
				}
			}
		}
*/
		//▼端末種別判定
		if(($term_type == $taf_f_terminal) || ($term_type == 2) || ($taf_f_terminal == 2))
		{
			//▼状態判定
			if($taf_f_status == 1)
			{
				//▼状態が"有効"の場合
				//アフィリエイトTOPタグ取得関数
				GetTAffiliateFTop($tam_fk_adcode_id,$taf_f_top_pc,$taf_f_top_mb);
				
				if($term_type == 0)
				{
					//◆PC
					$listing_tag = $taf_f_top_pc;
				}
				else
				{
					//◆MB
					$listing_tag = $taf_f_top_mb;
				}
			}
		}
//G.Chin AREQ-362 2010-11-22 chg end
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイトTOPタグ取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAffiliateFTop($fk_adcode_id,&$f_top_pc,&$f_top_mb)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_top_pc,f_top_mb ";
		$sql .= "from auction.t_affiliate2 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_top_pc	= mysql_result($result, 0, "f_top_pc");
			$f_top_mb	= mysql_result($result, 0, "f_top_mb");
		}
		else
		{
			$f_top_pc	= "";
			$f_top_mb	= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

//G.Chin 2010-11-27 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告コイン購入回数加算関数２												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CountUpTAffiliateFBuyCount2($fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate2
		$sql  = "update auction.t_affiliate2 set ";
		$sql .= "f_buy_count=f_buy_count+1 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告登録報告数加算関数２													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CountUpTAffiliateFRegReport2($fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate2
		$sql  = "update auction.t_affiliate2 set ";
		$sql .= "f_reg_report=f_reg_report+1 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告購入報告数加算関数２													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CountUpTAffiliateFBuyReport2($fk_adcode_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_affiliate2
		$sql  = "update auction.t_affiliate2 set ";
		$sql .= "f_buy_report=f_buy_report+1 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告登録判定関数２														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CheckAffiliateRegistReport2($fk_adcode_id,&$not_report)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_affiliate2
		$sql  = "select f_report ";
		$sql .= "from auction.t_affiliate2 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_report = mysql_result($result, 0, "f_report");
		}
		else
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		if($f_report == 0)
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		//t_admaster
		$sql  = "select tam.f_counter ";
		$sql .= "from auction.t_admaster as tam,auction.t_adcode as tad ";
		$sql .= "where tad.fk_adcode_id='$fk_adcode_id' ";
		$sql .= "and tad.fk_admaster_id=tam.fk_admaster_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_counter = mysql_result($result, 0, "f_counter");
		}
		else
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		//剰余計算
		//登録は、この関数が実行される時点で既に加算されている。
		if($f_counter % $f_report == 0)
		{
			$not_report = 1;
		}
		else
		{
			$not_report = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト広告購入報告判定関数２													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CheckAffiliateBuyReport2($fk_adcode_id,&$not_report)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_report,f_buy_count ";
		$sql .= "from auction.t_affiliate2 ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows >0)
		{
			$f_report		= mysql_result($result, 0, "f_report");
			$f_buy_count	= mysql_result($result, 0, "f_buy_count");
		}
		else
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		if(($f_report == 0) || ($f_buy_count == 0))
		{
			$not_report = 0;
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		
		//剰余計算
		//購入は、この関数が実行される時点ではまだ加算されていない。
		if(($f_buy_count + 1) % $f_report == 0)
		{
			$not_report = 1;
		}
		else
		{
			$not_report = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-11-27 add end

?>