<?php
/* 
 * アプリケーションの設定ファイル
 *テンプレートの関連設定
 */
return  array (

/*  ENCODE   */
'DEFAULT_CHARSET'=>'utf-8',
//'DEFAULT_CHARSET'=>'shift-jis',

/*    テンプレートの設定        */
'TEMPLATE_ENGINE_CLASS'=>FRAMEWORK_DIR.'.template.SmartyTemplateEngine',	//テンプレートエンジンを指定する
'TEMPLATE_PATH'=>'templates',			//テンプレートファイルの親パス

/*    CSSシートの設定        */
'STYLE_DIR'=>'style',                           //CSSシートのパス
'DEFAULT_THEME'=>'default',                     //デフォルドのtheme

/*    javascriptの設定        */
'JAVASCRIPT_DIR'=>'js',

/*  Token検証       */
'TOKEN_VALIDATION'=>true,       //Token検証するかどうか
'TOKEN_TYPE'=>'md5',             //Tockenの暗号化タイプ
'TOKEN_NAME'=>'_TOKEN_',         //Tockenの名前

/*    Ajax関連      */
'DEFAULT_AJAX_RETURN'=>'json',           //Ajaxデフォルトのリターンタイプ     xml と json　がサポートする

'TEMPLATE_WORD_FILE'=>'template_word.html'
);

?>
