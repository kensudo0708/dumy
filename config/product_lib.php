<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/06												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 商品関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品カテゴリ名取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTItemCategoryName($fk_item_category, &$f_category_name) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql = "select f_category_name from auction.t_item_category where fk_item_category='$fk_item_category' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_category_name = mysql_result($result, 0, "f_category_name");
        }
        else {
            $f_category_name = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品カテゴリ一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTItemCategoryList(&$fk_item_category, &$f_category_name, &$f_mail_category_val, &$f_tm_stamp, &$data_cnt) {

        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql = "select fk_item_category,f_category_name,f_mail_category_val,f_tm_stamp from auction.t_item_category where f_status = 0 order by fk_item_category";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_item_category[$i]		= mysql_result($result, $i, "fk_item_category");
            $f_category_name[$i]		= mysql_result($result, $i, "f_category_name");
            $f_mail_category_val[$i]	= mysql_result($result, $i, "f_mail_category_val");
            $f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション名一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetFAuctionNameList(&$fk_auction_type_id, &$f_auction_name, &$data_cnt) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql = "select fk_auction_type_id,f_auction_name from auction.t_auction_type order by fk_auction_type_id";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_auction_type_id[$i]	= mysql_result($result, $i, "fk_auction_type_id");
            $f_auction_name[$i]		= mysql_result($result, $i, "f_auction_name");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション名単体取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetFAuctionNameUnit($fk_auction_type_id, &$f_auction_name) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql = "select f_auction_name from auction.t_auction_type where fk_auction_type_id='$fk_auction_type_id'";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_auction_name	= mysql_result($result, 0, "f_auction_name");
        }
        else {
            $f_auction_name	= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート名一覧取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetFProductTemplateNameList(&$fk_products_template_id, &$template_name, &$data_cnt) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql = "select fk_products_template_id,f_products_name from auction.t_products_template where f_status!=9 order by fk_products_template_id";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_products_template_id[$i]	= mysql_result($result, $i, "fk_products_template_id");
            $template_name[$i]				= mysql_result($result, $i, "f_products_name");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品一覧取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsList($inp_status,$inp_category,$inp_auction,$inp_regdate,$inp_recmmend,$limit,$offset,&$fk_products_id,&$fk_item_category_id,&$f_products_name,&$f_start_time,&$f_status,&$f_photo1,&$f_now_price,&$f_auction_name,&$all_count,&$data_cnt,$pid,$sort,$regdate) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }
        //状態
        $where_status = '';
        if ( strlen($inp_status) ) {
            switch ($inp_status) {
            case 0:
                // 待機中
                $where_status = "tp.f_status=0 ";
                break;
            default:
                // 開催中
                $where_status = "tp.f_status IN (1,3,4,5,7) ";
                break;
            }
        } else {
            // すべて
            $where_status = "tp.f_status IN (0,1,3,4,5,7) ";
        }

        //カテゴリ
        $where_category = '';
        if($inp_category == "") {
            $where_category = "";
        }
        else {
            $where_category = "and tpm.fk_item_category_id='$inp_category' ";
        }

        //オークションタイプ
        if($inp_auction == "") {
            $where_auction = "";
        }
        else {
            $where_auction = "and tpm.fk_auction_type_id='$inp_auction' ";
        }

        //登録日
        if($inp_regdate == "") {
            $where_regdate = "";
        }
        else {
            $where_regdate = "and tpm.f_regdate>='$inp_regdate 00:00:00' and tpm.f_regdate<='$inp_regdate 23:59:59' ";
        }

        //注目商品
        if($inp_recmmend == "1") {
            $where_recmmend = "and tpm.f_recmmend='$inp_recmmend' ";
        }
        else {
            $where_recmmend = "";
        }

        //商品ID
        if($pid == "") {
            $where_pid = "";
        }
        else {
            $where_pid = "and tpm.fk_products_id = $pid ";
        }

        if($regdate ==0)
        {
            $where_ago ="";
        }
        else if($regdate >0)
        {
            $where_ago ="and tpm.f_regdate > DATE_SUB(NOW() ,INTERVAL $regdate DAY)";
        }

        //ソート順
        $order_sort = '';
        switch($sort) {
            case 0:
                $order_sort ="tpm2.fk_products_id ";
                break;
            case 1:
                $order_sort ="tpm2.fk_products_id desc ";
                break;
            case 2:
                $order_sort ="tpm2.f_start_time ";
                break;
            case 3:
                $order_sort ="tpm2.f_start_time desc ";
                break;
            case 4:
                $order_sort ="tp2.f_auction_time desc,tpm2.f_start_time desc ";
                break;
            case 5:
                $order_sort ="tp2.f_auction_time asc,tpm2.f_start_time asc ";
                break;
        }

        $sql_cnt = "
SELECT
  count(tpm2.fk_products_id)
FROM
(
  SELECT
    fk_products_id
  FROM
    auction.t_products_master AS tpm
  WHERE
    tpm.f_delflg=0 $where_ago
    ".$where_category."
    ".$where_auction."
    ".$where_regdate."
    ".$where_recmmend."
    ".$where_pid."
) AS tpm2
INNER JOIN
(
  SELECT
    fk_products_id
  FROM
    auction.t_products AS tp
  WHERE
    ".$where_status."
) AS tp2 ON tpm2.fk_products_id=tp2.fk_products_id
";
        //echo $sql_cnt.'<br>';

        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);
        $sql = "
SELECT
  tpm2.*,tp2.*,att2.f_auction_name
FROM
(
  SELECT
    tpm.fk_products_id,tpm.fk_item_category_id,tpm.f_products_name,tpm.f_start_time,tpm.f_photo1,tpm.fk_auction_type_id
  FROM
    auction.t_products_master AS tpm
  WHERE
    tpm.f_delflg=0 $where_ago
    ".$where_category."
    ".$where_auction."
    ".$where_regdate."
    ".$where_recmmend."
    ".$where_pid."
) AS tpm2
INNER JOIN
(
  SELECT
    tp.fk_products_id,tp.f_status,tp.f_now_price,tp.f_auction_time
  FROM
    auction.t_products AS tp
  WHERE
    ".$where_status."
) AS tp2 ON tpm2.fk_products_id=tp2.fk_products_id
INNER JOIN
  auction.t_auction_type att2 ON tpm2.fk_auction_type_id=att2.fk_auction_type_id
ORDER BY
  ".$order_sort."
LIMIT ".$limit." OFFSET ".$offset."
";
        //echo $sql.'<br>';

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_products_id[$i]			= mysql_result($result, $i, "fk_products_id");
            $fk_item_category_id[$i]	= mysql_result($result, $i, "fk_item_category_id");
            $f_products_name[$i]		= mysql_result($result, $i, "f_products_name");
            $f_start_time[$i]			= mysql_result($result, $i, "f_start_time");
            $f_status[$i]				= mysql_result($result, $i, "f_status");
            $f_photo1[$i]				= mysql_result($result, $i, "f_photo1");
            $f_now_price[$i]			= mysql_result($result, $i, "f_now_price");
            $f_auction_name[$i]			= mysql_result($result, $i, "f_auction_name");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品情報取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsInfo($fk_products_id,&$tpm_f_products_name,&$tpm_fk_item_category_id,&$tpm_f_photo1,&$tpm_f_photo2,&$tpm_f_photo3,&$tpm_f_photo4,&$tpm_f_photo5,&$tpm_f_photo1mb,&$tpm_f_photo2mb,&$tpm_f_photo3mb,&$tpm_f_photo4mb,&$tpm_f_photo5mb,&$tpm_f_top_description_pc,&$tpm_f_main_description_pc,&$tpm_f_top_description_mb,&$tpm_f_main_description_mb,&$tpm_f_start_price,&$tpm_f_min_price,&$tpm_f_r_min_price,&$tpm_f_r_max_price,&$tpm_f_market_price,&$tpm_fk_auction_type_id,&$tpm_f_delflg,&$tpm_f_memo,&$tpm_f_target_hard,&$tpm_f_recmmend,&$tpm_f_regdate,&$tp_f_bit_buy_count,&$tp_f_bit_free_count,&$tp_f_now_price,&$tp_f_last_bidder,&$tp_f_last_bidder_handle,&$tp_f_auction_time,&$tp_t_r_status,&$tp_f_status,&$tp_f_tm_stamp,&$tp_f_start_time,&$tpm_f_plus_coins,&$tpm_f_address_flag,&$tmp_fk_products_template_id,&$initial_stop_time) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }
        $sql  = "select f_products_name,fk_item_category_id,";
        $sql .= "f_photo1,f_photo2,f_photo3,f_photo4,f_photo5,f_photo1mb,f_photo2mb,f_photo3mb,f_photo4mb,f_photo5mb,";
        $sql .= "f_top_description_pc,f_main_description_pc,f_top_description_mb,f_main_description_mb,";
        $sql .= "f_start_price,f_min_price,f_r_min_price,f_r_max_price,f_market_price,fk_auction_type_id,";
        $sql .= "f_delflg,f_memo,f_target_hard,f_recmmend,f_regdate,f_plus_coins,f_address_flag, ";
        $sql .= "fk_products_template_id ";//20100424 落札者の声対応
        $sql .= "from auction.t_products_master where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $tpm_f_products_name			= mysql_result($result, 0, "f_products_name");
            $tpm_fk_item_category_id		= mysql_result($result, 0, "fk_item_category_id");
            $tpm_f_photo1					= mysql_result($result, 0, "f_photo1");
            $tpm_f_photo2					= mysql_result($result, 0, "f_photo2");
            $tpm_f_photo3					= mysql_result($result, 0, "f_photo3");
            $tpm_f_photo4					= mysql_result($result, 0, "f_photo4");
            $tpm_f_photo5					= mysql_result($result, 0, "f_photo5");
            $tpm_f_photo1mb					= mysql_result($result, 0, "f_photo1mb");
            $tpm_f_photo2mb					= mysql_result($result, 0, "f_photo2mb");
            $tpm_f_photo3mb					= mysql_result($result, 0, "f_photo3mb");
            $tpm_f_photo4mb					= mysql_result($result, 0, "f_photo4mb");
            $tpm_f_photo5mb					= mysql_result($result, 0, "f_photo5mb");
            $tpm_f_top_description_pc		= mysql_result($result, 0, "f_top_description_pc");
            $tpm_f_main_description_pc		= mysql_result($result, 0, "f_main_description_pc");
            $tpm_f_top_description_mb		= mysql_result($result, 0, "f_top_description_mb");
            $tpm_f_main_description_mb		= mysql_result($result, 0, "f_main_description_mb");
            $tpm_f_start_price				= mysql_result($result, 0, "f_start_price");
            $tpm_f_min_price				= mysql_result($result, 0, "f_min_price");
            $tpm_f_r_min_price				= mysql_result($result, 0, "f_r_min_price");
            $tpm_f_r_max_price				= mysql_result($result, 0, "f_r_max_price");
            $tpm_f_market_price				= mysql_result($result, 0, "f_market_price");
            $tpm_fk_auction_type_id			= mysql_result($result, 0, "fk_auction_type_id");
            $tpm_f_delflg					= mysql_result($result, 0, "f_delflg");
            $tpm_f_memo						= mysql_result($result, 0, "f_memo");
            $tpm_f_target_hard				= mysql_result($result, 0, "f_target_hard");
            $tpm_f_recmmend					= mysql_result($result, 0, "f_recmmend");
            $tpm_f_regdate					= mysql_result($result, 0, "f_regdate");
            $tpm_f_plus_coins				= mysql_result($result, 0, "f_plus_coins");
            $tpm_f_address_flag				= mysql_result($result, 0, "f_address_flag");
            $tmp_fk_products_template_id	= mysql_result($result, 0, "fk_products_template_id");//20100424 落札者の声対応
        }
        else {
            $tpm_f_products_name			= "";
            $tpm_fk_item_category_id		= "";
            $tpm_f_photo1					= "";
            $tpm_f_photo2					= "";
            $tpm_f_photo3					= "";
            $tpm_f_photo4					= "";
            $tpm_f_photo5					= "";
            $tpm_f_photo1mb					= "";
            $tpm_f_photo2mb					= "";
            $tpm_f_photo3mb					= "";
            $tpm_f_photo4mb					= "";
            $tpm_f_photo5mb					= "";
            $tpm_f_top_description_pc		= "";
            $tpm_f_main_description_pc		= "";
            $tpm_f_top_description_mb		= "";
            $tpm_f_main_description_mb		= "";
            $tpm_f_start_price				= "";
            $tpm_f_min_price				= "";
            $tpm_f_r_min_price				= "";
            $tpm_f_r_max_price				= "";
            $tpm_f_market_price				= "";
            $tpm_fk_auction_type_id			= "";
            $tpm_f_delflg					= "";
            $tpm_f_memo						= "";
            $tpm_f_target_hard				= "";
            $tpm_f_recmmend					= "";
            $tpm_f_regdate					= "";
            $tpm_f_plus_coins				= "";
            $tpm_f_address_flag				= "";
            $tmp_fk_products_template_id	= "0";//20100424 落札者の声対応
        }

        $sql  = "select f_bit_buy_count,f_bit_free_count,f_now_price,";
        $sql .= "f_last_bidder,f_last_bidder_handle,f_auction_time,";
        $sql .= "t_r_status,f_status,f_tm_stamp,f_start_time ";
        $sql .= "from auction.t_products where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $now = time();
            $tp_f_bit_buy_count			= mysql_result($result, 0, "f_bit_buy_count");
            $tp_f_bit_free_count		= mysql_result($result, 0, "f_bit_free_count");
            $tp_f_now_price				= mysql_result($result, 0, "f_now_price");
            $tp_f_last_bidder			= mysql_result($result, 0, "f_last_bidder");
            $tp_f_last_bidder_handle	= mysql_result($result, 0, "f_last_bidder_handle");
            $tp_f_auction_time			= mysql_result($result, 0, "f_auction_time");
            $tp_t_r_status				= mysql_result($result, 0, "t_r_status");
            $tp_f_status				= mysql_result($result, 0, "f_status");
            $tp_f_tm_stamp				= mysql_result($result, 0, "f_tm_stamp");
            $tp_f_start_time			= mysql_result($result, 0, "f_start_time");
    //      $initial_stop_time              =  date("Y-m-d H:i:s",strtotime($tp_f_start_time)+$tp_f_auction_time);
            if ($tp_f_status==0) {
                $initial_stop_time              =  date("Y-m-d H:i:s", strtotime($tp_f_start_time) + $tp_f_auction_time);
            }else {
                $initial_stop_time              =  date("Y-m-d H:i:s", $now + $tp_f_auction_time);

            }

        }
        else {
            $tp_f_bit_buy_count			= "";
            $tp_f_bit_free_count		= "";
            $tp_f_now_price				= "";
            $tp_f_last_bidder			= "";
            $tp_f_last_bidder_handle	= "";
            $tp_f_auction_time			= "";
            $tp_t_r_status				= "";
            $tp_f_status				= "";
            $tp_f_tm_stamp				= "";
            $tp_f_start_time			= "";
            $initial_stop_time			= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品情報登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function RegistTProductsData($f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_start_time,$fk_auction_type_id,$f_recmmend,$f_status,$f_auction_time,$f_plus_coins,$f_address_flag,$f_products_template_id,$t_r_status) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_master
    /*
                    $sql  = "insert into auction.t_products_master values ";
                    $sql .= "(0,'$f_products_name','$fk_item_category_id','$f_photo1','$f_photo2','$f_photo3',";
                    $sql .= "'$f_photo4','$f_photo5','$f_photo1mb','$f_photo2mb','$f_photo3mb','$f_photo4mb','$f_photo5mb',";
                    $sql .= "'$f_top_description_pc','$f_main_description_pc',";
                    $sql .= "'$f_top_description_mb','$f_main_description_mb',";
                    $sql .= "'$f_start_price','$f_min_price','$f_r_min_price','$f_r_max_price',";
                    $sql .= "'$f_market_price','$f_start_time','$fk_auction_type_id','0','',";
                    $sql .= "'0','$f_recmmend',now(),now()) ";
        */
        $sql  = "insert into auction.t_products_master ";
        $sql .= "(fk_products_id,f_products_name,";
        $sql .= "fk_item_category_id,f_photo1,f_photo2,f_photo3,f_photo4,f_photo5,";
        $sql .= "f_photo1mb,f_photo2mb,f_photo3mb,f_photo4mb,f_photo5mb,";
        $sql .= "f_top_description_pc,f_main_description_pc,";
        $sql .= "f_top_description_mb,f_main_description_mb,";
        $sql .= "f_start_price,f_min_price,f_r_min_price,f_r_max_price,";
        $sql .= "f_market_price,f_start_time,fk_auction_type_id,";

        //ポイントオークション対応 START
        $sql .= "f_plus_coins,f_address_flag,";
        //END

        $sql .= "f_recmmend,f_regdate,f_tm_stamp,fk_products_template_id) ";
        $sql .= "values ";
        $sql .= "(0,'$f_products_name','$fk_item_category_id',";
        $sql .= "'$f_photo1','$f_photo2','$f_photo3','$f_photo4','$f_photo5',";
        $sql .= "'$f_photo1mb','$f_photo2mb','$f_photo3mb','$f_photo4mb','$f_photo5mb',";
        $sql .= "'$f_top_description_pc','$f_main_description_pc',";
        $sql .= "'$f_top_description_mb','$f_main_description_mb',";
        $sql .= "'$f_start_price','$f_min_price','$f_r_min_price','$f_r_max_price',";
        $sql .= "'$f_market_price','$f_start_time','$fk_auction_type_id',";
        $sql .= "'$f_plus_coins','$f_address_flag',";
        $sql .= "'$f_recmmend',now(),now(),'$f_products_template_id') ";

        //■ＳＱＬ実行
        if(!mysql_query($sql, $db)){
			// output log
			error_log("[".__FILE__." : ".__LINE__."]".mysql_errno($db).": ".mysql_error($db)."\n",3,"/tmp/puroduct_lib.log");
			mysql_query("ROLLBACK", $db);
			db_close($db);
			return false;
        }

        //t_products
    /*
                    $sql  = "insert into auction.t_products values ";
                    $sql .= "(0,'0','0','$f_start_price','0','','$f_auction_time','0','$f_status','',now(),'$f_start_time') ";
        */
        $sql  = "insert into auction.t_products ";
        $sql .= "(fk_products_id,f_now_price,f_auction_time,f_status,f_tm_stamp,f_start_time,t_r_status) ";
        $sql .= "values ";
        $sql .= "(LAST_INSERT_ID(),'$f_start_price','$f_auction_time','$f_status',now(),'$f_start_time','$t_r_status') ";

        // exec sql
        if(!mysql_query($sql, $db)){
			// output log
			error_log("[".__FILE__." : ".__LINE__."]".mysql_errno($db).": ".mysql_error($db)."\n",3,"/tmp/puroduct_lib.log");
			mysql_query("ROLLBACK", $db);
			db_close($db);
			return false;
        }


        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品情報更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function UpdateTProductsData($fk_products_id,$f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_start_time,$fk_auction_type_id,$f_recmmend,$f_status,$f_auction_time,$f_plus_coins,$f_address_flag,$f_products_template_id) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //写真更新文字列
        $update_photo  = "";
        if($f_photo1 != "")		$update_photo .= "f_photo1='$f_photo1',";
        if($f_photo2 != "")		$update_photo .= "f_photo2='$f_photo2',";
        if($f_photo3 != "")		$update_photo .= "f_photo3='$f_photo3',";
        if($f_photo4 != "")		$update_photo .= "f_photo4='$f_photo4',";
        if($f_photo5 != "")		$update_photo .= "f_photo5='$f_photo5',";
        if($f_photo1mb != "")	$update_photo .= "f_photo1mb='$f_photo1mb',";
        if($f_photo2mb != "")	$update_photo .= "f_photo2mb='$f_photo2mb',";
        if($f_photo3mb != "")	$update_photo .= "f_photo3mb='$f_photo3mb',";
        if($f_photo4mb != "")	$update_photo .= "f_photo4mb='$f_photo4mb',";
        if($f_photo5mb != "")	$update_photo .= "f_photo5mb='$f_photo5mb',";

        //t_products_master
        $sql  = "update auction.t_products_master set ";
        $sql .= "f_products_name='$f_products_name',fk_item_category_id='$fk_item_category_id',";
        $sql .= $update_photo;
        $sql .= "f_top_description_pc='$f_top_description_pc',f_main_description_pc='$f_main_description_pc',";
        $sql .= "f_top_description_mb='$f_top_description_mb',f_main_description_mb='$f_main_description_mb',";
        $sql .= "f_start_price='$f_start_price',f_min_price='$f_min_price',";
        $sql .= "f_r_min_price='$f_r_min_price',f_r_max_price='$f_r_max_price',";
        $sql .= "f_market_price='$f_market_price',f_start_time='$f_start_time',";
        $sql .= "fk_auction_type_id='$fk_auction_type_id',f_recmmend='$f_recmmend', ";
        //ポイントオークション対応 START
        $sql .="f_plus_coins='$f_plus_coins',f_address_flag='$f_address_flag', ";
        //落札者の声対応
        $sql .="fk_products_template_id='$f_products_template_id' ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //t_products
        $sql  = "update auction.t_products set ";
        $sql .= "f_status='$f_status',f_now_price='$f_start_price',";
        $sql .= "f_auction_time='$f_auction_time',f_start_time='$f_start_time' ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		開催中商品更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function UpdateTProductsOpen($fk_products_id,$f_products_name,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_recmmend,$f_status,$f_address_flag,$f_products_template_id) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //写真更新文字列
        $update_photo  = "";
        if($f_photo1 != "")		$update_photo .= "f_photo1='$f_photo1',";
        if($f_photo2 != "")		$update_photo .= "f_photo2='$f_photo2',";
        if($f_photo3 != "")		$update_photo .= "f_photo3='$f_photo3',";
        if($f_photo4 != "")		$update_photo .= "f_photo4='$f_photo4',";
        if($f_photo5 != "")		$update_photo .= "f_photo5='$f_photo5',";
        if($f_photo1mb != "")	$update_photo .= "f_photo1mb='$f_photo1mb',";
        if($f_photo2mb != "")	$update_photo .= "f_photo2mb='$f_photo2mb',";
        if($f_photo3mb != "")	$update_photo .= "f_photo3mb='$f_photo3mb',";
        if($f_photo4mb != "")	$update_photo .= "f_photo4mb='$f_photo4mb',";
        if($f_photo5mb != "")	$update_photo .= "f_photo5mb='$f_photo5mb',";

        //t_products_master
        $sql  = "update auction.t_products_master set ";
        $sql .= "f_products_name='$f_products_name',";
        $sql .= $update_photo;
        $sql .= "f_top_description_pc='$f_top_description_pc',f_main_description_pc='$f_main_description_pc',";
        $sql .= "f_top_description_mb='$f_top_description_mb',f_main_description_mb='$f_main_description_mb',";
        $sql .= "f_min_price='$f_min_price',";
        $sql .= "f_r_min_price='$f_r_min_price',f_r_max_price='$f_r_max_price',";
        $sql .= "f_market_price='$f_market_price',";
        $sql .= "f_recmmend='$f_recmmend', ";
        //ポイントオークション対応 START
        $sql .="f_address_flag='$f_address_flag', ";
        //落札者の声対応
        $sql .="fk_products_template_id='$f_products_template_id' ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //t_products
        $sql  = "update auction.t_products set ";
        $sql .= "f_status='$f_status' ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品情報削除関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function DeleteTProductsData($fk_products_id) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_master
        $sql = "update auction.t_products_master set f_delflg=9 where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //t_products
        $sql = "update auction.t_products set f_status=9 where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品写真削除関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function DeleteTProductsPhoto($fk_products_id, $photo_num) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $feeld_pc = "f_photo".$photo_num;
        $feeld_mb = "f_photo".$photo_num."mb";

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_master
        $sql = "update auction.t_products_master set $feeld_pc='',$feeld_mb='' where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品金額取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsPrice($fk_products_id,&$f_start_price,&$f_min_price,&$f_r_min_price,&$f_r_max_price,&$f_market_price,&$f_now_price,$db) {
        //■ＤＢ接続
//        $db=db_connect();
        if($db == false) {
            exit;
        }

        //t_products_master
        $sql  = "select f_start_price,f_min_price,f_r_min_price,f_r_max_price,f_market_price ";
        $sql .= "from auction.t_products_master where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
//            print "$sql<BR>\n";
//            //■ＤＢ切断
//            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $tpm_f_start_price	= mysql_result($result, 0, "f_start_price");
            $tpm_f_min_price	= mysql_result($result, 0, "f_min_price");
            $tpm_f_r_min_price	= mysql_result($result, 0, "f_r_min_price");
            $tpm_f_r_max_price	= mysql_result($result, 0, "f_r_max_price");
            $tpm_f_market_price	= mysql_result($result, 0, "f_market_price");
        }
        else {
            $tpm_f_start_price	= "";
            $tpm_f_min_price	= "";
            $tpm_f_r_min_price	= "";
            $tpm_f_r_max_price	= "";
            $tpm_f_market_price	= "";
        }

        //t_products
        $sql = "select f_now_price from auction.t_products where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
//            print "$sql<BR>\n";
//            //■ＤＢ切断
//            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $tp_f_now_price	= mysql_result($result, 0, "f_now_price");
        }
        else {
            $tp_f_now_price	= "";
        }

//        //■ＤＢ切断
//        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品ポイントオークション時加算枚数取得関数												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsPlusCoins($fk_products_id,&$f_plus_coins) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_plus_coins from auction.t_products_master ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_plus_coins = mysql_result($result, 0, "f_plus_coins");
        }
        else {
            $f_plus_coins = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }


/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品ポイントオークション時加算枚数取得関数(会員落札商品入金用)												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsPlusCoins_money($fk_products_id,&$f_plus_coins,$db) {
        //■ＤＢ接続
 //       $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_plus_coins from auction.t_products_master ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
//            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_plus_coins = mysql_result($result, 0, "f_plus_coins");
        }
        else {
            $f_plus_coins = "";
        }

        //■ＤＢ切断
 //       db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品名取得関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsName($fk_products_id,&$f_products_name) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_products_name from auction.t_products_master ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_products_name = mysql_result($result, 0, "f_products_name");
        }
        else {
            $f_products_name = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品指定写真取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsSrcPhoto($fk_products_id,$photo_num,&$f_photo,&$f_photo_mb) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $photo_pc = "f_photo".$photo_num;
        $photo_mb = "f_photo".$photo_num."mb";

        $sql  = "select $photo_pc,$photo_mb from auction.t_products_master ";
        $sql .= "where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_photo	= mysql_result($result, 0, $photo_pc);
            $f_photo_mb	= mysql_result($result, 0, $photo_mb);
        }
        else {
            $f_photo	= "";
            $f_photo_mb	= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品アドレスフラグ取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsAddressFlag($fk_products_id,&$f_address_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_address_flag from auction.t_products_master ";
        $sql .= "where fk_products_id='$fk_products_id' ";
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_address_flag	= mysql_result($result, 0, "f_address_flag");
        }
        else {
            $f_address_flag	= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート一覧取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

//G.Chin AREQ-437 2010-12-01 chg sta
//	function GetTProductsTemplateList($inp_category,$limit,$offset,&$fk_products_template_id,&$fk_item_category_id,&$f_products_name,&$f_photo1,&$f_market_price,&$all_count,&$data_cnt,$sort)
	function GetTProductsTemplateList($inp_category,$inp_name,$limit,$offset,&$fk_products_template_id,&$fk_item_category_id,&$f_products_name,&$f_photo1,&$f_market_price,&$all_count,&$data_cnt,$sort)
//G.Chin AREQ-437 2010-12-01 chg end
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//カテゴリ
		if($inp_category == "")
		{
			$where_category = "";
		}
		else
		{
			$where_category = "and fk_item_category_id='$inp_category' ";
		}

//G.Chin AREQ-437 2010-12-01 add sta
		//商品名
		if($inp_name == "")
		{
			$where_name = "";
		}
		else
		{
			$where_name = "and f_products_name like '%$inp_name%' ";
		}
//G.Chin AREQ-437 2010-12-01 add end

		//ソート順
		switch($sort) {
			case 0:
				$order_sort ="f_market_price ";
				break;
			case 1:
				$order_sort ="f_market_price desc ";
				break;
			case 2:
				$order_sort ="f_tm_stamp ";
				break;
			case 3:
				$order_sort ="f_tm_stamp desc ";
				break;
		}

		$sql_cnt  = "select count( fk_products_template_id ) from auction.t_products_template ";
		$sql_cnt .= "where f_status!=9 ";
		$sql_cnt .= $where_category;
		$sql_cnt .= $where_name;	//G.Chin AREQ-437 2010-12-01 add

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		$sql  = "select fk_products_template_id,fk_item_category_id,";
		$sql .= "f_products_name,f_photo1,f_market_price ";
		$sql .= "from auction.t_products_template ";
		$sql .= "where f_status!=9 ";
		$sql .= $where_category;
		$sql .= $where_name;					//G.Chin AREQ-437 2010-12-01 add
//		$sql .= "order by f_tm_stamp desc ";	//G.Chin AREQ-437 2010-12-01 del
		$sql .= "order by $order_sort ";
		$sql .= "limit $limit offset $offset";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_products_template_id[$i]	= mysql_result($result, $i, "fk_products_template_id");
			$fk_item_category_id[$i]		= mysql_result($result, $i, "fk_item_category_id");
			$f_products_name[$i]			= mysql_result($result, $i, "f_products_name");
			$f_photo1[$i]					= mysql_result($result, $i, "f_photo1");
			$f_market_price[$i]				= mysql_result($result, $i, "f_market_price");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTProductsTemplateInfo($fk_products_template_id,&$tpt_f_products_name,&$tpt_fk_item_category_id,&$tpt_f_photo1,&$tpt_f_photo2,&$tpt_f_photo3,&$tpt_f_photo4,&$tpt_f_photo5,&$tpt_f_photo1mb,&$tpt_f_photo2mb,&$tpt_f_photo3mb,&$tpt_f_photo4mb,&$tpt_f_photo5mb,&$tpt_f_top_description_pc,&$tpt_f_main_description_pc,&$tpt_f_top_description_mb,&$tpt_f_main_description_mb,&$tpt_f_market_price,&$tpt_f_min_price,&$tpt_f_start_price,&$tpt_f_r_min_price,&$tpt_f_r_max_price,&$tpt_f_status,&$tpt_f_memo,&$tpt_f_tm_stamp,&$tpt_f_plus_coins,&$tpt_f_address_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_products_name,fk_item_category_id,";
        $sql .= "f_products_name,fk_item_category_id,";
        $sql .= "f_photo1,f_photo2,f_photo3,f_photo4,f_photo5,";
        $sql .= "f_photo1mb,f_photo2mb,f_photo3mb,f_photo4mb,f_photo5mb,";
        $sql .= "f_top_description_pc,f_main_description_pc,";
        $sql .= "f_top_description_mb,f_main_description_mb,";
        $sql .= "f_market_price,f_min_price,f_start_price,";
        $sql .= "f_r_min_price,f_r_max_price,f_status,f_memo,f_tm_stamp, ";
        $sql .= "f_plus_coins,f_address_flag ";
        $sql .= "from auction.t_products_template where fk_products_template_id='$fk_products_template_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $tpt_f_products_name		= mysql_result($result, 0, "f_products_name");
            $tpt_fk_item_category_id	= mysql_result($result, 0, "fk_item_category_id");
            $tpt_f_photo1				= mysql_result($result, 0, "f_photo1");
            $tpt_f_photo2				= mysql_result($result, 0, "f_photo2");
            $tpt_f_photo3				= mysql_result($result, 0, "f_photo3");
            $tpt_f_photo4				= mysql_result($result, 0, "f_photo4");
            $tpt_f_photo5				= mysql_result($result, 0, "f_photo5");
            $tpt_f_photo1mb				= mysql_result($result, 0, "f_photo1mb");
            $tpt_f_photo2mb				= mysql_result($result, 0, "f_photo2mb");
            $tpt_f_photo3mb				= mysql_result($result, 0, "f_photo3mb");
            $tpt_f_photo4mb				= mysql_result($result, 0, "f_photo4mb");
            $tpt_f_photo5mb				= mysql_result($result, 0, "f_photo5mb");
            $tpt_f_top_description_pc	= mysql_result($result, 0, "f_top_description_pc");
            $tpt_f_main_description_pc	= mysql_result($result, 0, "f_main_description_pc");
            $tpt_f_top_description_mb	= mysql_result($result, 0, "f_top_description_mb");
            $tpt_f_main_description_mb	= mysql_result($result, 0, "f_main_description_mb");
            $tpt_f_market_price			= mysql_result($result, 0, "f_market_price");
            $tpt_f_min_price			= mysql_result($result, 0, "f_min_price");
            $tpt_f_start_price			= mysql_result($result, 0, "f_start_price");
            $tpt_f_r_min_price			= mysql_result($result, 0, "f_r_min_price");
            $tpt_f_r_max_price			= mysql_result($result, 0, "f_r_max_price");
            $tpt_f_status				= mysql_result($result, 0, "f_status");
            $tpt_f_memo					= mysql_result($result, 0, "f_memo");
            $tpt_f_tm_stamp				= mysql_result($result, 0, "f_tm_stamp");
            $tpt_f_plus_coins			= mysql_result($result, 0, "f_plus_coins");
            $tpt_f_address_flag			= mysql_result($result, 0, "f_address_flag");
        }
        else {
            $tpt_f_products_name		= "";
            $tpt_fk_item_category_id	= "";
            $tpt_f_photo1				= "";
            $tpt_f_photo2				= "";
            $tpt_f_photo3				= "";
            $tpt_f_photo4				= "";
            $tpt_f_photo5				= "";
            $tpt_f_photo1mb				= "";
            $tpt_f_photo2mb				= "";
            $tpt_f_photo3mb				= "";
            $tpt_f_photo4mb				= "";
            $tpt_f_photo5mb				= "";
            $tpt_f_top_description_pc	= "";
            $tpt_f_main_description_pc	= "";
            $tpt_f_top_description_mb	= "";
            $tpt_f_main_description_mb	= "";
            $tpt_f_market_price			= "";
            $tpt_f_min_price			= "";
            $tpt_f_start_price			= "";
            $tpt_f_r_min_price			= "";
            $tpt_f_r_max_price			= "";
            $tpt_f_status				= "";
            $tpt_f_memo					= "";
            $tpt_f_tm_stamp				= "";
            $tpt_f_plus_coins			= "";
            $tpt_f_address_flag			= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート登録関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function RegistTProductsTemplate($f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_plus_coins,$f_address_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_template
    /*
                    $sql  = "insert into auction.t_products_template values ";
                    $sql .= "(0,'$f_products_name','$fk_item_category_id',";
                    $sql .= "'$f_photo1','$f_photo2','$f_photo3','$f_photo4',";
                    $sql .= "'$f_top_description_pc','$f_main_description_pc',";
                    $sql .= "'$f_top_description_mb','$f_main_description_mb',";
                    $sql .= "'$f_market_price','$f_min_price','$f_start_price',";
                    $sql .= "'$f_r_min_price','$f_r_max_price',";
                    $sql .= "'0','',now()) ";
        */
        $sql  = "insert into auction.t_products_template ";
        $sql .= "(fk_products_template_id,f_products_name,fk_item_category_id,";
        $sql .= "f_photo1,f_photo2,f_photo3,f_photo4,f_photo5,";
        $sql .= "f_photo1mb,f_photo2mb,f_photo3mb,f_photo4mb,f_photo5mb,";
        $sql .= "f_top_description_pc,f_main_description_pc,";
        $sql .= "f_top_description_mb,f_main_description_mb,";
        $sql .= "f_market_price,f_min_price,f_start_price,";
        $sql .= "f_plus_coins,f_address_flag,";
        $sql .= "f_r_min_price,f_r_max_price,f_status,f_tm_stamp) ";
        $sql .= "values ";
        $sql .= "(0,'$f_products_name','$fk_item_category_id',";
        $sql .= "'$f_photo1','$f_photo2','$f_photo3','$f_photo4','$f_photo5',";
        $sql .= "'$f_photo1mb','$f_photo2mb','$f_photo3mb','$f_photo4mb','$f_photo5mb',";
        $sql .= "'$f_top_description_pc','$f_main_description_pc',";
        $sql .= "'$f_top_description_mb','$f_main_description_mb',";
        $sql .= "'$f_market_price','$f_min_price','$f_start_price',";
        $sql .= "'$f_plus_coins','$f_address_flag',";
        $sql .= "'$f_r_min_price','$f_r_max_price','0',now()) ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function UpdateTProductsTemplate($fk_products_template_id,$f_products_name,$fk_item_category_id,$f_photo1,$f_photo2,$f_photo3,$f_photo4,$f_photo5,$f_photo1mb,$f_photo2mb,$f_photo3mb,$f_photo4mb,$f_photo5mb,$f_top_description_pc,$f_main_description_pc,$f_top_description_mb,$f_main_description_mb,$f_start_price,$f_min_price,$f_r_min_price,$f_r_max_price,$f_market_price,$f_plus_coins,$f_address_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //写真更新文字列
        $update_photo  = "";
        if($f_photo1 != "")	$update_photo .= "f_photo1='$f_photo1',";
        if($f_photo2 != "")	$update_photo .= "f_photo2='$f_photo2',";
        if($f_photo3 != "")	$update_photo .= "f_photo3='$f_photo3',";
        if($f_photo4 != "")	$update_photo .= "f_photo4='$f_photo4',";
        if($f_photo5 != "")	$update_photo .= "f_photo5='$f_photo5',";
        if($f_photo1mb != "")	$update_photo .= "f_photo1mb='$f_photo1mb',";
        if($f_photo2mb != "")	$update_photo .= "f_photo2mb='$f_photo2mb',";
        if($f_photo3mb != "")	$update_photo .= "f_photo3mb='$f_photo3mb',";
        if($f_photo4mb != "")	$update_photo .= "f_photo4mb='$f_photo4mb',";
        if($f_photo5mb != "")	$update_photo .= "f_photo5mb='$f_photo5mb',";

        //t_products_template
        $sql  = "update auction.t_products_template set ";
        $sql .= "f_products_name='$f_products_name',fk_item_category_id='$fk_item_category_id',";
        $sql .= $update_photo;
        $sql .= "f_top_description_pc='$f_top_description_pc',f_main_description_pc='$f_main_description_pc',";
        $sql .= "f_top_description_mb='$f_top_description_mb',f_main_description_mb='$f_main_description_mb',";
        $sql .= "f_start_price='$f_start_price',f_min_price='$f_min_price',";
        $sql .= "f_r_min_price='$f_r_min_price',f_r_max_price='$f_r_max_price',";
        $sql .= "f_market_price='$f_market_price',";
        $sql .= "f_plus_coins ='$f_plus_coins',f_address_flag='$f_address_flag' ";
        $sql .= "where fk_products_template_id='$fk_products_template_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート削除関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function DeleteTProductsTemplateData($fk_products_template_id) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_template
        $sql = "update auction.t_products_template set f_status=9 where fk_products_template_id='$fk_products_template_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品テンプレート写真削除関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function DeleteTProductsTemplatePhoto($fk_products_template_id, $photo_num) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $feeld_pc = "f_photo".$photo_num;
        $feeld_mb = "f_photo".$photo_num."mb";

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_master
        $sql = "update auction.t_products_template set $feeld_pc='',$feeld_mb='' where fk_products_template_id='$fk_products_template_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品一覧取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsList($inp_status,$inp_stock,$inp_pickup,$limit,$offset,&$fk_end_products_id,&$f_products_name,&$f_photo1,&$f_status,&$f_stock,&$f_end_price,&$f_bit_buy_count,&$f_bit_free_count,&$tep_f_bit_r_count,&$f_last_bidder,&$f_end_time,&$all_count,&$data_cnt,$pid,$sort) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //終了状態
        if(strlen($inp_status)) {
            $where_status = "and f_status='$inp_status' ";
        }
        else {
            $where_status = "";
        }

        //在庫・発注状態
        if(($inp_stock == "") || ($inp_stock == 0)) {
            $where_stock = "";
        }
        else {
            $where_stock = "and f_stock='$inp_stock' ";
        }

        //ピックアップ商品
        if($inp_pickup == "1") {
            $where_pickup = "and f_pickup_flg='$inp_pickup' ";
        }
        else {
            $where_pickup = "";
        }

        //商品ID
        if($pid == "") {
            $where_pid = "";
        }
        else {
            $where_pid = "and fk_end_products_id = $pid ";
        }

        $sql_cnt  = "select count(fk_end_products_id) from auction.t_end_products ";
        $sql_cnt .= "where fk_end_products_id>0 ";
        $sql_cnt .= $where_status;
        $sql_cnt .= $where_stock;
        $sql_cnt .= $where_pickup;
        $sql_cnt .= $where_pid;

        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);

        switch($sort) {
            case 0:
                $order_sort = "order by f_end_time desc ";
                break;
            case 1:
                $order_sort = "order by fk_end_products_id ";
                break;
            case 2:
                $order_sort = "order by fk_end_products_id desc ";
                break;
            case 3:
                $order_sort = "order by f_products_name ";
                break;
            case 4:
                $order_sort = "order by f_products_name desc ";
                break;
        }

        $sql  = "select fk_end_products_id,f_products_name, ";
        $sql .= "f_photo1,f_status,f_stock,f_end_price,";
        $sql .= "f_bit_buy_count,f_bit_free_count,f_last_bidder,f_end_time,f_bit_r_count ";
        $sql .= "from auction.t_end_products ";
        $sql .= "where fk_end_products_id>0 ";
        $sql .= $where_status;
        $sql .= $where_stock;
        $sql .= $where_pickup;
        $sql .= $where_pid;
        $sql .= $order_sort;
        $sql .= "limit $limit offset $offset";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_end_products_id[$i]	= mysql_result($result, $i, "fk_end_products_id");
            $f_products_name[$i]	= mysql_result($result, $i, "f_products_name");
            $f_photo1[$i]			= mysql_result($result, $i, "f_photo1");
            $f_status[$i]			= mysql_result($result, $i, "f_status");
            $f_stock[$i]			= mysql_result($result, $i, "f_stock");
            $f_end_price[$i]		= mysql_result($result, $i, "f_end_price");
            $f_bit_buy_count[$i]	= mysql_result($result, $i, "f_bit_buy_count");
            $f_bit_free_count[$i]	= mysql_result($result, $i, "f_bit_free_count");
            $f_last_bidder[$i]		= mysql_result($result, $i, "f_last_bidder");
            $f_end_time[$i]			= mysql_result($result, $i, "f_end_time");
            $tep_f_bit_r_count[$i]      = mysql_result($result, $i, "f_bit_r_count");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品会員グループ指定一覧取得関数										*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsSrcMemGroupList($inp_status,$inp_stock,$inp_pickup,$inp_group,$limit,$offset,&$fk_end_products_id,&$f_products_name,&$f_photo1,&$f_status,&$f_stock,&$f_end_price,&$f_bit_buy_count,&$f_bit_r_count,&$f_bit_free_count,&$f_last_bidder,&$f_end_time,&$all_count,&$data_cnt,$pid,$sort) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //終了状態
        if( strlen($inp_status) ) {
            $where_status = "and ep.f_status='$inp_status' ";
        }
        else {
            $where_status = "";
        }

        //在庫・発注状態
        if(($inp_stock == "") || ($inp_stock == 0)) {
            $where_stock = "";
        }
        else {
            $where_stock = "and ep.f_stock='$inp_stock' ";
        }

        //ピックアップ商品
        if($inp_pickup == "1") {
            $where_pickup = "and ep.f_pickup_flg='$inp_pickup' ";
        }
        else {
            $where_pickup = "";
        }

        //商品ID
        if($pid == "") {
            $where_pid = "";
        }
        else {
            $where_pid = "and ep.fk_end_products_id=$pid ";
        }

//        $sql_cnt  = "select count(fk_end_products_id) from auction.t_end_products as ep,auction.t_member_master as mm ";
//        $sql_cnt .= "where ep.fk_end_products_id>0 ";
//        $sql_cnt .= "and mm.fk_member_group_id='$inp_group' ";
//        $sql_cnt .= $where_status;
//        $sql_cnt .= $where_stock;
//        $sql_cnt .= $where_pickup;
//        $sql_cnt .= $where_pid;
//        $sql_cnt .= "and ep.f_last_bidder=mm.fk_member_id ";

        $sql_cnt = "
SELECT
  COUNT(fk_end_products_id)
FROM
(
  SELECT
    fk_end_products_id,f_last_bidder
  FROM
    auction.t_end_products AS ep
  WHERE
    ep.fk_end_products_id>0 and f_last_bidder  > 10000
    ".$where_status."
    ".$where_stock."
    ".$where_pickup."
    ".$where_pid."
) AS ep2
INNER JOIN
(
  SELECT
    mm.fk_member_id
  FROM
    auction.t_member_master as mm
  WHERE
    mm.fk_member_group_id='".$inp_group."'
) AS mm2 ON ep2.f_last_bidder=mm2.fk_member_id
";
//echo $sql_cnt.'<BR>';
        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);

        switch($sort) {
            case 0:
                $order_sort = "order by ep2.f_end_time desc ";
                break;
            case 1:
                $order_sort = "order by ep2.fk_end_products_id ";
                break;
            case 2:
                $order_sort = "order by ep2.fk_end_products_id desc ";
                break;
            case 3:
                $order_sort = "order by ep2.f_products_name ";
                break;
            case 4:
                $order_sort = "order by ep2.f_products_name desc ";
                break;
        }

//        $sql  = "select ep.fk_end_products_id,ep.f_products_name,";
//        $sql .= "ep.f_photo1,ep.f_status,ep.f_stock,ep.f_end_price,";
//        $sql .= "ep.f_bit_buy_count,ep.f_bit_free_count,ep.f_last_bidder,ep.f_end_time ";
//        $sql .= "from auction.t_end_products as ep,auction.t_member_master as mm ";
//        $sql .= "where ep.fk_end_products_id>0 ";
//        $sql .= "and mm.fk_member_group_id='$inp_group' ";
//        $sql .= $where_status;
//        $sql .= $where_stock;
//        $sql .= $where_pickup;
//        $sql .= $where_pid;
//        $sql .= "and ep.f_last_bidder=mm.fk_member_id ";
//        $sql .= $order_sort;
//        $sql .= "limit $limit offset $offset";

        $sql = "
SELECT
  ep2.*
  ,mm2.*
FROM
(
  SELECT
    ep.fk_end_products_id,ep.f_last_bidder,ep.f_products_name,ep.f_photo1,ep.f_status
    ,ep.f_stock,ep.f_end_price,ep.f_bit_buy_count,ep.f_bit_free_count,ep.f_end_time,ep.f_bit_r_count
  FROM
    auction.t_end_products AS ep
  WHERE
    ep.fk_end_products_id>0 and f_last_bidder  > 10000
    ".$where_status."
    ".$where_stock."
    ".$where_pickup."
    ".$where_pid."
) AS ep2
INNER JOIN
(
  SELECT
    mm.fk_member_id
  FROM
    auction.t_member_master as mm
  WHERE
    mm.fk_member_group_id='".$inp_group."'
) AS mm2 ON ep2.f_last_bidder=mm2.fk_member_id
".$order_sort."
LIMIT ".$limit." OFFSET ".$offset."
";
//echo $sql.'<BR>';
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_end_products_id[$i]	= mysql_result($result, $i, "fk_end_products_id");
            $f_products_name[$i]	= mysql_result($result, $i, "f_products_name");
            $f_photo1[$i]			= mysql_result($result, $i, "f_photo1");
            $f_status[$i]			= mysql_result($result, $i, "f_status");
            $f_stock[$i]			= mysql_result($result, $i, "f_stock");
            $f_end_price[$i]		= mysql_result($result, $i, "f_end_price");
            $f_bit_buy_count[$i]	= mysql_result($result, $i, "f_bit_buy_count");

            $f_bit_free_count[$i]	= mysql_result($result, $i, "f_bit_free_count");
            $f_last_bidder[$i]		= mysql_result($result, $i, "f_last_bidder");
            $f_end_time[$i]			= mysql_result($result, $i, "f_end_time");
            $f_bit_r_count[$i]	= mysql_result($result, $i, "f_bit_r_count");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsInfo($fk_end_products_id,&$f_products_name,&$fk_item_category_id,&$f_photo1,&$f_photo2,&$f_photo3,&$f_photo4,&$f_status,&$f_stock,&$fk_address_id,&$f_last_bidder,&$f_pickup_flg,&$f_end_time,&$f_address_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_products_name,fk_item_category_id,";
        $sql .= "f_photo1,f_photo2,f_photo3,f_photo4,";
        $sql .= "f_status,f_stock,f_address_flag,";
        $sql .= "f_last_bidder,f_pickup_flg,f_end_time,fk_address_id ";
        $sql .= "from auction.t_end_products where fk_end_products_id='$fk_end_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_products_name		= mysql_result($result, 0, "f_products_name");
            $fk_item_category_id	= mysql_result($result, 0, "fk_item_category_id");
            $f_photo1				= mysql_result($result, 0, "f_photo1");
            $f_photo2				= mysql_result($result, 0, "f_photo2");
            $f_photo3				= mysql_result($result, 0, "f_photo3");
            $f_photo4				= mysql_result($result, 0, "f_photo4");
            $f_status				= mysql_result($result, 0, "f_status");
            $f_stock				= mysql_result($result, 0, "f_stock");
            $fk_address_id			= mysql_result($result, 0, "fk_address_id");
            $f_last_bidder			= mysql_result($result, 0, "f_last_bidder");
            $f_pickup_flg			= mysql_result($result, 0, "f_pickup_flg");
            $f_end_time				= mysql_result($result, 0, "f_end_time");
            $f_address_flag			= mysql_result($result, 0, "f_address_flag");
        }
        else {
            $f_products_name		= "";
            $fk_item_category_id	= "";
            $f_photo1				= "";
            $f_photo2				= "";
            $f_photo3				= "";
            $f_photo4				= "";
            $f_status				= "";
            $f_stock				= "";
            $fk_address_id			= "";
            $f_last_bidder			= "";
            $f_pickup_flg			= "";
            $f_end_time				= "";
            $f_address_flag			= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品金額取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsPrice($fk_end_products_id,&$f_start_price,&$f_end_price,&$f_market_price) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_start_price,f_end_price,f_market_price ";
        $sql .= "from auction.t_end_products where fk_end_products_id='$fk_end_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_start_price	= mysql_result($result, 0, "f_start_price");
            $f_end_price	= mysql_result($result, 0, "f_end_price");
            $f_market_price	= mysql_result($result, 0, "f_market_price");
        }
        else {
            $f_start_price	= "";
            $f_end_price	= "";
            $f_market_price	= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品名取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsName($fk_end_products_id,&$f_products_name) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_products_name ";
        $sql .= "from auction.t_end_products where fk_end_products_id='$fk_end_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_products_name = mysql_result($result, 0, "f_products_name");
        }
        else {
            $f_products_name = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品アドレスフラグ取得関数												*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsAddressFlag($fk_end_products_id,&$tpm_f_address_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql = "select f_address_flag from auction.t_end_products where fk_end_products_id='$fk_end_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $tpm_f_address_flag	= mysql_result($result, 0, "f_address_flag");
        }
        else {
            $tpm_f_address_flag	= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品状態更新関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function SetTEndProductsStatusStock($fk_end_products_id, $f_status, $f_stock, $f_pickup_flg, $db) {
        //■ＤＢ接続
//            $db=db_connect();
            if($db == false) {
                echo "DB error";
                exit;
            }
//            if($db==false) {
//                exit;
//            }else {

//            //■トランザクション開始
//            mysql_query("BEGIN", $db);

            //t_end_products
            $sql  = "update auction.t_end_products set f_status='$f_status',f_stock='$f_stock',f_pickup_flg='$f_pickup_flg' ";
            $sql .= "where fk_end_products_id='$fk_end_products_id' ";
            //■ＳＱＬ実行
            $db=db_connect();
            $result = mysql_query($sql, $db);
            if( $result == false ) {
//                print "$sql<BR>\n";
//                //■ロールバック
//                mysql_query("ROLLBACK", $db);
//                //■ＤＢ切断
//                db_close($db);
                echo "return false";
                return false;
            }
//            //■コミット
//            mysql_query("COMMIT", $db);
//
//            //■ＤＢ切断
//            db_close($db);
            return true;
//        }
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品状態価格更新関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function SetTEndProductsStatusPrice($fk_end_products_id, $f_status, $f_stock, $f_pickup_flg, $f_min_price) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_end_products
        $sql  = "update auction.t_end_products set f_status='$f_status',f_stock='$f_stock',f_pickup_flg='$f_pickup_flg',f_min_price='$f_min_price' ";
        $sql .= "where fk_end_products_id='$fk_end_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return 1;
        }
        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return 0;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション金額上昇下降取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetRStatus($fk_auction_type_id,&$f_rev_flag) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //t_auction_type
        $sql  = "select f_rev_flag from auction.t_auction_type ";
        $sql .= "where fk_auction_type_id='$fk_auction_type_id' ";
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            db_close($db);
            return false;
        }

        $num=mysql_num_rows($result);
        if($num != 1) {
            return false;
        }
        $ret=mysql_fetch_array($result);
        $f_rev_flag=$ret['f_rev_flag'];

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品未払い件数取得関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTEndProductsNotPayCount($f_last_bidder, &$all_count)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql_cnt  = "select count(*) from auction.t_end_products ";
//G.Chin AWKT-933 2010-12-17 chg sta
//		$sql_cnt .= "where f_last_bidder='$f_last_bidder' and f_status<'2' ";
		$sql_cnt .= "where f_last_bidder='$f_last_bidder' and f_status=0 ";
//G.Chin AWKT-933 2010-12-17 chg end

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		最終入札者指定オークション終了商品一覧検索関数											*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function SrcLstBidderTEndProductsList($fk_member_id,$limit,$offset,&$fk_end_products_id,&$f_products_name,&$f_photo1,&$f_status,&$f_stock,&$f_end_price,&$f_bit_buy_count,&$f_bit_free_count,&$f_last_bidder,&$f_end_time,&$all_count,&$data_cnt) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql_cnt  = "select count(*) from auction.t_end_products ";
        $sql_cnt .= "where f_last_bidder='$fk_member_id' and f_status!=9 ";

        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);

        $sql  = "select fk_end_products_id,f_products_name,";
        $sql .= "f_photo1,f_status,f_stock,f_end_price,";
        $sql .= "f_bit_buy_count,f_bit_free_count,f_last_bidder,f_end_time ";
        $sql .= "from auction.t_end_products ";
        $sql .= "where f_last_bidder='$fk_member_id' and f_status!=9 ";
        $sql .= "order by f_status,fk_end_products_id desc ";
        $sql .= "limit $limit offset $offset";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_end_products_id[$i]	= mysql_result($result, $i, "fk_end_products_id");
            $f_products_name[$i]	= mysql_result($result, $i, "f_products_name");
            $f_photo1[$i]			= mysql_result($result, $i, "f_photo1");
            $f_status[$i]			= mysql_result($result, $i, "f_status");
            $f_stock[$i]			= mysql_result($result, $i, "f_stock");
            $f_end_price[$i]		= mysql_result($result, $i, "f_end_price");
            $f_bit_buy_count[$i]	= mysql_result($result, $i, "f_bit_buy_count");
            $f_bit_free_count[$i]	= mysql_result($result, $i, "f_bit_free_count");
            $f_last_bidder[$i]		= mysql_result($result, $i, "f_last_bidder");
            $f_end_time[$i]			= mysql_result($result, $i, "f_end_time");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品仕入価格取得関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsMinPrice($fk_end_products_id, &$f_min_price) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_min_price ";
        $sql .= "from auction.t_end_products ";
        $sql .= "where fk_end_products_id='$fk_end_products_id' ";
        //echo $sql;
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_min_price = mysql_result($result, 0, "f_min_price");
        }
        else {
            $f_min_price = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品メモ取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsMemo($fk_end_products_id, &$f_memo) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_memo ";
        $sql .= "from auction.t_end_products ";
        $sql .= "where fk_end_products_id='$fk_end_products_id' ";
        //echo $sql;
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_memo = mysql_result($result, 0, "f_memo");
        }
        else {
            $f_memo = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品メモ更新関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function UpdateTEndProductsMemo($fk_end_products_id, $f_memo) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_end_products
        $sql  = "update auction.t_end_products set f_memo='$f_memo' ";
        $sql .= "where fk_end_products_id='$fk_end_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		入札商品名取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetBidProductsName($fk_products_id,&$f_products_name) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //t_products
        $sql  = "select f_status ";
        $sql .= "from auction.t_products where fk_products_id='$fk_products_id' ";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_status = mysql_result($result, 0, "f_status");
        }
        else {
            $f_status = "";
        }

        //状態を判定
        if($f_status == 2) {
            //▼終了商品
            //t_end_products
            $sql  = "select f_products_name ";
            $sql .= "from auction.t_end_products where fk_end_products_id='$fk_products_id' ";
        }
        else {
            //▼開催中商品
            //t_products_master
            $sql  = "select f_products_name ";
            $sql .= "from auction.t_products_master where fk_products_id='$fk_products_id' ";
        }

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_products_name = mysql_result($result, 0, "f_products_name");
        }
        else {
            $f_products_name = "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		仕入価格集計関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function SetCostPay($pid,$sid,$mem_id,$cost) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            return 1;
        }

        //■トランザクション開始


        //▼指定商品の支払履歴を検索
        $sql = "select fk_pay_log_id from auction.t_pay_log where f_member_id = $mem_id and fk_products_id='$pid' and f_cert_status =0";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return 1;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows == 0) {
            //■ＤＢ切断
            db_close($db);
            return 2;
        }
        else {
            $fk_pay_log_id	= mysql_result($result, 0, "fk_pay_log_id");
        }

        //▼指定商品の支払履歴を検索
        $sql = "select fk_pay_log_id from auction.t_pay_log where f_status=4  and f_member_id = $mem_id and fk_products_id='$pid' and f_cert_status =0";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return 1;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows != 0) {
            //■ＤＢ切断
            db_close($db);
            return 3;
        }

        $sql = "insert into auction.t_pay_log(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,fk_products_id,f_tm_stamp)
                        values(4,$mem_id,$sid,$cost,0,2,$pid,now())";
    /*
                //▼指定商品の支払履歴を更新
                $sql  = "update auction.t_pay_log set ";
                $sql .= "f_staff_id='$sid',f_min_price='$cost' ";
                $sql .= "where fk_pay_log_id='$fk_pay_log_id'";
        */
        mysql_query("BEGIN", $db);
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return 1;
        }

        $sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d %H:00:00'),0,$cost)";
        $result = mysql_query($sql, $db);
        $check = mysql_affected_rows();
        if ( $check == -1 ) {
            mysql_query("ROLLBACK", $db);
            db_close($db);
            return false;
        }
        if ( $check == 0 ) {
        //if( $result == false ) {
            //print "$sql<BR>\n";
            $sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d %H:00:00') and f_type = 0";
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ロールバック
                mysql_query("ROLLBACK", $db);
                //■ＤＢ切断
                db_close($db);
                return 1;
            }
        }

        $sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d 23:00:00'),1,$cost)";
        $result = mysql_query($sql, $db);
        $check = mysql_affected_rows();
        if ( $check == -1 ) {
            mysql_query("ROLLBACK", $db);
            db_close($db);
            return false;
        }
        if ( $check == 0 ) {
        //if( $result == false ) {
            //print "$sql<BR>\n";
            $sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d 23:00:00') and f_type = 1";
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ロールバック
                mysql_query("ROLLBACK", $db);
                //■ＤＢ切断
                db_close($db);
                return 1;
            }
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return 0;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		仕入価格集計関数２																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function SetCostPay2($pid,$sid,$mem_id,$cost,$f_coin_add,$f_coin_result,$end_price) {
        //$sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$mem_id'";
        //                $sql3  = "insert into auction.t_pay_log ";
        //		$sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
        //		$sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
        if(COIN_PACK_PLAN == 0) //全てフリー
        {
            //echo 1;
            $sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$mem_id'";
            $sql3  = "insert into auction.t_pay_log ";
            $sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
            $sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
        }
        else if(COIN_PACK_PLAN == 1) //全て課金
        {
            //echo 2;
            $sql = "update auction.t_member set f_coin=f_coin+'$f_coin_add' where fk_member_id='$mem_id'";
            $sql3  = "insert into auction.t_pay_log ";
            $sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_coin_add,f_coin_result,fk_shiharai_type_id) ";
            $sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
        }
        else if(COIN_PACK_PLAN == 2) //計算にて算出
        {
            //echo 3;
            $tmp_add_coin = floor($end_price/POINT_VALUE);
            $tmp_free_add_coin=$f_coin_add - $tmp_add_coin;
            if($tmp_add_coin >=$f_coin_add ) {
                $tmp_add_coin=$f_coin_add;
                $tmp_free_add_coin = 0;
            }
            $sql = "update auction.t_member set f_coin=f_coin+'$tmp_add_coin',f_free_coin=f_free_coin+'$tmp_free_add_coin' where fk_member_id='$mem_id'";
            $sql3  = "insert into auction.t_pay_log ";
            $sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_coin_add,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
            $sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$tmp_add_coin,$tmp_free_add_coin,$f_coin_result,1)";
            //echo "$sql<BR>";
            //echo $sql3;
        }
        //■ＤＢ接続
            $db=db_connect();
            if($db == false) {
                return false;
            }
    //
        //■トランザクション開始
        mysql_query("BEGIN", $db);
//        if($db==false) {
//            exit;
//        }else {

            //会員無料コイン枚数加算
            //$sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$mem_id'";
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ロールバック
                mysql_query("ROLLBACK", $db);
                //■ＤＢ切断
                db_close($db);
                return false;
            }

            //オークション終了商品の終了状態を更新
            $sql2 = "update auction.t_end_products set f_status=4 where fk_end_products_id='$pid'";
            $result = mysql_query($sql2, $db);
            if( $result == false ) {
                print "$sql2<BR>\n";
                //■ロールバック
                mysql_query("ROLLBACK", $db);
                //■ＤＢ切断
                db_close($db);
                return false;
            }

            //支払履歴作成
            //$sql  = "insert into auction.t_pay_log ";
            //$sql .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
            //$sql .= "values (2,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
            $result = mysql_query($sql3, $db);
            if( $result == false ) {
                print "$sql3<BR>\n";
                //■ロールバック
                mysql_query("ROLLBACK", $db);
                //■ＤＢ切断
                db_close($db);
                return false;
            }

            $sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d %H:00:00'),0,$cost)";
            $result = mysql_query($sql, $db);
            $check = mysql_affected_rows();
            if ( $check == -1 ) {
                mysql_query("ROLLBACK", $db);
                db_close($db);
                return false;
            }
            if ( $check == 0 ) {
            //if( $result == false ) {
                //print "$sql<BR>\n";
                $sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d %H:00:00') and f_type=0";
                $result = mysql_query($sql, $db);
                if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ロールバック
                    mysql_query("ROLLBACK", $db);
                    //■ＤＢ切断
                    db_close($db);
                    return false;
                }
            }

            $sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d 23:00:00'),1,$cost)";
            $result = mysql_query($sql, $db);
            $check = mysql_affected_rows();
            if ( $check == -1 ) {
                mysql_query("ROLLBACK", $db);
                db_close($db);
                return false;
            }
            if ( $check == 0 ) {
            //if( $result == false ) {
                //print "$sql<BR>\n";
                $sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d 23:00:00') and f_type=1";
                $result = mysql_query($sql, $db);
                if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ロールバック
                    mysql_query("ROLLBACK", $db);
                    //■ＤＢ切断
                    db_close($db);
                    return false;
                }
            }

            //■コミット
            mysql_query("COMMIT", $db);

            //■ＤＢ切断
            db_close($db);
            return true;

    }


    /*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
    /*		仕入価格集計関数２(会員落札商品入金用)																		*/
    /*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function SetCostPay2_money($pid,$sid,$mem_id,$cost,$f_coin_add,$f_coin_result,$end_price,$db) {
        //$sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$mem_id'";
        //                $sql3  = "insert into auction.t_pay_log ";
        //		$sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
        //		$sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
        if(COIN_PACK_PLAN == 0) //全てフリー
        {
            //echo 1;
            $sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$mem_id'";
            $sql3  = "insert into auction.t_pay_log ";
            $sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
            $sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
        }
        else if(COIN_PACK_PLAN == 1) //全て課金
        {
            //echo 2;
            $sql = "update auction.t_member set f_coin=f_coin+'$f_coin_add' where fk_member_id='$mem_id'";
            $sql3  = "insert into auction.t_pay_log ";
            $sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_coin_add,f_coin_result,fk_shiharai_type_id) ";
            $sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
        }
        else if(COIN_PACK_PLAN == 2) //計算にて算出
        {
            //echo 3;
            $tmp_add_coin = floor($end_price/POINT_VALUE);
            $tmp_free_add_coin=$f_coin_add - $tmp_add_coin;
            if($tmp_add_coin >=$f_coin_add ) {
                $tmp_add_coin=$f_coin_add;
                $tmp_free_add_coin = 0;
            }
            $sql = "update auction.t_member set f_coin=f_coin+'$tmp_add_coin',f_free_coin=f_free_coin+'$tmp_free_add_coin' where fk_member_id='$mem_id'";
            $sql3  = "insert into auction.t_pay_log ";
            $sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_coin_add,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
            $sql3 .= "values (3,$mem_id,$sid,$cost,0,2,now(),0,$pid,$tmp_add_coin,$tmp_free_add_coin,$f_coin_result,1)";
            //echo "$sql<BR>";
            //echo $sql3;
        }
        //■ＤＢ接続
   //         $db=db_connect();
            if($db == false) {
                return false;
            }
    //
        //■トランザクション開始
//            mysql_query("BEGIN", $db);
//        if($db==false) {
//            exit;
//        }else {

            //会員無料コイン枚数加算
            //$sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$mem_id'";
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ロールバック
//                mysql_query("ROLLBACK", $db);
//                //■ＤＢ切断
//                db_close($db);
                return false;
            }

            //オークション終了商品の終了状態を更新
            $sql2 = "update auction.t_end_products set f_status=4 where fk_end_products_id='$pid'";
            $result = mysql_query($sql2, $db);
            if( $result == false ) {
                print "$sql2<BR>\n";
                //■ロールバック
//                mysql_query("ROLLBACK", $db);
//                //■ＤＢ切断
//                db_close($db);
                return false;
            }

            //支払履歴作成
            //$sql  = "insert into auction.t_pay_log ";
            //$sql .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
            //$sql .= "values (2,$mem_id,$sid,$cost,0,2,now(),0,$pid,$f_coin_add,$f_coin_result,1)";
            $result = mysql_query($sql3, $db);
            if( $result == false ) {
                print "$sql3<BR>\n";
                //■ロールバック
//                mysql_query("ROLLBACK", $db);
//                //■ＤＢ切断
//                db_close($db);
                return false;
            }

            $sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d %H:00:00'),0,$cost)";
            $result = mysql_query($sql, $db);
            $check = mysql_affected_rows();
            if ( $check == -1 ) {
                mysql_query("ROLLBACK", $db);
                db_close($db);
                return false;
            }
            if ( $check == 0 ) {
            //if( $result == false ) {
                //print "$sql<BR>\n";
                $sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d %H:00:00') and f_type=0";
                $result = mysql_query($sql, $db);
                if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ロールバック
//                    mysql_query("ROLLBACK", $db);
//                    //■ＤＢ切断
//                    db_close($db);
                    return false;
                }
            }

            $sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d 23:00:00'),1,$cost)";
            $result = mysql_query($sql, $db);
            $check = mysql_affected_rows();
            if ( $check == -1 ) {
                mysql_query("ROLLBACK", $db);
                db_close($db);
                return false;
            }
            if ( $check == 0 ) {
            //if( $result == false ) {
                //print "$sql<BR>\n";
                $sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d 23:00:00') and f_type=1";
                $result = mysql_query($sql, $db);
                if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ロールバック
//                    mysql_query("ROLLBACK", $db);
//                    //■ＤＢ切断
//                    db_close($db);
                    return false;
                }
            }

            //■コミット
//            mysql_query("COMMIT", $db);
//
//            //■ＤＢ切断
//            db_close($db);
            return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品カテゴリ表示順取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
        function GetCategoryDspno() {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
//        mysql_query("BEGIN", $db);

        //t_products_template

        $sql  = "select max(f_dspno) AS max_f_dspno from auction.t_item_category";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }


        $max_f_dspno=0;
        $data_cnt = mysql_num_rows($result);
        if($data_cnt>0){
            $max_f_dspno=mysql_result($result,0,"max_f_dspno");
            $max_f_dspno++;
        }

        //■ＤＢ切断
        db_close($db);
        return $max_f_dspno;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品カテゴリ登録関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

        function RegistProductsCategory($f_category_name,$now,$f_status,$maxdspno) {
        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        //■トランザクション開始
        mysql_query("BEGIN", $db);

        //t_products_template

        $sql  = "insert into auction.t_item_category ";
        $sql .= "(f_category_name,f_mail_category_val,";
        $sql .= "f_tm_stamp,f_rbstp,f_rbedp,f_status,f_dspno) ";
        $sql .= "values ";
        $sql .= "('$f_category_name',0,'$now',";
        $sql .= "0,0,$f_status,$maxdspno)";


        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品カテゴリ一覧取得関数(商品カテゴリ編集ページ用)																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetItemCategoryList(&$fk_item_category, &$f_category_name, &$f_mail_category_val, &$f_tm_stamp, &$f_dspno, &$f_status, &$data_cnt ,&$all_count, $sort, $limit, $offset) {
        //■ＤＢ接続

        $db=db_connect();
        if($db == false) {
            exit;
        }
        $sql_cnt  = "select count(*) from auction.t_item_category as tic ";
        $sql_cnt .= "where tic.f_status=0 ";

        //print "$sql_cnt<BR>\n";
        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);
        if($sort==0){
            $order_sort="tic.f_dspno";
        }
//        if($sort==1){
//            $order_sort_1="tic.f_dspno";
//        }
//        if($sort==2){
//            $order_sort_2="tic.f_dspno 11111";
//        }
        $sql = "select tic.fk_item_category,tic.f_category_name,tic.f_mail_category_val,tic.f_tm_stamp,tic.f_dspno,tic.f_status from auction.t_item_category as tic where tic.f_status = 0 order by $order_sort limit $limit offset $offset";
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_item_category[$i]		= mysql_result($result, $i, "fk_item_category");
            $f_category_name[$i]		= mysql_result($result, $i, "f_category_name");
            $f_mail_category_val[$i]	= mysql_result($result, $i, "f_mail_category_val");
            $f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
            $f_dspno[$i]				= mysql_result($result, $i, "f_dspno");
            $f_status[$i]				= mysql_result($result, $i, "f_status");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }


    function GetItemCategoryList_1(&$fk_item_category, &$f_category_name, &$f_mail_category_val, &$f_tm_stamp, &$f_status, &$data_cnt ,&$all_count, $sort, $limit, $offset){
	        //■ＤＢ接続

        $db=db_connect();
        if($db == false) {
            exit;
        }
        $sql_cnt  = "select count(*) from auction.t_item_category as tic ";
        $sql_cnt .= "where tic.f_status=1 ";

        //print "$sql_cnt<BR>\n";
        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);
        if($sort==0){
            $order_sort="tic.f_dspno";
        }
//        if($sort==1){
//            $order_sort_1="tic.f_dspno";
//        }
//        if($sort==2){
//            $order_sort_2="tic.f_dspno 11111";
//        }
        $sql = "select tic.fk_item_category,tic.f_category_name,tic.f_mail_category_val,tic.f_tm_stamp,tic.f_dspno,tic.f_status from auction.t_item_category as tic where tic.f_status = 1 order by $order_sort limit $limit offset $offset";
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        for($i=0; $i<$data_cnt; $i++) {
            $fk_item_category[$i]		= mysql_result($result, $i, "fk_item_category");
            $f_category_name[$i]		= mysql_result($result, $i, "f_category_name");
            $f_mail_category_val[$i]	= mysql_result($result, $i, "f_mail_category_val");
            $f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
            $f_dspno[$i]				= mysql_result($result, $i, "f_dspno");
            $f_status[$i]				= mysql_result($result, $i, "f_status");
        }

        //■ＤＢ切断
        db_close($db);
        return true;
	}

    function GetSortUpdate(&$fk_item_category,&$f_dspno,$sort,$dspno_desc){

        if($dspno_desc==0){
            return false;
        }

        if($sort==0){
            $order_sort="fk_item_category";
        }

        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        mysql_query("BEGIN", $db);
        //下向こう矢印押す場合

        $sql_s1="select * from auction.t_item_category where f_dspno in(select min( f_dspno) from auction.t_item_category where f_dspno > $f_dspno and fk_item_category<>0)";
        $sql_s2="select * from auction.t_item_category where f_dspno in(select max( f_dspno) from auction.t_item_category where f_dspno < $f_dspno and fk_item_category<>0)";

        if($dspno_desc==1) {
            $select_sql=$sql_s1;
        }else {
            $select_sql=$sql_s2;
        }
        //select sql実行
        $result = mysql_query($select_sql, $db);
	$rows = mysql_num_rows($result);
        if( $result == false or $rows==0) {
 //           print "$sql_s1<BR>\n";
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            $fk_item_category="";
            $f_dspno="";
            return false;
        }
        $fk_item_category_su=mysql_result($result,0,fk_item_category);
        $f_dspno_su=mysql_result($result,0,f_dspno);
        $sql_u1="update auction.t_item_category set f_dspno=$f_dspno where fk_item_category=$fk_item_category_su ";
        $sql_u2="update auction.t_item_category set f_dspno=$f_dspno_su where fk_item_category=$fk_item_category ";

        //update sql実行
        $result = mysql_query($sql_u1, $db);
        if( $result == false ) {
            print "$sql_u1<BR>\n";
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            $fk_item_category="";
            $f_dspno="";
            return false;
        }
        $result = mysql_query($sql_u2, $db);
        if( $result == false ) {
            print "$sql_u2<BR>\n";
            mysql_query("ROLLBACK", $db);
            //■ＤＢ切断
            db_close($db);
            $fk_item_category="";
            $f_dspno="";
            return false;
        }

        //■コミット
        mysql_query("COMMIT", $db);
        $fk_item_category="";
        $f_dspno="";
        return true;
    }


    function GetTCategoryInfo(&$fk_item_category,&$f_category_name,&$f_mail_category_val,&$f_tm_stamp,&$f_rbstp,&$f_rbedp,&$f_status,&$f_dspno) {

//        //■ＤＢ接続
//        $db=db_connect();
//        if($db == false) {
//            exit;
//        }

//        $sql  = "select fk_item_category,f_category_name,f_mail_category_val,f_tm_stamp,f_rbstp,f_rbedp,f_status,f_dspno ";
//        $sql .= "from auction.t_item_category where fk_item_category='$fk_item_category' ";
//
//        $msg=" $sql = ".$sql;
//        $msg=" ----------------- ";
//        //■ＳＱＬ実行
//        $result = mysql_query($sql, $db);
//        if( $result == false ) {
//            print "$sql<BR>\n";
//            //■ＤＢ切断
//            db_close($db);
//            return false;
//        }
//        //■データ件数
//        $rows = mysql_num_rows($result);
//
//        if($rows > 0) {
//
//
//            $fk_item_category	= mysql_result($result, 0, "fk_item_category");
//            $f_category_name	= mysql_result($result, 0, "f_category_name");
//            $f_mail_category_val	= mysql_result($result, 0, "f_mail_category_val");
//            $f_tm_stamp	= mysql_result($result, 0, "f_tm_stamp");
//            $f_rbstp	= mysql_result($result, 0, "f_rbstp");
//            $f_rbedp	= mysql_result($result, 0, "f_rbedp");
//            $f_status	= mysql_result($result, 0, "f_status");
//            $f_dspno	= mysql_result($result, 0, "f_dspno");
//        }
//        else {
//
//            $fk_item_category	= "";
            $f_category_name	= "";
//            $f_mail_category_val	= "";
//            $f_tm_stamp	= "";
//            $f_rbstp	= "";
//            $f_rbedp	= "";
//            $f_status	= "";
//            $f_dspno	= "";
//
//        }
//
//        //■ＤＢ切断
//        db_close($db);
        return true;
//        return $msg;
    }


/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品カテゴリ更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function UpdateCategory(&$fk_item_category_id,&$f_category_name,&$f_status) {

        //■ＤＢ接続
        $db=db_connect();
       if($db == false) {
           exit;
        }

        //■トランザクション開始        mysql_query("BEGIN", $db);


      $sql  = "update auction.t_item_category set f_category_name='$f_category_name',f_status=$f_status ";
       $sql .= "where fk_item_category=$fk_item_category_id";

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ロールバック
            mysql_query("ROLLBACK", $db);
           //■ＤＢ切断
            db_close($db);
            return false;
        }


        //■コミット
        mysql_query("COMMIT", $db);

        //■ＤＢ切断
        db_close($db);
        return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品自動入札履歴CSV作成関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetProductsAutoBitAllListCSV($dist_path,&$fk_auto_id,&$fk_member_id,&$fk_products_id,&$f_handle,&$f_login_id,&$f_min_price,&$f_max_price,&$f_free_coin,&$f_buy_coin,&$f_set_free_coin,&$f_set_buy_coin,&$f_status,&$csv_file_name,&$all_count)
	{
            //echo $sort;
            //■ＤＢ接続
            $db=db_connect();
            if($db == false)
            {
                    exit;
            }

//            $sql = "SELECT ab.fk_auto_id,ab.fk_products_id,ab.fk_member_id,mm.f_handle,mm.f_login_id,ab.f_min_price,ab.f_max_price,ab.f_free_coin,ab.f_buy_coin,ab.f_set_free_coin,ab.f_set_buy_coin,ab.f_status
//                                            FROM auction.t_auto_bidder ab,auction.t_member_master mm
//                                            WHERE ab.fk_member_id=mm.fk_member_id
//                                            AND ab.f_status=0 ";
                $sql = "
SELECT
  COUNT(ab2.fk_auto_id)
FROM
(
  SELECT
    ab.fk_auto_id,ab.fk_member_id
  FROM
    auction.t_auto_bidder ab
  WHERE
    ab.f_status=0
) AS ab2
INNER JOIN
  auction.t_member_master mm2 ON ab2.fk_member_id=mm2.fk_member_id
";
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ＤＢ切断
                    db_close($db);
                    return false;
            }
            //$all_count = mysql_num_rows($result);
            $all_count = mysql_result($result, 0, 0);

//            $sql = "SELECT ab.fk_auto_id,ab.fk_products_id,ab.fk_member_id,mm.f_handle,mm.f_login_id,ab.f_min_price,ab.f_max_price,ab.f_free_coin,ab.f_buy_coin,ab.f_set_free_coin,ab.f_set_buy_coin,ab.f_status
//                                            FROM auction.t_auto_bidder ab ,auction.t_member_master mm
//                                            WHERE ab.fk_member_id=mm.fk_member_id
//                                            AND ab.f_status=0
//                                            ORDER BY f_min_price ";
		$sql = "
SELECT
  ab2.*,mm2.f_handle,mm2.f_login_id
FROM
(
  SELECT
      ab.fk_auto_id,ab.fk_products_id,ab.fk_member_id,ab.f_min_price,ab.f_max_price,ab.f_free_coin,ab.f_buy_coin,ab.f_set_free_coin,ab.f_set_buy_coin,ab.f_status
  FROM
    auction.t_auto_bidder ab
  WHERE
    ab.f_status=0
) AS ab2
INNER JOIN
  auction.t_member_master mm2 ON ab2.fk_member_id=mm2.fk_member_id
ORDER BY
  f_min_price
";
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                    print "$sql<BR>\n";
                    //■ＤＢ切断
                    db_close($db);
                    return false;
            }
            //■データ件数
            $data_cnt_auto = mysql_num_rows($result);

            //■ＤＢ切断
            db_close($db);

            //■出力フォルダ内の全ファイル削除
            $FileDir = $dist_path;
            $FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
            for($i=0; $FileList[$i]!=""; $i++)
            {
                    unlink($FileList[$i]);
            }
//G.Chin #77 2011-01-12 chg sta
/*
            if($all_count!==0 && $all_count <= MNTSTAFF_MEMCNT)
            {
                    $file_name = "mntstaff.csv";
*/
            if($all_count!==0 && $all_count <= CSV_PRDBID_CNT)
            {
                    $file_name  = CSVBID_FILENAME;
					$file_name .= ".csv";
//G.Chin #77 2011-01-12 chg end

                    //■CSVファイル・オープン
                    $file_path = sprintf("%s%s", $dist_path, $file_name);
                    $myfile = fopen($file_path, "w");
                    $top_str = " ID,商品名,設定者,最小価格,最大価格,フリーコイン(使用コイン),課金コイン(使用コイン),フリーコイン(残コイン),課金コイン(残コイン),状態　";
                    //文字コード変換
                    $top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
                    fputs($myfile, "$top_str\n");

                    for($i=0; $i<$data_cnt_auto; $i++)
                    {


                        $fk_auto_id[$i]			= mysql_result($result, $i, "fk_auto_id");
                        $fk_products_id[$i]		= mysql_result($result, $i, "fk_products_id");
                        $f_handle[$i]			= mysql_result($result, $i, "f_handle");
                        $f_login_id[$i]			= mysql_result($result, $i, "f_login_id");
                        //▼設定者
			$name_str  = "";
			$name_str .= $f_login_id[$i];
			$name_str .= "(";
			$name_str .= $f_handle[$i];
			$name_str .= ")";
                        $f_min_price[$i]		= mysql_result($result, $i, "f_min_price");
                        $f_max_price[$i]		= mysql_result($result, $i, "f_max_price");
                        $f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
                        $f_buy_coin[$i]			= mysql_result($result, $i, "f_buy_coin");
                        $f_set_free_coin[$i]	= mysql_result($result, $i, "f_set_free_coin");
                        $f_set_buy_coin[$i]		= mysql_result($result, $i, "f_set_buy_coin");
                        $f_status[$i]			= mysql_result($result, $i, "f_status");

                        //商品名取得関数
                        GetTProductsName($fk_products_id[$i],$fk_products_name);
                        //文字コード変換
                        $fk_auto_id[$i]		= mb_convert_encoding($fk_auto_id[$i], "SJIS", "UTF-8");
                        $fk_products_name		= mb_convert_encoding($fk_products_name, "SJIS", "UTF-8");
                        $name_str			= mb_convert_encoding($name_str, "SJIS", "UTF-8");
                        $f_min_price[$i]		= mb_convert_encoding($f_min_price[$i], "SJIS", "UTF-8");
                        $f_max_price[$i]		= mb_convert_encoding($f_max_price[$i], "SJIS", "UTF-8");
                        $f_free_coin[$i]	= mb_convert_encoding($f_free_coin[$i], "SJIS", "UTF-8");
                        $f_buy_coin[$i]	= mb_convert_encoding($f_buy_coin[$i], "SJIS", "UTF-8");
                        $f_set_free_coin[$i]	= mb_convert_encoding($f_set_free_coin[$i], "SJIS", "UTF-8");
                        $f_set_buy_coin[$i]		= mb_convert_encoding($f_set_buy_coin[$i], "SJIS", "UTF-8");
                        $f_status[$i]	= mb_convert_encoding($f_status[$i], "SJIS", "UTF-8");

                        //■ファイル書込
                        fputs($myfile, "$fk_auto_id[$i],");
                        fputs($myfile, "$fk_products_name,");
                        fputs($myfile, "$name_str,");
                        fputs($myfile, "$f_min_price[$i],");
                        fputs($myfile, "$f_max_price[$i],");
                        fputs($myfile, "$f_free_coin[$i],");
                        fputs($myfile, "$f_buy_coin[$i],");
                        fputs($myfile, "$f_set_free_coin[$i],");
                        fputs($myfile, "$f_set_buy_coin[$i],");
                        fputs($myfile, "$f_status[$i]");

                        //■ファイル内改行
                        fputs($myfile, "\n");
                    }

                    //■CSVファイル・クローズ
                    fclose($myfile);

            }
            else if($all_count!==0)
            {
//G.Chin #77 2011-01-12 chg sta
//                    $file_cnt = ceil($all_count / MNTSTAFF_MEMCNT);
                    $file_cnt = ceil($all_count / CSV_PRDBID_CNT);
//G.Chin #77 2011-01-12 chg end
                    $output_cnt = 0;
                    $start = 0;

                    for($num=0; $num<$file_cnt; $num++)
                    {
                            $file_num = $num + 1;
//G.Chin #77 2011-01-12 chg sta
//                            $file_name = "mntstaff".$file_num.".csv";
                            $file_name  = CSVBID_FILENAME;
							$file_name .= $file_num;
							$file_name .= ".csv";
//G.Chin #77 2011-01-12 chg end

                            if($num == $file_cnt - 1)
                            {
                                    //最終ファイル
                                    $output_cnt = $all_count;
                            }
                            else
                            {
//G.Chin #77 2011-01-12 chg sta
//                                    $output_cnt = $output_cnt + MNTSTAFF_MEMCNT;
                                    $output_cnt = $output_cnt + CSV_PRDBID_CNT;
//G.Chin #77 2011-01-12 chg end
                            }

                            //■CSVファイル・オープン
                            $file_path = sprintf("%s%s", $dist_path, $file_name);
                            $myfile = fopen($file_path, "w");

                            $top_str = " ID,商品名,設定者,最小価格,最大価格,ﾌﾘｰｺｲﾝ(使用ｺｲﾝ),課金ｺｲﾝ(使用ｺｲﾝ),ﾌﾘｰｺｲﾝ(残ｺｲﾝ),課金ｺｲﾝ(残ｺｲﾝ),状態";
                            //文字コード変換
                            $top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
                            fputs($myfile, "$top_str\n");

                            for($i=$start; $i<$output_cnt; $i++)
                            {
                                $fk_auto_id[$i]			= mysql_result($result, $i, "fk_auto_id");
                                $fk_products_id[$i]		= mysql_result($result, $i, "fk_products_id");
                                $f_handle[$i]			= mysql_result($result, $i, "f_handle");
                                $f_min_price[$i]		= mysql_result($result, $i, "f_min_price");
                                $f_max_price[$i]		= mysql_result($result, $i, "f_max_price");
                                $f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
                                $f_buy_coin[$i]			= mysql_result($result, $i, "f_buy_coin");
                                $f_set_free_coin[$i]	= mysql_result($result, $i, "f_set_free_coin");
                                $f_set_buy_coin[$i]		= mysql_result($result, $i, "f_set_buy_coin");
                                $f_status[$i]			= mysql_result($result, $i, "f_status");

                                //商品名取得関数
                                GetTProductsName($fk_products_id[$i],$fk_products_name);

                                //文字コード変換
                                $fk_auto_id[$i]		= mb_convert_encoding($fk_auto_id[$i], "SJIS", "UTF-8");
                                $fk_products_name		= mb_convert_encoding($fk_products_name, "SJIS", "UTF-8");
                                $f_handle[$i]			= mb_convert_encoding($f_handle[$i], "SJIS", "UTF-8");
                                $f_min_price[$i]		= mb_convert_encoding($f_min_price[$i], "SJIS", "UTF-8");
                                $f_max_price[$i]		= mb_convert_encoding($f_max_price[$i], "SJIS", "UTF-8");
                                $f_free_coin[$i]	= mb_convert_encoding($f_free_coin[$i], "SJIS", "UTF-8");
                                $f_buy_coin[$i]	= mb_convert_encoding($f_buy_coin[$i], "SJIS", "UTF-8");
                                $f_set_free_coin[$i]	= mb_convert_encoding($f_set_free_coin[$i], "SJIS", "UTF-8");
                                $f_set_buy_coin[$i]		= mb_convert_encoding($f_set_buy_coin[$i], "SJIS", "UTF-8");
                                $f_status[$i]	= mb_convert_encoding($f_status[$i], "SJIS", "UTF-8");

                                //■ファイル書込
                                fputs($myfile, "$fk_auto_id[$i],");
                                fputs($myfile, "$fk_products_name,");
                                fputs($myfile, "$f_handle[$i],");
                                fputs($myfile, "$f_min_price[$i],");
                                fputs($myfile, "$f_max_price[$i],");
                                fputs($myfile, "$f_free_coin[$i],");
                                fputs($myfile, "$f_buy_coin[$i],");
                                fputs($myfile, "$f_set_free_coin[$i],");
                                fputs($myfile, "$f_set_buy_coin[$i],");
                                fputs($myfile, "$f_status[$i]");

                                //■ファイル内改行
                                fputs($myfile, "\n");
                    }

                            //■CSVファイル・クローズ
                            fclose($myfile);

                            $start = $i;
                    }
            }

            return true;
    }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品一覧CSV作成関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTProductsListCSV($dist_path,$inp_status,$inp_category,$inp_auction,$inp_regdate,$inp_recmmend,&$fk_products_id,&$fk_item_category_id,&$f_products_name,&$f_start_time,&$f_status,&$f_now_price,&$f_auction_name,&$all_count,&$data_cnt,$pid,$sort,&$csv_file_name)
	{
            //echo $sort;
            //■ＤＢ接続
            $db=db_connect();
            if($db == false)
            {
                    exit;
            }

            //状態
            $where_status = '';
            if ( strlen($inp_status) ) {
                switch ($inp_status) {
                case 0:
                    // 待機中
                    $where_status = "tp.f_status=0 ";
                    break;
                default:
                    // 開催中
                    $where_status = "tp.f_status IN (1,3,4,5,7) ";
                    break;
                }
            } else {
                // すべて
                $where_status = "tp.f_status IN (0,1,3,4,5,7) ";
            }

            //カテゴリ
            if($inp_category == "") {
                $where_category = "";
            }
            else {
                $where_category = "and tpm.fk_item_category_id='$inp_category' ";
            }

            //オークションタイプ
            if($inp_auction == "") {
                $where_auction = "";
            }
            else {
                $where_auction = "and tpm.fk_auction_type_id='$inp_auction' ";
            }

            //登録日
            if($inp_regdate == "") {
                $where_regdate = "";
            }
            else {
                $where_regdate = "and tpm.f_regdate>='$inp_regdate 00:00:00' and tpm.f_regdate<='$inp_regdate 23:59:59' ";
            }

            //注目商品
            if($inp_recmmend == "1") {
                $where_recmmend = "and tpm.f_recmmend='$inp_recmmend' ";
            }
            else {
                $where_recmmend = "";
            }

            //商品ID
            if($pid == "") {
                $where_pid = "";
            }
            else {
                $where_pid = "and tpm.fk_products_id = $pid ";
            }

            //ソート順
            switch($sort) {
                case 0:
                    $order_sort ="tpm2.fk_products_id ";
                    break;
                case 1:
                    $order_sort ="tpm2.fk_products_id desc ";
                    break;
                case 2:
                    $order_sort ="tpm2.f_start_time ";
                    break;
                case 3:
                    $order_sort ="tpm2.f_start_time desc ";
                    break;
                case 4:
                    $order_sort ="tp2.f_auction_time desc ";
                    break;
                case 5:
                    $order_sort ="tp2.f_auction_time ";
                    break;
            }

//            $sql_cnt  = "select count(*) from auction.t_products_master as tpm, auction.t_products as tp ";
//            $sql_cnt .= "where tpm.f_delflg=0 ";
//            $sql_cnt .= $where_status;
//            $sql_cnt .= $where_category;
//            $sql_cnt .= $where_auction;
//            $sql_cnt .= $where_regdate;
//            $sql_cnt .= $where_recmmend;
//            $sql_cnt .= $where_pid;
//            $sql_cnt .= "and tpm.fk_products_id=tp.fk_products_id ";
        $sql_cnt = "
SELECT
  count(tpm2.fk_products_id)
FROM
(
  SELECT
    fk_products_id
  FROM
    auction.t_products_master AS tpm
  WHERE
    tpm.f_delflg=0
    ".$where_category."
    ".$where_auction."
    ".$where_regdate."
    ".$where_recmmend."
    ".$where_pid."
) AS tpm2
INNER JOIN
(
  SELECT
    fk_products_id
  FROM
    auction.t_products AS tp
  WHERE
    ".$where_status."
) AS tp2 ON tpm2.fk_products_id=tp2.fk_products_id
";

            //print "$sql_cnt<BR>\n";
            //■ＳＱＬ実行
            $result = mysql_query($sql_cnt, $db);
            if( $result == false ) {
                print "$sql_cnt<BR>\n";
                //■ＤＢ切断
                db_close($db);
                return false;
            }
            //■全件数
            $all_count = mysql_result($result, 0, 0);
//            $sql  = "select tpm.fk_products_id,tpm.fk_item_category_id,tpm.f_products_name,tpm.f_start_time,";
//            $sql .= "tp.f_status,tpm.f_photo1,tp.f_now_price,att.f_auction_name ";
//            $sql .= "from auction.t_products_master as tpm, auction.t_products as tp ,auction.t_auction_type att ";
//            $sql .= "where tpm.f_delflg=0 ";
//            $sql .= $where_status;
//            $sql .= $where_category;
//            $sql .= $where_auction;
//            $sql .= $where_regdate;
//            $sql .= $where_recmmend;
//            $sql .= $where_pid;
//            $sql .= "and tpm.fk_products_id=tp.fk_products_id and tpm.fk_auction_type_id=att.fk_auction_type_id  ";
//            $sql .= "order by $order_sort ";
        $sql = "
SELECT
  tpm2.*,tp2.*,att2.f_auction_name
FROM
(
  SELECT
    tpm.fk_products_id,tpm.fk_item_category_id,tpm.f_products_name,tpm.f_start_time,tpm.f_photo1,tpm.fk_auction_type_id
  FROM
    auction.t_products_master AS tpm
  WHERE
    tpm.f_delflg=0
    ".$where_category."
    ".$where_auction."
    ".$where_regdate."
    ".$where_recmmend."
    ".$where_pid."
) AS tpm2
INNER JOIN
(
  SELECT
    tp.fk_products_id,tp.f_status,tp.f_now_price,tp.f_auction_time
  FROM
    auction.t_products AS tp
  WHERE
    ".$where_status."
) AS tp2 ON tpm2.fk_products_id=tp2.fk_products_id
INNER JOIN
  auction.t_auction_type att2 ON tpm2.fk_auction_type_id=att2.fk_auction_type_id
ORDER BY
  ".$order_sort."
";
//            print $sql."<BR>\n";
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false ) {
                print "$sql<BR>\n";
                //■ＤＢ切断
                db_close($db);
                return false;
            }
            //■データ件数
            $data_cnt = mysql_num_rows($result);

            //■ＤＢ切断
            db_close($db);

            //■出力フォルダ内の全ファイル削除
            $FileDir = $dist_path;
            $FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);

            for($i=0; $FileList[$i]!=""; $i++)
            {
                    unlink($FileList[$i]);
            }
//G.Chin #77 2011-01-12 chg sta
/*
            if($all_count!==0 && $all_count <= MNTSTAFF_MEMCNT)
            {
                    $file_name = "mntstaff.csv";
*/
            if($all_count!==0 && $all_count <= CSV_PRODUCT_CNT)
            {
                    $file_name  = CSVPRD_FILENAME;
					$file_name .= ".csv";
//G.Chin #77 2011-01-12 chg end

                    //■CSVファイル・オープン
                    $file_path = sprintf("%s%s", $dist_path, $file_name);
                    $myfile = fopen($file_path, "w");
//G.Chin #77 2011-01-12 chg sta
//                    $top_str = "商品ID,ｶﾃｺﾞﾘｵｰｸｼｮﾝﾀｲﾌﾟ,開催開始,商品名,状態,現在価格";
                    $top_str = "商品ID,ｶﾃｺﾞﾘｵｰｸｼｮﾝﾀｲﾌﾟ,商品名,開催開始,状態,現在価格";
//G.Chin #77 2011-01-12 chg end
                    //文字コード変換
                    $top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
                    fputs($myfile, "$top_str\n");

                    for($i=0; $i<$data_cnt; $i++) {
                        $fk_products_id[$i]			= mysql_result($result, $i, "fk_products_id");
                        $fk_item_category_id[$i]	= mysql_result($result, $i, "fk_item_category_id");
                        $f_products_name[$i]		= mysql_result($result, $i, "f_products_name");
                        $f_start_time[$i]			= mysql_result($result, $i, "f_start_time");
                        $f_status[$i]				= mysql_result($result, $i, "f_status");
                        $f_now_price[$i]			= mysql_result($result, $i, "f_now_price");
                        $f_auction_name[$i]			= mysql_result($result, $i, "f_auction_name");

                        //文字コード変換
                        $fk_products_id[$i]		= mb_convert_encoding($fk_products_id[$i], "SJIS", "UTF-8");
                        $fk_item_category_id[$i]		= mb_convert_encoding($fk_item_category_id[$i], "SJIS", "UTF-8");
                        $f_products_name[$i]			= mb_convert_encoding($f_products_name[$i], "SJIS", "UTF-8");
                        $f_start_time[$i]		= mb_convert_encoding($f_start_time[$i], "SJIS", "UTF-8");
                        $f_status[$i]		= mb_convert_encoding($f_status[$i], "SJIS", "UTF-8");
                        $f_now_price[$i]	= mb_convert_encoding($f_now_price[$i], "SJIS", "UTF-8");
                        $f_auction_name[$i]	= mb_convert_encoding($f_auction_name[$i], "SJIS", "UTF-8");

                        //■ファイル書込
                        fputs($myfile, "$fk_products_id[$i],");
                        fputs($myfile, "$fk_item_category_id[$i],");
                        fputs($myfile, "$f_products_name[$i],");
                        fputs($myfile, "$f_start_time[$i],");
                        fputs($myfile, "$f_status[$i],");
                        fputs($myfile, "$f_now_price[$i],");
                        fputs($myfile, "$f_auction_name[$i]");

                        //■ファイル内改行
                        fputs($myfile, "\n");
                    }

                    //■CSVファイル・クローズ
                    fclose($myfile);

            }
            else if($all_count!==0)
            {
//G.Chin #77 2011-01-12 chg sta
//                    $file_cnt = ceil($all_count / MNTSTAFF_MEMCNT);
                    $file_cnt = ceil($all_count / CSV_PRDBID_CNT);
//G.Chin #77 2011-01-12 chg end
                    $output_cnt = 0;
                    $start = 0;

                    for($num=0; $num<$file_cnt; $num++)
                    {
                            $file_num = $num + 1;
//G.Chin #77 2011-01-12 chg sta
//                            $file_name = "mntstaff".$file_num.".csv";
                            $file_name  = CSVPRD_FILENAME;
							$file_name .= $file_num;
							$file_name .= ".csv";
//G.Chin #77 2011-01-12 chg end
                            if($num == $file_cnt - 1)
                            {
                                    //最終ファイル
                                    $output_cnt = $all_count;
                            }
                            else
                            {
//G.Chin #77 2011-01-12 chg sta
//                                    $output_cnt = $output_cnt + MNTSTAFF_MEMCNT;
                                    $output_cnt = $output_cnt + CSV_PRODUCT_CNT;
//G.Chin #77 2011-01-12 chg end
                            }

                            //■CSVファイル・オープン
                            $file_path = sprintf("%s%s", $dist_path, $file_name);
                            $myfile = fopen($file_path, "w");
//G.Chin #77 2011-01-12 chg sta
//                            $top_str = "商品ID,ｶﾃｺﾞﾘｵｰｸｼｮﾝﾀｲﾌﾟ,開催開始,商品名,状態,現在価格";;
                            $top_str = "商品ID,ｶﾃｺﾞﾘｵｰｸｼｮﾝﾀｲﾌﾟ,商品名,開催開始,状態,現在価格";
//G.Chin #77 2011-01-12 chg end
                            //文字コード変換
                            $top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
                            fputs($myfile, "$top_str\n");
                            for($i=$start; $i<$output_cnt; $i++) {
                                $fk_products_id[$i]			= mysql_result($result, $i, "fk_products_id");
                                $fk_item_category_id[$i]	= mysql_result($result, $i, "fk_item_category_id");
                                $f_products_name[$i]		= mysql_result($result, $i, "f_products_name");
                                $f_start_time[$i]			= mysql_result($result, $i, "f_start_time");
                                $f_status[$i]				= mysql_result($result, $i, "f_status");
                                $f_now_price[$i]			= mysql_result($result, $i, "f_now_price");
                                $f_auction_name[$i]			= mysql_result($result, $i, "f_auction_name");

                                //文字コード変換
                                $fk_products_id[$i]		= mb_convert_encoding($fk_products_id[$i], "SJIS", "UTF-8");
                                $fk_item_category_id[$i]		= mb_convert_encoding($fk_item_category_id[$i], "SJIS", "UTF-8");
                                $f_products_name[$i]			= mb_convert_encoding($f_products_name[$i], "SJIS", "UTF-8");
                                $f_start_time[$i]		= mb_convert_encoding($f_start_time[$i], "SJIS", "UTF-8");
                                $f_status[$i]		= mb_convert_encoding($f_status[$i], "SJIS", "UTF-8");
                                $f_now_price[$i]	= mb_convert_encoding($f_now_price[$i], "SJIS", "UTF-8");
                                $f_auction_name[$i]	= mb_convert_encoding($f_auction_name[$i], "SJIS", "UTF-8");

                                //■ファイル書込
                                fputs($myfile, "$fk_products_id[$i],");
                                fputs($myfile, "$fk_item_category_id[$i],");
                                fputs($myfile, "$f_products_name[$i],");
                                fputs($myfile, "$f_start_time[$i],");
                                fputs($myfile, "$f_status[$i],");
                                fputs($myfile, "$f_now_price[$i],");
                                fputs($myfile, "$f_auction_name[$i]");

                                //■ファイル内改行
                                fputs($myfile, "\n");
                            }

                            //■CSVファイル・クローズ
                            fclose($myfile);

                            $start = $i;
                    }
            }

            return true;
    }

//G.Chin AWKT-518 2010-10-13 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		終了商品入金処理関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function PaymentEndProduct($f_staff_id,$fk_member_id,$fk_end_products_id,$fk_shiharai_type_id,$address_flag,$carriage,&$status_str,&$bgcolor,&$stock_str,&$f_photo_1_dsp,&$f_shiharai_name,&$errmsg)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//▼オークション終了商品情報を取得
		$sql  = "select f_products_name,fk_item_category_id,";
		$sql .= "f_photo1,f_photo2,f_photo3,f_photo4,";
		$sql .= "f_status,f_stock,";
		$sql .= "f_last_bidder,f_pickup_flg,f_end_time,fk_address_id, ";
		$sql .= "f_start_price,f_end_price,f_market_price ";
		$sql .= "from auction.t_end_products ";
		$sql .= "where fk_end_products_id='$fk_end_products_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>オークション終了商品の情報の取得に失敗しました。</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$errmsg = "<P>オークション終了商品の情報がありません。</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		else
		{
			$f_products_name		= mysql_result($result, 0, "f_products_name");
			$fk_item_category_id	= mysql_result($result, 0, "fk_item_category_id");
			$f_photo1				= mysql_result($result, 0, "f_photo1");
			$f_photo2				= mysql_result($result, 0, "f_photo2");
			$f_photo3				= mysql_result($result, 0, "f_photo3");
			$f_photo4				= mysql_result($result, 0, "f_photo4");
			$f_status				= mysql_result($result, 0, "f_status");
			$f_stock				= mysql_result($result, 0, "f_stock");
			$fk_address_id			= mysql_result($result, 0, "fk_address_id");
			$f_last_bidder			= mysql_result($result, 0, "f_last_bidder");
			$f_pickup_flg			= mysql_result($result, 0, "f_pickup_flg");
			$f_end_time				= mysql_result($result, 0, "f_end_time");

			$f_start_price			= mysql_result($result, 0, "f_start_price");
			$f_end_price			= mysql_result($result, 0, "f_end_price");
			$f_market_price			= mysql_result($result, 0, "f_market_price");
		}

		//▼商品アドレスフラグを取得
		$fk_products_id = $fk_end_products_id;
		$sql  = "select f_address_flag from auction.t_products_master ";
		$sql .= "where fk_products_id='$fk_products_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>オークション終了商品の商品アドレスフラグ取得に失敗しました。</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$f_address_flag = 0;
		}
		else
		{
			$f_address_flag	= mysql_result($result, 0, "f_address_flag");
		}

		//▼状態設定
		if((($f_address_flag == 0) && ($fk_address_id != "")) && (COIN_CATEGORY != $fk_item_category_id))
		{
			//配送先が決定している場合、"配送済み"に設定
			$f_status = 3;
			$tep_f_status = 3;
		}
		else
		{
			//"支払済み"に設定
			$f_status = 2;
			$tep_f_status = 2;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//▼オークション終了商品状態を更新
		$sql  = "update auction.t_end_products ";
		$sql .= "set f_status='$f_status',";
		$sql .= "f_stock='$f_stock',";
		$sql .= "f_pickup_flg='$f_pickup_flg' ";
		$sql .= "where fk_end_products_id='$fk_end_products_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			echo "return false";
			$errmsg = "<P>オークション終了商品状態更新処理に失敗しました。</P>";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//▼終了状態
		switch($f_status)
		{
			case 0:
					$status_str = "落札のみ";
					$bgcolor = "#FFFFFF";
					break;
			case 1:
					$status_str = "購入拒否ｷｬﾝｾﾙ";
					$bgcolor = "#FFFFFF";
					break;
			case 2:
					$status_str = "支払済み";
					$bgcolor = "#FFFFFF";
					break;
			case 3:
					$status_str = "配送先決定";
					$bgcolor = "#FFC8C8";
					break;
			case 4:
					$status_str = "配送済み(完了)";
					$bgcolor = "#C0C0C0";
					break;
			default:
					$status_str = "";
					$bgcolor = "#FFFFFF";
					break;
		}

		//▼在庫・発注
		switch($f_stock)
		{
			case 0:		$stock_str = "未指定";		break;
			case 1:		$stock_str = "在庫有り";	break;
			case 2:		$stock_str = "発注済み";	break;
			default:	$stock_str = "";			break;
		}

		//▼写真
		if($f_photo1 != "")
		{
			$f_photo_1_path = SITE_URL."images/".$f_photo1;
			$f_photo_1_dsp = "<img src='$f_photo_1_path'>";
		}
		else
		{
			$f_photo_1_dsp = "";
		}

		//▼商品の金額情報を取得
		//t_products_master
        $sql  = "select f_start_price,f_min_price,f_r_min_price,f_r_max_price,f_market_price ";
        $sql .= "from auction.t_products_master ";
		$sql .= "where fk_products_id='$fk_products_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>オークション終了商品の金額情報取得に失敗しました。(t_products_master)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$errmsg = "<P>オークション終了商品の金額情報取得がありません。(t_products_master)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		else
		{
			$f_start_price	= mysql_result($result, 0, "f_start_price");
			$f_min_price	= mysql_result($result, 0, "f_min_price");
			$f_r_min_price	= mysql_result($result, 0, "f_r_min_price");
			$f_r_max_price	= mysql_result($result, 0, "f_r_max_price");
			$f_market_price	= mysql_result($result, 0, "f_market_price");
		}

		//t_products
		$sql = "select f_now_price from auction.t_products where fk_products_id='$fk_products_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>オークション終了商品の金額情報取得に失敗しました。(t_products)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
        //■データ件数
        $rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$errmsg = "<P>オークション終了商品の金額情報取得がありません。(t_products_master)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		else
		{
			$f_now_price	= mysql_result($result, 0, "f_now_price");
		}

		//▼送料
		if($carriage == "")
		{
			$carriage = 0;
		}

		//▼アドレス要不要フラグ判定
		if($address_flag == 0)
		{
			$f_pay_money = $f_end_price;
		}
		else
		{
			$f_pay_money = $f_end_price + $carriage;
		}

		$pay_log_id = -1;

		//▼重複払い確認
		$sql  = "select fk_pay_log_id ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where f_cert_status=0 ";
		$sql .= "and fk_products_id='$fk_products_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>オークション終了商品の支払履歴取得に失敗しました。(t_products)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
        //■データ件数
        $rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$fk_pay_log_id = "";
		}
		else
		{
			$fk_pay_log_id = mysql_result($result, 0, "fk_pay_log_id");
		}

		if($fk_pay_log_id != -1 && $fk_pay_log_id != '')
		{
			$errmsg  = "";
			$errmsg .= "<br><br>\n";
			$errmsg .= "<font><b>この商品への支払は完了しています。</b></font>\n";
			$errmsg .= "<br><br><br>\n";
			$errmsg .= "<font size='-1'>　　下の閉じるﾎﾞﾀﾝを押して下さい。</font>\n";
			$errmsg .= "<br><br>\n";
			$errmsg .= "<form>\n";
			$errmsg .= "　　<input class='submit' style='background-color:#FFFBEC; color:#C84B00; border-color:#FFFAFA' type='button' value='閉じる' onclick='window.close()'>\n";
			$errmsg .= "</form>\n";
		}

		//▼支払方法の名称を取得
		//t_shiharai_type
		$sql = "select fk_shiharai_id from auction.t_shiharai_type where fk_shiharai_type_id='$fk_shiharai_type_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>支払方法の名称取得に失敗しました。(t_shiharai_type)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$errmsg = "<P>支払方法の名称がありません。(t_shiharai_type)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		else
		{
			$fk_shiharai_id	= mysql_result($result, 0, "fk_shiharai_id");
		}

		//t_shiharai_plan
		$sql = "select f_shiharai_name from auction.t_shiharai_plan where fk_shiharai_id='$fk_shiharai_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			$errmsg = "<P>支払方法の名称取得に失敗しました。(t_shiharai_plan)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows == 0)
		{
			$errmsg = "<P>支払方法の名称がありません。(t_shiharai_plan)</P>";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		else
		{
			$f_shiharai_name = mysql_result($result, 0, "f_shiharai_name");
		}

		//▼商品の支払履歴を記録
		$f_status = 1;
		$f_member_id = $fk_member_id;
		$f_coin_add = 0;
		$f_coin_result = 0;
		$f_cert_status = 0;
		$f_cert_err = "";
		$f_pay_pos = 2;
		$fk_adcode_id = 0;
		$sql  = "insert into auction.t_pay_log ";
		$sql .= "(fk_pay_log_id,f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,";
		$sql .= "f_min_price,f_cert_status,f_cert_err,f_pay_pos,fk_adcode_id,fk_products_id,f_tm_stamp) ";
		$sql .= "values ";
		$sql .= "(0,'$f_status','$f_member_id','$f_staff_id','$f_pay_money','$f_coin_add','$fk_shiharai_type_id','$f_coin_result',";
		$sql .= "'$f_min_price','$f_cert_status','$f_cert_err','$f_pay_pos','$fk_adcode_id','$fk_products_id',now()) ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			echo "return false";
			$errmsg = "<P>入金処理に失敗しました。</P>";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//▼売上集計を更新
		$f_type = 0;
		$f_prd_lastprice = $f_pay_money;
        if($address_flag == 0)
		{
			$coin_pack_price = $f_pay_money;
		}
		else
		{
			$coin_pack_price = 0;
		}

		//▼コインパック判定
		if($fk_item_category_id == COIN_CATEGORY)
		{
			//▼会員のコイン情報を取得
			//t_member
			$sql = "select f_coin,f_free_coin,f_total_pay from auction.t_member where fk_member_id='$fk_member_id' ";
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				$errmsg = "<P>会員のコイン情報取得に失敗しました。(t_member)</P>";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($result);
			if($rows == 0)
			{
				$errmsg = "<P>会員のコイン情報がありません。(t_member)</P>";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			else
			{
				$f_coin			= mysql_result($result, 0, "f_coin");
				$f_free_coin	= mysql_result($result, 0, "f_free_coin");
				$f_total_pay	= mysql_result($result, 0, "f_total_pay");
			}

			//t_member_master
			$sql = "select f_over_money from auction.t_member_master where fk_member_id='$fk_member_id' ";
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				$errmsg = "<P>会員のコイン情報取得に失敗しました。(t_member)</P>";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($result);
			if($rows == 0)
			{
				$errmsg = "<P>会員のコイン情報がありません。(t_member_master)</P>";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			else
			{
				$f_over_money	= mysql_result($result, 0, "f_over_money");
			}
			$total_coin = $f_coin + $f_free_coin;

			//▼ポイントオークション時の加算枚数を取得
			$sql  = "select f_plus_coins from auction.t_products_master ";
			$sql .= "where fk_products_id='$fk_products_id' ";
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				$errmsg = "<P>ポイントオークション時の加算枚数取得に失敗しました。</P>";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($result);
			if($rows == 0)
			{
				$errmsg = "<P>ポイントオークション時の加算枚数がありません。</P>";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			else
			{
				$f_plus_coins = mysql_result($result, 0, "f_plus_coins");
			}
			$coin_result = $f_plus_coins + $total_coin;

			//▼仕入価格を集計
			$cost = 0;
			$f_coin_add = $f_plus_coins;
			$f_coin_result = $coin_result;
			$end_price = $f_prd_lastprice;
			if(COIN_PACK_PLAN == 0) //全てフリー
			{
				//echo 1;
				$sql = "update auction.t_member set f_free_coin=f_free_coin+'$f_coin_add' where fk_member_id='$fk_member_id'";
				$sql3  = "insert into auction.t_pay_log ";
				$sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
				$sql3 .= "values (3,$fk_member_id,$f_staff_id,$cost,0,2,now(),0,$fk_end_products_id,$f_coin_add,$f_coin_result,1)";
			}
			else if(COIN_PACK_PLAN == 1) //全て課金
			{
				//echo 2;
				$sql = "update auction.t_member set f_coin=f_coin+'$f_coin_add' where fk_member_id='$fk_member_id'";
				$sql3  = "insert into auction.t_pay_log ";
				$sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_coin_add,f_coin_result,fk_shiharai_type_id) ";
				$sql3 .= "values (3,$fk_member_id,$f_staff_id,$cost,0,2,now(),0,$fk_end_products_id,$f_coin_add,$f_coin_result,1)";
			}
			else if(COIN_PACK_PLAN == 2) //計算にて算出
			{
				//echo 3;
				$tmp_add_coin = floor($end_price/POINT_VALUE);
				$tmp_free_add_coin=$f_coin_add - $tmp_add_coin;
				if($tmp_add_coin >=$f_coin_add )
				{
					$tmp_add_coin=$f_coin_add;
					$tmp_free_add_coin = 0;
				}
				$sql = "update auction.t_member set f_coin=f_coin+'$tmp_add_coin',f_free_coin=f_free_coin+'$tmp_free_add_coin' where fk_member_id='$fk_member_id'";
				$sql3  = "insert into auction.t_pay_log ";
				$sql3 .= "(f_status,f_member_id,f_staff_id,f_min_price,f_cert_status,f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id,f_coin_add,f_free_coin_add,f_coin_result,fk_shiharai_type_id) ";
				$sql3 .= "values (3,$fk_member_id,$f_staff_id,$cost,0,2,now(),0,$fk_end_products_id,$tmp_add_coin,$tmp_free_add_coin,$f_coin_result,1)";
			}

			//会員無料コイン枚数加算
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				$errmsg = "<P>会員無料コイン枚数加算に失敗しました。</P>";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
				db_close($db);
				return false;
			}

			//オークション終了商品の終了状態を更新
			$sql2 = "update auction.t_end_products set f_status=4 where fk_end_products_id='$fk_end_products_id'";
			$result = mysql_query($sql2, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				$errmsg = "<P>オークション終了商品の終了状態更新に失敗しました。</P>";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
				db_close($db);
				return false;
			}

			//支払履歴作成
			$result = mysql_query($sql3, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				$errmsg = "<P>支払履歴作成に失敗しました。</P>";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
				db_close($db);
				return false;
			}

			$sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d %H:00:00'),0,$cost)";
			$result = mysql_query($sql, $db);
			//if( $result == false )
                        if ( mysql_affected_rows() == 0 || mysql_affected_rows() == -1 )
                        {
				//print "$sql<BR>\n";
				$sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d %H:00:00') and f_type=0";
				$result = mysql_query($sql, $db);
				if( $result == false )
				{
					print "$sql<BR>\n";
					$errmsg = "<P>コインの加算に失敗しました。(時間別)</P>";
					//■ロールバック
					mysql_query("ROLLBACK", $db);
					//■ＤＢ切断
					db_close($db);
					return false;
				}
			}

			$sql = "INSERT IGNORE INTO auction.t_stat_sales (f_stat_dt,f_type,f_prd_cost) values(DATE_FORMAT(now(),'%Y-%m-%d 23:00:00'),1,$cost)";
			$result = mysql_query($sql, $db);
			//if( $result == false )
                        if ( mysql_affected_rows() == 0 || mysql_affected_rows() == -1 )
			{
				//print "$sql<BR>\n";
				$sql = "update auction.t_stat_sales set f_prd_cost=f_prd_cost+$cost where f_stat_dt=DATE_FORMAT(now(),'%Y-%m-%d 23:00:00') and f_type=1";
				$result = mysql_query($sql, $db);
				if( $result == false )
				{
					print "$sql<BR>\n";
					$errmsg = "<P>コインの加算に失敗しました。(日別)</P>";
					//■ロールバック
					mysql_query("ROLLBACK", $db);
					//■ＤＢ切断
					db_close($db);
					return false;
				}
			}
		}

		//▼会員情報を更新
		$sql  = "update auction.t_member set ";
		$sql .= "f_unpain_num=f_unpain_num-1 ";
		$sql .= "where fk_member_id=$fk_member_id and f_unpain_num>0";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			echo "return false";
			$errmsg = "<P>会員情報更新に失敗しました。</P>";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

                // ゲーム関係の処理（コインパックのみ）
		if ( $fk_item_category_id == COIN_CATEGORY) {
                    if (
                        defined( 'GAME_ENABLE' ) && constant( 'GAME_ENABLE' )
                        && defined( 'GAME_ID_A' )
                        && is_readable( realpath(COMMON_LIB.'game_lib.php') )
                    ) {
                        require_once( realpath(COMMON_LIB.'game_lib.php') );
                        $options = array();
                        if ( defined( 'GAME_ITEMS_A' ) ) {
                            $options['const.prize_items'] = GAME_ITEMS_A;
                        }
                        $game = new GameModel( $db, GAME_ID_A, $options );
                        if ( $game ) {
                            // プレイ権利付与と当時に抽選処理を行うため、ボーナス効果を先に付与してください
                            // ボーナス効果を即時利用するために、プレイ権利の付与に先駆けてボーナス効果の付与処理をコミットする必要があります。

                            // ボーナス効果
                            mysql_query("START TRANSACTION", $db);
                            $game->putGameBonusOfCoinPack( $db, $fk_member_id, $f_plus_coins );
                            mysql_query("COMMIT", $db);

                            // プレイ権利付与
                            mysql_query("START TRANSACTION", $db);
                            $game->putPlayTicketOfCoinPack( $db, $fk_member_id );
                            mysql_query("COMMIT", $db);
                        }
                    }
                }

		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin AWKT-518 2010-10-13 add end

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		オークション終了商品配送済みメールフラグと配達識別コード取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function GetTEndProductsMail($fk_end_products_id,&$f_send_mail,&$f_send_mail_id) {

        //■ＤＢ接続
        $db=db_connect();
        if($db == false) {
            exit;
        }

        $sql  = "select f_send_mail,f_send_mail_id ";
        $sql .= "from auction.t_end_products where fk_end_products_id='$fk_end_products_id' ";
        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $rows = mysql_num_rows($result);

        if($rows > 0) {
            $f_send_mail		= mysql_result($result, 0, "f_send_mail");
            $f_send_mail_id	= mysql_result($result, 0, "f_send_mail_id");
        }
        else {
            $f_send_mail		= 0;
            $f_send_mail_id	= "";
        }

        //■ＤＢ切断
        db_close($db);
        return true;
    }

?>
