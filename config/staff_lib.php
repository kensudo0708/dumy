<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/13												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 人事関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		人事情報取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTStaffInfo($fk_staff_id,&$f_login_id,&$f_login_pass,&$f_entry_time,&$f_status,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_login_id,f_login_pass,f_entry_time,f_status,f_tm_stamp ";
		$sql .= "from auction.t_staff where fk_staff_id='$fk_staff_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_login_id		= mysql_result($result, 0, "f_login_id");
			$f_login_pass	= mysql_result($result, 0, "f_login_pass");
			$f_entry_time	= mysql_result($result, 0, "f_entry_time");
			$f_status		= mysql_result($result, 0, "f_status");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_login_id		= "";
			$f_login_pass	= "";
			$f_entry_time	= "";
			$f_status		= "";
			$f_tm_stamp		= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		スタッフ管理者権限判定関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function CheckStaffTAuthority($fk_staff_id,$fk_authority_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select count(*) from auction.t_staff_authority ";
		$sql .= "where fk_staff_id='$fk_staff_id' and fk_authority_id='$fk_authority_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_result($result, 0, 0);
		
		if($rows == 0)
		{
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		else
		{
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
	}

?>