<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/14												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 販売関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払条件一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTAdConditionList(&$fk_adcon_id,&$f_type,&$f_adpricode,&$f_spend_value,&$f_pay_value,&$f_status,&$f_tm_stamp,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_adcon_id,f_type,f_adpricode,f_spend_value,f_pay_value,f_status,f_tm_stamp ";
		$sql .= "from auction.t_ad_condition ";
		$sql .= "order by f_adpricode ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_adcon_id[$i]	= mysql_result($result, $i, "fk_adcon_id");
			$f_type[$i]			= mysql_result($result, $i, "f_type");
			$f_adpricode[$i]	= mysql_result($result, $i, "f_adpricode");
			$f_spend_value[$i]	= mysql_result($result, $i, "f_spend_value");
			$f_pay_value[$i]	= mysql_result($result, $i, "f_pay_value");
			$f_status[$i]		= mysql_result($result, $i, "f_status");
			$f_tm_stamp[$i]		= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払条件更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTAdCondition($fk_adcon_id,$f_type,$f_adpricode,$f_spend_value,$f_pay_value,$f_status)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		if($f_spend_value == "")
		{
			$f_spend_value = 0;
		}
		
		if($f_pay_value == "")
		{
			$f_pay_value = 0;
		}
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_ad_condition
		$sql  = "update auction.t_ad_condition set ";
		$sql .= "f_type='$f_type',f_adpricode='$f_adpricode',f_spend_value='$f_spend_value',";
		$sql .= "f_pay_value='$f_pay_value',f_status='$f_status' ";
		$sql .= "where fk_adcon_id='$fk_adcon_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払条件追加関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function InsertTAdCondition($f_type,$f_adpricode,$f_spend_value,$f_pay_value,$f_status)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		if($f_spend_value == "")
		{
			$f_spend_value = 0;
		}
		
		if($f_pay_value == "")
		{
			$f_pay_value = 0;
		}
		
		//▼IDの最大値を取得
		$sql = "select max(fk_adcon_id) from auction.t_ad_condition ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$max_id = mysql_result($result, 0, 0);
		}
		else
		{
			$max_id = 0;
		}
		//ID作成
		$fk_adcon_id = $max_id + 1;
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_ad_condition
		$sql  = "insert into auction.t_ad_condition ";
		$sql .= "(fk_adcon_id,f_type,f_adpricode,f_spend_value,f_pay_value,f_status,f_tm_stamp) ";
		$sql .= "values ('$fk_adcon_id','$f_type','$f_adpricode','$f_spend_value','$f_pay_value','$f_status',now()) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払条件削除関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function DeleteTAdCondition($fk_adcon_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_ad_condition
		$sql = "delete from auction.t_ad_condition where fk_adcon_id='$fk_adcon_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		売上集計コイン購入更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTStatSalesBuyCoin($f_type,$f_buycoin,$buycoin_value,$coin_num)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		if($f_buycoin == "")
		{
			$f_buycoin = 0;
		}
		
		if($buycoin_value == "")
		{
			$buycoin_value = 0;
		}
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//▼現在時刻を取得
		$sql = "select current_date ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$c_date = mysql_result($result, 0, 0);
		
		$sql = "select current_time ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$c_time = mysql_result($result, 0, 0);
		$c_hour = substr($c_time, 0, 2);
		$f_stat_dt = $c_date." ".$c_hour.":00:00";
		
		//▼現在日時のレコードの存在チェック
		$sql = "select count(*) from auction.t_stat_sales where f_stat_dt='$f_stat_dt' and f_type='$f_type' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$rows = mysql_result($result, 0, 0);
		
		$buycoin_name = "f_buycoin".$coin_num;
		
		if($rows == 0)
		{
			//t_stat_sales
			$sql  = "insert into auction.t_stat_sales ";
			$sql .= "(f_stat_dt,f_type,f_buycoin,$buycoin_name) ";
			$sql .= "values ('$f_stat_dt','$f_type','$f_buycoin','$buycoin_value') ";
			
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
		}
		else
		{
			//t_stat_sales
			$sql  = "update auction.t_stat_sales set ";
			$sql .= "f_buycoin=f_buycoin+$f_buycoin,";
			$sql .= "$buycoin_name=$buycoin_name+$buycoin_value ";
			$sql .= "where f_stat_dt='$f_stat_dt' and f_type='$f_type' ";
			
			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		売上集計商品支払更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
        function UpdateTStatSalesPayProduct($f_type,$f_prd_lastprice,$coin_pack_price,$db) {
//            if($db==false) {
//                exit;
//            }else {
                //■ＤＢ接続
//	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
//
		//■トランザクション開始
//		mysql_query("BEGIN", $db);

                //▼現在時刻を取得
                $sql = "select current_date ";
                //■ＳＱＬ実行
                $result = mysql_query($sql, $db);
                if( $result == false ) {
//                    print "$sql<BR>\n";
//                    //■ＤＢ切断
//                    db_close($db);
                    return false;
                }
                $c_date = mysql_result($result, 0, 0);

                $sql = "select current_time ";
                //■ＳＱＬ実行
                $result = mysql_query($sql, $db);
                if( $result == false ) {
//                    print "$sql<BR>\n";
//                    //■ＤＢ切断
//                    db_close($db);
                    return false;
                }
                $c_time = mysql_result($result, 0, 0);
                $c_hour = substr($c_time, 0, 2);
                $f_stat_dt = $c_date." ".$c_hour.":00:00";

                //▼現在日時のレコードの存在チェック
                $sql = "select count(*) from auction.t_stat_sales where f_stat_dt='$f_stat_dt' and f_type='$f_type' ";
                //■ＳＱＬ実行
                $result = mysql_query($sql, $db);
                if( $result == false ) {
                    print "$sql<BR>\n";
//                    //■ＤＢ切断
//                    db_close($db);
                    return false;
                }
                $rows = mysql_result($result, 0, 0);

                if($rows == 0) {
                    //t_stat_sales
                    $sql  = "insert into auction.t_stat_sales ";
                    $sql .= "(f_stat_dt,f_type,f_prd_lastprice,f_package_sales) ";
                    $sql .= "values ('$f_stat_dt','$f_type','$f_prd_lastprice','$coin_pack_price') ";

                    //■ＳＱＬ実行
                    $result = mysql_query($sql, $db);
                    if( $result == false ) {
//                        print "$sql<BR>\n";
//                        //■ロールバック
//                        mysql_query("ROLLBACK", $db);
//                        //■ＤＢ切断
//                        db_close($db);
                        return false;
                    }
                }
                else {
                    //t_stat_sales
                    $sql  = "update auction.t_stat_sales set ";
                    $sql .= "f_prd_lastprice=f_prd_lastprice+$f_prd_lastprice ,f_package_sales=f_package_sales+$coin_pack_price ";
                    $sql .= "where f_stat_dt='$f_stat_dt' and f_type='$f_type' ";
//                    $sql .= "where f_stat_dt='$f_stat_dt' and f_type='' ";

                    //■ＳＱＬ実行
                    $result = mysql_query($sql, $db);
                    if( $result == false ) {
//                        print "$sql<BR>\n";
//                        //■ロールバック
//                        mysql_query("ROLLBACK", $db);
//                        //■ＤＢ切断
//                        db_close($db);
                        return false;
                    }
                }

                //■コミット
//                mysql_query("COMMIT", $db);

                //■ＤＢ切断
//                db_close($db);
                return true;
//            }
        }
?>