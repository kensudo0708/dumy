<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/09/01												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** ニュース関数ライブラリ **											*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ニュース一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTNewsList($inp_days,&$fk_news_id,&$f_category,&$f_body,&$f_status,&$f_regist_date,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		
		//表示日数
		if($inp_days == "")
		{
			$where_days = "";
		}
		else
		{
//			$w_days = $inp_days * 86400;
//			$src_day = date("Y-m-d 00:00:00", $w_days);
			$where_days = "and f_regist_date>=date_sub(now(),interval $inp_days day) ";
		}
		
		$sql  = "select fk_news_id,f_category,f_body,f_status,f_regist_date ";
		$sql .= "from auction.t_news ";
		$sql .= "where fk_news_id>0 ";
		$sql .= $where_days;
		$sql .= "order by f_regist_date desc";
		//echo $sql;
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_news_id[$i]		= mysql_result($result, $i, "fk_news_id");
			$f_category[$i]		= mysql_result($result, $i, "f_category");
			$f_body[$i]			= mysql_result($result, $i, "f_body");
			$f_status[$i]		= mysql_result($result, $i, "f_status");
			$f_regist_date[$i]	= mysql_result($result, $i, "f_regist_date");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ニュース状態更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTNewsStatus($fk_news_id, $f_status)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql = "update auction.t_news set f_status='$f_status' where fk_news_id='$fk_news_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ニュース登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTNews($f_category,$f_body)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_news
		$sql  = "insert into auction.t_news ";
		$sql .= "(fk_news_id,f_category,f_body,f_status,f_regist_date) ";
		$sql .= "values (0,'$f_category','$f_body',0,now()) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

?>