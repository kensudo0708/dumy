<?php
/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/09												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 会員関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員一覧取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberList($inp_activity,$inp_id_name,$inp_parent_ad,$inp_group,$inp_mail,$inp_s_regist,$inp_e_regist,$inp_regpos,$inp_s_coin,$inp_e_coin,$inp_s_free,$inp_e_free,$inp_s_totalpay,$inp_e_totalpay,$inp_sex,$inp_mem_id,$inp_name,$inp_unpain,$inp_cancel,$inp_s_last_entry,$inp_e_last_entry,$sort,$limit,$offset,&$fk_member_id,&$f_activity,&$f_login_id,&$f_login_pass,&$f_handle,&$fk_member_group_id,&$f_sex,&$f_regist_date,&$f_mail_address_pc,&$f_mail_address_mb,&$fk_parent_ad,&$fk_address_flg,&$f_delete_flag,&$f_coin,&$f_free_coin,&$f_last_entry,&$all_count,&$data_cnt,&$f_total_pay,&$f_tm_stamp,&$f_pay_money,&$pay_record,&$ip,&$f_unpain_num,&$f_cancel_num,$inp_area,$inp_job,$inp_ip)
	{
		//echo $sort;
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		$where_master="";
                $where_mem="";

		//状態
		if($inp_activity == "")
		{
			$where_activity = "";
		}
		else
		{
			$where_activity = "and tmm.f_activity='$inp_activity' ";
                    $where_master .=" and f_activity='$inp_activity' ";
		}

		//ログインID・ハンドルネーム
		if($inp_id_name == "")
		{
			$where_id_name = "";
		}
		else
		{
			$where_id_name = "and ((tmm.f_login_id like '%$inp_id_name%') or (tmm.f_handle like '%$inp_id_name%')) ";
                    $where_master .=" and ((f_login_id like '%$inp_id_name%') or (f_handle like '%$inp_id_name%'))  ";
		}

		//広告/紹介コード
		if($inp_parent_ad == "")
		{
			$where_parent_ad = "";
		}
		else
		{
			$where_parent_ad = "and tmm.fk_parent_ad='$inp_parent_ad' ";
                    $where_master .=" and fk_parent_ad='$inp_parent_ad'  ";
		}

		//会員グループ
		if($inp_group == "")
		{
			$where_group = "";
		}
		else
		{
			$where_group = "and tmm.fk_member_group_id='$inp_group' ";
                    $where_master .=" and fk_member_group_id='$inp_group' ";
		}

		//メールアドレス
		if($inp_mail == "")
		{
			$where_mail = "";
		}
		else
		{
			$where_mail = "and ((tmm.f_mail_address_pc like '%$inp_mail%') or (tmm.f_mail_address_mb like '%$inp_mail%')) ";
                        $where_master .=" and ((f_mail_address_pc like '%$inp_mail%') or (f_mail_address_mb like '%$inp_mail%')) ";
		}

		//登録日(開始)
		if($inp_s_regist == "")
		{
			$where_s_regist = "";
		}
		else
		{
			$where_s_regist = "and tmm.f_regist_date>='$inp_s_regist 00:00:00' ";
                        $where_master .=" and f_regist_date>='$inp_s_regist 00:00:00' ";
		}

		//登録日(終了)
		if($inp_e_regist == "")
		{
			$where_e_regist = "";
		}
		else
		{
			$where_e_regist = "and tmm.f_regist_date<='$inp_e_regist 23:59:59' ";
                        $where_master .=" and f_regist_date<='$inp_e_regist 23:59:59' ";
		}

		//登録媒体
		if($inp_regpos == "")
		{
			$where_regpos = "";
		}
		else
		{
			$where_regpos = "and tmm.f_regist_pos='$inp_regpos' ";
                        $where_master .= " and f_regist_pos='$inp_regpos ";
		}

		//残り購入コイン数(開始)
		if($inp_s_coin == "")
		{
			$where_s_coin = "";
		}
		else
		{
			$where_s_coin = "and tm.f_coin>='$inp_s_coin' ";
                        $where_mem .= " and f_coin>='$inp_s_coin' ";
		}

		//残り購入コイン数(終了)
		if($inp_e_coin == "")
		{
			$where_e_coin = "";
		}
		else
		{
			$where_e_coin = "and tm.f_coin<='$inp_e_coin' ";
                        $where_mem .= " and f_coin<='$inp_e_coin' ";
		}

		//残りサービスコイン数(開始)
		if($inp_s_free == "")
		{
			$where_s_free = "";
		}
		else
		{
			$where_s_free = "and tm.f_free_coin>='$inp_s_free' ";
                        $where_mem .= " and f_free_coin>='$inp_s_free' ";
		}

		//残りサービスコイン数(終了)
		if($inp_e_free == "")
		{
			$where_e_free = "";
		}
		else
		{
			$where_e_free = "and tm.f_free_coin<='$inp_e_free' ";
                    $where_mem .= " and f_free_coin<='$inp_e_free' ";
		}

		//合計コイン購入金額(開始)
		if($inp_s_totalpay == "")
		{
			$where_s_totalpay = "";
		}
		else
		{
			$where_s_totalpay = "and tm.f_total_pay>='$inp_s_totalpay' ";
                    $where_mem .=" and f_free_coin<='$inp_e_free' ";
		}

		//合計コイン購入金額(終了)
		if($inp_e_totalpay == "")
		{
			$where_e_totalpay = "";
		}
		else
		{
			$where_e_totalpay = "and tm.f_total_pay<='$inp_e_totalpay' ";
                        $where_mem .=" and f_total_pay<='$inp_e_totalpay' ";
		}

		//性別
		if($inp_sex == "")
		{
			$where_sex = "";
		}
		else
		{
			$where_sex = "and tmm.f_sex='$inp_sex' ";
                        $where_master .= " and f_sex='$inp_sex' ";
		}

		//会員ID
		if($inp_mem_id == "")
		{
			$where_mid = "";
		}
		else
		{
			$where_mid = " and tmm.fk_member_id=$inp_mem_id ";
                        $where_master .=" and fk_member_id=$inp_mem_id ";
                        $where_mem .=" and fk_member_id=$inp_mem_id ";
		}

		//氏名
		if($inp_name == "")
		{
			$where_name = "";
		}
		else
		{
			$where_name =" and (tmm.f_name like '%$inp_name%') ";
                        $where_master .=" and f_name like '%$inp_name%' ";
		}

		//地域
		if($inp_area == "" or $inp_area==0)
		{
			$where_area = "";
		}
		else
		{
			$where_area =" and (tmm.fk_area_id=$inp_area) ";
                        $where_master .=" and (fk_area_id=$inp_area) ";
		}

		//職業
		if($inp_job == "" or $inp_job==0)
		{
			$where_job = "";
		}
		else
		{
			$where_job =" and (tmm.fk_job_id=$inp_job) ";
                        $where_master .=" and (fk_job_id=$inp_job) ";
		}

		//IPアドレス
		if($inp_ip == "")
		{
			$where_ip = "";
		}
		else
		{
			$where_ip =" and (tmm.f_ip='$inp_ip') ";
                        $where_master .=" and (f_ip='$inp_ip') ";
		}

		//未払い商品数
		if($inp_unpain == "")
		{
			$where_unpain = "";
		}
		else
		{
			$where_unpain = " and (tm.f_unpain_num=$inp_unpain) ";
                        $where_mem .=" and f_unpain_num=$inp_unpain ";
		}

		//キャンセル商品数
		if($inp_cancel == "")
		{
			$where_cancel = "";
		}
		else
		{
			$where_cancel = " and (tm.f_cancel_num=$inp_cancel) ";
                        $where_mem .=" and f_cancel_num=$inp_cancel ";
		}

		//最終ログイン日(開始)
		if($inp_s_last_entry == "")
		{
			$where_s_last_entry = "";
		}
		else
		{
			$where_s_last_entry = " and tm.f_last_entry>='$inp_s_last_entry 00:00:00' ";
                        $where_mem .="  and f_last_entry>='$inp_s_last_entry 00:00:00' ";
		}

		//最終ログイン日(終了)
		if($inp_e_last_entry == "")
		{
			$where_e_last_entry = "";
		}
		else
		{
			$where_e_last_entry =" and tm.f_last_entry<='$inp_e_last_entry 23:59:59' ";
                        $where_mem .="  and f_last_entry<='$inp_e_last_entry 23:59:59' ";

		}

//		//ソートキー
//		if($order == "")
//		{
//			$order = "tm.fk_member_id";
//		}
		$order = "";

		//echo  $sort."<BR>\n";
		switch ($sort)
		{
			case 0:
				$order = "tm.fk_member_id ";
				break;
			case 1:
				$order = "tm.fk_member_id desc";
				break;
			case 2:
				$order = "tm.f_total_pay ";
				break;
			case 3:
				$order = "tm.f_total_pay desc ";
				break;
			case 4:
				$order = "tm.f_last_entry ";
				break;
			case 5:
				$order = "tm.f_last_entry desc ";
				break;
			case 6:
				$order = "p1.pay_total ";
//				$order = "p2.pay_total ";
				break;
			case 7:
				$order = "p1.pay_total desc ";
//				$order = "p2.pay_total desc ";
				break;
//			case 8:
//			$order = "tm.last_entry_str ";
//				break;
//			case 9:
//			$order = "tm.last_entry_str desc ";
//				break;
			case 10:
			$order = "p2.last_time ";
					break;
			case 11:
			$order = "p2.last_time desc ";
					break;
			case 12:
			$order = "p3.pay_record ";
					break;
			case 13:
			$order = "p3.pay_record desc ";
					break;
                        case 14:
			$order = "tm.f_unpain_num ";
					break;
			case 15:
			$order = "tm.f_unpain_num desc ";
					break;
                        case 16:
			$order = "tm.f_cancel_num ";
					break;
			case 17:
			$order = "tm.f_cancel_num desc ";
					break;
		}

//		$sql_cnt  = "select count(tmm.fk_member_id) from
//                    (select fk_member_id from auction.t_member_master
//                    where fk_member_id>0  tmm, auction.t_member as tm ";
////		$sql_cnt  = "select count(*) from auction.t_member_master as tmm, auction.t_member as tm ";
//		$sql_cnt .= "where tmm.fk_member_id>0 ";
//		$sql_cnt .= $where_activity;
//		$sql_cnt .= $where_id_name;
//		$sql_cnt .= $where_parent_ad;
//		$sql_cnt .= $where_group;
//		$sql_cnt .= $where_mail;
//		$sql_cnt .= $where_s_regist;
//		$sql_cnt .= $where_e_regist;
//		$sql_cnt .= $where_regpos;
//		$sql_cnt .= $where_s_coin;
//		$sql_cnt .= $where_e_coin;
//		$sql_cnt .= $where_s_free;
//		$sql_cnt .= $where_e_free;
//		$sql_cnt .= $where_s_totalpay;
//		$sql_cnt .= $where_e_totalpay;
//		$sql_cnt .= $where_sex;
//		$sql_cnt .= $where_mid;
//		$sql_cnt .= $where_name;
//		$sql_cnt .= $where_area;
//		$sql_cnt .= $where_job;
//		$sql_cnt .= $where_ip;
//		$sql_cnt .= $where_unpain;
//		$sql_cnt .= $where_cancel;
//		$sql_cnt .= $where_s_last_entry;
//		$sql_cnt .= $where_e_last_entry;
//		$sql_cnt .= "and tmm.fk_member_id=tm.fk_member_id ";

                $sql_cnt ="Select count(tmm.fk_member_id)
                    from
                    (select fk_member_id from auction.t_member_master
                    where fk_member_id>0 and f_delete_flag=0  $where_master ) tmm ,
                    (select fk_member_id from auction.t_member
                    where fk_member_id>0 $where_mem ) tm where tmm.fk_member_id=tm.fk_member_id";

                //echo $sql_cnt;
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		// インラインビューp2をp1にマージ
		$sql  = "select tmm.fk_member_id,tmm.f_activity,tmm.f_login_id,tmm.f_login_pass,tmm.f_handle,tmm.fk_member_group_id,tmm.f_ip,";
		$sql .= "tmm.f_sex,tmm.f_regist_date,tmm.f_mail_address_pc,tmm.f_mail_address_mb,tmm.fk_parent_ad,";
		$sql .= "tmm.fk_address_flg,tmm.f_delete_flag,tm.f_coin,tm.f_free_coin,tm.f_last_entry,tm.f_total_pay,tm.f_unpain_num,tm.f_cancel_num,";
		$sql .= "p2.last_time,p1.pay_total,p3.pay_record ";
		$sql .= "FROM auction.t_member_master AS tmm
						INNER JOIN auction.t_member tm ON tm.fk_member_id=tmm.fk_member_id
						LEFT OUTER JOIN
						(SELECT SUM(f_pay_money)AS pay_total,f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0 AND f_cert_status=0  GROUP BY f_member_id) p1
						ON p1.f_member_id=tm.fk_member_id
						LEFT OUTER JOIN
						(SELECT MAX(f_tm_stamp)AS last_time,f_member_id FROM auction.t_pay_log WHERE f_status<>4 AND f_cert_status=0 GROUP BY f_member_id) p2
						ON p2.f_member_id=tm.fk_member_id
						LEFT OUTER JOIN
						(SELECT COUNT(f_member_id) AS pay_record,f_member_id FROM(SELECT f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0  AND f_cert_status=0 GROUP BY f_member_id,fk_products_id) p3 GROUP BY f_member_id) p3
						ON p3.f_member_id=tm.fk_member_id ";
		$sql .= "where tmm.fk_member_id>0 ";
		$sql .= $where_activity;
		$sql .= $where_id_name;
		$sql .= $where_parent_ad;
		$sql .= $where_group;
		$sql .= $where_mail;
		$sql .= $where_s_regist;
		$sql .= $where_e_regist;
		$sql .= $where_regpos;
		$sql .= $where_s_coin;
		$sql .= $where_e_coin;
		$sql .= $where_s_free;
		$sql .= $where_e_free;
		$sql .= $where_s_totalpay;
		$sql .= $where_e_totalpay;
		$sql .= $where_sex;
		$sql .= $where_mid;
		$sql .= $where_name;
		$sql .= $where_area;
		$sql .= $where_job;
		$sql .= $where_ip;
		$sql .= $where_unpain;
		$sql .= $where_cancel;
		$sql .= $where_s_last_entry;
		$sql .= $where_e_last_entry;
		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";
		$sql .= "order by $order ";
//		$sql .= "order by $order $sort ";
//		$sql .= "order by tm.fk_member_id desc ";
		$sql .= "limit $limit offset $offset ";
//		ORDER BY tm.fk_member_id DESC
		//print "$sql<BR>\n";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_id[$i]		= mysql_result($result, $i, "fk_member_id");
			$f_activity[$i]			= mysql_result($result, $i, "f_activity");
			$f_login_id[$i]			= mysql_result($result, $i, "f_login_id");
			$f_login_pass[$i]		= mysql_result($result, $i, "f_login_pass");
			$f_handle[$i]			= mysql_result($result, $i, "f_handle");
			$fk_member_group_id[$i]	= mysql_result($result, $i, "fk_member_group_id");
			$f_sex[$i]				= mysql_result($result, $i, "f_sex");
			$f_regist_date[$i]		= mysql_result($result, $i, "f_regist_date");
			$f_mail_address_pc[$i]	= mysql_result($result, $i, "f_mail_address_pc");
			$f_mail_address_mb[$i]	= mysql_result($result, $i, "f_mail_address_mb");
			$fk_parent_ad[$i]		= mysql_result($result, $i, "fk_parent_ad");
			$fk_address_flg[$i]		= mysql_result($result, $i, "fk_address_flg");
			$f_delete_flag[$i]		= mysql_result($result, $i, "f_delete_flag");
			$f_coin[$i]				= mysql_result($result, $i, "f_coin");
			$f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
			$f_last_entry[$i]		= mysql_result($result, $i, "f_last_entry");
			$f_total_pay[$i]		= mysql_result($result, $i, "f_total_pay");
			$f_tm_stamp[$i]			= mysql_result($result, $i, "last_time");
			$f_pay_money[$i]		= mysql_result($result, $i, "pay_total");
			$pay_record[$i]			= mysql_result($result, $i, "pay_record");
			$ip[$i]					= mysql_result($result, $i, "f_ip");
			$f_unpain_num[$i]		= mysql_result($result, $i, "f_unpain_num");
			$f_cancel_num[$i]		= mysql_result($result, $i, "f_cancel_num");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

    function GetTMemberList_test($inp_activity,$inp_id_name,$inp_parent_ad,$inp_group,$inp_mail,$inp_s_regist,$inp_e_regist,$inp_regpos,$inp_s_coin,$inp_e_coin,$inp_s_free,$inp_e_free,$inp_s_totalpay,$inp_e_totalpay,$inp_sex,$inp_mem_id,$inp_name,$inp_unpain,$inp_cancel,$inp_s_last_entry,$inp_e_last_entry,$sort,$limit,$offset,&$fk_member_id,&$f_activity,&$f_login_id,&$f_login_pass,&$f_handle,&$fk_member_group_id,&$f_sex,&$f_regist_date,&$f_mail_address_pc,&$f_mail_address_mb,&$fk_parent_ad,&$fk_address_flg,&$f_delete_flag,&$f_coin,&$f_free_coin,&$f_last_entry,&$all_count,&$data_cnt,&$f_total_pay,&$f_tm_stamp,&$f_pay_money,&$pay_record,&$ip,&$f_unpain_num,&$f_cancel_num,$inp_area,$inp_job,$inp_ip)
    {
        //echo $sort;
        //■ＤＢ接続
        $db = db_connect();
        if( $db == false ) {
            exit;
        }
        //状態
        $where_activity = "";
        if( strlen($inp_activity) ) {
            $where_activity = "and tmm.f_activity='$inp_activity' ";
        }
        //ログインID・ハンドルネーム
        $where_id_name = "";
        if( strlen($inp_id_name) ) {
            $where_id_name = "and ((tmm.f_login_id like '%$inp_id_name%') or (tmm.f_handle like '%$inp_id_name%')) ";
        }
        //広告/紹介コード
        $where_parent_ad = "";
        if( strlen($inp_parent_ad) ) {
            $where_parent_ad = "and tmm.fk_parent_ad='$inp_parent_ad' ";
        }
        //会員グループ
        $where_group = "";
        if( strlen($inp_group) ) {
            $where_group = "and tmm.fk_member_group_id='$inp_group' ";
        }
        //メールアドレス
        $where_mail = "";
        if( strlen($inp_mail) ) {
            $where_mail = "and ((tmm.f_mail_address_pc like '%$inp_mail%') or (tmm.f_mail_address_mb like '%$inp_mail%')) ";
        }
        //登録日(開始)
        $where_s_regist = "";
        if( strlen($inp_s_regist) ) {
            $where_s_regist = "and tmm.f_regist_date>='$inp_s_regist 00:00:00' ";
        }
        //登録日(終了)
        $where_e_regist = "";
        if( strlen($inp_e_regist) ) {
            $where_e_regist = "and tmm.f_regist_date<='$inp_e_regist 23:59:59' ";
        }
        //登録媒体
        if($inp_regpos == "") {
            $where_regpos = "";
        } else {
            $where_regpos = "and tmm.f_regist_pos='$inp_regpos' ";
        }
        //残り購入コイン数(開始)
        $where_s_coin = "";
        if( strlen($inp_s_coin) ) {
            $where_s_coin = "and tm.f_coin>='$inp_s_coin' ";
        }
        //残り購入コイン数(終了)
        $where_e_coin = "";
        if( strlen($inp_e_coin) ) {
            $where_e_coin = "and tm.f_coin<='$inp_e_coin' ";
        }
        //残りサービスコイン数(開始)
        $where_s_free = "";
        if( strlen($inp_s_free) ) {
            $where_s_free = "and tm.f_free_coin>='$inp_s_free' ";
        }
        //残りサービスコイン数(終了)
        $where_e_free = "";
        if( strlen($inp_e_free) ) {
            $where_e_free = "and tm.f_free_coin<='$inp_e_free' ";
        }
        //合計コイン購入金額(開始)
        $where_s_totalpay = "";
        if( strlen($inp_s_totalpay) ) {
            $where_s_totalpay = "and tm.f_total_pay>='$inp_s_totalpay' ";
        }
        //合計コイン購入金額(終了)
        $where_e_totalpay = "";
        if( strlen($inp_e_totalpay) ) {
            $where_e_totalpay = "and tm.f_total_pay<='$inp_e_totalpay' ";
        }
        //性別
        $where_sex = "";
        if( strlen($inp_sex) ) {
            $where_sex = "and tmm.f_sex='$inp_sex' ";
        }
        //会員ID
        $where_mid = "";
        if( strlen($inp_mem_id) ) {
            $where_mid = " and tmm.fk_member_id=$inp_mem_id ";
        }
        //氏名
        $where_name = "";
        if( strlen($inp_name) ) {
            $where_name =" and (tmm.f_name like '%$inp_name%') ";
        }
        //地域
        $where_area = "";
        if( $inp_area ) {
            $where_area =" and (tmm.fk_area_id=$inp_area) ";
        }
        //職業
        $where_job = "";
        if( $inp_job ) {
            $where_job =" and (tmm.fk_job_id=$inp_job) ";
        }
        //IPアドレス
        $where_ip = "";
        if( strlen($inp_ip) ) {
            $where_ip =" and (tmm.f_ip='$inp_ip') ";
        }
        //未払い商品数
        $where_unpain = "";
        if( strlen($inp_unpain) ) {
            $where_unpain = " and (tm.f_unpain_num=$inp_unpain) ";
        }
        //キャンセル商品数
        $where_cancel = "";
        if( strlen($inp_cancel) ) {
            $where_cancel = " and (tm.f_cancel_num=$inp_cancel) ";
        }
        //最終ログイン日(開始)
        $where_s_last_entry = "";
        if( strlen($inp_s_last_entry) ) {
            $where_s_last_entry = " and tm.f_last_entry>='$inp_s_last_entry 00:00:00' ";
        }
        //最終ログイン日(終了)
        $where_e_last_entry = "";
        if( strlen($inp_e_last_entry) ) {
            $where_e_last_entry =" and tm.f_last_entry<='$inp_e_last_entry 23:59:59' ";
        }

        $order = "";

        //echo  $sort."<BR>\n";
        switch ($sort)
        {
            case 0:
                $order = "tm.fk_member_id ";
                break;
            case 1:
                $order = "tm.fk_member_id desc";
                break;
            case 2:
                $order = "tm.f_total_pay ";
                break;
            case 3:
                $order = "tm.f_total_pay desc ";
                break;
            case 4:
                $order = "tm.f_last_entry ";
                break;
            case 5:
                $order = "tm.f_last_entry desc ";
                break;
/* TODO:6,7,10,11,12,13は保留中、アプリケーションソート処理を実装すること
            case 6:
                $order = "p1.pay_total ";
                break;
            case 7:
                $order = "p1.pay_total desc ";
                break;
            case 10:
                $order = "p1.last_time ";
                break;
            case 11:
                $order = "p1.last_time desc ";
                break;
            case 12:
                $order = "p3.pay_record ";
                 break;
            case 13:
                $order = "p3.pay_record desc ";
                break;
*/
            case 14:
                $order = "tm.f_unpain_num ";
                break;
            case 15:
                $order = "tm.f_unpain_num desc ";
                break;
            case 16:
                $order = "tm.f_cancel_num ";
                break;
            case 17:
                $order = "tm.f_cancel_num desc ";
                break;
        }

        $sql_cnt = "
SELECT
  COUNT(tmm.fk_member_id)
FROM
  auction.t_member_master as tmm
WHERE
  tmm.fk_member_id>0
  ".$where_activity."
  ".$where_id_name."
  ".$where_parent_ad."
  ".$where_group."
  ".$where_mail."
  ".$where_s_regist."
  ".$where_e_regist."
  ".$where_regpos."
  ".$where_sex."
  ".$where_mid."
  ".$where_name."
  ".$where_area."
  ".$where_job."
  ".$where_ip."
";
//  -- tm
//  ".$where_s_coin."
//  ".$where_e_coin."
//  ".$where_s_free."
//  ".$where_e_free."
//  ".$where_s_totalpay."
//  ".$where_e_totalpay."
//  ".$where_unpain."
//  ".$where_cancel."
//  ".$where_s_last_entry."
//  ".$where_e_last_entry."
//";
//echo nl2br($sql_cnt).'<BR><BR>';
        //■ＳＱＬ実行
        $result = mysql_query($sql_cnt, $db);
        if( $result == false ) {
            print "$sql_cnt<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■全件数
        $all_count = mysql_result($result, 0, 0);

        // ,tm.f_coin,tm.f_free_coin,tm.f_last_entry,tm.f_total_pay,tm.f_unpain_num,tm.f_cancel_num
        $sql = "
SELECT
  tmm.fk_member_id,tmm.f_activity,tmm.f_login_id,tmm.f_login_pass,tmm.f_handle,tmm.fk_member_group_id,tmm.f_ip,
  tmm.f_sex,tmm.f_regist_date,tmm.f_mail_address_pc,tmm.f_mail_address_mb,tmm.fk_parent_ad,
  tmm.fk_address_flg,tmm.f_delete_flag
FROM
  auction.t_member_master AS tmm
INNER JOIN
  auction.t_member tm ON tm.fk_member_id=tmm.fk_member_id
WHERE
  tmm.fk_member_id>0
  AND tmm.fk_member_id=tm.fk_member_id
  ".$where_activity."
  ".$where_id_name."
  ".$where_parent_ad."
  ".$where_group."
  ".$where_mail."
  ".$where_s_regist."
  ".$where_e_regist."
  ".$where_regpos."
  ".$where_sex."
  ".$where_mid."
  ".$where_name."
  ".$where_area."
  ".$where_job."
  ".$where_ip."
ORDER BY fk_member_id DESC
LIMIT ".$limit." OFFSET ".$offset."
";
//  -- tm
//  ".$where_s_coin."
//  ".$where_e_coin."
//  ".$where_s_free."
//  ".$where_e_free."
//  ".$where_s_totalpay."
//  ".$where_e_totalpay."
//  ".$where_unpain."
//  ".$where_cancel."
//  ".$where_s_last_entry."
//  ".$where_e_last_entry."
//ORDER BY ".$order."
//LIMIT ".$limit." OFFSET ".$offset."
//";
//echo nl2br($sql).'<BR><BR>';

        //■ＳＱＬ実行
        $result = mysql_query($sql, $db);
        if( $result == false ) {
            print "$sql<BR>\n";
            //■ＤＢ切断
            db_close($db);
            return false;
        }
        //■データ件数
        $data_cnt = mysql_num_rows($result);

        $index_array = array();
        for ($i=0; $i<$data_cnt; $i++) {
            $fk_member_id[$i]       = mysql_result($result, $i, "fk_member_id");
            $f_activity[$i]         = mysql_result($result, $i, "f_activity");
            $f_login_id[$i]         = mysql_result($result, $i, "f_login_id");
            $f_login_pass[$i]       = mysql_result($result, $i, "f_login_pass");
            $f_handle[$i]           = mysql_result($result, $i, "f_handle");
            $fk_member_group_id[$i] = mysql_result($result, $i, "fk_member_group_id");
            $f_sex[$i]              = mysql_result($result, $i, "f_sex");
            $f_regist_date[$i]      = mysql_result($result, $i, "f_regist_date");
            $f_mail_address_pc[$i]  = mysql_result($result, $i, "f_mail_address_pc");
            $f_mail_address_mb[$i]  = mysql_result($result, $i, "f_mail_address_mb");
            $fk_parent_ad[$i]       = mysql_result($result, $i, "fk_parent_ad");
            $fk_address_flg[$i]     = mysql_result($result, $i, "fk_address_flg");
            $f_delete_flag[$i]      = mysql_result($result, $i, "f_delete_flag");
            $ip[$i]                 = mysql_result($result, $i, "f_ip");
            $f_coin[$i]             = NULL;// tm
            $f_free_coin[$i]        = NULL;// tm
            $f_last_entry[$i]       = NULL;// tm
            $f_total_pay[$i]        = NULL;// tm
            $f_unpain_num[$i]       = NULL;// tm
            $f_cancel_num[$i]       = NULL;// tm
            $f_tm_stamp[$i]         = NULL;//サブクエリp1...mysql_result($result, $i, "last_time");
            $f_pay_money[$i]        = NULL;//サブクエリp1...mysql_result($result, $i, "pay_total");
            $pay_record[$i]         = NULL;//サブクエリp3...mysql_result($result, $i, "pay_record");
            // member_idごとの行番号を保存
            $index_array[($fk_member_id[$i])] = $i;
        }

        if ( $data_cnt ) {
            // t_member
$sql = "
SELECT
  tm.fk_member_id,tm.f_coin,tm.f_free_coin,tm.f_last_entry,tm.f_total_pay,tm.f_unpain_num,tm.f_cancel_num
FROM
  auction.t_member AS tm
WHERE
  tm.fk_member_id IN (".( implode(',', $fk_member_id) ).")
";
//echo nl2br($sql).'<BR>';
            if ( $result !== FALSE ) {
                $row_count = mysql_num_rows($result);
                if ( $row_count ) {
                    while ( $row = mysql_fetch_assoc($result) ) {
                        $i = $index_array[($row['fk_member_id'])];
                        $f_coin[$i]       = $row['f_coin'];// tm
                        $f_free_coin[$i]  = $row['f_free_coin'];// tm
                        $f_last_entry[$i] = $row['f_last_entry'];// tm
                        $f_total_pay[$i]  = $row['f_total_pay'];// tm
                        $f_unpain_num[$i] = $row['f_unpain_num'];// tm
                        $f_cancel_num[$i] = $row['f_cancel_num'];// tm
                    }
                }
            }

            // サブクエリ...集計はPHP対応(旧p1,p3)
            $sql = "
SELECT
  f_member_id,fk_products_id,f_pay_money,f_tm_stamp
FROM
  auction.t_pay_log
WHERE
  fk_products_id<>0 AND f_cert_status=0 AND f_member_id IN (".(implode(',', $fk_member_id)).")
ORDER BY
  f_member_id ASC
";
//echo nl2br($sql).'<BR>';
            $result = mysql_query($sql, $db);
            if ( $result !== FALSE ) {
                $row_count = mysql_num_rows($result);
                if ( $row_count ) {
                    $tmp = array(
                        'f_member_id'=>NULL,
                        'last_time'=>NULL,
                        'pay_total'=>0,
                        'fk_products_id'=>NULL,
                        'pay_record'=>0,
                    );
                    while ( $row = mysql_fetch_assoc($result) ) {
                        if ( $tmp['f_member_id'] != $row['f_member_id'] ) {
                            if ( !is_null( $tmp['f_member_id'] ) ) {
                                $f_tm_stamp[($index_array[($tmp['f_member_id'])])] = $tmp['last_time'];
                                $f_pay_money[($index_array[($tmp['f_member_id'])])] = $tmp['pay_total'];
                                $pay_record[($index_array[($tmp['f_member_id'])])] = $tmp['pay_record'];
                            }
                            $tmp['f_member_id'] = $row['f_member_id'];
                            $tmp['last_time'] = $row['f_tm_stamp'];
                            $tmp['pay_total'] = $row['f_pay_money'];
                            $tmp['fk_products_id'] = $row['fk_products_id'];
                            $tmp['pay_record'] = 1;
                        } else {
                            if ( strtotime($row['f_tm_stamp']) > strtotime($tmp['f_tm_stamp']) ) {
                                $tmp['f_tm_stamp'] = $row['f_tm_stamp'];
                            }
                            if ( $tmp['fk_products_id'] != $row['fk_products_id'] ) {
                                $tmp['pay_record']++;
                                $tmp['fk_products_id'] = $row['fk_products_id'];
                            }
                            $tmp['pay_total'] = $tmp['pay_total'] + $row['f_pay_money'];
                        }
                    }
                    // 最終データ
                    if ( $tmp['f_member_id'] ) {
                        $f_tm_stamp[($index_array[($tmp['f_member_id'])])] = $tmp['last_time'];
                        $f_pay_money[($index_array[($tmp['f_member_id'])])] = $tmp['pay_total'];
                        $pay_record[($index_array[($tmp['f_member_id'])])] = $tmp['pay_record'];
                    }
                }
            }
        }
        //■ＤＢ切断
        db_close($db);
        return true;
    }
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員情報取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberInfo($fk_member_id,&$f_login_id,&$f_login_pass,&$f_handle,&$fk_area_id,&$fk_job_id,&$f_activity,&$fk_member_group_id,&$f_mail_address_pc,&$f_mail_address_mb,&$f_ser_no,&$f_mailmagazein_pc,&$f_mailmagazein_mb,&$fk_adcode_id,&$fk_parent_ad,&$f_regist_date,&$f_regist_pos,&$f_name,&$f_tel_no,&$f_sex,&$f_birthday,&$fk_address_flg,&$f_over_money,&$f_coin,&$f_free_coin,&$f_total_pay,&$f_last_entry,&$ip,&$f_unpain_num ,&$f_cancel_num)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select tmm.f_login_id,tmm.f_login_pass,tmm.f_handle,tmm.fk_area_id,tmm.fk_job_id,tmm.f_activity,";
		$sql .= "tmm.fk_member_group_id,tmm.f_mail_address_pc,tmm.f_mail_address_mb,";
		$sql .= "tmm.f_ser_no,tmm.f_mailmagazein_pc,tmm.f_mailmagazein_mb,tmm.f_ip,";
		$sql .= "tmm.fk_adcode_id,tmm.fk_parent_ad,tmm.f_regist_date,tmm.f_regist_pos,";
		$sql .= "tmm.f_name,tmm.f_tel_no,tmm.f_sex,tmm.f_birthday,tmm.fk_address_flg,";
		$sql .= "tmm.f_over_money,tm.f_coin,tm.f_free_coin,tm.f_total_pay,tm.f_last_entry, ";
		$sql .= "tm.f_unpain_num,tm.f_cancel_num ";
		$sql .= "from auction.t_member_master as tmm, auction.t_member as tm ";
		$sql .= "where tmm.fk_member_id='$fk_member_id' ";
		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";

//		$sql  = "select tmm.f_login_id,tmm.f_login_pass,tmm.f_handle,tmm.f_activity,";
//		$sql .= "tmm.fk_member_group_id,tmm.f_mail_address_pc,tmm.f_mail_address_mb,";
//		$sql .= "tmm.f_ser_no,tmm.f_mailmagazein_pc,tmm.f_mailmagazein_mb,";
//		$sql .= "ad.f_adcode,tmm.fk_parent_ad,tmm.f_regist_date,tmm.f_regist_pos,";
//		$sql .= "tmm.f_name,tmm.f_tel_no,tmm.f_sex,tmm.f_birthday,tmm.fk_address_flg,";
//		$sql .= "tmm.f_over_money,tm.f_coin,tm.f_free_coin,tm.f_total_pay,tm.f_last_entry ";
//		//$sql .= "from auction.t_member_master as tmm, auction.t_member as tm,auction.t_pay_log as pl,auction.t_adcode ad ";
//                $sql .= "from auction.t_member_master as tmm, auction.t_member as tm,auction.t_pay_log as pl left join auction.t_adcode ad  on tmm.fk_adcode_id=ad.fk_adcode_id";
//		$sql .= "where tmm.fk_member_id='$fk_member_id' ";
//		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_login_id			= mysql_result($result, 0, "f_login_id");
			$f_login_pass		= mysql_result($result, 0, "f_login_pass");
			$f_handle			= mysql_result($result, 0, "f_handle");
			$fk_area_id			= mysql_result($result, 0, "fk_area_id");
			$fk_job_id			= mysql_result($result, 0, "fk_job_id");
			$f_activity			= mysql_result($result, 0, "f_activity");
			$fk_member_group_id	= mysql_result($result, 0, "fk_member_group_id");
			$f_mail_address_pc	= mysql_result($result, 0, "f_mail_address_pc");
			$f_mail_address_mb	= mysql_result($result, 0, "f_mail_address_mb");
			$f_ser_no			= mysql_result($result, 0, "f_ser_no");
			$f_mailmagazein_pc	= mysql_result($result, 0, "f_mailmagazein_pc");
			$f_mailmagazein_mb	= mysql_result($result, 0, "f_mailmagazein_mb");
			$fk_adcode_id		= mysql_result($result, 0, "fk_adcode_id");
			$fk_parent_ad		= mysql_result($result, 0, "fk_parent_ad");
			$f_regist_date		= mysql_result($result, 0, "f_regist_date");
			$f_regist_pos		= mysql_result($result, 0, "f_regist_pos");
			$f_name				= mysql_result($result, 0, "f_name");
			$f_tel_no			= mysql_result($result, 0, "f_tel_no");
			$f_sex				= mysql_result($result, 0, "f_sex");
			$f_birthday			= mysql_result($result, 0, "f_birthday");
			$fk_address_flg		= mysql_result($result, 0, "fk_address_flg");
			$f_over_money		= mysql_result($result, 0, "f_over_money");
			$f_coin				= mysql_result($result, 0, "f_coin");
			$f_free_coin		= mysql_result($result, 0, "f_free_coin");
			$f_total_pay		= mysql_result($result, 0, "f_total_pay");
			$f_last_entry		= mysql_result($result, 0, "f_last_entry");
			$ip					= mysql_result($result, 0, "f_ip");
			$f_cancel_num		= mysql_result($result, 0, "f_cancel_num");
			$f_unpain_num		= mysql_result($result, 0, "f_unpain_num");
		}
		else
		{
			$f_login_id			= "";
			$f_login_pass		= "";
			$f_handle			= "";
			$fk_area_id			= "";
			$fk_job_id			= "";
			$f_activity			= "";
			$fk_member_group_id	= "";
			$f_mail_address_pc	= "";
			$f_mail_address_mb	= "";
			$f_ser_no			= "";
			$f_mailmagazein_pc	= "";
			$f_mailmagazein_mb	= "";
			$fk_adcode_id		= "";
			$fk_parent_ad		= "";
			$f_regist_date		= "";
			$f_regist_pos		= "";
			$f_name				= "";
			$f_tel_no			= "";
			$f_sex				= "";
			$f_birthday			= "";
			$fk_address_flg		= 0;
			$f_over_money		= 0;
			$f_coin				= 0;
			$f_free_coin		= 0;
			$f_total_pay		= 0;
			$f_last_entry		= "";
			$ip					= "0.0.0.0";
//			$f_tm_stamp			= "";
			$f_cancel_num		= 0;
			$f_unpain_num		= 0;
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員広告コード取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberFKAdcodeId($fk_member_id,&$fk_adcode_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_adcode_id ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$fk_adcode_id = mysql_result($result, 0, "fk_adcode_id");
		}
		else
		{
			$fk_adcode_id = "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員親紹介コード取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberFKParentAd($fk_member_id,&$fk_parent_ad)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_parent_ad ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$fk_parent_ad = mysql_result($result, 0, "fk_parent_ad");
		}
		else
		{
			$fk_parent_ad = "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員情報登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function RegistTMemberData($f_login_id,$inp_pass,$f_handle,$f_activity,$fk_member_group_id,$f_mail_address_pc,$f_mail_address_mb,$f_mailmagazein_pc,$f_mailmagazein_mb,$f_name,$f_tel_no,$f_sex,$f_birthday,$fk_adcode_id,$f_regist_pos,$f_memo)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//パスワード暗号化
		$f_login_pass = md5($inp_pass);

		//配信メルマガ
		if($f_mailmagazein_pc == "")
		{
			$f_mailmagazein_pc = 0;
		}
		if($f_mailmagazein_mb == "")
		{
			$f_mailmagazein_mb = 0;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
/*
		$sql  = "insert into auction.t_member_master values ";
		$sql .= "(0,'$f_login_id','$f_login_pass','$f_handle','','',";
		$sql .= "'$f_name','$f_tel_no','$f_sex','$f_birthday',";
		$sql .= "'0','0','0','$f_mail_address_pc','$f_mail_address_mb',";
		$sql .= "'$f_mailmagazein_pc','$f_mailmagazein_mb','$f_activity',";
		$sql .= "'0','0','0','$fk_member_group_id','0',now(),'0','0',now(),'0') ";
*/
		$sql  = "insert into auction.t_member_master ";
		$sql .= "(fk_member_id,f_login_id,f_login_pass,f_handle,f_name,f_tel_no,f_sex,f_birthday,";
		$sql .= "f_mail_address_pc,f_mail_address_mb,f_mailmagazein_pc,f_mailmagazein_mb,f_activity,";
		$sql .= "fk_member_group_id,fk_adcode_id,f_regist_pos,f_memo,f_regist_date,f_tm_stamp) ";
		$sql .= "values (0,'$f_login_id','$f_login_pass','$f_handle','$f_name','$f_tel_no','$f_sex','$f_birthday',";
		$sql .= "'$f_mail_address_pc','$f_mail_address_mb','$f_mailmagazein_pc','$f_mailmagazein_mb','$f_activity',";
		$sql .= "'$fk_member_group_id','$fk_adcode_id','$f_regist_pos','$f_memo',now(),now()) ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//t_member
		$sql  = "insert into auction.t_member ";
		$sql .= "(fk_member_id,f_tm_stamp) ";
		$sql .= "values (0,now()) ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員情報更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateTMemberInfo($staff_mail,$staff_address,$fk_member_id,$f_login_id,$inp_pass,$f_handle,$fk_area,$fk_job,$f_activity,$fk_member_group_id,$f_mail_address_pc,$f_mail_address_mb,$f_mailmagazein_pc,$f_mailmagazein_mb,$f_regist_date,$f_name,$f_tel_no,$f_sex,$f_birthday,$f_unpain_num = null,$f_cancel_num = null,$tm_f_coin,$tm_f_free_coin,$tm_f_total_pay,$tmm_f_over_money,$op_level)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//"状態"取得
		$sql  = "select f_activity ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$current_f_activity	= mysql_result($result, 0, "f_activity");
		}
		else
		{
			$current_f_activity	= "";
		}

		//暗号化
		$f_login_pass = md5($inp_pass);

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		$sql  = "update auction.t_member_master set ";
		$sql .= "f_login_id='$f_login_id',";
		$sql .= "f_handle='$f_handle',";

		if($current_f_activity != "99")
		{
			$sql .= "f_activity='$f_activity',";
		}
		$sql .= "fk_member_group_id='$fk_member_group_id' ";
		if($staff_mail==true)
		{
			$sql .= ",f_mail_address_pc='$f_mail_address_pc',";
			$sql .= " f_mail_address_mb='$f_mail_address_mb' ";
		}

		$sql .= ",f_mailmagazein_pc='$f_mailmagazein_pc',";
		$sql .= " f_mailmagazein_mb='$f_mailmagazein_mb',";
//		$sql .= "f_regist_date='$f_regist_date',";
		$sql .= "f_regist_date='$f_regist_date' ";

		if($staff_address==true)
		{
			$sql .= ",f_name='$f_name',";
			$sql .= " f_tel_no='$f_tel_no',";
			$sql .= " f_sex='$f_sex',";
			$sql .= " f_birthday='$f_birthday', ";
			$sql .= "fk_area_id=$fk_area,";
			$sql .= "fk_job_id=$fk_job ";
		}

		if($op_level==8)
		{
			$sql .= ",f_over_money=$tmm_f_over_money ";
		}
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		if($f_unpain_num != NULL && $f_cancel_num != null)
		{
//			$sql = "update auction.t_member set f_unpain_num=$f_unpain_num ,f_cancel_num=$f_cancel_num where fk_member_id=$fk_member_id";
			$sql = "update auction.t_member set f_unpain_num=$f_unpain_num ,f_cancel_num=$f_cancel_num ";
			if($op_level==8)
			{
				$sql .= ",f_coin=$tm_f_coin,";
				$sql .= "f_free_coin=$tm_f_free_coin,";
				$sql .= "f_total_pay=$tm_f_total_pay ";
			}
			$sql .= "where fk_member_id='$fk_member_id' ";
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
				db_close($db);
				return false;
			}
		}

//G.Chin 2010-08-05 add sta
		//"退会"の場合、会員の全ての自動入札を中止
		if($f_activity == 2)
		{
			$sql  = "update auction.t_auto_bidder set f_status=2 ";
			$sql .= "where fk_member_id='$fk_member_id' and f_status = 0";

			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
		}
//G.Chin 2010-08-05 add end

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員情報削除関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function DeleteTMemberData($fk_member_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
		$sql = "update auction.t_member_master set f_delete_flag=1 where fk_member_id='$fk_member_id' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

//G.Chin 2010-08-05 add sta
		//会員の全ての自動入札を中止
		$sql  = "update auction.t_auto_bidder set f_status=2 ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
//G.Chin 2010-08-05 add end

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員パスワード設定関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function SetTMemberLoginPass($fk_member_id, $inp_pass)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//暗号化
		$f_login_pass = md5($inp_pass);

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
		$sql = "update auction.t_member_master set f_login_pass='$f_login_pass' where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員コイン情報加算関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function AddTMemberCoinData($fk_member_id, $f_coin, $f_free_coin, $f_total_pay, $f_over_money)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//初期化
		if($f_coin == "")		$f_coin = 0;
		if($f_free_coin == "")	$f_free_coin = 0;
		if($f_total_pay == "")	$f_total_pay = 0;
		if($f_over_money == "")	$f_over_money = 0;

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member
//G.Chin AWKC-218 2010-11-12 chg sta
/*
		$sql  = "update auction.t_member set f_coin=f_coin+$f_coin,";
		$sql .= "f_free_coin=f_free_coin+$f_free_coin,";
		$sql .= "f_total_pay=f_total_pay+$f_total_pay ";
		$sql .= "where fk_member_id='$fk_member_id' ";
*/
		$sql  = "update auction.t_member set f_coin=f_coin+$f_coin,";
		$sql .= "f_free_coin=f_free_coin+$f_free_coin,";
		$sql .= "f_total_pay=f_total_pay+$f_total_pay,";
		$sql .= "f_total_pay_cnt=f_total_pay_cnt+1 ";
		$sql .= "where fk_member_id='$fk_member_id' ";
//G.Chin AWKC-218 2010-11-12 chg end

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//t_member_master
		$sql  = "update auction.t_member_master set f_over_money=f_over_money+$f_over_money ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		親会員無料コイン加算関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function AddParentMemberFreeCoin($fk_member_id, $fk_parent_ad)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//▼会員のコイン購入金額合計を取得
		//t_member
		$sql  = "select f_total_pay from auction.t_member ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_total_pay = mysql_result($result, 0, "f_total_pay");
		}
		else
		{
			$f_total_pay = 0;
		}

		//▼紹介者支払コイン枚数取得
		//t_ad_condition
		$sql  = "select f_adpricode,f_pay_value from auction.t_ad_condition ";
		$sql .= "where f_type=0 and f_spend_value<='$f_total_pay' ";
		$sql .= "order by f_spend_value desc ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_adpricode = mysql_result($result, 0, "f_adpricode");
			$f_pay_value = mysql_result($result, 0, "f_pay_value");
		}
		else
		{
			//■ＤＢ切断
		    db_close($db);
			return true;
		}

		//▼会員の現在の優先度を取得
		//t_member_master
		$sql  = "select f_adpri_value from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_adpri_value = mysql_result($result, 0, "f_adpri_value");
		}
		else
		{
			$f_adpri_value = 0;
		}

		//▼今回の優先度が前回を上回る場合
		if($f_adpricode > $f_adpri_value)
		{
			//▼紹介者の会員IDを取得
			//t_member_master
			$sql  = "select fk_member_id from auction.t_member_master ";
			$sql .= "where fk_adcode_id='$fk_parent_ad' ";

			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($result);

			if($rows > 0)
			{
				$parent_id = mysql_result($result, 0, "fk_member_id");
			}
			else
			{
				//■ＤＢ切断
			    db_close($db);
				return true;
			}

			//■トランザクション開始
			mysql_query("BEGIN", $db);

			//▼紹介者に無料コインを加算
			//t_member
			$sql  = "update auction.t_member set f_free_coin=f_free_coin+$f_pay_value ";
			$sql .= "where fk_member_id='$parent_id' ";

			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}

			//▼会員の報酬優先度番号を変更
			//t_member_master
			$sql  = "update auction.t_member_master set f_adpri_value='$f_adpricode' ";
			$sql .= "where fk_member_id='$fk_member_id' ";

			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}

			//■コミット
			mysql_query("COMMIT", $db);
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		広告コード検索会員ID取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function SrcFAdcodeIdGetTMemberId($fk_adcode_id,&$fk_member_id,&$f_login_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select fk_member_id,f_login_id from auction.t_member_master where fk_adcode_id='$fk_adcode_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$fk_member_id	= mysql_result($result, 0, "fk_member_id");
			$f_login_id		= mysql_result($result, 0, "f_login_id");
		}
		else
		{
			$fk_member_id	= "";
			$f_login_id		= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		グループID指定会員数取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function SrcFKGroupIdCountMember($fk_member_group_id, &$all_count)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql_cnt = "select count(*) from auction.t_member_master where fk_member_group_id='$fk_member_group_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員コイン情報取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberCoinData($fk_member_id,&$f_coin,&$f_free_coin,&$f_total_pay,&$f_over_money)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//t_member
		$sql = "select f_coin,f_free_coin,f_total_pay from auction.t_member where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_coin			= mysql_result($result, 0, "f_coin");
			$f_free_coin	= mysql_result($result, 0, "f_free_coin");
			$f_total_pay	= mysql_result($result, 0, "f_total_pay");
		}
		else
		{
			$f_coin			= "";
			$f_free_coin	= "";
			$f_total_pay	= "";
		}

		//t_member_master
		$sql = "select f_over_money from auction.t_member_master where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_over_money	= mysql_result($result, 0, "f_over_money");
		}
		else
		{
			$f_over_money	= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員コイン情報取得関数(会員落札商品入金用)																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberCoinData_money($fk_member_id,&$f_coin,&$f_free_coin,&$f_total_pay,&$f_over_money,$db)
	{
		//■ＤＢ接続
//		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//t_member
		$sql = "select f_coin,f_free_coin,f_total_pay from auction.t_member where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
//		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_coin			= mysql_result($result, 0, "f_coin");
			$f_free_coin	= mysql_result($result, 0, "f_free_coin");
			$f_total_pay	= mysql_result($result, 0, "f_total_pay");
		}
		else
		{
			$f_coin			= "";
			$f_free_coin	= "";
			$f_total_pay	= "";
		}

		//t_member_master
		$sql = "select f_over_money from auction.t_member_master where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
//		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_over_money	= mysql_result($result, 0, "f_over_money");
		}
		else
		{
			$f_over_money	= "";
		}

		//■ＤＢ切断
//	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員ログインIDハンドル取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberLoginIdHandle($fk_member_id, &$f_login_id, &$f_handle)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select f_login_id,f_handle from auction.t_member_master where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_login_id	= mysql_result($result, 0, "f_login_id");
			$f_handle	= mysql_result($result, 0, "f_handle");
		}
		else
		{
			$f_login_id	= "";
			$f_handle	= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員住所一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTAddressList($fk_member_id,&$fk_address_id,&$f_status,&$f_last_send,&$f_name,&$f_tel_no,&$f_post_code,&$fk_perf_id,&$f_address1,&$f_address2,&$f_address3,&$f_tm_stamp,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_address_id,f_status,f_last_send,f_name,";
		$sql .= "f_tel_no,f_post_code,fk_perf_id,";
		$sql .= "f_address1,f_address2,f_address3,f_tm_stamp ";
		$sql .= "from auction.t_address where fk_member_id='$fk_member_id' and f_status!=0 ";
		$sql .= "order by fk_address_id ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_address_id[$i]	= mysql_result($result, $i, "fk_address_id");
			$f_status[$i]		= mysql_result($result, $i, "f_status");
			$f_last_send[$i]	= mysql_result($result, $i, "f_last_send");
			$f_name[$i]			= mysql_result($result, $i, "f_name");
			$f_tel_no[$i]		= mysql_result($result, $i, "f_tel_no");
			$f_post_code[$i]	= mysql_result($result, $i, "f_post_code");
			$fk_perf_id[$i]		= mysql_result($result, $i, "fk_perf_id");
			$f_address1[$i]		= mysql_result($result, $i, "f_address1");
			$f_address2[$i]		= mysql_result($result, $i, "f_address2");
			$f_address3[$i]		= mysql_result($result, $i, "f_address3");
			$f_tm_stamp[$i]		= mysql_result($result, $i, "f_tm_stamp");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員住所取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTAddressInfo($fk_address_id,&$f_status,&$fk_member_id,&$f_last_send,&$f_name,&$f_tel_no,&$f_post_code,&$fk_perf_id,&$f_address1,&$f_address2,&$f_address3,&$f_tm_stamp)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_status,fk_member_id,f_last_send,f_name,";
		$sql .= "f_tel_no,f_post_code,fk_perf_id,";
		$sql .= "f_address1,f_address2,f_address3,f_tm_stamp ";
		$sql .= "from auction.t_address where fk_address_id='$fk_address_id' and f_status!=0 ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_status		= mysql_result($result, 0, "f_status");
			$fk_member_id	= mysql_result($result, 0, "fk_member_id");
			$f_last_send	= mysql_result($result, 0, "f_last_send");
			$f_name			= mysql_result($result, 0, "f_name");
			$f_tel_no		= mysql_result($result, 0, "f_tel_no");
			$f_post_code	= mysql_result($result, 0, "f_post_code");
			$fk_perf_id		= mysql_result($result, 0, "fk_perf_id");
			$f_address1		= mysql_result($result, 0, "f_address1");
			$f_address2		= mysql_result($result, 0, "f_address2");
			$f_address3		= mysql_result($result, 0, "f_address3");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_status		= "";
			$fk_member_id	= "";
			$f_last_send	= "";
			$f_name			= "";
			$f_tel_no		= "";
			$f_post_code	= "";
			$fk_perf_id		= "";
			$f_address1		= "";
			$f_address2		= "";
			$f_address3		= "";
			$f_tm_stamp		= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員住所情報登録関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function RegistTAddressInfo($fk_member_id,$f_name,$f_tel_no,$f_post_code,$fk_perf_id,$f_address1,$f_address2,$f_address3)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_address
		$sql  = "insert into auction.t_address ";
		$sql .= "(fk_address_id,f_status,fk_member_id,f_name,f_tel_no,f_post_code,fk_perf_id,";
		$sql .= "f_address1,f_address2,f_address3) ";
		$sql .= "values (0,'1','$fk_member_id','$f_name','$f_tel_no','$f_post_code','$fk_perf_id',";
		$sql .= "'$f_address1','$f_address2','$f_address3') ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員住所情報更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateTAddressInfo($fk_address_id,$f_name,$f_tel_no,$f_post_code,$fk_perf_id,$f_address1,$f_address2,$f_address3)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_address
		$sql  = "update auction.t_address set f_name='$f_name',f_tel_no='$f_tel_no',";
		$sql .= "f_post_code='$f_post_code',fk_perf_id='$fk_perf_id',";
		$sql .= "f_address1='$f_address1',f_address2='$f_address2',f_address3='$f_address3' ";
		$sql .= "where fk_address_id='$fk_address_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員住所情報削除関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function DeleteTAddressInfo($fk_address_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_address
		$sql = "update auction.t_address set f_status=0 where fk_address_id='$fk_address_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員住所件数取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTAddressCount($fk_member_id,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select count(*) from auction.t_address where fk_member_id='$fk_member_id' and f_status!=0 ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$data_cnt = mysql_result($result, 0, 0);
		}
		else
		{
			$data_cnt = 0;
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		都道府県名取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTPrefName($t_pref_id, &$t_pref_name)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select t_pref_name from auction.t_pref where t_pref_id='$t_pref_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$t_pref_name = mysql_result($result, 0, "t_pref_name");
		}
		else
		{
			$t_pref_name = "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		都道府県名一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTPrefNameList(&$t_pref_id, &$t_pref_name, &$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select t_pref_id,t_pref_name from auction.t_pref order by t_pref_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$t_pref_id[$i]		= mysql_result($result, $i, "t_pref_id");
			$t_pref_name[$i]	= mysql_result($result, $i, "t_pref_name");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループ一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMGroupList($limit,$offset,&$fk_member_group_id,&$f_member_group_name,&$fk_coin_group_id,&$f_memo,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql_cnt = "select count(*) from auction.t_member_group ";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		$sql  = "select fk_member_group_id,f_member_group_name,";
		$sql .= "fk_coin_group_id,f_memo,f_tm_stamp ";
		$sql .= "from auction.t_member_group order by fk_member_group_id ";
		$sql .= "limit $limit offset $offset";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_group_id[$i]		= mysql_result($result, $i, "fk_member_group_id");
			$f_member_group_name[$i]	= mysql_result($result, $i, "f_member_group_name");
			$fk_coin_group_id[$i]		= mysql_result($result, $i, "fk_coin_group_id");
			$f_memo[$i]					= mysql_result($result, $i, "f_memo");
			$f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループ情報登録関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function RegistTMGroupInfo($f_member_group_name,$fk_coin_group_id,$f_memo)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql  = "insert into auction.t_member_group ";
		$sql .= "(fk_member_group_id,f_member_group_name,fk_coin_group_id,f_memo,f_tm_stamp) ";
		$sql .= "values (0,'$f_member_group_name','$fk_coin_group_id','$f_memo',now()) ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループ名一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetFMGroupNameList(&$fk_member_group_id, &$f_member_group_name, &$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select fk_member_group_id,f_member_group_name from auction.t_member_group order by fk_member_group_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_group_id[$i]		= mysql_result($result, $i, "fk_member_group_id");
			$f_member_group_name[$i]	= mysql_result($result, $i, "f_member_group_name");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員都道府県名一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetAreaNameList(&$fk_area_id, &$f_name, &$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select fk_area_id,f_name from auction.t_member_area where f_status<>9 order by fk_area_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_area_id[$i]		= mysql_result($result, $i, "fk_area_id");
			$f_name[$i]	= mysql_result($result, $i, "f_name");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員都道府県名一覧取得関数(検索用)														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetAreaNameList_search(&$fk_area_id, &$f_name, &$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select fk_area_id,f_name from auction.t_member_area order by fk_area_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_area_id[$i]		= mysql_result($result, $i, "fk_area_id");
			$f_name[$i]	= mysql_result($result, $i, "f_name");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業名一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetJobNameList(&$fk_job_id, &$f_name, &$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select fk_job_id,f_name from auction.t_member_job  where f_status<>9 order by fk_job_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_job_id[$i]		= mysql_result($result, $i, "fk_job_id");
			$f_name[$i]	= mysql_result($result, $i, "f_name");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業名一覧取得関数(検索用)															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetJobNameList_search(&$fk_job_id, &$f_name, &$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select fk_job_id,f_name from auction.t_member_job  order by fk_job_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_job_id[$i]		= mysql_result($result, $i, "fk_job_id");
			$f_name[$i]	= mysql_result($result, $i, "f_name");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループID取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberGroupId($fk_member_id,&$fk_member_group_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_member_group_id from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$fk_member_group_id	= mysql_result($result, 0, "fk_member_group_id");
		}
		else
		{
			$fk_member_group_id	= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員メモ取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberMemo($fk_member_id,&$f_memo)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_memo from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_memo	= mysql_result($result, 0, "f_memo");
		}
		else
		{
			$f_memo	= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員メモ更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateTMemberMemo($fk_member_id,$f_memo)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
		$sql  = "update auction.t_member_master set ";
		$sql .= "f_memo='$f_memo' where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループ情報取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberGroupInfo($fk_member_group_id, &$f_member_group_name, &$fk_coin_group_id, &$f_memo, &$f_tm_stamp)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_member_group_name,fk_coin_group_id,f_memo,f_tm_stamp ";
		$sql .= "from auction.t_member_group ";
		$sql .= "where fk_member_group_id='$fk_member_group_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_member_group_name	= mysql_result($result, 0, "f_member_group_name");
			$fk_coin_group_id		= mysql_result($result, 0, "fk_coin_group_id");
			$f_memo					= mysql_result($result, 0, "f_memo");
			$f_tm_stamp				= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_member_group_name	= "";
			$fk_coin_group_id		= "";
			$f_memo					= "";
			$f_tm_stamp				= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループ情報更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateTMGroupInfo($fk_member_group_id,$f_member_group_name,$fk_coin_group_id,$f_memo)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql  = "update auction.t_member_group set ";
		$sql .= "f_member_group_name='$f_member_group_name',";
		$sql .= "fk_coin_group_id='$fk_coin_group_id',";
		$sql .= "f_memo='$f_memo' ";
		$sql .= "where fk_member_group_id='$fk_member_group_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員グループ情報削除関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function DeleteTMGroupInfo($fk_member_group_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql = "delete from auction.t_member_group where fk_member_group_id='$fk_member_group_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ブラックメールアドレス一覧取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTBMailList($inp_mail,$inp_status,$inp_base,$limit,$offset,&$fk_black_member_mail_id,&$f_mail_address,&$f_base,&$f_status,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//メールアドレス
		if($inp_mail == "")
		{
			$where_mail = "";
		}
		else
		{
			$where_mail = "and f_mail_address='$inp_mail' ";
		}

		//登録状態
		if($inp_status == "")
		{
			$where_status = "";
		}
		else
		{
			$where_status = "and f_status='$inp_status' ";
		}

		//ブラック登録元
		if($inp_base == "")
		{
			$where_base = "";
		}
		else
		{
			$where_base = "and f_base='$inp_base' ";
		}

		$sql_cnt  = "select count(*) from auction.t_black_member_mail ";
		$sql_cnt .= "where fk_black_member_mail_id>0 ";
		$sql_cnt .= $where_mail;
		$sql_cnt .= $where_status;
		$sql_cnt .= $where_base;
		$sql_cnt .= "and ((f_status=0) or (f_status=1) or (f_status=9)) ";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		$sql  = "select fk_black_member_mail_id,f_mail_address,";
		$sql .= "f_base,f_status,f_tm_stamp ";
		$sql .= "from auction.t_black_member_mail ";
		$sql .= "where fk_black_member_mail_id>0 ";
		$sql .= $where_mail;
		$sql .= $where_status;
		$sql .= $where_base;
		$sql .= "and ((f_status=0) or (f_status=1) or (f_status=9)) ";
		$sql .= "order by fk_black_member_mail_id ";
		$sql .= "limit $limit offset $offset";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_black_member_mail_id[$i]	= mysql_result($result, $i, "fk_black_member_mail_id");
			$f_mail_address[$i]				= mysql_result($result, $i, "f_mail_address");
			$f_base[$i]						= mysql_result($result, $i, "f_base");
			$f_status[$i]					= mysql_result($result, $i, "f_status");
			$f_tm_stamp[$i]					= mysql_result($result, $i, "f_tm_stamp");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ブラックメールアドレス情報取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTBMailInfo($fk_black_member_mail_id,&$f_mail_address,&$f_base,&$f_status,&$f_tm_stamp)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_mail_address,f_base,f_status,f_tm_stamp ";
		$sql .= "from auction.t_black_member_mail ";
		$sql .= "where fk_black_member_mail_id='$fk_black_member_mail_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_mail_address	= mysql_result($result, 0, "f_mail_address");
			$f_base			= mysql_result($result, 0, "f_base");
			$f_status		= mysql_result($result, 0, "f_status");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_mail_address	= "";
			$f_base			= "";
			$f_status		= "";
			$f_tm_stamp		= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ブラックメールアドレス登録関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function RegistTBMail($f_mail_address,$f_base,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql  = "insert into auction.t_black_member_mail ";
		$sql .= "(fk_black_member_mail_id,f_mail_address,f_base,f_status,f_tm_stamp) ";
		$sql .= "values (0,'$f_mail_address','$f_base','$f_status',now()) ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ブラックメールアドレス情報更新関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateTBMailInfo($fk_black_member_mail_id,$f_mail_address,$f_base,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql  = "update auction.t_black_member_mail set ";
		$sql .= "f_mail_address='$f_mail_address',";
		$sql .= "f_base='$f_base',";
		$sql .= "f_status='$f_status' ";
		$sql .= "where fk_black_member_mail_id='$fk_black_member_mail_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ブラックメールアドレス削除関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function DeleteTBMail($fk_black_member_mail_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_black_member_mail
		$sql = "delete from auction.t_black_member_mail where fk_black_member_mail_id='$fk_black_member_mail_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		携帯識別番号一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTBSerList($inp_ser,$inp_status,$inp_base,$limit,$offset,&$fk_black_member_ser_id,&$f_ser,&$f_base,&$f_status,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//メールアドレス
		if($inp_ser == "")
		{
			$where_ser = "";
		}
		else
		{
			$where_ser = "and f_ser='$inp_ser' ";
		}

		//登録状態
		if($inp_status == "")
		{
			$where_status = "";
		}
		else
		{
			$where_status = "and f_status='$inp_status' ";
		}

		//ブラック登録元
		if($inp_base == "")
		{
			$where_base = "";
		}
		else
		{
			$where_base = "and f_base='$inp_base' ";
		}

		$sql_cnt = "select count(*) from auction.t_black_member_ser ";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		$sql  = "select fk_black_member_ser_id,f_ser,";
		$sql .= "f_base,f_status,f_tm_stamp ";
		$sql .= "from auction.t_black_member_ser ";
		$sql .= "where fk_black_member_ser_id>0 ";
		$sql .= $where_ser;
		$sql .= $where_status;
		$sql .= $where_base;
		$sql .= "order by fk_black_member_ser_id ";
		$sql .= "limit $limit offset $offset";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_black_member_ser_id[$i]	= mysql_result($result, $i, "fk_black_member_ser_id");
			$f_ser[$i]					= mysql_result($result, $i, "f_ser");
			$f_base[$i]					= mysql_result($result, $i, "f_base");
			$f_status[$i]				= mysql_result($result, $i, "f_status");
			$f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		携帯識別番号情報取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTBSerInfo($fk_black_member_ser_id,&$f_ser,&$f_base,&$f_status,&$f_tm_stamp)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_ser,f_base,f_status,f_tm_stamp ";
		$sql .= "from auction.t_black_member_ser ";
		$sql .= "where fk_black_member_ser_id='$fk_black_member_ser_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_ser		= mysql_result($result, 0, "f_ser");
			$f_base		= mysql_result($result, 0, "f_base");
			$f_status	= mysql_result($result, 0, "f_status");
			$f_tm_stamp	= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_ser		= "";
			$f_base		= "";
			$f_status	= "";
			$f_tm_stamp	= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		携帯識別番号登録関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function RegistTBSer($f_ser,$f_base,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql  = "insert into auction.t_black_member_ser ";
		$sql .= "(fk_black_member_ser_id,f_ser,f_base,f_status,f_tm_stamp) ";
		$sql .= "values (0,'$f_ser','$f_base','$f_status',now()) ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		携帯識別番号情報更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateTBSerInfo($fk_black_member_ser_id,$f_ser,$f_base,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_group
		$sql  = "update auction.t_black_member_ser set ";
		$sql .= "f_ser='$f_ser',";
		$sql .= "f_base='$f_base',";
		$sql .= "f_status='$f_status' ";
		$sql .= "where fk_black_member_ser_id='$fk_black_member_ser_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		携帯識別番号削除関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function DeleteTBSer($fk_black_member_ser_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_black_member_ser
		$sql = "delete from auction.t_black_member_ser where fk_black_member_ser_id='$fk_black_member_ser_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ロボット会員名検索修正関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function ResetRobotHandle(&$up_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//一般会員のハンドル名取得
		$sql  = "select fk_member_id,f_handle ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where f_activity=1 ";
		$sql .= "order by fk_member_id ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		$up_cnt = 0;
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_id[$i]	= mysql_result($result, $i, "fk_member_id");
			$f_handle[$i]		= mysql_result($result, $i, "f_handle");

			//ロボット会員名とチェック
			$sql2  = "select fk_member_id,f_handle ";
			$sql2 .= "from auction.t_member_master ";
			$sql2 .= "where f_activity=99 and f_handle='$f_handle[$i]' ";
			$res2 = mysql_query($sql2, $db);
			if( $res2 == false )
			{
				print "$sql2<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($res2);
			for($j=0; $j<$rows; $j++)
			{
				//ロボット名を変更(→ハンドル名+ID)
				$robot_id		= mysql_result($res2, $j, "fk_member_id");
				$robot_handle	= mysql_result($res2, $j, "f_handle");
print "f_handle = [".$f_handle[$i]."]<br>\n";
print "robot_id = ".$robot_id."<br>\n";
				$new_handle = $robot_handle."_".$robot_id;

				$sql3 = "update auction.t_member_master set f_handle='$new_handle' where fk_member_id='$robot_id' ";
					print "$sql3<BR>\n";
				//■ＳＱＬ実行
				$res3 = mysql_query($sql3, $db);
				if( $res3 == false )
				{
					print "$sql3<BR>\n";
					//■ロールバック
					mysql_query("ROLLBACK", $db);
					//■ＤＢ切断
				    db_close($db);
					return false;
				}
				$up_cnt++;
			}
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ロボット会員名重複修正関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function ResetRobotRepetitionHandle(&$up_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//ロボット会員のハンドル名取得
		$sql  = "select fk_member_id,f_handle ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where f_activity=99 ";
		$sql .= "order by fk_member_id ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		$up_cnt = 0;
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_id[$i]	= mysql_result($result, $i, "fk_member_id");
			$f_handle[$i]		= mysql_result($result, $i, "f_handle");

			//ロボット会員名とチェック
			$sql2  = "select fk_member_id,f_handle ";
			$sql2 .= "from auction.t_member_master ";
			$sql2 .= "where f_activity=99 and f_handle='$f_handle[$i]' ";
			$sql2 .= "and fk_member_id!='$fk_member_id[$i]' ";
			$res2 = mysql_query($sql2, $db);
			if( $res2 == false )
			{
				print "$sql2<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
			    db_close($db);
				return false;
			}
			//■データ件数
			$rows = mysql_num_rows($res2);
			for($j=0; $j<$rows; $j++)
			{
				//ロボット名を変更(→ハンドル名+ID)
				$robot_id		= mysql_result($res2, $j, "fk_member_id");
				$robot_handle	= mysql_result($res2, $j, "f_handle");
if($j == 0)
{
print "fk_member_id = [".$fk_member_id[$i]."]<br>\n";
print "f_handle = [".$f_handle[$i]."]<br>\n";
}
print "robot_id = ".$robot_id."<br>\n";
				$new_handle = $robot_handle."_".$robot_id;

				$sql3 = "update auction.t_member_master set f_handle='$new_handle' where fk_member_id='$robot_id' ";
					print "$sql3<BR>\n";
				//■ＳＱＬ実行
				$res3 = mysql_query($sql3, $db);
				if( $res3 == false )
				{
					print "$sql3<BR>\n";
					//■ロールバック
					mysql_query("ROLLBACK", $db);
					//■ＤＢ切断
				    db_close($db);
					return false;
				}
				$up_cnt++;
			}
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員入金処理関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function PaymentTMemberCoin($fk_member_id,$f_coin,$f_free_coin,$f_total_pay,$f_over_money,$f_status,$f_staff_id,$f_pay_money,$f_coin_add,$fk_shiharai_type_id,$f_coin_result,$fk_adcode_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//初期化
		if($f_coin == "")		$f_coin = 0;
		if($f_free_coin == "")	$f_free_coin = 0;
		if($f_total_pay == "")	$f_total_pay = 0;
		if($f_over_money == "")	$f_over_money = 0;

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//▼会員のコイン情報に加算
		//t_member
//G.Chin AWKC-218 2010-11-12 chg sta
/*
		$sql  = "update auction.t_member set f_coin=f_coin+$f_coin,";
		$sql .= "f_free_coin=f_free_coin+$f_free_coin,";
		$sql .= "f_total_pay=f_total_pay+$f_total_pay ";
		$sql .= "where fk_member_id='$fk_member_id' ";
*/
		$sql  = "update auction.t_member set f_coin=f_coin+$f_coin,";
		$sql .= "f_free_coin=f_free_coin+$f_free_coin,";
		$sql .= "f_total_pay=f_total_pay+$f_total_pay,";
		$sql .= "f_total_pay_cnt=f_total_pay_cnt+1 ";
		$sql .= "where fk_member_id='$fk_member_id' ";
//G.Chin AWKC-218 2010-11-12 chg end

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//t_member_master
		$sql  = "update auction.t_member_master set f_over_money=f_over_money+$f_over_money ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}


		//▼支払履歴を記録
		//t_pay_log
		$sql  = "insert into auction.t_pay_log ";
		$sql .= "(fk_pay_log_id,f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,f_tm_stamp,fk_adcode_id,f_free_coin_add) ";
		$sql .= "values (0,'$f_status','$fk_member_id','$f_staff_id','$f_pay_money','$f_coin_add','$fk_shiharai_type_id','$f_coin_result',now(),'$fk_adcode_id','$f_free_coin') ";
		//echo $sql;
		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員広告コード設定関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function SetMemberFkAdcodeId($fk_member_id,$fk_adcode_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
		$sql  = "update auction.t_member_master set fk_adcode_id='$fk_adcode_id' ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員状態取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberActivity($fk_member_id,&$f_activity)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select f_activity from auction.t_member_master where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_activity	= mysql_result($result, 0, "f_activity");
		}
		else
		{
			$f_activity	= "";
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員メールアドレス取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberMailAddress($fk_member_id,&$f_mail_address_pc,&$f_mail_address_mb)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_mail_address_pc,f_mail_address_mb ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_mail_address_pc	= mysql_result($result, 0, "f_mail_address_pc");
			$f_mail_address_mb	= mysql_result($result, 0, "f_mail_address_mb");
		}
		else
		{
			$f_mail_address_pc	= "";
			$f_mail_address_mb	= "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

//G.Chin 2010-07-30 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員アフィリエイトキー取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberAflkey($fk_member_id,&$f_aflkey)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_aflkey ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_aflkey = mysql_result($result, 0, "f_aflkey");
		}
		else
		{
			$f_aflkey = "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-07-30 add end

/*======================================================================================================
* 広告コード削除用処理
*======================================================================================================*/
        function DelParAd($mem_id,$sex)
        {
            $sql = "SELECT ad.fk_admaster_id,f_regist_pos,f_regist_date FROM auction.t_member_master mm,auction.t_adcode ad WHERE fk_member_id = $mem_id AND mm.fk_parent_ad = ad.fk_adcode_id";
            $db=db_connect();
		if($db == false)
		{
	        return 1;
		}
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false )
            {
                    print "$sql<BR>\n";
                    //■ＤＢ切断
                db_close($db);
                    return 1;
            }
            //■データ件数
            $rows = mysql_num_rows($result);

            if($rows == 0)
            {
                return 1;
            }
            $ret = mysql_fetch_array($result);
            $fk_admaster_id =$ret["fk_admaster_id"];
            $f_regist_pos=$ret["f_regist_pos"];
            $f_regist_date=$ret["f_regist_date"];

            if( $fk_admaster_id == NULL)
            {
                db_close($db);
                return 2;
            }
            mysql_query("BEGIN", $db);

            $sql="update auction.t_member_master set fk_parent_ad=NULL WHERE fk_member_id = '$mem_id'";
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false )
            {
                    print "$sql<BR>\n";
                    mysql_query("ROLLBACK", $db);
                    //■ＤＢ切断
                    db_close($db);
                    return 1;
            }

            if($f_regist_pos == 0)
            {
                if($sex==0)
                {
                    $sql="update auction.t_stat_ad set f_pc_memreg=f_pc_memreg-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=0 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d %H:00:00')";
                    $sql2="update auction.t_stat_ad set f_pc_memreg=f_pc_memreg-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=1 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d 23:00:00')";
                }
                else
                {
                    $sql="update auction.t_stat_ad set f_pc_memreg_woman=f_pc_memreg_woman-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=0 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d %H:00:00')";
                    $sql2="update auction.t_stat_ad set f_pc_memreg_woman=f_pc_memreg_woman-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=1 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d 23:00:00')";
                }
            }
            else if($f_regist_pos == 1)
            {
                if($sex==0)
                {
                    $sql="update auction.t_stat_ad set f_mb_memreg=f_mb_memreg-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=0 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d %H:00:00')";
                    $sql2="update auction.t_stat_ad set f_mb_memreg=f_mb_memreg-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=1 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d 23:00:00')";
                }
                else
                {
                    $sql="update auction.t_stat_ad set f_mb_memreg_woman=f_mb_memreg_woman-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=0 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d %H:00:00')";
                    $sql2="update auction.t_stat_ad set f_mb_memreg_woman=f_mb_memreg_woman-1 WHERE fk_admaster_id = $fk_admaster_id and f_type=1 and f_stat_dt = DATE_FORMAT('$f_regist_date','%Y-%m-%d 23:00:00')";

                }
            }
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false )
            {
                    print "$sql<BR>\n";
                    mysql_query("ROLLBACK", $db);
                    //■ＤＢ切断
                    db_close($db);
                    return 1;
            }
            $result = mysql_query($sql2, $db);
            if( $result == false )
            {
                    print "$sql2<BR>\n";
                    mysql_query("ROLLBACK", $db);
                    //■ＤＢ切断
                    db_close($db);
                    return 1;
            }
            mysql_query("COMMIT", $db);
            return 0;
        }

/*======================================================================================================
* 会員削除権限取得用
*======================================================================================================*/
        function member_delete_authority($staff_id){
            $sql ="select * from auction.t_staff_authority sa where fk_staff_id= $staff_id and fk_authority_id=26";
            $db=db_connect();
             if($db == false)
		{
	        return false;
		}
            //■ＳＱＬ実行
            $result = mysql_query($sql, $db);
            if( $result == false )
            {
                    print "$sql<BR>\n";
                    //■ＤＢ切断
                db_close($db);
                    return false;
            }
            //■データ件数
            $rows = mysql_num_rows($result);

            if($rows == 0)
            {
                return false;
            }
            db_close($db);
            return $rows;
        }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員数取得関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberCount($inp_minutes, &$login_cnt, &$all_count)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//全登録数
		$sql_cnt  = "select count(*) from auction.t_member_master ";
		$sql_cnt .= "where f_activity=1 and f_delete_flag=0 ";
		//print "$sql_cnt<BR>\n";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

//G.Chin 2010-12-10 add sta
		if($inp_minutes == "")
		{
			$inp_minutes = 1;
		}
//G.Chin 2010-12-10 add end

		//検索時刻
//		$before_minutes = $inp_minutes * 60;
//		$now = time();
//		$src_minutes = $now - $before_minutes;
//		$src_time = date("Y-m-d H:i:s", $src_minutes);
		//ログイン数
//		$sql_cnt  = "select count(*) from auction.t_member_master as tmm,auction.t_member as tm ";
//		$sql_cnt .= "where tmm.f_activity=1 and tmm.f_delete_flag=0 ";
//		$sql_cnt .= "and tm.f_last_entry>='$src_time' ";
//		$sql_cnt .= "and tmm.fk_member_id=tm.fk_member_id ";

		$sql  = "SELECT COUNT(tmm.fk_member_id) FROM auction.t_member_master AS tmm,";
		$sql .= "(SELECT fk_member_id FROM auction.t_member WHERE f_last_entry>=date_sub(now(),interval $inp_minutes MINUTE )) AS tm ";
		$sql .= "WHERE tmm.f_activity=1 ";
		$sql .= "AND tmm.f_delete_flag=0 ";
		$sql .= "AND tmm.fk_member_id=tm.fk_member_id";
		//print "$sql<BR>\n";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■ログイン件数
		$login_cnt = mysql_result($result, 0, 0);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		最後ログインメンバー一覧取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

//G.Chin AWKT-683 2010-11-17 chg sta
/*
	function GetMemLastLoginList(&$fk_member_id,&$f_handle,&$f_regist_date,$mem_cnt,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_member_id,f_handle,f_regist_date ";
		$sql .= "from auction.t_member_master where f_activity=1 and f_delete_flag=0  ";
		$sql .= "order by f_regist_date desc limit $mem_cnt";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_id[$i]	= mysql_result($result, $i, "fk_member_id");
			$f_handle[$i]		= mysql_result($result, $i, "f_handle");
			$f_regist_date[$i]	= mysql_result($result, $i, "f_regist_date");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}
*/
	function GetMemLastLoginList(&$fk_member_id,&$f_handle,&$f_last_entry,$mem_cnt,&$data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

//		$sql  = "select tmm.fk_member_id,tmm.f_handle,tm.f_last_entry ";
//		$sql .= "from auction.t_member_master as tmm,auction.t_member as tm ";
//		$sql .= "where tmm.f_activity=1 and tmm.f_delete_flag=0 ";
//		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";
//		$sql .= "order by tm.f_last_entry desc limit $mem_cnt";

                $sql ="SELECT tm.f_last_entry  ,tmm.fk_member_id,tmm.f_handle FROM  (SELECT f_last_entry,fk_member_id FROM auction.t_member WHERE f_last_entry > DATE_SUB(NOW() ,INTERVAL 1 DAY) ORDER BY f_last_entry DESC LIMIT 10) tm
                LEFT OUTER JOIN
                (SELECT fk_member_id ,f_handle FROM auction.t_member_master  WHERE f_activity = 1 AND f_delete_flag = 0 ) tmm
                ON tm.fk_member_id = tmm.fk_member_id";
                //echo $sql;
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_member_id[$i]	= mysql_result($result, $i, "fk_member_id");
			$f_handle[$i]		= mysql_result($result, $i, "f_handle");
			$f_last_entry[$i]	= mysql_result($result, $i, "f_last_entry");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}
//G.Chin AWKT-683 2010-11-17 chg end

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員CSV作成関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

//G.Chin AWKC-218 2010-11-12 chg sta
/*
	function MakeMemberCSV($dist_path,$inp_activity,$inp_id_name,$inp_parent_ad,$inp_group,$inp_mail,$inp_s_regist,$inp_e_regist,$inp_regpos,$inp_s_coin,$inp_e_coin,$inp_s_free,$inp_e_free,$inp_s_totalpay,$inp_e_totalpay,$inp_sex,$inp_mem_id,$inp_name,$inp_unpain,$inp_cancel,$inp_s_last_entry,$inp_e_last_entry,$sort,&$file_name,&$all_count,$inp_area,$inp_job,$inp_ip)
	{
		//echo $sort;
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//状態
		if($inp_activity == "")
		{
			$where_activity = "and tmm.f_activity<5 ";
		}
		else
		{
			$where_activity = "and tmm.f_activity<5 and tmm.f_activity='$inp_activity' ";
		}

		//ログインID・ハンドルネーム
		if($inp_id_name == "")
		{
			$where_id_name = "";
		}
		else
		{
			$where_id_name = "and ((tmm.f_login_id like '%$inp_id_name%') or (tmm.f_handle like '%$inp_id_name%')) ";
		}

		//広告/紹介コード
		if($inp_parent_ad == "")
		{
			$where_parent_ad = "";
		}
		else
		{
			$where_parent_ad = "and tmm.fk_parent_ad='$inp_parent_ad' ";
		}

		//会員グループ
		if($inp_group == "")
		{
			$where_group = "";
		}
		else
		{
			$where_group = "and tmm.fk_member_group_id='$inp_group' ";
		}

		//メールアドレス
		if($inp_mail == "")
		{
			$where_mail = "";
		}
		else
		{
			$where_mail = "and ((tmm.f_mail_address_pc like '%$inp_mail%') or (tmm.f_mail_address_mb like '%$inp_mail%')) ";
		}

		//登録日(開始)
		if($inp_s_regist == "")
		{
			$where_s_regist = "";
		}
		else
		{
			$where_s_regist = "and tmm.f_regist_date>='$inp_s_regist 00:00:00' ";
		}

		//登録日(終了)
		if($inp_e_regist == "")
		{
			$where_e_regist = "";
		}
		else
		{
			$where_e_regist = "and tmm.f_regist_date<='$inp_e_regist 23:59:59' ";
		}

		//登録媒体
		if($inp_regpos == "")
		{
			$where_regpos = "";
		}
		else
		{
			$where_regpos = "and tmm.f_regist_pos='$inp_regpos' ";
		}

		//残り購入コイン数(開始)
		if($inp_s_coin == "")
		{
			$where_s_coin = "";
		}
		else
		{
			$where_s_coin = "and tm.f_coin>='$inp_s_coin' ";
		}

		//残り購入コイン数(終了)
		if($inp_e_coin == "")
		{
			$where_e_coin = "";
		}
		else
		{
			$where_e_coin = "and tm.f_coin<='$inp_e_coin' ";
		}

		//残りサービスコイン数(開始)
		if($inp_s_free == "")
		{
			$where_s_free = "";
		}
		else
		{
			$where_s_free = "and tm.f_free_coin>='$inp_s_free' ";
		}

		//残りサービスコイン数(終了)
		if($inp_e_free == "")
		{
			$where_e_free = "";
		}
		else
		{
			$where_e_free = "and tm.f_free_coin<='$inp_e_free' ";
		}

		//合計コイン購入金額(開始)
		if($inp_s_totalpay == "")
		{
			$where_s_totalpay = "";
		}
		else
		{
			$where_s_totalpay = "and tm.f_total_pay>='$inp_s_totalpay' ";
		}

		//合計コイン購入金額(終了)
		if($inp_e_totalpay == "")
		{
			$where_e_totalpay = "";
		}
		else
		{
			$where_e_totalpay = "and tm.f_total_pay<='$inp_e_totalpay' ";
		}

		//性別
		if($inp_sex == "")
		{
			$where_sex = "";
		}
		else
		{
			$where_sex = "and tmm.f_sex='$inp_sex' ";
		}

		//会員ID
		if($inp_mem_id == "")
		{
			$where_mid = "";
		}
		else
		{
			$where_mid = " and tmm.fk_member_id=$inp_mem_id ";
		}

		//氏名
		if($inp_name == "")
		{
			$where_name = "";
		}
		else
		{
			$where_name =" and (tmm.f_name like '%$inp_name%') ";
		}

		//地域
		if($inp_area == "" or $inp_area==0)
		{
			$where_area = "";
		}
		else
		{
			$where_area =" and (tmm.fk_area_id=$inp_area) ";
		}

		//職業
		if($inp_job == "" or $inp_job==0)
		{
			$where_job = "";
		}
		else
		{
			$where_job =" and (tmm.fk_job_id=$inp_job) ";
		}

		//IPアドレス
		if($inp_ip == "")
		{
			$where_ip = "";
		}
		else
		{
			$where_ip =" and (tmm.f_ip='$inp_ip') ";
		}

		//未払い商品数
		if($inp_unpain == "")
		{
			$where_unpain = "";
		}
		else
		{
			$where_unpain =" and (tm.f_unpain_num=$inp_unpain) ";
		}

		//キャンセル商品数
		if($inp_cancel == "")
		{
			$where_cancel = "";
		}
		else
		{
			$where_cancel =" and (tm.f_cancel_num=$inp_cancel) ";
		}

		//最終ログイン日(開始)
		if($inp_s_last_entry == "")
		{
			$where_s_last_entry = "";
		}
		else
		{
			$where_s_last_entry =" and tm.f_last_entry>='$inp_s_last_entry 00:00:00' ";
		}

		//最終ログイン日(終了)
		if($inp_e_last_entry == "")
		{
			$where_e_last_entry = "";
		}
		else
		{
			$where_e_last_entry =" and tm.f_last_entry<='$inp_e_last_entry 23:59:59' ";
		}

		$order = "";
		//echo  $sort."<BR>\n";
		switch ($sort)
		{
			case 0:
				$order = "tm.fk_member_id ";
				break;
			case 1:
				$order = "tm.fk_member_id desc";
				break;
			case 2:
				$order = "tm.f_total_pay ";
				break;
			case 3:
				$order = "tm.f_total_pay desc ";
				break;
			case 4:
				$order = "tm.f_last_entry ";
				break;
			case 5:
				$order = "tm.f_last_entry desc ";
				break;
			case 6:
				$order = "p1.pay_total ";
//				$order = "p2.pay_total ";
				break;
			case 7:
				$order = "p1.pay_total desc ";
//				$order = "p2.pay_total desc ";
				break;
			case 10:
				$order = "p1.last_time ";
				break;
			case 11:
				$order = "p1.last_time desc ";
				break;
			case 12:
				$order = "p3.pay_record ";
				break;
			case 13:
				$order = "p3.pay_record desc ";
				break;
			case 14:
				$order = "tm.f_unpain_num ";
				break;
			case 15:
				$order = "tm.f_unpain_num desc ";
				break;
			case 16:
				$order = "tm.f_cancel_num ";
				break;
			case 17:
				$order = "tm.f_cancel_num desc ";
				break;
		}

		$sql_cnt  = "select count(tmm.fk_member_id) from auction.t_member_master as tmm, auction.t_member as tm ";
//		$sql_cnt  = "select count(*) from auction.t_member_master as tmm, auction.t_member as tm ";
		$sql_cnt .= "where tmm.fk_member_id>0 ";
		$sql_cnt .= $where_activity;
		$sql_cnt .= $where_id_name;
		$sql_cnt .= $where_parent_ad;
		$sql_cnt .= $where_group;
		$sql_cnt .= $where_mail;
		$sql_cnt .= $where_s_regist;
		$sql_cnt .= $where_e_regist;
		$sql_cnt .= $where_regpos;
		$sql_cnt .= $where_s_coin;
		$sql_cnt .= $where_e_coin;
		$sql_cnt .= $where_s_free;
		$sql_cnt .= $where_e_free;
		$sql_cnt .= $where_s_totalpay;
		$sql_cnt .= $where_e_totalpay;
		$sql_cnt .= $where_sex;
		$sql_cnt .= $where_mid;
		$sql_cnt .= $where_name;
		$sql_cnt .= $where_area;
		$sql_cnt .= $where_job;
		$sql_cnt .= $where_ip;
		$sql_cnt .= $where_unpain;
		$sql_cnt .= $where_cancel;
		$sql_cnt .= $where_s_last_entry;
		$sql_cnt .= $where_e_last_entry;
		$sql_cnt .= "and tmm.fk_member_id=tm.fk_member_id ";
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		$sql  = "select tmm.fk_member_id,tmm.f_login_id,tmm.f_handle,tmm.f_activity,tmm.f_mail_address_pc,tmm.f_mail_address_mb,";
		$sql .= "tmm.f_tel_no,tmm.f_regist_date,tmm.f_user_agent,tmm.f_ser_no,tmm.f_sex,tmm.fk_parent_ad,";
		$sql .= "tmm.f_birthday,";																			//G.Chin AREQ-332 2010-11-08 add
		$sql .= "tm.f_coin,tm.f_free_coin,tm.f_last_entry,tm.f_total_pay,tm.f_unpain_num,tm.f_cancel_num,";
		$sql .= "p1.last_time,p1.pay_total,p3.pay_record ";
		$sql .= "FROM auction.t_member_master AS tmm
						INNER JOIN auction.t_member tm ON tm.fk_member_id=tmm.fk_member_id
						LEFT OUTER JOIN
						(SELECT MAX(f_tm_stamp)AS last_time,SUM(f_pay_money)AS pay_total,f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0 AND f_cert_status=0  GROUP BY f_member_id) p1
						ON p1.f_member_id=tm.fk_member_id
						LEFT OUTER JOIN
						(SELECT COUNT(f_member_id) AS pay_record,f_member_id FROM(SELECT f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0  AND f_cert_status=0 AND f_status=0 GROUP BY f_member_id,fk_products_id) p3 GROUP BY f_member_id) p3
						ON p3.f_member_id=tm.fk_member_id ";
//		$sql .= "FROM auction.t_member_master AS tmm
//						INNER JOIN auction.t_member tm ON tm.fk_member_id=tmm.fk_member_id
//						LEFT OUTER JOIN
//						(SELECT MAX(f_tm_stamp)AS last_time,f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0 AND f_cert_status=0  GROUP BY f_member_id) p1
//						ON p1.f_member_id=tm.fk_member_id
//						LEFT OUTER JOIN
//						(SELECT SUM(f_pay_money)AS pay_total,f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0 AND f_cert_status=0 GROUP BY f_member_id) p2
//						ON p2.f_member_id=tm.fk_member_id
//						LEFT OUTER JOIN
//						(SELECT COUNT(f_member_id) AS pay_record,f_member_id FROM(SELECT f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0  AND f_cert_status=0 AND f_status=0 GROUP BY f_member_id,fk_products_id) p3 GROUP BY f_member_id) p3
//						ON p3.f_member_id=tm.fk_member_id ";
		$sql .= "where tmm.fk_member_id>0 ";
		$sql .= $where_activity;
		$sql .= $where_id_name;
		$sql .= $where_parent_ad;
		$sql .= $where_group;
		$sql .= $where_mail;
		$sql .= $where_s_regist;
		$sql .= $where_e_regist;
		$sql .= $where_regpos;
		$sql .= $where_s_coin;
		$sql .= $where_e_coin;
		$sql .= $where_s_free;
		$sql .= $where_e_free;
		$sql .= $where_s_totalpay;
		$sql .= $where_e_totalpay;
		$sql .= $where_sex;
		$sql .= $where_mid;
		$sql .= $where_name;
		$sql .= $where_area;
		$sql .= $where_job;
		$sql .= $where_ip;
		$sql .= $where_unpain;
		$sql .= $where_cancel;
		$sql .= $where_s_last_entry;
		$sql .= $where_e_last_entry;
		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";
		$sql .= "order by $order ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		echo mysql_error($db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		//■ＤＢ切断
	    db_close($db);

		//■出力フォルダ内の全ファイル削除
		$FileDir = $dist_path;
		$FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
		for($i=0; $FileList[$i]!=""; $i++)
		{
			unlink($FileList[$i]);
		}

		if($all_count <= MNTSTAFF_MEMCNT)
		{
			$file_name = "mntstaff.csv";

			//■CSVファイル・オープン
			$file_path = sprintf("%s%s", $dist_path, $file_name);
			$myfile = fopen($file_path, "w");

//G.Chin AWKT-510 2010-10-06 chg sta
//			$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,登録日,最終ログイン日,コイン購入金額（合計）,コイン購入回数（合計）,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,連絡先住所";
//G.Chin AREQ-332 2010-11-08 chg sta
//			$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,登録日,最終ログイン日,コイン購入金額（合計）,コイン購入回数（合計）,未払い商品数,キャンセル商品数,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,配送先住所(1件目)";
			$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,誕生日,登録日,最終ログイン日,コイン購入金額（合計）,未払い商品数,キャンセル商品数,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,配送先住所(1件目)";
//G.Chin AREQ-332 2010-11-08 chg end
//G.Chin AWKT-510 2010-10-06 chg end
			//文字コード変換
			$top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
			fputs($myfile, "$top_str\n");

			for($i=0; $i<$data_cnt; $i++)
			{
				$fk_member_id[$i]		= mysql_result($result, $i, "fk_member_id");
				$f_login_id[$i]			= mysql_result($result, $i, "f_login_id");
				$f_handle[$i]			= mysql_result($result, $i, "f_handle");
				$f_activity[$i]			= mysql_result($result, $i, "f_activity");
				$f_mail_address_pc[$i]	= mysql_result($result, $i, "f_mail_address_pc");
				$f_mail_address_mb[$i]	= mysql_result($result, $i, "f_mail_address_mb");
				$f_tel_no[$i]			= mysql_result($result, $i, "f_tel_no");
				$f_regist_date[$i]		= mysql_result($result, $i, "f_regist_date");
				$f_user_agent[$i]		= mysql_result($result, $i, "f_user_agent");
				$f_ser_no[$i]			= mysql_result($result, $i, "f_ser_no");
				$f_sex[$i]				= mysql_result($result, $i, "f_sex");
				$fk_parent_ad[$i]		= mysql_result($result, $i, "fk_parent_ad");
				$f_birthday[$i]			= mysql_result($result, $i, "f_birthday");	//G.Chin AREQ-332 2010-11-08 add
				$f_coin[$i]				= mysql_result($result, $i, "f_coin");
				$f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
				$f_unpain_num[$i]		= mysql_result($result, $i, "f_unpain_num");
				$f_cancel_num[$i]		= mysql_result($result, $i, "f_cancel_num");
				$f_last_entry[$i]		= mysql_result($result, $i, "f_last_entry");
				$f_total_pay[$i]		= mysql_result($result, $i, "f_total_pay");
				$f_tm_stamp[$i]			= mysql_result($result, $i, "last_time");
				$f_pay_money[$i]		= mysql_result($result, $i, "pay_total");
				$pay_record[$i]			= mysql_result($result, $i, "pay_record");

				//状態
				switch($f_activity[$i])
				{
					case 0:		$status_str = "仮登録";	break;
					case 1:		$status_str = "会員";	break;
					case 2:		$status_str = "退会者";	break;
					default:	$status_str = "";		break;
				}

				//性別判定
				if($f_sex[$i] == 0)	$sex_str = "男性";
				else				$sex_str = "女性";

				//メールアドレス判定
				if($f_mail_address_pc[$i] != "")	$mail_address = $f_mail_address_pc[$i];
				else								$mail_address = $f_mail_address_mb[$i];

				//広告コード
				GetTAdcodeInfo($fk_parent_ad[$i],$ta_f_adcode,$ta_fk_admaster_id,$ta_f_type,$ta_f_tm_stamp);
				if($ta_f_adcode == "0")
				{
					$adcode = "NOP";
				}
				else
				{
					$adcode = $ta_f_adcode;
				}

				//会員住所一覧取得関数
				GetTAddressList($fk_member_id[$i],$tad_fk_address_id,$tad_f_status,$tad_f_last_send,$tad_f_name,$tad_f_tel_no,$tad_f_post_code,$tad_fk_perf_id,$tad_f_address1,$tad_f_address2,$tad_f_address3,$tad_f_tm_stamp,$tad_data_cnt);
				//住所
				if($tad_data_cnt > 0)
				{
					$post_before = substr($tad_f_post_code[0], 0, 3);
					$post_after  = substr($tad_f_post_code[0], 3, 4);
					$address_str = "〒".$post_before."-".$post_after." ".$tad_f_address1[0].$tad_f_address2[0]." ".$tad_f_address3[0];
				}
				else
				{
					$address_str = "";
				}

				//文字コード変換
				$fk_member_id[$i]	= mb_convert_encoding($fk_member_id[$i], "SJIS", "UTF-8");
				$f_login_id[$i]		= mb_convert_encoding($f_login_id[$i], "SJIS", "UTF-8");
				$f_handle[$i]		= mb_convert_encoding($f_handle[$i], "SJIS", "UTF-8");
				$status_str			= mb_convert_encoding($status_str, "SJIS", "UTF-8");
				$mail_address		= mb_convert_encoding($mail_address, "SJIS", "UTF-8");
				$f_tel_no[$i]		= mb_convert_encoding($f_tel_no[$i], "SJIS", "UTF-8");
				$f_regist_date[$i]	= mb_convert_encoding($f_regist_date[$i], "SJIS", "UTF-8");
				$f_last_entry[$i]	= mb_convert_encoding($f_last_entry[$i], "SJIS", "UTF-8");
				$f_pay_money[$i]	= mb_convert_encoding($f_pay_money[$i], "SJIS", "UTF-8");
				$pay_record[$i]		= mb_convert_encoding($pay_record[$i], "SJIS", "UTF-8");
				$f_unpain_num[$i]	= mb_convert_encoding($f_unpain_num[$i], "SJIS", "UTF-8");
				$f_cancel_num[$i]	= mb_convert_encoding($f_cancel_num[$i], "SJIS", "UTF-8");
				$f_user_agent[$i]	= mb_convert_encoding($f_user_agent[$i], "SJIS", "UTF-8");
				$f_ser_no[$i]		= mb_convert_encoding($f_ser_no[$i], "SJIS", "UTF-8");
				$sex_str			= mb_convert_encoding($sex_str, "SJIS", "UTF-8");
				$adcode				= mb_convert_encoding($adcode, "SJIS", "UTF-8");
				$address_str		= mb_convert_encoding($address_str, "SJIS", "UTF-8");
*/

//G.Chin AWKT-522 2010-10-14 add sta
//G.Chin AREQ-332 2010-11-08 del sta
/*
				$where_str  = "";
				$where_str .= "and f_status=0 ";
				$where_str .= "and f_cert_status=0 ";

				//支払件数取得関数
				GetTPayLogCountSum($fk_member_id[$i],$where_str,$payment_cnt,$payment_sum);
*/
//G.Chin AREQ-332 2010-11-08 del end
//G.Chin AWKT-522 2010-10-14 add end

/*
				//■ファイル書込
				fputs($myfile, "$fk_member_id[$i],");
				fputs($myfile, "$f_login_id[$i],");
				fputs($myfile, "$f_handle[$i],");
				fputs($myfile, "$status_str,");
				fputs($myfile, "$mail_address,");
				fputs($myfile, "$f_tel_no[$i],");
				fputs($myfile, "$f_birthday[$i],");	//G.Chin AREQ-332 2010-11-08 add
				fputs($myfile, "$f_regist_date[$i],");
				fputs($myfile, "$f_last_entry[$i],");
*/
//G.Chin AWKT-522 2010-10-14 chg sta
/*
				fputs($myfile, "$f_pay_money[$i],");
				fputs($myfile, "$pay_record[$i],");
*/
//G.Chin AREQ-332 2010-11-08 chg sta
/*
				fputs($myfile, "$payment_sum,");
				fputs($myfile, "$payment_cnt,");
*/
/*
				fputs($myfile, "$f_total_pay[$i],");
//G.Chin AREQ-332 2010-11-08 chg end
//G.Chin AWKT-522 2010-10-14 chg end
				fputs($myfile, "$f_unpain_num[$i],");
				fputs($myfile, "$f_cancel_num[$i],");
				fputs($myfile, "$f_user_agent[$i],");
				fputs($myfile, "$f_ser_no[$i],");
				fputs($myfile, "$sex_str,");
				fputs($myfile, "$adcode,");
				fputs($myfile, "$address_str");

				//■ファイル内改行
				fputs($myfile, "\n");
			}

			//■CSVファイル・クローズ
			fclose($myfile);
		}
		else
		{
			$file_cnt = ceil($all_count / MNTSTAFF_MEMCNT);
			$output_cnt = 0;
			$start = 0;

			for($num=0; $num<$file_cnt; $num++)
			{
				$file_num = $num + 1;
				$file_name = "mntstaff".$file_num.".csv";

				if($num == $file_cnt - 1)
				{
					//最終ファイル
					$output_cnt = $all_count;
				}
				else
				{
					$output_cnt = $output_cnt + MNTSTAFF_MEMCNT;
				}

				//■CSVファイル・オープン
				$file_path = sprintf("%s%s", $dist_path, $file_name);
				$myfile = fopen($file_path, "w");
//G.Chin AWKT-510 2010-10-06 chg sta
//				$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,登録日,最終ログイン日,コイン購入金額（合計）,コイン購入回数（合計）,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,連絡先住所";
//G.Chin AREQ-332 2010-11-08 chg sta
//				$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,登録日,最終ログイン日,コイン購入金額（合計）,コイン購入回数（合計）,未払い商品数,キャンセル商品数,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,配送先住所(1件目)";
				$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,誕生日,登録日,最終ログイン日,コイン購入金額（合計）,未払い商品数,キャンセル商品数,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,配送先住所(1件目)";
//G.Chin AREQ-332 2010-11-08 chg end
//G.Chin AWKT-510 2010-10-06 chg end
				//文字コード変換
				$top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
				fputs($myfile, "$top_str\n");
				for($i=$start; $i<$output_cnt; $i++)
				{
					$fk_member_id[$i]		= mysql_result($result, $i, "fk_member_id");
					$f_login_id[$i]			= mysql_result($result, $i, "f_login_id");
					$f_handle[$i]			= mysql_result($result, $i, "f_handle");
					$f_activity[$i]			= mysql_result($result, $i, "f_activity");
					$f_mail_address_pc[$i]	= mysql_result($result, $i, "f_mail_address_pc");
					$f_mail_address_mb[$i]	= mysql_result($result, $i, "f_mail_address_mb");
					$f_tel_no[$i]			= mysql_result($result, $i, "f_tel_no");
					$f_regist_date[$i]		= mysql_result($result, $i, "f_regist_date");
					$f_user_agent[$i]		= mysql_result($result, $i, "f_user_agent");
					$f_ser_no[$i]			= mysql_result($result, $i, "f_ser_no");
					$f_sex[$i]				= mysql_result($result, $i, "f_sex");
					$fk_parent_ad[$i]		= mysql_result($result, $i, "fk_parent_ad");
					$f_birthday[$i]			= mysql_result($result, $i, "f_birthday");	//G.Chin AREQ-332 2010-11-08 add
					$f_coin[$i]				= mysql_result($result, $i, "f_coin");
					$f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
					$f_last_entry[$i]		= mysql_result($result, $i, "f_last_entry");
					$f_total_pay[$i]		= mysql_result($result, $i, "f_total_pay");
					$f_tm_stamp[$i]			= mysql_result($result, $i, "last_time");
					$f_pay_money[$i]		= mysql_result($result, $i, "pay_total");
					$pay_record[$i]			= mysql_result($result, $i, "pay_record");
					$f_unpain_num[$i]		= mysql_result($result, $i, "f_unpain_num");
					$f_cancel_num[$i]		= mysql_result($result, $i, "f_cancel_num");

					//状態
					switch($f_activity[$i])
					{
						case 0:		$status_str = "仮登録";	break;
						case 1:		$status_str = "会員";	break;
						case 2:		$status_str = "退会者";	break;
						default:	$status_str = "";		break;
					}

					//性別判定
					if($f_sex[$i] == 0)	$sex_str = "男性";
					else				$sex_str = "女性";

					//メールアドレス判定
					if($f_mail_address_pc[$i] != "")	$mail_address = $f_mail_address_pc[$i];
					else								$mail_address = $f_mail_address_mb[$i];

					//広告コード
					GetTAdcodeInfo($fk_parent_ad[$i],$ta_f_adcode,$ta_fk_admaster_id,$ta_f_type,$ta_f_tm_stamp);
					if($ta_f_adcode == 0)
					{
						$adcode = "NOP";
					}
					else
					{
						$adcode = $ta_f_adcode;
					}

					//会員住所一覧取得関数
					GetTAddressList($fk_member_id[$i],$tad_fk_address_id,$tad_f_status,$tad_f_last_send,$tad_f_name,$tad_f_tel_no,$tad_f_post_code,$tad_fk_perf_id,$tad_f_address1,$tad_f_address2,$tad_f_address3,$tad_f_tm_stamp,$tad_data_cnt);
					//住所
					if($tad_data_cnt > 0)
					{
						$post_before = substr($tad_f_post_code[0], 0, 3);
						$post_after  = substr($tad_f_post_code[0], 3, 4);
						$address_str = "〒".$post_before."-".$post_after." ".$tad_f_address1[0].$tad_f_address2[0]." ".$tad_f_address3[0];
					}
					else
					{
						$address_str = "";
					}

					//文字コード変換
					$fk_member_id[$i]	= mb_convert_encoding($fk_member_id[$i], "SJIS", "UTF-8");
					$f_login_id[$i]		= mb_convert_encoding($f_login_id[$i], "SJIS", "UTF-8");
					$f_handle[$i]		= mb_convert_encoding($f_handle[$i], "SJIS", "UTF-8");
					$status_str			= mb_convert_encoding($status_str, "SJIS", "UTF-8");
					$mail_address		= mb_convert_encoding($mail_address, "SJIS", "UTF-8");
					$f_tel_no[$i]		= mb_convert_encoding($f_tel_no[$i], "SJIS", "UTF-8");
					$f_regist_date[$i]	= mb_convert_encoding($f_regist_date[$i], "SJIS", "UTF-8");
					$f_last_entry[$i]	= mb_convert_encoding($f_last_entry[$i], "SJIS", "UTF-8");
					$f_pay_money[$i]	= mb_convert_encoding($f_pay_money[$i], "SJIS", "UTF-8");
					$pay_record[$i]		= mb_convert_encoding($pay_record[$i], "SJIS", "UTF-8");
					$f_cancel_num[$i]	= mb_convert_encoding($f_cancel_num[$i], "SJIS", "UTF-8");
					$f_user_agent[$i]	= mb_convert_encoding($f_user_agent[$i], "SJIS", "UTF-8");
					$f_ser_no[$i]		= mb_convert_encoding($f_ser_no[$i], "SJIS", "UTF-8");
					$sex_str			= mb_convert_encoding($sex_str, "SJIS", "UTF-8");
					$adcode				= mb_convert_encoding($adcode, "SJIS", "UTF-8");
					$address_str		= mb_convert_encoding($address_str, "SJIS", "UTF-8");
*/

//G.Chin AWKT-522 2010-10-14 add sta
//G.Chin AREQ-332 2010-11-08 del sta
/*
					$where_str  = "";
					$where_str .= "and f_status=0 ";
					$where_str .= "and f_cert_status=0 ";

					//支払件数取得関数
					GetTPayLogCountSum($fk_member_id[$i],$where_str,$payment_cnt,$payment_sum);
*/
//G.Chin AREQ-332 2010-11-08 del end
//G.Chin AWKT-522 2010-10-14 add end

/*
					//■ファイル書込
					fputs($myfile, "$fk_member_id[$i],");
					fputs($myfile, "$f_login_id[$i],");
					fputs($myfile, "$f_handle[$i],");
					fputs($myfile, "$status_str,");
					fputs($myfile, "$mail_address,");
					fputs($myfile, "$f_tel_no[$i],");
					fputs($myfile, "$f_birthday[$i],");	//G.Chin AREQ-332 2010-11-08 add
					fputs($myfile, "$f_regist_date[$i],");
					fputs($myfile, "$f_last_entry[$i],");
*/
//G.Chin AWKT-522 2010-10-14 chg sta
/*
					fputs($myfile, "$f_pay_money[$i],");
					fputs($myfile, "$pay_record[$i],");
*/
//G.Chin AREQ-332 2010-11-08 chg sta
/*
					fputs($myfile, "$payment_sum,");
					fputs($myfile, "$payment_cnt,");
*/
/*
					fputs($myfile, "$f_total_pay[$i],");
//G.Chin AREQ-332 2010-11-08 chg end
//G.Chin AWKT-522 2010-10-14 chg end
					fputs($myfile, "$f_unpain_num[$i],");
					fputs($myfile, "$f_cancel_num[$i],");
					fputs($myfile, "$f_user_agent[$i],");
					fputs($myfile, "$f_ser_no[$i],");
					fputs($myfile, "$sex_str,");
					fputs($myfile, "$adcode,");
					fputs($myfile, "$address_str");
					//■ファイル内改行
					fputs($myfile, "\n");
				}

				//■CSVファイル・クローズ
				fclose($myfile);

				$start = $i;
			}
		}

		return true;
	}
*/
	function MakeMemberCSV($dist_path,$inp_activity,$inp_id_name,$inp_parent_ad,$inp_group,$inp_mail,$inp_s_regist,$inp_e_regist,$inp_regpos,$inp_s_coin,$inp_e_coin,$inp_s_free,$inp_e_free,$inp_s_totalpay,$inp_e_totalpay,$inp_sex,$inp_mem_id,$inp_name,$inp_unpain,$inp_cancel,$inp_s_last_entry,$inp_e_last_entry,$inp_area,$inp_job,$inp_ip,$sort,$limit,$offset,$save_flg,$inp_file_num,&$file_name,&$all_count,&$file_cnt)
	{
		//echo $sort;
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//状態
		if($inp_activity == "")
		{
			$where_activity = "and tmm.f_activity<5 ";
		}
		else
		{
			$where_activity = "and tmm.f_activity<5 and tmm.f_activity='$inp_activity' ";
		}

		//ログインID・ハンドルネーム
		if($inp_id_name == "")
		{
			$where_id_name = "";
		}
		else
		{
			$where_id_name = "and ((tmm.f_login_id like '%$inp_id_name%') or (tmm.f_handle like '%$inp_id_name%')) ";
		}

		//広告/紹介コード
		if($inp_parent_ad == "")
		{
			$where_parent_ad = "";
		}
		else
		{
			$where_parent_ad = "and tmm.fk_parent_ad='$inp_parent_ad' ";
		}

		//会員グループ
		if($inp_group == "")
		{
			$where_group = "";
		}
		else
		{
			$where_group = "and tmm.fk_member_group_id='$inp_group' ";
		}

		//メールアドレス
		if($inp_mail == "")
		{
			$where_mail = "";
		}
		else
		{
			$where_mail = "and ((tmm.f_mail_address_pc like '%$inp_mail%') or (tmm.f_mail_address_mb like '%$inp_mail%')) ";
		}

		//登録日(開始)
		if($inp_s_regist == "")
		{
			$where_s_regist = "";
		}
		else
		{
			$where_s_regist = "and tmm.f_regist_date>='$inp_s_regist 00:00:00' ";
		}

		//登録日(終了)
		if($inp_e_regist == "")
		{
			$where_e_regist = "";
		}
		else
		{
			$where_e_regist = "and tmm.f_regist_date<='$inp_e_regist 23:59:59' ";
		}

		//登録媒体
		if($inp_regpos == "")
		{
			$where_regpos = "";
		}
		else
		{
			$where_regpos = "and tmm.f_regist_pos='$inp_regpos' ";
		}

		//残り購入コイン数(開始)
		if($inp_s_coin == "")
		{
			$where_s_coin = "";
		}
		else
		{
			$where_s_coin = "and tm.f_coin>='$inp_s_coin' ";
		}

		//残り購入コイン数(終了)
		if($inp_e_coin == "")
		{
			$where_e_coin = "";
		}
		else
		{
			$where_e_coin = "and tm.f_coin<='$inp_e_coin' ";
		}

		//残りサービスコイン数(開始)
		if($inp_s_free == "")
		{
			$where_s_free = "";
		}
		else
		{
			$where_s_free = "and tm.f_free_coin>='$inp_s_free' ";
		}

		//残りサービスコイン数(終了)
		if($inp_e_free == "")
		{
			$where_e_free = "";
		}
		else
		{
			$where_e_free = "and tm.f_free_coin<='$inp_e_free' ";
		}

		//合計コイン購入金額(開始)
		if($inp_s_totalpay == "")
		{
			$where_s_totalpay = "";
		}
		else
		{
			$where_s_totalpay = "and tm.f_total_pay>='$inp_s_totalpay' ";
		}

		//合計コイン購入金額(終了)
		if($inp_e_totalpay == "")
		{
			$where_e_totalpay = "";
		}
		else
		{
			$where_e_totalpay = "and tm.f_total_pay<='$inp_e_totalpay' ";
		}

		//性別
		if($inp_sex == "")
		{
			$where_sex = "";
		}
		else
		{
			$where_sex = "and tmm.f_sex='$inp_sex' ";
		}

		//会員ID
		if($inp_mem_id == "")
		{
			$where_mid = "";
		}
		else
		{
			$where_mid = " and tmm.fk_member_id=$inp_mem_id ";
		}

		//氏名
		if($inp_name == "")
		{
			$where_name = "";
		}
		else
		{
			$where_name =" and (tmm.f_name like '%$inp_name%') ";
		}

		//地域
		if($inp_area == "" or $inp_area==0)
		{
			$where_area = "";
		}
		else
		{
			$where_area =" and (tmm.fk_area_id=$inp_area) ";
		}

		//職業
		if($inp_job == "" or $inp_job==0)
		{
			$where_job = "";
		}
		else
		{
			$where_job =" and (tmm.fk_job_id=$inp_job) ";
		}

		//IPアドレス
		if($inp_ip == "")
		{
			$where_ip = "";
		}
		else
		{
			$where_ip =" and (tmm.f_ip='$inp_ip') ";
		}

		//未払い商品数
		if($inp_unpain == "")
		{
			$where_unpain = "";
		}
		else
		{
			$where_unpain =" and (tm.f_unpain_num=$inp_unpain) ";
		}

		//キャンセル商品数
		if($inp_cancel == "")
		{
			$where_cancel = "";
		}
		else
		{
			$where_cancel =" and (tm.f_cancel_num=$inp_cancel) ";
		}

		//最終ログイン日(開始)
		if($inp_s_last_entry == "")
		{
			$where_s_last_entry = "";
		}
		else
		{
			$where_s_last_entry =" and tm.f_last_entry>='$inp_s_last_entry 00:00:00' ";
		}

		//最終ログイン日(終了)
		if($inp_e_last_entry == "")
		{
			$where_e_last_entry = "";
		}
		else
		{
			$where_e_last_entry =" and tm.f_last_entry<='$inp_e_last_entry 23:59:59' ";
		}

		$order = "";
		//echo  $sort."<BR>\n";
		switch ($sort)
		{
			case 0:
				$order = "tm.fk_member_id ";
				break;
			case 1:
				$order = "tm.fk_member_id desc";
				break;
			case 2:
				$order = "tm.f_total_pay ";
				break;
			case 3:
				$order = "tm.f_total_pay desc ";
				break;
			case 4:
				$order = "tm.f_last_entry ";
				break;
			case 5:
				$order = "tm.f_last_entry desc ";
				break;
			case 6:
				$order = "p1.pay_total ";
				break;
			case 7:
				$order = "p1.pay_total desc ";
				break;
			case 10:
				$order = "p2.last_time ";
				break;
			case 11:
				$order = "p2.last_time desc ";
				break;
			case 12:
				$order = "p3.pay_record ";
				break;
			case 13:
				$order = "p3.pay_record desc ";
				break;
			case 14:
				$order = "tm.f_unpain_num ";
				break;
			case 15:
				$order = "tm.f_unpain_num desc ";
				break;
			case 16:
				$order = "tm.f_cancel_num ";
				break;
			case 17:
				$order = "tm.f_cancel_num desc ";
				break;
		}

		$sql_cnt  = "select count(tmm.fk_member_id) from auction.t_member_master as tmm, auction.t_member as tm ";
		$sql_cnt .= "where tmm.fk_member_id>0 ";
		$sql_cnt .= $where_activity;
		$sql_cnt .= $where_id_name;
		$sql_cnt .= $where_parent_ad;
		$sql_cnt .= $where_group;
		$sql_cnt .= $where_mail;
		$sql_cnt .= $where_s_regist;
		$sql_cnt .= $where_e_regist;
		$sql_cnt .= $where_regpos;
		$sql_cnt .= $where_s_coin;
		$sql_cnt .= $where_e_coin;
		$sql_cnt .= $where_s_free;
		$sql_cnt .= $where_e_free;
		$sql_cnt .= $where_s_totalpay;
		$sql_cnt .= $where_e_totalpay;
		$sql_cnt .= $where_sex;
		$sql_cnt .= $where_mid;
		$sql_cnt .= $where_name;
		$sql_cnt .= $where_area;
		$sql_cnt .= $where_job;
		$sql_cnt .= $where_ip;
		$sql_cnt .= $where_unpain;
		$sql_cnt .= $where_cancel;
		$sql_cnt .= $where_s_last_entry;
		$sql_cnt .= $where_e_last_entry;
		$sql_cnt .= "and tmm.fk_member_id=tm.fk_member_id ";
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);

		$sql  = "select tmm.fk_member_id,tmm.f_login_id,tmm.f_handle,tmm.f_activity,tmm.f_mail_address_pc,tmm.f_mail_address_mb,";
		$sql .= "tmm.f_tel_no,tmm.f_regist_date,tmm.f_user_agent,tmm.f_ser_no,tmm.f_sex,tmm.fk_parent_ad,";
		$sql .= "tmm.f_birthday,";
		$sql .= "tm.f_coin,tm.f_free_coin,tm.f_last_entry,tm.f_total_pay,tm.f_unpain_num,tm.f_cancel_num,";
		$sql .= "tm.f_total_pay_cnt,";
		$sql .= "p2.last_time,p1.pay_total,p3.pay_record ";
		$sql .= "FROM auction.t_member_master AS tmm
						INNER JOIN auction.t_member tm ON tm.fk_member_id=tmm.fk_member_id
						LEFT OUTER JOIN
						(SELECT SUM(f_pay_money)AS pay_total,f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0 AND f_cert_status=0  GROUP BY f_member_id) p1
						ON p1.f_member_id=tm.fk_member_id
						LEFT OUTER JOIN
						(SELECT MAX(f_tm_stamp)AS last_time,f_member_id FROM auction.t_pay_log WHERE f_status<>4 AND f_cert_status=0 GROUP BY f_member_id) p2
						ON p2.f_member_id=tm.fk_member_id
						LEFT OUTER JOIN
						(SELECT COUNT(f_member_id) AS pay_record,f_member_id FROM(SELECT f_member_id FROM  auction.t_pay_log WHERE fk_products_id<>0  AND f_cert_status=0 AND f_status=0 GROUP BY f_member_id,fk_products_id) p3 GROUP BY f_member_id) p3
						ON p3.f_member_id=tm.fk_member_id ";
		$sql .= "where tmm.fk_member_id>0 ";
		$sql .= $where_activity;
		$sql .= $where_id_name;
		$sql .= $where_parent_ad;
		$sql .= $where_group;
		$sql .= $where_mail;
		$sql .= $where_s_regist;
		$sql .= $where_e_regist;
		$sql .= $where_regpos;
		$sql .= $where_s_coin;
		$sql .= $where_e_coin;
		$sql .= $where_s_free;
		$sql .= $where_e_free;
		$sql .= $where_s_totalpay;
		$sql .= $where_e_totalpay;
		$sql .= $where_sex;
		$sql .= $where_mid;
		$sql .= $where_name;
		$sql .= $where_area;
		$sql .= $where_job;
		$sql .= $where_ip;
		$sql .= $where_unpain;
		$sql .= $where_cancel;
		$sql .= $where_s_last_entry;
		$sql .= $where_e_last_entry;
		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";
		$sql .= "order by $order ";
		$sql .= "limit $limit offset $offset ";

			print "$sql<BR>\n";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		echo mysql_error($db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);

		//■ＤＢ切断
	    db_close($db);

		//■保存フラグがない場合
		if($save_flg == 0)
		{
			//■出力フォルダ内の全ファイル削除
			$FileDir = $dist_path;
			$FileList = explode("\n", `find $FileDir -type f -name \* -print | sort`);
			for($i=0; $FileList[$i]!=""; $i++)
			{
				unlink($FileList[$i]);
			}
		}

		if($all_count <= MNTSTAFF_MEMCNT)
		{
			$file_cnt = 1;
			$file_name[0] = "mntstaff.csv";

			//■CSVファイル・オープン
			$file_path = sprintf("%s%s", $dist_path, $file_name[0]);
			$myfile = fopen($file_path, "w");

			$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,誕生日,登録日,最終ログイン日,コイン購入金額（合計）,コイン購入回数（合計）,未払い商品数,キャンセル商品数,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,配送先住所(1件目)";

			//文字コード変換
			$top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
			fputs($myfile, "$top_str\n");

			for($i=0; $i<$data_cnt; $i++)
			{
				$fk_member_id[$i]		= mysql_result($result, $i, "fk_member_id");
				$f_login_id[$i]			= mysql_result($result, $i, "f_login_id");
				$f_handle[$i]			= mysql_result($result, $i, "f_handle");
				$f_activity[$i]			= mysql_result($result, $i, "f_activity");
				$f_mail_address_pc[$i]	= mysql_result($result, $i, "f_mail_address_pc");
				$f_mail_address_mb[$i]	= mysql_result($result, $i, "f_mail_address_mb");
				$f_tel_no[$i]			= mysql_result($result, $i, "f_tel_no");
				$f_regist_date[$i]		= mysql_result($result, $i, "f_regist_date");
				$f_user_agent[$i]		= mysql_result($result, $i, "f_user_agent");
				$f_ser_no[$i]			= mysql_result($result, $i, "f_ser_no");
				$f_sex[$i]				= mysql_result($result, $i, "f_sex");
				$fk_parent_ad[$i]		= mysql_result($result, $i, "fk_parent_ad");
				$f_birthday[$i]			= mysql_result($result, $i, "f_birthday");
				$f_coin[$i]				= mysql_result($result, $i, "f_coin");
				$f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
				$f_unpain_num[$i]		= mysql_result($result, $i, "f_unpain_num");
				$f_cancel_num[$i]		= mysql_result($result, $i, "f_cancel_num");
				$f_last_entry[$i]		= mysql_result($result, $i, "f_last_entry");
				$f_total_pay[$i]		= mysql_result($result, $i, "f_total_pay");
				$f_total_pay_cnt[$i]	= mysql_result($result, $i, "f_total_pay_cnt");
				$f_tm_stamp[$i]			= mysql_result($result, $i, "last_time");
				$f_pay_money[$i]		= mysql_result($result, $i, "pay_total");
				$pay_record[$i]			= mysql_result($result, $i, "pay_record");

				//状態
				switch($f_activity[$i])
				{
					case 0:		$status_str = "仮登録";	break;
					case 1:		$status_str = "会員";	break;
					case 2:		$status_str = "退会者";	break;
					default:	$status_str = "";		break;
				}

				//性別判定
				if($f_sex[$i] == 0)	$sex_str = "男性";
				else				$sex_str = "女性";

				//メールアドレス判定
				if($f_mail_address_pc[$i] != "")	$mail_address = $f_mail_address_pc[$i];
				else								$mail_address = $f_mail_address_mb[$i];

				//広告コード
				GetTAdcodeInfo($fk_parent_ad[$i],$ta_f_adcode,$ta_fk_admaster_id,$ta_f_type,$ta_f_tm_stamp);
				if($ta_f_adcode == "0")
				{
					$adcode = "NOP";
				}
				else
				{
					$adcode = $ta_f_adcode;
				}

				//会員住所一覧取得関数
				GetTAddressList($fk_member_id[$i],$tad_fk_address_id,$tad_f_status,$tad_f_last_send,$tad_f_name,$tad_f_tel_no,$tad_f_post_code,$tad_fk_perf_id,$tad_f_address1,$tad_f_address2,$tad_f_address3,$tad_f_tm_stamp,$tad_data_cnt);
				//住所
				if($tad_data_cnt > 0)
				{
					$post_before = substr($tad_f_post_code[0], 0, 3);
					$post_after  = substr($tad_f_post_code[0], 3, 4);
					$address_str = "〒".$post_before."-".$post_after." ".$tad_f_address1[0].$tad_f_address2[0]." ".$tad_f_address3[0];
				}
				else
				{
					$address_str = "";
				}

				//文字コード変換
				$fk_member_id[$i]	= mb_convert_encoding($fk_member_id[$i], "SJIS", "UTF-8");
				$f_login_id[$i]		= mb_convert_encoding($f_login_id[$i], "SJIS", "UTF-8");
				$f_handle[$i]		= mb_convert_encoding($f_handle[$i], "SJIS", "UTF-8");
				$status_str			= mb_convert_encoding($status_str, "SJIS", "UTF-8");
				$mail_address		= mb_convert_encoding($mail_address, "SJIS", "UTF-8");
				$f_tel_no[$i]		= mb_convert_encoding($f_tel_no[$i], "SJIS", "UTF-8");
				$f_regist_date[$i]	= mb_convert_encoding($f_regist_date[$i], "SJIS", "UTF-8");
				$f_last_entry[$i]	= mb_convert_encoding($f_last_entry[$i], "SJIS", "UTF-8");
				$f_pay_money[$i]	= mb_convert_encoding($f_pay_money[$i], "SJIS", "UTF-8");
				$pay_record[$i]		= mb_convert_encoding($pay_record[$i], "SJIS", "UTF-8");
				$f_unpain_num[$i]	= mb_convert_encoding($f_unpain_num[$i], "SJIS", "UTF-8");
				$f_cancel_num[$i]	= mb_convert_encoding($f_cancel_num[$i], "SJIS", "UTF-8");
				$f_user_agent[$i]	= mb_convert_encoding($f_user_agent[$i], "SJIS", "UTF-8");
				$f_ser_no[$i]		= mb_convert_encoding($f_ser_no[$i], "SJIS", "UTF-8");
				$sex_str			= mb_convert_encoding($sex_str, "SJIS", "UTF-8");
				$adcode				= mb_convert_encoding($adcode, "SJIS", "UTF-8");
				$address_str		= mb_convert_encoding($address_str, "SJIS", "UTF-8");

				//■ファイル書込
				fputs($myfile, "$fk_member_id[$i],");
				fputs($myfile, "$f_login_id[$i],");
				fputs($myfile, "$f_handle[$i],");
				fputs($myfile, "$status_str,");
				fputs($myfile, "$mail_address,");
				fputs($myfile, "$f_tel_no[$i],");
				fputs($myfile, "$f_birthday[$i],");
				fputs($myfile, "$f_regist_date[$i],");
				fputs($myfile, "$f_last_entry[$i],");
				fputs($myfile, "$f_total_pay[$i],");
				fputs($myfile, "$f_total_pay_cnt[$i],");
				fputs($myfile, "$f_unpain_num[$i],");
				fputs($myfile, "$f_cancel_num[$i],");
				fputs($myfile, "$f_user_agent[$i],");
				fputs($myfile, "$f_ser_no[$i],");
				fputs($myfile, "$sex_str,");
				fputs($myfile, "$adcode,");
				fputs($myfile, "$address_str");

				//■ファイル内改行
				fputs($myfile, "\n");
			}

			//■CSVファイル・クローズ
			fclose($myfile);
		}
		else
		{
			$file_cnt = ceil($all_count / MNTSTAFF_MEMCNT);

			for($num=0; $num<$file_cnt; $num++)
			{
				$file_num = $num + 1;
				$file_name[$num] = "mntstaff".$file_num.".csv";

				//■指定したファイル番号と同じ場合
				if($num == $inp_file_num)
				{
					//■CSVファイル・オープン
					$file_path = sprintf("%s%s", $dist_path, $file_name[$num]);
					$myfile = fopen($file_path, "w");
					$top_str = "会員ID,ログイン名,ハンドル名,ステータス（会員・退会者）,メールアドレス,電話番号,誕生日,登録日,最終ログイン日,コイン購入金額（合計）,コイン購入回数（合計）,未払い商品数,キャンセル商品数,携帯機種情報,携帯シリアル,性別,広告コード（自分の広告コード）,配送先住所(1件目)";

					//文字コード変換
					$top_str = mb_convert_encoding($top_str, "SJIS", "UTF-8");
					fputs($myfile, "$top_str\n");

					for($i=0; $i<$data_cnt; $i++)
					{
						$fk_member_id[$i]		= mysql_result($result, $i, "fk_member_id");
						$f_login_id[$i]			= mysql_result($result, $i, "f_login_id");
						$f_handle[$i]			= mysql_result($result, $i, "f_handle");
						$f_activity[$i]			= mysql_result($result, $i, "f_activity");
						$f_mail_address_pc[$i]	= mysql_result($result, $i, "f_mail_address_pc");
						$f_mail_address_mb[$i]	= mysql_result($result, $i, "f_mail_address_mb");
						$f_tel_no[$i]			= mysql_result($result, $i, "f_tel_no");
						$f_regist_date[$i]		= mysql_result($result, $i, "f_regist_date");
						$f_user_agent[$i]		= mysql_result($result, $i, "f_user_agent");
						$f_ser_no[$i]			= mysql_result($result, $i, "f_ser_no");
						$f_sex[$i]				= mysql_result($result, $i, "f_sex");
						$fk_parent_ad[$i]		= mysql_result($result, $i, "fk_parent_ad");
						$f_birthday[$i]			= mysql_result($result, $i, "f_birthday");
						$f_coin[$i]				= mysql_result($result, $i, "f_coin");
						$f_free_coin[$i]		= mysql_result($result, $i, "f_free_coin");
						$f_last_entry[$i]		= mysql_result($result, $i, "f_last_entry");
						$f_total_pay[$i]		= mysql_result($result, $i, "f_total_pay");
						$f_total_pay_cnt[$i]	= mysql_result($result, $i, "f_total_pay_cnt");
						$f_tm_stamp[$i]			= mysql_result($result, $i, "last_time");
						$f_pay_money[$i]		= mysql_result($result, $i, "pay_total");
						$pay_record[$i]			= mysql_result($result, $i, "pay_record");
						$f_unpain_num[$i]		= mysql_result($result, $i, "f_unpain_num");
						$f_cancel_num[$i]		= mysql_result($result, $i, "f_cancel_num");

						//状態
						switch($f_activity[$i])
						{
							case 0:		$status_str = "仮登録";	break;
							case 1:		$status_str = "会員";	break;
							case 2:		$status_str = "退会者";	break;
							default:	$status_str = "";		break;
						}

						//性別判定
						if($f_sex[$i] == 0)	$sex_str = "男性";
						else				$sex_str = "女性";

						//メールアドレス判定
						if($f_mail_address_pc[$i] != "")	$mail_address = $f_mail_address_pc[$i];
						else								$mail_address = $f_mail_address_mb[$i];

						//広告コード
						GetTAdcodeInfo($fk_parent_ad[$i],$ta_f_adcode,$ta_fk_admaster_id,$ta_f_type,$ta_f_tm_stamp);
						if($ta_f_adcode == 0)
						{
							$adcode = "NOP";
						}
						else
						{
							$adcode = $ta_f_adcode;
						}

						//会員住所一覧取得関数
						GetTAddressList($fk_member_id[$i],$tad_fk_address_id,$tad_f_status,$tad_f_last_send,$tad_f_name,$tad_f_tel_no,$tad_f_post_code,$tad_fk_perf_id,$tad_f_address1,$tad_f_address2,$tad_f_address3,$tad_f_tm_stamp,$tad_data_cnt);
						//住所
						if($tad_data_cnt > 0)
						{
							$post_before = substr($tad_f_post_code[0], 0, 3);
							$post_after  = substr($tad_f_post_code[0], 3, 4);
							$address_str = "〒".$post_before."-".$post_after." ".$tad_f_address1[0].$tad_f_address2[0]." ".$tad_f_address3[0];
						}
						else
						{
							$address_str = "";
						}

						//文字コード変換
						$fk_member_id[$i]	= mb_convert_encoding($fk_member_id[$i], "SJIS", "UTF-8");
						$f_login_id[$i]		= mb_convert_encoding($f_login_id[$i], "SJIS", "UTF-8");
						$f_handle[$i]		= mb_convert_encoding($f_handle[$i], "SJIS", "UTF-8");
						$status_str			= mb_convert_encoding($status_str, "SJIS", "UTF-8");
						$mail_address		= mb_convert_encoding($mail_address, "SJIS", "UTF-8");
						$f_tel_no[$i]		= mb_convert_encoding($f_tel_no[$i], "SJIS", "UTF-8");
						$f_regist_date[$i]	= mb_convert_encoding($f_regist_date[$i], "SJIS", "UTF-8");
						$f_last_entry[$i]	= mb_convert_encoding($f_last_entry[$i], "SJIS", "UTF-8");
						$f_pay_money[$i]	= mb_convert_encoding($f_pay_money[$i], "SJIS", "UTF-8");
						$pay_record[$i]		= mb_convert_encoding($pay_record[$i], "SJIS", "UTF-8");
						$f_cancel_num[$i]	= mb_convert_encoding($f_cancel_num[$i], "SJIS", "UTF-8");
						$f_user_agent[$i]	= mb_convert_encoding($f_user_agent[$i], "SJIS", "UTF-8");
						$f_ser_no[$i]		= mb_convert_encoding($f_ser_no[$i], "SJIS", "UTF-8");
						$sex_str			= mb_convert_encoding($sex_str, "SJIS", "UTF-8");
						$adcode				= mb_convert_encoding($adcode, "SJIS", "UTF-8");
						$address_str		= mb_convert_encoding($address_str, "SJIS", "UTF-8");

						//■ファイル書込
						fputs($myfile, "$fk_member_id[$i],");
						fputs($myfile, "$f_login_id[$i],");
						fputs($myfile, "$f_handle[$i],");
						fputs($myfile, "$status_str,");
						fputs($myfile, "$mail_address,");
						fputs($myfile, "$f_tel_no[$i],");
						fputs($myfile, "$f_birthday[$i],");
						fputs($myfile, "$f_regist_date[$i],");
						fputs($myfile, "$f_last_entry[$i],");
						fputs($myfile, "$f_total_pay[$i],");
						fputs($myfile, "$f_total_pay_cnt[$i],");
						fputs($myfile, "$f_unpain_num[$i],");
						fputs($myfile, "$f_cancel_num[$i],");
						fputs($myfile, "$f_user_agent[$i],");
						fputs($myfile, "$f_ser_no[$i],");
						fputs($myfile, "$sex_str,");
						fputs($myfile, "$adcode,");
						fputs($myfile, "$address_str");
						//■ファイル内改行
						fputs($myfile, "\n");
					}

					//■CSVファイル・クローズ
					fclose($myfile);
				}
			}
		}

		return true;
	}
//G.Chin AWKC-218 2010-11-12 chg end

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		全会員コイン数取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetAllMemberCoin(&$sum_f_coin, &$sum_f_free_coin)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select sum(tm.f_coin),sum(tm.f_free_coin) ";
		$sql .= "from auction.t_member as tm,auction.t_member_master as tmm ";
		$sql .= "where tmm.f_activity=1 and tmm.f_delete_flag=0 ";
		$sql .= "and tmm.fk_member_id=tm.fk_member_id ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$sum_f_coin			= mysql_result($result, 0, 0);
		$sum_f_free_coin	= mysql_result($result, 0, 1);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		初回銀行振込フラグ取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetMemberFirstBank($fk_member_id,&$f_first_bank)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql = "select f_first_bank from auction.t_member_master where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_first_bank = mysql_result($result, 0, "f_first_bank");
		}
		else
		{
			$f_first_bank = 0;
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		初回銀行振込フラグ更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateMemberFirstBank($fk_member_id,$f_first_bank)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
		$sql = "update auction.t_member_master set f_first_bank='$f_first_bank' where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員都道府県情報取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetAreaByID(&$fk_area_id,&$f_name,&$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_area_id,f_name,f_status ";
		$sql .= "from auction.t_member_area where fk_area_id=$fk_area_id ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$fk_area_id	= mysql_result($result, 0, "fk_area_id");
			$f_name	= mysql_result($result, 0, "f_name");
			$f_status	= mysql_result($result, 0, "f_status");
		}
		else
		{
			$fk_area_id	= "";
			$f_name	= "";
			$f_status	= "";
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員地域登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function RegistMemArea($fk_area_name,$now,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_area
		$sql  = "insert into auction.t_member_area ";
		$sql .= "(f_name,f_status,";
		$sql .= "f_tm_stamp) ";
		$sql .= "values ";
		$sql .= "('$fk_area_name',$f_status,'$now')";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員地域情報更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

    function UpdatetMemArea($fk_area_id,$f_name,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		//mysql_query("BEGIN", $db);

		$sql  = "update auction.t_member_area set f_name='$f_name',f_status=$f_status ";
		$sql .= "where fk_area_id=$fk_area_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員地域リスト取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetMemAreaList(&$fk_area_id, &$f_name, &$f_status, &$fk_all_cnt,$status,&$fk_data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		$sql_cnt  = "select count(*) from auction.t_member_area as tma ";
		$sql_cnt .= "where tma.f_status=$status and tma.fk_area_id<>0";

		//print "$sql_cnt<BR>\n";
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■全件数
		$fk_all_cnt = mysql_result($result, 0, 0);

		$sql  = "select tma.fk_area_id,tma.f_name,tma.f_status ";
		$sql .= "from auction.t_member_area as tma ";
		$sql .= "where tma.f_status=$status ";
		$sql .= "and tma.fk_area_id<>0 ";
		$sql .= "order by tma.fk_area_id ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$fk_data_cnt = mysql_num_rows($result);

		for($i=0; $i<$fk_data_cnt; $i++)
		{
			$fk_area_id[$i]	= mysql_result($result, $i, "fk_area_id");
			$f_name[$i]		= mysql_result($result, $i, "f_name");
			$f_status[$i]	= mysql_result($result, $i, "f_status");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員地域情報削除関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function AreaDelete($fk_area_id)
	{
		//■ＤＢ接続
		$ret = false;
		try {
			$db=db_connect();
			if($db == false) {
				exit;
			}

		    //■トランザクション開始
		    mysql_query("BEGIN", $db);

		    //t_member_area
		    $sql = "update auction.t_member_area set f_status=9 where fk_area_id='$fk_area_id' ";

			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
				db_close($db);
				return false;
			}

			//■コミット
			mysql_query("COMMIT", $db);

			//■ＤＢ切断
			db_close($db);
			return true;
		}
		catch(Exception $e)
		{
			DB::rollback();
		}
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function RegistMemJob($fk_job_name,$now,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_job
		$sql  = "insert into auction.t_member_job ";
		$sql .= "(f_name,f_status,";
		$sql .= "f_tm_stamp) ";
		$sql .= "values ";
		$sql .= "('$fk_job_name',$f_status,'$now')";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業リスト取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetMemJobList(&$fk_job_id, &$f_name, &$f_status, &$fk_all_cnt,$status,&$fk_data_cnt)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql_cnt  = "select count(*) from auction.t_member_job as tmj ";
		$sql_cnt .= "where tmj.f_status=$status and tmj.fk_job_id<>0 ";
		//print "$sql_cnt<BR>\n";

		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■全件数
		$fk_all_cnt = mysql_result($result, 0, 0);

		$sql  = "select tmj.fk_job_id,tmj.f_name,tmj.f_status ";
		$sql .= "from auction.t_member_job as tmj ";
		$sql .= "where tmj.f_status=$status ";
		$sql .= "and tmj.fk_job_id<>0 ";
		$sql .= "order by tmj.fk_job_id ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$fk_data_cnt = mysql_num_rows($result);

		for($i=0; $i<$fk_data_cnt; $i++)
		{
			$fk_job_id[$i]	= mysql_result($result, $i, "fk_job_id");
			$f_name[$i]		= mysql_result($result, $i, "f_name");
			$f_status[$i]	= mysql_result($result, $i, "f_status");
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業情報削除関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function JobDelete($fk_job_id)
	{
		//■ＤＢ接続
		$ret = false;
		try
		{
			$db=db_connect();
			if($db == false)
			{
				exit;
			}

			//■トランザクション開始
			mysql_query("BEGIN", $db);

			//t_member_job
			$sql = "update auction.t_member_job set f_status=9 where fk_job_id=$fk_job_id ";

			//■ＳＱＬ実行
			$result = mysql_query($sql, $db);
			if( $result == false )
			{
				print "$sql<BR>\n";
				//■ロールバック
				mysql_query("ROLLBACK", $db);
				//■ＤＢ切断
				db_close($db);
				return false;
			}

			//■コミット
			mysql_query("COMMIT", $db);

			//■ＤＢ切断
			db_close($db);
			return true;
		}
		catch(Exception $e)
		{
			DB::rollback();
		}
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業情報取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetJbByID(&$fk_job_id,&$f_name,&$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select fk_job_id,f_name,f_status ";
		$sql .= "from auction.t_member_job where fk_job_id='$fk_job_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$fk_job_id	= mysql_result($result, 0, "fk_job_id");
			$f_name		= mysql_result($result, 0, "f_name");
			$f_status	= mysql_result($result, 0, "f_status");
		}
		else
		{
			$fk_job_id	= "";
			$f_name		= "";
			$f_status	= "";
		}

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員職業情報更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdatetMemJob($fk_job_id,$f_name,$f_status)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		//mysql_query("BEGIN", $db);

		$sql  = "update auction.t_member_job set f_name='$f_name',f_status=$f_status ";
		$sql .= "where fk_job_id=$fk_job_id";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
			db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員機種情報取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberUserAgent($fk_member_id,&$f_user_agent)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_user_agent from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_user_agent = mysql_result($result, 0, "f_user_agent");
		}
		else
		{
			$f_user_agent = "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員機種情報更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function UpdateMemberUserAgent($fk_member_id,$f_user_agent)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		//t_member_master
		$sql  = "update auction.t_member_master set f_user_agent='$f_user_agent' ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ハンドルが存在かどうかチェック関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

//G.Chin AWKT-875 2010-12-15 chg sta
//	function isExistedHandle($handle,$member_id,&$rows)
	function CheckHandleExist($handle,$member_id,&$rows)
//G.Chin AWKT-875 2010-12-15 chg end
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select *  from auction.t_member_master ";
		$sql .= "where f_handle='$handle' ";
		$sql .= "and fk_member_id<>$member_id ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員IDで会員情報取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function getMemberById($mem_id,&$mem_handle,&$f_login_id,&$f_mail_address_pc,&$f_mail_address_mb,&$rows)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "SELECT mast.f_handle,mast.f_login_id,mast.f_mail_address_pc,mast.f_mail_address_mb ";
		$sql .= "FROM auction.t_member_master as mast ";
		$sql .= "where mast.f_activity=1 ";
		$sql .= "AND mast.f_delete_flag=0 ";
		$sql .= "AND mast.fk_member_id=$mem_id";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		$rows = mysql_num_rows($result);

		for($i=0; $i<$rows; $i++)
		{
			$mem_handle[$i]				= mysql_result($result, $i, "f_handle");
			$f_login_id[$i]				= mysql_result($result, $i, "f_login_id");
			$f_mail_address_pc[$i]		= mysql_result($result, $i, "f_mail_address_pc");
			$f_mail_address_mb[$i]		= mysql_result($result, $i, "f_mail_address_mb");
		}
		//■ＤＢ切断
		db_close($db);
		return true;
	}

	function get_setting ($id)
	{
		$sql = "select fk_knocked_id from auction.t_knock where f_status=0 and fk_member_id=$id";
		//echo $sql;
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		$rs = mysql_query($sql,$db);
		db_close($db);

		return mysql_num_rows($rs);
	}

	function get_setting_check ($id)
	{
		$sql  = "select fk_knocked_id,f_knocked_price,f_tm_stamp ";
		$sql .= "from auction.t_knock where f_status=0 and fk_member_id=$id";
		//echo $sql;
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		$rs = mysql_query($sql,$db);
		db_close($db);

		return $rs;
	}

	function set_setting ($id,$price)
	{
		$sql  = "insert into auction.t_knock(fk_member_id,f_knocked_price,f_status,f_tm_stamp) ";
		$sql .= "values($id,$price,0,now())";
		//echo $sql;
		$db=db_connect();
		if($db == false)
		{
			return false;
		}
		$rs = mysql_query($sql,$db);
		db_close($db);

		return $rs;
	}

	function set_setting_cancel($id)
	{
		$sql="update auction.t_knock set f_status =1,f_tm_stamp=now() where fk_knocked_id =$id";
		$db=db_connect();
		if($db == false)
		{
			return false;
		}
		$rs = mysql_query($sql,$db);
		db_close($db);

		return $rs;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員性別取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function GetTMemberSex($fk_member_id,&$f_sex)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}

		$sql  = "select f_sex from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";

		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);

		if($rows > 0)
		{
			$f_sex = mysql_result($result, 0, "f_sex");
		}
		else
		{
			$f_sex = "";
		}

		//■ＤＢ切断
	    db_close($db);
		return true;
	}

?>