<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/09												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** ＤＢ関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ＤＢ接続関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function db_connect()
	{
		$host=DB_SERVER;
		$user=DB_USER;
		$pass=DB_PASS;
		$dbname=DB_NAME;
		
	    //MYSQL DBへの接続データの取得
	    $db = mysql_connect($host,$user,$pass);
	    mysql_query("SET NAMES utf8",$db);
	    return $db;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		ＤＢ切断関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function db_close($db)
	{
	    mysql_close($db);
	}

?>
