<?php
/* 
 * アプリケーションの設定ファイル
 *ログ関連
 */
return  array (

/* ログ関連 */
'LOG_RECORD'=>true,
'LOG_DIR'=>'/home/auction/log/',
'LOG_FILE_NAME'=>'auction',
'LOG_FILE_SIZE'=>1024000,
'LOG_LEVEL'=>'FATAL,ERROR,WARN,DEBUG',        //ログ出力のレベルFATAL,ERROR,WARN,INFO,DEBUG
'LOG_APPENDER_CLASS'=>'log.appender.PHPAppender',
'LOG_DIRECT'=>true

    
);

?>
