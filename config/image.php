<?php
/* 
 * アプリケーションの設定ファイル
 *画像認証
 */
return  array (

/*    商品画像のパス      */
'PRODUCT_IMAGE_PATH'=>' ',   //自動設定されるので、手動で設定しないこと

    
/*    画像認証      */
'IMAGE_TYPE'=>'png',        //生成する画像のタイプ
'IGNORE_CASW'=>true,        //認証コードの大文字と小文字の違いを無視するか,
'STRING_LENGTH'=>VERIFY_STRING_LENGTH,         //生成する画像の文字の数
'STRING_MODE'=>VERIFY_STRING_MODE,         //生成する画像の文字のタイプ(StringBuilderに参照）
'IMAGE_WIDTH'=>110,         //生成する画像の幅
'IMAGE_HEIGHT'=>30,         //生成する画像の縦
'SESSION_VERIFY_NAME'=>'verify',    //認証コードをセッションに保存する時のキー

/* File upload */
'ALLOW_TYPE'=>'image/bmp,image/gif,image/jpeg,image/png',
'ALLOW_EXTS'=>'jpg,jpeg,gif,png,bmp',
'MAX_SIZE'=>10240,
'SAVE_PATH'=>APP_PATH.'/auction',
//'SAVE_PATH_'=>''
    
);

?>
