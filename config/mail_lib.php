<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/25												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** メール関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガ送信予約一覧取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTMailReserveList($limit,$offset,&$fk_reserve_id,&$f_subject,&$f_body,&$f_picture,&$f_target,&$f_smtp_host,&$f_smtp_port,&$f_send_type,&$f_reserve_time,&$f_send_counts,&$f_start_time,&$f_end_time,&$f_send_to,&$f_status,&$f_req_stop,&$f_sql,&$f_sql_desc,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql_cnt = "select count(*) from auction.t_mail_reserve ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		$sql  = "select fk_reserve_id,f_subject,f_body,f_picture,f_target,";
		$sql .= "f_smtp_host,f_smtp_port,f_send_type,f_reserve_time,f_send_counts,";
		$sql .= "f_start_time,f_end_time,f_send_to,f_status,f_req_stop,";
		$sql .= "f_sql,f_sql_desc,f_tm_stamp ";
		$sql .= "from auction.t_mail_reserve ";
		$sql .= "order by fk_reserve_id desc ";
		$sql .= "limit $limit offset $offset";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_reserve_id[$i]	= mysql_result($result, $i, "fk_reserve_id");
			$f_subject[$i]		= mysql_result($result, $i, "f_subject");
			$f_body[$i]			= mysql_result($result, $i, "f_body");
			$f_picture[$i]		= mysql_result($result, $i, "f_picture");
			$f_target[$i]		= mysql_result($result, $i, "f_target");
			$f_smtp_host[$i]	= mysql_result($result, $i, "f_smtp_host");
			$f_smtp_port[$i]	= mysql_result($result, $i, "f_smtp_port");
			$f_send_type[$i]	= mysql_result($result, $i, "f_send_type");
			$f_reserve_time[$i]	= mysql_result($result, $i, "f_reserve_time");
			$f_send_counts[$i]	= mysql_result($result, $i, "f_send_counts");
			$f_start_time[$i]	= mysql_result($result, $i, "f_start_time");
			$f_end_time[$i]		= mysql_result($result, $i, "f_end_time");
			$f_send_to[$i]		= mysql_result($result, $i, "f_send_to");
			$f_status[$i]		= mysql_result($result, $i, "f_status");
			$f_req_stop[$i]		= mysql_result($result, $i, "f_req_stop");
			$f_sql[$i]			= mysql_result($result, $i, "f_sql");
			$f_sql_desc[$i]		= mysql_result($result, $i, "f_sql_desc");
			$f_tm_stamp[$i]		= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガ送信予約情報取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTMailReserveInfo($fk_reserve_id,&$f_subject,&$f_body,&$f_picture,&$f_target,&$f_smtp_host,&$f_smtp_port,&$f_send_type,&$f_reserve_time,&$f_send_counts,&$f_start_time,&$f_end_time,&$f_send_to,&$f_status,&$f_req_stop,&$f_sql,&$f_sql_desc,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_reserve_id,f_subject,f_body,f_picture,f_target,";
		$sql .= "f_smtp_host,f_smtp_port,f_send_type,f_reserve_time,f_send_counts,";
		$sql .= "f_start_time,f_end_time,f_send_to,f_status,f_req_stop,";
		$sql .= "f_sql,f_sql_desc,f_tm_stamp ";
		$sql .= "from auction.t_mail_reserve ";
		$sql .= "where fk_reserve_id='$fk_reserve_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_subject		= mysql_result($result, 0, "f_subject");
			$f_body			= mysql_result($result, 0, "f_body");
			$f_picture		= mysql_result($result, 0, "f_picture");
			$f_target		= mysql_result($result, 0, "f_target");
			$f_smtp_host	= mysql_result($result, 0, "f_smtp_host");
			$f_smtp_port	= mysql_result($result, 0, "f_smtp_port");
			$f_send_type	= mysql_result($result, 0, "f_send_type");
			$f_reserve_time	= mysql_result($result, 0, "f_reserve_time");
			$f_send_counts	= mysql_result($result, 0, "f_send_counts");
			$f_start_time	= mysql_result($result, 0, "f_start_time");
			$f_end_time		= mysql_result($result, 0, "f_end_time");
			$f_send_to		= mysql_result($result, 0, "f_send_to");
			$f_status		= mysql_result($result, 0, "f_status");
			$f_req_stop		= mysql_result($result, 0, "f_req_stop");
			$f_sql			= mysql_result($result, 0, "f_sql");
			$f_sql_desc		= mysql_result($result, 0, "f_sql_desc");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_subject		= "";
			$f_body			= "";
			$f_picture		= "";
			$f_target		= "";
			$f_smtp_host	= "";
			$f_smtp_port	= "";
			$f_send_type	= "";
			$f_reserve_time	= "";
			$f_send_counts	= "";
			$f_start_time	= "";
			$f_end_time		= "";
			$f_send_to		= "";
			$f_status		= "";
			$f_req_stop		= "";
			$f_sql			= "";
			$f_sql_desc		= "";
			$f_tm_stamp		= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガ送信予約登録関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTMailReserve($f_subject,$f_body,$f_target,$f_reserve_time,$f_send_counts,$f_send_to,$f_sql,$f_sql_desc)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
        if ( !get_magic_quotes_gpc() ) {
            $f_subject = mysql_real_escape_string( $f_subject, $db);
            $f_body = mysql_real_escape_string( $f_body, $db);
        }

		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_mail_reserve
		$sql  = "insert into auction.t_mail_reserve ";
		$sql .= "(fk_reserve_id,f_subject,f_body,f_target,f_reserve_time,";
		$sql .= "f_send_counts,f_send_to,f_status,f_sql,f_sql_desc) ";
		$sql .= "values (0,'$f_subject','$f_body','$f_target','$f_reserve_time',";
		$sql .= "'$f_send_counts','$f_send_to','0','$f_sql','$f_sql_desc') ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガ送信予約更新関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTMailReserve($fk_reserve_id,$f_subject,$f_body,$f_target,$f_reserve_time,$f_send_counts,$f_send_to,$f_status,$f_sql,$f_sql_desc)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
        if ( !get_magic_quotes_gpc() ) {
            $f_subject = mysql_real_escape_string( $f_subject, $db);
            $f_body = mysql_real_escape_string( $f_body, $db);
        }
        
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_mail_reserve
		$sql  = "update auction.t_mail_reserve set ";
		$sql .= "f_subject='$f_subject',f_body='$f_body',";
		$sql .= "f_target='$f_target',f_reserve_time='$f_reserve_time',";
		$sql .= "f_send_counts='$f_send_counts',f_send_to='$f_send_to',";
		$sql .= "f_status='$f_status',f_sql='$f_sql',f_sql_desc='$f_sql_desc' ";
		$sql .= "where fk_reserve_id='$fk_reserve_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガ送信予約中断要求更新関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateFReqStop($fk_reserve_id,$f_req_stop)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_member_group
		$sql  = "update auction.t_mail_reserve set ";
		$sql .= "f_req_stop='$f_req_stop' ";
		$sql .= "where fk_reserve_id='$fk_reserve_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガテンプレート一覧取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTMailTemplateList(&$fk_mail_id,&$f_status,&$f_name,&$f_subject,&$f_body,&$f_picture,&$f_target,&$f_memo,&$f_tm_stamp,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_mail_id,f_status,";
		$sql .= "f_name,f_subject,f_body,";
		$sql .= "f_picture,f_target,f_memo,f_tm_stamp ";
		$sql .= "from auction.t_mail_template ";
		$sql .= "order by fk_mail_id desc";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_mail_id[$i]	= mysql_result($result, $i, "fk_mail_id");
			$f_status[$i]	= mysql_result($result, $i, "f_status");
			$f_name[$i]		= mysql_result($result, $i, "f_name");
			$f_subject[$i]	= mysql_result($result, $i, "f_subject");
			$f_body[$i]		= mysql_result($result, $i, "f_body");
			$f_picture[$i]	= mysql_result($result, $i, "f_picture");
			$f_target[$i]	= mysql_result($result, $i, "f_target");
			$f_memo[$i]		= mysql_result($result, $i, "f_memo");
			$f_tm_stamp[$i]	= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガテンプレート名一覧取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTMailTemplateNameList(&$fk_mail_id,&$f_name,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_mail_id,f_name ";
		$sql .= "from auction.t_mail_template ";
		$sql .= "order by fk_mail_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_mail_id[$i]	= mysql_result($result, $i, "fk_mail_id");
			$f_name[$i]		= mysql_result($result, $i, "f_name");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガテンプレート情報取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTMailTemplateInfo($fk_mail_id,&$f_status,&$f_name,&$f_subject,&$f_body,&$f_picture,&$f_target,&$f_memo,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_status,";
		$sql .= "f_name,f_subject,f_body,";
		$sql .= "f_picture,f_target,f_memo,f_tm_stamp ";
		$sql .= "from auction.t_mail_template ";
		$sql .= "where fk_mail_id='$fk_mail_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_status	= mysql_result($result, 0, "f_status");
			$f_name		= mysql_result($result, 0, "f_name");
			$f_subject	= mysql_result($result, 0, "f_subject");
			$f_body		= mysql_result($result, 0, "f_body");
			$f_picture	= mysql_result($result, 0, "f_picture");
			$f_target	= mysql_result($result, 0, "f_target");
			$f_memo		= mysql_result($result, 0, "f_memo");
			$f_tm_stamp	= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_status	= "";
			$f_name		= "";
			$f_subject	= "";
			$f_body		= "";
			$f_picture	= "";
			$f_target	= "";
			$f_memo		= "";
			$f_tm_stamp	= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガテンプレート登録関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTMailTemplate($f_name,$f_subject,$f_body,$f_target)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }

		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_mail_template
		$sql  = "insert into auction.t_mail_template ";
		$sql .= "(fk_mail_id,f_name,f_subject,f_body,f_target,f_tm_stamp) ";
		$sql .= "values (0,'$f_name','$f_subject','$f_body','$f_target',now()) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガテンプレート更新関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTMailTemplate($fk_mail_id,$f_name,$f_subject,$f_body,$f_target)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//t_mail_template
		$sql  = "update auction.t_mail_template set ";
		$sql .= "f_name='$f_name',";
		$sql .= "f_subject='$f_subject',";
		$sql .= "f_body='$f_body',";
		$sql .= "f_target='$f_target' ";
		$sql .= "where fk_mail_id='$fk_mail_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メール要求登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTMailRequest($f_type,$f_subject,$f_body,$f_sendfrom,$f_sendto)
	{
            //■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql  = "insert into auction.t_mail_request ";
		$sql .= "(t_mail_request,f_type,f_subject,f_body,f_sendfrom,f_sendto,f_tm_stamp) ";
		$sql .= "values (0,'$f_type','$f_subject','$f_body','$f_sendfrom','$f_sendto',now()) ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		配送済みのお知らせメール登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/

	function DeliveryMailRequest($f_type,$f_subject,$f_body,$f_sendfrom,$f_sendto,$pid,$f_send_mail,$f_send_mail_id)
	{
            //■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }

		//■トランザクション開始
		mysql_query("BEGIN", $db);

		$sql  = "insert into auction.t_mail_request ";
		$sql .= "(t_mail_request,f_type,f_subject,f_body,f_sendfrom,f_sendto,f_tm_stamp) ";
		$sql .= "values (0,'$f_type','$f_subject','$f_body','$f_sendfrom','$f_sendto',now()) ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

                $sql1  = "update auction.t_end_products ";
		$sql1 .= "set f_send_mail=$f_send_mail,f_send_mail_id='$f_send_mail_id' ";
		$sql1 .= "where fk_end_products_id=$pid ";
                $result = mysql_query($sql1, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}

		//■コミット
		mysql_query("COMMIT", $db);

		//■ＤＢ切断
	    db_close($db);
            return true;
	}


?>