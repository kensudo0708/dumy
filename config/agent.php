<?php
/* 
 * アプリケーションの設定ファイル
 *User Agentの関連設定
 */
return  array (

/* USER AGENT設定  */
'PC_DIR'=>'pc',
'MOBILE_DIR'=>'mobile',
'PC_AGENT'=>'ie, opera, netscape,mozilla, konqueror, firefox,chrome,googlebot,elinks,lynx',
'MOBILE_AGENT'=>'DoCoMo, KDDI, softbank,Vodafone'

);

?>
