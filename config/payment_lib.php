<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/10												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 支払関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		コイン売却価格一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTCoinSetupList($fk_coin_group_id,&$fk_coin_id,&$f_coin,&$fk_shiharai_type_id,&$f_inp_money,&$f_tm_stamp,&$f_free_coin,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_coin_id,f_coin,fk_shiharai_type_id,f_inp_money,f_tm_stamp,f_free_coin ";
		$sql .= "from auction.t_coin_setup ";
		$sql .= "where fk_coin_group_id='$fk_coin_group_id' ";
		$sql .= "order by f_coin";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_coin_id[$i]				= mysql_result($result, $i, "fk_coin_id");
			$f_coin[$i]					= mysql_result($result, $i, "f_coin");
			$fk_shiharai_type_id[$i]	= mysql_result($result, $i, "fk_shiharai_type_id");
			$f_inp_money[$i]			= mysql_result($result, $i, "f_inp_money");
			$f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
			$f_free_coin[$i]			= mysql_result($result, $i, "f_free_coin");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		コイン売却価格情報取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTCoinSetupInfo($fk_coin_id,&$fk_coin_group_id,&$f_coin,&$fk_shiharai_type_id,&$f_inp_money,&$f_tm_stamp,&$f_free_coin)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_coin_group_id,f_coin,fk_shiharai_type_id,f_inp_money,f_tm_stamp,f_free_coin ";
		$sql .= "from auction.t_coin_setup ";
		$sql .= "where fk_coin_id='$fk_coin_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_coin_group_id		= mysql_result($result, 0, "fk_coin_group_id");
			$f_coin					= mysql_result($result, 0, "f_coin");
			$fk_shiharai_type_id	= mysql_result($result, 0, "fk_shiharai_type_id");
			$f_inp_money			= mysql_result($result, 0, "f_inp_money");
			$f_tm_stamp				= mysql_result($result, 0, "f_tm_stamp");
			$f_free_coin			= mysql_result($result, 0, "f_free_coin");
		}
		else
		{
			$fk_coin_group_id		= "";
			$f_coin					= "";
			$fk_shiharai_type_id	= "";
			$f_inp_money			= "";
			$f_tm_stamp				= "";
			$f_free_coin= 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		コイン売却価格グループ名一覧取得関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTCoinGroupNameList(&$fk_coin_group_id, &$f_coin_group_name, &$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_coin_group_id,f_coin_group_name ";
		$sql .= "from auction.t_coin_group order by fk_coin_group_id";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_coin_group_id[$i]	= mysql_result($result, $i, "fk_coin_group_id");
			$f_coin_group_name[$i]	= mysql_result($result, $i, "f_coin_group_name");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		コイン売却価格グループ情報取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTCoinGroupInfo($fk_coin_group_id,&$f_coin_group_name,&$f_memo,&$f_status,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_coin_group_name,f_memo,f_status,f_tm_stamp ";
		$sql .= "from auction.t_coin_group ";
		$sql .= "where fk_coin_group_id='$fk_coin_group_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_coin_group_name	= mysql_result($result, 0, "f_coin_group_name");
			$f_memo				= mysql_result($result, 0, "f_memo");
			$f_status			= mysql_result($result, 0, "f_status");
			$f_tm_stamp			= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_coin_group_name	= "";
			$f_memo				= "";
			$f_status			= "";
			$f_tm_stamp			= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払履歴記録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function InsertTPayLog($f_member_id,$f_status,$f_staff_id,$f_pay_money,$f_coin_add,$fk_shiharai_type_id,$f_coin_result,$fk_adcode_id,$f_free_coin)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
/*
		//追加コインの累計を取得
		$sql  = "select f_coin_result ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where f_member_id='$f_member_id' ";
		$sql .= "order by fk_pay_log_id desc limit 1 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
//			$ttl_coin_result = mysql_result($result, 0, "f_coin_result");
			$ttl_coin_result = mysql_result($result, 0, 0);
		}
		else
		{
			$ttl_coin_result = 0;
		}
		
		//コイン購入後枚数に加算
		$f_coin_result = $ttl_coin_result + $f_coin_add;
*/
		//t_pay_log
		$sql  = "insert into auction.t_pay_log ";
                $sql .= "(fk_pay_log_id,'',f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,f_tm_stamp,fk_adcode_id,f_free_coin_add) ";
		$sql .= "values (0,'$f_status','$f_member_id','$f_staff_id','$f_pay_money','$f_coin_add','$fk_shiharai_type_id','$f_coin_result',now(),'$fk_adcode_id',$f_free_coin) ";
		//echo $sql;
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品支払記録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
        function InsertProductsTPayLog($f_member_id,$f_status,$f_staff_id,$f_pay_money,$f_coin_add,$fk_shiharai_type_id,$f_coin_result,$f_min_price,$f_cert_status,$f_cert_err,$f_pay_pos,$fk_adcode_id,$fk_products_id,$db) {
            //■ＤＢ接続
//	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
//
//            if($db==false) {
//                exit;
//            }else {
            //■トランザクション開始
//            mysql_query("BEGIN", $db);


                //t_pay_log
                $sql  = "insert into auction.t_pay_log ";
                $sql .= "(fk_pay_log_id,f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,";
                $sql .= "f_min_price,f_cert_status,f_cert_err,f_pay_pos,fk_adcode_id,fk_products_id,f_tm_stamp) ";
                $sql .= "values ";
                $sql .= "(0,'$f_status','$f_member_id','$f_staff_id','$f_pay_money','$f_coin_add','$fk_shiharai_type_id','$f_coin_result',";
                $sql .= "'$f_min_price','$f_cert_status','$f_cert_err','$f_pay_pos','$fk_adcode_id','$fk_products_id',now()) ";

                //■ＳＱＬ実行
                $result = mysql_query($sql, $db);
                if( $result == false ) {
//                    print "$sql<BR>\n";
//                    //■ロールバック
//                    mysql_query("ROLLBACK", $db);
//                    //■ＤＢ切断
//                    db_close($db);
                    return false;
                }

                //■コミット
//                mysql_query("COMMIT", $db);

                //■ＤＢ切断
//               db_close($db);
                return true;
//            }
        }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払履歴一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
//G.Chin 2010-07-12 chg sta
//	function GetTPayLogList($f_member_id,$limit,$offset,&$fk_pay_log_id,&$f_status,&$f_staff_id,&$f_pay_money,&$f_coin_add,&$fk_shiharai_type_id,&$f_coin_result,&$f_min_price,&$f_cert_status,&$f_cert_err,&$f_pay_pos,&$f_tm_stamp,&$fk_adcode_id,&$fk_products_id,&$all_count,&$data_cnt)
	function GetTPayLogList($f_member_id,$limit,$offset,&$fk_pay_log_id,&$f_status,&$f_staff_id,&$f_pay_money,&$f_coin_add,&$f_free_coin_add,&$fk_shiharai_type_id,&$f_coin_result,&$f_min_price,&$f_cert_status,&$f_cert_err,&$f_pay_pos,&$f_tm_stamp,&$fk_adcode_id,&$fk_products_id,&$all_count,&$data_cnt)
//G.Chin 2010-07-12 chg end
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql_cnt  = "select count(*) from auction.t_pay_log ";
		$sql_cnt .= "where f_member_id='$f_member_id' ";
		$sql_cnt .= "and f_cert_status=0 and f_status in(0,1,2,3,5) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
//G.Chin 2010-07-12 chg sta
//		$sql  = "select fk_pay_log_id,f_status,f_staff_id,f_pay_money,f_coin_add,";
		$sql  = "select fk_pay_log_id,f_status,f_staff_id,f_pay_money,f_coin_add,f_free_coin_add,";
//G.Chin 2010-07-12 chg end
		$sql .= "fk_shiharai_type_id,f_coin_result,f_min_price,f_cert_status,f_cert_err,";
		$sql .= "f_pay_pos,f_tm_stamp,fk_adcode_id,fk_products_id ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where f_member_id='$f_member_id' ";
//G.Chin 2010-07-26 chg sta
//		$sql .= "and f_cert_status=0 and f_status in(0,1,2) ";
		$sql .= "and f_cert_status=0 and f_status in(0,1,2,3,5) ";
//G.Chin 2010-07-26 chg end
		$sql .= "order by fk_pay_log_id desc ";
		$sql .= "limit $limit offset $offset";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_pay_log_id[$i]			= mysql_result($result, $i, "fk_pay_log_id");
			$f_status[$i]				= mysql_result($result, $i, "f_status");
			$f_staff_id[$i]				= mysql_result($result, $i, "f_staff_id");
			$f_pay_money[$i]			= mysql_result($result, $i, "f_pay_money");
			$f_coin_add[$i]				= mysql_result($result, $i, "f_coin_add");
			$f_free_coin_add[$i]		= mysql_result($result, $i, "f_free_coin_add");	//G.Chin 2010-07-12 add
			$fk_shiharai_type_id[$i]	= mysql_result($result, $i, "fk_shiharai_type_id");
			$f_coin_result[$i]			= mysql_result($result, $i, "f_coin_result");
			$f_min_price[$i]			= mysql_result($result, $i, "f_min_price");
			$f_cert_status[$i]			= mysql_result($result, $i, "f_cert_status");
			$f_cert_err[$i]				= mysql_result($result, $i, "f_cert_err");
			$f_pay_pos[$i]				= mysql_result($result, $i, "f_pay_pos");
			$f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
			$fk_adcode_id[$i]			= mysql_result($result, $i, "fk_adcode_id");
			$fk_products_id[$i]			= mysql_result($result, $i, "fk_products_id");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		アフィリエイト用支払履歴情報取得関数													*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetAffiliateTPayLogInfo($f_member_id,&$recent_money,&$post_money,&$all_count)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//全履歴件数を取得
		$sql_cnt  = "select count(*) from auction.t_pay_log ";
		$sql_cnt .= "where f_member_id='$f_member_id' ";
		$sql_cnt .= "and f_cert_status=0 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		//最新の決済１件の金額を取得
		$sql  = "select fk_pay_log_id,f_pay_money ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where f_member_id='$f_member_id' ";
//		$sql .= "and f_cert_status=0 ";
		$sql .= "order by fk_pay_log_id desc ";
		$sql .= "limit 1";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows == 0)
		{
			$recent_money	= 0;
			$post_money		= 0;
			
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		else
		{
			$recent_id		= mysql_result($result, 0, "fk_pay_log_id");
			$recent_money	= mysql_result($result, 0, "f_pay_money");
		}
		
		//最新の決済１件以外の金額合計を取得
		$sql  = "select sum(f_pay_money) ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where f_member_id='$f_member_id' ";
		$sql .= "and fk_pay_log_id!='$recent_id' ";
		$sql .= "and f_cert_status=0 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		$post_money = mysql_result($result, 0, 0);
		if($post_money == "")
		{
			$post_money = 0;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

//G.Chin 2010-08-06 add sta
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払端末取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetFPayPos($fk_pay_log_id,&$f_pay_pos)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_pay_pos ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where fk_pay_log_id='$fk_pay_log_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_pay_pos = mysql_result($result, 0, "f_pay_pos");
		}
		else
		{
			$f_pay_pos = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}
//G.Chin 2010-08-06 add end

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払履歴検索支払方法名取得関数															*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function SrcPayLogGetFShiharaiName($fk_shiharai_type_id, &$f_shiharai_name)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_shiharai_type
		$sql = "select fk_shiharai_id from auction.t_shiharai_type where fk_shiharai_type_id='$fk_shiharai_type_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_shiharai_id	= mysql_result($result, 0, "fk_shiharai_id");
		}
		else
		{
			$f_shiharai_name = "";
			return true;
		}
		
		//t_shiharai_plan
		$sql = "select f_shiharai_name from auction.t_shiharai_plan where fk_shiharai_id='$fk_shiharai_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_shiharai_name = mysql_result($result, 0, "f_shiharai_name");
		}
		else
		{
			$f_shiharai_name = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払履歴検索支払方法名取得関数	(落札商品入金用)										*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function SrcPayLogGetFShiharaiName_money($fk_shiharai_type_id, &$f_shiharai_name,$db)
	{
		//■ＤＢ接続
//	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//t_shiharai_type
		$sql = "select fk_shiharai_id from auction.t_shiharai_type where fk_shiharai_type_id='$fk_shiharai_type_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
//		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_shiharai_id	= mysql_result($result, 0, "fk_shiharai_id");
		}
		else
		{
			$f_shiharai_name = "";
			return true;
		}
		
		//t_shiharai_plan
		$sql = "select f_shiharai_name from auction.t_shiharai_plan where fk_shiharai_id='$fk_shiharai_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
//		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_shiharai_name = mysql_result($result, 0, "f_shiharai_name");
		}
		else
		{
			$f_shiharai_name = "";
		}
		
		//■ＤＢ切断
//	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員支払方法一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetMemShiharaiList(&$fk_shiharai_type_id,&$f_shiharai_name,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//$sql  = "select type.fk_shiharai_type_id,plan.f_shiharai_name ";
		$sql  = "select type.fk_shiharai_type_id,type.fk_shiharai_id,plan.f_shiharai_name ";
		$sql .= "from auction.t_shiharai_type as type,auction.t_shiharai_plan as plan ";
		$sql .= "where type.fk_shiharai_id=plan.fk_shiharai_id ";
		$sql .= "and type.f_term_type=0 ";
		$sql .= "and type.fk_shiharai_type=0 ";
		$sql .= "and plan.f_staff_use_flg=1 ";
		$sql .= "order by type.fk_shiharai_type_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			//$fk_shiharai_type_id[$i]	= mysql_result($result, $i, "fk_shiharai_type_id");
			$fk_shiharai_type_id[$i]	= mysql_result($result, $i, "fk_shiharai_type_id");
			$f_shiharai_name[$i]		= mysql_result($result, $i, "f_shiharai_name");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		商品支払額合計取得関数 2010-06-28 shoji																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetSumBuyProd($fk_member_id,&$sum_prod_pay)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select sum(f_pay_money) as shiharai from auction.t_pay_log ";
		$sql .= "where f_status=1 and f_cert_status=0 and f_member_id=$fk_member_id";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		$ret=mysql_fetch_array($result);
		if($ret['shiharai'] == NULL)
		{
			$sum_prod_pay=0;
		}
		else
		{
			$sum_prod_pay=$ret['shiharai'];
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

	function GetShiharaiId($fk_shiharai_type_id,&$fk_shiharai_id)
	{
		//■ＤＢ接続
		$db=db_connect();
		if($db == false)
		{
			exit;
		}
		$sql  = "select fk_shiharai_id from auction.t_shiharai_type ";
		$sql .= "where fk_shiharai_type_id = $fk_shiharai_type_id";
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		$ret=mysql_fetch_array($result);
		$fk_shiharai_id=$ret['fk_shiharai_id'];
		//■ＤＢ切断
		db_close($db);
		return true;
	}

/*==============================================================================
 * 重複払い確認関数
 =============================================================================*/
        function ChkProdBuy($pid,&$plid,$db)
        {
            $sql ="select fk_pay_log_id from auction.t_pay_log where f_cert_status =0
                    and fk_products_id = $pid";
            //echo $sql;
//            $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
            $result = mysql_query($sql, $db);
            if( $result == false )
            {
//		print "$sql<BR>\n";
//		//■ＤＢ切断
//                db_close($db);
                    return;
            }
            if(mysql_num_rows($result)==0)
            {
//                db_close($db);
//                return;
            }
            $ret=mysql_fetch_array($result);
            $plid = $ret['fk_pay_log_id'];
//            db_close($db);
            return;
            
        }

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		公式サイト決済一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTPublicSitePlanList($f_term_type,$fk_member_id,&$fk_publicsite_id,&$f_name,&$f_url,&$f_string,&$f_tm_stamp,&$link_tag,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_publicsite_id,f_name,f_url,f_string,f_tm_stamp ";
		$sql .= "from auction.t_publicsite_plan ";
		$sql .= "where f_stop=0 ";
		$sql .= "and ((f_term_type='$f_term_type') or (f_term_type=2)) ";
		$sql .= "order by fk_publicsite_id";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_publicsite_id[$i]	= mysql_result($result, $i, "fk_publicsite_id");
			$f_name[$i]				= mysql_result($result, $i, "f_name");
			$f_url[$i]				= mysql_result($result, $i, "f_url");
			$f_string[$i]			= mysql_result($result, $i, "f_string");
			$f_tm_stamp[$i]			= mysql_result($result, $i, "f_tm_stamp");
			
			//▼リンクタグ作成
			switch($fk_publicsite_id[$i])
			{
				case 1:
						//◆APROS
						$working_tag  = "";
						$working_tag .= "<a href='";
						$working_tag .= $f_url[$i];
						$working_tag .= "'>";
						$working_tag .= $f_string[$i];
						$working_tag .= "</a>";
						$working_tag = str_replace("%%MEMBER_ID%%", $fk_member_id, $working_tag);
			}
			
			//▼リンクタグ配列に格納
			$link_tag[$i] = $working_tag;
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		APROS決済関数																			*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PaymentApros($fk_member_id,$f_coin_add,$f_pay_money,$f_unique_code)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		//▼直近の購入後枚数を取得
		$sql  = "select f_coin_result from auction.t_pay_log ";
		$sql .= "where f_member_id='$fk_member_id' ";
		$sql .= "order by fk_pay_log_id desc limit 1 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_coin_result = mysql_result($result, 0, "f_coin_result");
		}
		else
		{
			$f_coin_result = 0;
		}
		
		$f_status = 0;									//支払タイプはコイン購入
		$f_member_id = $fk_member_id;
//G.Chin AWKT-507 2010-10-05 chg sta
//		$fk_shiharai_type_id = 0;						//支払方法は０固定
		
		//支払方法はMBでのコイン購入で固定
		//▼APROSの決済代行会社ID
		$sql  = "select fk_shiharai_agent_id ";
		$sql .= "from auction.t_shiharai_agent ";
		$sql .= "where f_name='APROS' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows > 0)
		{
			$fk_shiharai_agent_id = mysql_result($result, 0, "fk_shiharai_agent_id");
		}
		else
		{
			$fk_shiharai_agent_id = 0;
		}
		
		//▼APROSの支払方法ID
		$sql  = "select fk_shiharai_id ";
		$sql .= "from auction.t_shiharai_plan ";
		$sql .= "where f_shiharai_name='APROS' ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows > 0)
		{
			$fk_shiharai_id = mysql_result($result, 0, "fk_shiharai_id");
		}
		else
		{
			$fk_shiharai_id = 0;
		}
		
		//▼APROSの決済方法ID
		$sql  = "select fk_shiharai_type_id ";
		$sql .= "from auction.t_shiharai_type ";
		$sql .= "where fk_shiharai_agent_id='$fk_shiharai_agent_id' ";
		$sql .= "and fk_shiharai_id='$fk_shiharai_id' ";
		$sql .= "and fk_shiharai_type=0 and f_term_type=1 ";
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		if($rows > 0)
		{
			$fk_shiharai_type_id = mysql_result($result, 0, "fk_shiharai_type_id");
		}
		else
		{
			$fk_shiharai_type_id = 0;
		}
//G.Chin AWKT-507 2010-10-05 chg end
		$f_coin_result = $f_coin_result + $f_coin_add;
		$f_pay_pos = 1;									//支払端末はMB
		
		//▼会員の親紹介コードを取得
		$sql  = "select fk_parent_ad ";
		$sql .= "from auction.t_member_master ";
		$sql .= "where fk_member_id='$fk_member_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_adcode_id = mysql_result($result, 0, "fk_parent_ad");
		}
		else
		{
			$fk_adcode_id = 0;
		}
		
		$fk_products_id = 0;
		
		//▼支払履歴を作成
		$sql  = "insert into auction.t_pay_log ";
		$sql .= "(fk_pay_log_id,f_status,f_member_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,";
		$sql .= "f_pay_pos,fk_adcode_id,fk_products_id,f_unique_code,f_tm_stamp) ";
		$sql .= "values ";
		$sql .= "(0,'$f_status','$f_member_id','$f_pay_money','$f_coin_add','$fk_shiharai_type_id','$f_coin_result',";
		$sql .= "'$f_pay_pos','$fk_adcode_id','$fk_products_id','$f_unique_code',now()) ";
		
		//■ＳＱＬ実行
		$result_pl = mysql_query($sql, $db);
		if( $result_pl == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//▼広告主コードを取得
		$sql  = "select fk_admaster_id ";
		$sql .= "from auction.t_adcode ";
		$sql .= "where fk_adcode_id='$fk_adcode_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_admaster_id	= mysql_result($result, 0, "fk_admaster_id");
		}
		else
		{
			$fk_admaster_id	= 0;
		}
		
		//▼広告集計ＴＢＬを更新
		$sql  = "select count(*) ";
		$sql .= "from auction.t_stat_ad ";
		$sql .= "where f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') ";
		$sql .= "and fk_admaster_id='$fk_admaster_id' and f_type=0 ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_result($result, 0, 0);
		
		if($data_cnt > 0)
		{
	        $sql  = "update auction.t_stat_ad set f_mb_paycoin=f_mb_paycoin+$f_pay_money ";
			$sql .= "where f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') ";
			$sql .= "and fk_admaster_id='$fk_admaster_id' and f_type=0 ";
		}
		else
		{
			$sql  = "insert into auction.t_stat_ad ";
			$sql .= "(f_stat_dt,fk_admaster_id,f_type,f_mb_paycoin) ";
			$sql .= "values ";
			$sql .= "(date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),'$fk_admaster_id',0,'$f_pay_money')";
		}
		
		//■ＳＱＬ実行
		$result_sa = mysql_query($sql, $db);
		if( $result_sa == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//▼会員情報ＴＢＬのコイン枚数を加算
		$sql  = "update auction.t_member set f_coin=f_coin+$f_coin_add ";
		$sql .= "where fk_member_id='$fk_member_id' ";
		
		//■ＳＱＬ実行
		$result_m = mysql_query($sql, $db);
		if( $result_m == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		会員指定最新支払履歴情報取得関数														*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function SrcMemberGetRecentPayLogInfo($f_member_id,&$recent_id,&$recent_money,&$recent_pay_pos)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//最新の決済１件の金額を取得
		$sql  = "select fk_pay_log_id,f_pay_money,f_pay_pos ";
		$sql .= "from auction.t_pay_log ";
		$sql .= "where f_member_id='$f_member_id' ";
		$sql .= "and f_status!=2 ";
//		$sql .= "and f_cert_status=0 ";
		$sql .= "order by fk_pay_log_id desc ";
		$sql .= "limit 1";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows == 0)
		{
			$recent_id		= "";
			$recent_money	= 0;
			$recent_pay_pos	= "";
			
			//■ＤＢ切断
		    db_close($db);
			return true;
		}
		else
		{
			$recent_id		= mysql_result($result, 0, "fk_pay_log_id");
			$recent_money	= mysql_result($result, 0, "f_pay_money");
			$recent_pay_pos	= mysql_result($result, 0, "f_pay_pos");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		支払件数合計取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTPayLogCountSum($f_member_id,$where_str,&$payment_cnt,&$payment_sum)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//件数取得
		$sql_cnt  = "select count(*) from auction.t_pay_log ";
		$sql_cnt .= "where f_member_id='$f_member_id' ";
		$sql_cnt .= $where_str;
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$payment_cnt = mysql_result($result, 0, 0);
		
		//合計取得
		$sql_cnt  = "select sum(f_pay_money) from auction.t_pay_log ";
		$sql_cnt .= "where f_member_id='$f_member_id' ";
		$sql_cnt .= $where_str;
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$payment_sum = mysql_result($result, 0, 0);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}


?>