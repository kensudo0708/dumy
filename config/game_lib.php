<?php
/**
 * ■ゲーム機能用ビジネスロジック実装クラス
 *
 *
 * - 名前衝突を避けるためにクラスとしました。
 *
 * ////////// ※重要※ //////////
 *
 * 本ロジックファイルは管理ツール用のものです。
 * 仕様変更、バグフィックスの際には下記のサービス機能用ロジッククラスにも同様の修正を行う必要があります。
 * 両者は異なる実装ですが、同じ機能は同じ名称で実装しております。
 *
 * classes/model/GameModel.class.php
 * 
 * また、本クラスはサービス側の対応クラスと同一のクラス名となっておりますので、
 * 誤ってサービス側に読み込まないようお願い致します。クラス名重複エラーが発生する可能性があります。
 *
 * /////////////////////////////
 */

class GameModel {

    const PRIZE_ITEMS_LIMIT = 10;
    
    protected $gameId = NULL;
    protected $options = array(
        /**
         * ある行動に対して与えられるプレイ回数が定義されている項目の情報
         * t_game.f_play_count<n> = <t_game_log.f_play_type>
         */
        'play_type.daily'=>1,    // 毎日
        'play_type.coin'=>2,     // コイン購入
        'play_type.coin_pack'=>3,// コインパック購入
        //'play_type.friend'=>4,   // 友達紹介(コイン購入)
        /**
         * ボーナス効果種別
         */
        'bonus_type.coin'=>1,     // コイン購入
        'bonus_type.coin_pack'=>2,// コインパック購入
        /**
         * 定数的な値
         */
        'const.prize_items'=>4,        // 懸賞の数
        'const.t_pay_log.f_status'=>5, // t_pay_log.f_statusに設定する項目値。支払い区分＝ゲームの配当、を意味する
        /**
         * メッセージ領域の指定
         */
        'message.suspended'=>'f_info1', // 休止中メッセージ項目
        'message.empties'=>'f_info2',   // プレイ権利がない場合のメッセージ項目
    );

    // {{
    //     public functions
    // }}

    public function __construct( &$db, $gameId, $options=array() ) {
        $this->gameId = intval( $gameId );
        $this->options = array_merge( $this->options, $options );
        if ( $this->options['const.prize_items'] > self::PRIZE_ITEMS_LIMIT ) {
            $this->options['const.prize_items'] = self::PRIZE_ITEMS_LIMIT;
        }
        $this->game = $this->getGameRow( $db, $this->gameId );
    }

    /**
     * ゲーム（抽選）を行う
     *
     * @param t_game_log.rows
     */
    public function doPlay( &$db, &$gameLog ) {
        // 結果をハズレで初期化
        $gameLog['f_result_no'] = 0;
        $gameLog['f_name'] = $this->game['f_blank_name'];
        $gameLog['f_message'] = $this->game['f_blank_message'];
        $gameLog['f_item_a'] = $this->game['f_blank_item_a'];
        $gameLog['f_item_b'] = $this->game['f_blank_item_b'];
        $gameLog['f_item_c'] = $this->game['f_blank_item_c'];

        // ボーナス効果を取得
        $gameBonusLog = array();
        $list = $this->getAvailableBonusLogList( $db, $gameLog['f_member_id'] );
        if ( isset( $list[0] ) ) {
            $gameBonusLog = $list[0];
        }
        // 抽選懸賞一覧を取得
        $prizeList = $this->getAvailablePrizeList();
        $winsSummary = $this->getWinsSummary( $db );

        //trigger_error( '[懸賞設定]'.PHP_EOL.var_export($prizeList, TRUE) );
        //trigger_error( '[的中数]'.PHP_EOL.var_export($winsSummary, TRUE) );

        foreach ( $prizeList as $row ) {
            $row['f_value'] = floatval( $row['f_value'] );
            if ( !isset( $winsSummary[($row['f_result_no'])] ) ) {
                $winsSummary[($row['f_result_no'])] = 0;
            }
            if ( isset( $gameBonusLog[('f_value'.$row['f_result_no'])] ) ) {
                $row['f_value'] += floatval( $gameBonusLog[('f_value'.$row['f_result_no'])] );
            }
            // 当選確率を0-100%の範囲に補正
            if ( $row['f_value'] < 0 ) {
                $row['f_value'] = 0;
            } elseif ( $row['f_value'] > 100 ) {
                $row['f_value'] = 100;
            }
            // 抽選(0%はスキップ)
            $win = FALSE;
            if ( $row['f_value'] ) {
                /**
                 * （抽選タイプA）的中確率５０％以上
                 * 　・1～100までの範囲で乱数を生成し、生成値が的中確率未満ならアタリと判定
                 * 　・1%未満の設定は内部的には無視される
                 * 　
                 * （抽選タイプB）的中確率５０％以下
                 * 　・1～"四捨五入(100 / 的中確率)"した値の範囲で乱数を生成し、生成値が1ならアタリと判定
                 */
                if ( $row['f_value'] > 50 ) {
                    $value = mt_rand( 1, 100 );
                    if ( $value < round( $row['f_value'] ) ) {
                        $win = TRUE;
                    }
                } else {
                    $value = mt_rand( 1, round(100 / $row['f_value']) );
                    if ( $value == 1 ) {
                        $win = TRUE;
                    }
                }
            }
            if (
                $win
                // 的中本数上限チェック
                && (
                    $this->game[('f_limit'.$row['f_result_no'])] == 0  // 0は本数無制限
                    || $winsSummary[($row['f_result_no'])] < $this->game[('f_limit'.$row['f_result_no'])]
                )
            ) {
                $gameLog['f_result_no'] = $row['f_result_no'];
                $gameLog['f_value'] = $row['f_value'];
                $gameLog['f_name'] = $row['f_name'];
                $gameLog['f_message'] = $row['f_message'];
                $gameLog['f_item_a'] = $row['f_item_a'];
                $gameLog['f_item_b'] = $row['f_item_b'];
                $gameLog['f_item_c'] = $row['f_item_c'];
                break;
            }
        }
        if ( $gameLog['f_result_no'] != 0 && $gameBonusLog ) {
            // 当選時はボーナス効果を消費する
            $gameBonusLog['f_status'] = 1;
            $this->updateGameBonusLogRow( $db, $gameBonusLog );
        }
    }

    /**
     * ■払戻処理
     */
    public function doRefund( &$db, $gameLogId, $staffId ) {
        // ゲーム履歴取得
        $gameLog = $this->getGameLogRow( $db, $gameLogId, TRUE );
        if ( !$gameLog ) {
            trigger_error( 't_game_log(f_game_log_id='.$gameLogId.') is not found.' );
            return FALSE;
        }
        if ( $gameLog['f_refund_status'] == 0 ) {
            $gameLog['f_play_status'] = 1;
            $gameLog['f_refund_status'] = 1;
            $gameLog['f_refund_time'] = date('Y-m-d H:i:s');
            $gameLog['f_refund_type'] = 1;

            if ( $gameLog['f_item_a'] + $gameLog['f_item_b'] ) { // ※マイナス値が通過します
                // 会員情報取得
                $member = $this->getMemberRowForUpdate( $db, $gameLog['f_member_id'] );
                if ( !$member ) {
                    trigger_error('t_member(fk_member_id='.$gameLog['f_member_id'].') is not found.');
                    return FALSE;
                }
                // コイン数カウントアップ
                $member['f_coin'] += $gameLog['f_item_a'];
                $member['f_free_coin'] += $gameLog['f_item_b'];
                if ( !$this->updateMemberCoin( $db, $member ) ) {
                    return FALSE;
                }
                $coin_result = $member['f_coin'] + $member['f_free_coin'];
                $pay_log = $this->createPayLogRow( $gameLog, $coin_result, $staffId );
                if ( !$this->insertPayLogRow( $db, $pay_log ) ) {
                    return FALSE;
                }
                $gameLog['f_pay_log_id'] = $pay_log['f_pay_log_id'];
            }
            if ( !$this->updateGameLogRow( $db, $gameLog ) ) {
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * ゲームマスタ行の取得
     * - 主に管理機能用
     */
    public function getGameRow( &$db, $gameId=NULL ) {
        if ( is_null( $gameId ) ) {
            $gameId = $this->gameId;
        } else {
            $gameId = intval( $gameId );
        }
        $data = array();
        $sql = "SELECT * FROM auction.t_game WHERE f_game_id=".$this->_quote( $gameId );
        $res = mysql_query( $sql, $db );
        $data = FALSE;
        if ( $res !== FALSE ) {
            $data = mysql_fetch_assoc( $res );
            mysql_free_result( $res );
        } else {
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }

    /**
     * ゲームボーナスマスタ一覧の取得
     * - 主に管理機能用
     */
    public function getGameBonusList( &$db, $gameId=NULL, $tree=TRUE ) {
        if ( is_null( $gameId ) ) {
            $gameId = $this->gameId;
        } else {
            $gameId = intval( $gameId );
        }
        $sql = "
SELECT
  f_game_id
  ,f_bonus_id
  ,f_status
  ,f_priority_level
  ,f_bonus_type
  ,f_bonus_name
  ,f_trigger_value
  ,f_value1
  ,f_value2
  ,f_value3
  ,f_value4
  ,f_value5
  ,f_value6
  ,f_value7
  ,f_value8
  ,f_value9
  ,f_value10
  ,f_update_time
  ,f_update_staff_id
FROM
  auction.t_game_bonus
WHERE
  f_game_id=".$this->_quote( $gameId )."
ORDER BY f_bonus_type, f_bonus_id";
        $res = mysql_query($sql, $db);
        $data = array();
        if ( $res !== FALSE ) {
            $tmp = NULL;
            while ( $tmp = mysql_fetch_assoc( $res ) ) {
                // f_bonus_typeごとに木構造にフェッチ
                if ( $tree ) {
                    $data[($tmp['f_bonus_type'])][] = $tmp;
                } else {
                    $data[] = $tmp;
                }
            }
            mysql_free_result( $res );
        } else {
            $data = FALSE;
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }

    /**
     * ゲームボーナスマスタ一覧の取得
     * ・効果付与処理用
     */
    public function getGameBonusListByType( &$db, $bonusType ) {
        $sql = "
SELECT
  f_game_id
  ,f_bonus_id
  ,f_status
  ,f_priority_level
  ,f_bonus_type
  ,f_bonus_name
  ,f_trigger_value
  ,f_value1
  ,f_value2
  ,f_value3
  ,f_value4
  ,f_value5
  ,f_value6
  ,f_value7
  ,f_value8
  ,f_value9
  ,f_value10
  ,f_update_time
  ,f_update_staff_id
FROM
  auction.t_game_bonus
WHERE
  f_game_id=".$this->_quote( $this->gameId )."
  AND f_bonus_type=".$this->_quote( $bonusType )."
  AND f_status=0
ORDER BY f_trigger_value DESC
";
        $res = mysql_query($sql, $db);
        $data = array();
        if ( $res !== FALSE ) {
            $tmp = NULL;
            while ( $tmp = mysql_fetch_assoc( $res ) ) {
                $data[] = $tmp;
            }
            mysql_free_result( $res );
        } else {
            $data = FALSE;
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }

    public function getGameLogRow( &$db, $gameLogId, $isLock=FALSE ) {
        $sql = "
SELECT
  f_game_log_id
  ,f_member_id
  ,f_game_id
  ,f_play_status
  ,f_play_type
  ,f_result_no
  ,f_value
  ,f_name
  ,f_message
  ,f_item_a
  ,f_item_b
  ,f_item_c
  ,f_refund_status
  ,f_refund_type
  ,f_refund_request
  ,f_refund_time
  ,f_pay_log_id
  ,f_expire_time
  ,f_create_time
  ,f_update_time
FROM
  auction.t_game_log
WHERE
  f_game_log_id=".$this->_quote( $gameLogId );
        if ( $isLock ) {
            $sql .= " FOR UPDATE";
        }
        $res = mysql_query($sql, $db);
        $data = array();
        if ( $res !== FALSE ) {
            $data = mysql_fetch_assoc( $res );
            mysql_free_result( $res );
        } else {
            $data = FALSE;
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }

    /**
     * 開催状態を取得
     * @return bool
     */
    public function isHolding( &$db ) {
        $judge = FALSE;
        $game = $this->getGameRow( $db, $this->gameId );
        if ( isset( $game['f_status'] ) && $game['f_status'] == 0 ) {
            $judge = TRUE;
        }
        return $judge;
    }

    /**
     * ボーナス効果の付与
     */
    public function putGameBonus( &$db, $memberId, $value, $bonusType ) {
        $memberId= intval( $memberId );
        $bonusType = intval( $bonusType );
        $value = floatval( $value );

        // 開催休止中は処理しない
        if ( !$this->isHolding( $db ) ) {
            return NULL;
        }

        // ボーナス適用チェック
        $bonus = array();
        $gameBonus = $this->getGameBonusListByType( $db, $bonusType );
        foreach ( $gameBonus as $row ) {
            $row['f_trigger_value'] = floatval( $row['f_trigger_value'] );
            if ( $row['f_trigger_value'] <= $value ) {
                $bonus = $row;
                break;
            }
        }
        if ( $bonus ) {
            $gameBonusLog = $this->createGameBonusLogRow( $memberId, $bonus );
            $this->insertGameBonusLogRow( $db, $gameBonusLog );
        }
    }

    public function putGameBonusOfCoin( &$db, $memberId, $coin ) {
        $coin = intval( $coin );
        $this->putGameBonus( $db, $memberId, $coin, $this->options['bonus_type.coin'] );
    }

    public function putGameBonusOfCoinPack( &$db, $memberId, $coin ) {
        $coin = intval( $coin );
        $this->putGameBonus( $db, $memberId, $coin, $this->options['bonus_type.coin_pack'] );
    }

    /**
     * プレイ権利の付与
     */
    public function putPlayTicket( &$db, $memberId, $playType ) {
        $memberId = intval( $memberId );
        $playType = intval( $playType );

        // 開催休止中は処理しない
        if ( !$this->isHolding( $db ) ) {
            return NULL;
        }

        // 設定回数付与される
        $game = $this->getGameRow( $db );
        $count = intval( $game[('f_play_count'.$playType)] );
        for ( $i = 0; $i < $count; $i++ ) {
            $gameLog = $this->createGameLogRow( $memberId, $playType );
            $this->doPlay( $db, $gameLog );
            $this->insertGameLogRow( $db, $gameLog );
        }
    }
    public function putPlayTicketOfCoin( &$db, $memberId ) {
        $this->putPlayTicket( $db, $memberId, $this->options['play_type.coin']  );
    }
    public function putPlayTicketOfCoinPack( &$db, $memberId ) {
        $this->putPlayTicket( $db, $memberId, $this->options['play_type.coin_pack']  );
    }

    public function updateGame( &$db, $data, $staffId=NULL ) {
        if ( isset( $data['f_update_time'] )) {
            unset( $data['f_update_time'] );
        }
        $sql = "UPDATE auction.t_game SET ";
        $set = implode( "=%s, ", array_keys( $data ) )."=%s";
        $set = vsprintf( $set, array_map( array($this, '_quote'), array_values( $data ) ) );
        $set .= ",f_update_staff_id=".$this->_quote( $staffId );
        $sql .= $set." WHERE f_game_id=".$this->_quote( $data['f_game_id'] );
        if ( mysql_query( $sql, $db ) ) {
            return TRUE;
        } else {
            trigger_error( mysql_error( $db ) );
            return FALSE;
        }
    }

    public function updateGameBonus( &$db, $data, $staffId=NULL ) {
        if ( isset( $data['f_update_time'] )) {
            unset( $data['f_update_time'] );
        }
        $sql = "UPDATE auction.t_game_bonus SET ";
        $set = implode( "=%s, ", array_keys( $data ) )."=%s";
        $set = vsprintf( $set, array_map( array($this, '_quote'), array_values( $data ) ) );
        $set .= ",f_update_staff_id=".$this->_quote( $staffId );
        $sql .= $set." WHERE f_game_id=".$this->_quote( $data['f_game_id'] )." AND f_bonus_id=".$this->_quote( $data['f_bonus_id'] );
        if ( mysql_query( $sql, $db ) ) {
            return TRUE;
        } else {
            trigger_error( mysql_error( $db ) );
            return FALSE;
        }
    }
    
    public function updateGameBonusLogRow( $db, $gameBonusLog ) {
        // 更新対象外項目、キー項目を除外
        $id = $gameBonusLog['f_game_bonus_log_id'];
        unset( $gameBonusLog['f_game_bonus_log_id'] );
        unset( $gameBonusLog['f_update_time'] );
        $sql = "
UPDATE auction.t_game_bonus_log SET
  ".implode("=%s,", array_keys( $gameBonusLog ))."=%s
WHERE
  f_game_bonus_log_id=%s
";
        $params = array_map( array($this, '_quote'), array_values( $gameBonusLog ));
        $params[] = $this->_quote($id);
        $sql = vsprintf( $sql, $params );
        if ( mysql_query( $sql, $db ) ) {
            return TRUE;
        } else {
            trigger_error( mysql_error( $db ) );
            return FALSE;
        }
    }

    public function updateGameLogRow( &$db, $data ) {
        if ( isset( $data['f_update_time'] )) {
            unset( $data['f_update_time'] );
        }
        $sql = "UPDATE auction.t_game_log SET ";
        $set = implode( "=%s, ", array_keys( $data ) )."=%s";
        $set = vsprintf( $set, array_map( array($this, '_quote'), array_values( $data ) ) );
        $sql .= $set." WHERE f_game_log_id=".$this->_quote( $data['f_game_log_id'] );
        if ( mysql_query( $sql, $db ) ) {
            return TRUE;
        } else {
            trigger_error( mysql_error( $db ) );
            return FALSE;
        }
    }

    public function updateMemberCoin( &$db, $data ) {
        $sql = "UPDATE auction.t_member SET"
            ." f_coin=".$this->_quote( $data['f_coin'] ).", f_free_coin=".$this->_quote( $data['f_free_coin'] )
            ." WHERE fk_member_id=".$this->_quote( $data['fk_member_id'] );
        if ( mysql_query( $sql, $db ) ) {
            return TRUE;
        } else {
            trigger_error( mysql_error( $db ) );
            return FALSE;
        }
    }

    /**
     * t_gameレコード更新時に処置すべきバリデーション
     */
    public function validateGame( $data ) {
        for ( $i = 1; $i < 11; $i++ ) {
            // f_play_count<n>
            if ( $i < 6 ) {
                if ( isset( $data[('f_play_count').$i] ) ) {
                    $data[('f_play_count').$i] = intval( $data[('f_play_count').$i] );
                    if ( $data[('f_play_count').$i] < 0 ) {
                        $data[('f_play_count').$i] = 0;
                    }
                }
            }
            // f_item_a<n> , f_item_b<n>, f_item_c<n>
            if ( $i < 4) {
                if ( isset( $data[('f_item_a'.$i)] ) ) {
                    $data[('f_item_a'.$i)] = intval( $data[('f_item_a'.$i)] );
                    if ( $data[('f_item_a'.$i)] < 0 ) {
                        $data[('f_item_a'.$i)] = 0;
                    }
                }
                if ( isset( $data[('f_item_b'.$i)] ) ) {
                    $data[('f_item_b'.$i)] = intval( $data[('f_item_b'.$i)] );
                    if ( $data[('f_item_b'.$i)] < 0 ) {
                        $data[('f_item_b'.$i)] = 0;
                    }
                }
                if ( isset( $data[('f_item_c'.$i)] ) ) {
                    $data[('f_item_c'.$i)] = intval( $data[('f_item_c'.$i)] );
                    if ( $data[('f_item_c'.$i)] < 0 ) {
                        $data[('f_item_c'.$i)] = 0;
                    }
                }
            }
            // f_limit<n>
            if ( isset( $data[('f_limit'.$i)] ) ) {
                $data[('f_limit'.$i)] = intval( $data[('f_limit'.$i)] );
                if ( $data[('f_limit'.$i)] < 0 ) {
                    $data[('f_limit'.$i)] = 0;
                }
            }
        }
        // f_blank_item_a, f_blank_item_b, f_blank_item_c
        if ( isset( $data['f_blank_item_a'] ) ) {
            $data['f_blank_item_a'] = intval( $data['f_blank_item_a'] );
            if ( $data['f_blank_item_a'] < 0 ) {
                $data['f_blank_item_a'] = 0;
            }
        }
        if ( isset( $data['f_blank_item_b'] ) ) {
            $data['f_blank_item_b'] = intval( $data['f_blank_item_b'] );
            if ( $data['f_blank_item_b'] < 0 ) {
                $data['f_blank_item_b'] = 0;
            }
        }
        if ( isset( $data['f_blank_item_c'] ) ) {
            $data['f_blank_item_c'] = intval( $data['f_blank_item_c'] );
            if ( $data['f_blank_item_c'] < 0 ) {
                $data['f_blank_item_c'] = 0;
            }
        }
        return $data;
    }

    /**
     * t_game_bonusレコード更新時に措置すべきバリデーション
     */
    public function validateGameBonus( $data ) {
        return $data;
    }

    // {{
    //     public functions
    // }}

    protected function _quote( $value ) {
        if ( get_magic_quotes_gpc() ) {
            $value = stripslashes( $value );
        }
        if ( !is_numeric( $value ) ) {
            $value = "'".mysql_real_escape_string( $value )."'";
        }
        return $value;
    }

    /**
     * 新しくINSERTするためのt_game_bonus_logレコード連想配列を作成
     */
    protected function createGameBonusLogRow( $memberId, $gameBonus ) {
        $now = date('Y-m-d H:i:s');
        $data = array(
            'f_member_id'=>$memberId,
            'f_game_id'=>$this->gameId,
            'f_status'=>0,
            'f_priority_level'=>$gameBonus['f_priority_level'],
            'f_bonus_type'=>$gameBonus['f_bonus_type'],
            'f_bonus_name'=>$gameBonus['f_bonus_name'],
            'f_trigger_value'=>$gameBonus['f_trigger_value'],
            'f_create_time'=>$now,
            'f_update_time'=>$now,
        );
        for ( $i = 1; $i <= $this->options['const.prize_items']; $i++ ) {
            $data[('f_value'.$i)] = $gameBonus[('f_value'.$i)];
        }
        return $data;
    }

    /**
     * 新しくINSERTするためのt_game_logレコード連想配列を作成（！抽選前状態です）
     */
    protected function createGameLogRow( $memberId, $playType ) {
        $now = date('Y-m-d H:i:s');
        $row = array(
            // NULL値をインサートしたい項目はコメントアウト
            'f_member_id'=>$memberId,
            'f_game_id'=>$this->gameId,
            'f_play_status'=>0,
            'f_play_type'=>$playType,
            //'f_result_no'=>NULL,
            //'f_name'=>NULL,
            //'f_message'=>NULL,
            'f_item_a'=>0,
            'f_item_b'=>0,
            'f_item_c'=>0,
            //'f_refund_type'=>NULL,
            'f_refund_request'=>0,
            //'f_refund_time'=>NULL,
            //'f_pay_log_id'=>NULL,
            //'f_expire_time'=>NULL,
            'f_create_time'=>$now,
            'f_update_time'=>$now,
        );
        return $row;
    }

    protected function createPayLogRow( $gameLog, $coinResult, $staffId ) {
        $data = array(
            'f_status'=>5,
            'f_member_id'=>$gameLog['f_member_id'],
            'f_staff_id'=>$staffId,
            'f_pay_money'=>0,
            'f_coin_add'=>$gameLog['f_item_a'],
            'f_free_coin_add'=>$gameLog['f_item_b'],
            //'fk_shiharai_type_id'=>NULL,
            'f_coin_result'=>$coinResult,
            'f_min_price'=>0,
            'f_cert_status'=>0,
            //'f_cert_err'=>NULL,
            'f_pay_pos'=>0, // 管理機能操作なので0で固定してしまう
            'f_tm_stamp'=>$gameLog['f_refund_time'],
            'fk_adcode_id'=>0,
            'fk_products_id'=>0,
            'f_double'=>0,
            'f_unique_code'=>$gameLog['f_game_log_id'],

        );
        return $data;
    }


    /**
     * 有効なボーナス効果一覧を取得
     */
    public function getAvailableBonusLogList( &$db, $memberId ) {
        $data = array();
        $sql = "
SELECT * FROM
  auction.t_game_bonus_log
WHERE
  f_game_id=".$this->_quote( $this->gameId )."
  AND f_member_id=".$this->_quote( $memberId )."
  AND f_status=0     -- 未消費
ORDER BY
  f_priority_level ASC, f_game_bonus_log_id ASC
FOR UPDATE
";
        $res = mysql_query( $sql, $db );
        if ( $res !== FALSE) {
            $tmp = NULL;
            while ( $tmp = mysql_fetch_assoc( $res ) ) {
                $data[] = $tmp;
            }
            mysql_free_result( $res );
        } else {
            $data = FALSE;
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }
    /**
     * 抽選対象となる懸賞一覧を取得
     * @return array
     */
    public function getAvailablePrizeList() {
        $data = array();
        $sortIndex = array();
        for ( $i = 1; $i <= $this->options['const.prize_items']; $i++ ) {
            if ( isset( $this->game[('f_status'.$i)] ) && $this->game[('f_status'.$i)] == 0 ) {
                $sortIndex[($i)] = $this->game[('f_value'.$i)];
            }
        }
        asort( $sortIndex );
        foreach ( $sortIndex as $i=>$value ) {
            $data[] = array(
                'f_result_no'=>$i,
                'f_name'=>$this->game[('f_name'.$i)],
                'f_message'=>$this->game[('f_message'.$i)],
                'f_value'=>$this->game[('f_value'.$i)],
                'f_item_a'=>$this->game[('f_item_a'.$i)],
                'f_item_b'=>$this->game[('f_item_b'.$i)],
                'f_item_c'=>$this->game[('f_item_c'.$i)],
            );
        }
        return $data;
    }

    public function getMemberRowForUpdate( &$db, $memberId ) {
        $sql = "SELECT * FROM auction.t_member WHERE fk_member_id=".$this->_quote( $memberId )." FOR UPDATE";
        $res = mysql_query( $sql, $db );
        $data = array();
        if ( $res !== FALSE ) {
            $data = mysql_fetch_assoc( $res );
            mysql_free_result( $res );
        } else {
            $data = FALSE;
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }
    /**
     * 当選本数サマリを取得する
     * - default: today's summary
     */
    public function getWinsSummary( &$db, $from=NULL, $to=NULL ) {
        $data = array();
        if ( is_null( $from ) && is_null( $to ) ) {
            $today = date('Y-m-d');
            $from = $today.' 00:00:00';
            $to = $today.' 23:59:59';
        }
        $sql = "
SELECT f_result_no, COUNT( f_game_log_id ) AS `sum`
FROM
  auction.t_game_log
WHERE
  f_game_id=".$this->_quote( $this->gameId )."
  AND f_result_no<>0
  AND f_create_time BETWEEN ".$this->_quote( $from )." AND ".$this->_quote( $to )."
GROUP BY f_result_no
";
        $res = mysql_query( $sql, $db );
        if ( $res !== FALSE ) {
            $row = NULL;
            while ( $row = mysql_fetch_assoc( $res ) ) {
                $data[($row['f_result_no'])] = $row['sum'];
            }
            mysql_free_result( $res );
        } else {
            $data = FALSE;
            trigger_error( mysql_error( $db ) );
        }
        return $data;
    }

    public function insertGameBonusLogRow( &$db, &$data ) {
        $sql = "
INSERT INTO auction.t_game_bonus_log (
    ".implode(", ", array_keys( $data ))."
) VALUES (
    ".implode(", ", array_map(array($this, '_quote'), array_values( $data )) )."
)
";
        if ( mysql_query( $sql, $db ) ) {
            $data['f_game_bonus_log_id'] = mysql_insert_id();
            $msg = 'INSERT t_game_bonus_log('.$data['f_game_bonus_log_id'].')';
            error_log('success message:'.$msg);
            syslog( LOG_INFO, $msg );
            return TRUE;
        } else {
            $msg = mysql_error( $db );
            trigger_error( $msg );
            syslog( LOG_WARNING, $msg );
            error_log( $msg );
            return FALSE;
        }
    }

    public function insertGameLogRow( &$db, &$data ) {
        $sql = "
INSERT INTO auction.t_game_log (
    ".implode(", ", array_keys( $data ))."
) VALUES (
    ".implode(", ", array_map(array($this, '_quote'), array_values( $data )) )."
)
";
        if ( mysql_query( $sql, $db ) ) {
            $data['f_game_log_id'] = mysql_insert_id();
            $msg = 'INSERT t_game_log('.$data['f_game_log_id'].')';
            error_log('success message:'.$msg);
            syslog( LOG_INFO, $msg );
            return TRUE;
        } else {
            $msg = mysql_error( $db );
            trigger_error( $msg );
            error_log( $msg );
            syslog( LOG_WARNING, $msg );
            return FALSE;
        }
    }

    public function insertPayLogRow( &$db, &$data ) {
        $sql = "
INSERT INTO auction.t_pay_log (
    ".implode( ",", array_keys($data) )."
) VALUES (
    ".implode( ",", array_map(array($this,'_quote'), array_values($data)) )."
)
";
        if ( mysql_query( $sql, $db ) ) {
            $data['f_pay_log_id'] = mysql_insert_id();
            return TRUE;
        } else {
            trigger_error( mysql_error( $db ) );
            return FALSE;
        }
    }

}
?>
