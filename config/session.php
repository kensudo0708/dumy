<?php
/* 
 * アプリケーションの設定ファイル
 *Sessionの関連設定
 */
return  array (

/*    Sessionの関連設定     */
'SESSION_AUTO_START'=>true,      //セッション自動的に初期化する

/*    Cookieの関連設定     */
'COOKIE_PREFIX'=>'ark_',        //Cookieの接頭辞
'COOKIE_EXPIRE'=>86400,         //Cookieの有効期限
'COOKIE_PATH'=>'/',             //Cookieのパス
'COOKIE_DOMAIN'=>''            //Cookieの有効なドメイン

    
    
);

?>
