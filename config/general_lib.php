<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/09												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 一般的関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		テンプレート読込み関数														  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function ReadTemplate($tmplfile)
	{
		$filename = $tmplfile;
		$fsize    = filesize($filename);
		$fp       = fopen($filename, "rb");
		$out      = fread($fp, $fsize);
		fclose($fp);
		return $out;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		管理画面ページ表示関数														  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PrintAdminPage($TitleStr,$BodyStr)
	{       
		$indication = ReadTemplate(HOME_PATH."control/templ/basic.html");
		$indication = str_replace("%%TITLESTR%%", $TitleStr, $indication);
		$indication = str_replace("%%BODYSTR%%", $BodyStr, $indication);
		print $indication;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		管理画面メニューページ表示関数												  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PrintAdminMenuPage($BodyStr)
	{
		$indication = ReadTemplate(HOME_PATH."control/templ/basic_menu.html");
		$indication = str_replace("%%BODYSTR%%", $BodyStr, $indication);
		print $indication;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		管理画面入力ページ表示関数													  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PrintAdminInputPage($BodyStr)
	{
		$indication = ReadTemplate(HOME_PATH."control/templ/basic_input.html");
		$indication = str_replace("%%BODYSTR%%", $BodyStr, $indication);
		print $indication;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		管理画面広告主ページ表示関数												  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PrintAdminAdPage($TitleStr,$BodyStr)
	{
		$indication = ReadTemplate(HOME_PATH."ad/templ/basic.html");
		$indication = str_replace("%%TITLESTR%%", $TitleStr, $indication);
		$indication = str_replace("%%BODYSTR%%", $BodyStr, $indication);
		print $indication;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		管理画面広告主メニューページ表示関数										  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PrintAdminAdMenuPage($BodyStr)
	{
		$indication = ReadTemplate(HOME_PATH."ad/templ/basic_menu.html");
		$indication = str_replace("%%BODYSTR%%", $BodyStr, $indication);
		print $indication;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		管理画面広告主入力ページ表示関数											  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function PrintAdminAdInputPage($BodyStr)
	{
		$indication = ReadTemplate(HOME_PATH."ad/templ/basic_input.html");
		$indication = str_replace("%%BODYSTR%%", $BodyStr, $indication);
		print $indication;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		選択オブジェクト作成関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function MakeSelectObject($name, $id, $field, $data_cnt, $select_num, &$s_object,$coin =null)
	{
		$s_object  = "";
		$s_object .= "<select name='".$name."'>\n";
		//データ件数分リストを作成
		for($i=0; $i<$data_cnt; $i++)
		{
			if($id[$i] == $select_num || ($coin != null && $id[$i] == FREE_ID))
			{
				$s_object .= "<option value='".$id[$i]."' selected>".$field[$i]."</option>\n";
			}
			else
			{
				$s_object .= "<option value='".$id[$i]."'>".$field[$i]."</option>\n";
			}
		}
		$s_object .= "</select>\n";
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		日付文字列作成関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function MakeDateString($date, &$date_str)
	{
		$year	= substr($date, 0, 4);
		$month	= substr($date, 4, 2);
		$day	= substr($date, 6, 2);
		
		$date_str = $year."-".$month."-".$day;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		日付文字列変更関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function ChangeDateString($date, &$date_str)
	{
		$year	= substr($date, 0, 4);
		$month	= substr($date, 5, 2);
		$day	= substr($date, 8, 2);
		
		$date_str = $year.$month.$day;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		月末日取得関数																  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetMonthlimit($year, $month)
	{
		//"月"の判定
		switch ($month)
		{
			case 2:
				//"閏年"の判定
				if(($year % 4 == 0) && ($year % 100 != 0) && ($year % 400 == 0))
				{
					//"閏年"なら２９日
					$limit = 29;
				}
				else
				{
					//"閏年"でなければ２８日
					$limit = 28;
				}
				break;
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				//１，３，５，７，８，１０，１２月は３１日
				$limit = 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				//４，６，９，１１月は３０日
				$limit = 30;
				break;
			default:
				break;
		}
		return $limit;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		曜日文字列生成関数															  */
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function MakeWeekDayStr($date, &$string)
	{
		$w_year = substr($date, 0, 4);
		$w_month = substr($date, 5, 2);
		$w_day = substr($date, 8, 2);
		
		$timestamp = mktime("00","00","00",$w_month,$w_day,$w_year);
		$weekday = date("w", $timestamp);
		
		//"曜日"の判定
		switch ($weekday)
		{
			case 0:
					$string = "日";
					break;
			case 1:
					$string = "月";
					break;
			case 2:
					$string = "火";
					break;
			case 3:
					$string = "水";
					break;
			case 4:
					$string = "木";
					break;
			case 5:
					$string = "金";
					break;
			case 6:
					$string = "土";
					break;
			default:
				break;
		}
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		SQL実行件数取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetDataCount($sql_cnt,&$all_count)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		メルマガ送信先取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetMailSendList($sql,$limit,$offset,&$all_count,&$count,&$fk_member_id,&$f_handle,&$mail_to)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql_tmp="select count(*) from ($sql) as tmp where tmp.mail_to <>'' or tmp.f_handle<>''";
		//■ＳＱＬ実行
		$result = mysql_query($sql_tmp, $db);
		if( $result == false )
		{
			print "$sql_tmp<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		$sql_tmp = "select tmp.f_handle,tmp.mail_to from ($sql) as tmp where tmp.mail_to <>'' and tmp.f_handle <> '' limit $limit,$offset";
		
		$result = mysql_query($sql_tmp, $db);
		if( $result == false )
		{
			print "$sql_tmp<BR>\n";
			//■ＤＢ切断
			db_close($db);
			return false;
		}
		//echo $sql_tmp;
		$count = mysql_num_rows($result);
		
		for($i=0;$i<$count;$i++)
		{
			$f_handle[$i]= mysql_result($result, $i, 'f_handle');
			$mail_to[$i]= mysql_result($result, $i, 'mail_to');
			
			//▼会員ID取得
			$sql_id  = "select fk_member_id from auction.t_member_master ";
			$sql_id .= "where ((f_mail_address_pc='$mail_to[$i]') or (f_mail_address_mb='$mail_to[$i]')) ";
			
			$result2 = mysql_query($sql_id, $db);
			if( $result2 == false )
			{
				print "$sql_id<BR>\n";
				//■ＤＢ切断
				db_close($db);
				return false;
			}
			//echo $sql_id;
			$fk_member_id[$i] = mysql_result($result2, 0, 'fk_member_id');
		}
		//■ＤＢ切断
		db_close($db);
		return true;
	}

?>
