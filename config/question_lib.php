<?php

/*◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆*/
/*																				*/
/*		作成者		:	G.Chin													*/
/*		作成日		:	2010/04/27												*/
/*		修正日		:															*/
/*																				*/
/*◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇*/

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 質問関数ライブラリ **												*/
/*																			*/
/*		functionリスト														*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問一覧取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTQuestionList($inp_flag,$inp_category,$limit,$offset,&$fk_question_id,&$fk_question_category_id,&$f_subject,&$f_main,&$f_name,&$fk_member_id,&$f_mail_address,&$f_flag,&$f_recv_dt,&$f_check_dt,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//状態
		if($inp_flag == "")
		{
			$where_flag = "";
		}
		else
		{
			$where_flag = "and f_flag='$inp_flag' ";
		}
		
		//状態
		if($inp_category == "")
		{
			$where_category = "";
		}
		else
		{
			$where_category = "and fk_question_category_id='$inp_category' ";
		}
		
		$sql_cnt  = "select count(*) from auction.t_question ";
		$sql_cnt .= "where fk_question_id>0 ";
		$sql_cnt .= $where_flag;
		$sql_cnt .= $where_category;
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		$sql  = "select fk_question_id,fk_question_category_id,f_subject,f_main,f_name,";
		$sql .= "fk_member_id,f_mail_address,f_flag,f_recv_dt,f_check_dt,f_tm_stamp ";
		$sql .= "from auction.t_question ";
		$sql .= "where fk_question_id>0 ";
		$sql .= $where_flag;
		$sql .= $where_category;
		$sql .= "order by fk_question_id desc ";
		$sql .= "limit $limit offset $offset";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_question_id[$i]				= mysql_result($result, $i, "fk_question_id");
			$fk_question_category_id[$i]	= mysql_result($result, $i, "fk_question_category_id");
			$f_subject[$i]					= mysql_result($result, $i, "f_subject");
			$f_main[$i]						= mysql_result($result, $i, "f_main");
			$f_name[$i]						= mysql_result($result, $i, "f_name");
			$fk_member_id[$i]				= mysql_result($result, $i, "fk_member_id");
			$f_mail_address[$i]				= mysql_result($result, $i, "f_mail_address");
			$f_flag[$i]						= mysql_result($result, $i, "f_flag");
			$f_recv_dt[$i]					= mysql_result($result, $i, "f_recv_dt");
			$f_check_dt[$i]					= mysql_result($result, $i, "f_check_dt");
			$f_tm_stamp[$i]					= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問情報取得関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTQuestionInfo($fk_question_id,&$fk_question_category_id,&$f_subject,&$f_main,&$f_name,&$fk_member_id,&$f_mail_address,&$f_flag,&$f_recv_dt,&$f_check_dt,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_question_category_id,f_subject,f_main,f_name,";
		$sql .= "fk_member_id,f_mail_address,f_flag,f_recv_dt,f_check_dt,f_tm_stamp ";
		$sql .= "from auction.t_question where fk_question_id='$fk_question_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$fk_question_category_id	= mysql_result($result, 0, "fk_question_category_id");
			$f_subject					= mysql_result($result, 0, "f_subject");
			$f_main						= mysql_result($result, 0, "f_main");
			$f_name						= mysql_result($result, 0, "f_name");
			$fk_member_id				= mysql_result($result, 0, "fk_member_id");
			$f_mail_address				= mysql_result($result, 0, "f_mail_address");
			$f_flag						= mysql_result($result, 0, "f_flag");
			$f_recv_dt					= mysql_result($result, 0, "f_recv_dt");
			$f_check_dt					= mysql_result($result, 0, "f_check_dt");
			$f_tm_stamp					= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$fk_question_category_id	= "";
			$f_subject					= "";
			$f_main						= "";
			$f_name						= "";
			$fk_member_id				= "";
			$f_mail_address				= "";
			$f_flag						= "";
			$f_recv_dt					= "";
			$f_check_dt					= "";
			$f_tm_stamp					= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問状態更新関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTQuestionFlag($fk_question_id, $f_flag)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql = "update auction.t_question set f_flag='$f_flag',f_tm_stamp=now() where fk_question_id='$fk_question_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問カテゴリ一覧取得関数																*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTQuestionCategoryList(&$fk_question_category_id,&$f_category_name,&$f_tm_stamp,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select fk_question_category_id,f_category_name,f_tm_stamp ";
		$sql .= "from auction.t_question_category ";
		$sql .= "order by fk_question_category_id ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_question_category_id[$i]	= mysql_result($result, $i, "fk_question_category_id");
			$f_category_name[$i]			= mysql_result($result, $i, "f_category_name");
			$f_tm_stamp[$i]					= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問カテゴリ名取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTQuestionCategoryName($fk_question_category_id, &$f_category_name)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql = "select f_category_name from auction.t_question_category where fk_question_category_id='$fk_question_category_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_category_name = mysql_result($result, 0, "f_category_name");
		}
		else
		{
			$f_category_name = "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問カテゴリ登録関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTQuestionCategory($f_category_name)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql = "insert into auction.t_question_category values (0,'$f_category_name',now()) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問カテゴリ更新関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function UpdateTQuestionCategory($fk_question_category_id, $f_category_name)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql  = "update auction.t_question_category set f_category_name='$f_category_name' ";
		$sql .= "where fk_question_category_id='$fk_question_category_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問カテゴリ削除関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function DeleteTQuestionCategory($fk_question_category_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql = "delete from auction.t_question_category where fk_question_category_id='$fk_question_category_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問回答登録関数																		*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function RegistTQuestionAnswer($f_question_id,$f_subject,$f_main,$f_name,$f_staff_id)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		//■トランザクション開始
		mysql_query("BEGIN", $db);
		
		$sql  = "insert into auction.t_question_answer ";
		$sql .= "(fk_question_answer_id,f_question_id,f_subject,f_main,f_name,f_staff_id,f_tm_stamp) ";
		$sql .= "values (0,'$f_question_id','$f_subject','$f_main','$f_name','$f_staff_id',now()) ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ロールバック
			mysql_query("ROLLBACK", $db);
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		
		//■コミット
		mysql_query("COMMIT", $db);
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問回答一覧取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTQuestionAnswerList($limit,$offset,$f_question_id,&$fk_question_answer_id,&$f_subject,&$f_main,&$f_name,&$f_staff_id,&$f_tm_stamp,&$all_count,&$data_cnt)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql_cnt  = "select count(*) from auction.t_question_answer ";
		$sql_cnt .= "where f_question_id='$f_question_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql_cnt, $db);
		if( $result == false )
		{
			print "$sql_cnt<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■全件数
		$all_count = mysql_result($result, 0, 0);
		
		$sql  = "select fk_question_answer_id,f_subject,f_main,f_name,f_staff_id,f_tm_stamp ";
		$sql .= "from auction.t_question_answer ";
		$sql .= "where f_question_id='$f_question_id' ";
		$sql .= "order by fk_question_answer_id,f_tm_stamp desc ";
		$sql .= "limit $limit offset $offset";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$data_cnt = mysql_num_rows($result);
		
		for($i=0; $i<$data_cnt; $i++)
		{
			$fk_question_answer_id[$i]	= mysql_result($result, $i, "fk_question_answer_id");
			$f_subject[$i]				= mysql_result($result, $i, "f_subject");
			$f_main[$i]					= mysql_result($result, $i, "f_main");
			$f_name[$i]					= mysql_result($result, $i, "f_name");
			$f_staff_id[$i]				= mysql_result($result, $i, "f_staff_id");
			$f_tm_stamp[$i]				= mysql_result($result, $i, "f_tm_stamp");
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
/*		質問回答情報取得関数																	*/
/*━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━*/
	
	function GetTQuestionAnswerInfo($fk_question_answer_id,&$f_question_id,&$f_subject,&$f_main,&$f_name,&$f_staff_id,&$f_tm_stamp)
	{
		//■ＤＢ接続
	    $db=db_connect();
	    if($db == false)
	    {
	        exit;
	    }
		
		$sql  = "select f_question_id,f_subject,f_main,f_name,f_staff_id,f_tm_stamp ";
		$sql .= "from auction.t_question_answer ";
		$sql .= "where fk_question_answer_id='$fk_question_answer_id' ";
		
		//■ＳＱＬ実行
		$result = mysql_query($sql, $db);
		if( $result == false )
		{
			print "$sql<BR>\n";
			//■ＤＢ切断
		    db_close($db);
			return false;
		}
		//■データ件数
		$rows = mysql_num_rows($result);
		
		if($rows > 0)
		{
			$f_question_id	= mysql_result($result, 0, "f_question_id");
			$f_subject		= mysql_result($result, 0, "f_subject");
			$f_main			= mysql_result($result, 0, "f_main");
			$f_name			= mysql_result($result, 0, "f_name");
			$f_staff_id		= mysql_result($result, 0, "f_staff_id");
			$f_tm_stamp		= mysql_result($result, 0, "f_tm_stamp");
		}
		else
		{
			$f_question_id	= "";
			$f_subject		= "";
			$f_main			= "";
			$f_name			= "";
			$f_staff_id		= "";
			$f_tm_stamp		= "";
		}
		
		//■ＤＢ切断
	    db_close($db);
		return true;
	}

?>