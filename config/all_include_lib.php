<?php

/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
/*																			*/
/*	** 総合的関数ライブラリ **												*/
/*																			*/
/*		functionリスト												    	*/
/*																			*/
/*																			*/
/*			all_include_lib.inc		⇒	common全ての関数をincludeします		*/
/*																			*/
/*																			*/
/*																			*/
/*																			*/
/*																			*/
/*∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇∇*/
	
	function i18n_convert($str, $to_encoding, $from_encoding  = null)
	{
		if (is_null($from_encoding))
		{
			return mb_convert_encoding($str, $to_encoding, "SJIS");
		}
		else
		{
			return mb_convert_encoding($str, $to_encoding, $from_encoding);
		}
	}
	
	include "../lib/server.php";
	include "db_lib.php";
	include "general_lib.php";
	include "product_lib.php";
	include "member_lib.php";
	include "payment_lib.php";
	include "staff_lib.php";
	include "sale_lib.php";
	include "mail_lib.php";
	include "adcode_lib.php";
	include "question_lib.php";
	include "news_lib.php";
	include "bid_lib.php";

?>