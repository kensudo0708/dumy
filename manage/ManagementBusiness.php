<?php
/*******************************************************************************
 * モージュールID ：ManagementBusiness.php
 * モジュール名称  ：
 * 機能概要　　 　： 管理ツールにビジネスロジックを提供する
 * 改訂履歴　　 　：Rev1.0 2010/12/18 新規kensudo
 ******************************************************************************/
require_once("./inc/mySql_inc.php");               //MySQL操作クラス
require_once("./inc/Func_inc.php");                //共通関数
require_once("./common/CommonFunc.php");           //共通関数クラス

//---------------------------------------
//　機能概要:セッション変数をクリアする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_clearSession() {
	unset($_SESSION['PROGRAMNAME']);
	unset($_SESSION['ADM_USER_ID']);
	unset($_SESSION['WEBNAME']);
	unset($_SESSION['FILENAME']);
	return;
}
//---------------------------------------
//　機能概要:
//　引数　　　:
//　戻り値　　:
// 備考          :未使用
//--------------------------------------
function PHP_DispUpdateForm() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrProgramName;
	
	$lstrMsg                = "";
	$gstrOnLoad = "onLoad=\"document.forms[0].KeyYear.focus();\"";
	include($gstrInputPage);
	exit;
}
//---------------------------------------
//　機能概要:ログイン処理をコントロールする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_DispLoginForm() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrLoginPage;
	global $gstrMenuPage;
	global $gstrHostPage;
	global $gstrDataPage;
	global $gstrDirectory;;
	global $gstrProgram;
	
	$gstrOnload = "onLoad=\"document.forms[0].userid.focus();\"";
	include($gstrLoginPage);
	exit;
}
//---------------------------------------
//　機能概要:番組選択メニューをコントロールする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_DispMenuForm() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrLoginPage;
	global $gstrMenuPage;
	global $gstrHostPage;
	global $gstrDataPage;
	global $gstrDirectory;;
	global $gstrProgramName;
	global $gstrWebName;
	$lstrWebName = $_SESSION['WEBNAME'];
	$gstrWebName  = "<a href=\"http://".$lstrWebName."\">".$lstrWebName."</a>";
	
	include($gstrMenuPage);
	exit;
}
//---------------------------------------
//　機能概要:ホスト選択処理をコントロールする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_DispHostForm() {
	global $gstrHost;
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrLoginPage;
	global $gstrMenuPage;
	global $gstrHostPage;
	global $gstrDataPage;
	global $gstrDirectory;
	global $gstrProgramName;
	global $gstrWebName,$lstrWebName;
	$lstrWebName = $_SESSION['WEBNAME'];
	$gstrWebName  = "<a href=\"http://".$lstrWebName."\">".$lstrWebName."</a>";
	
	$gobjCommon = new ClCommonFunc();
	$gstrHost = $gobjCommon->PHP_createHost($gstrDirectory);
	//echo "host   ".$gstrHost;
	
	include($gstrHostPage);
	exit;
}
//---------------------------------------
//　機能概要:データ選択メニューをコントロールする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_DispDataForm() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrLoginPage;
	global $gstrMenuPage;
	global $gstrHostPage;
	global $gstrDataPage;
	global $gstrDirectory;;
	global $gstrData;
	global $gstrProgramName;
	global $gstrContents;
	global $gstrWebName;
	global $gstrFileName;
	$lstrWebName = $_SESSION['WEBNAME'];
	@ $gstrFilenName = $_SESSION['FILENAME'];
	
	$gstrWebName  = "<a href=\"http://".$lstrWebName."\">".$lstrWebName."</a>";
	
	$gobjCommon = new ClCommonFunc();
	$_SESSION['FILENAME'] = substr($gstrDirectory,strrpos($gstrDirectory,"/")+1);
	//echo "    name   ".$_SESSION['FILENAME'];
	$gstrData = $gobjCommon->PHP_createData($gstrDirectory);
	$gstrProgramName = $_SESSION['PROGRAMNAME'];
	if (is_file($gstrDirectory)){
		$gstrContents = $gobjCommon->PHP_getContents($gstrDirectory);
	}
	include($gstrDataPage);
	exit;
}
//---------------------------------------
//　機能概要:データ選択メニューをコントロールする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_DispContentsForm() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrLoginPage;
	global $gstrMenuPage;
	global $gstrHostPage;
	global $gstrDataPage;
	global $gstrDirectory;;
	global $gstrData;
	global $gstrProgramName;
	global $gstrContents;
	global $gstrWebName;
	global $gstrFileName;
	global $gstrContentsPage;
	$lstrWebName = $_SESSION['WEBNAME'];
	$gstrFilenName = $_SESSION['FILENAME'];
	
	$gstrWebName  = "<a href=\"http://".$lstrWebName."\">".$lstrWebName."</a>";
	
	$gobjCommon = new ClCommonFunc();
	$_SESSION['FILENAME'] = substr($gstrDirectory,strrpos($gstrDirectory,"/")+1);
	//echo "    name   ".$_SESSION['FILENAME'];
	$gstrData = $gobjCommon->PHP_createData($gstrDirectory);
	$gstrProgramName = $_SESSION['PROGRAMNAME'];
	if (is_file($gstrDirectory)){
		$gstrContents = $gobjCommon->PHP_getContents($gstrDirectory);
	}
	include($gstrContentsPage);
	exit;
}
//---------------------------------------
//　機能概要:アパッチデーモンを再スタートする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_UpdateServerInfo() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrProgramName;
    global $gstrDataPage;
    
    //system('/home/ark/arkuser1/scp.sh > /dev/null 2>&1', $retval);
    system('cd /home/backup;tar zcvf log1.tar.gz log > /dev/null 2>&1', $retval);
    //echo $retval;
    $gstrOnLoad = "onload=\"alert('各サーバの情報を更新しました');\"";
    PHP_DispMenuForm();
	exit;
}
//---------------------------------------
//　機能概要:ログ情報をダウンロードする
//　引数　　　:
//　戻り値　　:
// 備考          :
//--------------------------------------
function PHP_Download() {
	global $gstrUserID;      //ＩＤ
    global $gstrInputPage;
    global $gstrController;
    global $gstrOnLoad;
    global $gstrYear;
    global $gstrMonth;
    global $gstrDay;
    global $gstrStartHour;
    global $gstrStartMinute;
    global $gstrEndHour;
    global $gstrEndMinute;
    global $gstrProgramName;
    
    include($gstrInputPage);
	exit;	
}
?>