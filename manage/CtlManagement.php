<?php
/*******************************************************************************
 * モージュールID ：Ctlmanagement.php
 * モジュール名称  ：
 * 機能概要　　 　：
 * 改訂履歴　　 　：Rev1.0 2010/12/18 新規kensudo
 ******************************************************************************/
require_once("./inc/mySql_inc.php");              //MySQL操作クラス
require_once("./inc/Func_inc.php");
require_once("./ManagementBusiness.php");         //本機能のビジネスロジック
require_once("./common/CommonFunc.php");          //共通関数クラス
//ログインチェック
PHP_LoginChk();
//権限チェック
//require_once("./inc/Administrator_inc.php");  //管理者情報クラス
//$gobjAuth = new ClAdministratorAuthority;
//if (!$gobjAuth->HaveAuthMemberRegist($_SESSION['ADM_USER_AUTHORITY'])) {
	//権限なし
//	$gstrErrNo = "AuthErr";
	//メッセージ表示後トップ画面へ移動
//	include("./html/errmsg.html"); 
//	exit;
//} 
 
//変数初期化
$strMsg      = "";
$gstrAction  = "";
$strOnLoad   = "";
$lintMode    = "";
//セッション変数からユーザ情報を取得する(ログイン時に取得する)
$gstrUserID      = $_SESSION['ADM_USER_ID'];      //ＩＤ
$gstrInputPage   = "html/management.html";
$gstrController  = "CtlManagement.php";

//フォームデータ取得
if (isset($_REQUEST["MODE"])) {
	$lintMode               = $_REQUEST["MODE"];    //処理モード
	if(isset($_REQUEST['MYPAGE'])) {
		$_SESSION['MYPAGE'] = $_REQUEST['H_MYPAGE'];
	}
	if(isset($_REQUEST['KeyYear'])) {
		$gstrKeyYear = $_REQUEST['KeyYear'];
	}
	if(isset($_REQUEST['KeyMonth'])) {
		$gstrKeyMonth = $_REQUEST['KeyMonth'];
	}
	if(isset($_REQUEST['KeyDay'])) {
		$gstrKeyDay = $_REQUEST['KeyDay'];
	}
	if(isset($_REQUEST['KeyStartHour'])) {
		$gstrStartHour = $_REQUEST['KeyStartHour'];
	}
	if(isset($_REQUEST['KeyStartMinute'])) {
		$gstrStartMinute = $_REQUEST['KeyStartMinute'];
	}
	if(isset($_REQUEST['KeyEndHour'])) {
		$gstrEndHour = $_REQUEST['KeyEndHour'];
	}
	if(isset($_REQUEST['KeyEndMinute'])) {
		$gstrEndMinute = $_REQUEST['KeyEndMinute'];
	}
}else {
	$lintMode           = "1";
	unset($_SESSION['COMPLETE']);
	unset($_SESSION['MYPAGE']);
	unset($_SESSION['ADM_USER_ID']);
	$gstrYear  = date('Y');
	$gstrMonth = date('n');
	$gstrDay   = date('d');
}
/**
 * 処理開始
 */
switch ($lintMode) {
	case "1":       //グラフ更新表示
		unset($_SESSION['COMPLETE']);
		PHP_DispUpdateForm();
		break;
	case "2":       //アパッチデーモンリスタート
		if ($_SESSION['COMPLETE'] <> '1'){
			$_SESSION['COMPLETE'] = '1';
			PHP_RestartHttpd();
		}else{
			$gstrOnLoad = "onload=\"alert('アパッチデーモンは既に再スタートされました');document.forms[0].KeyYear.focus();\"";
			include($gstrInputPage);
		}
		break;
	case "3":       //ダウンロード
		PHP_Download();
		break;
}
exit;

?>
