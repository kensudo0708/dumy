<?php
/*******************************************************************************
 * モージュールID ：ClSqlMember.php
 * モジュール名称  ：会員テーブル(T_MEMBER)操作
 * 機能概要　　 　：
 * 改訂履歴　　 　：Rev1.0 2010/12/21 新規kensudo
 ******************************************************************************/
require_once("./common/Constant.php");   //グローバル定数
//include_once(C_ENCRYPT_KEY_FILE);         //個人情報暗号化キー

class ClSqlMember {
	private  $lstrSQL = "";  //ローカル変数SQL組立エリア
	
  	//---------------------------------------
    //　機能概要:該当の会員が存在するか否かをチェックする
    //　引数　　　:$pstrID                  会員番号
    //　戻り値　　:$lstrSQL                 該当の会員が存在するか否かをチェックするSQL文
    // 備考          :
    //---------------------------------------
	public function PHP_IsValidMember($pstrID) {
    	$this->lstrSQL = "";
    	
		$this->lstrSQL  = "SELECT COUNT(ID) FROM T_MEMBER";
		$this->lstrSQL .= " WHERE ID = '".PHP_MetaEscape($pstrID)."'";
  		return $this->lstrSQL;
  	}
  	
	function __construct() {
  	}
	function __destruct() {
  	}
	
}
?>