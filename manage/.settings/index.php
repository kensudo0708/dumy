<?php
/*******************************************************************************
 * モージュールID ：index.php
 * モジュール名称  ：メニュー制御
 * 機能概要　　 　：
 * 改訂履歴　　 　：Rev1.0 2010/12/21 新規kensudo
 ******************************************************************************/
require_once("./inc/mySql_inc.php");               //mySQL操作クラス
require_once("./inc/Func_inc.php");                //共通ファンクションクラス
require_once("./common/CommonFunc.php");           //共通関数クラス
require_once("./sql/ClSqlMember.php");             //会員テーブルSQL操作関数

//変数初期化
global $gstrOnLoad;
global $gobjCommon;

$lstrMsg       = "";         //エラーメッセージ
$lintMode      = "";         //処理モード
$gstrOnload    = "";         //オンロードイベント
$ldateNow = date("Y/m/d");

//セッションチェック
if (session_id() == "") { 
	//セッション開始
	session_start();
}
if (isset($_SESSION['ADM_USER_ID'])) {
	//セッションが存在
	$lstrUserID = $_SESSION['ADM_USER_ID'];
	If ($lstrUserID != "") {
		//ログイン認証済み
		$lintMode = "2";
	}
}
/**
 * フォームデータ取得
 */
if (isset($_POST["mode"])) {
	$lintMode          = $_POST["mode"];
	if ( $lintMode == "1" ) {
		$lstrUserID    = $_POST["userid"];      //ログインＩＤ
		$lstrUserPWD   = $_POST["passwd"];      //パスワード
	} else {
	}
}else{
	$lintMode = "3";
}
try {
	switch ($lintMode) {
		case "1":
			/*
			$lobjDB   = new mySqlDB();                            //MySqlオブジェクト
			$lobjSql  = new ClSqlmember();                        //会員テーブルSQL文オブジェクト
			$lstrSql  = $lobjSql->PHP_IsValidMember($lstrUserID); //会員チェックSQL文
			$lobjRS   = $lobjDB->Query($lstrSql);
	    
			if ($larrRows = $lobjDB->FetchRow($lobjRS)) {
				//共通関数のインスタンス作成
				$lobjComm = new clCommonFunc();
                if ($larrRows['PASS'] == md5($lstrUserPWD)){ 
                   	$_SESSION['ADM_USER_ID']     = $lstrUserID;
					$_SESSION['USER_PASSWD']     = $lstrUserPWD;
					$gstrOnload = "";
					$gobjCommon = new clCommonFunc();
					include("./html/menu.html");
					exit;
				} else {
					//パスワード不一致
					$lstrMsg = "IDまたはパスワードが正しくありません。 ";
				} 
			} else {
				//IDなし
				$lstrMsg = "当システムへのアクセスが許可されていません。";
			}
        	if ($lstrMsg != "") {
				throw new MyCustomException($lstrMsg);
			}
			*/
			//ログイン画面表示
			include("./html/login.html");
			$gstrOnload = "onLoad=\"document.forms[0].userid.focus();\"";
			break;
		case "2":
			$gobjCommon = new clCommonFunc();
			//echo $gobjCommon->PHP_createMenu();
			//include("./html/menu.html");
			include("./index2.php");
			break;
		case "3":
			//ログアウト
			// セッション変数を全て解除する
			$_SESSION = array();
			unset($_SESSION['ADM_USER_ID']);
			//セッションを破棄する
			session_destroy();
			//ログイン画面表示
			$gstrOnload = "onLoad=\"document.forms[0].userid.focus();\"";
			include("./html/login.html");
			
			break;
		default:
			//ログアウト
			// セッション変数を全て解除する
			$_SESSION = array();
			unset($_SESSION['ADM_USER_ID']);
			//セッションを破棄する
			session_destroy();
			//ログイン画面表示
			$gstrOnload = "onLoad=\"document.forms[0].userid.focus();\"";
			include("./html/login.html");
			
			break;
		}
	}catch(MyCustomException $e){
		$gstrOnload = "onLoad=\"alert('".$lstrMsg."');\"";
		include("./html/login.html");
		PHP_Errlog("index.php",$e."   ＩＤ  => ".$lstrUserID."  ++  ".$lstrUserPWD);
    }
	catch(Exception $e){
		$gstrOnload = "onLoad=\"alert('".$lstrMsg."');\"";
		include("./html/login.html");
		PHP_Errlog("index.php",$e);
    }
	
exit;
?>