// JavaScript Document

// グローバル変数宣言
var dir;
var num;

// 第一階層判定
if(id == "etc") {
	if(str.match(/\/news\//)) {
		id = "news"
	} else if(str.match(/\/alert\//)) {
		id = "alert"
	} else if(str.match(/\/awards\//)) {
		id = "awards"
	} else if(str.match(/\/community\//)) {
		id = "community"
	} else if(str.match(/\/entertainments\//)) {
		id = "entertainments"
	} else if(str.match(/\/calendar\//)) {
		id = "calendar"
	} else if(str.match(/\/award_bunka\//)) {
		id = "bunka"
	} else if(str.match(/\/award_mijinko\//)) {
		id = "mijinko"
	} else if(str.match(/\/award_gijutsu\//)) {
		id = "gijutsu"
	}
}

// IDとフォルダ名の紐付け
var id_array = new Array();
id_array["#navAbout"] = "aboutus"
id_array["#navEvent"] = "event"
id_array["#navPublic"] = "publications"
id_array["#navJoin"] = "joining"
id_array["#navMember"] = "member"
id_array["#navContact"] = "contact"
id_array["news"] = "news"
id_array["alert"] = "alert"
id_array["awards"] = "awards"
id_array["community"] = "community"
id_array["calendar"] = "calendar"
id_array["entertainments"] = "entertainments"
id_array["bunka"] = "award_bunka"
id_array["mijinko"] = "award_mijinko"
id_array["gijutsu"] = "award_gijutsu"

// 階層取得関数
function get_dir(fld) {
	var re = new RegExp(".+\/" + fld + "\/");
	dir = str.replace(re, "");
	if(dir.match(/\//)) {
		num = dir.match(/\//g).length;
	} else {
		num = 0;
	}
	dir = "";
	for(var i = 0; i < num; i++) {
		dir += "../";
	}
}

// 階層取得
if(id != "etc") get_dir(id_array[id]);

// 第一階層パンくず取得関数
function first_crumbs(ca, txt) {
	if((num == 0) && (str.match(/index\.html/))) {
		ca += txt;
	} else {
		ca += "<a href='" + dir + "index.html'>" + txt + "</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;";
	}
	return ca;
}

// 第二階層パンくず取得関数
function second_crumbs(ca, fld, txt) {
	var reg = new RegExp(".+\/" + fld + "\/");
	str = str.replace(reg, "");
	if((str.match(/index\.html/)) && (!str.match(/\//))) {
		ca += txt;
	} else {
		dir = dir.replace(/\.\.\//, "");
		ca += "<a href='" + dir + "index.html'>" + txt + "</a>&nbsp;&nbsp;&gt;&nbsp;";
	}
	return ca;
}

// パンくず文字列
var crumbs_array = new Array();
crumbs_array[id] = "<a href='" + dir + "../index.html'>HOME</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;";
switch(id) {
	case "#navAbout":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "日本水環境学会とは");
		if(str.match(/\/objective\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "objective", "目的と活動内容");
		} else if(str.match(/\/composition\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "composition", "会員構成");
		} else if(str.match(/\/history\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "history", "沿革");
		} else if(str.match(/\/organization\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "organization", "組織");
		} else if(str.match(/\/subdivision\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "subdivision", "支部");
		} else if(str.match(/\/research\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "research", "研究委員会");
		} else if(str.match(/\/meet_coope\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "meet_coope", "産官学協力委員会・水環境懇話会");
		} else if(str.match(/\/prize\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "prize", "表彰");
		} else if(str.match(/\/information\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "information", "情報公開資料");
		} else if(str.match(/\/associations\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "associations", "関連団体");
		}
		break;
	case "#navEvent":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "イベント");
		if(str.match(/\/lectures\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "lectures", "年会");
		} else if(str.match(/\/symposium\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "symposium", "シンポジウム");
		} else if(str.match(/\/seminars\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "seminars", "セミナー・市民セミナー");
		}
		break;
	case "#navPublic":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "学術雑誌・出版物");
		if(str.match(/\/journals\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "journals", "水環境学会誌");
		} else if(str.match(/\/guidelines\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "guidelines", "水環境学会誌に論文投稿をお考えの方へ");
		} else if(str.match(/\/features\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "features", "水環境学会誌特集企画の募集");
		} else if(str.match(/\/jwet\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "jwet", "Journal of Water and Environment Technology");
		} else if(str.match(/\/instructions\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "instructions", "JWET: Instructions to authors");
		} else if(str.match(/\/data\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "data", "出版物");
		}
		break;
	case "#navJoin":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "入会のご案内");
		break;
	case "#navMember":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "会員専用ページ");
		break;
	case "#navContact":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "お問い合わせ");
		break;
	case "news":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "更新履歴");
		break;
	case "alert":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "関連雑誌・新聞のコンテンツアラート");
		break;
	case "awards":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "学生会員・国内会員・international researcherへの各種表彰");
		if(str.match(/\/kurita\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "kurita", "クリタ賞");
		} else if(str.match(/\/idea\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "idea", "いであ賞");
		} else if(str.match(/\/organo\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "organo", "博士研究奨励賞（オルガノ賞）");
		} else if(str.match(/\/lion\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "lion", "ライオン賞");
		} else if(str.match(/\/metawater\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "metawater", "年間優秀論文賞（メタウォーター賞）");
		}
		break;
	case "community":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "人材育成・社会貢献");
		if(str.match(/\/booklet\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "booklet", "水生生物調査法に関する冊子等頒布のご案内");
		} else if(str.match(/\/training\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "training", "次世代の研究者・技術者の育成");
		} else if(str.match(/\/interview\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "interview", "活躍する若手研究者・若手技術者");
		} else if(str.match(/\/businessguidance\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "businessguidance", "水環境ビジネスガイダンス（就職活動予定の学生へ）");
		} else if(str.match(/\/citizenseminar\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "citizenseminar", "セミナー・市民セミナー");
		} else if(str.match(/\/environment\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "environment", "日本の水環境");
		} else if(str.match(/\/recruit\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "recruit", "公募情報");
		}
		break;
	case "calendar":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "イベントカレンダー");
		if(str.match(/\/jswe\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "jswe", "本会・他学会水環境関連行事");
		} else if(str.match(/\/iwa\//)) {
			crumbs_array[id] = second_crumbs(crumbs_array[id], "iwa", "IWA関連行事");
		}
		break;
	case "entertainments":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "年会開催予定");
		break;
	case "bunka":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "水環境文化賞（地域で活動されている皆様へ）");
		break;
	case "mijinko":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "みじん子賞（ジュニアの皆様へ）");
		break;
	case "gijutsu":
		crumbs_array[id] = first_crumbs(crumbs_array[id], "技術賞");
		break;
	default:
}

// パンくず書き出し関数
function crumbs() {
	document.write(crumbs_array[id]);
}
