// JavaScript Document

// グローバル変数宣言
var st1;
var st2;
var h1;
var h2;
var id;
var str = new String(document.URL);
str = str.replace(/\\/g, "/");

// ID取得
if(str.match(/\/aboutus\//)) {
	id = "#navAbout";
} else if(str.match(/\/event\//)) {
	id = "#navEvent";
} else if(str.match(/\/publications\//)) {
	id = "#navPublic";
} else if(str.match(/\/joining\//)) {
	id = "#navJoin";
} else if(str.match(/\/member\//)) {
	id = "#navMember";
} else if(str.match(/\/contact\//)) {
	id = "#navContact";
} else {
	id = "etc";
}

$(function(){
	// 初期化
	if(id != "etc") $(id + ">a").addClass("setbgi");
	// プルダウン一段目
	$("div.box_nav>ul>li").hover(
		function() {
			if(id != "#" + $(this).attr("id")) {
				$(">a", $(this)).addClass("setbgi");
			}
			h1 = $(">ul>li", $(this)).size();
			h1 = h1 * 40;
			if(h1 < 240) h1 = 240;
			var tmp = $(this);
			st1 = setTimeout(
				function() {
					$(">ul", tmp).slideDown(h1);
					clearTimeout(st1);
				}, 300
			);
		},
		function() {
			clearTimeout(st1);
			if(id != "#" + $(this).attr("id")) {
				$(">a", $(this)).removeClass("setbgi");
			}
			var tmp = $(this);
			var st1b = setTimeout(
				function() {
					$(">ul", tmp).slideUp(h1);
					clearTimeout(st1b);
				}, h1
			);
		}
	);
	// プルダウン二段目
	$("li.pd").hover(
		function() {
			$(">a", $(this)).css({"background-color":"#7ed0d6"});
			h2 = $(">ul>li", $(this)).size();
			h2 = h2 * 40;
			if(h2 < 240) h2 = 240;
			var tmp = $(this);
			st2 = setTimeout(
				function() {
					$(">ul", tmp).slideDown(h2);
					clearTimeout(st2);
				}, 300
			);
		},
		function() {
			clearTimeout(st2);
			$(">a", $(this)).css({"background-color":"#cbe6ec"});
			var tmp = $(this);
			var st2b = setTimeout(
				function() {
					$(">ul", tmp).slideUp(h2);
					clearTimeout(st2b);
				}, h2
			);
		}
	);
});
