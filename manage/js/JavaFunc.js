//
// ブランク以外の入力有無チェック
//
function JS_BlankCheck(pstr) {
	var i, str;
	for ( i = 0; i < pstr.length; i++ ) {
		str = pstr.charAt(i);
		if ( str != " " && str != "　") {
			return true;
		}
	}
	return false;
}

//
// 全角カタカナのみチェック
//
function JS_KanaCheck(pstr) {
	var i, str, strKana;
	strKana = 'アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲン';
	strKana += 'ヴガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォヵヶッャュョヮー　';
	for ( i = 0; i < pstr.length; i++ ) {
		str = pstr.charAt(i);
		if ( strKana.indexOf(str) < 0 ) {
			return false;
		}
	}
	return true;
}

//パラメータの英数字チェック
function JS_EngCheck(pstr) {
	var i, str, strEng;
	strEng = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	strEng += '0123456789!"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~ ';
	for ( i = 0; i < pstr.length; i++ ) {
		str = pstr.charAt(i);
		if ( strEng.indexOf(str) < 0 ) {
			return false;
		}
	}
	return true;
}

//パラメータの数字チェック
function JS_NumberCheck(pstr) {
	var i, str, strNumber;
	strNumber = '0123456789';
	for ( i = 0; i < pstr.length; i++ ) {
		str = pstr.charAt(i);
		if ( strNumber.indexOf(str) < 0 ) {
			return false;
		}
	}
	return true;
}

//パラメータで渡された月の妥当性チェック
function JS_MonthCheck(pstr) {
	var i, str, strNumber;
	if ( pstr.length == 1 && pstr > 0 ) {
		return true;
	}
	if ( pstr.length == 2 ) {
		if ( pstr.charAt(0) == 0 ) {
			if ( pstr.charAt(1) > 0 ) {
				return true;
			}
		} else if ( pstr > 9 && pstr < 13 ) {
			return true;
		}
	}
	return false;
}

//パラメータで渡された年月日の妥当性チェック
function JS_DateCheck(pstrYear, pstrMonth, pstrDay) {
	var lintYear, lintMonth, lintDay;
	var larrEndDay;
	lintYear = eval(pstrYear);
	if ( lintYear < 1900 || lintYear > 2100 ) {
		return false;
	}
	lintMonth = eval(pstrMonth);
	if ( lintMonth < 1 || lintMonth > 12 ) {
		return false;
	}
	lintDay = eval(pstrDay);
	if ( lintDay < 1 || lintDay > 31) {
		return false;
	}
	if ( lintYear % 4 == 0 && lintYear % 100 != 0 || lintYear % 400 == 0 ) {
		larrEndDay = new Array(0,31,29,31,30,31,30,31,31,30,31,30,31);
	} else {
		larrEndDay = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31);
	}
	if ( lintDay > larrEndDay[lintMonth] ) {
		return false;
	}
	return true;
}

//郵便番号の妥当性チェック
function JS_ZipCheck(pstr) {
	var i, str, strNumber;
	strNumber = '0123456789';
	if ( pstr.length != 8 ) return false;
	for ( i = 0; i < pstr.length; i++ ) {
		str = pstr.charAt(i);
		if ( i != 3 ) {
			if ( strNumber.indexOf(str) < 0 ) {
				return false;
			}
		} else {
			if ( pstr.charAt(i) != '-' ) {
				return false;
			}
		}
	}
	return true;
}

//
//金額（数字とカンマ）チェック
//
function JS_KingakuCheck(s) {
	var n_char = '0123456789,';
	var p;
	var i;
	var num;

	num = false;
	for (i = 0; i < s.length; i++) {
		p = s.charAt(i);
		if (n_char.indexOf(p) < 0) {
			return false;
		} else if (p != ',') {
			num = true;
		}
	}
//	return true;
	return num;
}

//Emailアドレスチェック
function JS_EmailCheck(s) {
	var e_char,i,p
	e_char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@_-';
	for (i = 0; i < s.length; i++) {
		p = s.charAt(i);
		if (e_char.indexOf(p) < 0) {
			return false;
		}
	}
	if (s.indexOf('@') < 0) {
		return false;
	}
	if (s.charAt(s.length-1) == '@' || s.charAt(s.length-1) == '.') {
		return false;
	}
	return true;
}

//英数字記号以外の文字が含まれているかチェック（ID用）
function JS_EisuCheck_ID(s) {
	var e_char,i,p
	e_char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ#$%&-.=_';
	for (i = 0; i < s.length; i++) {
		p = s.charAt(i);
		if (e_char.indexOf(p) < 0) {
			return false;
		}
	}
	return true;
}

//数字と*以外の文字が含まれているかチェック（IPアドレス用）
function JS_NumCheck_IP(s) {
	var h_char,z_char,i,p
	h_char = '0123456789*';
	for (i = 0; i < s.length; i++) {
		p = s.charAt(i);
		if (h_char.indexOf(p) < 0) {
			return false;
		}
	}
	return true;
}

//数字と-以外の文字が含まれているかチェック（郵便番号,電話番号用）
function JS_NumCheck_TEL(s) {
	var h_char,z_char,i,p;
//	h_char = '0123456789-';
	h_char = '0123456789-+';	// 2010/01/06 国番号(+)も許可
	for (i = 0; i < s.length; i++) {
		p = s.charAt(i);
		if (h_char.indexOf(p) < 0) {
			return false;
		}
	}
	if (s.charAt(0) == '-' || s.charAt(s.length-1) == '-') {
		return false;
	}
	return true;
}

/*----------------------------------*/
/*バイト数チェック					*/
/*	引数：pStr	チェック対象文字列	*/
/*		  plength		最大バイト数*/
/*----------------------------------*/
function JS_getlength(pStr,plength) {
	var li_len = 0;
	var li_cnt;
	var ls_Tmp = "";
	for (var li_cnt = 0; li_cnt < pStr.length; li_cnt++) {
		ls_Tmp = pStr.charCodeAt(li_cnt);
		// Unicode  : 0x20 ～ 0x7f, 0xff61 ～ 0xff9f
		if((0x20 <= ls_Tmp && ls_Tmp <= 0x7f) || (0xff61 <= ls_Tmp && ls_Tmp <= 0xff9f)){
			li_len += 1;
		}else{
			li_len += 2;
		}
	}
	if (plength < li_len){

		return false;
	}
	return true;
}

/*----------------------------------*/
/*	日付の妥当性チェック処理		*/
/*	引数:1.日付 (yyyy/mm/dd)		*/
/*----------------------------------*/
function JS_IsDateChk(pDATE){
	var ls_val;
	var li_ymd;
	var i;
	var li_yy;
	var li_mm;
	var li_dd;

	ls_val = pDATE;
	if (ls_val.length<5){ return false }

	li_ymd = ls_val.split("/");
	if (li_ymd.length<3){ return false }

	for(i=0;i<li_ymd.length;i++){
		if(li_ymd[i].length<=0){return false}
		if(isNaN(li_ymd[i]) == true){return false}
	}

	li_yy = eval(li_ymd[0]);
	if(li_yy <=1980 || li_yy >2100){return false}

	li_mm = eval(li_ymd[1]);
	if(li_mm <=0 || li_mm >12){return false}

	li_dd = eval(li_ymd[2]);
	if(li_dd <=0 || li_dd >31){return false}

	if(li_yy%4==0 && li_yy%100 !=0 || li_yy%400==0){
		li_ed = new Array(0,31,29,31,30,31,30,31,31,30,31,30,31);
	}
	else{
		li_ed = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31);
	}
	if (li_dd > li_ed[li_mm]){return false}
	return true;
}

/*----------------------------------*/
/*	時間の妥当性チェック処理		*/
/*	引数:1.時間 (hh:mi)		*/
/*----------------------------------*/
function JS_IsTimeChk(pTIME){
	var ls_val;
	var li_hhmi;
	var i;
	var li_hh;
	var li_mi;

	ls_val = pTIME;
	if (ls_val.length<3){ return false }

	li_hhmi = ls_val.split(":");
	if (li_hhmi.length<2){ return false }

	for(i=0;i<li_hhmi.length;i++){
		if(li_hhmi[i].length<=0){return false}
		if(isNaN(li_hhmi[i]) == true){return false}
	}

	li_hh = eval(li_hhmi[0]);
	if(li_hh <0 || li_hh >23){return false}

	li_mi = eval(li_hhmi[1]);
	if(li_mi <0 || li_mi >59){return false}

	return true;
}

//金額にカンマを付加する
function JS_addCommaToKingaku(s) {
	var i,ss,s1,p,r;
	r = '';
	ss = s;

	ss = String(ss);

	while (ss.match(",") != null){
		ss = ss.replace(',','');
	}

	if (ss.charAt(0) == '-') {
		ss = ss.substring(1,ss.length);
		r = '-';
	}
	if (ss.length <= 3) {
		return s;
	}
	if (ss.indexOf(',') >= 0) {
		return s;
	}
	s1 = ss.split('.');
	if (s1[0].length <= 3 || s1[0].indexOf(',') >= 0) {
		return s;
	}

	for (i=0; i<s1[0].length; i++) {
		p = s1[0].charAt(i);
		if (i > 0 && ((s1[0].length-i) % 3) == 0) {
			r = r + ',';
		} 
		r = r + p;
	}
	if (s1.length > 1) {
		r = r + '.' + s1[1];
	}
	//s = r;
	return r;
}

//全角→半角変換
function JS_ToHankakuNum(motoText)
{
	var han = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ()*!"#$%&\'+,-./:;<=>?@[\]^_`{|}~ ';
	var zen = '０１２３４５６７８９ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ（）＊！”＃＄％＆￥’＋，－．／：；＜＝＞？＠［￥］＾＿｀｛｜｝￣　';
	var str = "";
	var i;

	for (i=0; i<motoText.length; i++)
	{
		c = motoText.charAt(i);
		n = zen.indexOf(c,0);
		if (n >= 0) c = han.charAt(n);
		str += c;
	}
	return str;
}

//パスワード生成(2009/11/18)
function JS_MakePassword() {
	var lstrPassword = "";
	var lintLen;

	lstrPassData = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	//乱数の発生文字列格納
	lintLen = lstrPassData.length;
	for (i=0; i < 10; i++) {
		lintRnd = Math.random() * lintLen;					//0～文字列の長さまでの乱数を発生
		lstrPassword += lstrPassData.charAt(lintRnd);
	}
	return lstrPassword;
}

//フィールドの背景を赤にする
function JS_chgBkColorRed(pelement) {
	var bgcolor = '#ff6666';
	var i;

	if (pelement.length) {
		for (i=0; i < pelement.length; i++) {
			pelement[i].style.backgroundColor = bgcolor;
		}
	} else {
		pelement.style.backgroundColor = bgcolor;
	}
}

//フィールドの背景を透過にする
function JS_chgBkColorDefault(pelement) {
	var bgcolor = 'transparent';

	if (pelement.length) {
		for (i=0; i < pelement.length; i++) {
			pelement[i].style.backgroundColor = bgcolor;
		}
	} else {
		pelement.style.backgroundColor = bgcolor;
	}
}
function m_win(url,windowname,width,height) {
	 var features="location=no, menubar=no, status=yes, scrollbars=yes, resizable=yes, toolbar=no";
	 if (width) {
	  if (window.screen.width > width)
	   features+=", left="+(window.screen.width-width)/2;
	  else width=window.screen.width;
	  features+=", width="+width;
	 }
	 if (height) {
	  if (window.screen.height > height)
	   features+=", top="+(window.screen.height-height)/2;
	  else height=window.screen.height;
	  features+=", height="+height;
	 }
	 window.open(url,windowname,features);
	}
