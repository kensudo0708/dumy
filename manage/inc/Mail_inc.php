<?php
/**
 * メール送信情報クラス
 */
class ClSendmail {
	var $strLanguage;
	var $strEncoding;

    function __construct() {
    	$this->strLanguage = "Japanese";
    	$this->strEncoding = "UTF-8";
    }
	//-----------------------------
	//メール送信
	//-----------------------------
    function Send($pstrFrom,$pstrTo,$pstrCc,$pstrSubject,$pstrBody,$pstrBcc="") {
    	//ヘッダ設定
    	$lstrHeaders = "From: ".$pstrFrom."\n";
    	//CC:
    	if ($pstrCc != "") {
	    	$lstrHeaders .= "Cc: ".$pstrCc."\n";
    	}
    	//BCC:  2009/12/25追加
    	if ($pstrBcc != "") {
	    	$lstrHeaders .= "Bcc: ".$pstrBcc."\n";
    	}
		//ランゲージ設定
		mb_language($this->strLanguage);
		//エンコーディング設定
		mb_internal_encoding($this->strEncoding);

		$lstrSubject = $pstrSubject;
		$lstrEncode = mb_detect_encoding($lstrSubject,"UTF-8,sjis-win,SJIS,eucJP-win");
		if ($lstrEncode != "UTF-8") {
			//UTF-8でなければ変換
			$lstrSubject = mb_convert_encoding($lstrSubject, $lstrEncode, "UTF-8");
		}
		$lstrBody = $pstrBody;
		$lstrEncode = mb_detect_encoding($lstrBody,"UTF-8,sjis-win,SJIS,eucJP-win");
		if ($lstrEncode != "UTF-8") {
			//UTF-8でなければ変換
			$lstrBody = mb_convert_encoding($lstrBody, $lstrEncode, "UTF-8");
		}

		//メール送信
		$lblResult = mb_send_mail($pstrTo,$lstrSubject,$lstrBody,$lstrHeaders);
	    $this->Log($lblResult,$pstrFrom,$pstrTo,$pstrCc,$pstrBcc,$lstrSubject,$lstrBody);
		return $lblResult;
    }
	//-----------------------------
	//ログ採取
	//-----------------------------
    function Log($plblResult,$pstrFrom,$pstrTo,$pstrCc,$pstrBcc,$pstrSubject,$pstrBody) {
		$lstrLogFile = "/home/backup/log/maillog-".date('Ymd').".txt";
		//日付
		$lstrDate = date('Y/m/d H:i:s');
		//送信結果
		$lstrResult = ($plblResult) ? "送信成功" : "送信失敗";

		@ $lobjFile = fopen($lstrLogFile.$from,"a+");
        @ fputs($lobjFile,"日付: ".$lstrDate."\n"); 
        @ fputs($lobjFile,"結果: ".$lstrResult."\n"); 
        @ fputs($lobjFile,"From: ".$pstrFrom."\n"); 
        @ fputs($lobjFile,"  To: ".$pstrTo."\n"); 
        @ fputs($lobjFile,"  Cc: ".$pstrCc."\n"); 
        @ fputs($lobjFile," Bcc: ".$pstrBcc."\n"); 
        @ fputs($lobjFile,"件名: ".$pstrSubject."\n"); 
        @ fputs($lobjFile,$pstrBody."\n"); 
        @ fputs($lobjFile,"\n"); 
        
		@ fclose($lobjFile);
	}
}
?>
