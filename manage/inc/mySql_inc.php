<?php
class mySqlDB {
    var $link_id;
    var $pconnect;
    var $result_id;
    var $last_error_no;
    var $last_error_msg;
 
    function __construct($server = 'localhost',$user = 'fis-mm-com',$password = 'C2nwogmB',$database = 'fis-mm-com00001',$presistency = true){
        $this->pconnect = $presistency;

        if($presistency){
            $this->link_id = mysql_pconnect($server,$user,$password);
        }
        else{
            $this->link_id = mysql_connect($server,$user,$password);
        }
        if($this->link_id){
            if($database != ""){
                $db = mysql_select_db($database,$this->link_id);
                if(!$db){
                    mysql_close($this->link_id);
                    return false;
                }
                return $this->link_id;
            }
        }
        return false;
    }
    function __destruct(){
        if($this->link_id){
            if(!$this->pconnect){
                mysql_close($this->link_id);
            }
        }
    }
    function Query($sql){
        if($sql != ""){
            $this->result_id = mysql_query($sql, $this->link_id);
            return $this->result_id;
        }
    }
	function max_packet_init(){
  		return mysql_query('SET GLOBAL max_allowed_packet=16*1024*1024',$this->link_id);
	}
    function FreeQuery($query_id = 0){

        $query_id = $this->GetQueryID($query_id);

        if ($query_id){
            mysql_free_result($query_id);
        }
        else{
            return false;
        }
        return true;
    }
    function FetchRow($query_id = 0){

        $query_id = $this->GetQueryID($query_id);

        if($query_id){
            $row = mysql_fetch_array($query_id);
            return $row;
        }
        else{
            return false;
        }
    }
    function FetchDynadset($query_id = 0){

        $query_id = $this->GetQueryID($query_id);

        if($query_id){
            while($row = mysql_fetch_array($query_id)){
                $dynaset[] = $row;
            }
            return $dynaset;
        }
        else{
            return false;
        }
    }
    function ExecuteSQL($sql){
        if($sql != ""){
            $this->result_id = mysql_query($sql, $this->link_id);
            return $this->result_id;
        }
    }
    function GetRowCount($query_id = 0)
    {
        $query_id = $this->GetQueryID($query_id);
        
        if($query_id){
            $result = mysql_num_rows($query_id);
            return $result;
        }
        else{
            return false;
        }
    }
    function GetExecRowCount()
    {
        if($this->link_id){
            $result = mysql_affected_rows($this->link_id);
            return $result;
        }
        else{
            return false;
        }
    }
    function GetQueryID($query_id){
        if(!$query_id){
            $id = $this->result_id;
        }
        else{
            $id = $query_id;
        }
        return $id;
    }
    function Move($query_id = 0,$rownum){

        $query_id = $this->GetQueryID($query_id);

        if($query_id){
            $result = mysql_data_seek($query_id, $rownum);
            return $result;
        }
        else{
            return false;
        }
    }
    function GetCurrentID(){
        if($this->link_id){
            $next_id = mysql_insert_id($this->link_id);
            return $next_id;
        }
        else{
            return false;
        }
    }    
    function GetLastError()
    {
        $error["code"] = mysql_errno($this->link_id);
        $error["message"] = mysql_error($this->link_id);

        return $error;
    }
    function BeginTrans()
    {
        $this->result_id = mysql_query('START TRANSACTION', $this->link_id);
        return $this->result_id;
    }
    function CommitTrans()
    {
        $this->result_id = mysql_query('COMMIT', $this->link_id);
        return $this->result_id;
    }
    function Rollback()
    {
        $this->result_id = mysql_query('ROLLBACK', $this->link_id);
        return $this->result_id;
    }
}
?>
