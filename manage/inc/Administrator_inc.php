<?php
include_once("/home/fis-mm-com/common/constant_key_inc.php");      //個人情報暗号化キー（開発）
//include_once("/home/jswe-or-jp/common/constant_key_inc.php");      //個人情報暗号化キー（本番）
include_once("./inc/SystemMail_inc.php");

//暗号化キー(AES)
//define('C_AES_ADM_KEY','JSWE ADMIN PASSWORD');	//管理者用キー
//権限フラグ
define('C_AuthAdministrator'      ,0);	//管理者権限管理機能
define('C_AuthYearPay'            ,1);	//年会費管理機能
define('C_AuthSignature'          ,2);	//請求書印字設定管理機能
define('C_AuthMemberRegist'       ,3);	//入会受付管理機能
define('C_AuthWithdrawalRegist'   ,4);	//退会受付管理機能
define('C_AuthMemberManage'       ,5);	//会員情報管理機能
define('C_AuthSymposiumManage'    ,6);	//講演会管理機能
define('C_AuthPresenterRegist'    ,7);	//発表受付機能
define('C_AuthParticipantRegist'  ,8);	//参加受付機能
define('C_AuthPresentNoImport'    ,9);	//講演番号インポート機能
//define('C_AuthPaymentInfoImport'  ,10);	//入金情報インポート機能
define('C_AuthPaymentInfoManage'  ,10);	//入金情報管理機能
define('C_AuthInvoiceInfoDownload',11);	//請求書用データダウンロード機能
define('C_AuthEnvelopesDownload'  ,12);	//宛名用データダウンロード機能
define('C_AuthMemberInfoDownload' ,13);	//会員データダウンロード機能
class ClAdministratorAuthority {
	var $intNo;					//個数
	var $intMaxNo;				//最大個数
	var $arrCd;
	var $arrNm;

	//コンストラクタ(PHP4)
	function ClAdministratorAuthority() {
		$this->__construct();
	}
	//コンストラクタ(PHP5)
	function __construct() {
		$this->arrCd = array(C_AuthAdministrator,C_AuthYearPay,C_AuthSignature,C_AuthMemberRegist,C_AuthWithdrawalRegist,C_AuthMemberManage,C_AuthSymposiumManage,C_AuthPresenterRegist,C_AuthParticipantRegist,C_AuthPresentNoImport,C_AuthPaymentInfoManage,C_AuthInvoiceInfoDownload,C_AuthEnvelopesDownload,C_AuthMemberInfoDownload);
		$this->arrNm = array('管理者権限管理','年会費管理','請求書印字設定管理','入会受付管理','退会受付管理','会員情報管理','講演会管理','発表受付','参加受付','講演番号インポート','入金情報管理','請求書用データダウンロード','宛名用データダウンロード','会員データダウンロード');
		$this->intNo = count($this->arrCd);
		$this->intMaxNo = 32;
	}
	//権限文字列取得
	function GetAuthNames($pstrAuthData) {
		$lstrAuthData = '';
		for ($i=0; $i < $this->intNo; $i++) {
			$lstrAuth = substr($pstrAuthData,$this->arrCd[$i],1);
			if ($lstrAuth == '1') {
				if ($lstrAuthData != '') {
					$lstrAuthData .= ",";
				}
				$lstrAuthData .= $this->arrNm[$i];
			}
		}
		return $lstrAuthData;
	}
	//権限文字列取得（<br>タグ付き）
	function GetAuthNamesWithBR($pstrAuthData) {
		$lstrAuthData = '';
		$lintCT = 1;
		for ($i=0; $i < $this->intNo; $i++) {
			$lstrAuth = substr($pstrAuthData,$this->arrCd[$i],1);
			if ($lstrAuth == '1') {
				if ($lstrAuthData != '') {
					$lstrAuthData .= ($lintCT++ % 2 == 0) ? "<br>" : "，";
				}
				$lstrAuthData .= $this->arrNm[$i];
			}
		}
		return $lstrAuthData;
	}
	//権限文字列取得（<table>タグ付き）
	function GetAuthNamesWithTB($pstrAuthData) {
		$lstrAuthData = '';
		$lintCT = 1;
		if ($this->intNo > 0) {
			$lstrAuthData = '<table class="mini"><tr>';
			for ($i=0; $i < $this->intNo; $i++) {
				$lstrAuth = substr($pstrAuthData,$this->arrCd[$i],1);
				if ($lstrAuth == '1') {
					if ($lstrAuthData != '' && $i > 0) {
						$lstrAuthData .= ($lintCT++ % 2 == 0) ? "</td></tr><tr>" : "</td>";
					}
					$lstrAuthData .= '<td>';
					$lstrAuthData .= $this->arrNm[$i];
				}
			}
			$lstrAuthData .= ($lintCT++ % 2 == 0) ? "" : "</td><td>&nbsp;";
			$lstrAuthData .= "</td></tr></table>";
		}
		return $lstrAuthData;
	}
	//権限文字列取得（○<table>タグ付き）
	function GetAllAuthNamesWithTB($pstrAuthData) {
		$lstrAuthData = '';
		$lintCT = 1;
		if ($this->intNo > 0) {
			$lstrAuthData = '<table class="mini"><tr>';
			for ($i=0; $i < $this->intNo; $i++) {
				$lstrAuth = substr($pstrAuthData,$this->arrCd[$i],1);
				if ($lstrAuthData != '' && $i > 0) {
					$lstrAuthData .= ($lintCT++ % 2 == 0) ? "</td></tr><tr>" : "</td>";
				}
				$lstrAuthData .= '<td>';
				if ($lstrAuth == '1') {
					$lstrAuthData .= '■';
				} else {
//					$lstrAuthData .= '&nbsp;';
					$lstrAuthData .= '□';
				}
				$lstrAuthData .= '</td>';
				$lstrAuthData .= '<td>';
				$lstrAuthData .= $this->arrNm[$i];
			}
			$lstrAuthData .= ($lintCT++ % 2 == 0) ? "" : "</td><td>&nbsp;";
			$lstrAuthData .= "</td></tr></table>";
		}
		return $lstrAuthData;
	}
	//権限文字列取得（通知メール用）
	function GetAllAuthNamesForMail($pstrAuthData) {
		$lstrAuthData = '';
		$lintCT = 1;
		if ($this->intNo > 0) {
			for ($i=0; $i < $this->intNo; $i++) {
				$lstrAuth = substr($pstrAuthData,$this->arrCd[$i],1);
				if ($i > 0) {
					$lstrAuthData .= '　　　　　　';
				}
				if ($lstrAuth == '1') {
					$lstrAuthData .= '■';
				} else {
					$lstrAuthData .= '□';
				}
				$lstrAuthData .= $this->arrNm[$i]."\n";
			}
		}
		return $lstrAuthData;
	}
	//権限チェックボックス取得（<table>タグ付き）
	function GetAuthCheckboxWithTB($pstrAuthData,$pstrAuthName) {
		$lstrAuthData = '';
		$lintCT = 0;
		if ($this->intNo > 0) {
			$lstrAuthData = '<table class="mini"><tr>';
			for ($i=0; $i < $this->intNo; $i++) {
				if ($i > 0) {
					$lstrAuthData .= ($lintCT % 2 == 0) ? "</td></tr><tr>" : "</td>";
				}
				$lintCT++;
				$lstrChk = '';
				If (substr($pstrAuthData,$this->arrCd[$i],1) == '1') {
					$lstrChk = ' checked ';
				}
				$lstrAuthData .= '<td>';
				$lstrAuthData .= '<input type="checkbox" name="'.$pstrAuthName.'" value="'.$this->arrCd[$i].'" '.$lstrChk.' onChange="JS_chgBkColorDefault(this);">&nbsp;'.$this->arrNm[$i];
			}
			$lstrAuthData .= ($lintCT++ % 2 == 0) ? "" : "</td><td>&nbsp;";
			$lstrAuthData .= "</td></tr></table>";
		}
		return $lstrAuthData;
	}
	//権限hiddenデータ取得
	function GetAuthHidden($pstrAuthData,$pstrAuthName) {
		$lstrAuthData = '';
		if ($this->intNo > 0) {
			$lstrAuthData = '';
			for ($i=0; $i < $this->intNo; $i++) {
				If (substr($pstrAuthData,$this->arrCd[$i],1) == '1') {
					$lstrAuthData .= '<input type="hidden" name="'.$pstrAuthName.'" value="'.$this->arrCd[$i].'">'."\n";
				}
			}
		}
		return $lstrAuthData;
	}
	//権限チェック（true/false)
	function AuthCheck($pstrAuthData,$pstrAuthKind) {
		$lstrAuth = substr($pstrAuthData,$pstrAuthKind,1);
		if ($lstrAuth == '1') {
			return true;
		} else {
			return false;
		}
	}
	//管理者権限管理権限チェック
	function HaveAuthAdministrator($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthAdministrator);
	}
	//年会費管理権限チェック
	function HaveAuthYearPay($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthYearPay);
	}
	//請求書印字設定管理権限チェック
	function HaveAuthSignature($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthSignature);
	}
	//入会受付管理権限チェック
	function HaveAuthMemberRegist($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthMemberRegist);
	}
	//退会受付管理権限チェック
	function HaveAuthWithdrawalRegist($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthWithdrawalRegist);
	}
	//会員情報管理権限チェック
	function HaveAuthMemberManage($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthMemberManage);
	}
	//講演会管理権限チェック
	function HaveAuthSymposiumManage($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthSymposiumManage);
	}
	//発表受付権限チェック
	function HaveAuthPresenterRegist($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthPresenterRegist);
	}
	//参加受付権限チェック
	function HaveAuthParticipantRegist($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthParticipantRegist);
	}
	//講演番号インポート権限チェック
	function HaveAuthPresentNoImport($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthPresentNoImport);
	}
	//入金情報管理権限チェック
	function HaveAuthPaymentInfoManage($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthPaymentInfoManage);
	}
	//請求書用データダウンロード権限チェック
	function HaveAuthInvoiceInfoDownload($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthInvoiceInfoDownload);
	}
	//宛名用データダウンロード権限チェック
	function HaveAuthEnvelopesDownload($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthEnvelopesDownload);
	}
	//会員データダウンロード権限チェック
	function HaveAuthMemberInfoDownload($pstrAuthData) {
		return $this->AuthCheck($pstrAuthData,C_AuthMemberInfoDownload);
	}
	//権限設定
	function AuthSet($pstrAuthData,$pstrAuthSet) {
		$lstrAuthData = $pstrAuthData;
		if (strlen($lstrAuthData) == 0) $lstrAuthData = sprintf("%0".$this->intMaxNo."s","0");
		if (strlen($pstrAuthSet) > 0) {
			$lstrAuthData = substr_replace($lstrAuthData,"1",$pstrAuthSet,1);
		}
		return $lstrAuthData;
	}
}

//管理者情報リスト
class ClAdministratorList {
	var $strID;				//管理者ID
	var $strPass;			//パスワード
	var $strAccessIP;		//アクセス可能なIPアドレス
	var $strOffice;			//支部
	var $strEmail;			//E_MAILアドレス
	var $strAuthority;		//権限
	var $strUDate;			//更新日
	var $strDDate;			//削除日
}

//管理者情報
class ClAdministrator {
	var $strID;				//管理者ID
	var $strPass;			//パスワード
	var $strAccessIP;		//アクセス可能なIPアドレス
	var $strOffice;			//支部ID
	var $strEmail;			//E_MAILアドレス
	var $strAuthority;		//権限コード
	var $strUDate;			//更新日
	var $strDDate;			//削除日
	var $strMemo;			//備考
}

//管理者登録通知メール
class ClAdministratorRegistMail {
	var $strFrom;			//差出人
	var $strTo;				//送信先
	var $strCc;				//CC	2009/12/25追加
	var $strBcc;			//BCC	2009/12/25追加
	var $strSubject;		//件名
	var $strBody;			//本文
	var $strSignature;		//署名

    //PHP4 Constructor
    function ClAdministratorRegistMail(){
        $this->__construct();
    }

    //PHP5 Constructor
    function __construct() {
		//システムメールよりメール情報取得
		$lobjSystemMail = new ClSystemMail(20);

		if (strlen($lobjSystemMail->strSubject) > 0) {
			$this->strFrom      = $lobjSystemMail->strFrom;
			$this->strSignature = $lobjSystemMail->strSignature;
			$this->strSubject   = $lobjSystemMail->strSubject;
			$this->strBody      = $lobjSystemMail->strBody;
			$this->strCc        = $lobjSystemMail->strCc;	//2009/12/25追加
			$this->strBcc       = $lobjSystemMail->strBcc;	//2009/12/25追加
		} else {
			//署名取得
			$lobjSignature = new ClSignature();
			$lobjSignature->getDB();
			//差出人
			$this->strFrom = $lobjSignature->getFromEmail();
			//署名
			$this->strSignature = $lobjSignature->getSignature();
			//件名
			$this->strSubject = "（日本水環境学会）管理者%Action%完了のお知らせ";
			//本文
			$this->strBody    = "（日本水環境学会）管理者%Action%完了のお知らせ\n";
			$this->strBody   .= "\n";
			$this->strBody   .= "管理者として下記の通り、%Action%完了いたしました。\n";
			$this->strBody   .= "\n";
			$this->strBody   .= "　　　　　　　　記\n";
			$this->strBody   .= "\n";
			$this->strBody   .= "　管理者ID：%ID%\n";
			$this->strBody   .= "パスワード：%Pass%\n";
			$this->strBody   .= "IPアドレス：%AccessIP%\n";
			$this->strBody   .= "　　　支部：%Office%\n";
			$this->strBody   .= "　　E-Mail：%Email%\n";
			$this->strBody   .= "　　　権限：%Authority%\n";
			$this->strBody   .= "\n";
			$this->strBody   .= "\n";
			$this->strBody   .= "以上、よろしくお願い申し上げます。\n";
			$this->strBody   .= "\n";
		}
		unset($lobjSystemMail);
    }

    //受付メール送信
    function Sendmail($pobjAdministrator,$pobjAuth,$pobjOffice,$pstrAction) {
    	//送信先
    	$this->strTo = $pobjAdministrator->strEmail;
    	//件名生成
    	$lstrSubject = $this->strSubject;
    	$lstrSubject = mb_ereg_replace("%Action%" ,($pstrAction == "add"?"登録":($pstrAction=="mod"?"変更":"削除")) ,$lstrSubject);
    	//本文生成
    	$lstrBody = $this->strBody;
    	$lstrBody = mb_ereg_replace("%Action%"   ,($pstrAction == "add"?"登録":($pstrAction=="mod"?"変更":"削除")) ,$lstrBody);
    	$lstrBody = mb_ereg_replace("%ID%"       ,$pobjAdministrator->strID       ,$lstrBody);
    	$lstrBody = mb_ereg_replace("%Pass%"     ,(strlen($pobjAdministrator->strPass)==0?"変更なし":$pobjAdministrator->strPass) ,$lstrBody);
    	$lstrBody = mb_ereg_replace("%AccessIP%" ,$pobjAdministrator->strAccessIP ,$lstrBody);
    	$lstrBody = mb_ereg_replace("%Office%"   ,$pobjOffice->getOfficeName($pobjAdministrator->strOffice)   ,$lstrBody);
    	$lstrBody = mb_ereg_replace("%Email%"    ,$pobjAdministrator->strEmail    ,$lstrBody);
    	$lstrBody = mb_ereg_replace("%Authority%",$pobjAuth->GetAllAuthNamesForMail($pobjAdministrator->strAuthority)    ,$lstrBody);
    	$lstrBody .= $this->strSignature;

    	$lobjMail = new ClSendmail();
//    	$lobjMail->Send($this->strFrom,$this->strTo,"",$lstrSubject,$lstrBody);	//2009/12/25変更
    	$lobjMail->Send($this->strFrom,$this->strTo,$this->strCc,$lstrSubject,$lstrBody,$this->strBcc);	//2009/12/25変更
    }
}
?>
