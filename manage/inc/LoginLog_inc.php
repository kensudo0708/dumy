<?php
include_once("./inc/mySql_inc.php");
include_once("./inc/Func_inc.php");
include_once("./inc/Browser_inc.php");

class ClLoginLog {
	var $lintID;
	var $lstrIP;
	var $lstrOS;
	var $lstrBrowser;
	var $lstrUserAgent;

	//コンストラクタ(PHP4)
	function ClLoginLog() {
		$this->__construct();
	}
	//コンストラクタ(PHP5)
	function __construct() {
		$this->lstrIP        = $_SERVER['REMOTE_ADDR'];
		$this->lstrUserAgent = $_SERVER['HTTP_USER_AGENT'];
		$this->lstrOS        = PHP_GetOSType($this->lstrUserAgent);
		$this->lstrBrowser   = PHP_GetBrowserType($this->lstrUserAgent);
	}
  
	//ログインログ保存
	function SetLoginLog($pstrUserID,$pintType) {
		$lobjDB = new mySqlDB(); //DB接続
		//トランザクション開始
		$result = $lobjDB->BeginTrans();

		$lstrSQL  = "INSERT INTO LOGIN_LOG(LOGIN_TYPE,USER_ID,IP,OS,BROWSER,USER_AGENT,STIME)";
		$lstrSQL .= " VALUES (  ".$pintType."";
		$lstrSQL .= "         ,'".$pstrUserID."'";
		$lstrSQL .= "         ,'".$this->lstrIP."'";
		$lstrSQL .= "         ,'".$this->lstrOS."'";
		$lstrSQL .= "         ,'".$this->lstrBrowser."'";
		$lstrSQL .= "         ,'".$this->lstrUserAgent."'";
		$lstrSQL .= "         , NOW()";
		$lstrSQL .= "         )";
		$result = $lobjDB->ExecuteSQL($lstrSQL);
		if ($result == false) {
			//更新エラー
			$lstrMsg = "ログイン履歴テーブル更新中にエラーが発生しました（ " . mb_convert_encoding(mysql_error(), "UTF-8", "eucJP-win")."）";
			//ロールバック
			$result = $lobjDB->Rollback();
			PHP_Errlog("LoginLog_inc.php::SetLoginLog",$lstrMsg);
			$lblnInf = false;
		} else {
			//LOGIN_LOGテーブルに挿入した列のID取得
			$this->lintID = $lobjDB->GetCurrentID();
			//コミット
			$result = $lobjDB->CommitTrans();
			//セッション変数にIDを保存（ログアウト処理用）
			$_SESSION['LOGINLOG_ID'] = $this->lintID;
			//cookieにIDを保存（セッションタイムオーバー処理用）
			$lstrCookie = "JSWE_LOGIN_".$pintType;
			setcookie($lstrCookie,$this->lintID);
		}
		unset($lobjDB);
	}

	//ログインログ更新（ログアウト時）
	function SetLogoutLog($pintType) {
		$lstrCookie = "JSWE_LOGIN_".$pintType;
		//セッション変数からIDを取得
		$this->lintID = $_SESSION['LOGINLOG_ID'];
		if ($this->lintID == "") {
			//セッションタイムオーバーの場合、cookieからIDを取得
			$this->lintID = $_COOKIE[$lstrCookie];
		}
		if ($this->lintID == "") return;

		$lobjDB = new mySqlDB(); //DB接続
		//トランザクション開始
		$result = $lobjDB->BeginTrans();

		$lstrSQL  = "UPDATE LOGIN_LOG SET ";
		$lstrSQL .= " ETIME = NOW()";
		$lstrSQL .= " WHERE ID =  ".$this->lintID."";
		$result = $lobjDB->ExecuteSQL($lstrSQL);
		if ($result == false) {
			//更新エラー
			$lstrMsg = "ログイン履歴テーブル更新中にエラーが発生しました（ " . mb_convert_encoding(mysql_error(), "UTF-8", "eucJP-win")."）";
			//ロールバック
			$result = $lobjDB->Rollback();
			PHP_Errlog("LoginLog_inc.php::SetLogoutLog",$lstrMsg);
			$lblnInf = false;
		} else {
			//コミット
			$result = $lobjDB->CommitTrans();
		}
		unset($lobjDB);

		//cookie削除
		setcookie($lstrCookie,False);
	}

	//ログイン失敗ログ保存
	function SetLoginFailedLog($pstrUserID,$pstrPass,$pintType,$pstrCause) {
		$lobjDB = new mySqlDB(); //DB接続
		//トランザクション開始
		$result = $lobjDB->BeginTrans();

		$lstrSQL  = "INSERT INTO LOGIN_FAILED_LOG(LOGIN_TYPE,USER_ID,PASS,IP,OS,BROWSER,USER_AGENT,CAUSE,STIME)";
		$lstrSQL .= " VALUES (  ".$pintType."";
		$lstrSQL .= "         ,'".$pstrUserID."'";
		$lstrSQL .= "         ,'".$pstrPass."'";
		$lstrSQL .= "         ,'".$this->lstrIP."'";
		$lstrSQL .= "         ,'".$this->lstrOS."'";
		$lstrSQL .= "         ,'".$this->lstrBrowser."'";
		$lstrSQL .= "         ,'".$this->lstrUserAgent."'";
		$lstrSQL .= "         ,'". mb_convert_encoding($pstrCause,"eucJP-win", "UTF-8")."'";
		$lstrSQL .= "         , NOW()";
		$lstrSQL .= "         )";
		$result = $lobjDB->ExecuteSQL($lstrSQL);
		if ($result == false) {
			//更新エラー
			$lstrMsg = "ログイン履歴テーブル更新中にエラーが発生しました（ " . mb_convert_encoding(mysql_error(), "UTF-8", "eucJP-win")."）";
			//ロールバック
			$result = $lobjDB->Rollback();
			PHP_Errlog("LoginLog_inc.php::SetLoginFailedLog",$lstrMsg);
			$lblnInf = false;
		} else {
			//コミット
			$result = $lobjDB->CommitTrans();
		}
		unset($lobjDB);
	}
}
?>
