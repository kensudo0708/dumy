<?php
/**
 * ログインしているかチェックする
 */
function PHP_LoginChk() {
	//セッションチェック
	if (session_id() == "") {
		//セッション開始
		session_start();
	}
	if (!isset($_SESSION['ADM_USER_ID'])) {
		$lstrURL = "./index.php";
		include_once("./inc/LoginLog_inc.php");
		$lobjLoginLog = new ClLoginLog();
		$lobjLoginLog->SetLogoutLog(0);
		//メッセージ表示後トップ画面へ移動
		include("./html/errmsg.html");
		exit;
	}
	return true;
}

//エラーログ出力
function PHP_Errlog($pstrFile,$pstrMsg) {
	$strErrLogFile = "/home/fis-mm-com/log/errlog-".date('Ymd').".txt"; 

	//日付
	$strDate = date('Y/m/d H:i:s');

	//文字化け防止
	$lstrFile = $pstrFile;
	$lstrEncode = mb_detect_encoding($lstrFile,"UTF-8,sjis-win,SJIS,eucJP-win");
	if ($lstrEncode != "UTF-8") {
		//UTF-8でなければ変換
		$lstrFile = mb_convert_encoding($lstrFile, "UTF-8", $lstrEncode);
	}
	$lstrMsg = $pstrMsg;
	$lstrEncode = mb_detect_encoding($lstrMsg,"UTF-8,sjis-win,SJIS,eucJP-win");
	if ($lstrEncode != "UTF-8") {
		//UTF-8でなければ変換
		$lstrMsg = mb_convert_encoding($lstrFile, "UTF-8", $lstrEncode);
	}

	//ファイルオープン
	$objFile = fopen($strErrLogFile,"a+");

	//出力
	fwrite($objFile, $strDate.",".$lstrFile.",".$lstrMsg."\n");

	//ファイルクローズ
	fclose($objFile);
	return true;
}
//
// メタキャラクタエスケープ 
//
function PHP_MetaEscape($pstrData) {
	$strData = $pstrData;
	if (get_magic_quotes_gpc()) {
		// 入力データがクォートされているときは、バックスラッシュを外す
		$strData = stripslashes($strData);
	}
	$strData = htmlspecialchars($strData);
	if (get_magic_quotes_gpc()) {
		// 入力データがクォートされているときは、バックスラッシュを外す
		$strData = stripslashes($strData);
	}
	return $strData;
}
//
// メタキャラクタエスケープ（DB保存用）
//
function PHP_MetaEscapeDB($pstrData) {
	$strData = $pstrData;
	if (get_magic_quotes_gpc()) {
		// 入力データがクォートされているときは、バックスラッシュを外す
		$strData = stripslashes($strData);
	}
	$strData = mb_convert_encoding($strData, "eucJP-win", "UTF-8");
	$strData = mysql_real_escape_string($strData);
	return $strData;
}
function PHP_MetaEscapeD($pstrData) {
	return PHP_MetaEscapeDB($pstrData);
}

function PHP_fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
    $d = preg_quote($d);
    $e = preg_quote($e);
    $_line = "";
    while ($eof != true) {
        $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
        $itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
        if ($itemcnt % 2 == 0) $eof = true;
    }
	
	$_line = mb_convert_encoding($_line, "UTF-8", "sjis-win");
    $_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
    $_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
    preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
    $_csv_data = $_csv_matches[1];
    for($_csv_i=0; $_csv_i < count($_csv_data); $_csv_i++){
        $_csv_data[$_csv_i] = preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
        $_csv_data[$_csv_i] = str_replace($e.$e, $e, $_csv_data[$_csv_i]);
    }
    return empty($_line) ? false : $_csv_data;
}
?>
