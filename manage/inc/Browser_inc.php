<?php
function PHP_GetBrowserType($pstrUserAgent) {
	$lstrBrowser = "";
	if (strpos($pstrUserAgent,"Opera") > 0) {
		//Opera
		if (strpos($pstrUserAgent,"Version") > 0) {
			//Opera 10
			$larrSP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Version")+7));
		} else {
			//Opera 9以前
			$larrSP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Opera")+6));
		}
		$lstrBrowser = "Opera ".trim($larrSP[0]);
	} elseif (strpos($pstrUserAgent,"MSIE") > 0) {
		//IE
		if (strpos(substr($pstrUserAgent,strpos($pstrUserAgent,"MSIE")+5),";") > 0) {
			$larrSP = explode(";",substr($pstrUserAgent,strpos($pstrUserAgent,"MSIE")+5));
		} else {
			$larrSP = explode(")",substr($pstrUserAgent,strpos($pstrUserAgent,"MSIE")+5));
		}
		$lstrBrowser = "Internet Explorer ".trim($larrSP[0]);
	} elseif (strpos($pstrUserAgent,"Firefox") > 0) {
		//Firefox
		$larrSP = explode("/",substr($pstrUserAgent,strpos($pstrUserAgent,"Firefox")));
		$lstrBrowser = "Firefox ".trim($larrSP[1]);
		if (strpos($lstrBrowser,"(") > 0) {
			//(.NET CLR xxx)を除く
			$lstrBrowser = substr($lstrBrowser,0,strpos($lstrBrowser,"("));
		}
	} elseif (strpos($pstrUserAgent,"Sleipnir") > 0) {
		//Sleipnir
		$larrP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Sleipnir")));
		$lstrBrowser = "Sleipnir ".trim($larrSP[2]);
	} elseif (strpos($pstrUserAgent,"*Internet Explorer") > 0) {
		//Sleipnir 2
		$lstrBrowser = "Sleipnir 2.00";
	} elseif (strpos($pstrUserAgent,"Chrome") > 0) {
		//Google Chrome
		$larrSP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Chrome")+7));
		$lstrBrowser = "Google Chrome ".trim($larrSP[0]);
	} elseif (strpos($pstrUserAgent,"Safari") > 0) {
		//Safari
		if (strpos($pstrUserAgent,"Version") > 0) {
			$larrSP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Version")+8));
		} else {
			$larrSP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Safari")+7));
		}
		$lstrBrowser = "Safari ".trim($larrSP[0]);
	} elseif (strpos($pstrUserAgent,"Gecko") > 0) {
		//NN6以上
		$larrSP = explode(" ",substr($pstrUserAgent,strpos($pstrUserAgent,"Netscape")+9));
		$lstrBrowser = "Netscape ".trim($larrSP[0]);
	} else {
		//NC4
		$larrSP = explode(" ",substr($pstrUserAgent,9));
		$lstrBrowser = "Netscape ".trim($larrSP[0]);
	}

	return($lstrBrowser);
}

function PHP_GetOSType($pstrUserAgent) {
//	$pstrUserAgent = Request.ServerVariables("HTTP_USER_AGENT")
	$larrOS = "";
	if (strpos($pstrUserAgent,"compatible") > 0) {
		//IE,Opera
		$larrSP = explode(";",$pstrUserAgent);
		$lstrOS = trim($larrSP[2]);
	} elseif (strpos($pstrUserAgent,"Gecko") > 0) {
		//NN6以上
		$larrSP = explode(";",$pstrUserAgent);
		$lstrOS = trim($larrSP[2]);
	} elseif (strpos($pstrUserAgent,"Mozilla/4") > 0) {
		//NC4
		$larrSP = explode(";",substr($pstrUserAgent,20));
		$lstrOS = trim($larrSP[0]);
	} elseif (strpos($pstrUserAgent,"Sleipnir") > 0) {
		//Sleipnir 2
		$lstrOS = "???";
	} elseif (strpos($pstrUserAgent,"*Internet Explorer") > 0) {
		//Sleipnir 2.0
		$lstrOS = "???";
	} else {
		//Opera
		$larrSP = explode(";",substr($pstrUserAgent,13));
		$lstrOS = trim($lstrSP[0]);
	}

	if (strpos($lstrOS,"95") > 0) {
		$lstrOS = "Windows 95";
	} elseif (strpos($lstrOS,"98") > 0) {
		$lstrOS = "Windows 98";
	} elseif (strpos($lstrOS,"ME") > 0) {
		$lstrOS = "Windows ME";
	} elseif (strpos($lstrOS,"NT 4") > 0) {
		$lstrOS = "Windows NT4.0";
	} elseif (strpos($lstrOS,"NT 5.0") > 0) {
		$lstrOS = "Windows 2000";
	} elseif (strpos($lstrOS,"NT 5.1") > 0 || strpos($lstrOS,"XP 5.1") > 0) {
		$lstrOS = "Windows XP";
		if (strpos($pstrUserAgent,"SV1") > 0 || strpos($pstrUserAgent,"MSIE 7") > 0) {
			$lstrOS .= " SP2";
		}
	} elseif (strpos($lstrOS,"NT 5.2") > 0) {
		$lstrOS = "Windows 2003";
	} elseif (strpos($lstrOS,"NT 6.0") > 0) {
		$lstrOS = "Windows Vista";
	} elseif (strpos($lstrOS,"NT 6.1") > 0) {
		$lstrOS = "Windows 7";
	}

	return($lstrOS);
}
?>
