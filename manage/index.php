<?php
/*******************************************************************************
 * モージュールID ：index.php
 * モジュール名称  ：メニュー制御
 * 機能概要　　 　：
 * 改訂履歴　　 　：Rev1.0 2010/12/21 新規kensudo
 ******************************************************************************/
require_once("./inc/mySql_inc.php");               //mySQL操作クラス
require_once("./inc/Func_inc.php");                //共通ファンクションクラス
require_once("./common/CommonFunc.php");           //共通関数クラス
require_once("./sql/ClSqlMember.php");             //会員テーブルSQL操作関数
require_once("./common/Constant.php");             //グローバル定数
require_once("./ManagementBusiness.php");          //本機能のビジネスロジック

//変数初期化
global $gstrOnLoad;                                //オンロードイベントをセットするグローバル変数
global $gobjCommon;                                //共通関数オブジェクト
global $gstrDirectory;                             //ディレクトリ情報
global $gstrLoginPage;                             //ログインページ名称
global $gstrMenuPage;                              //番組選択ページ名称
global $gstrHostPage;                              //ホスト選択ページ名称
global $gstrDataPage;                              //ログデータ選択ページ名称
global $gstrController;                            //コントローラ
global $gstrUserID;                                //ユーザＩＤ

$gstrLoginPage     = "./html/login.html";          //ログインページ
$gstrMenuPage      = "./html/menu.html";           //番組選択ページ
$gstrHostPage      = "./html/host.html";           //ホスト選択ページ
$gstrDataPage      = "./html/data.html";           //データ選択ページ
$gstrContentsPage  = "./html/contents.html";           //データ選択ページ
$gstrController    = "index.php";                  //コントローラ
$gstrDirectory     = "";

$lstrMsg       = "";                               //エラーメッセージ
$lintMode      = "";                               //処理モード
$gstrOnLoad    = "";                               //オンロードイベント
$ldateNow = date("Y/m/d");                         //本日日付

//セッションチェック
if (session_id() == "") { 
	//セッション開始
	session_start();
}
/*
if (isset($_SESSION['ADM_USER_ID'])) {
	$gstrUserID = $_SESSION['ADM_USER_ID'];        //セッションが存在すれば認証情報をグローバル変数に格納する
	If ($gstrUserID != "") {
		$lintMode = "2";
	}
}
*/

/**
 * フォームデータ取得
 */

if (isset($_REQUEST["mode"])) {
	@ $gstrDirectory       = $_REQUEST["directory"];
	if (isset($_REQUEST["program"]) && !empty($_REQUEST["program"])){
		$laryProgram = array();
		$lobjCommon = new ClCommonFunc();
		$laryProgram = explode(",",$lobjCommon->PHP_getProgram($_REQUEST["program"]));
		$_SESSION['PROGRAMNAME']       = $laryProgram[2];
		$_SESSION['WEBNAME']           = $laryProgram[1];
	}
	$lintMode          = $_REQUEST["mode"];
	if ( $lintMode == "1" ) {
		$gstrUserID    = $_REQUEST["userid"];      //ログインＩＤ
		$lstrUserPWD   = $_REQUEST["passwd"];      //パスワード
		$_SESSION['ADM_USER_ID'] = $_REQUEST["userid"];
	} else if ($lintMode == "") {
		$lintMode = "1";
		PHP_clearSession();
	}
}else{   //初回処理
	$lintMode = "1";                               //処理モード(ログイン処理)
	PHP_clearSession();                            //セッション情報をクリアする
	$_SESSION['WEBNAME'] = "v-market.jp";          //初期設定値はバリューマーケット
}
//念の為に入れる
if (!isset($_SESSION['WEBNAME']) || $_SESSION['WEBNAME'] == ""){
	$_SESSION['WEBNAME'] = "v-market.jp";
}
/*******************************************************
 * コントロール
 * 
 *******************************************************/
try {
	switch ($lintMode) {
		case "1":        //ログイン処理
			/*
			$lobjDB   = new mySqlDB();                            //MySqlオブジェクト
			$lobjSql  = new ClSqlmember();                        //会員テーブルSQL文オブジェクト
			$lstrSql  = $lobjSql->PHP_IsValidMember($lstrUserID); //会員チェックSQL文
			$lobjRS   = $lobjDB->Query($lstrSql);
	    
			if ($larrRows = $lobjDB->FetchRow($lobjRS)) {
				//共通関数のインスタンス作成
				$lobjComm = new clCommonFunc();
                if ($larrRows['PASS'] == md5($lstrUserPWD)){ 
                   	$_SESSION['ADM_USER_ID']     = $lstrUserID;
					$_SESSION['USER_PASSWD']     = $lstrUserPWD;
					$gstrOnload = "";
					$gobjCommon = new clCommonFunc();
					include("./html/menu.html");
					exit;
				} else {
					//パスワード不一致
					$lstrMsg = "IDまたはパスワードが正しくありません。 ";
				} 
			} else {
				//IDなし
				$lstrMsg = "当システムへのアクセスが許可されていません。";
			}
        	if ($lstrMsg != "") {
				throw new MyCustomException($lstrMsg);
			}
			*/
			//ログイン画面表示
			PHP_DispLoginForm();
			break;
		case "2":        //番組選択処理
			PHP_DispMenuForm();
			break;
		case "3":        //ホスト選択処理
			PHP_DispHostForm();
			break;
		case "4":        //ログデータ選択処理
			PHP_DispDataForm();
			break;
		case "5":        //ログデータ内容表示処理
			PHP_DispContentsForm();
			break;
		case "6":        //ログアウト処理
			$_SESSION = array();
			PHP_clearSession();
			//セッションを破棄する
			session_destroy();
			//ログイン画面表示
			$gstrOnload = "onLoad=\"document.forms[0].userid.focus();\"";
			PHP_DispLoginForm();
			
			break;
		case "7":         //アパッチデーモン再スタート
			PHP_UpdateServerInfo();
			break;
		default:
			$_SESSION = array();
			PHP_clearSession();
			//セッションを破棄する
			session_destroy();
			//ログイン画面表示
			$gstrOnload = "onLoad=\"document.forms[0].userid.focus();\"";
			PHP_DispLoginForm();
			
			break;
		}
	}catch(MyCustomException $e){
		$gstrOnload = "onLoad=\"alert('".$lstrMsg."');\"";
		include("./html/login.html");
		PHP_Errlog("index.php",$e."   ＩＤ  => ".$gstrUserID."  ++  ".$lstrUserPWD);
    }
	catch(Exception $e){
		$gstrOnload = "onLoad=\"alert('".$lstrMsg."');\"";
		include("./html/login.html");
		PHP_Errlog("index.php",$e);
    }
	
exit;
?>