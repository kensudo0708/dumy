<?php
/*
 * DAOクラス
 */
//include_once('./common/Constant.php');                      //スーパーグローバル定数
//require_once('./common/CommonFunc.php');                    //共通関数くらす

class BaseDatabase {
	private $lhostname = "";         //データベースホスト
  	private $lusername = "";         //ユーザ名	
  	private $lpassword = "";         //パスワード
  	private $ldatabase = "";         //データベース
  	private $llink     = null;       //コネクション
  	private $result_id;              //結果セット
  	private $lrow;                   //レコード行
  	private $lerrMsg   = "";
  	
	/*
	 * コンストラクタ
	 * 【パラメータ】
	 * �@ホスト名
	 * �Aユーザ名
	 * �Bパスワード
	 * �Cデータベース
	 */
  	function __construct($phostname="",$pusername="" ,$ppassword="" ,$pdatabase=""){
        try {
  			if ($phostname!="") $this->lhostname = $phostname;
  			else $this->lhostname = C_HOSTNAME;
  		
  			if ($pusername!="") $this->lusername = $pusername;
  			else $this->lusername = C_USERNAME;
  		
  			if ($ppassword!="") $this->lpassword = $ppassword;
  			$this->lpassword = C_PASSWORD;
  		
  			if ($pdatabase!="") $this->ldatabase = $pdatabase;
  			$this->ldatabase = C_DATABASE;
  		
  			$this->PHP_connect();
        }
  		catch(MyCustomException $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
		catch(Exception $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
  	}
  	/*
  	 * デストラクタ
  	 */
  	function __destruct(){
  		$this->PHP_disconnect();
  	}
  	
  	/*
  	 * コネクション取得･返却
  	 */
  	private function PHP_getConn() {
    	try {
  			if(is_null($this->llink)) {
    			$this->PHP_pconnect();
    		}
    		return $this->llink;
    	}	
  		catch(MyCustomException $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
		catch(Exception $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
  	}
    /*
     * コネクション確立
     */
  	private function PHP_connect(){
		try {
  			if( is_null($this->llink)){
  				$this->llink = mysql_pconnect($this->lhostname,$this->lusername,$this->lpassword);
			    //$this->llink->query("SET NAMES euc-jp");
  				//if(mysqli_connect_errno()){
  				if (!$this->llink){
  					$lerrMsg = "サーバとの接続に失敗しました";
  					throw new Exception($lerrMsg);
  				}
  			}
			//$this->PHP_selectDb();
			if( !mysql_select_db($this->ldatabase, $this->llink) ) {
    			$lerrMsg = "データベースの接続に失敗しました";
  				throw new Exception($lerrMsg);
    		}
			$this->result_id = mysql_query('SET AUTOCOMMIT=0', $this->llink);
		}
  		catch(MyCustomException $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
		catch (Exception $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
		}
  	}
  	/*
  	 * データベース設定
  	 */
  	private function PHP_selectDb(){
    	if( !mysql_select_db($this->ldatabase, $this->llink) ) {
    		$lerrMsg = "データベースの接続に失敗しました";
  			throw new Exception($lerrMsg);
    	}
    	return;
   }
   /*
    * クエリー実行：row返却
    */
   protected function PHP_Fill($pstrSQL){
   		try {	
   			is_null($this->llink) and $this->PHP_connect();
   			$lresult = mysql_query($pstrSQL);
			if( !$lresult ) {
  				$lerrMsg = "クエリーの実行に失敗しました";
  				throw new Exception($lerrMsg);
   			}
   			$lrow = mysql_fetch_array($lresult);
   			//$lresult.close();
   			return $lrow;
   		}
   		catch(MyCustomException $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
		catch (Exception $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
		}
   }
   /*
    * クエリー実行:レコードセット返却
    */
   protected function PHP_Find($pstrSQL){
   		try {
   			is_null($this->llink) and $this->PHP_connect();
   			$lresult = mysql_query($pstrSQL);
			if( !$lresult ) {
  				$lerrMsg = "クエリーの実行に失敗しました";
  				throw new Exception($lerrMsg);
   			}
   			return $lresult;
   		}
   		catch(MyCustomException $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
		catch (Exception $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
		}
   }
   /*
    * 実行
    */
   protected function PHP_ExecuteSQL($sql){
    	try {
   			is_null($this->llink) and $this->PHP_connect();
			$lresult = mysql_query($sql, $this->llink);
   			if (!$lresult){
				$lerrMsg = "クエリーの実行に失敗しました";
  				throw new Exception($lerrMsg);
			}
			return $lresult;
        }
   		catch(MyCustomException $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
    	}
		catch (Exception $e){
			$this->PHP_Err("BaseDatabase.php ",$e);
		}
        
   }
   /*
    * エラー処理
    */
   private function PHP_Err($pstrSQL){
   		$lmessage  = 'Invalid query: ' . mysql_error() . "\n";
    	$lmessage .= 'Whole query: ' . $pstrSQL;
    	die($lmessage);   	
   }
   /*
    * 
    */
   private function __tostring(){
   	
   }
   /*
    * データベース切断
    */
   private function PHP_disconnect(){
   		is_null($this->llink) or mysql_close($this->llink);
   }
}
?>