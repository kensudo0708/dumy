<?php
/*******************************************************************************
 * モージュールID ：createGrouph.php
 * モジュール名称  ：
 * 機能概要　　 　：
 * 改訂履歴　　 　：Rev1.0 2010/12/18 新規kensudo
 ******************************************************************************/
require_once("./common/CommonFunc.php");              //共通関数クラス
$lstrOut         = "";
//年
if (is_numeric($_REQUEST['year'])){
	$lstrYear  = $_REQUEST['year'];
}else{
	$lstrYear  = date('Y');
}

//月
if (is_numeric($_REQUEST['month'])){
	$lstrMonth       = $_REQUEST['month'];
}else{
	$lstrMonth       = date('m');
}

//日
if (is_numeric($_REQUEST['day'])){
	$lstrDay         = $_REQUEST['day'];
}else{
	$lstrDay         = date('d');
}

//開始時間
if (is_numeric($_REQUEST['starthour'])){
	$lstrStartHour   = $_REQUEST['starthour'];
}else{
	$lstrStartHour   = "00";
}
//開始分
if (is_numeric($_REQUEST['startminute'])){
	$lstrStartMinute = $_REQUEST['startminute'];
}else{
	$lstrStartMinute = "00";
}
//終了時間
if (is_numeric($_REQUEST['endhour'])){
	$lstrEndHour     = $_REQUEST['endhour'];
}else{
	$lstrEndHour     = "00";
}
//終了分
if (is_numeric($_REQUEST['endtminute'])){
	$lstrEndMinute   = $_REQUEST['endtminute'];
}else{
	$lstrEndMinute   = "00";
}
$aryTitle        = array();
$aryData         = array();

$handle = fopen("/var/www/html/manegement/alerm_mail/httpd_num", "r");
$lstrOut .="<p class=\"left\"><span class=\"formPoint\">&nbsp;&nbsp;";
$cnt = 1;
while (!feof($handle)) {
	//日付、時間、httpd本数に分割して配列に格納する
	if ($cnt % 2 == 0){
		$aryData  = explode(" ",fgets($handle));
	}else{
		$aryTitle = explode(" ",fgets($handle));
	}
	//var_dump($aryData);
	//echo trim($lstrYear."-".$lstrMonth."-".$lstrDay)."  ".trim($aryData[0]);
	/*
	if (trim($lstrYear."-".$lstrMonth."-".$lstrDay) <= trim($aryData[0])){
		if (is_numeric($lstrStartHour) && is_numeric($lstrStartMinute)){
			if (is_numeric($lstrEndHour) && is_numeric($lstrEndMinute)){
				if (trim($lstrStartHour.":".$lstrStartMinute) >= trim($aryData[1]) && trim($lstrEndHour.":".$lstrEndMinute) <= trim($aryData[1])){
						$lstrOut .= $aryData[0].$aryData[1].$aryData[2]."&nbsp;&nbsp;";
				}else{
				}
			}else{
				if (trim($lstrStartHour.":".$lstrStartMinute) >= trim($aryData[1])){
					$lstrOut .= $aryData[0].$aryData[1].$aryData[2]."&nbsp;&nbsp;";
				}else{	
				}
			}
		}else{
			if (is_numeric($lstrEndHour) && is_numeric($lstrEndMinute)){
				if (trim($lstrEndHour.":".$lstrEndMinute) <= trim($aryData[1])){
					$lstrOut .= $aryData[0].$aryData[1].$aryData[2]."&nbsp;&nbsp;";
				}else{	
				}
			}else{
				$lstrOut .= $aryData[0].$aryData[1].$aryData[2]."&nbsp;&nbsp;";
			}
		}
	}
	*/
	//echo trim($lstrYear."-".$lstrMonth."-".$lstrDay)." ".trim($aryTitle[0]);
	
	if (trim($lstrYear."-".$lstrMonth."-".$lstrDay) <= trim($aryTitle[0])){
		$lstrOut .= $aryTitle[0]."&nbsp;".$aryTitle[1]."&nbsp;".$aryData[0]."&nbsp;&nbsp;";
	}
	$cnt++;
}
$lstrOut .="</p></span>";
fclose($handle);
echo $lstrOut;

?>