<?php
/*******************************************************************************
 * モージュールID ：CommonFunc.php
 * モジュール名称  ：本システム共通関数
 * 機能概要　　 　：システム全体で利用される汎用関数
 * 改訂履歴　　 　：Rev1.0 2010/12/18 新規kensudo
 ******************************************************************************/
//require_once('./dao/BaseDatabase.php');
class ClCommonFunc{
//class ClCommonFunc extends BaseDatabase{
	private $lstrSQL = "";
	//コンストラクト
	function __construct(){ 
		//parent::__construct(); 
	}
	
	//---------------------------------------
	// 機能概要 :番組選択ページ
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_getProgram($pitem) {
		
		$arrCd[1]  = "1";
		$arrNm[1]  = "yasunehonpo.com";
		$arrDr[1]  = C_YASUNEHONPO_DICRECTORY;
		$arrCo[1]  = "<span class=\"formPoint\">安値本舗</span>&nbsp;&nbsp;(27XBPcLjeL)&nbsp;&nbsp;エビス<br>180.189.34.207";
		$arrNa[1]  = "安値本舗";
		$arrCd[2]  = "2";
		$arrNm[2]  = "zerooku.com";
		$arrDr[2]  = C_ZEROOKU_DICRECTORY;
		$arrCo[2]  = "<span class=\"formPoint\">ゼロオク</span>&nbsp;&nbsp;(a0b3z9)&nbsp;&nbsp;ゼロオク<br>180.189.34.202";
		$arrNa[2]  = "ゼロオク";
		$arrCd[3]  = "3";
		$arrNm[3]  = "gigaoku.jp";
		$arrDr[3]  = C_GIGAOKU_DICRECTORY;
		$arrCo[3]  = "<span class=\"formPoint\">ギガオク</span>&nbsp;&nbsp;(5s73it)&nbsp;&nbsp;テスタ<br>180.189.34.203";
		$arrNa[3]  = "ギガオク";
		$arrCd[4]  = "4";
		$arrNm[4]  = "poke-oku.jp";
		$arrCo[4]  = "<span class=\"formPoint\">ポケオク</span>&nbsp;&nbsp;(9ess4z)&nbsp;&nbsp;ウェーブアイ<br>180.189.34.201";
		$arrNa[4]  = "ポケオク";
		$arrDr[4]  = C_POKEOKU_DICRECTORY;
		$arrCd[5]  = "5";
		$arrNm[5]  = "megaoku.jp";
		$arrDr[5]  = C_MEGAOKU_DICRECTORY;
		$arrCo[5]  = "<span class=\"formPoint\">メガオク</span>&nbsp;&nbsp;(z2fbp5)&nbsp;&nbsp;ラパン<br>203.86.239.10";
		$arrNa[5]  = "メガオク";
		$arrCd[6]  = "6";
		$arrNm[6]  = "i-oku.com";
		$arrDr[6]  = C_IOKU_DICRECTORY;
		$arrCo[6]  = "<span class=\"formPoint\">イタダキオークション</span>&nbsp;&nbsp;(z2ckgkR5a4)&nbsp;&nbsp;メディアビュー<br>180.189.34.204";
		$arrNa[6]  = "イタダキオークション";
		$arrCd[7]  = "7";
		$arrNm[7]  = "o-toku.jp";
		$arrDr[7]  = C_OTOKU_DICRECTORY;
		$arrCo[7]  = "<span class=\"formPoint\">得オク</span>&nbsp;&nbsp;(baVQ59cLMW)&nbsp;&nbsp;ワールドミル<br>180.189.34.205(WEB)<br>180.189.34.220(DB)";
		$arrNa[7]  = "得オク";
		$arrCd[8]  = "8";
		$arrNm[8]  = "joyoku.jp";
		$arrDr[8]  = C_JOYOKU_DICRECTORY;
		$arrCo[8]  = "<span class=\"formPoint\">JOYオク</span>&nbsp;&nbsp;(5XJr9FM1zj)&nbsp;&nbsp;ジェイズ<br>180.189.34.208";
		$arrNa[8]  = "JOYオク";
		$arrCd[9]  = "9";
		$arrNm[9]  = "hachioku.jp";
		$arrDr[9]  = C_HACHIOKU_DICRECTORY;
		$arrCo[9]  = "<span class=\"formPoint\">ハチオク</span>&nbsp;&nbsp;(nbF78NEsE7)&nbsp;&nbsp;RBRAIN<br>210.175.62.105";
		$arrNa[9]  = "ハチオク";
		$arrCd[10] = "10";
		$arrNm[10] = "v-market.jp";
		$arrDr[10]  = C_VMARKET_DICRECTORY;
		$arrCo[10]  = "<span class=\"formPoint\">バリューマーケット</span>&nbsp;&nbsp;(4o2EM02V9e)&nbsp;&nbsp;フリーワールド<br>180.189.34.241";
		$arrNa[10]  = "バリューマーケット";
		$arrCd[11] = "11";
		$arrNm[11] = "now-oku.net";
		$arrDr[11]  = C_NOWOKU_DICRECTORY;
		$arrCo[11]  = "<span class=\"formPoint\">なうおく</span>&nbsp;&nbsp;(tfNboK3j6C)&nbsp;&nbsp;ルーター<br>202.215.248.150";
		$arrNa[11]  = "なうおく";
		$arrCd[12] = "12";
		$arrNm[12] = "marutoku.net";
		$arrDr[12]  = C_MARUTOKU_DICRECTORY;
		$arrCo[12]  = "<span class=\"formPoint\">まる得プライス</span>&nbsp;&nbsp;(shMa9Kpr7h)&nbsp;&nbsp;フロントライン<br>27.96.41.35";
		$arrNa[12]  = "まる得プライス";
		$arrCd[13] = "13";
		$arrNm[13] = "zero-01.jp";
		$arrDr[13]  = C_ZERO01_DICRECTORY;
		$arrCo[13]  = "<span class=\"formPoint\">ゼロワン</span>&nbsp;&nbsp;(PJV3rw5SRu)&nbsp;&nbsp;アットフューチャー<br>61.197.194.54(WEB)<br>61.197.194.55(DB)";
		$arrNa[13]  = "ゼロワン";
		$arrCd[14] = "14";
		$arrNm[14] = "ibid-auction.jp";
		$arrDr[14]  = C_IBIDAUCTION_DICRECTORY;
		$arrCo[14]  = "<span class=\"formPoint\">i-Bid</span>&nbsp;&nbsp;(K7ShV3uzjI)&nbsp;&nbsp;クロスネット<br>122.220.221.44(WEB)<br>122.220.221.43(DB)";
		$arrNa[14]  = "クロスネット";
		$arrCd[15] = "15";
		$arrNm[15] = "age-oku.com";
		$arrDr[15]  = C_AGEOKU_DICRECTORY;
		$arrCo[15]  = "<span class=\"formPoint\">Ageオク</span>(5HwRlI07Rq)&nbsp;&nbsp;ナノテック<br>180.189.34.231";
		$arrNa[15]  = "Ageオク";
		$arrCd[16] = "16";
		$arrNm[16] = "happy-oku.jp";
		$arrDr[16]  = C_HAPPYOKU_DICRECTORY;
		$arrCo[16]  = "<span class=\"formPoint\">レイト</span>&nbsp;&nbsp;(RO1BtEn9Sp)&nbsp;&nbsp;レイト<br>59.106.169.23";
		$arrNa[16]  = "レイト";
		$arrCd[17] = "17";
		$arrNm[17] = "p-off.jp";
		$arrDr[17]  = C_POFF_DICRECTORY;
		$arrCo[17]  = "<span class=\"formPoint\">パーセントオフ</span>&nbsp;&nbsp;(i0QXp4GhAf)&nbsp;&nbsp;LDL<br>112.78.206.222(WEB)<br>112.78.206.222(DB)";
		$arrNa[17]  = "パーセントオフ";
		$arrCd[18] = "18";
		$arrNm[18] = "anshin-auc.com";
		$arrDr[18]  = C_ANSHINAUC_DICRECTORY;
		$arrCo[18]  = "<span class=\"formPoint\">あんしんオークション</span>&nbsp;&nbsp;(rdK26d6LzG)&nbsp;&nbsp;ネクステージ<br>180.189.34.223";
		$arrNa[18]  = "あんしんオークション";
		
		$lstrName = "";
		for ($i=1; $i <= $arrCd; $i++) {
			if ($pitem == $arrCd[$i]) {
				$lstrName = $arrCd[$i].",".$arrNm[$i].",".$arrDr[$i].",".$arrNa[$i].",".$arrCo[$i];
				break;
			}
		}
		return $lstrName;
	}
	
	//---------------------------------------
	// 機能概要 :番組メニュー作成
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_createMenu(){
		$lstrProgramName = "index.php";
		$lstrOut     = "";
		$laryProgram = array();
		$lstrOut .= "<table width=\"400\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$lstrOut .= "<tr class=\"subtitle\">";
		$lstrOut .= "<td width=\"100%\">番組名</td>"."\n";
		//$lstrOut .= "<td>詳細</td>"."\n";
		//$lstrOut .= "<td>番組名</td>"."\n";
		$lstrOut .= "</tr>"."\n";
		for ($i = 0;$i < C_MAX_PROGRAM_NUM; $i++){
        	$laryProgram = explode(",",$this->PHP_getProgram($i+1));
        	$id = intval($i)+1;
        	$lstrOut .= "<td><a href=\"$lstrProgramName?mode=3&program=$laryProgram[0]&directory=$laryProgram[2]\" class=\"triangle\">".($i+1).") ".$laryProgram[1]."</a></td></tr>";
        	//$lstrOut .= "<td align=\"center\" ><a href=\"#\" STYLE=\"text-decoration:none\" onclick=\"m_win('./html/comment.html?id=$id','$laryProgram[3]',400, 300);\"><span class=\"formPoint\">$laryProgram[3]</span></a></td></tr>";
        	//$lstrOut .= "<td align=\"center\" ><img onclick=\"javascript:void(window.open('./html/comment.html', '詳細', 'width=400, height=300, menubar=no, toolbar=no,border=0,scrollbars=no'));\" 
        	//			src=\"img/shousaihyouji.gif\" style=\"cursor:pointer;\" border=\"0\" alt=\"詳細\" width=\"40px\" height=\"20px\" ></td></tr>";
        	/*
        	if (($i+1)%3==1)        $lstrOut .= "<tr class=\"itiran\"><td><a href=\"$lstrProgramName?mode=3&program=$laryProgram[0]&directory=$laryProgram[2]\" class=\"triangle\">".$laryProgram[3]."</td>";
				elseif(($i+1)%3==0) $lstrOut .= "<td><a href=\"$lstrProgramName?mode=3&program=$laryProgram[0]&directory=$laryProgram[2]\" class=\"triangle\">".$laryProgram[3]."</td></tr>";
				else                $lstrOut .= "<td><a href=\"$lstrProgramName?mode=3&program=$laryProgram[0]&directory=$laryProgram[2]\" class=\"triangle\">".$laryProgram[3]."</td>";
			*/
		}
		if (substr($lstrOut,-5) == "</tr>") $lstrOut .= "</table>";
		else $lstrOut .= "</tr></table>";
        return $lstrOut;
	}
    //---------------------------------------
	// 機能概要 :サーバ選択ページ
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_getServer($pitem) {
		//安値
		$arrCd[1]                 = "1";               
		$arrNm[1]                 = "yasunehonpo.com";
		$arrIp['yasunehonpo'][1]  = "180.189.34.207";
		$arrPt['yasunehonpo'][1]  = "22";
		$arrDr[1]                 = C_YASUNEHONPO_DICRECTORY;
	    //ゼロオク
		$arrCd[2]                 = "2";
		$arrNm[2]                 = "zerooku.com";
		$arrIp['zerooku'][1]      = "180.189.34.202";
		$arrPt['zerooku'][1]      = "10101";
		$arrPt['zerooku'][2]      = "10102";
		$arrPt['zerooku'][3]      = "10103";
		$arrPt['zerooku'][4]      = "10104";
		$arrDr[2]                 = C_ZEROOKU_DICRECTORY;
		//ギガオク
		$arrCd[3]                 = "3";
		$arrNm[3]                 = "gigaoku.jp";
		$arrIp['gigaoku'][1]      = "180.189.34.203";
		$arrPt['gigaoku'][1]      = "22";
		$arrDr[3]                 = C_GIGAOKU_DICRECTORY;
		//ポケオク
		$arrCd[4]                 = "4";
		$arrNm[4]                 = "poke-oku.jp";
		$arrIp['poke-oku'][1]     = "180.189.34.201";
		$arrPt['poke-oku'][1]     = "22";
		$arrDr[4]                 = C_POKEOKU_DICRECTORY;
		//メガオク
		$arrCd[5]                 = "5";
		$arrNm[5]                 = "megaoku.jp";
		$arrIp['megaoku'][1]      = "203.86.239.10";
		$arrPt['megaoku'][1]      = "50";
		$arrDr[5]                 = C_MEGAOKU_DICRECTORY;
		//いただきオークション
		$arrCd[6]                 = "6";
		$arrNm[6]                 = "i-oku.com";
		$arrIp['i-oku'][1]        = "180.189.34.204";
		$arrPt['i-oku'][1]        = "22";
		$arrDr[6]                 = C_IOKU_DICRECTORY;
		//オー得
		$arrCd[7]                 = "7";
		$arrNm[7]                 = "o-toku.jp";
		$arrIp['o-toku'][1]       = "180.189.34.205";
		$arrIp['o-toku'][2]       = "180.189.34.220";
		$arrPt['o-toku'][1]       = "22";
		$arrDr[7]                 = C_OTOKU_DICRECTORY;
		//ジョイオク
		$arrCd[8]                 = "8";
		$arrNm[8]                 = "joyoku.jp";
		$arrIp['joyoku'][1]       = "180.189.34.208";
		$arrPt['joyoku'][1]       = "22";
		$arrDr[8]                 = C_JOYOKU_DICRECTORY;
		//ハチオク
		$arrCd[9]                 = "8";
		$arrNm[9]                 = "hachioku.jp";
		$arrIp['hachioku'][9]     = "210.175.62.105";
		$arrPt['hachioku'][9]     = "22";
		$arrDr[9]                 = C_HACHIOKU_DICRECTORY;
		//バリューマーケット
		$arrCd[10]                = "10";
		$arrNm[10]                = "v-market.jp";
		$arrIp['v-market'][1]     = "180.189.34.241";
		$arrPt['v-market'][1]     = "10001";
		$arrPt['v-market'][2]     = "10002";
		$arrPt['v-market'][3]     = "10003";
		$arrPt['v-market'][4]     = "10011";
		$arrDr[10]                = C_VMARKET_DICRECTORY;
		//ナウオク
		$arrCd[11]                = "11";
		$arrNm[11]                = "now-oku.net";
		$arrIp['now-oku'][1]      = "202.215.248.150";
		$arrPt['now-oku'][1]      = "22";
		$arrDr[11]                = C_NOWOKU_DICRECTORY;
		//マル得
		$arrCd[12]                = "12";
		$arrNm[12]                = "marutoku.net";
		$arrIp['marutoku'][1]     = "27.96.41.35";
		$arrPt['marutoku'][1]     = "22";
		$arrDr[12]                = C_MARUTOKU_DICRECTORY;
		//ゼロワン
		$arrCd[13]                = "13";
		$arrNm[13]                = "zero-01.net";
		$arrIp['zero-01'][1]      = "61.197.194.54";
		$arrIp['zero-01'][2]      = "61.197.194.55";
		$arrPt['zero-01'][1]      = "22";
		$arrDr[13]                = C_ZERO01_DICRECTORY;
		//iBid
		$arrCd[14] = "14";
		$arrNm[14]                = "ibid-auction.jp";
		$arrIp['ibid-auction'][1] = "122.220.221.44";
		$arrIp['ibid-auction'][2] = "122.220.221.45";
		$arrPt['ibid-auction'][1] = "22";
		$arrDr[14]                = C_IBIDAUCTION_DICRECTORY;
		//アゲオク
		$arrCd[15]                = "15";
		$arrNm[15]                = "age-oku.jp";
		$arrIp['age-oku'][1]      = "180.189.34.231";
		$arrPt['age-oku'][1]      = "22";
		$arrDr[15]                = C_AGEOKU_DICRECTORY;
		//ハッピーおく
		$arrCd[16]                = "16";
		$arrNm[16]                = "happy-oku.jp";
		$arrIp['happy-oku'][1]    = "59.106.169.23";
		$arrPt['happy-oku'][1]    = "22";
		$arrDr[16]                = C_HAPPYOKU_DICRECTORY;
		//
		$arrCd[17]                = "17";
		$arrNm[17]                = "p-off.jp";
		$arrIp['p-off'][1]        = "112.78.206.222";
		$arrIp['p-off'][2]        = "112.78.206.223";
		$arrPt['p-off'][1]        = "22";
		$arrDr[17]                = C_POFF_DICRECTORY;
		//あんしん
		$arrCd[18]                = "18";
		$arrNm[18]                = "anshin-auc.jp";
		$arrIp['anshin-auc'][1]   = "180.189.34.222";
		$arrIp['anshin-auc'][2]   = "180.189.34.223";
		$arrPt['anshin-auc'][1]   = "22";
		$arrDr[18]                = C_ANSHINAUC_DICRECTORY;
		
		$lstrName = "";
		for ($i=1; $i <= $arrCd; $i++) {
			if ($pitem == $arrNm[$i]) {
				$lstrName = $arrCd[$i].",".$arrWb[$i].",".$arrDb[$i];
				//$laryName[] = 
				break;
			}
		}
		
		return $lstrName;
	}
	//---------------------------------------
	// 機能概要 :サーバ選択メニュー作成
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_createHost_nop($pitem){
		$lstrOut = "";
		
		switch ($pitem){
			case '1':
			case '3':
			case '4':
			case '5':
			case '7':
			case '8':
			case '9':
			case '11':
			case '12':
			case '13':
			case '14':
			case '15':
			case '16':
			case '17':
			case '18':
				$lstrProgramName = "index.php";
				$lstrOut = "";
				$laryProgram = array();
				for ($i = 1;$i <= C_MAX_PROGRAM_NUM; $i++){
        			$laryProgram = explode(",",$this->PHP_getProgram($i));
        			$lstrOut .= "<a href=\"$lstrProgramName?mode=4&program=$laryProgram[0]\" class=\"triangle\">".$laryProgram[1]."</a><br><br>";
				}
        		return $lstrOut;
        		break;
		}
	}
    //---------------------------------------
	// 機能概要 :
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_createData($pitem){
		$lstrDirectory = "";
		$lstrOut = "";
		$lstrProgramName = "index.php";
		$i = 0;
		$lstrOut .= "<table width=\"960\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$lstrOut .= "<tr class=\"subtitle\">";
		$lstrOut .= "<td>ファイル名</td>"."\n";
		//$lstrOut .= "<td>ファイル名</td>"."\n";
		//$lstrOut .= "<td>ファイル名</td>"."\n";
		//$lstrOut .= "<td>ファイル名</td>"."\n";
		$lstrOut .= "</tr>"."\n";
		$lstrOut .= "<tr class=\"itiran\">"."\n";
		if ($dir = @ opendir( $pitem )){
			while ($fnm[] = @ readdir($dir));
  			@ closedir($dir);
  			rsort($fnm);
  			reset($fnm);
			$flg = false;
			//if ($dir = opendir( $pitem )){
			//	while ($fnm[] = readdir($dir));
  			//	closedir($dir);
  			//	rsort($fnm);
  			//	reset($fnm);
			while ($file = each($fnm)) {
				if ($file[1] != "." && $file[1] != ".." && $file[1] != ""){
					
					$lstrOut .= "<td><a href=\"$lstrProgramName?mode=5&directory=$pitem/$file[1]\" 
            							style=\"color:#3d67ce;text-decoration:underline;font-size:100%;\" target=\"_self\" width=800,height=200,status=no,scrollbars=yes,
            							directories=no,menubar=no,resizable=yes,toolbar=no class=\"triangle\">".$file[1]."</a></td></tr>";
					
					/*
					if (($i+1)%4==1)    $lstrOut .= "<tr class=\"itiran\"><td><a href=\"$lstrProgramName?mode=5&directory=$pitem/$file[1]\" 
										style=\"color:#3d67ce;text-decoration:underline;font-size:100%;\" target=\"_brank\" width=800,height=1200,status=no,scrollbars=yes,
										directories=no,menubar=no,resizable=yes,toolbar=no class=\"triangle\">".$file[1]."</a></td>";
            		elseif(($i+1)%4==0) $lstrOut .= "<td><a href=\"$lstrProgramName?mode=5&directory=$pitem/$file[1]\" 
            							style=\"color:#3d67ce;text-decoration:underline;font-size:100%;\" target=\"_brank\" width=800,height=200,status=no,scrollbars=yes,
            							directories=no,menubar=no,resizable=yes,toolbar=no class=\"triangle\">".$file[1]."</a></td></tr>";
            		else                $lstrOut .= "<td><a href=\"$lstrProgramName?mode=5&directory=$pitem/$file[1]\" 
            						    style=\"color:#3d67ce;text-decoration:underline;font-size:100%;\" target=\"_brank\" width=800 ,height=1200 ,status=no,scrollbars=yes,
            						    directories=no,menubar=no,resizable=yes,toolbar=no class=\"triangle\">".$file[1]."</a></td>";
            		*/
            		$i++;
				}
        	}
		}
		if (substr($lstrOut,-5) == "</tr>") $lstrOut .= "</table>";
		else $lstrOut .= "</tr></table>";
		//closedir( $dir );
		return $lstrOut;
	}
	//---------------------------------------
	// 機能概要 :
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_createHost($pitem){
		$lstrDirectory   = "";
		$flg             = false;
		$lstrProgramName = "index.php";
		$lstrName        = substr($pitem,strrpos($pitem,"/")+1);
		if ($dir = opendir( $pitem )){
			while ($fnm[] = readdir($dir));
  			closedir($dir);
  			rsort($fnm);
  			reset($fnm);
			$flg = false;
			$lstrDirectory .= "<table width=\"400\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$lstrDirectory .= "<tr class=\"subtitle\">";
		    $lstrDirectory .= "<td width=\"100%\">ホスト名</td>"."\n";
		    //$lstrDirectory .= "<td width=\"30%\">詳細</td>"."\n";
		    $lstrDirectory .= "</tr>"."\n";
		    $lstrDirectory .= "<tr class=\"itiran\">"."\n";
			while ($file = each($fnm)) {
			//while (($file = readdir($dir)) !== false) {
				if ($file[1] != "." && $file[1] != ".." && $file[1] != "") {
                    switch ($pitem){
                    	case C_YASUNEHONPO_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU L5410 @ 2.33GHz 64<br>Linux version 2.6.33.5-112.fc13.x86_64 (mockbuild@x86-09.phx2.fedoraproject.org)<br>(gcc version 4.4.4 20100503 (Red Hat 4.4.4-2) (GCC) ) #1 SMP Thu May 27 02:28:31 UTC 2010<br>8GB<br>Fedora13";
                    		break;
                    	case C_ZEROOKU_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5355 @ 2.66GHz 64<br>Linux version 2.6.18-164.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-46)) #1 SMP Thu Sep 3 03:28:30 EDT 20090<br>8GB<br>CentOS5(-p 10101)";
                    		}else if ($file[1] == "web01"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5355 @ 2.66GHz 64<br>Linux version 2.6.18-164.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-46)) #1 SMP Thu Sep 3 03:28:30 EDT 2009<br>8GB<br>CentOS5(-p 10102)";
                    		}else if ($file[1] == "web02"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5355 @ 2.66GHz 64<br>Linux version 2.6.18-164.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-46)) #1 SMP Thu Sep 3 03:28:30 EDT 2009<br>8GB<br>CentOS5(-p 10103)";
                    		}else if ($file[1] == "web03"){
								$lstrComment = "Intel(R) Xeon(R) CPU X5355 @ 2.66GHz 64<br>Linux version 2.6.18-164.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-46)) #1 SMP Thu Sep 3 03:28:30 EDT 2009<br>8GB<br>CentOS5(-p 10104)";
                    		}else{}
                    		break;
                    	case C_GIGAOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU L5410 @ 2.33GHz 6<br>Linux version 2.6.18-194.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Fri Apr 2 14:58:14 EDT 2010<br>8GB<br>CentOS5";
                    		break;
                    	case C_POKEOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU L5410 @ 2.33GHz 6<br>Linux version 2.6.18-164.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-46)) #1 SMP Thu Sep 3 03:28:30 EDT 2009<br>8GB<br>CentOS5";
                    		break;
                    	case C_MEGAOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU E5504 @ 2.00GHz 64<br>Linux version 2.6.30.10-105.2.23.fc11.x86_64 (mockbuild@x86-01.phx2.fedoraproject.org)<br>(gcc version 4.4.1 20090725 (Red Hat 4.4.1-2) (GCC) ) #1 SMP Thu Feb 11 07:06:34 UTC 2010<br>16GB<br>Fedora11(-p 50)";
                    		break;
                    	case C_IOKU_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5355 @ 2.66GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5(-p 10011)";
                    		}else if ($file[1] == "web01"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU L5410 @ 2.33GHz 64 4コア<br>Linux version 2.6.18-194.8.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Thu Jul 1 19:04:48 EDT 2010<br>8GB<br>CentOS5(-p 10001)";
                    		}else if ($file[1] == "web02"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X3450 @ 2.67GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>8GB<br>CentOS5(-p 10002)";
                    		}else{}
                    		break;
                    	case C_OTOKU_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5355 @ 2.66GHz 64<br>Linux version 2.6.18-194.11.3.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Mon Aug 30 16:19:16 EDT 2010<br>8GB<br>CentOS5";
                    		}else if ($file[1] == "web"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU L5410 @ 2.33GHz 64<br>Linux version 2.6.18-194.8.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Thu Jul 1 19:04:48 EDT 2010<br>8GB<br>CentOS5";
                    		}else{}
                    		break;
                    	case C_JOYOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU L5410 @ 2.33GHz 64<br>Linux version 2.6.33.5-112.fc13.x86_64 (mockbuild@x86-09.phx2.fedoraproject.org)<br>(gcc version 4.4.4 20100503 (Red Hat 4.4.4-2) (GCC) ) #1 SMP Thu May 27 02:28:31 UTC 2010<br>8GB<br>CentOS5";
                    		break;
                    	case C_HACHIOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU E5620 @ 2.40GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>12GB<br>CentOS5";
                    		break;
                    	case C_VMARKET_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5460 @ 3.16GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5(-p 10011)";
                    		}else if ($file[1] == "web01"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5460 @ 3.16GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org) (gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5(-p 10001)";
                    		}else if ($file[1] == "web02"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5460 @ 3.16GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org) (gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5(-p 10002)";
                    		}else if ($file[1] == "web03"){
								$lstrComment = "Intel(R) Xeon(R) CPU X5460 @ 3.16GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org) (gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5(-p 10003)";
                    		}else{}
                    		break;
                    	case C_NOWOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU E5506 @ 2.13GHz 64<br>Linux version 2.6.29.4-167.fc11.x86_64 (mockbuild@xenbuilder4.fedora.phx.redhat.com) (gcc version 4.4.0 20090506 (Red Hat 4.4.0-4) (GCC) ) #1 SMP Wed May 27 17:27:08 EDT 2009<br>8GB<br>CentOS5";
                    		break;
                    	case C_MARUTOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU E5506 @ 2.13GHz 64<br>Linux version 2.6.30.10-105.2.23.fc11.x86_64 (mockbuild@x86-01.phx2.fedoraproject.org) (gcc version 4.4.1 20090725 (Red Hat 4.4.1-2) (GCC) ) #1 SMP Thu Feb 11 07:06:34 UTC 2010<br>16GB<br>CentOS5";
                    		break;
                    	case C_ZERO01_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU E5506 @ 2.13GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>8GB<br>CentOS5";
                    		}else if ($file[1] == "web"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU E5506 @ 2.13GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>8GB<br>CentOS5";
                    		}else{}
                    		break;
                    	case C_IBIDAUCTION_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X3430 @ 2.40GHz 64<br>Linux version 2.6.18-194.17.1.el5PAE (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Wed Sep 29 13:31:51 EDT 2010<br>32GB<br>CentOS5";
                    		}else if ($file[1] == "web"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X5560 @ 2.80GHz 64<br>Linux version 2.6.18-194.17.1.el5PAE (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Wed Sep 29 13:31:51 EDT 2010<br>8GB<br>CentOS5";
                    		}else{}
                    		break;
                    	case C_AGEOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU X5460 @ 3.16GHz 64<br>Linux version 2.6.18-194.17.4.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Mon Oct 25 15:50:53 EDT 2010<br>16GB<br>CentOS5";
                    		break;
                    	case C_HAPPYOKU_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU X5560 @ 2.80GHz 64 × 16processor<br>Linux version 2.6.18-194.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Fri Apr 2 14:58:14 EDT 2010<br>8GB<br>CentOS5";
                    		break;
                    	case C_POFF_DICRECTORY:
                    		if ($file[1]       == "db"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU L5506 @ 2.13GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5";
                    		}else if ($file[1] == "web"){
                    			$lstrComment = "Intel(R) Xeon(R) CPU X3450 @ 2.67GHz 64<br>Linux version 2.6.18-194.26.1.el5 (mockbuild@builder10.centos.org)<br>(gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Tue Nov 9 12:54:20 EST 2010<br>16GB<br>CentOS5";
                    		}else{}
                    		break;
                    	case C_ANSHINAUC_DICRECTORY:
                    		$lstrComment = "Intel(R) Xeon(R) CPU X3430 @ 2.40GHz 64<br>Linux version 2.6.18-194.el5 (mockbuild@builder10.centos.org) (gcc version 4.1.2 20080704 (Red Hat 4.1.2-48)) #1 SMP Fri Apr 2 14:58:14 EDT 2010<br>8GB<br>CentOS5";
                    		break;
                    }
					$lstrDirectory .= "<tr class=\"itiran\"><td><a href=\"$lstrProgramName?mode=4&directory=$pitem/$file[1]\" target=\"_self\"
					style=\"color:#3d67ce;text-decoration:underline;font-size:100%;\" class=\"triangle\">".$file[1]."</a></td></tr>";
					//$lstrDirectory .= "<td align=\"center\" >
					//<a href=\"#\" STYLE=\"text-decoration:none\" onclick=\"m_win('./html/comment.html?comment=$lstrComment','ホスト選択',400, 300);\"><span class=\"formPoint\">$file[1]</span></a></td></tr>";
					//<a href=\"javascript:void(window.open('./html/comment.html', '詳細', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes'));\"><img src=\"img/shousaihyouji.gif\" border=\"0\" alt=\"詳細\" width=\"40px\" height=\"20px\" ></a></td></tr>";
					//style=\"color:#3d67ce;text-decoration:underline;font-size:100%;\" class=\"triangle\"><span class=\"formPoint\">".$file[1]."</span></a><br>".$lstrComment."</td></tr>";
				}
			}
			if (substr($lstrDirectory,-5) == "</tr>") $lstrDirectory .= "</table>";
			else $lstrDirectory .= "</tr></table>";
		}
		return $lstrDirectory;
	}
	//---------------------------------------
	// 機能概要 :
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_createFile($pitem){
		$lstrDirectory = "";
		while ($fnm[] = readdir($dir));
  		closedir($dir);
  		rsort($fnm);
  		reset($fnm);
		$flg = false;
		if ($dir = opendir( $pitem )){
			while ($fnm[] = readdir($dir));
  			closedir($dir);
  			rsort($fnm);
  			reset($fnm);
  			while ($file = each($fnm)) {
			//while (($file = readdir($dir)) !== false) {
				if ($file != "." && $file != ".." && $file != "") {
					$lstrDirectory .= "<a href=\"$lstrProgramName?mode=4&directory=$pitem/$file\" class=\"triangle\">".$file."</a><br><br>";
					//$lstrDirectory .= "<A href=\"$file\">$file</A><br>";
				}
			}
		}
		closedir( $dir );
		return $lstrDirectory;
	}
	//---------------------------------------
	// 機能概要 :
	// 引数           :
    //　戻り値　　 :
    // 備考           :
	//---------------------------------------
	function PHP_getContents($pitem){
		$buffer = "";
		$fp = fopen($pitem, "r");
		$buffer .="<table width=\"400\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$buffer .="<tr class=\"subtitle\">";
		$buffer .="<td width=\"100%\">記録内容</td>"."\n";
		$buffer .= "</tr>"."\n";
		while( ! feof($fp) ) {
			$buffer .= "<tr class=\"itiran\"><td>";
			$buffer .= PHP_MetaEscape(fgets( $fp, 4096 ));
  			$buffer .= "</td></tr>";
		}
		fclose ($fp);
		$buffer .= "</table>";
		return $buffer;
	}
}
/*********************
 * 例外インターフェース
 *********************/
interface IException 
	{
    	/* Protected methods inherited from Exception class */
    	public function getMessage();                 // Exception message 
    	public function getCode();                    // User-defined Exception code
    	public function getFile();                    // Source filename
    	public function getLine();                    // Source line
    	public function getTrace();                   // An array of the backtrace()
    	public function getTraceAsString();           // Formated string of trace
    
    	/* Overrideable methods inherited from Exception class */
    	public function __toString();                 // formated string for display
    	public function __construct($message = null, $code = 0);
}
/********************
 * 例外抽象クラス 
 ********************/
abstract class CustomException extends Exception implements IException
{
    protected $message = 'Unknown exception';     // Exception message
    private   $string;                            // Unknown
    protected $code    = 0;                       // User-defined exception code
    protected $file;                              // Source filename of exception
    protected $line;                              // Source line of exception
    private   $trace;                             // Unknown

    public function __construct($message = null, $code = 0)
    {
        if (!$message) {
            throw new $this('Unknown '. get_class($this));
        }
        parent::__construct($message, $code);
    }
    
    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
                                . "{$this->getTraceAsString()}";
    }
}
/****************
 * カスタム例外
 ****************/
class MyCustomException extends CustomException
{
    public function __construct($message = null, $code = 0)
    {
        parent::__construct($message, $code);
    }
}
/***************
 * カスタム例外（NotFound）
 ***************/
class NotFoundException extends CustomException
{
    public function __construct($message = null, $code = 0)
    {
        parent::__construct($message, $code);
    }
}
/***************
 * カスタム例外(会員情報不正)
 ***************/
class MemberException extends CustomException
{
    public function __construct($message = null, $code = 0)
    {
        parent::__construct($message, $code);
    }
}
?>