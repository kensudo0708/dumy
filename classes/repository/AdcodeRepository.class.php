<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import('classes.model.AdcodeModel');
import(FRAMEWORK_DIR.'.cache.AbstractRepository');
/**
 * Description of AdcodeRepository
 *
 * @author mw
 */
class AdcodeRepository extends AbstractRepository {
    //put your code here
    private static $adcodePrefix="adcode_info";


    /**
     * アプリケーション起動する時、実行される、抽象的なメソッド、サブクラスがオーバーライド
     */
    public function startup() {
        parent::createEngine();
        $model=new AdcodeModel();
        //広告コードの情報を取得し、キャッシュする
        $infos=$model->getAdcodeInfo();
        foreach ($infos as $obj)
            parent::$cache->addElement(self::$adcodePrefix,$obj->fk_adcode_id,$obj);
    }

    /**
     *指定されたIDで広告コードの情報を取得する
     * @param <int> $id fk_adcode_id
     * @return <object> 広告コードの情報
     */
    public static function getAdcodeById($id) {
        parent::createEngine();
        $info=parent::$cache->getElement(self::$adcodePrefix,$id);
        if($info==null) {
            $model=new AdcodeModel();
            $info=$model->getAdcodeInfoById($id);
            if($info!=NULL)
                parent::$cache->addElement(self::$adcodePrefix,$info->fk_adcode_id,$info);
        }
        return $info;
    }

    /**
     *指定されたIDで広告コードの情報を取得する (未定）確認？？？
     * <string> $code f_adcode
     * @return <object> 広告コードの情報
     */
    public static function getAdcodeInfoByAdcode($code) {
        parent::createEngine();
        $info=parent::$cache->getElement(self::$adcodePrefix,$id);
        if($info==null) {
            $model=new AdcodeModel();
            $info=$model->getAdcodeInfoById($id);
            if($info!=NULL)
                parent::$cache->addElement(self::$adcodePrefix,$info->fk_adcode_id,$info);
        }
        return $info;
    }


    /**
     *広告からのサインログを保存
     * @param <type> $adcodeId
     * @param <type> $memberId
     * @return <type>
     */
    public static function saveAdcodeSignLog($adcodeId,$memberId){
        $info=self::getAdcodeById($adcodeId);
        $model=new AdcodeModel();
        return $model->saveAdcodeSignLog($adcodeId, $memberId,$info->fk_price_id);
    }
    
}
?>
