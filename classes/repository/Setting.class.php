<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import(FRAMEWORK_DIR.'.cache.Repository');
/**
 * Description of SettingRepository
 *
 * @author ark
 */
class Setting implements Repository {
    //put your code here
    private static $prefix="system_setting";
    private static $cache;
    private static $model;
    /**
     *キャッシュエンジンを設置する
     * @param <CacheEngine> $engine CacheEngineのインスタンス
     */
    public function setCacheEngine($engine) {
        self::$cache=$engine;
    }
    
    public function startup() {
//        self::$cache = createInstance(self::$cache);
//        self::$model=new SettingModel();
//        $settingInfos=self::$model->getSettingInfo();
//        foreach ($settingInfos as $obj) {
//            self::$cache->addElement(self::$prefix,$obj->fk_products_id,$obj);
//        }
    }

    private static function createEngine(){
        self::$cache = createInstance(Config::value("CACHE_ENGINE_CLASS"));
    }

    /**
     *システムの設定値を取得（仮）
     * @param <type> $key
     * @return <type>
     */
    public static function value($key){
        $data=include("setting.php");
        return $data[$key];
    }
}
?>
