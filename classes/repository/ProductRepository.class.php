<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.ProductModel');
import(FRAMEWORK_DIR.'.cache.AbstractRepository');
/**
 * Description of ProductRepository
 *
 * @author mw
 */
class ProductRepository extends AbstractRepository {
    //put your code here

    private static $product_master_prefix="product_master_info";
    private static $product_open_prefix="product_open_info";
    private static $auction_type_prefix="auction_type";

    private static $product_open_area_a_prefix="product_open_area_a";
    private static $product_open_area_b_prefix="product_open_area_b";

    private static $in_area_prefix="in_area";
    private static $out_area_prefix="out_area";
    
    private static $out_area_a_flag="out_area_a";
    private static $out_area_b_flag="out_area_b";

    /**
     * アプリケーション起動する時、実行される、抽象的なメソッド、サブクラスがオーバーライド
     */
    public function startup() {
        parent::createEngine();
//        $model=new ProductModel();
//        //商品マスタから開催中オークション商品の詳細情報を取得、キャッシュする
//        $productsInfos=$model->getAuctionProductInfo();
//        foreach ($productsInfos as $obj)
//            parent::$cache->addElement(self::$product_master_prefix,$obj->fk_products_id,$obj);
    }


    public static function getProductByIdArray($ids) {
        parent::createEngine();
        $ids=explode(",", $ids);
        $products=array();
        $in = self::getInFlagValue();
        $area="";
        $out="";
        if($in=="a"){
            $area=self::$product_open_area_b_prefix;
            $out=self::$out_area_b_flag;
        }
        else{
            $area=self::$product_open_area_a_prefix;
            $out=self::$out_area_a_flag;
        }
//        $out = $in =="a" ? "b" : "a";
        $flag = self::getOutFlayValue($out);
        $flag++;
//        parent::$cache->addVariable(self::$out_area_prefix,$out);
        foreach ($ids as $id) {
            $product=parent::$cache->getElement($area,$id);
//            var_dump($product);
            if(!empty ($product))
                array_push($products, $product);
        }
        $flag--;
        parent::$cache->addVariable($out,$flag);
        return $products;
    }

    public static function getInFlagValue() {
        return parent::$cache->getVariable(self::$in_area_prefix);
//        $in = parent::$cache->getVariable(self::$in_area_prefix);
//        if(empty ($in)){
//            $in ="a";
//            parent::$cache->addVariable(self::$in_area_prefix,$in);
//        }
//        return $in;
    }
    public static function getOutFlayValue($area) {
//        $out = parent::$cache->getVariable(self::$out_area_prefix);
//        if(empty ($out)) {
//            $out ="a";
//            parent::$cache->addVariable(self::$out_area_prefix,$out);
//        }
//        return $out;
        $out = parent::$cache->getVariable($area);
        return $out===FALSE || empty ($out) ? 0 :$out;
    }

//    public static function getOutFlayValue() {
//        $out = parent::$cache->getVariable(self::$out_area_prefix);
//        if(empty ($out)) {
//            $out ="a";
//            parent::$cache->addVariable(self::$out_area_prefix,$out);
//        }
//        return $out;
//    }


    public static function getProductBidLog($id) {
        parent::createEngine();

    }

    /**
     * *開催中の商品情報を取得（マスタ、カテゴリ、オークションの情報を含む）
     */
    public static function getAuctionProductInfo() {
        parent::createEngine();
        $products=parent::$cache->getElements(self::$product_master_prefix);
        if($products==null) {
            $model=new ProductModel();
            $products=$model->getAuctionProductInfo();
            foreach ($products as $obj)
                parent::$cache->addElement(self::$product_master_prefix,$obj->fk_products_id,$obj);
        }
        return $products;
    }

    /**
     *指定されたIDで開催中の商品情報を取得（マスタとオークションの情報を含む）
     * @return <object> 商品情報Object
     */
    public static function getAuctionProductById($id) {
        parent::createEngine();
        $product=parent::$cache->getElement(self::$product_master_prefix,$id);
        if($product==null) {
            $model=new ProductModel();
            $product=$model->getAuctionProductById($id);
            if($product!=NULL)
                parent::$cache->addElement(self::$product_master_prefix,$product->fk_products_id,$product);
        }
        return $product;
    }


    /**
     *開催中の商品の数
     * @return <type>
     */
    public static function getAuctionProductCount() {
        self::createEngine();
        $size=parent::$cache->getCacheSize(self::$product_master_prefix);
        if($size==0) {
            $model=new ProductModel();
            $products=$model->getAuctionProductInfo();
            foreach ($products as $obj)
                parent::$cache->addElement(self::$product_master_prefix,$obj->fk_products_id,$obj);
            return count($products);
        }
        return $size;

    }

    /**
     *指定されたIDで商品マスター情報を取得する、取得したObjectをキャッシュに保存する
     * @param <int> $id 商品ID
     * @return <object> 商品マスター情報
     */
    public static function getProductMasterById($id) {
        self::createEngine();
        $product=parent::$cache->getElement(self::$product_master_prefix,$id);
        if($product==NULL) {
            $model=new ProductModel();
            $product=$model->getProductMasterById($id);
            if($product!=NULL)
                parent::$cache->addElement(self::$product_master_prefix,$product->fk_products_id,$product);
        }
        return $product;
    }

    /**
     *商品情報の削除処理
     * @param <int> $id 商品ID
     */
    public static function removeProductMaster($id) {
        self::createEngine();
        $model=new ProductModel();
        if($model->removeProduct($id))
            parent::$cache->removeElement(self::$product_master_prefix,$id);
    }

    /**
     *指定されたIDでオークションタイプを取得、取得したObjectをキャッシュに保存する
     * @param <int> $id タイプID
     * @return <object>
     */
    public static function getAuctionTypeById($id) {
        self::createEngine();
        $type=parent::$cache->getElement(self::$auction_type_prefix,$id);
        if($type==NULL) {
            $model=new ProductModel();
            $type=$model->getAuctionTypeById($id);
            if($type!=NULL)
                parent::$cache->addElement(self::$auction_type_prefix,$id,$type);
        }
        return $type;

    }


}
?>
