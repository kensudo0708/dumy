<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import('classes.model.MemberModel');
import(FRAMEWORK_DIR.'.cache.Repository');
/**
 * Description of MemberRepository
 *
 * @author mw
 */
class MemberRepository implements Repository {
    //put your code here
    private static $memberPrefix="member_master_info";
    private static $groupPrefix="member_group_info";
    private static $cache;

    
    public function setCacheEngine($engine) {
        self::$cache=$engine;
    }
    public function startup() {
        $model=new MemberModel();
        
    }

    private static function createEngine(){
        self::$cache = createInstance(Config::value("CACHE_ENGINE_CLASS"));
    }

    /**
     *idでメンバー情報を取得(master情報も含む)
     * @param <type> $id
     * @return <type>
     */
    public static function getMemberInfoById($id){
        self::createEngine();
        $member=self::$cache->getElement(self::$memberPrefix,$id);
        $model=new MemberModel();
        if($member==NULL) {
            $member=$model->getMemberMasterById($id);
            if($member!=NULL)
                self::$cache->addElement(self::$memberPrefix,$member->fk_member_id,$member);
        }
        $member=Utils::objectMerge($member, $model->getMemberById($id));
        return $member;
    }

    public static function getMemberMasterByLoginId($id){
        self::createEngine();
        $member=self::$cache->getElement(self::$memberPrefix,$id);
        if($member==NULL) {
            $model=new MemberModel();
            $member=$model->getMemberMasterByLoginId($id);
            if($member!=NULL)
                self::$cache->addElement(self::$memberPrefix,$member->fk_products_id,$member);
        }
        return $member;
    }

    /**
     *会員グループ情報を取得する
     * @return <array>
     */
    public static function getMemberGroups(){
        self::createEngine();
        $groups=self::$cache->getElements(self::$groupPrefix);
        if($groups==NULL){
            $groups=$model->getMemberGroups();
            if($groups!=NULL){
                foreach ($groups as $obj) 
                    self::$cache->addElement(self::$groupPrefix,$obj->fk_member_group_id,$obj);
            }
        }
        return $groups;
    }

    /**
     *指定されたIDで会員グルップ情報を取得する
     * @param <int> $id グルップID
     * @return <object> グルップ情報
     */
    public static function getMemberGroupById($id){
        self::createEngine();
        $group=self::$cache->getElement(self::$groupPrefix,$id);
        if($group==NULL) {
            $group=$model->getMemberGroupById($id);
            if($group!=NULL)
                self::$cache->addElement(self::$groupPrefix,$group->fk_member_group_id,$group);
        }
        return $member;
    }
    
}
?>
