<?php

/**
 *smartyテンプレートの絵文字対応のプラグイン
 * @param <type> $params
 * @param <type> $smarty
 */
function smarty_function_em($params, &$smarty) {

    if(isset ($params['c']) && isset ($_SESSION['MOBILE_PICTURE_CODE'])) {
        $pic=$_SESSION['MOBILE_PICTURE_CODE'];
        return isset($pic[$params['c']]) ? $pic[$params['c']] : null;
    }
    else
        return null;
}

?>
