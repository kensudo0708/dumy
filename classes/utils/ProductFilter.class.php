<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of ProductFilter
 *
 * @author ark
 */
class ProductFilter {
    //put your code here

    /**
     *初心者オークション商品フィルタ
     * @param <array> $products
     * @param <object> $member
     */
    public static function firstFilter($products,$member) {
        if(!is_array($products) || empty ($member))
            return $products;
        $type = DB::uniqueObject("SELECT * FROM t_auction_type WHERE f_bidder_flag=1");

        if(empty ($type))
            return $products;
        
        if($member->t_hummer_count > $type->f_bidder_param1) {
            $p=array();
            foreach ($products as $pro) {
                if($pro->auction_type != $type->fk_auction_type_id)
                    array_push($p, $pro);
            }
            return $p;
        }
        else
            return $products;
    }


    /**
     *指定された商品が初心者フィルタにフィルタされるかどうか、チェックする
     * @param <object> $product
     * @param <object> $member
     * @return <boolean>
     */
    public static function matchFirstFilter($product,$member) {
        if(empty($product) || empty ($member))
            return false;
        $product->auction_type = $product->fk_auction_type_id;
        $product=self::firstFilter(array($product), $member);
        return count($product)==0 ? true : false;
    }
}
?>
