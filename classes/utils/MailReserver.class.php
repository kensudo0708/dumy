<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * メール送信要求処理
 *
 * @author mw
 */
class MailReserver {
    //$f_type 要求タイプ
    const REGIST = 100;
    const FRIEND = 10;
    const EMAIL = 80;
    const PASSWORD = 50;
    const WINNER =90 ;
    const DELIVERY =70;

    /**
     * メール送信要求処理
     * @param <int> $f_type 要求タイプ(0=メルマガ,1=会員登録等)
     * @param <string> $f_subject 件名
     * @param <string> $f_body 本文
     * @param <string> $f_sendfrom 送信者
     * @param <string> $f_sendto 相手のメアド
     * @return <boolean> 成功の場合：true
     */
    public static function send($f_type,$f_subject,$f_body,$f_sendfrom,$f_sendto) {
        try {
            DB::begin();
            $sql ="INSERT INTO t_mail_request
                        (f_type, f_subject, f_body, f_sendfrom, f_sendto, f_status, f_tm_stamp)
                        VALUES
                        ('$f_type','$f_subject','$f_body','$f_sendfrom','$f_sendto',0,now())";
            $result = DB::executeNonQuery($sql);

//            $result = DB::executeNonQuery("INSERT INTO t_mail_request
//                (f_type, f_subject, f_body, f_sendfrom, f_sendto, f_status, f_tm_stamp)
//                VALUES
//                (?, ?, ?, ?, ?, 0, CURRENT_TIMESTAMP)",
//                    array($f_type,$f_subject,$f_body,$f_sendfrom,$f_sendto));
            DB::commit();
            return $result ==1 ? true : false;
        }
        catch (Exception $e) {
            DB::rollback();
        }
        return false;
    }

    /**
     *メール送信要求処理 (本文内容はテンプレートよりの場合）
     * @param <int> $f_type 要求タイプ(0=メルマガ,1=会員登録等)
     * @param <string> $f_subject 件名
     * @param <string> $tplFile テンプレートファイルパス
     * @param <array> $tplParams テンプレートの置換文字のパラメータ
     * @param <string> $f_sendfrom 送信者
     * @param <string> $f_sendto 相手のメアド
     * @return <boolean> 成功の場合：true
     */
    public static function sendTemplateMail($f_type,$f_subject,$tplFile,$tplParams,$f_sendfrom,$f_sendto) {
        $f_body=self::loadTemplateContent($tplFile,$tplParams);
        return self::send($f_type, $f_subject, $f_body, $f_sendfrom, $f_sendto);
    }

    private static function loadTemplateContent($tplFile,$params="") {
        $handle=fopen($tplFile, "r");
        $body=fread($handle, filesize($tplFile));
        fclose($handle);
        if(is_array($params)) {
            foreach ($params as $key=>$value) {
                if(stripos($body,$key)!==FALSE)
                    $body=str_ireplace("{%".$key."%}", $value, $body);
            }
        }
        return $body;
    }


    /**
     *会員本登録した後、メール送信処理
     * @param <object> $member 会員情報
     * @param <type> $isPc
     */
    public static function sendRegistSuccessMail($member,$isPc=true) {
        $to = $isPc ? $member->f_mail_address_pc : $member->f_mail_address_mb;
        $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/member_regist.html";
        $from = "From: ".INFO_MAIL."\nReturn-Path:".INFO_MAIL;
        $params=array("member_name"=>$member->f_handle,
                "point_name"=>POINT_NAME,
                "site_name"=>SITE_NAME,
                "login_id"=>$member->f_login_id,
                "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
        self::sendTemplateMail(self::REGIST,REGIST_SUCCESS_SUBJECT, $tpl, $params, $from, $to);
    }
}
?>
