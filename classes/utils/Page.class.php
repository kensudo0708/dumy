<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * お客の要望に対応するためのPageNumber生成するクラス
 *
 * @author ark
 */
class Page {
    //put your code here

        /**
     *モバイル用のページnumber
     * @param <type> $curPage
     * @param <type> $countPage
     * @param <type> $url
     * @param <type> $extendPage
     * @param <type> $style
     * @param <type> $pagetag
     * @return <type>
     */
    public static function getPageNumbersI($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;
        $t="";
        if(empty($style))
            $style='class="pagenum_box"';
        $curStyle='class="pagenum_box2"';
        $indexStyle='class="pagenum_box3"';
        $firstStr="<<先頭";
        $prevStr="<前";
        $nextStr="次>";
        $lastStr="最後>>";

        $curPage=$curPage < 1 ? 1 : $curPage;
        $countPage=$countPage < 1 ? 1 : $countPage;
        $curPage=$curPage > $countPage ? $countPage :$curPage;

        $mid=$curPage."/".$countPage;
        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $first=$curPage."/".$countPage;
        if($countPage > 1) {
            if($curPage != 1) {
                $firstUrl=$url.$pagetag."=1";
                $first="<a href=".$firstUrl."#pos1>$firstStr</a>";
                $prevUrl=$url.$pagetag."=".($curPage-1);
                $prev="<a href=".$prevUrl."#pos1>$prevStr</a>";
            }
            else {
                $first=$firstStr;
                $prevUrl=$url.$pagetag."=1";
                $prev=$prevStr;
            }
            $t=$first." ".$prev." ".$mid;
            if($curPage < $countPage) {
                $nextUrl=$url.$pagetag."=".($curPage+1);
                $next="<a href=".$nextUrl."#pos1>$nextStr</a>";
                $lastUrl=$url.$pagetag."=".$countPage;
                $last="<a href=".$lastUrl."#pos1>$lastStr</a>";
            }
            else {
                $nextUrl=$url.$pagetag."=".$countPage;
                $next=$nextStr;
                $last=$lastStr;
            }
            $t.=" ".$next." ".$last;
        }
        return $t;
    }
    
    /**
     *PC用のページNumber
     * @param <type> $curPage
     * @param <type> $countPage
     * @param <type> $url
     * @param <type> $extendPage
     * @param <type> $style
     * @param <type> $pagetag
     * @return <type>
     */
    public static function getPageNumbers($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        $nums=self::getPageNumber($curPage, $countPage,$url,$extendPage,$style,$pagetag);
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;
        $t="";
        if(empty($style))
            $style='class="pagenum_box"';
        $curStyle='class="pagenum_box2"';
        $indexStyle='class="pagenum_box3"';
        $firstStr="<<先頭";
        $prevStr="<前";
        $nextStr="次>";
        $lastStr="最後>>";

        $curPage=$curPage < 1 ? 1 : $curPage;
        $countPage=$countPage < 1 ? 1 : $countPage;
        $curPage=$curPage > $countPage ? $countPage :$curPage;

        $pageView="<span $indexStyle>".$curPage."/".$countPage."</span>";
        $mid=$nums;
        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $first="<span $style>".$curPage."/".$countPage."</span>";
        if($countPage > 1) {
            if($curPage != 1) {
                $firstUrl=$url.$pagetag."=1";
                $first="<span $style><a href=".$firstUrl."#pos1>$firstStr</a></span>";
                $prevUrl=$url.$pagetag."=".($curPage-1);
                $prev="<span $style><a href=".$prevUrl."#pos1>$prevStr</a></span>";
            }
            else {
                $first="<span $style >$firstStr</span>";
                $prevUrl=$url.$pagetag."=1";
                $prev="<span $style >$prevStr</span>";
            }
            $t=$first.$prev.$mid;
            if($curPage < $countPage) {
                $nextUrl=$url.$pagetag."=".($curPage+1);
                $next="<span $style ><a href=".$nextUrl."#pos1>$nextStr</a></span>";
                $lastUrl=$url.$pagetag."=".$countPage;
                $last="<span $style ><a href=".$lastUrl."#pos1>$lastStr</a></span>";
            }
            else {
                $nextUrl=$url.$pagetag."=".$countPage;
                $next="<span $style>$nextStr</span>";
                $last="<span $style >$lastStr</span>";
            }
            $t.=$next.$last.$pageView;
        }
        return $t;
    }

    private static function getPageNumber($curPage, $countPage,$url,$extendPage,$style="",$pagetag="page") {
        if (empty($pagetag))
            $pagetag = "page";
        $startPage = 1;
        $endPage = 1;

        if(empty($style)) {
            $style='class="pagenum_box"';
            $curStyle='class="pagenum_box2"';
//        $indexStyle='class="pagenum_box3"';
        }

        if (strpos($url,"?") > 0)
            $url .= "&";
        else
            $url .="?";
        $t1 = "<a $style href=".$url."&".$pagetag."=1";
        $t2 = "<a $style href=".$url."&".$pagetag."=".$countPage;
        $t1 .= ">&laquo;</a>";
        $t2 .= ">&raquo;</a>";
        if ($countPage < 1)
            $countPage = 1;
        if ($extendPage < 3)
            $extendPage = 2;

        if ($countPage > $extendPage) {
            if ($curPage - ($extendPage / 2) > 0) {
                if ($curPage + floor($extendPage / 2) < $countPage) {
                    $startPage = $curPage - floor($extendPage / 2);
                    $endPage = $startPage + $extendPage - 1;
                }
                else {
                    $endPage = $countPage;
                    $startPage = $endPage - $extendPage + 1;
                    $t2 = "";
                }
            }
            else {
                $endPage = $extendPage;
                $t1 = "";
            }
        }
        else {
            $startPage = 1;
            $endPage = $countPage;
            $t1 = "";
            $t2 = "";
        }

        $s="";// $t1;
        for ($i = $startPage;$i <= $endPage;$i++) {
            if ($i == $curPage) {
                $s.="<span $curStyle>";
                $s.=$i;
                $s.="</span>";
            }
            else {
                $s.="<span $style><a href=";
                $s.=$url;
                $s.=$pagetag;
                $s.="=";
                $s.=$i;

                $s.="#pos1>";
                $s.=$i;
                $s.="</a></span>";
            }
        }
//    $s.=$t2;
        return $s;
    }
}
?>
