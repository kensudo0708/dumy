    <?php
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
    */

    /**
     * 画面上の入力チェック処理
     *
     * @author mw
     */
    class InputCheck {

        /**
         *文字の最小桁数のチェック処理、チェックが通らない場合、
         * INPUT_MIN_LENGTHメッセージをメッセージの配列に追加する
         * @param <array> $messageArray メッセージの配列
         * @param <string> $str チェックする文字列
         * @param <int> $minLen 最小桁数
         * @param <string> $itemName 入力の項目名（チェックが通らない場合のメッセージのパラメータ）
         * @return <boolean> チェックが通らない場合：true[エラーメッセージが追加されたこと]
         */
        public static function addMinLenCheck(&$messageArray,$str,$minLen,$itemName) {
            if(Utils::lessMinLen($str, $minLen,DB_CHARSET)) {
                array_push($messageArray, sprintf(Msg::get("INPUT_MIN_LENGTH"),$itemName,$minLen));
                return true;
            }
            return false;
        }
        
        /**
         *文字の最大桁数のチェック処理、チェックが通らない場合、
         * INPUT_MAX_LENGTHメッセージをメッセージの配列に追加する
         * @param <array> $messageArray メッセージの配列
         * @param <string> $str チェックする文字列
         * @param <int> $maxLen 最大桁数
         * @param <string> $itemName 入力の項目名（チェックが通らない場合のメッセージのパラメータ）
         * @return <boolean> チェックが通らない場合：true[エラーメッセージが追加されたこと]
         */
        public static function addMaxLenCheck(&$messageArray,$str,$maxLen,$itemName) {
            if(Utils::moreMaxLen($str, $maxLen,DB_CHARSET)) {
                array_push($messageArray, sprintf(Msg::get("INPUT_MAX_LENGTH"),$itemName,$maxLen));
                return true;
            }
            return false;
        }

        /**
         *文字の桁数のbetweenチェック処理、チェックが通らない場合、
         * INPUT_BETWEEN_LENGTHメッセージをメッセージの配列に追加する
         * @param <array> $messageArray メッセージの配列
         * @param <string> $str チェックする文字列
         * @param <int> $minLen 最小桁数
         * @param <int> $maxLen 最大桁数
         * @param <string> $itemName 入力の項目名（チェックが通らない場合のメッセージのパラメータ）
         * @return <boolean> チェックが通らない場合：true[エラーメッセージが追加されたこと]
         */
        public static function addBetweenLenCheck(&$messageArray,$str,$minLen,$maxLen,$itemName) {
            if(Utils::betweenLen($str, $minLen, $maxLen, DB_CHARSET)) {
                array_push($messageArray, sprintf(Msg::get("INPUT_BETWEEN_LENGTH"),$itemName,$minLen,$maxLen));
                return true;
            }
            return false;
        }

        /**
         *登録入力チェック処理
         * @param <array> $p 画面に入力した内容
         * @return <boolean> チェック通った場合:true 、通らない場合：エラーメッセージ
         */
        public static function registInputCheck(&$p) {
            import("classes.model.MemberModel");
            $model = new MemberModel();
            $message=array();
            if(empty($p["handle"]))
                array_push($message, Msg::get("REGISTER_HANDLE_EMPTY"));
            else {
                if(self::isExistKeyWord($p["handle"]))
                    array_push($message, sprintf(Msg::get("INPUT_EXIST_KEYWORD"),Msg::get("HANDLE")));
                elseif(self::isFaultWord($p["handle"]))
                    array_push($message, sprintf(Msg::get("INPUT_EXIST_KEYWORD"),Msg::get("HANDLE")));
                else
                    self::addMaxLenCheck($message, $p["handle"], Msg::get("HANDLE_LEN"), Msg::get("HANDLE"));
            }
            if(empty($p["login_id"]))
                array_push($message, Msg::get("REGISTER_LOGINID_EMPTY"));
            else {
                if(!self::addMaxLenCheck($message, $p["login_id"], Msg::get("LOGINID_LEN"), Msg::get("LOGINID"))) {
                    if(!ereg("^[A-Za-z0-9]+$", $p["login_id"]))
                        array_push($message, Msg::get("REGISTER_LOGIN_INVALID_FORMAT"));
                }
            }
            if(empty($p["login_pass"]))
                array_push($message, Msg::get("REGISTER_PASS_EMPTY"));
            else
                self::addBetweenLenCheck($message, $p["login_pass"], PASSWORD_MIN_LEN, Msg::get("PASSWORD_LEN"), Msg::get("PASSWORD"));

            $isPc=Config::value("PC_DIR") == AgentInfo::getAgentDir();
            if($isPc) {
                //PCのみ、チェックする必要
                if(empty($p["mail_address"]))
                    array_push($message, Msg::get("REGISTER_EMAIL_EMPTY"));
                else {
                    if(!self::addMaxLenCheck($message, $p["mail_address"], Msg::get("EMAIL_LEN"), Msg::get("EMAIL"))) {
                        if(!Utils::isMailAddress($p["mail_address"]))
                            array_push($message, Msg::get("REGISTER_EMAIL_INVALID"));
                        else {
                            //blackメールかどうかチェック
                            if($model->isBlackMail($p["mail_address"]))
                                array_push($message, Msg::get("IS_BLACK_EMAIL"));
                        }
                    }
                }
            }

            if(!empty($p["tel_no"])) {
                if( !ereg("[0-9]+", $p["tel_no"]))
                    array_push($message, array_push($message, Msg::get("REGISTER_TEL_INVALID")));
                else
                    self::addMaxLenCheck($message, $p["tel_no"], Msg::get("TEL_LEN"), Msg::get("TEL"));
            }

            //誕生日のチェック
            if($p["birth_year"]=="00")
                array_push($message, Msg::get("REGISTER_BIRTH_YEAR_EMPTY"));
            if($p["birth_month"]=="00")
                array_push($message, Msg::get("REGISTER_BIRTH_MONTH_EMPTY"));
            if($p["birth_day"]=="00")
                array_push($message, Msg::get("REGISTER_BIRTH_DAY_EMPTY"));
            if($p["birth_year"]!="00" && $p["birth_month"]!="00" && $p["birth_day"]!="00") {
                if(checkdate($p["birth_month"], $p["birth_day"], $p["birth_year"])==false)
                    array_push($message, Msg::get("ERROR_BIRTH_DAY"));
            }

            if(!empty($p["onamae"]))
                self::addMaxLenCheck($message, $p["onamae"], Msg::get("NAME_LEN"), Msg::get("NAME"));
            if(MEM_REG_AREA_FLG==1){
                if($p["area"]==0)
                    array_push($message, Msg::get("REGISTER_AREA_NULL"));
            }
            if(MEM_REG_JOB_FLG==1){
                if($p["job"]==0)
                    array_push($message, Msg::get("REGISTER_JOB_NULL"));
            }

            $member_id = null;
            if($isPc)
            //PCのみ、仮登録済みの会員が再登録しているかどうか、チェックする
                $member_id = $model->getTempMemberIdByMail($p["mail_address"]);

            if(!empty ($member_id))
                $p['fk_member_id'] = $member_id;
            //仮登録済みの会員が再登録ではない場合、重複チェックが必要
            //ハンドル名とログインIDとメールアドの存在チェック
    //        if(!empty($p["handle"]) && !empty($p["login_id"]) && !empty($p["mail_address"])) {
            if(!empty($p["handle"]) && $model->isExistedHandle($p["handle"],$member_id))
                array_push($message, Msg::get("REGISTER_HANDLE_EXISTED"));
            if(!empty($p["login_id"]) && $model->isExistedLoginId($p["login_id"],$member_id))
                array_push($message, Msg::get("REGISTER_LOGINID_EXISTED"));
            if(!empty($p["mail_address"]) && empty($member_id) && $model->isExistedEmail($p["mail_address"],$member_id))
                array_push($message, Msg::get("REGISTER_EMAIL_EXISTED"));
    //        }

//G.Chin AWKC-216 2010-12-09 add sta
            //PCのみ、同一IPの会員が再登録しているかどうか、チェックする
			if($isPc)
			{
				if(FORBID_REG_PCMEMIP_FLAG == 1)
				{
					if($model->isExistedIp($_SERVER['REMOTE_ADDR']))
						array_push($message, Msg::get("REGISTER_IP_EXISTED"));
				}
			}
//G.Chin AWKC-216 2010-12-09 add end

            if(empty($p["agreement"]) || $p["agreement"] !="1")
                array_push($message, Msg::get("REGISTER_AGREEMENT_NO"));

            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;
        }


        /**
         *マイページの配送先入力チェック
         * @param <array> $p 画面に入力した内容
         * @return <boolean> チェック通った場合:true 、通らない場合：エラーメッセージ
         */
        public static function myAddressinputCheck($p) {
            $message=array();

            if(empty($p["f_name"]))
                array_push($message, Msg::get("MEMBER_ADDRESS_NAME"));
            else
                self::addMaxLenCheck($message, $p["f_name"], Msg::get("ADDRESS_NAME_LEN"), Msg::get("ADDRESS_NAME"));
            if(empty($p["f_tel_no"]))
                array_push($message, Msg::get("REGISTER_TEL_EMPTY"));
            else {
                if( !ereg("[0-9]+", $p["f_tel_no"]))
                    array_push($message, Msg::get("REGISTER_TEL_INVALID"));
                else
                    self::addMaxLenCheck($message, $p["f_tel_no"], Msg::get("TEL_LEN"), Msg::get("TEL"));
            }

            if(empty($p["f_post_code"]))
                array_push($message, Msg::get("REGISTER_POST_EMPTY"));
            else {
                if( !ereg("[0-9]+", $p["f_post_code"]) )
                    array_push($message, Msg::get("REGISTER_POST_INVALID"));
                else
                    self::addMaxLenCheck($message, $p["f_post_code"], Msg::get("POST_LEN"), Msg::get("POST"));
            }
            if(isset($p["fk_perf_id"]) && $p["fk_perf_id"]=="0")
                array_push($message, Msg::get("MEMBER_ADDRESS_PERF"));

            if(empty($p["f_address1"]))
                array_push($message, Msg::get("REGISTER_ADDRESS1_EMPTY"));
            else
                self::addMaxLenCheck($message, $p["f_address1"], Msg::get("ADDRESS1_LEN"), Msg::get("ADDRESS1"));
            if(empty($p["f_address2"]))
                array_push($message, Msg::get("REGISTER_ADDRESS2_EMPTY"));
            else
                self::addMaxLenCheck($message, $p["f_address2"], Msg::get("ADDRESS2_LEN"), Msg::get("ADDRESS2"));

            if(!empty($p["f_address3"]))
                self::addMaxLenCheck($message, $p["f_address3"], Msg::get("ADDRESS3_LEN"), Msg::get("ADDRESS3"));

            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;
        }


        /**
         *マイページのprofile変更の入力チェック
         * @param <array> $p 画面に入力した内容
         * @return <boolean> チェック通った場合:true 、通らない場合：エラーメッセージ
         */
        public static function myProfileInputCheck(&$p,$member_id) {
            $message=array();
            $model = new MemberModel();
        if(empty($p["f_handle"]))
            array_push($message, Msg::get("REGISTER_HANDLE_EMPTY"));
        else {
            if(!self::addMaxLenCheck($message, $p["f_handle"], Msg::get("HANDLE_LEN"), Msg::get("HANDLE"))) {

            }
            if(!empty($p["f_handle"]) && $model->isExistedHandle($p["f_handle"],$member_id))
                array_push($message, Msg::get("REGISTER_HANDLE_EXISTED"));
            if(self::isExistKeyWord($p["f_handle"]))
                array_push($message, sprintf(Msg::get("INPUT_EXIST_KEYWORD"),Msg::get("HANDLE")));
            elseif(self::isFaultWord($p["f_handle"]))
                    array_push($message, sprintf(Msg::get("INPUT_EXIST_KEYWORD"),Msg::get("HANDLE")));
        }

//            if(empty($p["f_handle"]))
//                array_push($message, Msg::get("REGISTER_HANDLE_EMPTY"));
//
//            if(!self::addMaxLenCheck($message, $p["f_handle"], Msg::get("HANDLE_LEN"), Msg::get("HANDLE"))) {
//
//              }
//             if(!empty($p["f_handle"]) && $model->isExistedHandle($p["f_handle"],$member_id)){
//                array_push($message, Msg::get("REGISTER_HANDLE_EXISTED"));
//             }
            //誕生日のチェック
            if($p["birth_year"]=="00")
                array_push($message, Msg::get("REGISTER_BIRTH_YEAR_EMPTY"));
            if($p["birth_month"]=="00")
                array_push($message, Msg::get("REGISTER_BIRTH_MONTH_EMPTY"));
            if($p["birth_day"]=="00")
                array_push($message, Msg::get("REGISTER_BIRTH_DAY_EMPTY"));
            if($p["birth_year"]!="00" && $p["birth_month"]!="00" && $p["birth_day"]!="00") {
                if(checkdate($p["birth_month"], $p["birth_day"], $p["birth_year"])==false)
                    array_push($message, Msg::get("ERROR_BIRTH_DAY"));
            }

            if(!empty($p["name"]))
                self::addMaxLenCheck($message, $p["name"], Msg::get("NAME_LEN"), Msg::get("NAME"));

            if(!empty($p["tel"])) {
                if( !ereg("[0-9]+", $p["tel"]))
                    array_push($message, array_push($message, Msg::get("REGISTER_TEL_INVALID")));
                else
                    self::addMaxLenCheck($message, $p["tel"], Msg::get("TEL_LEN"), Msg::get("TEL"));
            }

            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;
        }


        /**
         *友達紹介の入力チェック
         * @param <array> $p 画面に入力した内容
         * @return <boolean> チェック通った場合:true 、通らない場合：エラーメッセージ
         */
        public static function myFriendInputCheck($p) {
            $message=array();
            if(empty($p["friend_name"]))
                array_push($message, Msg::get("MY_FRIEND_NAME_EMPTY"));
            else
                self::addMaxLenCheck($message, $p["friend_name"], Msg::get("FRIEND_NAME_LEN"), Msg::get("FRIEND_NAME"));

            if(empty($p["friend_email"]))
                array_push($message, Msg::get("MY_FRIEND_EMAIL_EMPTY"));
            else {
                if(!self::addMaxLenCheck($message, $p["friend_email"], Msg::get("EMAIL_LEN"), Msg::get("FRIEND_EMAIL"))) {
                    if(!Utils::isMailAddress($p["friend_email"]))
                        array_push($message, Msg::get("REGISTER_EMAIL_INVALID"));
                }
            }
            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;
        }

        /**
         *マイページのメールアド変更の入力チェック
         * @param <array> $p 画面に入力した内容
         * @return <boolean> チェック通った場合:true 、通らない場合：エラーメッセージ
         */
        public static function myEmailInputCheck($email) {
            $message=array();
            if(empty($email))
                array_push($message, Msg::get("REGISTER_EMAIL_EMPTY"));
            else {
                if(!self::addMaxLenCheck($message, $email, Msg::get("EMAIL_LEN"), Msg::get("EMAIL"))) {
                    if(!Utils::isMailAddress($email))
                        array_push($message, Msg::get("REGISTER_EMAIL_INVALID"));
                }
            }
            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;
        }

        /**
         *マイページのパスワード変更の入力チェック
         * @param <array> $p 画面に入力した内容
         * @return <boolean> チェック通った場合:true 、通らない場合：エラーメッセージ
         */
        public static function myPasswordInputCheck($p) {
            $message=array();
            if(empty($p["old_pass"]))
                array_push($message, Msg::get("MY_OLD_PASSWORD_EMPTY"));
            else
                self::addBetweenLenCheck($message, $p["old_pass"], PASSWORD_MIN_LEN, Msg::get("PASSWORD_LEN"), Msg::get("OLD_PASSWORD"));
            if(empty($p["new_pass"]))
                array_push($message, Msg::get("MY_NEW_PASSWORD_EMPTY"));
            else
                self::addBetweenLenCheck($message, $p["new_pass"], PASSWORD_MIN_LEN, Msg::get("PASSWORD_LEN"), Msg::get("NEW_PASSWORD"));
            if(empty($p["repeat_pass"]))
                array_push($message,Msg::get("MY_REPEAT_PASSWORD_EMPTY"));
            else
                self::addBetweenLenCheck($message, $p["repeat_pass"], PASSWORD_MIN_LEN, Msg::get("PASSWORD_LEN"), Msg::get("REPEAT_PASSWORD"));
            if(!empty($p["new_pass"]) && !empty($p["repeat_pass"])) {
                if($p["new_pass"] != $p["repeat_pass"])
                    array_push($message,Msg::get("MY_PASSWORD_NOT_REPEAT"));
            }
            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;

        }

        /**
         *問い合わせの入力チェック処理
         * @param <type> $p
         * @return <type>
         */
        public static function contactInputCheck($p) {
            $message=array();
            if(empty($p["name"]))
                array_push($message, Msg::get("REGISTER_NAME_EMPTY"));
            else
                self::addMaxLenCheck($message, $p["name"], Msg::get("QUESTION_NAME_LEN"), Msg::get("NAME"));

            if(empty($p["mail"]))
                array_push($message, Msg::get("REGISTER_EMAIL_EMPTY"));
            else {
                if(!self::addMaxLenCheck($message, $p["mail"], Msg::get("EMAIL_LEN"), Msg::get("EMAIL"))) {
                    if(!Utils::isMailAddress($p["mail"]))
                        array_push($message, Msg::get("REGISTER_EMAIL_INVALID"));
                }
            }

            if(empty($p["title"]))
                array_push($message,Msg::get("TITLE_EMPTY"));

            if(empty($p["main"]))
                array_push($message,Msg::get("MAIN_EMPTY"));

            if(count($message)==0)
                return TRUE;
            $m="";
            foreach ($message as $value) {
                $m.=$value."<br>";
            }
            return $m;
        }

    //    private static function isExistKeyWord(&$str) {
    //        $keyword=array("'",";",":","\\","\"");
    //
    //        foreach ($keyword as $value) {
    //            if(stripos($str, $value)!==FALSE){
    //                $str=str_replace($value, '', $str);//&nbsp;
    //                return true;
    //            }
    //        }
    //
    //        return false;
    //    }
    //        private static function isExistKeyWord(&$str) {
    //            $strtmp = html_entity_decode($str);
    ////            $keyword=array("'",";",":","\\","\"");
    //            $keyword=array("'",";",":","\\","\"");
    //            $result=false;
    //            foreach ($keyword as $value) {
    //                if(stripos($strtmp, $value)!==FALSE) {
    //                    $strtmp=str_replace($value, '', $strtmp);//&nbsp;
    //                    //               return true;
    //                    $result=true;
    //                }
    //            }
    //
    ////        return false;
    //            return $result;
    //        }
        public static function isExistKeyWord(&$str) {
            $strtemp = html_entity_decode($str);
            $keyword=array("'",";",":","\\","\"");
            $result=false;
            foreach ($keyword as $value) {
                if(stripos($strtemp, $value)!==FALSE) {

                    $strtemp=str_replace($value, '', $strtemp);//&nbsp;
                    //                return true;
                    $result=true;
                }
            }
            if($result==true) {
                $str = $strtemp;
            }
            return $result;
        }

        public static function isFaultWord(&$str) {
            import('classes.model.MemberModel');
            $strtemp = html_entity_decode($str);
            $model=new MemberModel();
            $result=$model->isFaultWords($strtemp);
            if($result==true) {
                $str = $strtemp;
            }
            return $result;
        }
    }
    ?>
