<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * 共通のメッセージ処理クラス
 *
 * @author meng
 */
class Message {
//put your code here
    private static $message=array();

    /**
     *メッセージをメッセージリストに追加する
     * @param <type> $message String型、また配列
     */
    public static function addMessage($message){
        if(!empty($message)){
            if(is_array($message)){
                //$messageが配列の場合
                foreach ($message as $value) {
                    array_push(self::$message, $value);
                }
            }
            else
                array_push(self::$message, $message);
        }
    }


    /**
     *マイページ処理する際、確認、処理失敗などのメッセージが表示する
     * （inputチェック結果メッセージの場合、使わないで）
     * @param <object> $actionObj Actionのインスタンス
     * @param <string> $message メッセージの内容
     * @return <ActionResult>
     */
    public static function showMyPageMessage($actionObj,$message) {
        if($actionObj instanceof BaseAction) {
            $actionObj->addTplParam("my_message", $message);
            $include_file=AgentInfo::getAgentDir()."/parts/my_message.html";
            $actionObj->addTplParam("include_file", $include_file);
            return $actionObj->createTemplateResult("my.html");
        }
        throw new AppException(ConstKeys::NOT_ACTION_INSTANCE);
    }

    /**
     *マイページ処理する際、「無効なリクエスト」の場合、メッセージが表示
     * @param <object> $actionObj Actionのインスタンス
     * @return <ActionResult>
     */
    public static function showMyPageErrorRequest($actionObj) {
        if($actionObj instanceof BaseAction) {
            $msg = Msg::get("INVALID_REQUEST");
            return self::showMyPageMessage($actionObj, $msg);
        }
        throw new AppException(ConstKeys::NOT_ACTION_INSTANCE);
    }

    /**
     *処理する際、「無効なリクエスト」の場合、メッセージが表示
     * @param <object> $actionObj Actionのインスタンス
     * @return <ActionResult>
     */
    public static function showErrorRequest($actionObj) {
        if($actionObj instanceof BaseAction) {
            $msg = Msg::get("INVALID_REQUEST");
            return self::showConfirmMessage($actionObj, $msg);
        }
        throw new AppException(ConstKeys::NOT_ACTION_INSTANCE);
    }

    /**
     *確認、などのメッセージが表示する
     * @param <object> $actionObj Actionのインスタンス
     * @param <string> $message
     * @return <ActionResult>
     */
    public static function showConfirmMessage($actionObj,$message,$returnUrl="",$returnStr=""){
        if($actionObj instanceof BaseAction) {
            $returnUrl= $returnUrl=="" ? Config::value("SERVER_PATH") : $returnUrl;
            $returnStr= $returnStr=="" ? "ホームへ" : $returnStr;
            $actionObj->addTplParam("message", "<li>$message</li>");
            $actionObj->addTplParam("url", $returnUrl);
            $actionObj->addTplParam("str", $returnStr);
            return $actionObj->createTemplateResult("message.html");
        }
        throw new AppException(ConstKeys::NOT_ACTION_INSTANCE);
    }

     /**
     *確認、などのメッセージが表示する(メッセージリストに溜ったものが表示する）
     * @param <object> $actionObj Actionのインスタンス
     * @return <ActionResult>
     */
    public static function showConfirmMessageList($actionObj){
        if($actionObj instanceof BaseAction) {
            $msg="";
            foreach (self::$message as $value) 
                $msg.="<li>$value</li>";
            return self::showConfirmMessage($actionObj, $msg);
        }
        throw new AppException(ConstKeys::NOT_ACTION_INSTANCE);
    }
}
?>
