<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of OWNERBank
 *
 * @author mw
 */
class OWNERBank {
    //put your code here

    /**
     *運営者口座情報を表示(コインの場合）
     * @param <type> $coin_id
     * @return <type>
     */
    public function coin($coin_id) {
        return "/index.php/coin/bank?coin_id=$coin_id";
    }

    /**
     *運営者口座情報を表示(落札商品の場合）
     * @param <type> $pro_id
     * @param <type> $sf_money
     * @return <type>
     */
    public function product($pro_id,$sf_money){
        return "/index.php/mypay/bank?pro_id=$pro_id&sf_money=$sf_money";
    }
}
?>
