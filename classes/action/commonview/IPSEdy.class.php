<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import("classes.model.MemberModel");
import("classes.model.ShiharaiModel");

/**
 * Description of IPSEdyclass
 *
 * @author kaz
 */
class IPSEdy // extends BaseAction
{
    public function execute($buy_money, $sf_money, $coin_count, $type=NULL, $pro_id=NULL)
    {
        $model = new ShiharaiModel();
        $member_id  = $_SESSION['member_id'];
        $status     = $type;    //支払いﾀｲﾌﾟ：ｺｲﾝ購入/商品購入
        $staff_id   = 0;        //STAFF-ID  :無し
        $min_price  = 0;        //仕入れ価格 :0円
        $model ->PutReservePayLog($status, $member_id, $staff_id, Utils::getInt($buy_money)+Utils::getInt($sf_money), $coin_count, SHIHARAI_TYPE_EDY, 0, $min_price,$pro_id);
        $last_id = DB::getObjectByQuery("SELECT LAST_INSERT_ID() as last_id");
        $last_insert_id = $last_id[0]->last_id;

        //IPS-Edyに必要な情報を定義(POST先から各種パラメータまで）
        $post_url_pc = "https://credit.ipservice.jp/gateway/edypayform.aspx";
        $post_url_mb = "https://credit.ipservice.jp/igateway/edypayform.aspx";
        $aid         = IPS_EDY_AID;           //店舗番号
        $cod         = "";                    //ｵｰﾀﾞｰ番号
        $jb_mb       = "MOBILE";              //ｼﾞｮﾌﾞﾀｲﾌﾟ(MB)
        $jb_pc       = "CYBER";               //ｼﾞｮﾌﾞﾀｲﾌﾟ(PC)
        $am          = $buy_money;            //決済金額
        $tx          = "0";                   //税金
        $sf          = Utils::getInt($sf_money,0);             //送料
        $em          = "";                    //ﾒｰﾙｱﾄﾞﾚｽ
        $member_id   = "";                    //独自ﾊﾟﾗﾒｰﾀ：会員ID

        if ($type == SHIHARAI_COIN_TYPE)
        {
            $target  = "COINS";                     //独自ﾊﾟﾗﾒｰﾀ：購入対象(COIN)
        }
        else if ($type == SHIHARAI_PRODUCT_TYPE)
        {
            $target  = sprintf("PRD_%s", $pro_id);  //独自ﾊﾟﾗﾒｰﾀ：購入対象(商品)
        }

        //会員情報取得（会員IDはｾｯｼｮﾝより取得)
        $member_model = new MemberModel();
        $member_id = $_SESSION['member_id'];
        $member_data  = $member_model->getMemberMasterById($member_id);
        $em = $member_data->f_mail_address_pc;

        //機種情報取得PC・MBでPOST情報が違う場合あり
        $term = AgentInfo::getAccessTerminal();
        $edytype = $_GET['edytype'];

        if (($term == "PC") && ($edytype == 1))
        {
            //PCでパソリの場合の処理
            $jump_url = sprintf("%s/?aid=%s&jb=%s&am=%s&tx=%s&sf=%s&cod=%s&em=%s&target=%s&member_id=%s&reserve_id=%s",
                                $post_url_pc,
                                $aid,
                                $jb_pc,
                                $am,
                                $tx,
                                $sf,
                                $cod,
                                $em,
                                $target,
                                $member_id,
                                $last_insert_id);
        }
        else if (($term == "PC") && ($edytype == 2))
        {
            //PCでﾓﾊﾞｲﾙEdyの場合の処理
            $jump_url = sprintf("%s/?aid=%s&jb=%s&am=%s&tx=%s&sf=%s&cod=%s&em=%s&target=%s&member_id=%s&reserve_id=%s",
                                $post_url_mb,
                                $aid,
                                $jb_mb,
                                $am,
                                $tx,
                                $sf,
                                $cod,
                                $em,
                                $target,
                                $member_id,
                                $last_insert_id);
        }
        else
        {
            //MBの場合の処理
            $jump_url = sprintf("%s/?aid=%s&jb=%s&am=%s&tx=%s&sf=%s&cod=%s&em=%s&target=%s&member_id=%s&reserve_id=%s",
                                $post_url_mb,
                                $aid,
                                $jb_mb,
                                $am,
                                $tx,
                                $sf,
                                $cod,
                                $em,
                                $target,
                                $member_id,
                                $last_insert_id);
        }

        //print "$jump_url<BR>\n";
//        header("Location: $jump_url");
//        exit;
        return $jump_url;
    }
}
?>
