<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import("classes.model.MemberModel");
/**
 * Description of IPSConvclass
 *
 * @author kaz
 */

//注意事項！！！
//
//2010-04-08時点のﾃｽﾄ用IPSアカウントではｺﾝﾋﾞﾆ決済を申し込んでいないので
//未ﾃｽﾄ状態です！！！
//使用の際は再度ロジックテスト・実テストを要するので注意してください。
//Are You OK?
//
//

class IPSConv //extends BaseAction
{
    //put your code here
    public function execute($buy_money)
    {
        //IPSｺﾝﾋﾞﾆに必要な情報を定義(POST先から各種パラメータまで）
        $post_url_pc = "https://credit.ipservice.jp/gateway/cvpayform.aspx";
        $post_url_mb = "https://credit.ipservice.jp/gateway/icsvform.aspx";
        $aid         = IPS_CONV_AID;          //店舗番号
        $jb          = "CAPTURE";             //ｼﾞｮﾌﾞﾀｲﾌﾟ
        $st          = "1";                   //店舗情報1：Loppi 2：Seven 3：FAMIMA 11：C-Check 71：ACD 73：ｵﾝﾗｲﾝ
        $am          = $buy_money;            //決済金額
        $tx          = "0";                   //税金
        $sf          = $sf_money;             //送料
        $em          = "";                    //ﾒｰﾙｱﾄﾞﾚｽ
        $cmd         = "0";                   //編集モード
        $target      = "COINS";               //独自ﾊﾟﾗﾒｰﾀ：購入対象
        $member_id = "";                      //独自ﾊﾟﾗﾒｰﾀ：会員ID

        //会員情報取得（会員IDはｾｯｼｮﾝより取得)
        $member_model = new MemberModel();
        $member_id = $_SESSION['member_id'];
        $member_data  = $member_model->getMemberMasterById($member_id);

        //機種情報取得PC・MBでPOST情報が違う場合あり
        $term = AgentInfo::getAccessTerminal();
        if ($term == "PC")
        {
            //PCの場合の処理
            $em = $member_data->f_mail_address_pc;
            $jump_url = sprintf("%s/?aid=%s&jb=%s&st=%s&am=%s&tx=%s&sf=%s&em=%s&cmd=%s&taregt=%s&member_id=%s",
                                $post_url_pc,
                                $aid,
                                $jb,
                                $st,
                                $am,
                                $tx,
                                $sf,
                                $em,
                                $cmd,
                                $target,
                                $member_id);
        }
        else
        {
            //MBの場合の処理
            $em = $member_data->f_mail_address_mb;
            $jump_url = sprintf("%s/?aid=%s&jb=%s&st=%s&am=%s&tx=%s&sf=%s&em=%s&cmd=%s&target=%s&member_id=%s",
                                $post_url_mb,
                                $aid,
                                $jb,
                                $st,
                                $am,
                                $tx,
                                $sf,
                                $em,
                                $cmd,
                                $target,
                                $member_id);
        }
        //print "JUMP!=> $jump_url<BR>\n";
//        header("Location: $jump_url");
//        exit;
        Logger::debug("conv_url:$jump_url");
        return $jump_url;
    }
}
?>
