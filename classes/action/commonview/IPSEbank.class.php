<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import("classes.model.MemberModel");
import("classes.model.ShiharaiModel");
/**
 * Description of IPSEbankclass
 *
 * @author kaz
 */
class IPSEbank //extends BaseAction
{
    public function execute($buy_money,$sf_money, $coin_count, $type=NULL, $pro_id=NULL)
    {
        //2010-04-08 KazTanaka 決済要求時に支払い記録ﾃｰﾌﾞﾙに「予約」で書き込む
        $model = new ShiharaiModel();
        $member_id  = $_SESSION['member_id'];

        $status     = $type;    //支払いﾀｲﾌﾟ：ｺｲﾝ購入/商品購入
        $staff_id   = 0;    //STAFF-ID  :無し
        $min_price  = 0;    //仕入れ価格 :0円
        $model ->PutReservePayLog($status, $member_id, $staff_id, Utils::getInt($buy_money)+Utils::getInt($sf_money), $coin_count, SHIHARAI_TYPE_INETBANKING, 0, $min_price,$pro_id);
        $last_id = DB::getObjectByQuery("SELECT LAST_INSERT_ID() as last_id");
        $last_insert_id = $last_id[0]->last_id;

        //IPSオート銀振に必要な情報を定義(POST先から各種パラメータまで）
        $post_url_pc = "https://credit.ipservice.jp/gateway/ebform.aspx";
        $post_url_mb = "https://credit.ipservice.jp/gateway/iebform.aspx";
        $aid         = IPS_EBANK_AID;         //店舗番号
        $cod         = "";                    //ｵｰﾀﾞｰ番号
        $am          = $buy_money;            //決済金額
        $tx          = "0";                   //税金
        $sf          = Utils::getInt($sf_money,0);             //送料
        $em          = "";                    //ﾒｰﾙｱﾄﾞﾚｽ
        if ($type == SHIHARAI_COIN_TYPE)
        {
            $target  = "COINS";           //独自ﾊﾟﾗﾒｰﾀ：購入対象(COIN)
        }
        else if ($type == SHIHARAI_PRODUCT_TYPE)
        {
            $target  = sprintf("PRD_%s", $pro_id);  //独自ﾊﾟﾗﾒｰﾀ：購入対象(商品)
        }
        $member_id   = "";                    //独自ﾊﾟﾗﾒｰﾀ：会員ID

        //会員情報取得（会員IDはｾｯｼｮﾝより取得)
        $member_model = new MemberModel();
        $member_id = $_SESSION['member_id'];
        $member_data  = $member_model->getMemberMasterById($member_id);
        $em = $member_data->f_mail_address_pc;

        //機種情報取得PC・MBでPOST情報が違う場合あり
        $term = AgentInfo::getAccessTerminal();
        if ($term == "PC")
        {
            //PCの場合の処理
            $jump_url = sprintf("%s/?aid=%s&cod=%s&am=%s&tx=%s&sf=%s&em=%s&target=%s&member_id=%s&reserve_id=%s",
                                $post_url_pc,
                                $aid,
                                $cod,
                                $am,
                                $tx,
                                $sf,
                                $em,
                                $target,
                                $member_id,
                                $last_insert_id);
        }
        else
        {
            //MBの場合の処理
            $jump_url = sprintf("%s/?aid=%s&cod=%s&am=%s&tx=%s&sf=%s&em=%s&target=%s&member_id=%s&reserve_id=%s",
                                $post_url_mb,
                                $aid,
                                $cod,
                                $am,
                                $tx,
                                $sf,
                                $em,
                                $target,
                                $member_id,
                                $last_insert_id);
        }
        Logger::DEBUG($jump_url);
        //print "$jump_url<BR>\n";
//        header("Location: $jump_url");
//        exit;
        return $jump_url;
    }
}
?>
