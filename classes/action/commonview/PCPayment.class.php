<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import("classes.model.ShiharaiModel");
import("classes.action.commonview.IPSCredit");
import("classes.action.commonview.IPSEbank");
import("classes.action.commonview.IPSEdy");
import("classes.action.commonview.IPSConv");
import("classes.action.commonview.WebMoney");
import("classes.action.commonview.TelecomCredit");
/**
 * Description of PCPaymentclass
 *
 * @author kaz
 */
class PCPayment extends PcAction {
    //put your code here

    private $beforeResult="";
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        //ユーザログイン状況確認
        if(!$this->isLogin()) {
            //ログインしていない場合
            $this->addTplParam("error", Msg::get("NOT_LOGIN"));
            $this->beforeResult=$this->createTemplateResult("login.html");
        }
        else
        //ログイン情報をテンプレートに追加
            $this->checkSessionInfo();
    }

    public function execute() {
        if(!$this->isLogin()) {
            //ログインしていない場合
            return $this->beforeResult;
        }
//        Logger::debug("sessionId:".session_id());
        $payment_type = $_GET['payment'];
        if ($payment_type == "") {
            $payment_type = $_POST['payment'];
        }

        //ﾍﾟｰｼﾞ間ﾃﾞｰﾀより金額を取得
        $buy_money = $_GET['buy_money'];
        if ($buy_money == "") {
            $buy_money = $_POST['buy_money'];
        }

        //ｺｲﾝ枚数取得
        $coin_count=$this->getRequestParam("coin_count");

        //送料
        $sf_money=$this->getRequestParam("sf_money");
        $edytype=$this->getRequestParam("edytype");

//        $coin_count = $_GET['coin_count'];
//        if ($coin_count == "")
//        {
//            $coin_count = $_POST['coin_count'];
//        }

//        //銀行振り込みの場合
//        if($payment_type==1){
//
//        }

        //ｺﾝﾋﾞﾆ決済選択の場合に店舗情報の確認
        //GET・POSTから読み出す
        if (array_key_exists("st", $_GET)) {
            $st = $_GET['st'];
            if ($st == "") {
                $st = $_POST['st'];
            }
        }
        else {
            $st = "";
        }

        //支払い管理インスタンス生成
        $model = new ShiharaiModel();
        $data = $model->GetShiharaiType($payment_type);
        $data_count = count($data);
        if ($data_count >= 1) {
            $use_module = $data[0]->f_module;

            $type=$this->getRequestParam("type");
            $pro_id=$this->getRequestParam("pro_id");

            //2010-04-08 Kaz Tanaka 特記仕様
            //IPSｺﾝﾋﾞﾆ決済の場合は店舗選択ﾍﾟｰｼﾞにｼﾞｬﾝﾌﾟする（店舗情報無しなら）
            //2010/04/23 meng del start
//            if (($payment_type == SHIHARAI_TYPE_CONV) && ($st == "")) {
            //2010/04/23 meng del end
            //2010/04/23 meng add start
            //$payment_typeの値がfk_shiharai_idから→fk_shiharai_type_idにしました（buymethod.htmlに参照）
            if (($data[0]->f_module == "IPSConv") && ($st == "")) {
                //2010/04/23 meng add end


                //$this->checkSessionInfo();
                //$this->addTplParam("buy_money", $buy_money);
                //$this->addTplParam("payment",   $payment_type);
                //return $this->createTemplateResult("IPSConvSelectStore.html");
                $jump_url="/index.php/coin/conv/?buy_money=$buy_money&coin_count=$coin_count&payment=$payment_type&sf_money=$sf_money";
//                header("Location: /index.php/coin/conv/?buy_money=$buy_money&coin_count=$coin_count&payment=$payment_type&sf_money=$sf_money");
//                exit;
                return $this->createRedirectResult($jump_url);
            }
            else if (($data[0]->f_module == "IPSEdy") && ($edytype == "")) {
                $jump_url = "/index.php/coin/edy/?buy_money=$buy_money&coin_count=$coin_count&payment=$payment_type&sf_money=$sf_money&type=$type&pro_id=$pro_id";
                //print "$jump_url<BR>\n";
//                header("Location: $jump_url");
//                exit;
                return $this->createRedirectResult($jump_url);
            }
//G.Chin 2010-12-02 add sta
            else if ($data[0]->f_module == "ZeroCredit") {
                $jump_url = "/index.php/coin/zero/?buy_money=$buy_money&coin_count=$coin_count&payment=$payment_type&sf_money=$sf_money&type=$type&pro_id=$pro_id";
                //print "$jump_url<BR>\n";
//                header("Location: $jump_url");
//                exit;
                return $this->createRedirectResult($jump_url);
            }
//G.Chin 2010-12-02 add end
            else if (!empty ($data[0]->f_module) && $data[0]->f_module == "OWNERBank") {
                //運営者銀行口座情報が表示される
                $type=$this->getRequestParam("type");
                import("classes.action.commonview.OWNERBank");
                $mod=new OWNERBank();
                switch ($type) {
                    case SHIHARAI_COIN_TYPE :
                    //コイン購入
                        $coin_id=$this->getRequestParam("coin_menu_id");
                        $url=$mod->coin($coin_id);
                        break;
                    case SHIHARAI_PRODUCT_TYPE :
                    //落札商品購入
                        $pro_id=$this->getRequestParam("pro_id");
                        $sf_money=$this->getRequestParam("sf_money");
                        $url=$mod->product($pro_id, $sf_money);
                        break;
                }
                return $this->createRedirectResult($url);
            }

            //print "EDY-TYPE : $edytype";
            //exit;
            $model = new $use_module();
            $resultUrl = $model->execute($buy_money,$sf_money, $coin_count, $type, $pro_id);
            return $this->createRedirectResult($resultUrl);
        }
        return $this->createTextResult($payment_type);
    }
}
?>
