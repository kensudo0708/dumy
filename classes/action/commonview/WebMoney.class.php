<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import("classes.model.MemberModel");
import("classes.model.ShiharaiModel");

/**
 * Description of IPSCreditclass
 *
 * @author kaz
 */
class WebMoney //extends BaseAction
{
    public function execute($buy_money,$sf_money, $coin_count, $type=NULL, $pro_id=NULL)
    {
//2010-04-08 KazTanaka 決済要求時に支払い記録ﾃｰﾌﾞﾙに「予約」で書き込む
        $model = new ShiharaiModel();
        $member_id  = $_SESSION['member_id'];
        $status     = empty($type) ? SHIHARAI_COIN_TYPE : $type;    //支払いﾀｲﾌﾟ：ｺｲﾝ購入/商品購入
        $staff_id   = 0;        //STAFF-ID  :無し
        $min_price  = 0;        //仕入れ価格 :0円
        $total = Utils::getInt($buy_money)+Utils::getInt($sf_money);
        $model ->PutReservePayLog($status, $member_id, $staff_id, $total, $coin_count, SHIHARAI_TYPE_CREDIT, 0, $min_price,$pro_id);
        $last_id = DB::getObjectByQuery("SELECT LAST_INSERT_ID() as last_id");
        $last_insert_id = $last_id[0]->last_id;

        $proName="";
        if($coin_count >0){
            $proName=POINT_NAME."購入";
            $pro_id = "000";
        }
        else
            $proName="落札商品";


        $scd         = "KYKTH0002704";        //契約コード
        $gcd          = "GDSTH".rand();                    //商品コード
        $pnm          = mb_convert_encoding($proName,"EUC-JP","auto");                    //商品名
        $prc          = $total;                   //単価
        $num          = 1;             //購入個数
        $rtu          = SITE_URL;                    //戻りURL
        $atu         =SITE_URL."cli_ep.php/webmoney?wmret=".mb_convert_encoding("####","EUC-JP","auto");                   //認証URL
        $ctu         = SITE_URL."cgi-bin/pagecon.cgi";                   //通知用URL
        $payinfo    =$status."-".$total."-".$member_id."-".$last_insert_id."-".$pro_id;

        $shopfirst ="/var/www/cgi-bin/shopfirst.cgi";
//        $cmd =$shopfirst." ".$scd." ".$gcd." ".$pnm." ".$prc." ".$num." ".$rtu." ".$atu." ".$ctu." ".$payinfo;
        $cmd =$shopfirst." ".$scd." ".$gcd." ".$pnm." ".$prc." ".$num." ".$rtu." ".$atu." ".$ctu." ".$payinfo;
        $dir=getcwd();
//        chdir("WEBMONEY/module/32bit");
        chdir("/var/www/cgi-bin");
        exec($cmd,$rtn);
        chdir($dir);
//        var_dump($rtn);
//        var_dump(str_replace("Location: ", "", $rtn[0]));
        Logger::debug("cmd:$cmd,\nsent webmoney url：$rtn[0]");
        return str_replace("Location: ", "", $rtn[0]);
    }
}
?>
