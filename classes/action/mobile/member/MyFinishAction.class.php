<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import("classes.model.MyProductModel");
import("classes.utils.Message");
/**
 * Description of MyPayAction
 *
 * @author ark
 */
class MyFinishAction extends MobileAction {
    //put your code here
    private $beforeResult="";

    private $product="";

    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->beforeResult = $this->checkPhoneSerNo();
        //ユーザログイン状況確認
        if(!$this->isLogin()) {
            //ログインしていない場合
            $this->addTplParam("error", Msg::get("NOT_LOGIN"));
            $this->beforeResult=$this->createTemplateResult("login.html");
            $this->setSession("login_change",$_SERVER["REQUEST_URI"]);
        }
        else {
            //ログイン情報をテンプレートに追加
            $this->checkSessionInfo();
            //落札商品の情報を取得

            $pro_id=$this->getRequestParam("pro_id");
            if(empty ($pro_id))
            //無効なアクセス
//                $this->beforeResult= Message::showMyPageErrorRequest($this);
                $this->beforeResult= $this->createRedirectResult(Config::value("SERVER_PATH").LOGIN_JUMP_URL);

            $model= new MyProductModel();
            $this->product = $model->getEndProductById($pro_id,$this->getMemberIdFromSession());

            if(empty($this->product))
                $this->beforeResult= $this->createRedirectResult(Config::value("SERVER_PATH").LOGIN_JUMP_URL);
            else{
                $this->product->f_photo1=Config::value("PRODUCT_IMAGE_PATH")."/".$this->product->f_photo1;
                $this->addTplParam("product", $this->product);
            }
        }

    }

    /**
     *落札商品の購入
     * @return <ActionResult>
     */
    public function pay() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        //既に支払い済みの場合、メッセージを表示する
        import("classes.model.ProductGeneral");
        $result = ProductGeneral::hasPaid($this, $this->getMemberIdFromSession(), $this->product->fk_end_products_id);
        if(!empty ($result))
            return $result;

        import("classes.model.CoinModel");
        $coinModel=new CoinModel();
        //支払い方法を取得
        $methods=$coinModel->getPayMethod(SHIHARAI_PRODUCT_TYPE,$this->product->f_end_price,TERM_TYPE_PC);

        $this->addTplParam("methods", $methods);
        //購入タイプ落札商品
        $this->addTplParam("type", SHIHARAI_PRODUCT_TYPE);
        if(!empty ($this->product->f_address_flag) && $this->product->f_address_flag== 1) {
            //配送料フラグ＝1の場合も、送料０にする
            $this->addTplParam("carriage", 0);
        }
        else {
            //送料
            $this->addTplParam("carriage", CARRIAGE);
        }
        $include_file=AgentInfo::getAgentDir()."/parts/my_finish_pay.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }


    /**
     *
     * @return <type>
     */
    public function address() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $member_id=$this->getMemberIdFromSession();

        //最終配送アドレスを取得する
//        import("classes.model.MemberModel");
//        $model=new MemberModel();
//        $last = $model->getMemberLastAddress($member_id);
//        if(!empty ($last))
//            $last->address=$last->f_name.' 〒'.$last->f_post_code.' '.$last->t_pref_name;
//
//        $this->addTplParam("last", $last);

        $model=new MemberModel();
        $address=$model->getAddressByMemberId($this->getMemberIdFromSession());
        $this->addTplParam("address", $address);

        $include_file=AgentInfo::getAgentDir()."/parts/my_finish_adr.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");

    }

    /**
     *配送先アドレス選択処理
     * @return <type>
     */
    public function adr_choose() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $address_id=$this->getRequestParam("address");
        if($address_id==0) {
            //新規追加処理
            $model=new MemberModel();
            $this->addTplParam("perfs", $model->getPerfs());
            $include_file=AgentInfo::getAgentDir()."/parts/my_finish_adrnew.html";
            $this->addTplParam("include_file", $include_file);
            return $this->createTemplateResult("my.html");
        }
        else {
            //仕様変更より、コメントする　start
            //
            //アドレス更新
//            $productModel=new MyProductModel();
//            //最終配送アドレスを取得する
//            import("classes.model.MemberModel");
//            $model=new MemberModel();
//            $member_id=$this->getMemberIdFromSession();
//            $last = $model->getMemberLastAddress($member_id);
//            if(empty($last)) {
//                   //無効なアクセス
//                return Message::showMyPageErrorRequest($this);
//            }
            $productModel=new MyProductModel();
            $result=$productModel->updateAddress($address_id,$this->product->fk_end_products_id);
            if($result===FALSE)
            //処理失敗した場合、メッセージが表示する
                return Message::showMyPageMessage($this, Msg::get("PROCESS_FAILED"));
            //処理成功、落札商品リストにもどる
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/myproduct/finish");
        }
    }

    /**
     *新しい配送先を保存する
     * @return <type>
     */
    public function new_address() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        $p=array();

        $p["f_post_code"]=$this->getRequestParam("f_post_code");
        $p["fk_perf_id"]=$this->getRequestParam("fk_perf_id");
        $p["f_address1"]=$this->getRequestParam("f_address1");
        $p["f_address2"]=$this->getRequestParam("f_address2");
        $p["f_address3"]=$this->getRequestParam("f_address3");
        $p["f_tel_no"]=$this->getRequestParam("f_tel_no");
        $p["f_name"]=$this->getRequestParam("f_name");

        //入力チェック
        import("classes.utils.InputCheck");
        $message=InputCheck::myAddressinputCheck($p);
        if($message===TRUE) {
            //登録処理
            $p["member_id"]=$this->getMemberIdFromSession();
            $model=new MyProductModel();
            $p["member_id"]=$this->getMemberIdFromSession();
            $p["fk_end_products_id"]=$this->product->fk_end_products_id;
            $result = $model->updateEndProductAddress($p);
            if($result) {
                //落札リストに戻る
                return $this->createRedirectResult(Config::value("SERVER_PATH")."/myproduct/finish");
            }
            else
                $this->addTplParam("my_message", Msg::get("PROCESS_FAILED"));
        }
        else {
            //再入力
            $this->addTplParam("message", $message);
            $this->addTplParam("address", $p);
            $model=new MemberModel();
            $this->addTplParam("perfs", $model->getPerfs());
            $include_file=AgentInfo::getAgentDir()."/parts/my_finish_adrnew.html";
        }
        //ページ名
        $this->addTplParam("page_name", "finish_adrnew");
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     * 購入キャンセル処理
     */
    public function cancel() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $mode=$this->getRequestParam("mode");
        if(empty ($mode)) {
            //放棄画面を表示する
            $include_file=AgentInfo::getAgentDir()."/parts/my_finish_cancel.html";
            $this->addTplParam("include_file", $include_file);
            return $this->createTemplateResult("my.html");
        }
        else {
            if($mode!="exe")
            //無効なアクセス
                return Message::showMyPageErrorRequest($this);

            //放棄処理
            $model=new MyProductModel();
            //shoji 20100910
                        //キャンセル数チェック
            $ret = $model->GetProdCancel($this->product->fk_end_products_id);
            if($ret->f_cancel_num >= PROD_CANCEL_NUM && PROD_CANCEL_NUM != 0)
            {
                return Message::showMyPageMessage($this, Msg::get("PROD_CANCEL_FAULT"));
            }
            $result=$model->endProductCancel($this->product->fk_end_products_id);
            if($result===FALSE)
            //処理失敗した場合、メッセージが表示する
                return Message::showMyPageMessage($this, Msg::get("PROCESS_FAILED"));

            //処理成功、落札商品リストにもどる
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/myproduct/finish");
        }
    }

    /**
     *商品を銀行振り込みで購入する
     * @return <type>
     */
    public function bank() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        //送料
        $this->product->sf_money = $this->getRequestParam("sf_money");

        //合計（商品落札額＋送料）
        $this->product->total_price =number_format($this->product->sf_money + $this->product->f_end_price);
        //商品落札額
        $this->product->f_end_price=number_format( $this->product->f_end_price);
        //送料
        $this->product->sf_money=number_format( $this->product->sf_money);

        $this->addTplParam("product", $this->product);
        //商品購入タイプ
        $this->addTplParam("type", SHIHARAI_PRODUCT_TYPE);
        return $this->createTemplateResult("bank_info.html");
    }

}
?>
