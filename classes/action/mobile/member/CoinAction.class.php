<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

//G.Chin APROS 2010-08-24 add sta
//☆★	ライブラリ読込み	★☆
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;
//G.Chin APROS 2010-08-24 add end

import("classes.model.CoinModel");
//G.Chin 2010-12-02 add sta
import("classes.model.MemberModel");
import("classes.model.ShiharaiModel");
import("classes.model.MyProductModel");
//G.Chin 2010-12-02 add end
/**
 * Description of CoinAction
 *
 * @author ark
 */
class CoinAction extends MobileAction {
    //put your code here
    private $beforeResult="";
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->beforeResult = $this->checkPhoneSerNo();
        //ユーザログイン状況確認
        if(!$this->isLogin()) {
            //ログインしていない場合
            $this->addTplParam("error", Msg::get("NOT_LOGIN"));
            $this->beforeResult=$this->createTemplateResult("login.html");
            $this->setSession("login_change",$_SERVER["REQUEST_URI"]);
        }
        else
            //ログイン情報をテンプレートに追加
            $this->checkSessionInfo();
    }

    public function execute() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        $model=new CoinModel();
        $menu=$model->getCoinMenuByMemberId($this->getMemberIdFromSession());
        $this->addTplParam("menu", $menu);
        
//G.Chin APROS 2010-09-01 add sta
		$f_term_type = 1;	//MB
		$member_id=$this->getMemberIdFromSession();
/*
$user_agent = $_SERVER['HTTP_USER_AGENT'];
print "user_agent = [".$user_agent."]<br>\n";
*/
		
		//公式サイト決済一覧取得関数
		GetTPublicSitePlanList($f_term_type,$member_id,$tpp_fk_publicsite_id,$tpp_f_name,$tpp_f_url,$tpp_f_string,$tpp_f_tm_stamp,$tpp_link_tag,$tpp_data_cnt);
		if($tpp_data_cnt > 0)
		{
//G.Chin APROS 2010-09-30 chg sta
//			$publicsite_str = "↓公式サイト決済を利用される方はこちらをクリック。<br>\n";
//			$publicsite_str = "あと少しｺｲﾝが欲しい、ｸﾚｼﾞｯﾄｶｰﾄﾞを持っていない、そんな方はｺﾁﾗ↓<br>\n";
			$publicsite_str = "";
//G.Chin APROS 2010-09-30 chg end
			for($i=0; $i<$tpp_data_cnt; $i++)
			{
				$publicsite_str .= $tpp_link_tag[$i];
				$publicsite_str .= "<br>\n";
			}
			$publicsite_str .= "<br>\n";
		}
		else
		{
			$publicsite_str = "";
		}
        $this->addTplParam("publicsite_cnt", $tpp_data_cnt);	//G.Chin APROS 2010-09-30 add
        $this->addTplParam("publicsite_str", $publicsite_str);
//G.Chin APROS 2010-09-01 add end
        //coinplan.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        return $this->createTemplateResult("coinplan.html");
    }

    /**
     *コインプランを選択処理
     * @return <type>
     */
    public function buy() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $coinId=$this->getRequestParam("coin_id");
        if(empty($coinId))
            //エラーページに行く、後で追加する(仮で、ホームに戻る）
            return $this->createRedirectResult(Config::value("SERVER_PATH"));


        $model=new CoinModel();
        $member_id=$this->getMemberIdFromSession();
        $menu=$model->getCoinMenuById($member_id,$coinId);
        $methods=$model->getPayMethod(SHIHARAI_COIN_TYPE,$menu->f_inp_money,TERM_TYPE_MB);
		
        //購入タイプコイン
        $this->addTplParam("type", SHIHARAI_COIN_TYPE);
        $this->addTplParam("menu", $menu);
        $this->addTplParam("methods", $methods);
        return $this->createTemplateResult("buymethod.html");
    }


    /**
     *コインを銀行振り込みで購入する
     * @return <type>
     */
    public function bank(){
         //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $coin_id = $this->getRequestParam("coin_id");
        $model=new CoinModel();
        $coin=$model->getCoinSettingById($coin_id);

        $coin->f_coin = number_format($coin->f_coin);
        $coin->f_inp_money = number_format($coin->f_inp_money);
        $this->addTplParam("coin", $coin);

        //コイン購入タイプ
        $this->addTplParam("type", SHIHARAI_COIN_TYPE);

        return $this->createTemplateResult("bank_info.html");
    }

//G.Chin 2010-12-02 add sta
    /**
     *コインをｾﾞﾛｸﾚｼﾞｯﾄ決済で購入する
     * @return <type>
     */
    public function zero()
    {
         //ユーザログイン状況確認
        if(!empty($this->beforeResult))
        {
            return $this->beforeResult;
        }
        
        $buy_money = $_GET['buy_money'];
        if ($buy_money == "")
        {
            $buy_money = $_POST['buy_money'];
        }
        $payment_type = $_GET['payment'];
        if ($payment_type == "")
        {
            $payment_type = $_POST['payment'];
        }
//        $coin_count = $_GET['coin_count'];
//        if ($coin_count == "")
//        {
//            $coin_count = $_POST['coin_count'];
//        }
        $coin_count=$this->getRequestParam("coin_count");
        $pro_id=$this->getRequestParam("pro_id");
        $sf_money = $this->getRequestParam("sf_money");
        $type     = $this->getRequestParam("type");
        
        switch ($type){
            case SHIHARAI_COIN_TYPE :
                $coin->count=number_format($coin_count);
                $coin->total_price=number_format($buy_money);
                $this->addTplParam("coin", $coin);
                break;
            case SHIHARAI_PRODUCT_TYPE:
                $model= new MyProductModel();
                $product = $model->getEndProductById($pro_id);
                $product->f_end_price=number_format($product->f_end_price);
                $carriage = number_format($sf_money);
                
                $this->addTplParam("carriage",$carriage);
                $this->addTplParam("product",$product);
                $this->addTplParam("sf_money",   $sf_money);
                break;
        }
        
        $model = new ShiharaiModel();
        $member_id  = $_SESSION['member_id'];
        $status     = $type;    //支払いﾀｲﾌﾟ：ｺｲﾝ購入/商品購入
        $staff_id   = 0;        //STAFF-ID  :無し
        $min_price  = 0;        //仕入れ価格 :0円
        $model ->PutReservePayLog($status, $member_id, $staff_id, Utils::getInt($buy_money)+Utils::getInt($sf_money), $coin_count, SHIHARAI_TYPE_CREDIT, 0, $min_price,$pro_id);
        $last_id = DB::getObjectByQuery("SELECT LAST_INSERT_ID() as last_id");
        $last_insert_id = $last_id[0]->last_id;
        
        //会員情報取得（会員IDはｾｯｼｮﾝより取得)
        $member_model = new MemberModel();
        $member_id = $_SESSION['member_id'];
        $member_data  = $member_model->getMemberMasterById($member_id);
		
		//メールアドレス
		if($member_data->f_mail_address_pc != "")
		{
            $em = $member_data->f_mail_address_pc;
		}
		else
		{
            $em = $member_data->f_mail_address_mb;
		}
		
        //ZEROｸﾚｼﾞｯﾄに必要な情報を定義(POST先から各種パラメータまで）
        $act         = "imode";                //決済形態(固定値:MB)
        $send        = "cardsv";               //決済形態(固定値:PC)
        $custom      = "yes";                  //決済種別(固定値)
        $money       = $buy_money + $sf_money; //決済金額
        $usrtel      = $member_data->f_tel_no; //電話番号
        $usrmail     = $em;                    //メールアドレス
        
        //ｾﾞﾛ･ｸﾚｼﾞｯﾄに必要な情報を定義(POST先から各種パラメータまで）
        $post_url_pc = "https://credit.zeroweb.ne.jp/cgi-bin/order.cgi?orders";
        $post_url_mb = "https://credit.zeroweb.ne.jp/cgi-bin/order.cgi";
        $redirect_back_url  = SITE_URL;
        $redirect_back_url .= "index.php/thanks/";        //処理終了後戻り先
        $siteurl = $redirect_back_url;         //リンク先URL
        $sitestr = "戻る";                     //リンクタグテキスト
        
        if ($type == SHIHARAI_COIN_TYPE)
        {
            $option  = "COINS";           //独自ﾊﾟﾗﾒｰﾀ：購入対象(COIN)
        }
        else if ($type == SHIHARAI_PRODUCT_TYPE)
        {
            $option  = sprintf("PRD_%s", $pro_id);  //独自ﾊﾟﾗﾒｰﾀ：購入対象(商品)
        }
        $reserveid = $last_insert_id;         //ログ予約ID
        
        $sendid      = $member_id;             //会員ID
        $sendid     .= "_";
        $sendid     .= $option;
        
        $sendpoint   = $coin_count;            //コイン数
        $sendpoint  .= "_";
        $sendpoint  .= $reserveid;
        
        //機種情報取得PC・MBでPOST情報が違う場合あり
        $term = AgentInfo::getAccessTerminal();
        if ($term == "PC")
        {
            //PCの場合の処理
            $em = $member_data->f_mail_address_pc;
	        $clientip = ZERO_CREDIT_PC_AID;     //店舗番号
			$jump_url = $post_url_pc;
        }
        else
        {
            //MBの場合の処理
            $em = $member_data->f_mail_address_mb;
	        $clientip = ZERO_CREDIT_MB_AID;     //店舗番号
			$jump_url = $post_url_mb;
        }
/*
print "<br>\n";
print "jump_url = ".$jump_url."<br>\n";
print "clientip = ".$clientip."<br>\n";
print "send = ".$send."<br>\n";
print "custom = ".$custom."<br>\n";
print "money = ".$money."<br>\n";
print "usrtel = ".$usrtel."<br>\n";
print "usrmail = ".$usrmail."<br>\n";
print "sendid = ".$sendid."<br>\n";
print "sendpoint = ".$sendpoint."<br>\n";
print "act = ".$act."<br>\n";
print "siteurl = ".$siteurl."<br>\n";
print "sitestr = ".$sitestr."<br>\n";
*/
        
        $this->addTplParam("jump_url",   $jump_url);
        $this->addTplParam("clientip",   $clientip);
        $this->addTplParam("send",       $send);
        $this->addTplParam("custom",     $custom);
        $this->addTplParam("money",      $money);
        $this->addTplParam("usrtel",     $usrtel);
        $this->addTplParam("usrmail",    $usrmail);
        $this->addTplParam("sendid",     $sendid);
        $this->addTplParam("sendpoint",  $sendpoint);
        
        $this->addTplParam("act",        $act);
        $this->addTplParam("siteurl",    $siteurl);
        $this->addTplParam("sitestr",    $sitestr);
        
        $this->addTplParam("buy_money",  $buy_money);
        $this->addTplParam("payment",    $payment_type);
        $this->addTplParam("type",       $type);
        
        return $this->createTemplateResult("ZeroCreditConfirm.html");
    }
//G.Chin 2010-12-02 add end




}
?>
