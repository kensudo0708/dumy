<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

//☆★	ライブラリ読込み	★☆
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;

import('classes.model.MemberModel');
import("classes.utils.Message");
import("classes.model.RequestConfirm");
/**
 * Description of MemberAction
 *
 * @author ark
 */
class MemberAction extends MobileAction {
    //put your code here
    private $result=null;
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->result = $this->checkPhoneSerNo();
    }

    /**
     * myページ
     */
    public function execute() {

    }

    /**
     *会員登録画面
     * @return <ActionResult>
     */
    public function entry() {
        if(!empty ($this->result))
            return $this->result;

        $url_params=$this->session("URL_PARAMS");
        if(!empty($url_params) && count($url_params) >= 2) {
            $adcodeArray = explode(",", Config::value("ADCODE"));
            $friendArray = explode(",", Config::value("FRIEND_CODE"));
//                if($url_params[0]==Config::value("FRIEND_CODE")
//                        || $url_params[0]==Config::value("ADCODE")) {
            if((in_array($url_params[0],$adcodeArray) || in_array($url_params[0],$friendArray))) {
                //友達紹介か、広告から、コードをメールのボディに保存する
                $adcode="code:".$url_params[1];
//G.Chin 2010-07-30 add sta
		        $aflkey=$this->session("aflkey");
                $adcode.="%0D%0A";
				$adcode.="aflkey:";
				$adcode.=$aflkey;
//G.Chin 2010-07-30 add end
                $this->addTplParam("adcode", $adcode);
//G.Chin AWKT-947 2010-12-22 add sta
                if(in_array($url_params[0],$friendArray)) {
                    $this->setSession("friend_code",  $url_params[1]);
				}
//G.Chin AWKT-947 2010-12-22 add end
            }
        }
        $this->addTplParam("EMPTY_EMAIL_SEND_ADDRESS", EMPTY_EMAIL_SEND_ADDRESS);
        return $this->createTemplateResult("entry.html");
    }
    /**
     * 新規登録
     */
    public function register() {
        if(!empty ($this->result)) {
            return $this->result;
        }
        $model=new MemberModel();
        $mode=$this->getRequestParam("mode");

        $code=$this->getRequestParam("code");
//        $friend=$this->getRequestParam("friend");
        
        $url_params=Config::value("URL_PARAMS");
        if(count($url_params) >= 2) {
            $adcodeArray = explode(",", Config::value("ADCODE"));
            $friendArray = explode(",", Config::value("FRIEND_CODE"));
            if((in_array($url_params[0],$adcodeArray) || in_array($url_params[0],$friendArray))) {
                //友達紹介か、広告から
                $this->setSession("URL_PARAMS",  Config::value("URL_PARAMS"));
                return $this->createRedirectResult(Config::value("SERVER_PATH")."/member/entry");
            }
        }

        $tmp=$model->getReqhist($code);
        if(empty($tmp)) {
            $message=Msg::get("LINK_TIMEOUT");
            return Message::showConfirmMessage($this, $message);
        }
        if(empty ($code)) {
            //認証コードがない場合、正しくないURLとする、ホームに行かせ
            return $this->createRedirectResult(Config::value("SERVER_PATH"));
        }

        //年齢データを取得
        $this->addTplParam("brithday", $model->getBrithdayArray(Utils::getInt(ENTRY_AGE_LIMIT, 18)));
        //都道府県データ取得
//        $test=$model->getPerfs();
//        echo $test;

        // データ取得
        $areas=$model->getArea();
        $jobs=$model->getJob();
        $area_ids=array();
        $area_names=array();
        $job_ids=array();
        $job_names=array();
        $a_key_value=array();
        $j_key_value=array();
        foreach($areas as $area){
            array_push($area_ids, $area->area_id);
            array_push($area_names, $area->area_name);
            $a_key_value=array_combine($area_ids,$area_names);
        }
        foreach($jobs as $job){
            array_push($job_ids, $job->job_id);
            array_push($job_names, $job->job_name);
            $j_key_value=array_combine($job_ids,$job_names);
        }
        $jobs_array=array('job_id'=>$job_ids,'job_name'=>$job_names);
        $this->addTplParam("a_key_value",$a_key_value);
        $this->addTplParam("j_key_value",$j_key_value);

        if(empty ($mode)) {
            //入力画面を表示
            $member=$model->getMemberByCode($code);
            if(!empty ($member) && $member->f_activity==1) {
                //既に会員登録済みの場合、ホームに行かせ
                return $this->createRedirectResult(Config::value("SERVER_PATH"));
            }
            $this->addTplParam("code", $code);
            $this->addTplParam("mode", "inputcheck");
            return $this->createTemplateResult("register.html");
        }

        if($mode=="inputcheck") {
            $p=array();
            //$p["login_id"]=trim($this->getRequestParam("login_id"));
            $p["login_id"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("login_id"));
            //$p["login_pass"]=trim($this->getRequestParam("login_pass"));
            $p["login_pass"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("login_pass"));
            $p["handle"]=trim($this->getRequestParam("handle"));
            //$p["handle"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("handle"));
            $p["tel_no"]=trim($this->getRequestParam("tel_no"));
            $p["birth_year"]=$this->getRequestParam("birth_year");
            $p["birth_month"]=$this->getRequestParam("birth_month");
            $p["birth_day"]=$this->getRequestParam("birth_day");
            $p["sex"]=$this->getRequestParam("sex");
            $p["onamae"]=trim($this->getRequestParam("onamae"));
            $p["post_code"]=$this->getRequestParam("post_code");
            $p["pref_id"]=$this->getRequestParam("pref_id");
            $p["address1"]=$this->getRequestParam("address1");
            $p["address2"]=$this->getRequestParam("address2");
            $p["guid"]=$this->getRequestParam("guid");
            $p["address3"]=$this->getRequestParam("address3");
            $p["mailmagazine"]=$this->getRequestParam("mailmagazine");
            $p["agreement"]=$this->getRequestParam("agreement");
            $p["area"]=$this->getRequestParam("area");
            $p["job"]=$this->getRequestParam("job");

            $p["sex"] = isset($p["sex"]) ? ($p["sex"] == "0" ? "0" : "1") : "0";
            //入力チェック
            import("classes.utils.InputCheck");
            $message=InputCheck::registInputCheck($p);
            if($message===TRUE) {
                //チェックを通った後、登録内容を確認させる
                $p["birthday"]=$p["birth_year"]."年".$p["birth_month"]."月".$p["birth_day"]."日";
                $p["sex_string"]=$p["sex"]=="0" ? "男性" : "女性";
                $p["mailmagazine_string"]=$p["mailmagazine"]==1 ? "Yes" : "No";

//                //都道府県
//                if($p["pref_id"]!=0) {
//                    $pre=$model->getPrefById($p["pref_id"]);
//                    $p["pref_name"]=$pre->t_pref_name;
//                }
                //会員登録する際、住所を入力しないことに変更する
                //都道府県
                if(isset($p["area"])) {
                    $area=$model->getAreaById($p["area"]);
                    foreach($area as $area_id_name){
                         $p["area_name"]=$area_id_name->area_name;
                    }
                }
                //職業
                if(isset($p["job"])) {
                    $job=$model->getJobById($p["job"]);
                    foreach($job as $job_id_name){
                        $p["job_name"]=$job_id_name->job_name;
                    }

                }
//                echo $p["area_name"];
//                echo $p["job_name"];
                $p["fk_address_flg"]=0;
                if(ereg("KDDI",$_SERVER['HTTP_USER_AGENT']) ) {
                    $p["handle"]=mb_convert_kana($p["handle"],"K","SJIS" );
                    $p["onamae"]=mb_convert_kana($p["onamae"],"K","SJIS" );
                    $p["handle"]=mb_convert_encoding($p["handle"], "UTF-8","SJIS");
                    $p["onamae"]=mb_convert_encoding($p["onamae"], "UTF-8","SJIS");
                }
                else {
                    $p["handle"]=mb_convert_kana($p["handle"],"K","UTF-8" );
                    $p["onamae"]=mb_convert_kana($p["onamae"],"K","UTF-8" );
                }
                $this->addTplParam("p", $p);
                $this->addTplParam("mode", "register");
                $this->addTplParam("code", $code);


                //入力した内容をセッションに保存
                $this->setSession("inputcontent", $p);
                return $this->createTemplateResult("register_confirm.html");
            }
            else {
                //再入力
                if(ereg("KDDI",$_SERVER['HTTP_USER_AGENT']) ) {

                    $p["handle"]=mb_convert_kana($p["handle"],"K","SJIS" );
                    $p["onamae"]=mb_convert_kana($p["onamae"],"K","SJIS" );
                    $p["handle"]=mb_convert_encoding($p["handle"], "UTF-8","SJIS");
                    $p["onamae"]=mb_convert_encoding($p["onamae"], "UTF-8","SJIS");
                }
                else {
                    $p["handle"]=mb_convert_kana($p["handle"],"K","UTF-8" );
                    $p["onamae"]=mb_convert_kana($p["onamae"],"K","UTF-8" );
                }
                $this->addTplParam("message", $message);
                $this->addTplParam("p", $p);
                $this->addTplParam("mode", "inputcheck");
                $this->addTplParam("code", $code);
                return $this->createTemplateResult("register.html");
            }
        }
        else {

            $p=$this->session("inputcontent");
            if($p===false) {
                //セッション時間切れ、再入力させる
                $this->addTplParam("mode", "inputcheck");
                return $this->createTemplateResult("register.html");
            }
            //登録処理
            $p["f_adcode"]=$code;
            $p["birthday"]=$p["birth_year"]."-".$p["birth_month"]."-".$p["birth_day"];
            //PCの場合、普通会員として登録する(仮登録）
            $p["activity"]="1";
            $p["member_group"]="1";
            //メールマガジン　　0:拒否　１：受取
            $p["mailmagazine"] = $p["mailmagazine"]==1 ? 1 : 0;
            //パスワード暗号化(仮のもの）
            $p["login_pass"]=md5($p["login_pass"]);

            $ser_no=AgentInfo::getPhoneCode();
            if(!empty($ser_no))
                $p["f_ser_no"]=$ser_no;

            if(ereg("KDDI",$_SERVER['HTTP_USER_AGENT']) ) {
                $p["handle"]=mb_convert_encoding($p["handle"], "UTF-8","UTF-8,SJIS,EUC-JP");
                $p["onamae"]=mb_convert_encoding($p["onamae"], "UTF-8","UTF-8,SJIS,EUC-JP");
            }
            $member=$model->saveNewMemberFromMobile($p);
            if($member!=FALSE) {
                //入力した内容をクリアする
                $this->removeSession("inputcontent");
                //登録成功、
                $this->addSessionInfo($member);
                $message=Msg::get("REGISTER_SUCCESS");
//                return $this->createRedirectResult(Config::value("SERVER_PATH"));
                //メール送信処理
                import("classes.utils.MailReserver");
                MailReserver::sendRegistSuccessMail($member,false);
            }
            else {
                //登録失敗
                $message=Msg::get("PROCESS_FAILED");
//                return $this->createTemplateResult("register.html");
            }
            $this->addTplParam("message", $message);

//G.Chin 2010-06-30 add sta
//G.Chin 2010-07-28 chg sta
/*
            $so = $member->fk_member_id;

            //会員親紹介コード取得関数
            GetTMemberFKParentAd($so,$tmm_fk_parent_ad);
            //アフィリエイト広告情報取得関数
            GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
			//アフィリエイト広告登録判定関数
			CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
			if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($taf_f_type == 1) && ($not_report == 0))
			{
                //▼状態が"有効"で、登録時報酬が"有り"で、種類が"MOBA8"の場合
                $pid = $taf_fk_program_id;
                //▼SIパラメータの有無を判定
                if($taf_f_param2 == "") {
                    $affiliate_tag = "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=1.1.1.regist' width='100' height='100'><br>";
                }
                else {
                    $si = $taf_f_param2;
                    $affiliate_tag = "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=$si' width='100' height='100'><br>";
                }
				
				//アフィリエイト広告登録報告数加算関数
				CountUpTAffiliateFRegReport($tmm_fk_parent_ad);	//G.Chin 2010-07-26 add
            }
            else {
                //▼状態が"無効"、もしくは種類が"MOBA8"でない場合
                $affiliate_tag = "";
            }
*/
			//アフィリエイトタグ作成関数
			MakeAffiliateTag($member->fk_member_id,0,1,$affiliate_tag);
            $this->addTplParam("affiliate_tag", $affiliate_tag);
//G.Chin 2010-07-28 chg end
//G.Chin 2010-06-30 add end
			
//G.Chin 2010-11-05 add sta
			//アフィリエイトタグ作成関数２
			MakeAffiliateTag2($member->fk_member_id,0,1,$affiliate_tag2);
			//メッセージ画面にチェンジする
			$this->addTplParam("affiliate_tag2", $affiliate_tag2);
//G.Chin 2010-11-05 add end
            return $this->createTemplateResult("memreg_complete.html");
        }

    }

    /**
     * パスワードを忘れた場合
     */
    public function forget() {
//        if(!empty ($this->result))
//            return $this->result;

        $mode=$this->getRequestParam("mode");
        if(empty ($mode)) {
            //入力フォームが表示
            $this->addTplParam("mode", "send");
            return $this->createTemplateResult("password_forget.html");
        }

        $email=$this->getRequestParam("email");
        $model=new MemberModel();
        if(empty ($email)) {
            $this->addTplParam("mode", "send");
            $this->addTplParam("message", Msg::get("REGISTER_EMAIL_EMPTY"));
            return $this->createTemplateResult("password_forget.html");
        }
        else {
            //メールの存在チェック
            $member=$model->getMemberMasterByMail($email);
            if(empty ($member)) {
                $this->addTplParam("mode", "send");
                $this->addTplParam("message", Msg::get("EMAIL_NOT_EXISTED"));
                return $this->createTemplateResult("password_forget.html");
            }
        }

        //メールが既に送信した場合、再送信しない
        $confirmModel = new RequestConfirm();
        $serial = $confirmModel->getRequestConfirmBySerial($member->f_adcode);
        if(!empty($serial)) {
            return Message::showConfirmMessage($this, Msg::get("PASSWORD_RESET_EMAIL_HAS_SENT"));
        }

        //メール送信処理
//        $time_limit=md5(date("Y-m-d H:i:s"));
        $to = $email;
        $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/pass_reset_body.html";
        $from = "From: ".INFO_MAIL."\nReturn-Path:".INFO_MAIL;
        $params=array("member_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
                "login_id"=>$member->f_login_id,
                "reg_confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/member/pass_reset?code=".$member->f_adcode,
                "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
//        Utils::sendTemplateMail($to, PASSWORD_RESET_SUBJECT, $tpl,$from,$params);

        import("classes.utils.MailReserver");
        $result = MailReserver::sendTemplateMail(MailReserver::PASSWORD,PASSWORD_RESET_SUBJECT, $tpl, $params, $from, $to);
        if(!$result) {
            //メールを送信失敗
            $this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
            return $this->createTemplateResult("password_forget.html");
        }
        //送信時刻を記録する
        $confirm=new RequestConfirm();
        $confirm->saveNewConfirm(1, $member->f_adcode, PASSWORD_RESET_LIMIT);
        return Message::showConfirmMessage($this, Msg::get("PASSWORD_RESET_EMAIL_SEND"));
    }


    /**
     * パスワード再設定処理
     */
    public function pass_reset() {
//        if(!empty ($this->result))
//            return $this->result;

        $model=new MemberModel();

        $mode=$this->getRequestParam("mode");
        if(empty($mode) || !$this->isPost()) {
            //リングからきた場合
            $code=$this->getRequestParam("code");
            if(empty ($code)) {
                //無効なリクエスト
                return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));
            }
            else {
                //コードの有効性と、時間制限が越えたかどうか、チェック
                $member=$model->getMemberByCode($code);
                if(empty ($member))
                //無効なリクエスト
                    return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

                $confirmModel=new RequestConfirm();
                $confirm = $confirmModel->getRequestConfirmBySerial($code);
                if(empty($confirm))
                //無効なリクエスト
                    return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

                $now=strtotime($confirm->sys_time);
                $sendTime=strtotime($confirm->f_updatetm);
//                $dif= Utils::getInt(($now- $sendTime) / 3600,0);
                if($now - $sendTime  > $confirm->f_timelimit)
                //リングの有効期限チェックする
                    return Message::showConfirmMessage($this, Msg::get("LINK_TIMEOUT"));

                //再設定フォームが表示する
                $this->addTplParam("mode", "send");
                $this->addTplParam("idcode", $code);
                return $this->createTemplateResult("password_reset.html");
            }
        }


        $code=$this->getRequestParam("idcode");
        
        //入力チェック
        $p["new_pass"]=$this->getRequestParam("pass");
        $p["repeat_pass"]=$this->getRequestParam("repass");

        //入力チェック
        $m=array();
        if(empty($p["new_pass"]))
            array_push($m, Msg::get("MY_NEW_PASSWORD_EMPTY"));
        if(empty($p["repeat_pass"]))
            array_push($m,Msg::get("MY_REPEAT_PASSWORD_EMPTY"));
        if(!empty($p["new_pass"]) && !empty($p["repeat_pass"]) && $p["repeat_pass"]!=$p["new_pass"])
            array_push($m,Msg::get("MY_PASSWORD_NOT_REPEAT"));
        $message="";
        if(count($m)!=0) {
            //再入力
            foreach ($m as $value)
                $message.=$value."<br>";
            $this->addTplParam("message", $message);
            $this->addTplParam("mode", "send");
            $this->addTplParam("idcode", $code);
            return $this->createTemplateResult("password_reset.html");
        }
        //パスワード更新処理
        
        
        if(empty ($code))
        //無効なリクエスト
            return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

        $member=$model->getMemberByCode($code);
        if(empty ($member))
        //無効なリクエスト
            return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

        $result = $model->updatePassword($member->fk_member_id, md5($p["new_pass"]));
        if($result===false)
            return Message::showConfirmMessage($this, Msg::get("PROCESS_FAILED"));
        else {
            //リクエスト確認データを削除する
            $confirmModel=new RequestConfirm();
            $confirmModel->deleteConfirm($code);
            return Message::showConfirmMessage($this, Msg::get("PASSWORD_RESET_SUCCESS"));
        }
    }

}
?>
