<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.ProductModel');
import('classes.model.ProductGeneral');
/**
 * 開催カテゴリ商品一覧の処理
 *
 * @author mw
 */
class ProductCategoryAction extends MobileAction {
    //put your code here

    private $result=null;
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->result = $this->checkPhoneSerNo();
    }
    
    /**
     *開催中カテゴリ商品リスト
     * @return <type>
     */
    public function execute() {
        if(!empty ($this->result))
            return $this->result;

        //チェックログイン状況
        $this->checkSessionInfo();
        
        $model=new ProductModel();
        $cate_id=$this->getRequestParam("id");

        //レコード数を取得
        $recordCount=Utils::getInt($model->getCategoryProductCount($cate_id,$this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $pageSize=Utils::getInt(MOBILE_PROUDUCT_CATEGORY_QTY,0);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbersI($pageId,$pageCount,Config::value("SERVER_PATH")."/category?id=$cate_id",10);
            //開催中のカテゴリ商品を取得
            $products=$model->getCategoryProducts($cate_id,($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"),false);
            
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/mb/".$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $obj->price=number_format($obj->price);
                //ﾃﾞｨｽｶｳﾝﾄ額
                $obj->discount_price=$obj->market_price-$price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;
            }
            $this->addTplParam("products", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }
//        print("count:".count($products));
        $cate=$model->getProductCategoriyById($cate_id);
        $this->addTplParam("cate", $cate);
        
        //商品のカテゴリの種類データを取得する
        ProductGeneral::addCategoriyInfo($this);
        
        return $this->createTemplateResult("auction_category.html");
    }
}
?>
