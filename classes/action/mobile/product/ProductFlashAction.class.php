<?php
import('classes.model.ProductModel');
import('classes.utils.Message');
import('classes.utils.ProductFilter');

/**
 * モバイルFlashページへのリダイレクタ
 *
 */
class ProductFlashAction extends MobileAction {

    private $result=null;
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->result = $this->checkPhoneSerNo();
        //ユーザログイン状況確認
        $this->checkSessionInfo();
    }

    /**
     *開催商品の詳細情報を表示
     * @return <type>
     */
    public function execute() {
        if(!empty ($this->result)) {
            return $this->result;
        }

        // システムパラメータ確認
        $flagName = 'MOBILE_FLASH_ENABLE';
        if ( !defined($flagName) || !constant($flagName) ) {
            Logger::error('constant '.$flagName.' is undefined or disabled.');
            //無効なリクエストの場合
            return Message::showErrorRequest($this);
        }
        $constant_array = array(
            'MOBILE_FLASH_DETAIL_LAYOUT'=>'1,2,3,4',
            'MOBILE_FLASH_LOGO_IMG'=>'',
            'MOBILE_FLASH_BG_IMG'=>'',
            'MOBILE_FLASH_FONT_COLOR'=>'#000',
            'MOBILE_FLASH_FONT_COLOR_TIMELIMIT'=>'#F00',
            'MOBILE_FLASH_PRD_IMG_PREFIX'=>'',
            'MOBILE_FLASH_PRD_NO_IMG'=>'',
        );
        foreach ( $constant_array as $key=>$value ) {
            if ( defined($key) ) {
                $constant_array[($key)] = constant( $key );
            } else {
                Logger::warn('constant '.$key.' is undefined.');
            }
        }

        /**
         * ■リクエストパラメータ
         *   pro_id: 商品ID
         *   boot: 0:商品一覧画面からアクセス、1:商品詳細画面からアクセス
         *   page: 一覧上の現在ページ（セッション情報として設定されている場合はそちらを優先）
         */
        $product_id = $this->getRequestParam("pro_id");
        $boot = Utils::getInt($this->getRequestParam("boot"),0);
        $pageId = $this->getRequestParam("page");
        if ( isset( $_SESSION['page'] ) ) {
            $pageId = $_SESSION['page'];
        }

        // 商品詳細情報の取得
        if( !$product_id ) {
            //無効なリクエストの場合
            return Message::showErrorRequest($this);
        }

        //商品ID
        $model = new ProductModel();
        $p = $model->getDetailProductById( $product_id );
        if(!empty ($p)) {
            switch ( $p->f_status ) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                // nop
                break;
            default:
                //無効なリクエストの場合
                return Message::showErrorRequest($this);
            }
        } else {
            //無効なリクエストの場合
            return Message::showErrorRequest($this);
        }

        // 商品の詳細情報
        $product = $model->getAuctionProductById( $product_id, false );
        if ( $this->isLogin() ) {
            $member = $this->getRequestAttribute("member");
            //初心者の場合、該当商品が初心者フィルタにかけられたかどうか、チェックする
            if ( ProductFilter::matchFirstFilter($product, $member) ) {
                //無効なリクエストの場合
                return Message::showErrorRequest($this);
            }
        }
        // 戻り先URL
        $retURL = SITE_URL;
        if ( $boot ) {
            $retURL = '/index.php/product?pro_id='.$product_id;
        } else {
            if ( $pageId ) {
                $retURL = '/index.php/list?page='.$pageId;
            }
        }

        $flash_param_array = array(
            'pro_id'=>$product_id,
            'PHPSESSID'=>isset($_GET['PHPSESSID']) ? $_GET['PHPSESSID']:'',
            'program_name'=>SITE_NAME,
            'top_url'=>SITE_URL,
            'ret_url'=>$retURL,
            'connect_url'=>AJAX_HOST.'/index.php/ajax/auto',
            'font_color'=>$constant_array['MOBILE_FLASH_FONT_COLOR'],
            'timelimit_font_color'=>$constant_array['MOBILE_FLASH_FONT_COLOR_TIMELIMIT'],
            'mobile_logo'=>$constant_array['MOBILE_FLASH_LOGO_IMG'],
            'mobile_bg'=>$constant_array['MOBILE_FLASH_BG_IMG'],
            'img_url01'=>'',
            'img_url02'=>'',
            'img_url03'=>'',
            'img_url04'=>'',
            'many_connections'=>AJAX_LIMIT_COUNT,
            'layout'=>$constant_array['MOBILE_FLASH_DETAIL_LAYOUT'],
            'product_list'=>$this->order($pageId),
        );
        // 画像のパスを生成
        $photoes = array("f_photo1mb","f_photo2mb","f_photo3mb","f_photo4mb");
        $dir = Config::value("PRODUCT_IMAGE_PATH").'/mb/';
        $i = 1;
        foreach( $photoes as $name ) {
            $key = sprintf('img_url%02d', $i);
            if( isset($product->$name) && $product->$name ) {
                $flash_param_array[($key)] = $dir.$constant_array['MOBILE_FLASH_PRD_IMG_PREFIX'].md5($product->$name).'.jpg';
            } else {
                $flash_param_array[($key)] = MOBILE_FLASH_PRD_NO_IMG;
            }
            $i++;
        }

        $url = SITE_URL.'/flash/wrap.php';
        $params = implode( '=%s&', array_keys( $flash_param_array ) ).'=%s';
        $params = vsprintf( $params, array_map( 'urlencode', $flash_param_array ) );
        $url .= '?'.$params;
        return $this->createRedirectResult( $url );
    }

    public function help() {
        return $this->createTemplateResult( 'realtime_bid.html' );
    }

    /**
     * ページ番号に対応する商品IDをカンマ区切り文字列で取得する
     * @param int $pageId
     * @return <string>
     */
    protected function order($pageId) {
        if ( !$pageId ) {
            $pageId = 1;
        }

        $model = new ProductModel();
        //開催中商品のレコード数を取得
        $recordCount=Utils::getInt($model->getAuctionProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //１ページの商品数
            $pageSize = MOBILE_PROUDUCT_OPEN_QTY;
            //開催中の商品を取得
            $products=$model->getProducts(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));

            $result = '';
            if(is_array($products)) {
                foreach ($products as $obj) {
                    if ( $result ) {
                        $result .= ',';
                    }
                    $result .= $obj->id;
                }
            }
        }
        return $result;
    }
}
?>
