<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of LogoutAction
 *
 * @author mw
 */
class LogoutAction extends MobileAction {
    //put your code here
    /**
     * ログアウト処理
     */
    public function execute() {
        if($this->isLogin()) {
            import("classes.model.MemberModel");
            $model = new MemberModel();
            $model->clearMemberSerno($this->getMemberIdFromSession());
            $this->clearSessionInfo();
            
        }
        return $this->createRedirectResult(Config::value("SERVER_PATH"));
    }
}
?>
