<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.MemberModel');
import("classes.utils.Message");
/**
 * Description of LoginActionclass
 *
 * @author mw
 */

//☆★	ライブラリ読込み	★☆
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;

class LoginAction extends MobileAction {

    private $result=null;
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->result = $this->checkPhoneSerNo();
    }
    /**
     * ユーザのログイン処理
     */
    public function execute() {
        if(!empty ($this->result))
            return $this->result;
        if($this->isLogin())
        //ログイン済みの場合、index画面に行かせ
            return $this->createRedirectResult(Config::value("SERVER_PATH"));

        $id=trim($this->getRequestParam("login_id"));
        $pass=trim($this->getRequestParam("login_pass"));

        if(empty($id) || empty($pass)) {
            //未入力の場合
            $this->addTplParam("error",  Msg::get("ID_PASS_ERROR"));
            return $this->createTemplateResult("login.html");
        }
        else {
            //入力チェック
            //キャッシュからマスタ情報取得
            $model=new MemberModel();
            $master=$model->getMemberMasterByLoginId($id);
            if($master===NULL || md5($pass)!=$master->f_login_pass || $master->f_activity!=1) {
                //ログインできない場合
                $this->addTplParam("error", Msg::get("ID_PASS_ERROR"));
                return $this->createTemplateResult("login.html");
            }

            switch ($master->f_activity) {
                case 1:     //普通会員
                //Sessionにログイン情報を追加

                    $this->addSessionInfo($master);
                    //ログイン成功、マイページに戻る(仮でホームに戻る）
                    $ser_no=AgentInfo::getPhoneCode();
                    if(!empty ($ser_no))
                    //識別コードが取得できたら、更新処理
                        $model->updateMemberSerNo($master->fk_member_id, $ser_no);
                    if (isset($_SESSION["login_change"])) {
                        $result=$this->createRedirectResult($this->session("login_change"));
                        $this->removeSession("login_change");
                    }else
                        $result=$this->createRedirectResult(Config::value("SERVER_PATH").LOGIN_JUMP_URL);
                    break;
                case 2:     //退会会員
                //ログインできない場合
                    $this->addTplParam("error", Msg::get("ID_PASS_ERROR"));
                    return $this->createTemplateResult("login.html");
                    break;
                default :
                    $this->addTplParam("error", Msg::get("ID_PASS_ERROR"));
                    return $this->createTemplateResult("login.html");
                    break;

            }
            return $result;
        }
    }

    /**
     *ログインフォームを表示する
     * @return <type>
     */
    public function form() {
        if(!empty ($this->result))
            return $this->result;
        if($this->isLogin())
        //ログイン済みの場合、index画面に行かせ
            return $this->createRedirectResult(Config::value("SERVER_PATH"));
        $phone=AgentInfo::getPhoneType();
        //DoCoMoの携帯の場合のみ
        if(!empty($phone) && $phone=="DoCoMo") {
            $p="?guid=ON";
            $this->addTplParam("guid", $p);
        }
        return $this->createTemplateResult("login.html");
    }

    /**
     * 機種変更より、再登録
     */
    public function phone() {
        if(!empty ($this->result))
            return $this->result;

        import("classes.utils.Message");
        $code=$this->getRequestParam("code");
        if(empty ($code))
        //無効なアクセス
            Message::showErrorRequest($this);

        import('classes.model.MemberModel');
        $model = new MemberModel();
        $member = $model->getMemberByCode($code);
        if(empty ($member))
        //無効なアクセス
            Message::showErrorRequest($this);

        $ser_no=AgentInfo::getPhoneCode();
        $model->updateMemberSerNo($member->fk_member_id, $ser_no);

        return Message::showConfirmMessage($this, Msg::get("MEMBER_PHONE_CHANGE_SUCCESS"));

    }

    /**
     *仮会員のURL確認処理
     */
    public function confirm() {
        if(!empty ($this->result))
            return $this->result;

        $code=$this->getRequestParam("code");
        $model=new MemberModel();

        $member=$model->getMemberByCode($code);
        if($member!==NULL && $member->f_activity!=2) {
            if($member->f_activity==1) {
                //登録済みの場合
                $message=Msg::get("HAS_REGISTERED");
            }
            else {
                //登録する
                //識別コードを取得する

                $member->f_ser_no=AgentInfo::getPhoneCode();

                if($model->updateMemberActivity($member,false)) {
                    $this->addSessionInfo($member);
                    $message=Msg::get("REGISTER_SUCCESS");
                    //メール送信処理
                    import("classes.utils.MailReserver");
                    MailReserver::sendRegistSuccessMail($member);
                }
                else {
                    $message=Msg::get("REGISTER_CONFIRM_FAILED");

                }


                $this->addTplParam("message", $message);

//G.Chin 2010-06-25 add sta
//G.Chin 2010-07-28 chg sta
                /*
			$so = $member->fk_member_id;
			
			//会員親紹介コード取得関数
			GetTMemberFKParentAd($so,$tmm_fk_parent_ad);
			//アフィリエイト広告情報取得関数
			GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
			//アフィリエイト広告登録判定関数
			CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
			if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($taf_f_type == 1) && ($not_report == 0))
			{
				//▼状態が"有効"で、登録時報酬が"有り"で、種類が"MOBA8"の場合
				$pid = $taf_fk_program_id;
				//▼SIパラメータの有無を判定
				if($taf_f_param1 == "")
				{
					$affiliate_tag = "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=1.1.1.regist' width='100' height='100'><br>";
				}
				else
				{
					$si = $taf_f_param1;
					$affiliate_tag = "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=$si' width='100' height='100'><br>";
				}
				
				//アフィリエイト広告登録報告数加算関数
				CountUpTAffiliateFRegReport($tmm_fk_parent_ad);	//G.Chin 2010-07-26 add
			}
			else
			{
				//▼状態が"無効"、もしくは種類が"MOBA8"でない場合
				$affiliate_tag = "";
			}
                */
                //アフィリエイトタグ作成関数
                MakeAffiliateTag($member->fk_member_id,0,1,$affiliate_tag);
                //アフィリエイトタグ作成関数２
                MakeAffiliateTag2($member->fk_member_id,0,0,$affiliate_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-07-28 chg end
                /*
            if(MOBA8_FLAG == 0)
            {
                $affiliate_tag = "";
            }
            else
            {
                $pid = MOBA8_PID;
                $so = $member->fk_member_id;
                $affiliate_tag = "<img src='http://px.moba8.net/svt/sales?guid=on&PID=$pid&SO=$so&SI=1.1.1.regist' width='100' height='100'><br>";
            }
                */
                $this->addTplParam("affiliate_tag", $affiliate_tag);
                $this->addTplParam("affiliate_tag2", $affiliate_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-06-25 add end

                return $this->createTemplateResult("memreg_complete.html");
//            return Message::showConfirmMessage($this, $message);
            }
            //code認証できない場合、ホームに行かせ
            return $this->createRedirectResult(Config::value("SERVER_PATH"));
        }

        $message=Msg::get("REGISTER_TIMEOUT");
        $this->addTplParam("message", $message);
        return $this->createTemplateResult("memreg_complete.html");
    }
}
?>
