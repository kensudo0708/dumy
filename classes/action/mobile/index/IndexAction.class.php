<?php


import('classes.model.ProductModel');
import('classes.model.ProductGeneral');
//import('classes.repository.ProductRepository');
/**
 * Description of IndexAction
 *
 * @author mw
 */
class IndexAction extends MobileAction {

    private $result=null;
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->result = $this->checkPhoneSerNo();
    }
    /**
     *ヤスオクのホームページ
     * @return <ActionResult>
     */
    public function execute() {
        if(!empty ($this->result))
            return $this->result;

        //チェックログイン状況
        $this->checkSessionInfo();

        $model=new ProductModel();
        //開催中商品データ取得
        $recordCount=Utils::getInt($model->getAuctionProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            if ( isset($_SESSION['page']) ) {
                unset($_SESSION['page']);
            }
            //１ページの商品数
            $pageSize=Utils::getInt(MOBILE_INDEX_OPEN_QTY);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbersI($pageId,$pageCount,Config::value("SERVER_PATH")."/list",10);
            import("classes.utils.Page");
            $pageNumber1=Page::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH"),10);
            //開催中の商品を取得
            $products=$model->getProducts(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"),false);
            
            if(is_array($products)) {
                foreach ($products as $obj) {
                    $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/mb/".$obj->image;
                    if($obj->time <=0)
                    {
                        $obj->time="処理中です。";
                    }
                    else
                    {
                        $obj->time=Utils::getTimeString($obj->time);
                    }
                    $price=($obj->price);
                    $obj->price=number_format($obj->price);
                    //ﾃﾞｨｽｶｳﾝﾄ額
                    $obj->discount_price=$obj->market_price-$price;
                    $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                    $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                    $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;
                }
                $this->addTplParam("products", $products);
                $this->addTplParam("pageNumber", $pageNumber);
                $this->addTplParam("pageNumber1", $pageNumber1);
            }
        }

        //注目商品データを取得
        $qty=Utils::getInt(MOBILE_NOTICE_CLOSE_QTY,0);
        $noticeProducts=$model->getNoticeProduct($qty,$this->getRequestAttribute("member"),false);
        
        if(is_array($noticeProducts)) {
            foreach ($noticeProducts as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/mb/".$obj->image;
                if($obj->time <=0)
                {
                    $obj->time="処理中です。";
                }
                else
                {
                    $obj->time=Utils::getTimeString($obj->time);
                }
                $price=($obj->price);
                $obj->price=number_format($obj->price);
                //ﾃﾞｨｽｶｳﾝﾄ額
                $obj->discount_price=$obj->market_price-$price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;
            }

            $this->addTplParam("notices", $noticeProducts);
        }
        //終了商品データを取得
        $pageSize=Utils::getInt(MOBILE_INDEX_CLOSE_QTY,0);
        $endProducts=$model->getEndProductsIndex($pageSize,false);
        if(count($endProducts)>0) {
            foreach ($endProducts as $obj) {
//                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/mb/".$obj->image;
//                $obj->time=Utils::getDateTimeString($obj->time);
//
////              $obj->price=number_format($obj->price);
//                $pro_id=$obj->id;
//                $mem_id=$obj->last_bidder_id;
//                $bid =$model->GetEndProdCoin($pro_id, $mem_id);
//                //$endProduct=$model->getEndProductById($pro_id);
//                //落札コイン数
//                //$endProduct->bidcoins=($endProduct->bidcoins)*($endProduct->f_spend_coin);
//                //落札用コインマネ
//                //$endProduct->bidcoin_money=($endProduct->bidcoins)*POINT_VALUE;
//                //落札商品の落札価格と落札用コインマネのプラス結果
//                $endProduct->bidcoins=($bid->bidcoins)*($obj->f_spend_coin);
//                $endProduct->bidcoin_money=($endProduct->bidcoins)*POINT_VALUE;
//
//                $obj->plus_end_price=$obj->price+$endProduct->bidcoin_money;
//                //ﾃﾞｨｽｶｳﾝﾄ額
//                $obj->discount_price=$obj->market_price-$obj->price;
//                $obj->plus_discount_price=$obj->market_price-$obj->plus_end_price;
//                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
//                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
//                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;
//
//                $obj->plus_discount_rate=floor(($obj->plus_discount_price/$obj->market_price) * 100);
//                $obj->plus_discount_rate=$obj->plus_discount_rate<1 ? 0 :$obj->plus_discount_rate;
//                $obj->price=number_format($obj->price);
//G.Chin AWKT-546 2010-10-27 chg sta
//                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/mb/".$obj->image;
//G.Chin AWKT-546 2010-10-27 chg end
                $obj->time=Utils::getDateTimeString($obj->time);
                $price = $obj->price;
                $obj->price=number_format($obj->price);
                $pro_id=$obj->id;
                $tmp_auction = $model->GetEndProdDate($pro_id);
                $mem_id=$obj->last_bidder_id;
                $bid =$model->GetEndProdCoin($pro_id, $mem_id);

                $endProduct->bidcoins=($bid->bidcoins)*($tmp_auction->f_spend_coin);
                $endProduct->bidcoin_money=($endProduct->bidcoins)*POINT_VALUE;

                $obj->f_auction_name = $tmp_auction->f_auction_name;
                $obj->auction_type = $tmp_auction->fk_auction_type_id;
                $tmp_mem =$model->GetEndBidderDate($mem_id);
                //var_dump($tmp_auction);
                //var_dump($tmp_mem);

                $obj->market_price=$tmp_auction->f_market_price;

                $obj->last_bidder = $tmp_mem->f_handle;
                //echo $obj->last_bidder;
                $obj->plus_end_price=$obj->price+$endProduct->bidcoin_money;
                $obj->discount_price=$obj->market_price-$price;
                //$obj->discount_price=$obj->market_price-$obj->price;
                $obj->plus_discount_price=$obj->market_price-$obj->plus_end_price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;

                $obj->plus_discount_rate=floor(($obj->plus_discount_price/$obj->market_price) * 100);
                $obj->plus_discount_rate=$obj->plus_discount_rate<1 ? 0 :$obj->plus_discount_rate;
            }
            $this->addTplParam("ends", $endProducts);
        }

        //商品のカテゴリの種類データを取得する
        ProductGeneral::addCategoriyInfo($this);

        //ピックアップ商品を取得する
        $pageSize=Utils::getInt(MOBILE_INDEX_PICKUP_QTY,0);
        $picks=$model->getPickupProducts($pageSize,false);
        if(count($picks)>0) {
            foreach ($picks as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/mb/".$obj->image;
                $obj->time=Utils::getDateTimeString($obj->time);
                $price=($obj->price);
                $obj->price=number_format($obj->price);

                $pro_id=$obj->id;
                $mem_id=$obj->last_bidder_id;
                $endProduct=$model->GetEndProdCoin($pro_id, $mem_id);
                $endProduct->bidcoins=($endProduct->bidcoins)*($obj->f_spend_coin);
                $endProduct->bidcoin_money=($endProduct->bidcoins)*POINT_VALUE;
                $obj->plus_end_price=$price+$endProduct->bidcoin_money;
                
                //ﾃﾞｨｽｶｳﾝﾄ額
                $obj->discount_price=$obj->market_price-$price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;
            }
            $this->addTplParam("picks", $picks);
        }


//G.Chin 2010-06-18 del sta
/*
        //URLについていたパラメータがあったら、
        $url_params=Config::value("URL_PARAMS");
        if(count($url_params)>0) {
            //URL_PARAMSを記録する(広告コートなど)
            if(!isset($_SESSION['URL_PARAMS']) && count($url_params) >= 2 && $url_params[0]==Config::value("ADCODE")) {
                //広告主の場合
                import('classes.model.MemberModel');
                $model = new MemberModel();
                $ad = $model->getAdcodeMasterId($url_params[1]);
                if(!empty ($ad)) {
                    import('classes.model.Total');
                    Total::access($ad->fk_admaster_id,false);
                }
            }
            $this->setSession("URL_PARAMS",  Config::value("URL_PARAMS"));
        }
*/
//G.Chin 2010-06-18 del end
        //indexページに会員グループ追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        return $this->createTemplateResult("index.html");
    }








}



?>