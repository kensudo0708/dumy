<?php
/**
 * 本モジュールはモバイルFlashからアクセスするために暫定的にPC側の実装をコピーしています。
 *
 *
 *
 */


//require_once( realpath(dirname(__FILE__).'/../../pc/index/IndexAction.class.php') );

// form:classes/action/pc/index/IndexAction.class.php
import('classes.model.ProductAjaxModel');
import('classes.model.ProductGeneral');

// from:classes/action/pc/product/ProductAction.class.php
//import('classes.model.ProductModel');
import('classes.model.BidModel');
import("classes.utils.Message");
import('classes.utils.ProductFilter');

class AjaxAction extends MobileAction {

// form:pc/index/IndexAction.class.php

    /**
     * クライアント側からのAjax通信処理
     *
     * このメソッドは商品一覧ページと商品詳細ページからアクセスされ、それぞれでリクエストパラメータが異なります。
     *
     * @param ids 商品一覧情報の取得時。商品IDをカンマ区切りで
     * @param detail 商品ID
     *
     * @return <ActionResult>
     */
    public function auto() {

        header('ContentType: text/plain; charset=UTF-8');
        $resulText = '';

        // メンテナンス中チェック
        $maite = FALSE;
        if( MAINTE_FLAG == SYS_CLOSE ) {
            if($_SERVER['REMOTE_ADDR'] == "219.101.37.233" || $_SERVER['REMOTE_ADDR'] == USER_SERVER) {
            } else {
                $maite = TRUE;
            }
        }

        if ( $maite ) {
            //メンテナンス中の場合
            $resultText = '0"'.Msg::get("SYSTEM_MAINTENANCE");
        } else {
            // リクエストパラメータ取得
            $ids = $this->getRequestParam('ids');
            $idArray = array();
            if ( $ids ) {
                $idArray = array_map('intval', explode(',', $ids ));
            }
            $id = intval( $this->getRequestParam('detail') );

            $model = new ProductAjaxModel();

            if ( $idArray ) {
                // 一覧
                $data = $model->getList( $idArray );
                $resultText = $model->convertTextList( $data );
            } elseif ( $id ) {
                // 詳細
                $data = $model->getDetail( $id );
                $resultText = $model->convertTextDetail( $data );
            }
        }
        if ( !$resultText ) {
            // 結果が取れない場合は無効なリクエストとする
            $resultText = '0"'.Msg::get("INVALID_REQUEST");
        }
        return $this->createTextResult( $resultText );
    }

    /**
     * 入札を押した時の処理
     *
     * pc/product/ProductAction.class.phpの同名メソッドからリプレイス
     *
     * @return <ActionResult>
     */
    public function bid() {

        $sep = '"'; // 結果テキストの区切り文字定義
        $returnProduct = new stdClass();

        if(MAINTE_FLAG == SYS_CLOSE) {
            //メンテナンス中の場合
            if($_SERVER['REMOTE_ADDR'] == "219.101.37.233" || $_SERVER['REMOTE_ADDR'] == USER_SERVER) {

            } else {
                $returnProduct->message=Msg::get("SYSTEM_MAINTENANCE");
                $returnProduct->message = str_replace($sep, '', $returnProduct->message );
                return $this->createTextResult( $returnProduct->message );
            }
        }

        // ブラック端末チェック
        $this->result = $this->checkPhoneSerNo();
        if ( $this->result ) {
            $resultText = Msg::get("INVALID_REQUEST");
            $this->createTextResult( $resultText );
        }

        //セッションからログイン者のmemberidを取得
        $memberId=$this->getMemberIdFromSession();
        if(empty ($memberId)) {
            //セッションにユーザID存在しない場合、再ログインさせる
            return $this->createRedirectResult( Config::value("SERVER_PATH")."/login" );
        }
        
        $productId=$this->getRequestParam("id");

        //member情報を取得
        import('classes.model.MemberModel');
        $model = new MemberModel();

        // 未払い商品数チェック
        $ret=$model->getMemberunpaid($memberId);
        if($ret->f_unpain_num >= UNPAIN_NUM && UNPAIN_NUM != 0) {
            import("classes.utils.Message");
            $returnProduct->message = Msg::get("UNPAIN_FALT");
            $returnProduct->message = str_replace($sep, '', $returnProduct->message );
            return $this->createTextResult($returnProduct->message);
        }

        $member=$model->getMemberById($memberId);

        import('classes.model.ProductGeneral');
        $result = ProductGeneral::bid($this, $member, $productId);
        if ( $result instanceof RedirectResult ) {
            // すでに最終入札者
            $message = Msg::get("LASTEST_BIDDER");
            $message = str_replace( $sep, '', $message );
            return $this->createTextResult( $message );
        } elseif ($result instanceof ActionResult) {
            $message = $result->getParams();
            $message = str_replace( $sep, '', $message );
            return $this->createTextResult( $message );
        }

        //ユーザが参加しているオークション数も更新する
        import("classes.model.MyProductModel");
        $model = new MyProductModel();
        $bid_count = Utils::getInt($model->getBidProductCount($member),0);
        $result->product_bid_count = Utils::getInt($model->getProductBidCount($member->fk_member_id,$productId),0);
        $result->bid_count=$bid_count;
        //ユーザのコイン数更新する
        $result->coin = $member->f_free_coin + $member->f_coin;

        $tmp = '';
        $field_array = array(
            'discount_price',
            'discount_rate',
            'price',
            'last_bidder',
            'time',
            'product_bid_count',
            'bid_count',
            'coin',
        );
        foreach ( $field_array as $name ) {
            if ( isset( $result->$name ) ) {
                if ( $tmp ) {
                    $tmp .= $sep;
                }
                $tmp .= str_replace($sep, '', $result->$name );
            }
        }
        $result = $tmp;

        return $this->createTextResult( $result );
    }
    
    /**
     * AjaxのActionResultを作成
     *
     * @param <type> $obj
     * @param <String> $type Ajaxの通信の結果タイプ　（xml,json)
     * @return AjaxResult
     */
    public function createAjaxResult( $obj, $type='' ) {
        import(FRAMEWORK_DIR.'.result.AjaxResult');
        return new AjaxResult( $obj, $type );
    }

    /**
     * Ajaxのリクエストかどうかチェック
     * @return <boolean>
     */
    public function isAjax() {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
            if('xmlhttprequest' == strtolower($_SERVER['HTTP_X_REQUESTED_WITH']))
                return true;
        }
        return false;
    }

    /**
     *不正なアクセスかどうか、チェックする
     * @return <boolean>
     */
    public function isFailedAccess(){
        return $this->isPost() && $this->isAjax();
    }
}
?>
