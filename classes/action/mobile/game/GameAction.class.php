<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
//import('classes.model.ProductModel');
//import('classes.repository.ProductRepository');
import("classes.model.MemberModel");
import("classes.utils.Message");
/**
 * 終了商品一覧処理
 *
 * @author mw
 */
class GameAction extends MobileAction {
    private $beforeResult="";

    public function before() {
        //ユーザログイン状況確認
        if(!$this->isLogin()) {
            //ログインしていない場合
            $this->addTplParam("error", Msg::get("NOT_LOGIN"));
            $this->beforeResult=$this->createTemplateResult("login.html");
            $this->setSession("login_change",$_SERVER["REQUEST_URI"]);
        }
        else
        //ログイン情報をテンプレートに追加
            $this->checkSessionInfo();
    }

    public function execute() {

        if(!$this->isLogin()) {
            //ログインしていない場合
            return $this->beforeResult;
        }
        if(SERVICE_SLOT_OPTION==0){
            $message=Msg::get("GAME_CONTROL");
            $this->addTplParam("message", $message);
            return $this->createTemplateResult("memreg_complete.html");
        }else{
            return $this->createTemplateResult("game_slot.html");
        }
    }

}
?>
