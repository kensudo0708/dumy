<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.ProductModel');
import('classes.model.ProductGeneral');
//import('classes.repository.ProductRepository');
/**
 * 開催商品一覧の処理
 *
 * @author mw
 */
class ProductListAction extends PcAction {
    //put your code here

    /**
     *開催中商品リスト
     * @return <type>
     */
    public function execute() {
         //チェックログイン状況
        $this->checkSessionInfo();
        
        $model=new ProductModel();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getAuctionProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=PROUDUCT_OPEN_COLUMNS;
            $rows=PROUDUCT_OPEN_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/list",10);
            //開催中の商品を取得
            $products=$model->getProducts(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));
            
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $price=$obj->price;
                $obj->price=number_format($obj->price);
                //ﾃﾞｨｽｶｳﾝﾄ額
                $obj->discount_price=$obj->market_price-$price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;

            }
            $this->addTplParam("columns", $columns);
            $this->addTplParam("products", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }
        //商品のカテゴリの種類データを取得する
        ProductGeneral::addCategoriyInfo($this);

        return $this->createTemplateResult("auction_open.html");
    }
}
?>
