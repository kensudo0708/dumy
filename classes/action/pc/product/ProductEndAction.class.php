<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.ProductModel');
//import('classes.repository.ProductRepository');
/**
 * 終了商品一覧処理
 *
 * @author mw
 */
class ProductEndAction extends PcAction {
    //put your code here
    public function execute() {
        //チェックログイン状況
        $this->checkSessionInfo();
        
        $model=new ProductModel();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getEndProductCount(),0);
        
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=PROUDUCT_END_COLUMNS;
            $rows=PROUDUCT_END_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/end",10);
            //開催中の商品を取得
//            $model=new ProductModel();
            $products=$model->getEndProducts(($pageId-1) *$pageSize ,$pageSize);
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getDateTimeString($obj->time);
                $obj->price=number_format($obj->price);
                $pro_id=$obj->id;
                $endProduct=$model->getEndProductById($pro_id);
                //落札コイン数
                $endProduct->bidcoins=($endProduct->bidcoins)*($endProduct->f_spend_coin);
                //落札用コインマネ
                $endProduct->bidcoin_money=($endProduct->bidcoins)*POINT_VALUE;
                //落札商品の落札価格と落札用コインマネのプラス結果
                $obj->plus_end_price=$obj->now_price+$endProduct->bidcoin_money;
                //ﾃﾞｨｽｶｳﾝﾄ額
                $obj->discount_price=$obj->market_price-$obj->now_price;
                $obj->plus_discount_price=$obj->market_price-$obj->plus_end_price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;

                $obj->plus_discount_rate=floor(($obj->plus_discount_price/$obj->market_price) * 100);
                $obj->plus_discount_rate=$obj->plus_discount_rate<1 ? 0 :$obj->plus_discount_rate;

            }
            $this->addTplParam("end_columns", PROUDUCT_END_COLUMNS);
            $this->addTplParam("end_rows", PROUDUCT_END_ROWS);
            $this->addTplParam("products", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }

        
        return $this->createTemplateResult("auction_end.html");
    }

}
?>
