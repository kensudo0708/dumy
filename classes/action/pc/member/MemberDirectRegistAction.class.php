<?php
/**
 * ダイレクト会員登録アクション
 *
 *
 */
import('classes.model.MemberRegistManager');
if ( !class_exists('TNewsModel') ) {
    import('classes.model.TNewsModel');
}
class MemberDirectRegistAction extends BaseAction {

    /**
     * ■主処理
     */
    public function execute() {

        session_start();

        $statusCode = NULL;
        $message = NULL;

        $this->prepareCheck( $statusCode, $message );
        if ( $statusCode !== '0' ) {
            // エラー
            $responseText = $this->createResponseBody( $statusCode, $message );
            return $this->createTextResult( $responseText );
        }

        /**
         * リクエストパラメータの取得
         */
        $reqParamNameArray = array(
            'hn',
            'ad',
            'id',
            'pass',
            'mail',
            'sex',
            'birth',
            'name',
            'area',
            'work',
            'verify',
            'afc',
            'agr',
        );
        $reqParamArray = array();
        foreach ( $reqParamNameArray as $name ) {
            $reqParamArray[($name)] = $this->getRequestParam( $name );
            if ( is_null($reqParamArray[($name)]) ) {
                $reqParamArray[($name)] = ''; // 最低限空文字を設定する
            }
        }
        $this->sanitize( $reqParamArray );

        $activity = intval( constant('MEM_DIRECT_REG_ACTIVITY') );
        try {
            $mdrManager = new MemberRegistManager();

            // Validation
            $mdrManager->validate( $reqParamArray, $statusCode, $message );

            $member = NULL;

            /**
             * 以下、会員登録処理とその他の処理のトランザクションを分割しています。
             */

            // 会員登録処理
            if ( $statusCode === '0' ) {
                try {
                    DB::begin();
                    $member = $mdrManager->regist( $reqParamArray, $activity );
                    if ( $member ) {
                        $statusCode = '0';
                        if ( $activity ) {
                            $message = Msg::get("REGISTER_SUCCESS");
                        }
                    } else {
                        Logger::error( 'ダイレクト会員登録失敗。('.implode(',', $reqParamArray).')'.' '.__FILE__.'('.__LINE__.')' );
                        $statusCode = '-99';
                        $message = 'サーバエラーが発生しました。';
                    }
                    DB::commit();
                } catch ( Exception $e ) {
                    DB::rollback();
                    Logger::error( 'ダイレクト会員登録失敗。('.implode(',', $reqParamArray).')'.' '.__FILE__.'('.__LINE__.')' );
                    $statusCode = '-99';
                    $message = 'サーバエラーが発生しました。';
                }
            }
            
            // 広告カウント、友達紹介サービス処理
            if ( $statusCode === '0' ) {
                try {
                    DB::begin();
                    $mdrManager->afterProcess( $member->fk_member_id );
                    DB::commit();
                } catch ( Exception $e ) {
                    DB::rollback();
                    Logger::error( 'ダイレクト会員登録に付随する処理に失敗。('.implode(',', $reqParamArray).')'.' '.__FILE__.'('.__LINE__.')' );
                }
            }

            // 会員登録完了メールの送信

            // (開発時メモ)
            // 本当は会員登録データ更新処理に含めたかったのですが、
            // 既存のメール送信処理が独立したトランザクションとして制御されていたため外出しにしました。
            // 従って、データベース上は仮会員登録されているもののメール通知されないために、
            // 登録希望者が再トライを試みてもメールアドレス存在エラーなどで拒否される問題などがあります。
            if ( $statusCode === '0' ) {
                if ( !class_exists( 'MailReserver' ) ) {
                    import("classes.utils.MailReserver");
                }
                $judge = NULL;
                switch ( $activity ) {
                case 0:
                    $judge = $mdrManager->sendPreRegisteredMail( $member );
                    if ( $judge ) {
                        $message = Msg::get("REGISTER_TEMP_SUCCESS");
                    }
                    break;
                case 1:
                    $judge = $mdrManager->sendRegisteredMail( $member );
                    break;
                default:
                    // nop
                }
                if ( !$judge ) {
                    $statusCode = '-99';
                    $message = Msg::get("EMAIL_SEND_FAILED");
                }
            }
        } catch ( Exception $e ) {
            Logger::error( $e->getMessage() );
            $statusCode = '-99';
            $message = 'サーバエラーが発生しました。';
        }

        if ( $statusCode === '0' && intval(constant('MEM_DIRECT_REG_NEWS')) ) {
            try {
                $news = new TNewsModel();
                $row = array(
                    'category'=>'ダイレクト会員登録',
                    'body'=>'会員ID:'.$member->fk_member_id.'('.($activity ? '本登録':'仮登録').') 広告コード:'.$reqParamArray['ad'],
                );
                $news->insert( $row );
            } catch ( Exception $e ) {
                Logger::warn('ダイレクト会員登録のニュース配信に失敗しました。');
            }
        }

        $responseBody = $this->createResponseBody( $statusCode, $message );
        return $this->createTextResult( $responseBody );
    }

    /**
     * レスポンスボディを生成
     */
    protected function createResponseBody( $statusCode, $message ) {
        $search = array('[', ']');
        $replace = array('\[', '\]');
        $message = addslashes($message);
        $message = str_replace( $search, $replace, $message );
        $text = 'jark_receive(["'.$statusCode.'","'.$message.'"])';
        return $text;
    }

    /**
     * ■機能チェックなどの前処理
     * 
     * @param &$statusCode 処理結果コード
     * @param &$message    処理結果メッセージ(このメソッドではエラー時のみ設定)
     *
     * @return void
     */
    protected function prepareCheck( &$statusCode, &$message ) {
        /**
         * 本機能の有効化チェック
         */
        if ( !defined('MEM_DIRECT_REG_ENABLE') || !constant('MEM_DIRECT_REG_ENABLE') ) {
            $statusCode = '-99';
            if ( defined('MEM_DIRECT_REG_CLOSED_MSG') ) {
                $message = constant('MEM_DIRECT_REG_CLOSED_MSG');
            }
        } else {
            $statusCode = '0';
        }
        return NULL;
    }

    /**
     * サニタイジング
     * @return void
     */
    protected function sanitize( &$reqParamArray ) {
        // 下記の項目はトリミングを行う @see MemberAction.register()
        $trimFieldArray = array(
            'id',
            'pass',
            'hn',
            'mail',
            // 以下は本機能でのみ追加したもの
            'ad',
            'afc',
        );
        foreach ( $trimFieldArray as $name ) {
            if ( isset( $reqParamArray[($name)] ) ) {
                $reqParamArray[($name)] = preg_replace( '/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $reqParamArray[($name)] );
            }
        }
        $reqParamArray['sex'] = $reqParamArray['sex'] ? 1:0;
        $reqParamArray['agr'] = $reqParamArray['agr'] ? 1:0;
    }
}
?>
