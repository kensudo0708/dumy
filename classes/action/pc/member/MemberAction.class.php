<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.MemberModel');
import("classes.utils.Message");
import("classes.model.RequestConfirm");
/**
 * Description of MemberAction
 *
 * @author ark
 */
class MemberAction extends PcAction {
    //put your code here

    /**
     *
     */
    public function execute() {
    }

    /**
     * 新規登録
     */
    public function register() {
        if($this->isLogin())
        //既にログインした場合、ホームに行かせる
            $this->createRedirectResult(Config::value("SERVER_PATH"));
        $model=new MemberModel();
        $mode=$this->getRequestParam("mode");
        //年齢データを取得

        $this->addTplParam("brithday", $model->getBrithdayArray(Utils::getInt(ENTRY_AGE_LIMIT, 18)));

        //都道府県
//        if($p["pref_id"]!=0) {
//            $pre=$model->getPrefById($p["pref_id"]);
//            $p["pref_name"]=$pre->t_pref_name;
//        }
        $areas=$model->getArea();
        $jobs=$model->getJob();

        $area_ids=array();
        $area_names=array();
        $job_ids=array();
        $job_names=array();
        $a_key_value=array();
        $j_key_value=array();

          foreach($areas as $area){
            array_push($area_ids, $area->area_id);
            array_push($area_names, $area->area_name);
            $a_key_value=array_combine($area_ids,$area_names);
         }

         foreach($jobs as $job){
            array_push($job_ids, $job->job_id);
            array_push($job_names, $job->job_name);
            $j_key_value=array_combine($job_ids,$job_names);
         }

        $jobs_array=array('job_id'=>$job_ids,'job_name'=>$job_names);

        $this->addTplParam("a_key_value",$a_key_value);
        $this->addTplParam("j_key_value",$j_key_value);

        if(empty ($mode)) {
            //入力画面を表示
            $this->addTplParam("mode", "inputcheck");
            
            return $this->createTemplateResult("register.html");
        }

        if($mode=="inputcheck") {
            $p=array();
            //$p["login_id"]=trim($this->getRequestParam("login_id"));
            $p["login_id"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("login_id"));
            //$p["login_pass"]=trim($this->getRequestParam("login_pass"));
            $p["login_pass"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("login_pass"));
            //$p["handle"]=trim($this->getRequestParam("handle"));
            $p["handle"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("handle"));
            //$p["mail_address"]=trim($this->getRequestParam("mail_address"));
            $p["mail_address"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("mail_address"));
            $p["tel_no"]=trim($this->getRequestParam("tel_no"));
            $p["birth_year"]=$this->getRequestParam("birth_year");
            $p["birth_month"]=$this->getRequestParam("birth_month");
            $p["birth_day"]=$this->getRequestParam("birth_day");
            $p["sex"]=$this->getRequestParam("sex");
            $p["onamae"]=$this->getRequestParam("onamae");
            $p["post_code"]=$this->getRequestParam("post_code");
            $p["pref_id"]=$this->getRequestParam("pref_id");
            $p["address1"]=$this->getRequestParam("address1");
            $p["address2"]=$this->getRequestParam("address2");
            $p["guid"]=$this->getRequestParam("guid");
            $p["address3"]=$this->getRequestParam("address3");
            $p["mailmagazine"]=$this->getRequestParam("mailmagazine");
            $p["agreement"]=$this->getRequestParam("agreement");
            $p["verify"]=$this->getRequestParam("verify");
            $p["agreement"]=$this->getRequestParam("agreement");
            $p["verify"]=$this->getRequestParam("verify");
            $p["area"]=$this->getRequestParam("area");
            $p["job"]=$this->getRequestParam("job");
           

            $p["sex"] = isset($p["sex"]) ? ($p["sex"] == "0" ? "0" : "1") : "0";
            //他の会員メンバーに紹介された場合、friend_adcodeを取得
            $friend_adcode=$this->getRequestParam("friend");
            if(!empty ($friend_adcode))
                $this->setSession('friend_adcode', $friend_adcode);

            //入力チェック
            import("classes.utils.InputCheck");
            $message=InputCheck::registInputCheck($p);
            

            //認証コードチェック
            if(!$this->checkVerifyCode()) {
                $m=Msg::get("REGISTER_VERIFY_INVALID");
                $message =$message===TRUE ? $m : $message.$m;
            }
            if($message===TRUE) {
                //チェックを通った後、登録内容を確認させる
                $p["birthday"]=$p["birth_year"]."年".$p["birth_month"]."月".$p["birth_day"]."日";
                $p["sex_string"]=$p["sex"]=="0" ? "男性" : "女性";
                $p["mailmagazine_string"]=$p["mailmagazine"]==1 ? "Yes" : "No";

                //都道府県

                if(isset($p["area"])) {
                    $area=$model->getAreaById($p["area"]);
                    foreach($area as $area_id_name){
                         $p["area_name"]=$area_id_name->area_name;
//                         echo $p["area_name"]."area_name";
                    }
                }
                //職業
                if(isset($p["job"]) ) {
                    $job=$model->getJobById($p["job"]);
                    foreach($job as $job_id_name){
                        $p["job_name"]=$job_id_name->job_name;
//                        echo $p["job_name"]."job_name";
                    }

                }
                //会員登録する際、住所を入力しないことに変更する
                $p["fk_address_flg"]=0;

                $this->addTplParam("p", $p);
                $this->addTplParam("mode", "register");
                //入力した内容をセッションに保存
                $this->setSession("inputcontent", $p);
                return $this->createTemplateResult("register_confirm.html");
            }
            else {
                //再入力
                $this->addTplParam("message", $message);
                $this->addTplParam("p", $p);
                $this->addTplParam("mode", "inputcheck");
                return $this->createTemplateResult("register.html");
            }
        }
        else {

            $p=$this->session("inputcontent");
            if($p===false) {
                //セッション時間切れ、再入力させる
                $this->addTplParam("mode", "inputcheck");
                return $this->createTemplateResult("register.html");
            }
            //登録処理
            import(FRAMEWORK_DIR.'.utils.StringBuilder');
            //唯一のadcode作成
            $p["f_adcode"]=StringBuilder::uuid();
            $p["birthday"]=$p["birth_year"]."-".$p["birth_month"]."-".$p["birth_day"];
            //PCの場合、普通会員として登録する(仮登録）
            $p["activity"]="0";
            $p["member_group"]="1";
            //メールマガジン　　0:拒否　１：受取
            $p["mailmagazine"]=$p["mailmagazine"]==1 ? 1 : 0;
            //パスワード暗号化(仮のもの）
            $p["login_pass"]=md5($p["login_pass"]);

            //URLのパラメータを取得する
            $url_params=$this->session("URL_PARAMS");
//            //echo $url_params."url_params<br>";
            $p["aflkey"]=$this->session("aflkey");	//G.Chin 2010-07-30 add
            if(count($url_params) >= 2) {
                $adcodeArray = explode(",", Config::value("ADCODE"));
                $friendArray = explode(",", Config::value("FRIEND_CODE"));
//                var_dump($friendArray);
//                if($url_params[0]==Config::value("FRIEND_CODE")
//                        || $url_params[0]==Config::value("ADCODE")) {
                if((in_array($url_params[0],$adcodeArray) || in_array($url_params[0],$friendArray))) {
                    //友達紹介か、広告から
                    $info=$model->getMemberAdcodeInfoByCode($url_params[1]);
                    //echo $info."info<br>";
                    if(!empty ($info)) {
                        //紹介の親会員,また広告が存在する場合
                        $p["fk_parent_ad"]=$info->fk_adcode_id;
                    }
                }
            }

            if(!empty($p["fk_parent_ad"])) {
                //友達紹介,また広告から新規登録、(仮登録）
                $member=$model->saveAdcodeNewMember($p);
                //セッションから、adcodeをクリアする
                $this->removeSession("URL_PARAMS");
                $this->removeSession("aflkey");	//G.Chin 2010-07-30 add

            }
            else
            {
                $p["fk_parent_ad"]="0";
                //echo  $p["fk_parent_ad"]."fk_parent_ad=0<br>";
                $member=$model->saveNoAdcodeNewMember($p);
                //print_r($member) ;
                //print "member";
                //exit;
            }
            if($member!=FALSE) {

                //入力した内容をクリアする
                $this->removeSession("inputcontent");
                //仮登録の確認メールを送信処理
                $to=$member->f_mail_address_pc;
                $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/memreg_premail.html";
                $from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
                $params=array("member_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
                        "reg_confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/login/confirm?code=".$member->fk_adcode_id,
                        "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                        "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
//                $result = Utils::sendTemplateMail($to, REGIST_MAIL_SUBJECT, $tpl,$from,$params);
                import("classes.utils.MailReserver");
                $result = MailReserver::sendTemplateMail(MailReserver::REGIST,REGIST_MAIL_SUBJECT, $tpl, $params, $from, $to);
                if($result)
                //メールを送信成功
                    return Message::showConfirmMessage($this, Msg::get("REGISTER_TEMP_SUCCESS"));
                else {
                    //メールを送信失敗
                    $this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
                    return $this->createTemplateResult("register.html");
                }

            }
            else {
                //登録失敗
                $this->addTplParam("message", Msg::get("PROCESS_FAILED"));
                return $this->createTemplateResult("register.html");
            }
        }

    }


    /**
     *認証imageの作成
     * @return <type>
     */
    public function verify() {
        return $this->createVerifyImageResult();
    }

    /**
     * パスワードを忘れた場合
     */
    public function forget() {
        $mode=$this->getRequestParam("mode");
        if(empty ($mode)) {
            //入力フォームが表示
            $this->addTplParam("mode", "send");
            return $this->createTemplateResult("password_forget.html");
        }

        $email=$this->getRequestParam("email");
        $model=new MemberModel();
        if(empty ($email)) {
            $this->addTplParam("mode", "send");
            $this->addTplParam("message", Msg::get("REGISTER_EMAIL_EMPTY"));
            return $this->createTemplateResult("password_forget.html");
        }
        else {
            //メールの存在チェック
            $member=$model->getMemberMasterByMail($email);
            if(empty ($member)) {
                $this->addTplParam("mode", "send");
                $this->addTplParam("message", Msg::get("EMAIL_NOT_EXISTED"));
                return $this->createTemplateResult("password_forget.html");
            }
        }

        //メールが既に送信した場合、再送信しない
//        $confirmModel = new RequestConfirm();
//        $serial = $confirmModel->getRequestConfirmBySerial($member->f_adcode);
//        if(!empty($serial)) {
//            return Message::showConfirmMessage($this, Msg::get("PASSWORD_RESET_EMAIL_HAS_SENT"));
//        }

        //メール送信処理
//        $time_limit=date("Y-m-d_H:i:s");
        $to = $email;
        $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/pass_reset_body.html";
        $from = "From: ".INFO_MAIL."\nReturn-Path:".INFO_MAIL;
        $params=array("member_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,"login_id"=>$member->f_login_id,
                "reg_confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/member/pass_reset?code=".$member->f_adcode,
                "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
//        Utils::sendTemplateMail($to, PASSWORD_RESET_SUBJECT, $tpl,$from,$params);
        import("classes.utils.MailReserver");
        $result = MailReserver::sendTemplateMail(MailReserver::PASSWORD,PASSWORD_RESET_SUBJECT, $tpl, $params, $from, $to);
        if(!$result) {
            //メールを送信失敗
            $this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
            return $this->createTemplateResult("password_forget.html");
        }

        //送信時刻を記録する
        $confirm=new RequestConfirm();
        $confirm->saveNewConfirm(1, $member->f_adcode, PASSWORD_RESET_LIMIT);
        return Message::showConfirmMessage($this, Msg::get("PASSWORD_RESET_EMAIL_SEND"));
    }


    /**
     * パスワード再設定処理
     */
    public function pass_reset() {
        $model=new MemberModel();

        $mode=$this->getRequestParam("mode");
        if(empty($mode) || !$this->isPost()) {
            //リングからきた場合
            $code=$this->getRequestParam("code");
            if(empty ($code)) {
                //無効なリクエスト
                return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));
            }
            else {
                //コードの有効性と、時間制限が越えたかどうか、チェック
                $member=$model->getMemberByCode($code);
                if(empty ($member))
                //無効なリクエスト
                    return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

                $confirmModel=new RequestConfirm();
                $confirm = $confirmModel->getRequestConfirmBySerial($code);
                if(empty($confirm))
                //無効なリクエスト
                    return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

                $now=strtotime($confirm->sys_time);
                $sendTime=strtotime($confirm->f_updatetm);
//                $dif= Utils::getInt(($now- $sendTime) / 3600,0);
                if($now - $sendTime  > $confirm->f_timelimit)
                //リングの有効期限チェックする
                    return Message::showConfirmMessage($this, Msg::get("LINK_TIMEOUT"));

                //再設定フォームが表示する
                $this->addTplParam("mode", "send");
                $this->addTplParam("idcode", $code);
                return $this->createTemplateResult("password_reset.html");
            }
        }

        $code=$this->getRequestParam("idcode");
        //入力チェック
        $p["new_pass"]=$this->getRequestParam("pass");
        $p["repeat_pass"]=$this->getRequestParam("repass");

        //入力チェック
        $m=array();
        if(empty($p["new_pass"]))
            array_push($m, Msg::get("MY_NEW_PASSWORD_EMPTY"));
        if(empty($p["repeat_pass"]))
            array_push($m,Msg::get("MY_REPEAT_PASSWORD_EMPTY"));
        if(!empty($p["new_pass"]) && !empty($p["repeat_pass"]) && $p["repeat_pass"]!=$p["new_pass"])
            array_push($m,Msg::get("MY_PASSWORD_NOT_REPEAT"));

        $message="";
        if(count($m)!=0) {
            //再入力
            foreach ($m as $value)
                $message.=$value."<br>";
            $this->addTplParam("message", $message);
            $this->addTplParam("mode", "send");
            $this->addTplParam("idcode", $code);
            return $this->createTemplateResult("password_reset.html");
        }
        //パスワード更新処理
//        $code=$this->getRequestParam("idcode");
        if(empty ($code))
        //無効なリクエスト
            return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

        $member=$model->getMemberByCode($code);
        if(empty ($member))
        //無効なリクエスト
            return Message::showConfirmMessage($this, Msg::get("INVALID_REQUEST"));

        $result = $model->updatePassword($member->fk_member_id, md5($p["new_pass"]));
        if($result===false)
            return Message::showConfirmMessage($this, Msg::get("PROCESS_FAILED"));
        else {
            //リクエスト確認データを削除する
            $confirmModel=new RequestConfirm();
            $confirmModel->deleteConfirm($code);
            return Message::showConfirmMessage($this, Msg::get("PASSWORD_RESET_SUCCESS"));
        }
    }

//G.Chin AWKC-186 2011-01-19 add sta
	/**
	 * 楽天会員新規登録
	 */
	public function regrakuten() {
		$openId=$this->getRequestParam("openId");
		$mode=$this->getRequestParam("mode");
		
		//表示モード判定
		if($mode == "memregist")
		{
			$message = "";
			
			$p=array();
			$p["handle"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("handle"));
			$p["mail_address"]=preg_replace('/^[ 　 ]*(.*?)[ 　]*$/u', '$1', $this->getRequestParam("mail_address"));
			
			//▼ニックネーム
			if($p["handle"] == "")
			{
				$message .= "ニックネームが入力されていません。<br>\n";
			}
			
			//▼メールアドレス
			if($p["mail_address"] == "")
			{
				$message .= "メールアドレスが入力されていません。<br>\n";
			}
			else if(!ereg("@", $p["mail_address"]))
			{
				$message .= "メールアドレスが不正です。<br>\n";
			}
			
			if($message == "")
			{
				//唯一のadcode作成
				$p["f_adcode"]=StringBuilder::uuid();
				$p["activity"]="0";		//仮登録
				$p["member_group"]="1";	//普通
				
				$model=new MemberModel();
				
				//楽天会員：会員登録関数
				$ret = $model->RegistRakutenMember($p,$openId);
				if($ret == true)
				{                //仮登録の確認メールを送信処理
					$to=$p["mail_address"];
					$tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/memreg_rakuten.html";
					$from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
					$params=array("member_name"=>$p["handle"],"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
									"reg_confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/member/conrakuten?openId=".$openId,
									"question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
									"url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
					import("classes.utils.MailReserver");
					$result = MailReserver::sendTemplateMail(MailReserver::REGIST,REGIST_MAIL_SUBJECT, $tpl, $params, $from, $to);
					if($result)
					//メールを送信成功
						return Message::showConfirmMessage($this, Msg::get("REGISTER_TEMP_SUCCESS"));
					else {
						//メールを送信失敗
						$this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
						return $this->createTemplateResult("r_meminput.html");
					}
				}
			}
		}
		
		//入力画面を表示
		$this->addTplParam("message", $message);
		$this->addTplParam("openId", $openId);
		
		return $this->createTemplateResult("r_meminput.html");
	}

	/**
	 * 楽天会員本登録確定
	 */
	public function conrakuten() {
		$openId=$this->getRequestParam("openId");
		
		$model=new MemberModel();
		
		//楽天会員：会員本登録確定関数
		$ret = $model->updateRakutenActivity($openId);
		if($ret == true)
		{
			//登録成功
			
			//楽天会員：オープンID検索会員情報取得関数
			$member=$model->getRakutenByOpenId($openId);
			
			//メール送信処理
			import("classes.utils.MailReserver");
			MailReserver::sendRegistSuccessMail($member);
			
			//会員登録画面へ遷移
			$jump_url = SITE_URL."index.php/login?openId=$openId";
			header("Location: $jump_url");
			exit;
		}
	}
//G.Chin AWKC-186 2011-01-19 add end

    /**
     *住所のチェック処理
     * @param <type> $p
     * @return <boolean>
     */
    private function isExistAddress(&$p) {
        if(empty($p["post_code"]) && $p["pref_id"]==0 && empty($p["address1"]) && empty($p["address2"])
                && empty($p["address3"])) {
            //未入力の場合、OK
            $p["fk_address_flg"]=0;
            return true;
        }
        if(!empty($p["post_code"]) && $p["pref_id"]!=0 && !empty($p["address1"]) && !empty($p["address2"])
                && !empty($p["address3"])) {
            //すべて入力した場合も、OK
            $p["fk_address_flg"]=1;
            return true;
        }
        //以外の場合 NG
        return false;
    }
}

?>
