<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
/**
 * Description of MyProduct
 *
 * @author ark
 */
class MyProductAction extends PcAction {
    //put your code here

    private $beforeResult="";

    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        //ユーザログイン状況確認
        if(!$this->isLogin()) {
            //ログインしていない場合
            $this->addTplParam("error", Msg::get("NOT_LOGIN"));
            $this->beforeResult=$this->createTemplateResult("login.html");
            $this->setSession("login_change",$_SERVER["REQUEST_URI"]);
        }
        else
        //ログイン情報をテンプレートに追加
            $this->checkSessionInfo();

    }

    /**
     * マイの入札中商品
     */
    public function execute() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $member_id=$this->getMemberIdFromSession();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getBidProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=MY_BID_PROUDUCT_COLUMNS;
            $rows=MY_BID_PROUDUCT_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/myproduct",10);
            //マイの入札中の商品を取得
            $products=$model->getBidProduct(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));
            
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $price=$obj->price;
                $obj->price=number_format($obj->price);
                //ﾃﾞｨｽｶｳﾝﾄ額
                $obj->discount_price=$obj->market_price-$price;
                $obj->market_price=$obj->market_price==0 ? 1 :$obj->market_price;
                $obj->discount_rate=floor(($obj->discount_price/$obj->market_price) * 100);
                $obj->discount_rate=$obj->discount_rate<1 ? 0 :$obj->discount_rate;

            }
            $this->addTplParam("columns", $columns);
            $this->addTplParam("products", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }
        //my_bid_product.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "myproduct");
        $include_file=AgentInfo::getAgentDir()."/parts/my_bid_product.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *自動入札設定中のオークション
     * @return <ActionResult>
     */
    public function autobid() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $member_id=$this->getMemberIdFromSession();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getAutoBidProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=MY_AUTO_BID_PROUDUCT_COLUMNS;
            $rows=MY_AUTO_BID_PROUDUCT_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/myproduct/autobid",10);
            //自動入札商品を取得
            $products=$model->getAutoBidProduct(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));

            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->f_min_price=number_format($obj->f_min_price);
                $obj->f_max_price=number_format($obj->f_max_price);
                $obj->coins=number_format($obj->coins);
            }

            $this->addTplParam("autobids", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }
        //my_auto_bid_product.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "autobid");
        $include_file=AgentInfo::getAgentDir()."/parts/my_auto_bid_product.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *自動入札設定をキャンセル処理
     * @return <type>
     */
    public function autobid_cancel() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $auto_id=$this->getRequestParam("id");
        if(empty ($auto_id)){
            //無効なリクエストの場合
            import("classes.utils.Message");
            return Message::showErrorRequest($this);
        }
//        import("classes.model.MyProductModel");
//        $model=new MyProductModel();
//        $result = $model->autoBidCancel($auto_id);
//        if(!$result)
//            $this->addTplParam("message", Msg::get("CANNOT_CANCEL"));
        import("classes.model.ProductGeneral");
        ProductGeneral::autoBidCancel($this, $auto_id);
        return $this->createRedirectResult(Config::value("SERVER_PATH")."/myproduct/autobid");
    }

    /**
     *watchしているオークション商品
     * @return <ActionResult>
     */
    public function watch() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $member_id=$this->getMemberIdFromSession();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getWatchProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=MY_WATCH_PROUDUCT_COLUMNS;
            $rows=MY_WATCH_PROUDUCT_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/myproduct/watch",10);
            //watch商品を取得
            $products=$model->getWatchProduct(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));
            

            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("columns", $columns);
            $this->addTplParam("rows", $rows);
            $this->addTplParam("products", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }
        //my_watch_product.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "watch");
        $include_file=AgentInfo::getAgentDir()."/parts/my_watch_product.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *落札済みオークション商品
     * @return <ActionResult>
     */
    public function finish() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $member_id=$this->getMemberIdFromSession();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getEndProductCount($member_id),0);

        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=MY_FINISH_PROUDUCT_COLUMNS;
            $rows=MY_FINISH_PROUDUCT_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/myproduct/finish",10);
            //落札済み商品を取得
            $products=$model->getEndProduct(($pageId-1) *$pageSize ,$pageSize,$member_id);
            //echo $products;
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getDateTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("end_columns", PROUDUCT_END_COLUMNS);
            $this->addTplParam("end_rows", PROUDUCT_END_ROWS);
            $this->addTplParam("products", $products);
            $this->addTplParam("pageNumber", $pageNumber);
        }
        //my_finish_product.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "finish");
        $include_file=AgentInfo::getAgentDir()."/parts/my_finish_product.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *自動入札設定
     * @return <ActionResult>
     */
    public function set_auto() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        //不正なアクセスかどうか、チェックする
        /*
        if(!$this->isPost() || $this->isFailedAccess()) {
            //検証できない、無効な入札
            if($this->isAjax()) {
                //ajax通信の場合
                $returnProduct->message=Msg::get("INVALID_REQUEST");
                return $this->createAjaxResult($returnProduct, "json");
            }
            else
                return Message::showErrorRequest($this);
        }
        */
        $pro_id=$this->getRequestParam("pro_id");
        $model=new MyProductModel();
        if($model->isAutoBidProduct( $pro_id,$this->getMemberIdFromSession())){
            //既に自動入札せってい済み
            $this->setSession("message", Msg::get("AUTO_BID_SET_FINISH"));
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$pro_id);
        }
        import('classes.model.MemberModel');
        $model=new MemberModel();

        $ret=$model->getMemberunpaid($this->getMemberIdFromSession());
        if($ret->f_unpain_num >=UNPAIN_NUM && UNPAIN_NUM !=0)
        {
            $this->setSession("message", Msg::get("UNPAIN_FALT"));
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$pro_id);
            //return Message::showConfirmMessage($this,Msg::get("UNPAIN_FALT"));
        }
        $from=Utils::getInt($this->getRequestParam("price_from"),-1);
        $to=Utils::getInt($this->getRequestParam("price_to"),-1);
        $count=Utils::getInt($this->getRequestParam("bid_count"),-1);
        
//G.Chin 2010-07-14 add sta
        $smax_cnt= DB::result_rows("SELECT fk_auto_id FROM t_auto_bidder WHERE fk_member_id= ? AND fk_products_id= ? AND f_max_price= ? AND f_status= ?",
                                 array($this->getMemberIdFromSession(),$pro_id,$to,0));
//G.Chin 2010-07-14 add end
        
        $product="";
        $m=array();
        if(empty ($pro_id)) {
            array_push($m, Msg::get("INVALID_REQUEST"));
        }
        else {
            if($from==-1 || $to==-1 || $count ==-1)
                array_push($m, Msg::get("INVALID_AUTO_BID_SETTING"));
//G.Chin 2010-07-13 add sta
//G.Chin 2010-07-14 chg sta
/*
            else if($from<AUTOBID_BASE_COIN) {
                array_push($m, Msg::get("INVALID_AUTO_BID_BASECOUNT"));
                $m = str_replace("%s", AUTOBID_BASE_COIN, $m);;
*/
            else if($count<AUTOBID_BASE_COUNT) {
                array_push($m, Msg::get("INVALID_AUTO_BID_BASECOUNT"));
                $m = str_replace("%s", AUTOBID_BASE_COUNT, $m);;
//G.Chin 2010-07-14 chg end
            }
            else if($from==$to) {
                array_push($m, Msg::get("INVALID_AUTO_BID_SAME"));
            }
//G.Chin 2010-07-13 add end
//G.Chin 2010-07-14 add sta
            else if($smax_cnt>0) {
                array_push($m, Msg::get("INVALID_AUTO_BID_MAX"));
            }
//G.Chin 2010-07-14 add end
            else {
                import("classes.model.ProductModel");
                $model=new ProductModel();
                $product=$model->getProductForSetAutoBid($pro_id);
                if(is_object($product)===false)
                //商品終了、また強制終了された場合
                    array_push($m, Msg::get("INVALID_AUCTION_PRODUCT"));
                else {
                    if($product->f_auto_flag!=1)
                        array_push($m, Msg::get("AUTO_BID_NOT_ALLOW"));
                    else {
                        //商品価格が上昇か、下がるかどうか、チェックする
                        if($product->t_r_status==1 && $product->f_rev_flag==1 ) {
                            //下がる場合
                            if($from < $to)
                                array_push($m, Msg::get("INVALID_AUTO_BID_SETTING"));
                        }
                        else {
                            //上昇場合
                            if($from > $to)
                                array_push($m, Msg::get("INVALID_AUTO_BID_SETTING"));
                        }
                        //ユーザのコイン数チェック
                        import('classes.model.MemberModel');
                        $model=new MemberModel();
                        $member=$model->getMemberMasterById($this->getMemberIdFromSession());
                        $member->coin_count=Utils::getInt($member->f_coin,0) + Utils::getInt($member->f_free_coin,0);
                        if($count * $product->f_spend_coin >$member->coin_count)
                        //コイン数が足りない
                            array_push($m, Msg::get("COIN_NOT_ENOUGH"));
                    }
                }
            }
        }
        $message="";
        if(count($m)>0) {
            foreach ($m as $value)
                $message.=$value."<br>";
            $this->setSession("message", $message);
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$pro_id);
        }
        else {

            if($product->f_spend_coin >0){
                //自動入札設定する
                $auto="";
                $spendCoins=$count * $product->f_spend_coin;
                $member->f_free_coin=Utils::getInt($member->f_free_coin,0);
                $member->f_coin=Utils::getInt($member->f_coin,0);
                if($member->f_free_coin > $spendCoins) {
                    //フリーコイン足りる場合、フリーコインから消費する
                    $member->f_free_coin = $member->f_free_coin-$spendCoins;
                    //消費するコイン
                    $auto->f_free_coin = $spendCoins;
                    $auto->f_buy_coin = 0;
                }
                else {
                    //フリーコイン足りない場合、フリーコインを商品のコイン消費分の割り切れる分を減算する
                    $res = $member->f_free_coin + $member->f_coin  - $spendCoins;
                    $auto->f_free_coin = $member->f_free_coin - ($member->f_free_coin % $product->f_spend_coin);
                    $auto->f_buy_coin = $spendCoins - $auto->f_free_coin;

                    $member->f_free_coin = $member->f_free_coin % $product->f_spend_coin;
                    $member->f_coin = $res - $member->f_free_coin;

                }
            }
            else {
                if($to > $product->f_market_price )
                {
                    $this->setSession("message", Msg::get("OVER_MARKET"));
                    return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$pro_id);
                }
                $auto->f_free_coin = $count;
                $auto->f_buy_coin =0;
            }
            $auto->fk_member_id = $member->fk_member_id;
            $auto->fk_products_id = $product->fk_products_id;
            $auto->f_min_price = $from;
            $auto->f_max_price = $to;
            $auto->f_status = 0;

            //DB処理
            import("classes.model.BidModel");
            $model=new BidModel();
            if($model->setAutoBid($auto, $member,$count)===true)
                $this->setSession("message", Msg::get("AUTO_BID_SETTING_SUCCESS"));
            else
                $this->setSession("message", Msg::get("AUTO_BID_SETTING_FAILED"));
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$pro_id);
        }

    }

    
    public function auto_cancel(){
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        
    }


}
?>
