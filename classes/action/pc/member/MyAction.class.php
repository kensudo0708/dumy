<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

//G.Chin 2010-09-16 add sta
//☆★	ライブラリ読込み	★☆
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;
//G.Chin 2010-09-16 add end

import("classes.model.MemberModel");
import("classes.utils.Message");
/**
 * Description of MyAction
 *
 * @author ark
 */
class MyAction extends PcAction {
    //put your code here

    private $beforeResult="";
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        //ユーザログイン状況確認
        if(!$this->isLogin()) {
            //ログインしていない場合
            $this->addTplParam("error", Msg::get("NOT_LOGIN"));
            $this->beforeResult=$this->createTemplateResult("login.html");
            $this->setSession("login_change",$_SERVER["REQUEST_URI"]);
        }
        else
        //ログイン情報をテンプレートに追加
            $this->checkSessionInfo();
    }


    public function execute() {
        if(!$this->isLogin()) {
            //ログインしていない場合
            return $this->beforeResult;
        }
//G.Chin 2010-09-30 add st
		$member_id=$this->getMemberIdFromSession();
		
		//初回銀行振込フラグ取得関数
		GetMemberFirstBank($member_id,$tmm_f_first_bank);
		if($tmm_f_first_bank == 0)
		{
			//▼初回振込でない場合
			$affiliate_tag = "";
			$affiliate_tag2 = "";	//G.Chin 2010-11-05 add
		}
		else
		{
			//▼初回振込の場合
			//アフィリエイトタグ作成関数
			MakeAffiliateTag($member_id,1,0,$affiliate_tag);
			//アフィリエイトタグ作成関数２
			MakeAffiliateTag2($member_id,1,0,$affiliate_tag2);	//G.Chin 2010-11-05 add
			//▼初回銀行振込をクリア
			//初回銀行振込フラグ更新関数
			UpdateMemberFirstBank($member_id, 0);
		}
		$this->addTplParam("affiliate_tag", $affiliate_tag);
		$this->addTplParam("affiliate_tag2", $affiliate_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-09-30 add end
        //ページ名
        $this->addTplParam("page_name", "index");
        $include_file=AgentInfo::getAgentDir()."/parts/my_index.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *プロフィール変更画面を表示
     * @return <type>
     */
    public function profile() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        import('classes.model.MemberModel');
        $member=$this->getRequestAttribute("member");
        $birthday=explode("-", $member->f_birthday);
        $member->birth_year=$birthday[0];
        $member->birth_month=$birthday[1];
        $member->birth_day=$birthday[2];
        //ﾌﾟﾛﾌｨｰﾙ変更エラーの場合画面のハンドル名を修正前の会員のを表示
        $session_member_id=$_SESSION['member_id'];
        $model=new MemberModel();
        $session_member=$model->getMemberMasterById($session_member_id);
        $this->addTplParam("member", $member);
        //my_profile.htmlページにメンバーグループIDを追加
//        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "profile");
        //年齢データを取得
        $model=new MemberModel();
        $this->addTplParam("brithday", $model->getBrithdayArray(Utils::getInt(ENTRY_AGE_LIMIT, 18)));
        $this->addTplParam("session_member", $session_member);
        $include_file=AgentInfo::getAgentDir()."/parts/my_profile.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }
    /**
     *パスワード変更
     * @return <ActionResult>
     */
    public function password() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        if($this->isPost()) {
            //入力チェック
            $p=array();
            $p["old_pass"]=$this->getRequestParam("old_pass");
            $p["new_pass"]=$this->getRequestParam("new_pass");
            $p["repeat_pass"]=$this->getRequestParam("repeat_pass");

            //入力チェック
            import("classes.utils.InputCheck");
            $message=InputCheck::myPasswordInputCheck($p);
            if($message!==TRUE) {
                $this->addTplParam("message", $message);
            }
            else {
                //oldパスワードチェック
                $member=$this->getRequestAttribute("member");
                if(md5($p["old_pass"])!=$member->f_login_pass) {
                    $this->addTplParam("message", Msg::get("MY_PASSWORD_INVALID"));
                }
                else {
                    //更新処理
                    $model=new MemberModel();
                    $member->f_login_pass=md5($p["new_pass"]);
                    $model->updatePassword($member->fk_member_id,$member->f_login_pass);
                    return Message::showMyPageMessage($this, Msg::get("MEMBER_PASSWORD_UPDATED"));
                }
            }
        }
        //my_password.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "password");
        $include_file=AgentInfo::getAgentDir()."/parts/my_password.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     * 配信メルマガ変更画面を表示
     */
    public function mailmagazein() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        //my_mailmagazein.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);

        //ページ名
        $this->addTplParam("page_name", "mailmagazein");
        $include_file=AgentInfo::getAgentDir()."/parts/my_mailmagazein.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");

    }

    /**
     *配信メルマガ変更処理
     * @return <type>
     */
    public function update_mailmagazein() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $f_mailmagazein_pc=$this->getRequestParam("f_mailmagazein_pc");
        $f_mailmagazein_pc = $f_mailmagazein_pc==1 ? 1 : 0;
        $member->f_mailmagazein_pc=$f_mailmagazein_pc;
        $member->fk_member_id=$this->getMemberIdFromSession();
        import('classes.model.MemberModel');
        $model=new MemberModel();
        $member=$model->updateMemberMailmagazin($member);

        return Message::showMyPageMessage($this, Msg::get("UPDATE_MAIL_MAGAZIN"));
    }


    /**
     *コイン残高
     */
    public function coins() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        //my_coins.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);

        //ページ名
        $this->addTplParam("page_name", "coins");
        $include_file=AgentInfo::getAgentDir()."/parts/my_coins.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *登録済みの住所を一覧で表示する
     */
    public function address() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;


        $model=new MemberModel();
        $address=$model->getAddressByMemberId($this->getMemberIdFromSession());
        $this->addTplParam("address", $address);

        //my_address_select.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);

        //ページ名
        $this->addTplParam("page_name", "address");
        $include_file=AgentInfo::getAgentDir()."/parts/my_address_select.html";
//        $include_file=AgentInfo::getAgentDir()."/parts/my_address.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *住所変更、新規編集formを表示する
     * @return <ActionResult>
     */
    public function address_edit() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $ad_id= $this->getRequestParam("address");
        if(empty ($ad_id))
        //無効なリクエスト
            return Message::showMyPageErrorRequest($this);

        $model=new MemberModel();
        $this->addTplParam("perfs", $model->getPerfs());
        if("new" != $ad_id) {
            //修正
            $address=$model->getAddressByAddressId($ad_id);
            $this->addTplParam("address", $address);
        }

        $this->addTplParam("page_name", "address_edit");
        $include_file=AgentInfo::getAgentDir()."/parts/my_address.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *配送先変更処理
     */
    public function update_address() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $p=array();

        $p["f_post_code"]=$this->getRequestParam("f_post_code");
        $p["fk_perf_id"]=$this->getRequestParam("fk_perf_id");
        $p["f_address1"]=$this->getRequestParam("f_address1");
        $p["f_address2"]=$this->getRequestParam("f_address2");
        $p["f_address3"]=$this->getRequestParam("f_address3");
        $p["f_tel_no"]=$this->getRequestParam("f_tel_no");
        $p["f_name"]=$this->getRequestParam("f_name");

        //入力チェック
        import("classes.utils.InputCheck");
        $message=InputCheck::myAddressinputCheck($p);
        $model=new MemberModel();
        if($message===TRUE) {
            //登録処理
            $p['fk_address_id']=$this->getRequestParam("address");
            $p["member_id"]=$this->getMemberIdFromSession();
            $result = $model->saveMyAddress($p);
            if($result)
                return Message::showMyPageMessage($this, Msg::get("MEMBER_ADDRESS_SUCCESS"));
            else
                return Message::showMyPageMessage($this, Msg::get("PROCESS_FAILED"));
        }
        else {
            //再入力
            $this->addTplParam("message", $message);
            $this->addTplParam("address", $p);
            $this->addTplParam("perfs", $model->getPerfs());
        }
        //ページ名
        $this->addTplParam("page_name", "address");
        $include_file=AgentInfo::getAgentDir()."/parts/my_address.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *プロフィール変更処理
     */
    public function update_profile() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $p=array();
        $p["f_handle"]=$this->getRequestParam("f_handle");
        $p["birth_year"]=$this->getRequestParam("birth_year");
        $p["birth_month"]=$this->getRequestParam("birth_month");
        $p["birth_day"]=$this->getRequestParam("birth_day");
        $p["name"]=$this->getRequestParam("name");
        $p["tel"]=$this->getRequestParam("tel");
//kensudo add 2011-01-16(start)-------->
        $p["sex"]=$this->getRequestParam("sex");
//kensudo add 2011-01-16(end)---------->
        $member=$this->getRequestAttribute("member");
        $member_id=$member->fk_member_id;
        //ﾌﾟﾛﾌｨｰﾙ変更エラーの場合画面のハンドル名を修正前の会員のを表示
        $session_member_id=$_SESSION['member_id'];
        $model=new MemberModel();
        $session_member=$model->getMemberMasterById($session_member_id);

        //入力チェック
        import("classes.utils.InputCheck");
        $message=InputCheck::myProfileInputCheck($p,$member_id);
        if($message!==TRUE) {
            //再入力
            $member->f_handle=$p["f_handle"];
            $member->birth_year=$p["birth_year"];
            $member->birth_month=$p["birth_month"];
            $member->birth_day=$p["birth_day"];
            $member->f_name=$p["name"];
            $member->f_tel_no=$p["tel"];
//kensudo add 2011-01-16(start)-------->
            $member->f_sex = $p["sex"] == "0" ? "0" : "1";
//kensudo add 2011-01-16(end)---------->
            $this->addTplParam("member", $member);
            $this->addTplParam("message", $message);
            //年齢データを取得
            $model=new MemberModel();
            $this->addTplParam("brithday", $model->getBrithdayArray(Utils::getInt(ENTRY_AGE_LIMIT, 18)));

        }
        else {
            //更新処理
            $member->fk_member_id=$this->getMemberIdFromSession();
            $member->f_handle=$p["f_handle"];
            $member->birthday=$p["birth_year"]."-".$p["birth_month"]."-".$p["birth_day"];
            $member->f_name=$p["name"];
            $member->f_tel_no=$p["tel"];
//kensudo add 2011-01-16(start)-------->
            $member->f_sex = $p["sex"] == "0" ? "0" : "1";
//kensudo add 2011-01-16(end)----------> 
            $model=new MemberModel();
            $model->updateProfile($member);
            $_SESSION['member_handle']=$member->f_handle;

            return Message::showMyPageMessage($this, Msg::get("MEMBER_PROFILE_UPDATED"));
        }

        //ページ名
        $this->addTplParam("session_member", $session_member);
        $this->addTplParam("page_name", "profile");
        $include_file=AgentInfo::getAgentDir()."/parts/my_profile.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");

    }

    /**
     *友達紹介ページ表示
     * @return <type>
     */
    public function friend() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        //my_friend.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "friend");
        $include_file=AgentInfo::getAgentDir()."/parts/my_friend.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     * 友達にメール送信する
     */
    public function friend_send() {
        $p=array();
        $p["friend_name"]=$this->getRequestParam("friend_name");
        $p["friend_email"]=$this->getRequestParam("friend_email");

        //入力チェック
        import("classes.utils.InputCheck");
        $message=InputCheck::myFriendInputCheck($p);
        if($message!==TRUE) {
            //再入力
            $this->addTplParam("friend_name", $p["friend_name"]);
            $this->addTplParam("friend_email", $p["friend_email"]);
            $this->addTplParam("message", $message);
        }
        else {
            //メール送信

            import("classes.model.MemberModel");
            $model=new MemberModel();
            $member_id=$this->getMemberIdFromSession();
            $member=$model->getMemberAdcodeInfo($member_id);

            $to=$p["friend_email"];
            $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/my_friend_mail.html";
            $from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
            $subject=sprintf(FRIEND_MAIL_SUBJECT,$member->f_handle);
            $params=array("name"=>$p["friend_name"],"my_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
                    "regist_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/member/register/".Config::value("FRIEND_CODE")."/".$member->f_adcode,
                    "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                    "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/".Config::value("FRIEND_CODE")."/".$member->f_adcode);
//            Utils::sendTemplateMail($to, $subject, $tpl,$from,$params);

            import("classes.utils.MailReserver");
            $result = MailReserver::sendTemplateMail(MailReserver::FRIEND,$subject, $tpl, $params, $from, $to);
            if($result)
            //メールを送信成功
                return Message::showMyPageMessage($this, sprintf(Msg::get("MEMBER_FRIEND_SENT_FINISH"), $p["friend_email"]));
            else {
                //メールを送信失敗
                $this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
            }
        }
        //ページ名
        $this->addTplParam("page_name", "friend");
        $include_file=AgentInfo::getAgentDir()."/parts/my_friend.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *メールアドレス変更フォーム表示
     * @return <type>
     */
    public function email() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        //my_change_email.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "email");
        $include_file=AgentInfo::getAgentDir()."/parts/my_change_email.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    public function email_update() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $email=$this->getRequestParam("email");
        //入力チェック
        import("classes.utils.InputCheck");
        $message=InputCheck::myEmailInputCheck($email);
        if($message!==TRUE) {
            //再入力
            $this->addTplParam("email",$email);
            $this->addTplParam("message", $message);
        }
        else {
            //メールの重複チェック
            import("classes.model.MemberModel");
            $model=new MemberModel();
            if($model->isExistedEmail($email)>0) {
                //再入力
                $this->addTplParam("email",$email);
                $this->addTplParam("message", Msg::get("EMAIL_EXISTED"));
            }
            else {
                //メール送信
                $member_id=$this->getMemberIdFromSession();
                $member=$model->getMemberAdcodeInfo($member_id);
                $to=$email;
                $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/mail_change_body.html";
                $from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
                $subject=EMAIL_CHANGE_SUBJECT;
                $params=array("member_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
                        "login_id"=>$member->f_login_id,
                        "confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/my/email_change?code=$member->f_adcode&email=".$email,
                        "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
                        "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH"));
//                Utils::sendTemplateMail($to, $subject, $tpl,$from,$params);
                import("classes.utils.MailReserver");
                $result = MailReserver::sendTemplateMail(MailReserver::EMAIL,$subject, $tpl, $params, $from, $to);
                if($result)
                //メールを送信成功
                    return Message::showMyPageMessage($this, Msg::get("MEMBER_EMAIL_CHANGE_MAIL"));
                else
                //メールを送信失敗
                    $this->addTplParam("message", Msg::get("EMAIL_SEND_FAILED"));
            }
        }
        //ページ名
        $this->addTplParam("page_name", "email");
        $include_file=AgentInfo::getAgentDir()."/parts/my_change_email.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     * メール変更処理(未使用)
     */
    public function email_change() {
        $email=$this->getRequestParam("email");
        $code=$this->getRequestParam("code");

        $model=new MemberModel();
        if($model->isExistedEmail($email)>0) {
            //再入力
            $this->addTplParam("my_message", Msg::get("EMAIL_EXISTED"));
        }
        else {
            $member=$model->getMemberByCode($code);
            if(!empty ($member)) {
                $member->f_mail_address_pc=$email;
                $model->updateMemberEmail($member);
                return Message::showMyPageMessage($this, Msg::get("MEMBER_EMAIL_SUCCESS"));
            }
        }

        //ページ名
        $this->addTplParam("page_name", "email");
        $include_file=AgentInfo::getAgentDir()."/parts/my_change_email.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }


    /**
     *退会フォーム表示
     * @return <type>
     */
    public function close() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;
        //my_withdraw.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "close");
        $include_file=AgentInfo::getAgentDir()."/parts/my_withdraw.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *退会処理
     * @return <type>
     */
    public function member_close() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        $agree=$this->getRequestParam("agree");

        if($agree!=1) {
            //再入力
            $this->addTplParam("message", Msg::get("CLOSE_AGREEMENT_NO"));
        }
        else {
            $model=new MemberModel();
            $model->closeMemberById($this->getMemberIdFromSession());
            //セッションクリア
            $this->clearSessionInfo();
            return Message::showMyPageMessage($this, Msg::get("MEMBER_CLOSE_MESSAGE"));
        }


        //ページ名
        $this->addTplParam("page_name", "close");
        $include_file=AgentInfo::getAgentDir()."/parts/my_withdraw.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");
    }

    /**
     *入札中商品を表示するMy_indexページ
     * @return <type>
     */
    public function index_ext() {
        //ユーザログイン状況確認
        if(!empty($this->beforeResult))
            return $this->beforeResult;

        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $member_id=$this->getMemberIdFromSession();
        //マイの入札中商品 レコード数を取得
        $recordCount=Utils::getInt($model->getBidProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("bid_page"),1);
            //１ページの商品数
            $columns=MY_BID_PROUDUCT_COLUMNS;
            $rows=MY_BID_PROUDUCT_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbersI($pageId,$pageCount,Config::value("SERVER_PATH")."/my/index_ext",10,"","bid_page");
            //マイの入札中の商品を取得
            $products=$model->getBidProduct(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));

            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("columns", $columns);
            $this->addTplParam("bid_products_count", $recordCount);
            $this->addTplParam("bid_products", $products);
            $this->addTplParam("bid_pageNumber", $pageNumber);
        }
        $member_id=$this->getMemberIdFromSession();
        //レコード数を取得
        $recordCount=Utils::getInt($model->getEndProductCount($member_id),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("end_page"),1);
            //１ページの商品数
            $columns=MY_FINISH_PROUDUCT_COLUMNS;
            $rows=MY_FINISH_PROUDUCT_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbersI($pageId,$pageCount,Config::value("SERVER_PATH")."/my/index_ext",10,"","end_page");
            //落札済み商品を取得
            $products=$model->getEndProduct(($pageId-1) *$pageSize ,$pageSize,$member_id);
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getDateTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("end_products_count", $recordCount);
            $this->addTplParam("end_products", $products);
            $this->addTplParam("end_pageNumber", $pageNumber);
        }
//G.Chin 2010-09-16 add sta
		//初回銀行振込フラグ取得関数
		GetMemberFirstBank($member_id,$tmm_f_first_bank);
		if($tmm_f_first_bank == 0)
		{
			//▼初回振込でない場合
			$affiliate_tag = "";
			$affiliate_tag2 = "";	//G.Chin 2010-11-05 add
		}
		else
		{
			//▼初回振込の場合
			//アフィリエイトタグ作成関数
			MakeAffiliateTag($member_id,1,0,$affiliate_tag);
			//アフィリエイトタグ作成関数２
			MakeAffiliateTag2($member_id,1,0,$affiliate_tag2);	//G.Chin 2010-11-05 add
			//▼初回銀行振込をクリア
			//初回銀行振込フラグ更新関数
			UpdateMemberFirstBank($member_id, 0);
		}
		$this->addTplParam("affiliate_tag", $affiliate_tag);
		$this->addTplParam("affiliate_tag2", $affiliate_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-09-16 add end
        //my_index_ext.htmlページにメンバーグループIDを追加
        $member = $this->getRequestAttribute("member");
        $member_group_id = (!empty ($member->fk_member_group_id)?$member->fk_member_group_id:'-1');
        $this->addTplParam("member_group_id", $member_group_id);
        //ページ名
        $this->addTplParam("page_name", "my_index_ext");
        $include_file=AgentInfo::getAgentDir()."/parts/my_index_ext.html";
        $this->addTplParam("include_file", $include_file);
        return $this->createTemplateResult("my.html");


    }
}
?>
