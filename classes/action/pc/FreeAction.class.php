<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of FreeAction
 *
 * @author ark
 */
class FreeAction extends PcAction {
    //put your code here
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        //ユーザログイン状況確認
        $this->checkSessionInfo();
    }


    /**
     *ゼロオクって何？
     * @return <ActionResult>
     */
    public function execute() {
		//広告・紹介コード取得
		$adcode=$this->getRequestParam("a");
		$fcode=$this->getRequestParam("f");
		
		//広告コードが存在する場合
		if($adcode != "")
		{
			$_SESSION['URL_PARAMS'][0] = "a";
			$_SESSION['URL_PARAMS'][1] = $adcode;
			
            import('classes.model.MemberModel');
            $model = new MemberModel();
            $ad = $model->getAdcodeMasterId($adcode);
            if(!empty($ad))
            {
                $tmp_adcode=$adcode;
				$aflkey=$this->getRequestParam("aflkey");
				$_SESSION["aflkey"]=$aflkey;
				import('classes.model.Total');
				Total::access($ad->fk_admaster_id);
            }
		}
		//紹介コードが存在する場合
		else if($fcode != "")
		{
			$_SESSION['URL_PARAMS'][0] = "f";
			$_SESSION['URL_PARAMS'][1] = $fcode;
			
            import('classes.model.MemberModel');
            $model = new MemberModel();
            $ad = $model->getAdcodeMasterId($fcode);
            if(!empty($ad))
            {
				//友達紹介
				import('classes.model.Total');
				Total::access($ad->fk_admaster_id);
            }
			else
            {
				//友達紹介
				import('classes.model.Total');
				Total::access("0");
            }
		}
		else
		{
             import('classes.model.MemberModel');
             $model = new MemberModel();
             $ad = $model->getAdcodeMasterId("0");
             import('classes.model.Total');
             Total::access($ad->fk_admaster_id);
		}
		
		
        $num=$this->getRequestParam("p");
		$page = "free".$num.".html";
        return $this->createTemplateResult($page);
    }

}
?>
