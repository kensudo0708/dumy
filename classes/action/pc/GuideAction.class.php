<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of GuideAction
 *
 * @author ark
 */
class GuideAction extends PcAction {
    //put your code here
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        //ユーザログイン状況確認
        $this->checkSessionInfo();
    }


    /**
     *ゼロオクって何？
     * @return <ActionResult>
     */
    public function execute() {
        return $this->createTemplateResult("guide.html");
    }

    /**
     *誓約
     * @return <ActionResult>
     */
    public function commitment() {
        return $this->createTemplateResult("commitment.html");
    }

    /**
     *運営会社
     * @return <ActionResult>
     */
    public function company() {
        return $this->createTemplateResult("company.html");
    }

    /**
     *プライバシーポリシー
     * @return <ActionResult>
     */
    public function privacy() {
        return $this->createTemplateResult("privacy.html");
    }

    /**
     *利用規約
     * @return <ActionResult>
     */
    public function terms() {
        $this->addTplParam("PRODUCT_PAYLIMIT_DAY", PRODUCT_PAYLIMIT_DAY);
        return $this->createTemplateResult("terms.html");
    }

    /**
     *お問い合わせ
     * @return <ActionResult>
     */
    public function contact() {
        import('classes.model.RequestConfirm');
        $question=new RequestConfirm();
        $category=$question->getQuestionCategory();
//        $tpl=Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/inquiry_body.html";
//        $body=Utils::loadTemplateContent($tpl);
//        $body=str_replace("\n", "%0D%0A",$body);
        $this->addTplParam('category', $category);
        if($this->isLogin()) {
            $member = $this->getRequestAttribute("member");
            $member->f_mail_address_pc=empty($member->f_mail_address_pc) ? $member->f_mail_address_mb
                    : $member->f_mail_address_pc;
            $this->addTplParam("member", $member);
        }
//        $this->addTplParam('url', MAIL_ADDRESS);
//        $this->addTplParam('body', $body);
        return $this->createTemplateResult("contact.html");
    }

    //問い合わせ内容登録
    public function contact_insert() {
        import('classes.model.RequestConfirm');
        $question=new RequestConfirm();
        $category=$question->getQuestionCategory();

        $p=array();
        $p["name"]=$this->getRequestParam("name");
        $p["mem_id"]=$this->getRequestParam("mem_id");
        $p["mail"]=$this->getRequestParam("mail");
        $p["title"]=$this->getRequestParam("title");
        $p["category"]=$this->getRequestParam("category");
        $p["main"]=$this->getRequestParam("main");

        //入力チェック
        import("classes.utils.InputCheck");
        $message=InputCheck::contactInputCheck($p);

        if($message===true) {
            $question->setQuestion($p["category"],$p["mem_id"],$p["name"], $p["mail"],$p["title"],$p["main"]);
            $message=Msg::get("INFORMATION_FINISH");
            if(INFO_SEND==1) {
                //$to=INFO_MAIL;
                //$from = "From: ".$p["mail"]."\nReturn-Path:".$p["mail"];
                //$subject=$p["title"];
                //Utils::sendMail($to, $subject, $p["main"],$from);
                $question->setQuestionDB($p["mail"],$p["title"],$p["main"]);
            }
        }
        else {
            if($this->isLogin()) {
                $member = $this->getRequestAttribute("member");
                $member->f_mail_address_pc=empty($member->f_mail_address_pc) ? $member->f_mail_address_mb
                        : $member->f_mail_address_pc;
                $this->addTplParam("member", $member);
            }
            $this->addTplParam('p', $p);
        }
        $this->addTplParam('message', $message);
        $this->addTplParam('category', $category);
//        $this->addTplParam('url', MAIL_ADDRESS);
//        $this->addTplParam('body', $body);
        return $this->createTemplateResult("contact.html");
    }

    /**
     *初心者ガイド
     * @return <ActionResult>
     */
    public function how() {
        return $this->createTemplateResult("how.html");
    }

    /**
     *上級者ガイド
     * @return <ActionResult>
     */
    public function about_auction() {
        return $this->createTemplateResult("about_auction.html");
    }

    /**
     *自動落札とは？
     * @return <ActionResult>
     */
    public function auto_bid() {
        return $this->createTemplateResult("auto_bid.html");
    }

    /**
     *よくある質問
     * @return <ActionResult>
     */
    public function question() {
        return $this->createTemplateResult("question.html");
    }

    /**
     *ヘルプ
     * @return <ActionResult>
     */
    public function help() {
        return $this->createTemplateResult("help.html");
    }

    /**
     *サイトマップ
     * @return <ActionResult>
     */
    public function sitemap() {
        return $this->createTemplateResult("sitemap.html");
    }

    /**
     *落札者の声
     * @return <ActionResult>
     */
    public function winners_voice() {
        return $this->createTemplateResult("winners_voice.html");
    }

    /**
     *フリーページ指定メンバ(hyde)
     * @return <ActionResult>
     */
    public function free() {
        $page=$this->getRequestParam("p");
        return $this->createTemplateResult($page.".html");
    }

/*
    public function member_coin_add(){
        try {
            DB::begin();
            $members=DB::getObjectByQuery("SELECT * from t_member where fk_member_id>10000");

            foreach ($members as $m) {
                //t_memberのコイン数を更新
                DB::executeNonQuery("UPDATE t_member set f_free_coin=f_free_coin+20 where fk_member_id=?",
                        array($m->fk_member_id));
                $coin_old=$m->f_free_coin+$m->f_coin;
                $coins = $m->f_free_coin+$m->f_coin+20;
                //t_coin_logに記録を追加
                DB::executeNonQuery("INSERT INTO t_pay_log(f_status,f_member_id,f_free_coin_add,f_coin_result,f_tm_stamp) values
                        (2,?,20, ? ,CURRENT_TIMESTAMP)",
                        array($m->fk_member_id,$coins));
                Logger::debug("会員ID:".$m->fk_member_id.",".$coin_old."+20=".$coins."コイン");
                //usleep(100000);
            }
           
            DB::commit();
            return $this->createTextResult("OK");
        }
        catch (Exception $e) {
            DB::rollback();
            Logger::debug("20コイン追加処理が失敗しました。");
            return $this->createTextResult("NG");
        }
        
    }
 
 */
}
?>
