<?php

import('classes.model.ProductModel');
import('classes.model.ProductGeneral');
import('classes.model.ShiharaiModel');
/**
 * Description of IndexAction
 *
 * @author mw
 */

//☆★	ライブラリ読込み	★☆
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;

class ThanksAction extends PcAction {

    /**
     *ヤスオクのホームページ
     * @return <ActionResult>
     */
    public function execute() {
        //チェックログイン状況
        $this->checkSessionInfo();


        //支払い済みでt_pay_logを更新　更新　０完了　１認証中　２エラ　３支払い済み

        $reserve_id=$this->getRequestParam("reserve_id");
        $ShiharaiModel=new ShiharaiModel();
        $UpdatePayLog=$ShiharaiModel->UpdatePayLog($reserve_id);

        //開催中商品のレコード数を取得
        $model=new ProductModel();
        $recordCount=Utils::getInt($model->getAuctionProductCount($this->getRequestAttribute("member")),0);
        if($recordCount>0) {
            //ページIDを取得
            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
            $columns=INDEX_OPEN_COLUMNS;
            $rows=INDEX_OPEN_ROWS;
            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH"),10);
            import("classes.utils.Page");
            $pageNumber1=Page::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH"),10);
            //開催中の商品を取得
            $products=$model->getProducts(($pageId-1) *$pageSize ,$pageSize,$this->getRequestAttribute("member"));

            if(is_array($products)) {
                foreach ($products as $obj) {
                    $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                    $obj->time=Utils::getTimeString($obj->time);
                    $obj->price=number_format($obj->price);
                }

                $this->addTplParam("index_open_columns", $columns);
                $this->addTplParam("products", $products);
                $this->addTplParam("pageNumber", $pageNumber);
                $this->addTplParam("pageNumber1", $pageNumber1);
            }
        }
        //注目商品データを取得
        $index_notice_columns=Utils::getInt(INDEX_NOTICE_COLUMNS,0);
        $index_notice_rows=Utils::getInt(INDEX_NOTICE_ROWS,0);
        $noticeProducts=$model->getNoticeProduct($index_notice_columns*$index_notice_rows,$this->getRequestAttribute("member"));

        if(is_array($noticeProducts)) {
            foreach ($noticeProducts as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("index_notice_columns", $index_notice_columns);
            $this->addTplParam("index_notice_rows", $index_notice_rows);
            $this->addTplParam("notices", $noticeProducts);
        }
        //終了商品データを取得
        $index_end_columns=Utils::getInt(INDEX_END_COLUMNS,0);
        $index_end_rows=Utils::getInt(INDEX_END_ROWS,0);
        $endProducts=$model->getEndProductsIndex($index_end_columns*$index_end_rows);
        if(is_array($endProducts)) {
            foreach ($endProducts as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getDateTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("end_columns", $index_end_columns);
            $this->addTplParam("end_rows", $index_end_rows);
            $this->addTplParam("ends", $endProducts);
        }
        //商品のカテゴリの種類データを取得する
        ProductGeneral::addCategoriyInfo($this);

        //ピックアップ商品を取得する

        $index_pickup_columns=Utils::getInt(INDEX_PICKUP_COLUMNS,0);
        $index_pickup_rows=Utils::getInt(INDEX_PICKUP_ROWS,0);
        $picks=$model->getPickupProducts($index_pickup_columns * $index_pickup_rows);
        if(is_array($picks)) {
            foreach ($picks as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".PRODUCT_LITTLE_PREFIX.$obj->image;
                $obj->time=Utils::getDateTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            $this->addTplParam("index_pickup_columns", $index_pickup_columns);
            $this->addTplParam("picks", $picks);
        }

//G.Chin 2010-06-18 del sta
        /*
        //URLについていたパラメータがあったら、
        $url_params=Config::value("URL_PARAMS");
        if(count($url_params)>0) {
            //URL_PARAMSを記録する(広告コートなど)
            if(!isset($_SESSION['URL_PARAMS']) && count($url_params) >= 2 && $url_params[0]==Config::value("ADCODE")) {
                //広告主の場合
                import('classes.model.MemberModel');
                $model = new MemberModel();
                $ad = $model->getAdcodeMasterId($url_params[1]);
                if(!empty ($ad)) {
                    import('classes.model.Total');
                    Total::access($ad->fk_admaster_id);
                }
            }
            $this->setSession("URL_PARAMS",  Config::value("URL_PARAMS"));
        }
        */
//G.Chin 2010-06-18 del end

//G.Chin 2010-06-25 add sta
//G.Chin 2010-07-23 del sta
/*
        $so = $this->getMemberIdFromSession();

        //会員親紹介コード取得関数
        GetTMemberFKParentAd($so,$tmm_fk_parent_ad);
        //アフィリエイト広告情報取得関数
        GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
        if(($taf_f_status == 1) && ($taf_f_first_buy == 1) && ($taf_f_type == 0)) {
            //▼状態が"有効"で、初回コイン購入報酬が"有り"で、種類が"A8"の場合
            //アフィリエイト用支払履歴情報取得関数
            GetAffiliateTPayLogInfo($so,$recent_money,$post_money,$all_count);
            if(($post_money == 0) && ($recent_money >= $taf_f_first_buy_price)) {
                //▼初回購入(累計0円)で、今回決済金額が基準金額以上の場合
                $pid = $taf_fk_program_id;
                //▼SIパラメータの有無を判定
                if($taf_f_param2 == "") {
                    $affiliate_tag = "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=1.1.1.coin' width='1' height='1'><br>";
                }
                else {
                    $si = $taf_f_param2;
                    $affiliate_tag = "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=$si' width='1' height='1'><br>";
                }
            }
            else {
                $affiliate_tag = "";
            }
        }
        else {
            //▼状態が"無効"、もしくは種類が"A8"でない場合
            $affiliate_tag = "";
        }
//G.Chin 2010-06-25 add end
        //メッセージ画面にチェンジする

        $this->addTplParam("affiliate_tag", $affiliate_tag);
*/
//G.Chin 2010-07-23 del end
//G.Chin 2010-06-25 add end
		
//G.Chin 2010-08-12 add sta
//G.Chin 2010-08-13 chg sta
//		//支払端末取得関数
//		GetFPayPos($reserve_id,$tpl_f_pay_pos);
//G.Chin 2010-09-24 del sta
/*
		//▼予約IDを判定
		if($reserve_id == "")
		{
			$tpl_f_pay_pos = 2;
		}
		else
		{
			//支払端末取得関数
			GetFPayPos($reserve_id,$tpl_f_pay_pos);
		}
*/
//G.Chin 2010-09-24 del end
//G.Chin 2010-08-13 chg end
		$member_id = $this->getMemberIdFromSession();
//G.Chin 2010-09-24 add sta
		//▼予約IDを判定
		if($reserve_id == "")
		{
			//会員指定最新支払履歴情報取得関数
			SrcMemberGetRecentPayLogInfo($member_id,$recent_id,$recent_money,$recent_pay_pos);
			//▼予約IDを判定
			if($recent_id == "")
			{
				$tpl_f_pay_pos = 2;
			}
			else
			{
				$tpl_f_pay_pos = $recent_pay_pos;
			}
		}
		else
		{
			//支払端末取得関数
			GetFPayPos($reserve_id,$tpl_f_pay_pos);
		}
//G.Chin 2010-09-24 add end
		//アフィリエイトタグ作成関数
		MakeAffiliateTag($member_id,1,$tpl_f_pay_pos,$affiliate_tag);
		//メッセージ画面にチェンジする
		$this->addTplParam("affiliate_tag", $affiliate_tag);
//G.Chin 2010-08-12 add end
		
//G.Chin 2010-11-05 add sta
		//アフィリエイトタグ作成関数２
		MakeAffiliateTag2($member_id,1,$tpl_f_pay_pos,$affiliate_tag2);
		//メッセージ画面にチェンジする
		$this->addTplParam("affiliate_tag2", $affiliate_tag2);
//G.Chin 2010-11-05 add end
		
       return $this->createTemplateResult("thanks.html");
    }


}

?>