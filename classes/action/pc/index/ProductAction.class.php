<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.ProductModel');
import('classes.utils.Message');
import('classes.utils.ProductFilter');

//import('classes.repository.ProductRepository');
/**
 * 商品詳細画面の処理
 *
 * @author mw
 */
class ProductAction extends MobileAction {
//put your code here

    private $result=null;
    /**
     * マッピングしたメソッドを実行する前に、実行する処理
     */
    public function before() {
        $this->result = $this->checkPhoneSerNo();
        //ユーザログイン状況確認
        $this->checkSessionInfo();
    }


    /**
     *開催商品の詳細情報を表示
     * @return <type>
     */
    public function execute() {
        if(!empty ($this->result))
            return $this->result;

        //商品ID
        $model=new ProductModel();
        $product_id=$this->getRequestParam("pro_id");
        $p=$model->getDetailProductById($product_id);
        if(!empty ($p)) {
            if($p->f_status == 2)
            //終了商品の場合
                return $this->end();
            elseif($p->f_status != 1 && $p->f_status !=3 && $p->f_status !=4 && $p->f_status !=5)
            //無効なリクエストの場合
                return Message::showErrorRequest($this);
        }
        else
        //無効なリクエストの場合
            return Message::showErrorRequest($this);

        //開催商品の詳細情報を取得する
        $product=$model->getAuctionProductById($product_id);
        if($this->isLogin()) {
            $member=$this->getRequestAttribute("member");
            //初心者の場合、該当商品が初心者フィルタにかけられたかどうか、チェックする
            if(ProductFilter::matchFirstFilter($product, $member))
            //無効なリクエストの場合
                return Message::showErrorRequest($this);
        }
        $product=Utils::objectMerge($product, $p);
        //最後の入札者のハンドル名
        $product->f_last_bidder_handle= empty($product->f_last_bidder) ? Msg::get("NOT_BIDDER")
                : $product->f_last_bidder_handle;
        //ﾃﾞｨｽｶｳﾝﾄ額
        $product->discount_price=$product->f_market_price-$product->f_now_price;
//        $product->f_market_price=$product->f_market_price == 0 ? 1 : $product->f_market_price;
        if($product->f_market_price == 0) {
            $product->discount_rate=0;
        }
        elseif($product->f_market_price > 0) {
            $product->discount_rate=floor(($product->discount_price / $product->f_market_price) * 100);
        }

        $product->f_auction_time=Utils::getTimeString($product->f_auction_time);
        $product->f_now_price=number_format($product->f_now_price);
        $product->discount_price=number_format($product->discount_price);
        $product->discount_rate=number_format($product->discount_rate);
        $product->f_market_price=number_format($product->f_market_price);
        $comment = $model->getBidderComment($product->fk_products_template_id);
        $comment->f_memo = empty($comment->f_memo)?Msg::get("NO_COMMENT"):$comment->f_memo;
        //$product->f_market_price=number_format($product->f_market_price);
        //画像処理
        $photoes=array("f_photo1mb","f_photo2mb","f_photo3mb","f_photo4mb");
        $photoes_pc=array("f_photo1","f_photo2","f_photo3","f_photo4");
        $imgs=array();
        for($i=0;$i<count($photoes);$i++) {
            if(!empty ($product->$photoes[$i])) {
                array_push($imgs, $product->$photoes_pc[$i]);
                $product->$photoes[$i] =
                        Config::value("PRODUCT_IMAGE_PATH")."/mb/".$product->$photoes[$i];
            }
        }
        if(count($imgs)>1)
            $this->addTplParam("imgs", $imgs);

        //入札ログ
        import('classes.model.BidModel');
        $model=new BidModel();
        $logs=$model->getProductBidLog($product_id,Utils::getInt(MOBILE_PRODUCT_BIDLOG_QTY,0));

        if($this->getMemberIdFromSession()!==FALSE) {
            //会員のbidCount
            $bidCount=$model->getMemberBidCount($product_id,$this->getMemberIdFromSession());
            $product->bid_count=number_format($bidCount);
        }
        $this->addTplParam("product", $product);
        $this->addTplParam("logs", $logs);
        $this->addTplParam("comment",$comment);
        if($this->session("message")!==False) {
            $this->addTplParam("message", $this->session("message"));
            //セッションに保存されたメッセージをクリアする
            $this->removeSession("message");
        }
        import("classes.model.ProductGeneral");
        if($this->isLogin()) {
            //該当商品が既にウォッチリストに追加されたかどうか
            import('classes.model.MyProductModel');
            $model=new MyProductModel();
            $isWatched = $model->isExistWatchList($this->getMemberIdFromSession(), $product_id);
            $this->addTplParam("isWatched", $isWatched);

            //この商品がすでに自動入札設定済みかどうか、チェックする
            if($model->isAutoBidProduct( $product_id,$this->getMemberIdFromSession()))
                $this->addTplParam("auto_bid_message", Msg::get("AUTO_BID_SETTING"));
            //会員の自動入札設定情報
            if(defined("OPTION_AUTO_BID_INFO") && OPTION_AUTO_BID_INFO==1) {
                $this->addTplParam("option_auto_bid_info", true);
                ProductGeneral::addAutoBidSettingInfo($this, $this->getMemberIdFromSession(), $product_id);
            }
        }
        //商品のカテゴリの種類データを取得する
        ProductGeneral::addCategoriyInfo($this);
        ProductGeneral::getPrevSameProduct($this, $product_id);

        return $this->createTemplateResult("product_open.html");

    }

    /**
     *商品の画像を表示する
     * @return <type>
     */
    public function image() {
        if(!empty ($this->result))
            return $this->result;
        $product_id=$this->getRequestParam("pro_id");
        $img=$this->getRequestParam("img");

        if(!empty ($product_id) || !empty ($img)) {
            $img=Config::value("PRODUCT_IMAGE_PATH")."/".$img;
            $this->addTplParam("img", $img);
            $this->addTplParam("product_id", $product_id);
            $this->addTplParam("width", PRODUCT_IMAGE_WIDTH_PC);
            $this->addTplParam("height", PRODUCT_IMAGE_HEIGHT_PC);
            return $this->createTemplateResult("product_image.html");
        }
        else
        //無効なリクエストの場合
            return Message::showErrorRequest($this);
    }


    /**
     *終了商品の詳細情報を表示
     * @return <type>
     */
    public function end() {
        if(!empty ($this->result))
            return $this->result;
        //商品ID
        $model=new ProductModel();
        $product_id=$this->getRequestParam("pro_id");
        $product=$model->getEndProductById($product_id);
        if(empty ($product))
        //無効なリクエストの場合
            return Message::showErrorRequest($this);

        //画像処理
        $photoes=array("f_photo1mb","f_photo2mb","f_photo3mb","f_photo4mb");
        $photoes_pc=array("f_photo1","f_photo2","f_photo3","f_photo4");
        $imgs=array();
        for($i=0;$i<count($photoes);$i++) {
            if(!empty ($product->$photoes[$i])) {
                array_push($imgs, $product->$photoes_pc[$i]);
                $product->$photoes[$i] =
                        Config::value("PRODUCT_IMAGE_PATH")."/mb/".$product->$photoes[$i];
            }
        }
        if(count($imgs)>1)
            $this->addTplParam("imgs", $imgs);
        //落札コイン数
        $product->bidcoins=($product->bidcoins)*($product->f_spend_coin);
        //落札用コインマネ
        $product->bidcoin_money=($product->bidcoins)*POINT_VALUE;
        //落札商品の現在価格と落札用コインマネのプラス結果
        $product->plus_end_price=$product->f_end_price+$product->bidcoin_money;

        //ﾃﾞｨｽｶｳﾝﾄ額
        $product->discount_price=$product->f_market_price-$product->f_end_price;
        $product->plus_discount_price=$product->f_market_price-$product->plus_end_price;
//      $product->f_market_price=$product->f_market_price == 0 ? 1 : $product->f_market_price;
	$product->discount_rate=$product->f_market_price==0?0:floor(($product->discount_price / $product->f_market_price) * 100);

        $product->plus_discount_rate=$product->f_market_price==0?0:floor(($product->plus_discount_price / $product->f_market_price) * 100);

        $product->f_end_price=number_format($product->f_end_price);
        $product->discount_price=number_format($product->discount_price);
        $product->discount_rate=number_format($product->discount_rate);
        $product->plus_discount_rate=number_format($product->plus_discount_rate);
//        $product->f_market_price=number_format($product->f_market_price);
        //入札ログ
        import('classes.model.BidModel');
        $model=new BidModel();
        $logs=$model->getEndProductBidLog($product_id,Utils::getInt(MOBILE_PRODUCT_BIDLOG_QTY,0));

       //会員のbidCount
        if($this->getMemberIdFromSession()!==FALSE) {
            //会員のbidCount
            $bidCount=$model->getMemberBidCount($product_id,$this->getMemberIdFromSession());
            $product->bid_count=number_format($bidCount);
        }

        $this->addTplParam("product", $product);
        $this->addTplParam("logs", $logs);
//        $this->addTplParam("bid_count", $bidCount);
        //商品のカテゴリの種類データを取得する
        import("classes.model.ProductGeneral");
        ProductGeneral::addCategoriyInfo($this);

        return $this->createTemplateResult("product_end.html");

    }


    /**
     *入札を押した時の処理
     * @return <type>
     */
    public function push() {
        if(!empty ($this->result))
            return $this->result;
//        $model=new ProductModel();
        $productId=$this->getRequestParam("id");

        //member情報チェック
        //セッションからログイン者のmemberidを取得
        $memberId=$this->getMemberIdFromSession();
        if(empty ($memberId)) {
            //セッションにユーザID存在しない場合、再ログインさせる
            return $this->createRedirectResult(Config::value("SERVER_PATH")."/login");
        }

//        //商品の詳細情報を取得
//        $productModel=new ProductModel();
//        $product = $productModel->getAuctionProductById($productId);
//
//        if(empty ($product))
//        //開催→終了　になる場合
//            return Message::showConfirmMessage($this,Msg::get("INVALID_AUCTION_PRODUCT"));
//
        //member情報を取得
        import('classes.model.MemberModel');
        $model=new MemberModel();
        $member=$model->getMemberById($memberId);
//
        //商品を取得(t_productsから）
//        $model=new ProductModel();
//        $p=$model->getProductByID($productId);
//
//        if($p->f_last_bidder==$member->fk_member_id) {
//            //ユーザが既に最終入札者の場合
//            $this->setSession("message", Msg::get("LASTEST_BIDDER"));
//            return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
//        }
//
//        //memberコイン数チェック
//        $coinTotal=$member->f_coin + $member->f_free_coin;
//        if($coinTotal < $product->f_spend_coin) {
//            //memberコイン数が足りない場合、コイン購入画面にリングさせ
//            $this->setSession("message", Msg::get("COIN_NOT_ENOUGH"));
//            return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
//        }
//
//        $product=Utils::objectMerge($product, $p);
//        $product->f_last_free_flg=0;
//        //フリーコインから消費する
//        if($member->f_free_coin >= $product->f_spend_coin) {
//            $member->f_free_coin -= $product->f_spend_coin;
//            $product->f_bit_free_count++;
//            $product->f_coin_type=0;
//            $product->f_last_free_flg=1;
//        }
//        else {
//            if($member->f_free_coin > 0)
//                    $product->f_last_free_flg=1;
//            //有料コイン
//            $member->f_coin =$member->f_coin - $product->f_spend_coin + $member->f_free_coin;
//            $member->f_free_coin=0;
//            $product->f_bit_buy_count++;
//            $product->f_coin_type=1;
//        }
//        if($product->f_spend_coin == 0)
//        {
//            $product->f_last_free_flg=0;
//        }
////        $product->f_last_free_flg = $product->f_spend_coin == 0 ? 2 : $product->f_last_free_flg;
//        $member->t_contact_count++;
//
//        //商品価格更新
//        $model->updateProductPrice($product);
//
//        //最終入札者更新
//        $product->f_last_bidder=$memberId;
//        $product->f_last_bidder_handle=$this->session("member_handle");
//        $product->f_auction_time+=Utils::getInt(BID_ADD_SECONDS,0);
//        $product->f_bid_type=0;
//
//
//        //DB更新処理
//        if(!$model->bidProcess($member,$product)) {
//            Logger::warn(sprintf(Msg::get("BID_ERROR_LOG"),$memberId,$productId));
//            $returnProduct->message=Msg::get("BIDDER_ERROR");
//            $returnProduct->time=$product->f_auction_time;
//            return $this->createAjaxResult($returnProduct, "json");
//        }

        import('classes.model.ProductGeneral');
        $result = ProductGeneral::bid($this, $member, $productId);
        if($result instanceof ActionResult)
            return $result;
//        $returnProduct->price=number_format($product->f_now_price);
//        $returnProduct->last_bidder=$product->f_last_bidder_handle;
//        $returnProduct->time=$product->f_auction_time;

        return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
    }

    /**
     *商品をウォッチリストに追加する
     * @return <ActionResult>
     */
    public function watch() {
        if(!empty ($this->result))
            return $this->result;
        $productId=$this->getRequestParam("pro_id");
        if(!empty($productId)) {
            import('classes.model.MyProductModel');
            $model=new MyProductModel();
            $result=$model->addWatchList($this->getMemberIdFromSession(), $productId);
            if($result) {
                return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
            }
        }
        //商品存在していない場合
        return Message::showConfirmMessage($this,Msg::get("INVALID_AUCTION_PRODUCT"));
    }

    public function cancel() {
        //ユーザログイン状況確認
        if(!empty ($this->result))
            return $this->result;

        $auto_id=$this->getRequestParam("auto_id");
        $pro_id=$this->getRequestParam("pro_id");
        if(empty ($auto_id) || empty($pro_id)) {
            //無効なリクエストの場合
            import("classes.utils.Message");
            return Message::showErrorRequest($this);
        }
        import("classes.model.ProductGeneral");
        ProductGeneral::autoBidCancel($this, $auto_id);
        return $this->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=$pro_id");
    }

}
?>
