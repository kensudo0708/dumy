<?php
import('classes.model.ProductAjaxModel');
import("classes.utils.Message");

class AjaxAction extends PcAction {
    public function auto() {

        header('ContentType: text/plain; charset=UTF-8');
        $resultText = '';

        // メンテナンス中チェック
        $maite = FALSE;
        if( MAINTE_FLAG == SYS_CLOSE ) {
            if($_SERVER['REMOTE_ADDR'] == "219.101.37.233" || $_SERVER['REMOTE_ADDR'] == USER_SERVER) {
            } else {
                $maite = TRUE;
            }
        }

        if ( $maite ) {
            //メンテナンス中の場合
            $resultText = '0"'.Msg::get("SYSTEM_MAINTENANCE");
        } else {
            // リクエストパラメータ取得
            $ids = $this->getRequestParam('ids');
            $idArray = array();
            if ( $ids ) {
                $idArray = array_map('intval', explode(',', $ids ));
            }
            $id = intval( $this->getRequestParam('detail') );

            $model = new ProductAjaxModel();

            if ( $idArray ) {
                // 一覧
                $data = $model->getList( $idArray );
                $resultText = $model->convertTextList( $data );
            } elseif ( $id ) {
                // 詳細
                $data = $model->getDetail( $id );
                $resultText = $model->convertTextDetail( $data );
            }
        }
        if ( !$resultText ) {
            // 結果が取れない場合は無効なリクエストとする
            $resultText = '0"'.Msg::get("INVALID_REQUEST");
        }
        return $this->createTextResult( $resultText );
    }
}
?>
