<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of LogoutAction
 *
 * @author mw
 */
class LogoutAction extends PcAction {
    //put your code here
    /**
     * ログアウト処理
     */
    public function execute() {
        $this->clearSessionInfo();
        //クッキをクリアする
        $this->clearCookieInfo();
        return $this->createRedirectResult(Config::value("SERVER_PATH"));
    }

    
}
?>
