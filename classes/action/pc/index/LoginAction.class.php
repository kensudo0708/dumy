<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import('classes.model.MemberModel');
import("classes.utils.Message");
/**
 * Description of LoginActionclass
 *
 * @author mw
 */

//☆★	ライブラリ読込み	★☆
$all_include_path = COMMON_LIB."all_include_lib.php";
include $all_include_path;

class LoginAction extends PcAction {
    /**
     * ログイン処理
     */
    public function execute() {
//G.Chin AWKC-186 2011-01-18 chg sta
/*
        // デベロッパからのアクセスはパスワードを要求しない
        //$isDeveloper = $_SERVER['REMOTE_ADDR'] == '219.101.37.233';
        $isDeveloper = FALSE;
        $id=trim($this->getRequestParam("login_id"));
        $pass=trim($this->getRequestParam("login_pass"));
*/
		$model=new MemberModel();

		$openId=$this->getRequestParam("openId");

		//楽天会員の場合
		if($openId != "")
		{
			//楽天会員：オープンID検索関数
			$memcnt = $model->CheckRakutenOpenId($openId);
			if($memcnt == 0)
			{
				//会員登録画面へ遷移
				$jump_url = SITE_URL."index.php/member/regrakuten?openId=$openId";
				header("Location: $jump_url");
				exit;
			}
			else
			{
				// デベロッパからのアクセスはパスワードを要求しない
				//$isDeveloper = $_SERVER['REMOTE_ADDR'] == '219.101.37.233';
				$isDeveloper = FALSE;
				
				//楽天会員：ログインIDパスワード取得関数
				$rakumem = $model->GetRakutenLoginIdPassword($openId);
				$id = $rakumem->f_login_id;
				
				$master=$model->getMemberMasterByLoginId($id);
				//Sessionにログイン情報を追加
				$this->addSessionInfo($master);
				//Cookieにログイン情報を追加
				$this->addCookieMemberInfo($master);
				//ログイン成功、マイページに戻る(仮でホームに戻る）
				if (isset($_SESSION["login_change"])) {
					$result=$this->createRedirectResult($this->session("login_change"));
					$this->removeSession("login_change");
				}else
					$result=$this->createRedirectResult(Config::value("SERVER_PATH").LOGIN_JUMP_URL);
				return $result;
			}
		}
		else
		{
			// デベロッパからのアクセスはパスワードを要求しない
			//$isDeveloper = $_SERVER['REMOTE_ADDR'] == '219.101.37.233';
			$isDeveloper = FALSE;
			$id=trim($this->getRequestParam("login_id"));
			$pass=trim($this->getRequestParam("login_pass"));
			
			//楽天会員：オープンID取得関数
			$w_openId = $model->GetRakutenOpenId($id,$pass);
			if($w_openId->f_open_id != "")
			{
				$this->addTplParam("error",  Msg::get("ID_PASS_ERROR"));
				return $this->createTemplateResult("login.html");
			}
		}
//G.Chin AWKC-186 2011-01-18 chg end
        if(
            (!$isDeveloper && ( empty($id) || empty($pass) ))
            || ($isDeveloper && empty($id))
        ) {
            //未入力の場合
            $this->addTplParam("error",  Msg::get("ID_PASS_ERROR"));
            return $this->createTemplateResult("login.html");
        }
        else {
            //入力チェック
            //キャッシュからマスタ情報取得
            $model=new MemberModel();
            $master=$model->getMemberMasterByLoginId($id);
            if(
                (!$isDeveloper && ($master===NULL || md5($pass)!=$master->f_login_pass || $master->f_activity!=1))
                || ($isDeveloper && ($master===NULL || $master->f_activity!=1))
            ) {
                //ログインできない場合
                $this->addTplParam("error", Msg::get("ID_PASS_ERROR"));
                return $this->createTemplateResult("login.html");
            }

            switch ($master->f_activity) {
                case 1:     //普通会員
                //Sessionにログイン情報を追加
                    $this->addSessionInfo($master);
                    //Cookieにログイン情報を追加
                    $this->addCookieMemberInfo($master);
                    //ログイン成功、マイページに戻る(仮でホームに戻る）
                    if (isset($_SESSION["login_change"])) {
                        $result=$this->createRedirectResult($this->session("login_change"));
                        $this->removeSession("login_change");
                    }else
                        $result=$this->createRedirectResult(Config::value("SERVER_PATH").LOGIN_JUMP_URL);
                    break;
                case 2:     //退会会員
                //ログインできない場合
                    $this->addTplParam("error", Msg::get("ID_PASS_ERROR"));
                    return $this->createTemplateResult("login.html");
                    break;
                default :
                    $this->addTplParam("error", Msg::get("ID_PASS_ERROR"));
                    return $this->createTemplateResult("login.html");
                    break;

            }
            return $result;
        }
    }

    /**
     *ログインフォームを表示する
     * @return <type>
     */
    public function show() {
        return $this->createTemplateResult("login.html");
    }


    /**
     *仮会員のURL確認処理
     */
    public function confirm() {
        $code=$this->getRequestParam("code");
        $model=new MemberModel();
        $member=$model->getMemberByCode($code);

        if($member!==NULL && $member->f_activity!=2) {
            if($member->f_activity==1) {
            //登録済みの場合
                $message=Msg::get("HAS_REGISTERED");
            } else {
                //登録する
                if($model->updateMemberActivity($member)) {
                    $this->addSessionInfo($member);
                    //Cookieにログイン情報を追加
                    $this->addCookieMemberInfo($member);
                    $message=Msg::get("REGISTER_SUCCESS");
                    //メール送信処理
                    import("classes.utils.MailReserver");
                    MailReserver::sendRegistSuccessMail($member);
                }
                else {
                    //urlが有効期間過ぎた場合
//                    $timeout_count="";
//                    $timeout_count=$model->check_kari_timeout($code);
//                    if($timeout_count==0) {
//                        $message=Msg::get("REGISTER_TIMEOUT");
//
//                    }
//                    else
                        $message=Msg::get("REGISTER_CONFIRM_FAILED");
                }
                $this->addTplParam("message", $message);

//G.Chin 2010-06-25 add sta
//G.Chin 2010-07-28 chg sta
                /*
			$so = $member->fk_member_id;
			
			//会員親紹介コード取得関数
			GetTMemberFKParentAd($so,$tmm_fk_parent_ad);
			//アフィリエイト広告情報取得関数
			GetTAffiliateInfo($tmm_fk_parent_ad,$taf_fk_program_id,$taf_f_regist,$taf_f_regist_value,$taf_f_first_buy,$taf_f_first_buy_price,$taf_f_first_buy_value,$taf_f_param1,$taf_f_param2,$taf_f_type,$taf_f_status,$taf_f_bank_pay,$taf_f_report,$taf_f_buy_count);
			//アフィリエイト広告登録判定関数
			CheckAffiliateRegistReport($tmm_fk_parent_ad,$not_report);
			if(($taf_f_status == 1) && ($taf_f_regist == 1) && ($taf_f_type == 0) && ($not_report == 0))
			{
				//▼状態が"有効"で、登録時報酬が"有り"で、種類が"A8"の場合
				$pid = $taf_fk_program_id;
				//▼SIパラメータの有無を判定
				if($taf_f_param1 == "")
				{
					$affiliate_tag = "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=1.1.1.regist' width='1' height='1'><br>";
				}
				else
				{
					$si = $taf_f_param1;
					$affiliate_tag = "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=$si' width='1' height='1'><br>";
				}
				
				//アフィリエイト広告登録報告数加算関数
				CountUpTAffiliateFRegReport($tmm_fk_parent_ad);	//G.Chin 2010-07-26 add
			}
			else
			{
				//▼状態が"無効"、もしくは種類が"A8"でない場合
				$affiliate_tag = "";
			}
                */
                //アフィリエイトタグ作成関数
                MakeAffiliateTag($member->fk_member_id,0,0,$affiliate_tag);
                //アフィリエイトタグ作成関数２
                MakeAffiliateTag2($member->fk_member_id,0,0,$affiliate_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-07-28 chg end
                /*
            if(A8_FLAG == 0)
            {
                $affiliate_tag = "";
            }
            else
            {
                $pid = A8_PID;
                $so = $member->fk_member_id;
                $affiliate_tag = "<img src='https://px.a8.net/cgi-bin/a8fly/sales?pid=$pid&so=$so&si=1.1.1.regist' width='1' height='1'><br>";
            }
                */
                $this->addTplParam("affiliate_tag", $affiliate_tag);
                $this->addTplParam("affiliate_tag2", $affiliate_tag2);	//G.Chin 2010-11-05 add
//G.Chin 2010-06-25 add end

                return $this->createTemplateResult("memreg_complete.html");
//            return Message::showConfirmMessage($this,$message);
            }
            //code認証できない場合、ホームに行かせ
            return $this->createRedirectResult(Config::value("SERVER_PATH"));
        }

        $message=Msg::get("REGISTER_TIMEOUT");
        $this->addTplParam("message", $message);
        return $this->createTemplateResult("memreg_complete.html");
    }
}
?>
