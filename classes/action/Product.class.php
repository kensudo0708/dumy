<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import('classes.model.ProductModel');
//import('classes.repository.ProductRepository');
/**
 * Description of Productclass
 *
 * @author mw
 */
class Product {
    //put your code here


    /**
     *開催中商品を取得する
     * @param <int> $columns 商品コラム数
     * @param <int> $rows　商品の行数
     * @param <boolean> $pageNum　ページnumber
     * @return <array> 商品のObjectの配列
     */
    public final static function getActionProducts($columns,$rows,$pageNum=false,$url='',$extendPage=10,$style="",$pagetag="page"){
        //レコード数を取得
//        $recordCount=Utils::getInt(ProductRepository::getAuctionProductCount(),0);
//        if($recordCount>0) {
            //ページIDを取得
//            $pageId=Utils::getInt($this->getRequestParam("page"),1);
            //１ページの商品数
//            $columns=Setting::value("prouduct_list_columns");
//            $rows=Setting::value("prouduct_list_rows");
//            $pageSize=Utils::getInt($columns*$rows);
            //総ぺージ数を計算
            $pageCount=Utils::getInt(($recordCount + $pageSize -1)/ $pageSize );
            $pageNumber=Utils::getPageNumbers($pageId,$pageCount,Config::value("SERVER_PATH")."/list",10);
            //開催中の商品を取得
            $model=new ProductModel();
            $products=$model->getProducts(($pageId-1) *$pageSize ,$pageSize);
            foreach ($products as $obj) {
                $obj->image=Config::value("PRODUCT_IMAGE_PATH")."/".$obj->image;
                $obj->time=Utils::getTimeString($obj->time);
                $obj->price=number_format($obj->price);
            }
            return $products;
//        }
//        return FALSE;
    }


}
?>
