<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import("classes.model.ShiharaiModel");
import("classes.model.TNewsModel");		//G.Chin #143 2011-01-19 add
/**
 * Description of IPSEbankclass
 *
 * @author kaz
 */

	//☆★	ライブラリ読込み	★☆
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

class IPSEbank extends BaseAction
{
    //put your code here
    public function execute()
    {
        //各種ﾊﾟﾗﾒｰﾀ取得
        // ex)http://www.ameoku.net/cli_ep.php/ipsebank/?gid=100357&rst=3&ec=&god=356&cod=&am=1001&tr=1001&ta=500&target=COINS&member_id=6&reserve_id=32

        $gid        = $_GET['gid'];
        $rst        = $_GET['rst'];
        $ec         = $_GET['ec'];
        $god        = $_GET['god'];
        $cod        = $_GET['cod'];
        $am         = $_GET['am'];
        $pay_money  = $_GET['tr'];
        $target     = $_GET['target'];
        $member_id  = $_GET['member_id'];
        $reserve_id = $_GET['reserve_id'];

        //支払い処理クラス生成
        $shiharaiModel = new ShiharaiModel();
        
		//ニュース処理クラス生成
		$newsModel = new TNewsModel();	//G.Chin #143 2011-01-19 add

        //受信結果に応じて処理を分岐
        if ($rst == IPS_EBANK_OK)
        {
//G.Chin #143 2011-01-18 chg sta
/*
            if ($target == "COINS")
            {
                //ｺｲﾝ購入処理実行
                $shiharaiModel->buyCoins($member_id, $pay_money, SHIHARAI_TYPE_INETBANKING, $reserve_id);
				
                //会員親紹介コード取得関数
                GetTMemberFKParentAd($member_id,$tmm_fk_parent_ad);	//G.Chin 2010-11-27 add

                //アフィリエイト広告コイン購入回数加算関数
                CountUpTAffiliateFBuyCount($tmm_fk_parent_ad);
                //アフィリエイト広告コイン購入回数加算関数２
                CountUpTAffiliateFBuyCount2($tmm_fk_parent_ad);	//G.Chin 2010-11-27 add
            }
            else
            {
                $pro_id = str_replace("PRD_", "", $target);
                $shiharaiModel->buyProduct($member_id, $pay_money, SHIHARAI_TYPE_INETBANKING, $reserve_id, $pro_id);
            }
*/
			if ($target == "COINS")
			{
				//パラメータチェック
				$ret = "";
				//▼会員ID
				if(($member_id == "") || (is_numeric($member_id) == false))
				{
					$ret = "NG";
				}
				//▼支払金額
				else if(($pay_money == "") || (is_numeric($pay_money) == false))
				{
					$ret = "NG";
				}
				//▼支払履歴ID
				else if(($reserve_id == "") || (is_numeric($reserve_id) == false))
				{
					$ret = "NG";
				}
				
				if($ret == "NG")
				{
					Logger::debug("[コイン購入エラー]pay_log ID:$reserve_id,会員ID:$member_id,支払金額:$pay_money");
					$shiharaiModel->FixPayLogNG($reserve_id, SHIHARAI_TYPE_INETBANKING, $rst);
					
					$row=array();
					$row["category"] = "決済関連";
					$row["body"] = "コイン購入エラー:決済完了通知のパラメータが不正 pay_log ID:$reserve_id,会員ID:$member_id,支払金額:$pay_money";
					$row["status"] = 0;
					$newsModel->insert($row);
				}
				else
				{
					//ｺｲﾝ購入処理実行
					$shiharaiModel->buyCoins($member_id, $pay_money, SHIHARAI_TYPE_INETBANKING, $reserve_id);
					
					//会員親紹介コード取得関数
					GetTMemberFKParentAd($member_id,$tmm_fk_parent_ad);
					
					//アフィリエイト広告コイン購入回数加算関数
					CountUpTAffiliateFBuyCount($tmm_fk_parent_ad);
					//アフィリエイト広告コイン購入回数加算関数２
					CountUpTAffiliateFBuyCount2($tmm_fk_parent_ad);
				}
			}
			else
			{
				$pro_id = str_replace("PRD_", "", $target);
				//パラメータチェック
				$ret = "";
				//▼会員ID
				if(($member_id == "") || (is_numeric($member_id) == false))
				{
					$ret = "NG";
				}
				//▼支払金額
				else if(($pay_money == "") || (is_numeric($pay_money) == false))
				{
					$ret = "NG";
				}
				//▼支払履歴ID
				else if(($reserve_id == "") || (is_numeric($reserve_id) == false))
				{
					$ret = "NG";
				}
				//▼商品ID
				else if(($pro_id == "") || (is_numeric($pro_id) == false))
				{
					$ret = "NG";
				}
				
				if($ret == "NG")
				{
					Logger::debug("[商品購入エラー]pay_log ID:$reserve_id,会員ID:$member_id,支払金額:$pay_money,商品ID:$pro_id");
					$shiharaiModel->FixPayLogNG($reserve_id, SHIHARAI_TYPE_INETBANKING, $rst);
					
					$row=array();
					$row["category"] = "決済関連";
					$row["body"] = "商品購入エラー:決済完了通知のパラメータが不正 pay_log ID:$reserve_id,会員ID:$member_id,支払金額:$pay_money,商品ID:$pro_id";
					$row["status"] = 0;
					$newsModel->insert($row);
				}
				else
				{
					$shiharaiModel->buyProduct($member_id, $pay_money, SHIHARAI_TYPE_INETBANKING, $reserve_id, $pro_id);
				}
			}
//G.Chin #143 2011-01-18 chg end
        }
        else if (($rst == IPS_EBANK_LOWER) || ($rst == IPS_EBANK_OVER))
        {
            $shiharaiModel->FixPayLogNG($reserve_id, SHIHARAI_TYPE_INETBANKING, $rst);
        }
        return $this->createTextResult("OK");
    }
}
?>
