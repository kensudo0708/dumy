<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import("classes.model.ShiharaiModel");
import("classes.model.TNewsModel");		//G.Chin #143 2011-01-19 add
import("classes.utils.PayCondition");
/**
 * Description of PSAprosclass
 *
 * @author chin
 */

	//☆★	ライブラリ読込み	★☆
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

class PSApros extends BaseAction
{
    //put your code here
    public function execute()
    {
        //各種ﾊﾟﾗﾒｰﾀ取得
        // ex)clientip=2790488&rel=1&ap=TestMod&ec=&god=2790478&cod=&am=1001&tx=0&sf=0&money=1001&option=COINS&sendid=6
        
        $member_id  = $_GET['uid'];
        $cid        = $_GET['cid'];
        $afid       = $_GET['afid'];
        $pt         = $_GET['pt'];
        $money      = $_GET['price'];
        
        $fk_member_id  = $member_id;
        $f_coin_add    = $pt;
        $f_pay_money   = $money;
        $f_unique_code = $afid;
        
		//ニュース処理クラス生成
		$newsModel = new TNewsModel();	//G.Chin #143 2011-01-19 add

//G.Chin #143 2011-01-18 add sta
		//パラメータチェック
		//▼会員ID
		if(($fk_member_id == "") || (is_numeric($fk_member_id) == false))
		{
			$ret = "NG";
		}
		//▼追加コイン
		else if(($f_coin_add == "") || (is_numeric($f_coin_add) == false))
		{
			$ret = "NG";
		}
		//▼支払金額
		else if(($f_pay_money == "") || (is_numeric($f_pay_money) == false))
		{
			$ret = "NG";
		}
		//▼ユニークコード
		else if($f_unique_code == "")
		{
			$ret = "NG";
		}
		
		if($ret == false)
		{
			$err_message = "[Error]:コイン購入:会員ID:".$fk_member_id."コイン枚数:".$f_coin_add."金額:".$f_pay_money;
			Logger::error($err_message);
					
			$row=array();
			$row["category"] = "決済関連";
			$row["body"] = "コイン購入エラー:決済完了通知のパラメータが不正 会員ID:".$fk_member_id."コイン枚数:".$f_coin_add."金額:".$f_pay_money;
			$row["status"] = 0;
			$newsModel->insert($row);
			
			return $this->createTextResult("OK");
			exit;
		}
//G.Chin #143 2011-01-18 add end

        //APROS決済関数
        $ret = PaymentApros($fk_member_id,$f_coin_add,$f_pay_money,$f_unique_code);
        if ($ret == false)
        {
            $err_message = "[Error]:コイン購入:会員ID:".$fk_member_id."コイン枚数:".$f_coin_add."金額:".$f_pay_money;
            Logger::error($err_message);
        }
        return $this->createTextResult("OK");
    }
}
?>
