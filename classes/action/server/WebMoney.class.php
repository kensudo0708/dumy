<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import("classes.model.ShiharaiModel");
import("classes.model.TNewsModel");		//G.Chin #143 2011-01-19 add
import("classes.utils.PayCondition");
/**
 * Description of IPSCreditclass
 *
 * @author kaz
 */

	//☆★	ライブラリ読込み	★☆
	$all_include_path = COMMON_LIB."all_include_lib.php";
	include $all_include_path;

class WebMoney extends BaseAction {
    //put your code here
    public function execute() {
        $data=$this->getRequestParam("wmret");
        $shoplast ="/var/www/cgi-bin/shoplast";
//        $cmd =$shoplast." -o Web0001979 ".mb_convert_encoding($data,"EUC-JP","auto");
        $cmd =$shoplast." -o Web0001979 '$data'";

        $dir=getcwd();
        chdir("/var/www/cgi-bin");
        exec($cmd,$rtn);
        chdir($dir);
        
        if(is_array($rtn) && count($rtn)>0) {
            $info=$rtn[0];
            Logger::debug("webmoney return info：$info");
            if(strlen($info) >=86) {
                //pay情報を解析
                $info = trim(substr($info, 86));
                $payinfo = explode("-", $info);
                if(count($payinfo) == 5) {
                    /*
                    $payinfo[0] 支払いﾀｲﾌ
                    $payinfo[1] 金額
                    $payinfo[2] ユーザID
                    $payinfo[3] t_pay_logのID
                    $payinfo[4] 商品ID
                    */
                    $shiharaiModel = new ShiharaiModel();
                    //受信結果に応じて処理を分岐
//G.Chin #143 2011-01-18 chg sta
/*
                    if ($payinfo[0] == 0) {
                        //ｺｲﾝ購入処理実行
                        $shiharaiModel->buyCoins($payinfo[2], $payinfo[1], SHIHARAI_TYPE_CREDIT, $payinfo[3]);
						
                        //会員親紹介コード取得関数
                        GetTMemberFKParentAd($member_id,$tmm_fk_parent_ad);	//G.Chin 2010-11-27 add

                        //アフィリエイト広告コイン購入回数加算関数
                        CountUpTAffiliateFBuyCount($tmm_fk_parent_ad);
                        //アフィリエイト広告コイン購入回数加算関数２
                        CountUpTAffiliateFBuyCount2($tmm_fk_parent_ad);	//G.Chin 2010-11-27 add
                    }
                    else {
                        $shiharaiModel->buyProduct($payinfo[2], $payinfo[1], SHIHARAI_TYPE_CREDIT, $payinfo[3], $payinfo[4]);
                    }
*/
					if ($payinfo[0] == 0) {
						//パラメータチェック
						$ret = "";
						//▼会員ID
						if(($payinfo[2] == "") || (is_numeric($payinfo[2]) == false))
						{
							$ret = "NG";
						}
						//▼支払金額
						else if(($payinfo[1] == "") || (is_numeric($payinfo[1]) == false))
						{
							$ret = "NG";
						}
						//▼支払履歴ID
						else if(($payinfo[3] == "") || (is_numeric($payinfo[3]) == false))
						{
							$ret = "NG";
						}
						
						if($ret == "NG")
						{
							Logger::debug("[コイン購入エラー]pay_log ID:$payinfo[3],会員ID:$payinfo[2],支払金額:$payinfo[1]");
							Logger::error("正しくない決済情報：$info");
					        
							//ニュース処理クラス生成
							$newsModel = new TNewsModel();	//G.Chin #143 2011-01-19 add
							
							$row=array();
							$row["category"] = "決済関連";
							$row["body"] = "コイン購入エラー:決済完了通知のパラメータが不正 pay_log ID:$payinfo[3],会員ID:$payinfo[2],支払金額:$payinfo[1]";
							$row["status"] = 0;
							$newsModel->insert($row);
						}
						else
						{
							//ｺｲﾝ購入処理実行
							$shiharaiModel->buyCoins($payinfo[2], $payinfo[1], SHIHARAI_TYPE_CREDIT, $payinfo[3]);
							
							//会員親紹介コード取得関数
							GetTMemberFKParentAd($member_id,$tmm_fk_parent_ad);
							
							//アフィリエイト広告コイン購入回数加算関数
							CountUpTAffiliateFBuyCount($tmm_fk_parent_ad);
							//アフィリエイト広告コイン購入回数加算関数２
							CountUpTAffiliateFBuyCount2($tmm_fk_parent_ad);
						}
					}
					else {
						$pro_id = str_replace("PRD_", "", $payinfo[4]);
						//パラメータチェック
						$ret = "";
						//▼会員ID
						if(($payinfo[2] == "") || (is_numeric($payinfo[2]) == false))
						{
							$ret = "NG";
						}
						//▼支払金額
						else if(($payinfo[1] == "") || (is_numeric($payinfo[1]) == false))
						{
							$ret = "NG";
						}
						//▼支払履歴ID
						else if(($payinfo[3] == "") || (is_numeric($payinfo[3]) == false))
						{
							$ret = "NG";
						}
						//▼商品ID
						else if(($pro_id == "") || (is_numeric($pro_id) == false))
						{
							$ret = "NG";
						}
						
						if($ret == "NG")
						{
							Logger::debug("[商品購入エラー]pay_log ID:$payinfo[3],会員ID:$payinfo[2],支払金額:$payinfo[1],商品ID:$pro_id");
							Logger::error("正しくない決済情報：$info");
					        
							//ニュース処理クラス生成
							$newsModel = new TNewsModel();	//G.Chin #143 2011-01-19 add
							
							$row=array();
							$row["category"] = "決済関連";
							$row["body"] = "商品購入エラー:決済完了通知のパラメータが不正 pay_log ID:$payinfo[3],会員ID:$payinfo[2],支払金額:$payinfo[1],商品ID:$pro_id";
							$row["status"] = 0;
							$newsModel->insert($row);
						}
						else
						{
							$shiharaiModel->buyProduct($payinfo[2], $payinfo[1], SHIHARAI_TYPE_CREDIT, $payinfo[3], $pro_id);
						}
					}
//G.Chin #143 2011-01-18 chg end
                }
                else
                    Logger::error("正しくない決済情報：$info");

            }
            else {
                //決済結果情報の解析失敗
                Logger::error("決済結果情報の解析失敗:$info;\n戻ってきたデータ:$data");
            }
            return $this->createRedirectResult("/index.php");
        }

    }
}
?>
