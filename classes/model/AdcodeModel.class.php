<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.'.model.Model');
/**
 * Description of AdcodeModel
 *
 * @author mw
 */
class AdcodeModel extends Model {
    //put your code here

    /**
     *広告コードの情報を取得する
     * @return <type>
     */
    public function getAdcodeInfo() {
		$sql  = "select adcode.fk_adcode_id,adcode.f_adcode,adcode.fk_adcode_id,adcode.fk_admaster_id ";
		$sql .= "from t_adcode as adcode,t_admaster as mast ";
		$sql .= "where mast.fk_admaster_id=adcode.fk_admaster_id ";
		return DB::getObjectByQuery($sql);
/*
        return DB::getObjectByQuery("SELECT adcode.fk_adcode_id,adcode.f_adcode,adcode.fk_adcode_id,adcode.fk_admaster_id,mast.fk_agent_id,
                                        agent.fk_price_id,price.fk_price_id
                                        FROM t_adcode adcode
                                        LEFT OUTER JOIN t_admaster mast ON mast.fk_admaster_id=adcode.fk_admaster_id
                                        LEFT OUTER JOIN t_ad_agent agent ON agent.fk_agent_id=mast.fk_agent_id
                                        LEFT OUTER JOIN t_price price ON price.fk_price_id=agent.fk_price_id");
*/
    }

    /**
     *指定されたIDで広告コードの情報を取得する
     *@param <int> $id fk_adcode_id
     * @return <object> 広告コードの情報
     */
    public function getAdcodeInfoById($id) {
		$sql  = "select adcode.fk_adcode_id,adcode.f_adcode,adcode.fk_adcode_id,adcode.fk_admaster_id ";
		$sql .= "from t_adcode as adcode,t_admaster as mast ";
		$sql .= "where mast.fk_admaster_id=adcode.fk_admaster_id ";
		$sql .= "and adcode.fk_adcode_id=$id ";
		return DB::getObjectByQuery($sql);
/*
        return DB::uniqueObject("SELECT adcode.fk_adcode_id,adcode.f_adcode,adcode.fk_adcode_id,adcode.fk_admaster_id,mast.fk_agent_id,
                                    agent.fk_price_id,price.fk_price_id
                                    FROM t_adcode adcode
                                    LEFT OUTER JOIN t_admaster mast ON mast.fk_admaster_id=adcode.fk_admaster_id
                                    LEFT OUTER JOIN t_ad_agent agent ON agent.fk_agent_id=mast.fk_agent_id
                                    LEFT OUTER JOIN t_price price ON price.fk_price_id=agent.fk_price_id
                                    WHERE adcode.fk_adcode_id=?",array($id));
*/
    }

    /**
     *指定されたf_adcodeで広告コードの情報を取得する
     * @param <string> $code f_adcode
     * @return <object> 広告コードの情報
     */
    public function getAdcodeInfoByAdcode($code) {
		$sql  = "select adcode.fk_adcode_id,adcode.f_adcode,adcode.fk_adcode_id,adcode.fk_admaster_id ";
		$sql .= "from t_adcode as adcode,t_admaster as mast ";
		$sql .= "where mast.fk_admaster_id=adcode.fk_admaster_id ";
		$sql .= "and adcode.f_adcode=$code ";
		return DB::getObjectByQuery($sql);
/*
        return DB::uniqueObject("SELECT adcode.fk_adcode_id,adcode.f_adcode,adcode.fk_adcode_id,adcode.fk_admaster_id,mast.fk_agent_id,
                                agent.fk_price_id
                                FROM t_adcode adcode
                                LEFT OUTER JOIN t_admaster mast ON mast.fk_admaster_id=adcode.fk_admaster_id
                                LEFT OUTER JOIN t_ad_agent agent ON agent.fk_agent_id=mast.fk_agent_id
                                LEFT OUTER JOIN t_member_master member ON member.fk_adcode_id=adcode.fk_adcode_id
                                WHERE adcode.f_adcode=?",array($code));
*/
    }

    /**
     *広告からのサインログを保存
     * @param <type> $adcodeId
     * @param <type> $memberId
     * @param <type> $priceId
     * @return <type>
     */
    public function saveAdcodeSignLog($adcodeId,$memberId,$priceId) {
        return DB::executeNonQuery("INSERT INTO t_adsignlog (fk_adcode_id,fk_member_id,fk_price_id,f_pay_flag,f_tm_stamp)
                        VALUES (?,?,?,0,CURRENT_TIMESTAMP)", array($adcodeId,$memberId,$priceId));
    }


}
?>
