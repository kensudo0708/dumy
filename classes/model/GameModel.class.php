<?php
/**
 * ■ゲーム機能用ビジネスロジック実装クラス
 *
 * 
 * ////////// ※重要※ //////////
 *
 * 本ロジックファイルはサービス機能用のものです。
 * 仕様変更、バグフィックスの際には下記の管理機能用ロジッククラスにも同様の修正を行う必要があります。
 * 両者は異なる実装ですが、同じ機能は同じ名称で実装しております。
 *
 * config/game_lib.php
 *
 * /////////////////////////////
 */
import("classes.model.ShiharaiModel");

class GameModel {

    const PRIZE_ITEMS_LIMIT = 10;

    protected $gameId = NULL;
    protected $game = array(); // t_game
    protected $options = array(
        /**
         * ある行動に対して与えられるプレイ回数が定義されている項目の情報
         * t_game.f_play_count<n> = <t_game_log.f_play_type>
         */
        'play_type.daily'=>1,    // 毎日
        'play_type.coin'=>2,     // コイン購入
        'play_type.coin_pack'=>3,// コインパック購入
        //'play_type.friend'=>4,   // 友達紹介(コイン購入)
        /**
         * ボーナス効果種別
         */
        'bonus_type.coin'=>1,     // コイン購入
        'bonus_type.coin_pack'=>2,// コインパック購入
        /**
         * 定数的な値
         */
        'const.prize_items'=>4,        // 懸賞の数(上限値10=DBの項目数)
        'const.t_pay_log.f_status'=>5, // t_pay_log.f_statusに設定する項目値。支払い区分＝ゲームの配当、を意味する
        /**
         * メッセージ領域の指定
         */
        'message.suspended'=>'f_info1', // 休止中メッセージ項目
        'message.empties'=>'f_info2',   // プレイ権利がない場合のメッセージ項目
    );

    // {{
    //
    // public functions
    //
    // }}

    /**
     * コンストラクタ
     * @param <type> $gameId
     * @param <type> $options
     */
    public function __construct( $gameId, $options = array() ) {
        $this->gameId = intval( $gameId );
        $this->options = array_merge( $this->options, $options );
        if ( $this->options['const.prize_items'] > self::PRIZE_ITEMS_LIMIT ) {
            $this->options['const.prize_items'] = self::PRIZE_ITEMS_LIMIT;
        }
        $this->game = $this->getGameRow( $this->gameId );
        if ( !$this->game ) {
            return NULL;
        }
    }

    /**
     * ■ゲーム完了処理
     * （このメソッドはトランザクションスコープの中から呼び出される必要があります）
      *
     * @param int t_member_master.fk_member_id
     * @param int t_game_log.f_game_log_id
     *
     * @return bool is first hand?
     */
    public function doCompletion( $memberId, $gameLogId ) {
        $memberId = intval( $memberId );
        $gameLogId = intval( $gameLogId );
        $gameLog = $this->getGameLogRow( $gameLogId );
        if ( $gameLog && is_array( $gameLog ) ) {
            // 会員ID整合性チェック
            if ( $gameLog['f_member_id'] != $memberId ) {
                $message = '要求されたゲーム履歴と会員情報が一致しません。'.PHP_EOL
                    .' -要求した会員(id='.$memberId.')'.PHP_EOL
                    .' -要求されたゲーム履歴(id='.$gameLogId.')'.PHP_EOL
                    .' -ゲーム履歴に設定されている会員(id='.$gameLog['f_member_id'].')';
                throw new RuntimeException( $message );
            }
            // プレイ終了
            $gameLog['f_play_status'] = 1;
            // 払戻要求シグナル受信回数をカウントアップ
            $gameLog['f_refund_request'] = intval( $gameLog['f_refund_request'] ) + 1;

            $this->updateGameLogRow( $gameLog );
        } else {
            throw new RuntimeException('要求されたゲーム履歴(id='.$gameLogId.')は存在しません。');
        }
    }

    /**
     * 払戻処理
     * （このメソッドはトランザクションスコープの中から呼び出される必要があります）
     *
     * @return bool do refund_process?
     */
    public function doRefund( $memberId, $gameLogId ) {
        $memberId = intval( $memberId );
        $gameLogId = intval( $gameLogId );

        $gameLog = $this->getGameLogRow( $gameLogId );
        if ( $gameLog && is_array( $gameLog ) ) {
            // 会員ID整合性チェック
            if ( $gameLog['f_member_id'] != $memberId ) {
                $message = '要求されたゲーム履歴と会員情報が一致しません。'.PHP_EOL
                    .' -要求した会員(id='.$memberId.')'.PHP_EOL
                    .' -要求されたゲーム履歴(id='.$gameLogId.')'.PHP_EOL
                    .' -ゲーム履歴に設定されている会員(id='.$gameLog['f_member_id'].')';
                throw new RuntimeException( $message );
            }

            if ( $gameLog['f_refund_status'] == 0 ) {

                $gameLog['f_refund_time'] = date('Y-m-d H:i:s');
                $gameLog['f_refund_status'] = 1; // 払戻済
                $gameLog['f_refund_type'] = 0;   // ０：リアルタイム払戻

                // 配当がゼロの場合は以下の処理は不要（マイナスは処理されます）
                if ( $gameLog['f_item_a'] + $gameLog['f_item_b'] ) {
                    /**
                     * 払戻処理
                     * １．会員所持コインの加算(update: t_member)
                     * ２．支払履歴の作成(insert: t_pay_log)
                     */
                    $model = new ShiharaiModel();

                    // １．会員所持コインの加算
                    $coins = new stdClass();
                    $coins->f_coin = $gameLog['f_item_a'];
                    $coins->f_free_coin = $gameLog['f_item_b'];
                    $model->addMemberCoins( $memberId, $coins );

                    $model = new MemberModel();
                    $member = $model->getMemberById( $memberId );

                    // ２．支払履歴の作成
                    $coinResult = $member->f_coin + $member->f_free_coin;
                    $payLog = $this->createPayLogRow( $gameLog, $coinResult );
                    $this->insertPayLogRow( $payLog );
                    $gameLog['f_pay_log_id'] = $payLog['f_pay_log_id'];
                }
            }
            $this->updateGameLogRow( $gameLog );
        } else {
            throw new RuntimeException('要求されたゲーム履歴(id='.$gameLogId.')は存在しません。');
        }
    }

    /**
     * 休止中メッセージを取得
     * @return <type>
     */
    public function getSuspendedMessage() {
        return $this->game[($this->options['message.suspended'])];
    }

    /**
     * プレイ権利がない場合のメッセージを取得
     * @return <type>
     */
    public function getEmptiesMessage() {
        return $this->game[($this->options['message.empties'])];
    }

    /**
     * ■ゲームが開催中か
     *
     * @return bool
     */
    public function isHolding() {
        $judge = FALSE;
        if ( isset( $this->game['f_status'] ) && $this->game['f_status'] == 0 ) {
            $judge = TRUE;
        }
        return $judge;
    }

    /**
     * ■プレイ権利を取得する
     * （このメソッドはトランザクションスコープの中から呼び出される必要があります）
      *
     * @param 会員ID(t_member_master.f_member_id)
     * @return t_game_log.f_game_log_id or FALSE
     */
     public function getPlayTicket( $memberId ) {
        //Logger::debug( '[ENTER]'.__METHOD__ );
        $memberId = intval( $memberId );
        $gameLog = array();
        $exists = FALSE;
        $availableTryCount = 0;  // 毎日付与されるプレイ回数

        // 開催休止中は処理しない
        if ( !$this->isHolding() ) {
            return $gameLog;
        }

        $dailyCount = $this->getDailyCount();
        $availableTryCount = $dailyCount;
        $dailyCountDown = 0;
        $list = $this->getAvailableGameLogList( $memberId );
        if ( is_array( $list ) ) {
            foreach ( $list as $row ) {
                // 毎日権利
                if ( $row['f_play_type'] == $this->options['play_type.daily'] ) {
                    if ( $row['f_play_status'] == 0 ) {
                        // 未プレイのものがあれば採用
                        if ( $dailyCountDown >= $dailyCount ) {
                            $availableTryCount++;
                        }
                        if ( !$gameLog ) {
                            $gameLog = $row;
                        }
                    } else {
                        // 運用上、マスタ設定回数以上の毎日権利を付与するケースもあるためカウントダウンはマスタ設定回数まで
                        if ( $dailyCountDown < $dailyCount ) {
                            $availableTryCount--;
                            $dailyCountDown++;
                        }
                    }
                } else {
                    // 毎日権利以外
                    if ( $row['f_play_status'] == 0 ) {
                        $availableTryCount++;
                        if ( !$gameLog ) {
                            // 最初にヒットしたものを取得
                            $gameLog = $row;
                        }
                    }
                }
            }
        }
        if ( $availableTryCount < 0 ) {
            $availableTryCount = 0;
        }

        /**
         * プレイ権利が登録されておらず、$availableTryCountが残っている場合は作成する。この権利は毎日付与される種類のものである。
         */
        if ( !$gameLog && $availableTryCount > 0 ) {
            // 毎日権利の発行
            $gameLog = $this->createGameLogRow( $memberId, $this->options['play_type.daily'] );
        } else {
            $exists = TRUE;
        }
        if ( $gameLog ) {
            $doPlay = FALSE;
            if ( $gameLog['f_play_status'] == 0 && !isset( $gameLog['f_result_no'] ) ) {
                // まだ抽選を行っていない場合は行う
                $this->doPlay( $gameLog );
                $doPlay = TRUE;
            }
            if ( !$exists ) {
                // insert
                $this->insertGameLogRow( $gameLog );
            } elseif ( $doPlay ) {
                // update
                $this->updateGameLogRow( $gameLog );
            }
        }
        // 画面表示用に残りプレイ回数を追加して返却
        $gameLog['available_try_count'] = $availableTryCount;
        return $gameLog;
    }

    /**
     * ボーナス効果の付与
     */
    public function putGameBonus( $memberId, $value, $bonusType ) {
        $memberId= intval( $memberId );
        $bonusType = intval( $bonusType );
        $value = floatval( $value );

        // 開催休止中は処理しない
        if ( !$this->isHolding() ) {
            return NULL;
        }

        // ボーナス適用チェック
        $bonus = array();
        $gameBonus = $this->getGameBonusList( $bonusType );
        foreach ( $gameBonus as $row ) {
            $row['f_trigger_value'] = floatval( $row['f_trigger_value'] );
            if ( $row['f_trigger_value'] <= $value ) {
                $bonus = $row;
                break;
            }
        }
        if ( $bonus ) {
            $gameBonusLog = $this->createGameBonusLogRow( $memberId, $bonus );
            $this->insertGameBonusLogRow( $gameBonusLog );
        }
    }

    public function putGameBonusOfCoin( $memberId, $coin ) {
        $coin = intval( $coin );
        $this->putGameBonus( $memberId, $coin, $this->options['bonus_type.coin'] );
    }

    public function putGameBonusOfCoinPack( $memberId, $coin ) {
        $coin = intval( $coin );
        $this->putGameBonus( $memberId, $coin, $this->options['bonus_type.coin_pack'] );
    }

    /**
     * プレイ権利の付与
     */
    public function putPlayTicket( $memberId, $playType ) {
        $memberId= intval( $memberId );
        $playType = intval( $playType );

        // 開催休止中は処理しない
        if ( !$this->isHolding() ) {
            return NULL;
        }
        
        // 設定回数付与される
        $count = intval( $this->game[('f_play_count'.$playType)] );
        for ( $i = 0; $i < $count; $i++ ) {
            $gameLog = $this->createGameLogRow($memberId, $playType);
            $this->doPlay( $gameLog );
            $this->insertGameLogRow( $gameLog );
        }
    }
    public function putPlayTicketOfCoin( $memberId ) {
        $this->putPlayTicket( $memberId, $this->options['play_type.coin']  );
    }
    public function putPlayTicketOfCoinPack( $memberId ) {
        $this->putPlayTicket( $memberId, $this->options['play_type.coin_pack']  );
    }

    // {{
    //
    // protected functions
    //
    // }}

    /**
     * 新しくINSERTするためのt_game_bonus_logレコード連想配列を作成
     */
    protected function createGameBonusLogRow( $memberId, $gameBonus ) {
        $now = date('Y-m-d H:i:s');
        $data = array(
            'f_member_id'=>$memberId,
            'f_game_id'=>$this->gameId,
            'f_status'=>0,
            'f_priority_level'=>$gameBonus['f_priority_level'],
            'f_bonus_type'=>$gameBonus['f_bonus_type'],
            'f_bonus_name'=>$gameBonus['f_bonus_name'],
            'f_trigger_value'=>$gameBonus['f_trigger_value'],
            'f_create_time'=>$now,
            'f_update_time'=>$now,
        );
        for ( $i = 1; $i <= $this->options['const.prize_items']; $i++ ) {
            $data[('f_value'.$i)] = $gameBonus[('f_value'.$i)];
        }
        return $data;
    }

    /**
     * 新しくINSERTするためのt_game_logレコード連想配列を作成（！抽選前状態です）
     */
    protected function createGameLogRow( $memberId, $playType ) {
        $now = date('Y-m-d H:i:s');
        $row = array(
            // NULL値をインサートしたい項目はコメントアウト
            'f_member_id'=>$memberId,
            'f_game_id'=>$this->gameId,
            'f_play_status'=>0,
            'f_play_type'=>$playType,
            //'f_result_no'=>NULL,
            //'f_name'=>NULL,
            //'f_message'=>NULL,
            'f_item_a'=>0,
            'f_item_b'=>0,
            'f_item_c'=>0,
            //'f_refund_type'=>NULL,
            'f_refund_request'=>0,
            //'f_refund_time'=>NULL,
            //'f_pay_log_id'=>NULL,
            //'f_expire_time'=>NULL,
            'f_create_time'=>$now,
            'f_update_time'=>$now,
        );
        return $row;
    }

    /**
     * 新しくINSERTするためのt_pay_logレコード連想配列を作成
     */
    protected function createPayLogRow( $gameLog, $coinResult ) {
        $data = array(
            'f_status'=>$this->options['const.t_pay_log.f_status'],
            'f_member_id'=>$gameLog['f_member_id'],
            //'f_staff_id'=>NULL,
            'f_pay_money'=>0,
            'f_coin_add'=>$gameLog['f_item_a'],
            'f_free_coin_add'=>$gameLog['f_item_b'],
            //'fk_shiharai_type_id'=>NULL,
            'f_coin_result'=>$coinResult,
            'f_min_price'=>0,
            'f_cert_status'=>0,
            //'f_cert_err'=>NULL,
            'f_pay_pos'=>AgentInfo::isPc() ? 0:1, // 払戻要求を発信した端末区分
            'f_tm_stamp'=>$gameLog['f_refund_time'],
            'fk_adcode_id'=>0,
            'fk_products_id'=>0,
            'f_double'=>0,
            'f_unique_code'=>$gameLog['f_game_log_id'],
        );
        return $data;
    }

    /**
     * ■ゲーム（抽選）を行う
     *
     * @param t_game_log.rows
     */
    protected function doPlay( &$gameLog ) {

        // 結果をハズレで初期化
        $gameLog['f_result_no'] = 0;
        $gameLog['f_name'] = $this->game['f_blank_name'];
        $gameLog['f_message'] = $this->game['f_blank_message'];
        $gameLog['f_item_a'] = $this->game['f_blank_item_a'];
        $gameLog['f_item_b'] = $this->game['f_blank_item_b'];
        $gameLog['f_item_c'] = $this->game['f_blank_item_c'];

        // ボーナス効果を取得
        $gameBonusLog = array();
        $list = $this->getAvailableBonusLogList( $gameLog['f_member_id'] );
        if ( isset( $list[0] ) ) {
            $gameBonusLog = $list[0];
        }
        // 抽選懸賞一覧を取得
        $prizeList = $this->getAvailablePrizeList();
        $winsSummary = $this->getWinsSummaryInToday();

        Logger::debug( '[懸賞設定]'.PHP_EOL.var_export($prizeList, TRUE) );
        Logger::debug( '[的中数]'.PHP_EOL.var_export($winsSummary, TRUE) );

        foreach ( $prizeList as $row ) {
            $row['f_value'] = floatval( $row['f_value'] );
            if ( !isset( $winsSummary[($row['f_result_no'])] ) ) {
                $winsSummary[($row['f_result_no'])] = 0;
            }
            if ( isset( $gameBonusLog[('f_value'.$row['f_result_no'])] ) ) {
                $row['f_value'] += floatval( $gameBonusLog[('f_value'.$row['f_result_no'])] );
            }
            // 当選確率を0-100%の範囲に補正
            if ( $row['f_value'] < 0 ) {
                $row['f_value'] = 0;
            } elseif ( $row['f_value'] > 100 ) {
                $row['f_value'] = 100;
            }
            //Logger::debug( $row['f_result_no'].':'.$row['f_value'].'%' );
            // 抽選(0%はスキップ)
            $win = FALSE;
            if ( $row['f_value'] ) {
                /**
                 * （抽選タイプA）的中確率５０％以上
                 * 　・1～100までの範囲で乱数を生成し、生成値が的中確率未満ならアタリと判定
                 * 　・1%未満の設定は内部的には無視される
                 * 　
                 * （抽選タイプB）的中確率５０％以下
                 * 　・1～"四捨五入(100 / 的中確率)"した値の範囲で乱数を生成し、生成値が1ならアタリと判定
                 */
                if ( $row['f_value'] > 50 ) {
                    $value = mt_rand( 1, 100 );
                    if ( $value < round( $row['f_value'] ) ) {
                        $win = TRUE;
                    }
                } else {
                    $value = mt_rand( 1, round(100 / $row['f_value']) );
                    if ( $value == 1 ) {
                        $win = TRUE;
                    }
                }
            }
            //Logger::debug( 'Summary < Limit: '.$winsSummary[($row['f_result_no'])].' < '.$this->game[('f_limit'.$row['f_result_no'])] );
            if (
                $win
                // 的中本数上限チェック
                && (
                    $this->game[('f_limit'.$row['f_result_no'])] == 0  // 0は本数無制限
                    || $winsSummary[($row['f_result_no'])] < $this->game[('f_limit'.$row['f_result_no'])]
                )
            ) {
                $gameLog['f_result_no'] = $row['f_result_no'];
                $gameLog['f_value'] = $row['f_value'];
                $gameLog['f_name'] = $row['f_name'];
                $gameLog['f_message'] = $row['f_message'];
                $gameLog['f_item_a'] = $row['f_item_a'];
                $gameLog['f_item_b'] = $row['f_item_b'];
                $gameLog['f_item_c'] = $row['f_item_c'];
                break;
            }
        }
        if ( $gameLog['f_result_no'] != 0 && $gameBonusLog ) {
            // 当選時はボーナス効果を消費する
            $gameBonusLog['f_status'] = 1;
            $this->updateGameBonusLogRow( $gameBonusLog );
        }
    }
    
    /**
     * 有効なボーナス効果一覧を取得
     */
    protected function getAvailableBonusLogList( $memberId ) {
        $data = array();
        $sql = "
SELECT
  f_game_bonus_log_id
  ,f_member_id
  ,f_game_id
  ,f_status
  ,f_priority_level
  ,f_bonus_type
  ,f_bonus_name
  ,f_trigger_value
  ,f_value1
  ,f_value2
  ,f_value3
  ,f_value4
  ,f_value5
  ,f_value6
  ,f_value7
  ,f_value8
  ,f_value9
  ,f_value10
  ,f_expire_time
  ,f_create_time
  ,f_update_time
FROM
  auction.t_game_bonus_log
WHERE
  f_game_id=?
  AND f_member_id=?
  AND f_status=0     -- 未消費
ORDER BY
  f_priority_level ASC, f_game_bonus_log_id ASC
FOR UPDATE
";
        $params = array(
            $this->gameId,
            $memberId,
        );
        try {
            $data = DB::getArrayByQuery( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
        return $data;
    }

    /**
     * 有効なゲーム履歴一覧（未プレイ状態）を取得 ←ウソ、毎日権利はプレイ済も取得
     */
    protected function getAvailableGameLogList( $memberId ) {
        $data = array();
        $today = date('Y-m-d');
        $sql = "
SELECT
  f_game_log_id
  ,f_member_id
  ,f_game_id
  ,f_play_status
  ,f_play_type
  ,f_result_no
  ,f_value
  ,f_name
  ,f_message
  ,f_item_a
  ,f_item_b
  ,f_item_c
  ,f_refund_status
  ,f_refund_type
  ,f_refund_request
  ,f_refund_time
  ,f_pay_log_id
  ,f_expire_time
  ,f_create_time
  ,f_update_time
FROM
  auction.t_game_log
WHERE
  -- 毎日プレイ権利で本日以前のものでも未プレイ状態のものは有効な権利と見なします。
  f_game_id=?
  AND f_member_id=?
  AND (
    (f_play_type<>? AND f_play_status=0) -- 毎日権限以外で未プレイ
    OR (f_play_type=? AND f_create_time BETWEEN ? AND ? ) -- 毎日権限で本日付け
  )
ORDER BY
  f_game_log_id ASC -- 古いものから優先的に使用される
FOR UPDATE
";
        $params = array(
            $this->gameId,
            $memberId,
            $this->options['play_type.daily'],
            $this->options['play_type.daily'],
            $today.' 00:00:00',
            $today.' 23:59:59',
        );
        try {
            $data = DB::getArrayByQuery( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
        // dailyの権利が先に消費されるよう整列
        $daily = array();
        $others = array();
        foreach ( $data as $row ) {
            if ( $row['f_play_type'] == $this->options['play_type.daily'] ) {
                $daily[] = $row;
            } else {
                $others[] = $row;
            }
        }
        $data = array_merge( $daily, $others );
        return $data;
    }

    /**
     * 抽選対象となる懸賞一覧を取得
     * @return array
     */
    protected function getAvailablePrizeList() {
        $data = array();
        $sortIndex = array();
        for ( $i = 1; $i <= $this->options['const.prize_items']; $i++ ) {
            if ( isset( $this->game[('f_status'.$i)] ) && $this->game[('f_status'.$i)] == 0 ) {
                $sortIndex[($i)] = $this->game[('f_value'.$i)];
            }
        }
        asort( $sortIndex );
        foreach ( $sortIndex as $i=>$value ) {
            $data[] = array(
                'f_result_no'=>$i,
                'f_name'=>$this->game[('f_name'.$i)],
                'f_message'=>$this->game[('f_message'.$i)],
                'f_value'=>$this->game[('f_value'.$i)],
                'f_item_a'=>$this->game[('f_item_a'.$i)],
                'f_item_b'=>$this->game[('f_item_b'.$i)],
                'f_item_c'=>$this->game[('f_item_c'.$i)],
            );
        }
        return $data;
    }

    /**
     * プレイ権利（毎日）に設定された回数を取得
     */
    protected function getDailyCount() {
        $count = intval( $this->game[('f_play_count'.$this->options['play_type.daily'])] );
        return $count;
    }

    /**
     * 指定IDのゲームボーナスマスタ一覧を取得
     *
     * @param <type> $gameLogId
     * @return <type>
     */
    protected function getGameBonusList( $bonusType ) {
        $data = array();
        $sql = "
SELECT
  f_game_id
  ,f_bonus_id
  ,f_status
  ,f_priority_level
  ,f_bonus_type
  ,f_bonus_name
  ,f_trigger_value
  ,f_value1
  ,f_value2
  ,f_value3
  ,f_value4
  ,f_value5
  ,f_value6
  ,f_value7
  ,f_value8
  ,f_value9
  ,f_value10
  ,f_update_time
  ,f_update_staff_id
FROM
  auction.t_game_bonus
WHERE
  f_game_id=? AND f_bonus_type=? AND f_status=0
ORDER BY f_trigger_value DESC";
        $params = array( $this->gameId, $bonusType );
        try {
            $data = DB::getArrayByQuery( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
        return $data;
    }

    /**
     * 指定IDのゲームプレイ履歴レコードを取得
     *
     * @param <type> $gameLogId
     * @return <type>
     */
    protected function getGameLogRow( $gameLogId ) {
        $data = array();
        $sql = "
SELECT
  f_game_log_id
  ,f_member_id
  ,f_game_id
  ,f_play_status
  ,f_play_type
  ,f_result_no
  ,f_value
  ,f_name
  ,f_message
  ,f_item_a
  ,f_item_b
  ,f_item_c
  ,f_refund_status
  ,f_refund_type
  ,f_refund_request
  ,f_refund_time
  ,f_pay_log_id
  ,f_expire_time
  ,f_create_time
  ,f_update_time
FROM
  auction.t_game_log
WHERE
  f_game_log_id=? FOR UPDATE";
        $params = array( $gameLogId );
        try {
            $data = DB::uniqueArray( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
        return $data;
    }

    /**
     * 指定IDのゲームマスタレコードを取得
     *
     * @param <type> $gameId
     * @return <type>
     */
    protected function getGameRow( $gameId ) {
        $gameId = intval( $gameId );
        $data = array();
        $sql = "
SELECT
  f_game_id
  ,f_game_name
  ,f_status
  ,f_play_count1
  ,f_play_count2
  ,f_play_count3
  ,f_play_count4
  ,f_play_count5
  ,f_info1
  ,f_info2
  ,f_info3
  ,f_blank_item_a
  ,f_blank_item_b
  ,f_blank_item_c
  ,f_blank_name
  ,f_blank_message
  ,f_status1
  ,f_status2
  ,f_status3
  ,f_status4
  ,f_status5
  ,f_status6
  ,f_status7
  ,f_status8
  ,f_status9
  ,f_status10
  ,f_item_a1
  ,f_item_a2
  ,f_item_a3
  ,f_item_a4
  ,f_item_a5
  ,f_item_a6
  ,f_item_a7
  ,f_item_a8
  ,f_item_a9
  ,f_item_a10
  ,f_item_b1
  ,f_item_b2
  ,f_item_b3
  ,f_item_b4
  ,f_item_b5
  ,f_item_b6
  ,f_item_b7
  ,f_item_b8
  ,f_item_b9
  ,f_item_b10
  ,f_item_c1
  ,f_item_c2
  ,f_item_c3
  ,f_item_c4
  ,f_item_c5
  ,f_item_c6
  ,f_item_c7
  ,f_item_c8
  ,f_item_c9
  ,f_item_c10
  ,f_value1
  ,f_value2
  ,f_value3
  ,f_value4
  ,f_value5
  ,f_value6
  ,f_value7
  ,f_value8
  ,f_value9
  ,f_value10
  ,f_limit1
  ,f_limit2
  ,f_limit3
  ,f_limit4
  ,f_limit5
  ,f_limit6
  ,f_limit7
  ,f_limit8
  ,f_limit9
  ,f_limit10
  ,f_name1
  ,f_name2
  ,f_name3
  ,f_name4
  ,f_name5
  ,f_name6
  ,f_name7
  ,f_name8
  ,f_name9
  ,f_name10
  ,f_message1
  ,f_message2
  ,f_message3
  ,f_message4
  ,f_message5
  ,f_message6
  ,f_message7
  ,f_message8
  ,f_message9
  ,f_message10
  ,f_update_time
  ,f_update_staff_id
FROM
  auction.t_game
WHERE
  f_game_id=?";
        $params = array( $gameId );
        try {
            $data = DB::uniqueArray( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
        return $data;
    }

    /**
     * 本日付の当選本数サマリを取得する
     */
    protected function getWinsSummaryInToday() {
        $data = array();
        $today = date('Y-m-d');
        $sql = "
SELECT f_result_no, COUNT( f_game_log_id ) AS `sum`
FROM
  auction.t_game_log
WHERE
  f_game_id=?
  AND f_result_no<>0
  AND f_create_time BETWEEN ? AND ?
GROUP BY f_result_no
";
        $params = array(
            $this->gameId,
            $today.' 00:00:00',
            $today.' 23:59:59',
        );
        try {
            $tmp = DB::getArrayByQuery( $sql, $params );
            if ( is_array( $tmp ) ) {
                foreach ( $tmp as $row ) {
                    $data[($row['f_result_no'])] = $row['sum'];
                }
            }
        } catch ( Exception $e ) {
            throw $e;
        }
        return $data;
    }

    /**
     * ゲームボーナス履歴を挿入する
     */
    protected function insertGameBonusLogRow( &$gameBonusLog ) {
        foreach ( $gameBonusLog as $key=>$value ) {
            // SELECT時のフェッチモードがBOTHで固定されているため
            if ( is_numeric( $key ) ) {
                unset( $gameBonusLog[($key)] );
            }
        }
        $sql = "
INSERT INTO auction.t_game_bonus_log (
    ".implode(", ", array_keys( $gameBonusLog ))."
) VALUES (
    ".implode(", ", array_fill( 0, count($gameBonusLog), '?'))."
)
";
        $params = $gameBonusLog;
        try {
            DB::executeNonQuery( $sql, $params );
            $gameBonusLog['f_game_bonus_log_id'] = mysql_insert_id();
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    /**
     * ゲーム履歴を挿入する
     */
    protected function insertGameLogRow( &$gameLog ) {
        foreach ( $gameLog as $key=>$value ) {
            // SELECT時のフェッチモードがBOTHで固定されているため
            if ( is_numeric( $key ) ) {
                unset( $gameLog[($key)] );
            }
        }
        $sql = "
INSERT INTO auction.t_game_log (
    ".implode(", ", array_keys( $gameLog ))."
) VALUES (
    ".implode(", ", array_fill( 0, count($gameLog), '?'))."
)
";
        $params = array_values( $gameLog );
        try {
            DB::executeNonQuery( $sql, $params );
            $gameLog['f_game_log_id'] = mysql_insert_id();
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    /**
     * ゲーム履歴に対応する支払い履歴レコードを作成
     */
    protected function insertPayLogRow( &$payLog ) {
        $sql = "
INSERT INTO auction.t_pay_log (
    ".implode(", ", array_keys( $payLog ))."
) VALUES (
    ".implode(", ", array_fill( 0, count( $payLog ), '?'))."
)
";
        $params = array_values( $payLog );
        try {
            DB::executeNonQuery( $sql, $params );
            $payLog['f_pay_log_id'] = mysql_insert_id();
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    /**
     * ボーナス効果を更新する
     */
    protected function updateGameBonusLogRow( $gameBonusLog ) {
        foreach ( $gameBonusLog as $key=>$value ) {
            // SELECT時のフェッチモードがBOTHで固定されているため
            if ( is_numeric( $key ) ) {
                unset( $gameBonusLog[($key)] );
            }
        }
        // 更新対象外項目、キー項目を除外
        $id = $gameBonusLog['f_game_bonus_log_id'];
        unset( $gameBonusLog['f_game_bonus_log_id'] );
        unset( $gameBonusLog['f_update_time'] );
        $sql = "
UPDATE auction.t_game_bonus_log SET
  ".implode("=?,", array_keys( $gameBonusLog ))."=?
WHERE
  f_game_bonus_log_id=?
";
        $params = array_values( $gameBonusLog );
        $params[] = $id;
        try {
            DB::executeNonQuery( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    /**
     * ゲーム履歴を更新する
     */
    protected function updateGameLogRow( $gameLog ) {
        foreach ( $gameLog as $key=>$value ) {
            // SELECT時のフェッチモードがBOTHで固定されているため
            if ( is_numeric( $key ) ) {
                unset( $gameLog[($key)] );
            }
        }
        // 更新対象外項目、キー項目を除外
        $id = $gameLog['f_game_log_id'];
        unset( $gameLog['f_game_log_id'] );
        unset( $gameLog['f_update_time'] );
        $sql = "
UPDATE auction.t_game_log SET
  ".implode("=?,", array_keys( $gameLog ))."=?
WHERE
  f_game_log_id=?
";
        $params = array_values( $gameLog );
        $params[] = $id;
        try {
            DB::executeNonQuery( $sql, $params );
        } catch ( Exception $e ) {
            throw $e;
        }
    }
}
?>
