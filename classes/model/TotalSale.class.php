<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * このクラスの呼び出しはトランザクション内部で
 *
 * @author mw
 */
class TotalSale {
    //put your code here

    /**
     *会員登録数の記録処理
     *@return <boolean> 成功:true
     */
    public static function register() {
        return self::columnUpdate("f_memreg",1);
    }

    /**
     *ｺｲﾝ購入分入金額の記録処理
     * @param <type> $money 購入金額
     * @param <type> $coins コイン数
     * @param <type> $type_id 支払いID（t_shiharai_type.fk_shiharai_type_id）
     * @return <boolean> 成功:true
     */
    public static function buycoin($money,$coins,$type_id) {
        $plan =DB::uniqueObject("SELECT plan.f_statistics_no FROM t_shiharai_type t
                            LEFT OUTER JOIN t_shiharai_plan plan ON t.fk_shiharai_id=plan.fk_shiharai_id
                            WHERE t.fk_shiharai_type_id=?",array($type_id));
        if(empty ($plan))
            return false;
        $f_buycoin = "f_buycoin".$plan->f_statistics_no;
//G.Chin 2010-05-21 chg sta
//        $sql = "UPDATE t_stat_sales SET f_buycoin=IFNULL(f_buycoin,0) + ?,$f_buycoin=? WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=1";
        $sql = "UPDATE t_stat_sales SET f_buycoin=IFNULL(f_buycoin,0) + ?,$f_buycoin=$f_buycoin+? WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0";
//G.Chin 2010-05-21 chg end
        $params =array($money,$money);
        return self::paramsUpdate($sql, $params);
    }

    /**
     *商品購入入金額の記録処理
     * @param <int> $pro_id 商品ID
     * @return <boolean> 成功:true
     */
    public static function buyproduct($pro_id,$isCoinPack) {
        $product=DB::uniqueObject("SELECT pro.f_min_price,`end`.f_end_price,end.f_address_flag FROM t_products_master pro
                            LEFT OUTER JOIN t_end_products `end` ON `end`.fk_end_products_id=pro.fk_products_id
                            WHERE pro.fk_products_id=?", array($pro_id));
        if(empty($product))
            return false;
        //$sql = "UPDATE t_stat_sales SET f_prd_lastprice=IFNULL(f_prd_lastprice,0) + ?,f_prd_cost=IFNULL(f_prd_cost,0) + ? WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0";
        //$params =array($product->f_end_price,$product->f_min_price);
        $coinSql="";
        if($isCoinPack)
            $coinSql=",f_package_sales=IFNULL(f_package_sales,0)+".$product->f_end_price;
        $sql = "UPDATE t_stat_sales SET f_prd_lastprice=IFNULL(f_prd_lastprice,0) + ? $coinSql WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0";
        if($product->f_address_flag==1)
        {
            $params =array($product->f_end_price);
        }
        else if($product->f_address_flag==0)
        {
            $params =array($product->f_end_price+CARRIAGE);
        }
        return self::paramsUpdate($sql, $params);
    }

    /**
     *入札の記録処理
     * @param <type> $acTypeNo 入札された商品のt_auction_type.f_statistics_no
     * @param <type> $buy 入札で消費された有料コイン数
     * @param <type> $free 入札で消費されたフリーコイン数
     * @return <boolean> 成功:true
     */
    public static function bid($acTypeNo,$buy,$free){
        $f_auctype = "f_auctype".$acTypeNo;
        $f_sauctype = "f_sauctype".$acTypeNo;
        $sql = "update t_stat_sales set f_coin_use=IFNULL(f_coin_use,0)+?,f_scoin_use=IFNULL(f_scoin_use,0)+?,
                f_bid_hand=IFNULL(f_bid_hand,0)+?,$f_auctype=IFNULL($f_auctype,0)+?,$f_sauctype=IFNULL($f_sauctype,0)+?
                WHERE WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0";
        $params =array($buy,$free,($buy+$free),$buy,$free);
        return self::paramsUpdate($sql, $params);
    }


    private static function paramsUpdate($sql,$params=array()) {
        return self::saleUpdate(null,null,$sql,$params);
    }

    private static function columnUpdate($column,$value) {
        return self::saleUpdate($column,$value);
    }

    public static function freeCoin($val)
    {
        $obj = DB::uniqueObject(self::getRowLockCoin());
        $count=0;
        $sql="UPDATE t_stat_coin SET f_scoin_gen=f_scoin_gen+$val WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0";
        if(!empty($obj)) {
            $count=DB::executeNonQuery($sql);
        }
        else {
            //存在しない
            try {
                $sql="INSERT INTO t_stat_coin (f_stat_dt,f_type,f_scoin_gen) VALUES (date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),0,$val)";
                $count=DB::executeNonQuery($sql);
            }
            catch(Exception $e) {
                throw $e;
            }
        }
        return $count>0 ? true :false;
    }

    private static function saleUpdate($column="",$value="",$sql="",$params=array()) {
        $obj = DB::uniqueObject(self::getRowLock());
        $count=0;
        if(empty ($sql)) {
            $sql = self::getUpdate($column);
            $params=array($value);
        }
        if(!empty($obj)) {
            //存在
            $count=DB::executeNonQuery($sql, $params);
        }
        else {
            //存在しない
            try {
                $count=DB::executeNonQuery(self::getInsert());
                $count+=DB::executeNonQuery($sql, $params);
            }
            catch(Exception $e) {
                //insert失敗、update処理
                try {
                    $count=DB::executeNonQuery($sql, $params);
                }
                catch(Exception $ex) {
                    throw $ex;
                }
            }
        }
        return $count>0 ? true :false;
    }

//G.Chin 2010-11-19 add sta
    public static function register2($f_sex) {
		if($f_sex == 0)
		{
			return self::columnUpdate2("f_memreg",1);
		}
		else
		{
			return self::columnUpdate2("f_memreg_woman",1);
		}
    }

    private static function columnUpdate2($column,$value) {
        return self::saleUpdate2($column,$value);
    }

    private static function saleUpdate2($column="",$value="") {
        $obj = DB::uniqueObject(self::getRowLock());
        $count=0;
        if(empty ($sql)) {
            $sql = self::getUpdate($column);
            $params=array($value);
        }
        if(!empty($obj)) {
            //存在
            $count=DB::executeNonQuery($sql, $params);
        }
        else {
            //存在しない
            try {
                $count=DB::executeNonQuery(self::getInsert());
                $count+=DB::executeNonQuery($sql, $params);
            }
            catch(Exception $e) {
                //insert失敗、update処理
                try {
                    $count=DB::executeNonQuery($sql, $params);
                }
                catch(Exception $ex) {
                    throw $ex;
                }
            }
        }
        return $count>0 ? true :false;
    }
//G.Chin 2010-11-19 add end

    private static function getRowLock() {
        return "SELECT f_stat_dt,f_type FROM t_stat_sales WHERE f_stat_dt=DATE_FORMAT(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0 FOR UPDATE";
    }
    private static function getRowLockCoin() {
        return "SELECT f_stat_dt,f_type FROM t_stat_coin WHERE f_stat_dt=DATE_FORMAT(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0 FOR UPDATE";
    }

    private static function getUpdate($column) {
        return "UPDATE t_stat_sales SET $column=IFNULL($column,0) + ? WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0";
    }

    private static function getInsert() {
        return "INSERT IGNORE INTO t_stat_sales (f_stat_dt,f_type) VALUES (date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),0)";
    }
}
?>
