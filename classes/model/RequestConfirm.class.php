<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.'.model.Model'); 
/**
 * Description of RequestConfirm
 *
 * @author ark
 */
class RequestConfirm extends Model {
    //put your code here

    /**
     *新しいリクエスト確認データを追加する
     * @param <int> $f_reqtype タイプ
     * @param <string> $f_serial 唯一のstring
     * @param <int> $f_timelimit 秒数
     */
    public function saveNewConfirm($f_reqtype,$f_serial,$f_timelimit) {
        try {
            DB::begin();
            $count = DB::result_rows("SELECT f_reqtype,f_serial,f_timelimit,f_updatetm FROM t_reqhist WHERE f_serial=?", array($f_serial));
            if($count==0)
                DB::executeNonQuery("INSERT INTO t_reqhist(f_reqtype,f_serial,f_timelimit,f_updatetm) VALUES (?,?,?,CURRENT_TIMESTAMP)",
                    array($f_reqtype,$f_serial,$f_timelimit));
            else
                DB::executeNonQuery("UPDATE t_reqhist SET f_updatetm=CURRENT_TIMESTAMP WHERE f_serial=? ",
                    array($f_serial));
            DB::commit();
        }
        catch(Exception $e) {
            DB::rollback();
        }
    }

    /**
     *指定された$f_serialのリクエスト確認データを取得する
     * @param <string> $f_serial
     * @return <object>
     */
    public function getRequestConfirmBySerial($f_serial) {
         return DB::uniqueObject("SELECT f_serial,f_timelimit,f_updatetm,CURRENT_TIMESTAMP AS sys_time FROM t_reqhist WHERE f_serial=?", array($f_serial));
    }

    /**
     *指定された$f_serialのリクエスト確認データを削除する
     * @param <string> $f_serial
     */
    public function deleteConfirm($f_serial){
        try {
            DB::begin();
            DB::executeNonQuery("DELETE FROM t_reqhist WHERE f_serial=?",array($f_serial));
            DB::commit();
        }
        catch(Exception $e) {
            DB::rollback();
        }
    }

    public function getQuestionCategory() {
         return DB::getObjectByQuery("SELECT fk_question_category_id,f_category_name from auction.t_question_category");
    }

    //質問内容をDBに登録
    public function setQuestion($category,$mem_id,$name,$mail,$title,$main)
    {
        $sql="";
        if($mem_id =="")
        {
            $sql ="INSERT INTO auction.t_question (fk_question_category_id,f_subject,f_main,f_name,fk_member_id,f_mail_address,
                   f_flag,f_recv_dt)VALUES('$category','$title','$main','$name',NULL,'$mail',0,now())";
        }
        else
        {
            $sql ="INSERT INTO auction.t_question (fk_question_category_id,f_subject,f_main,f_name,fk_member_id,f_mail_address,
                   f_flag,f_recv_dt)VALUES('$category','$title','$main','$name','$mem_id','$mail',0,now())";
        }
        //printf($sql);
        try
        {
            DB::begin();
            DB::executeNonQuery($sql);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
        }
    }

    public function setQuestionDB($mail,$title,$main)
    {
        $send_to=INFO_MAIL;
        $sql="";
        $send_from="FROM: ". $mail."\nRETURN-Path: $mail ";
        $sql = "insert into auction.t_mail_request(f_type,f_subject,f_body,f_sendfrom,f_sendto) values(20,'$title','$main','$send_from','$send_to')";
        //echo $sql;
        try
        {
            DB::begin();
            DB::executeNonQuery($sql);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
        }
    }
}
?>
