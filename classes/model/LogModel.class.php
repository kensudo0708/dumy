<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class LogModel{
    /*
     * @param string $file  filename  ファイル名 + YYYYMMDD
     * @param string $body  HH:mm:ss : body
     */

    public function write($file,$body)
    {
        $now_day = date("Ymd");
        $now_time = date("H:i:s");
        $LogFile = "/home/auction/log/$file$now_day.log";
        $writebody="$now_time : $body\n";

        $fp = fopen($LogFile, "a+");
        flock($fp, LOCK_EX);
        fputs($fp,$writebody);
        flock($fp, LOCK_UN);
        fclose($fp);
    }
}
?>
