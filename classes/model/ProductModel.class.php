<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.'.model.Model');
import('classes.repository.Setting');

/**
 * 商品に関連するDB処理
 *
 * @author mw
 */
class ProductModel extends Model {
    //put your code here

//    /**
//     *開催中商品の数を取得
//     * @return <type>
//     */
//    public function getAuctionProductCount() {
//        $result=DB::uniqueArray("SELECT count(fk_products_id) FROM t_products
//                        WHERE f_start_time <= CURRENT_TIMESTAMP");
//        return $result[0];
//    }

//    /**
//     *開催中商品の数を取得
//     * @return <type>
//     */
//    public function getAuctionProductCount() {
//        return DB::result_rows("SELECT product.fk_products_id
//                        FROM t_products product
//                        INNER JOIN t_products_master mast ON product.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type t ON mast.fk_auction_type_id=t.fk_auction_type_id
//                        WHERE t.f_break_flag=0 AND t.f_start_time <=CURRENT_TIMESTAMP AND
//                        t.f_stop_time> CURRENT_TIMESTAMP AND mast.f_start_time<=CURRENT_TIMESTAMP
//                        AND product.f_auction_time>0 AND mast.f_recmmend=0
//                        ORDER BY product.f_auction_time");
//    }

    /**
     *開催中商品の数を取得
     * @return <type>
     */
    public function getAuctionProductCount($member) {
        if(empty($member)) {
            $t_hummer_count=0;
        }
        else {
            $t_hummer_count=$member->t_hummer_count;
        }
//        $sql ="SELECT p.fk_products_id FROM
//                t_products p , t_products_master mast , t_auction_type `type`
//                WHERE p.fk_products_id=mast.fk_products_id AND mast.fk_auction_type_id=`type`.fk_auction_type_id
//                AND `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg <> 9
//                UNION ALL
//                SELECT p.fk_products_id
//                FROM t_products p ,t_products_master mast,t_auction_type `type`
//                WHERE mast.fk_auction_type_id=`type`.fk_auction_type_id
//                AND p.fk_products_id=mast.fk_products_id AND `type`.f_bidder_flag >0
//                AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5)
//                AND (`type`.f_bidder_param1 >='$t_hummer_count' OR `type`.f_bidder_param1 IS NULL) AND mast.f_delflg <> 9";

//        $sql ="SELECT p.fk_products_id FROM
//                t_products p , t_products_master mast , t_auction_type `type`
//                WHERE p.fk_products_id=mast.fk_products_id AND mast.fk_auction_type_id=`type`.fk_auction_type_id
//                AND ((`type`.f_bidder_flag = 0 ) OR (`type`.f_bidder_flag >0 AND `type`.f_bidder_param1 >='$t_hummer_count'))
//                AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5 OR p.f_status=6 OR p.f_status=7) AND mast.f_delflg <> 9";

        $sql ="SELECT p.fk_products_id FROM
                (SELECT fk_products_id FROM t_products WHERE (f_status=1 OR f_status=3 OR f_status=4 OR f_status=5 OR f_status=6 OR f_status=7) ) p , t_products_master mast , t_auction_type `type`
                WHERE p.fk_products_id=mast.fk_products_id AND mast.fk_auction_type_id=`type`.fk_auction_type_id
                AND ((`type`.f_bidder_flag = 0 ) OR (`type`.f_bidder_flag >0 AND `type`.f_bidder_param1 >='$t_hummer_count')) AND mast.f_delflg <> 9";

                //ECHO $sql."<BR>";
                return DB::result_rows($sql);
//        return DB::result_rows("SELECT p.fk_products_id FROM t_products p
//                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 or p.f_status=4 OR p.f_status=5)
//                and mast.f_delflg <> 9
//                UNION ALL
//                SELECT p.fk_products_id FROM t_products p
//                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 or p.f_status=4 OR p.f_status=5) AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL) and mast.f_delflg <> 9"
//                ,array($t_hummer_count));//AND mast.f_recmmend = 0
    }

    /**
     *カテゴリ指定で商品の数を取得する
     * @param <int> $cate_id カテゴリID
     * @return <int>
     */
    public function getCategoryProductCount($cate_id,$member) {
        if(empty($member)) {
            $t_hummer_count=0;
        }
        else {
            $t_hummer_count=$member->t_hummer_count;
        }

//G.Chin 2010-10-09 chg sta
/*
        return DB::result_rows("SELECT p.fk_products_id FROM t_products p
                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
                WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 or p.f_status=4 OR p.f_status=5) AND mast.fk_item_category_id=? and mast.f_delflg <> 9
                UNION ALL
                SELECT p.fk_products_id FROM t_products p
                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
                WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 or p.f_status=4 OR p.f_status=5) AND mast.fk_item_category_id=? AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL) and mast.f_delflg <> 9"
                ,array($cate_id,$cate_id,$t_hummer_count));
*/
				$sql  = "SELECT ";
				$sql .= "p.fk_products_id ";
				$sql .= "FROM t_products p,t_products_master mast,t_auction_type `type` ";
				$sql .= "WHERE ((`type`.f_bidder_flag = 0) or (`type`.f_bidder_flag >0)) ";
				$sql .= "AND (p.f_status=1 OR p.f_status=3 or p.f_status=4 OR p.f_status=5) ";
				$sql .= "AND mast.fk_item_category_id=$cate_id ";
				$sql .= "and mast.f_delflg <> 9 ";
				$sql .= "AND (`type`.f_bidder_param1 >=$t_hummer_count OR `type`.f_bidder_param1 IS NULL) ";
				$sql .= "and p.fk_products_id=mast.fk_products_id ";
				$sql .= "and mast.fk_auction_type_id=`type`.fk_auction_type_id ";
				return DB::result_rows($sql);
//G.Chin 2010-10-09 chg end
    }

    /**
     *開催中の商品を取得
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     * @return <type>
     */
    public function getProducts($s,$t,$member,$isPc=true) {
        $photo = $isPc ? "pm.f_photo1" : "pm.f_photo1mb";
        if(empty($member)) {
            $t_hummer_count=0;
        }
        else {
            $t_hummer_count=$member->t_hummer_count;
        }
        $sql ="
            SELECT pm.fk_products_id id,p.f_now_price price,
            (CASE WHEN (p.f_last_bidder_handle IS NULL OR p.f_last_bidder_handle='') THEN '".Msg::get("NOT_BIDDER")."' ELSE p.f_last_bidder_handle END) AS last_bidder,
            p.f_auction_time 'time' ,pm.f_products_name 'name' ,$photo image,
            pm.fk_auction_type_id auction_type,att.f_auction_name,pm.f_market_price  market_price
            FROM auction.t_products_master pm,
            (SELECT fk_products_id,f_last_bidder_handle ,f_auction_time,f_now_price
             FROM auction.t_products 
             WHERE (f_status=1 OR f_status=3 OR f_status=4 OR f_status=5 OR f_status=6 OR f_status=7 )
             ORDER BY f_auction_time ) p,
            auction.t_auction_type att
            WHERE pm.fk_products_id = p.fk_products_id
            AND pm.fk_auction_type_id = att.fk_auction_type_id
            AND (att.f_bidder_flag = 0 OR ( att.f_bidder_flag = 1 AND att.f_bidder_param1 >=?)) AND pm.f_delflg <> 9 ORDER BY p.f_auction_time
            LIMIT $s,$t";
            //echo $sql;
            $products=DB::getObjectByQuery($sql,array($t_hummer_count));
//        $products=DB::getObjectByQuery(
//                "SELECT   product.fk_products_id       AS id,
//                          product.f_now_price          AS price,
//                          (CASE WHEN product.f_last_bidder_handle IS NULL OR product.f_last_bidder_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE product.f_last_bidder_handle END)  AS last_bidder,
//                          product.f_auction_time	 AS `time`,
//                          product.f_products_name	       AS `name`,
//                $photo	       	       AS image,
//                          product.fk_auction_type_id         AS auction_type,
//                          product.f_auction_name,
//                          product.f_market_price       AS market_price
//                        FROM
//                        (
//                        SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name,mast.f_market_price
//                        FROM t_products p
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) and mast.f_delflg <> 9
//                        UNION ALL
//                        SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name,mast.f_market_price
//                        FROM t_products p
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//
//                        WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5)  AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL) and mast.f_delflg <> 9
//                        ) product
//                        ORDER BY f_auction_time
//                        LIMIT $s,$t",array($t_hummer_count));// AND mast.f_recmmend = 0
        return $products;
    }

    /**
     *開催中の商品を取得
     * @param <int> $cate_id カテゴリID
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     * @return <type>
     */
    public function getCategoryProducts($cate_id,$s,$t,$member,$isPc=true) {
        $photo = $isPc ? "f_photo1" : "f_photo1mb";
        if(empty($member)) {
            $t_hummer_count=0;
        }
        else {
            $t_hummer_count=$member->t_hummer_count;
        }

        $sql ="
            SELECT pm.fk_products_id id,p.f_now_price price,
            (CASE WHEN (p.f_last_bidder_handle IS NULL OR p.f_last_bidder_handle='') THEN '".Msg::get("NOT_BIDDER")."' ELSE p.f_last_bidder_handle END) AS last_bidder,
            p.f_auction_time 'time' ,pm.f_products_name 'name' ,$photo image,
            pm.fk_auction_type_id auction_type,att.f_auction_name,pm.f_market_price  market_price
            FROM auction.t_products_master pm,
            (SELECT fk_products_id,f_last_bidder_handle ,f_auction_time,f_now_price
             FROM auction.t_products
             WHERE (f_status=1 OR f_status=3 OR f_status=4 OR f_status=5 OR f_status=6 OR f_status=7 )
            ) p,
            auction.t_auction_type att
            WHERE pm.fk_products_id = p.fk_products_id
            AND pm.fk_auction_type_id = att.fk_auction_type_id
            AND (att.f_bidder_flag = 0 OR ( att.f_bidder_flag = 1 AND att.f_bidder_param1 >=$t_hummer_count)) AND pm.f_delflg <> 9
            AND pm.fk_item_category_id=$cate_id
            ORDER BY f_auction_time
            LIMIT $s,$t";
		$products=DB::getObjectByQuery($sql);
        return $products;
    }

    /**
     *商品のカテゴリのデータを取得する
     * @return <array> Category
     */
    public function getProductCategories() {
        return DB::getObjectByQuery("SELECT fk_item_category AS id,f_category_name AS `name` FROM t_item_category WHERE NOT fk_item_category=0 and f_status = 0 order by f_dspno");
    }


    /**
     * ピックアップ商品を取得する
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     */
    public function getPickupProducts($qty,$isPc=true) {
        $photo = $isPc ? "product.f_photo1" : "product.f_photo1mb";
        $sql="SELECT product.fk_end_products_id AS id,product.f_products_name AS `name`,product.f_end_price AS price,product.f_last_bidder AS last_bidder_id,
                product.f_end_time AS `time`,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,
                master.f_market_price AS market_price ,products.f_now_price AS now_price,`type`.f_spend_coin f_spend_coin
                FROM t_end_products product,t_member_master member ,t_auction_type `type` ,t_products_master `master`,t_products products
                WHERE member.fk_member_id=product.f_last_bidder
                AND `type`.fk_auction_type_id=product.fk_auction_type_id
                AND master.fk_products_id=product.fk_end_products_id
                AND products.fk_products_id=product.fk_end_products_id
                AND product.f_pickup_flg=1
                AND product.f_status <>9
                ORDER BY product.f_end_time DESC
                LIMIT 0,$qty";
        //echo $sql;
        return DB::getObjectByQuery($sql);
//        return DB::getObjectByQuery("SELECT product.fk_end_products_id AS id,product.f_products_name AS name,product.f_end_price AS price,
//                    product.f_end_time AS `time`,product.f_market_price AS market_price,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,products.f_now_price AS price
//
//                    FROM t_end_products product
//                    LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
//                    LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
//                    LEFT OUTER JOIN t_products products ON products.fk_products_id=product.fk_end_products_id
//                    WHERE product.f_pickup_flg=1 and product.f_status <>9
//                    ORDER BY product.f_end_time DESC
//                    LIMIT 0,$qty");
    }

    /**
     *カテゴリIDで商品のカテゴリのデータを取得する
     * @return <object> Category
     */
    public function getProductCategoriyById($cate_id) {
        return DB::uniqueObject("SELECT fk_item_category AS id,f_category_name AS `name`
                    FROM t_item_category WHERE fk_item_category=?",array($cate_id));
    }


    /**
     *Ajax通信で、商品ID(複数）で商品情報を取得
     * @param <type> $ids
     */
    public function getProductByIdArray($ids) {
//        $sql ="SELECT prod.id ,prod.price,prod.time,prod.market_price,mm.f_handle AS last_bidder FROM
//                (SELECT p.fk_products_id AS id ,p.f_now_price AS price ,p.f_last_bidder AS memid,p.f_last_bidder_handle AS handle,p.f_auction_time AS TIME,mast.f_market_price    AS market_price
//                FROM auction.t_products_master mast,auction.t_products p
//                WHERE mast.fk_products_id = p.fk_products_id
//                AND p.fk_products_id = $ids ) AS prod
//                LEFT JOIN
//                (SELECT fk_member_id ,f_handle FROM t_member_master) mm
//                ON mm.fk_member_id = prod.memid";

//G.Chin 2010-10-09 chg sta
/*
        $products=DB::getObjectByQuery(
//                "SELECT
//                          fk_products_id       AS id,
//                          FORMAT(f_now_price,0)          AS price,
//                          (CASE WHEN f_last_bidder_handle IS NULL OR f_last_bidder_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE f_last_bidder_handle END)  AS last_bidder,
//                          f_auction_time       AS `time`
//                        FROM t_products
//                        WHERE fk_products_id in ($ids) AND (f_status=1 OR f_status=3 OR f_status=4 OR f_status=5)");
//                   "SELECT
//                          p.fk_products_id       AS id,
//                          p.f_now_price          AS price,
//                          (CASE WHEN p.f_last_bidder_handle IS NULL OR p.f_last_bidder_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE p.f_last_bidder_handle END)  AS last_bidder,
//                          p.f_auction_time       AS `time`,
//                          mast.f_market_price    AS market_price
//                    FROM t_products p
//                    INNER JOIN t_products_master mast ON mast.fk_products_id=p.fk_products_id
//                    WHERE p.fk_products_id in ($ids) AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5)");


                                   "SELECT
                          p.fk_products_id       AS id,
                          p.f_now_price          AS price,
                          (CASE WHEN mm.f_handle IS NULL OR mm.f_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE mm.f_handle END)  AS last_bidder,
                          p.f_auction_time       AS `time`,
                          mast.f_market_price    AS market_price
                    FROM t_products p
                    INNER JOIN t_products_master mast ON mast.fk_products_id=p.fk_products_id
                    Left JOIN auction.t_member_master mm ON p.f_last_bidder = mm.fk_member_id
                WHERE (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5 OR p.f_status=7) and  p.fk_products_id in ($ids)");
                    //WHERE (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) and  p.fk_products_id in ($ids)");
*/
			$sql  = "SELECT ";
			$sql .= "p.fk_products_id       AS id,";
			$sql .= "p.f_now_price          AS price,";
			$sql .= "(CASE WHEN mm.f_handle IS NULL OR mm.f_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE mm.f_handle END)  AS last_bidder,";
			$sql .= "p.f_auction_time       AS `time`,";
                        //$sql .=" TIME_TO_SEC(TIMEDIFF(DATE_ADD(p.f_start_time,INTERVAL p.f_auction_time  SECOND ),NOW())) time, ";
                        $sql .= "mast.f_market_price    AS market_price ";
			$sql .= "FROM t_products_master mast,t_products p ";
                        $sql .= "LEFT OUTER JOIN t_member_master mm ON p.f_last_bidder = mm.fk_member_id ";
			$sql .= "WHERE mast.fk_products_id=p.fk_products_id ";
			$sql .= "and (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5 OR p.f_status=7) ";
			$sql .= "and  p.fk_products_id in ($ids)";
                        
			$products=DB::getObjectByQuery($sql);
//G.Chin 2010-10-09 chg end
        
//        if($products->last_bidder =="")
//        {
//            $products->last_bidder=Msg::get("NOT_BIDDER");
//        }
        return $products;
    }

    /**
     *商品詳細画面のAjax通信で、商品IDで商品情報を取得（ディスカウント含む）
     * @param <int> $ids
     */
    public function getProductDetailById($id) {
//        return DB::getObjectByQuery("SELECT p.fk_products_id,p.f_last_bidder_handle,p.f_auction_time,mast.f_market_price,p.f_now_price,f_status
//                            FROM t_products p
//                            INNER JOIN t_products_master mast ON mast.fk_products_id=p.fk_products_id
//                            WHERE p.fk_products_id = ?",array($id));//WHERE p.fk_products_id in ($id)
//G.Chin 2010-10-09 chg sta

        return DB::getObjectByQuery("SELECT p.fk_products_id,mm.f_handle as f_last_bidder_handle,p.f_auction_time,mast.f_market_price,p.f_now_price,p.f_status,p.t_r_status
                            FROM t_products p
                            INNER JOIN t_products_master mast ON mast.fk_products_id=p.fk_products_id
                            Left JOIN auction.t_member_master mm ON p.f_last_bidder = mm.fk_member_id
                            WHERE p.fk_products_id = ?",array($id));//WHERE p.fk_products_id in ($id)

//		$sql  = "SELECT ";
//		$sql .= "p.fk_products_id,mm.f_handle as f_last_bidder_handle,p.f_auction_time,mast.f_market_price,p.f_now_price,f_status ";
//		$sql .= "FROM t_products as p, t_products_master as mast, t_member_master as mm ";
//		$sql .= "WHERE p.fk_products_id = $id ";
//		$sql .= "and mast.fk_products_id=p.fk_products_id ";
//		$sql .= "and p.f_last_bidder = mm.fk_member_id ";
		return DB::getObjectByQuery($sql);
//G.Chin 2010-10-09 chg end


    }

    /**
     *Ajax通信で 商品IDで商品情報を取得
     * @param <type> $ids
     */
    public function getAjaxProductById($id) {
        $product=DB::uniqueObject(
                "SELECT
                          fk_products_id       AS id,
                          f_now_price          AS price,
                          f_last_bidder_handle AS last_bidder,
                          f_auction_time       AS `time`
                        FROM t_products
                        WHERE fk_products_id =$id");
        return $product;
    }

    //落札者の声を取得
    public function getBidderComment($id) {
        return DB::uniqueObject("select end.f_memo,end.f_end_price
                                        from t_products_master mast,t_end_products end
                                        where end.f_memo IS NOT NULL
                                        and mast.fk_products_template_id ='$id'
                                        and mast.fk_products_id = end.fk_end_products_id
                                        order by end.f_end_time desc limit 0,1
                ");
    }

    /**
     *商品情報を取得　（t_productsから)(終了商品を除く）
     * @param <type> $id
     * @return <type>
     */
    public function getProductByID($id) {
        return DB::uniqueObject("select pro.fk_products_id,
                                        pro.f_bit_buy_count,pro.f_bit_free_count,pro.f_now_price,pro.f_last_bidder,pro.f_last_free_flg,
                                        pro.f_last_bidder_handle,pro.f_auction_time,pro.t_r_status,pro.f_status,mast.fk_products_template_id
                                        from t_products pro,t_products_master mast where pro.fk_products_id=mast.fk_products_id and (pro.f_status=1 or pro.f_status=3 or pro.f_status=4 or pro.f_status=5) and
                                        pro.fk_products_id=?",array($id));
    }

    /** *******************ログかけるパターン*****************************
     *商品情報を取得　（t_productsから)(終了商品を除く）
     * @param <type> $id
     * @return <type>
     */
    public function getLockProductByID($id) {
        return DB::uniqueObject("select pro.fk_products_id,
                                        pro.f_bit_buy_count,pro.f_bit_free_count,pro.f_now_price,pro.f_last_bidder,pro.f_last_free_flg,
                                        pro.f_last_bidder_handle,pro.f_auction_time,pro.t_r_status,pro.f_status,mast.fk_products_template_id
                                        from t_products pro,t_products_master mast where pro.fk_products_id=mast.fk_products_id and (pro.f_status=1 or pro.f_status=5 or pro.f_status=7 or pro.f_status=3) and
                                        pro.fk_products_id=? for update",array($id));
    }

    /**
     *商品情報を取得　（商品詳細ページ用)(終了商品を含む）
     * @param <type> $id
     * @return <type>
     */
    public function getDetailProductById($id) {
        return DB::uniqueObject("select pro.fk_products_id,
                                        pro.f_bit_buy_count,pro.f_bit_free_count,pro.f_now_price,pro.f_last_bidder,
                                        pro.f_last_bidder_handle,pro.f_auction_time,pro.t_r_status,pro.f_status,mast.fk_products_template_id
                                        from t_products pro,t_products_master mast where pro.fk_products_id=mast.fk_products_id and mast.f_delflg<>9 and
                                        pro.fk_products_id=?",array($id));
    }

    public function updateProduct($id,$price,$hour,$minute,$second,$hit) {
        DB::executeNonQuery("update product set price=?,hour=?,minute=?,second=?,hit=?
                            where id=? and sell_flag='0'",array($price,$hour,$minute,$second,$hit,$id));
    }

    public function updateProductTime($id,$hour,$minute,$second) {
        DB::executeNonQuery("update product set hour=?,minute=?,second=?
                            where id=? and sell_flag='0'",array($hour,$minute,$second,$id));
    }

    public function getAuctionProducts() {
        $products=DB::getObjectByQuery("select `fk_products_id`,`f_bit_buy_count`,`f_bit_free_count`,`f_now_price`,`f_last_bidder`,
                                        `f_last_bidder_handle`,`f_auction_time`,`t_r_status`,`f_status`,`f_session`
                                        from t_products where sell_flag='0'");
        return $products;
    }

    /**
     *開催中の商品情報を取得（マスタ、カテゴリ、オークションの情報を含む）(キャッシュするため）
     * @return <array> 商品情報Objectの配列
     */
    public function getAuctionProductInfo() {
//G.Chin 2010-10-09 chg sta
/*
        $products=DB::getObjectByQuery(
                "SELECT mast.*,product.f_last_bidder_handle,category.f_category_name,t.f_auction_name,t.f_spend_coin,t.f_bidder_flag,t.f_auto_flag,
            t.f_rev_flag,t.f_break_flag,f_up_price,t.f_down_price,t.f_start_time,t.f_stop_time FROM t_products_master mast
            INNER JOIN t_auction_type t ON mast.fk_auction_type_id=t.fk_auction_type_id
            INNER JOIN t_products product ON product.fk_products_id=mast.fk_products_id
            LEFT OUTER JOIN t_item_category category ON category.fk_item_category=mast.fk_item_category_id
            WHERE product.f_status=1 OR product.f_status=3 OR product.f_status=4 OR product.f_status=5
            ORDER BY product.fk_products_id
            ");
*/
		$sql  = "SELECT ";
		$sql .= "mast.*,product.f_last_bidder_handle,category.f_category_name,t.f_auction_name,t.f_spend_coin,t.f_bidder_flag,t.f_auto_flag,";
		$sql .= "t.f_rev_flag,t.f_break_flag,f_up_price,t.f_down_price,t.f_start_time,t.f_stop_time ";
		$sql .= "FROM t_products_master mast,t_products product,t_auction_type t,t_item_category category ";
		$sql .= "WHERE (product.f_status=1 OR product.f_status=3 OR product.f_status=4 OR product.f_status=5) ";
		$sql .= "and mast.fk_auction_type_id=t.fk_auction_type_id ";
		$sql .= "and product.fk_products_id=mast.fk_products_id ";
		$sql .= "and category.fk_item_category=mast.fk_item_category_id ";
		$sql .= "ORDER BY product.fk_products_id ";
		$products=DB::getObjectByQuery($sql);
//G.Chin 2010-10-09 chg end
        return $products;
    }

    /**
     *指定されたIDで開催中の商品情報を取得（マスタとオークションの情報を含む）
     * @return <object> 商品情報Object
     */
    public function getAuctionProductById($id,$isPc=true) {
        if($isPc)
        {
            $tmp ="`mast`.`f_photo1`,`mast`.`f_photo2`,`mast`.`f_photo3`,`mast`.`f_photo4`,`mast`.`f_photo5`,`mast`.`f_top_description_pc`,`mast`.`f_main_description_pc`,";
        }
        else
        {
            $tmp ="`mast`.`f_photo1mb`,`mast`.`f_photo2mb`,`mast`.`f_photo3mb`,`mast`.`f_photo4mb`,`mast`.`f_photo5mb`,`mast`.`f_top_description_mb`,`mast`.`f_main_description_mb`,";
        }

        $sql = "SELECT `mast`.`fk_products_id`,`mast`.`f_products_name`,`mast`.`fk_item_category_id`,".$tmp."
            `mast`.`f_start_price`,`mast`.`f_min_price`,`mast`.`f_r_min_price`,`mast`.`f_r_max_price`,mast.`fk_auction_type_id` AS auction_type,
            `mast`.`f_market_price`,`mast`.`f_start_time`,`mast`.`fk_auction_type_id`,`mast`.`f_plus_coins`,
            `mast`.`f_recmmend`,`mast`.`f_regdate`,`mast`.`fk_auction_type_id`,product.f_last_bidder_handle,category.f_category_name,
            t.f_auction_name,t.f_spend_coin,t.f_bidder_flag,t.f_auto_flag,mast.fk_products_template_id,
            t.f_rev_flag,t.f_break_flag,f_up_price,t.f_down_price,t.f_start_time,t.f_stop_time,product.t_r_status
            FROM t_products_master mast,t_auction_type t,t_products product,t_item_category category 
            WHERE mast.fk_auction_type_id=t.fk_auction_type_id and product.fk_products_id=mast.fk_products_id and
            category.fk_item_category=mast.fk_item_category_id and (product.f_status=1 OR product.f_status=3 OR product.f_status=4 OR product.f_status=5 or product.f_status=6 or product.f_status=7) 
            and product.fk_products_id=?";
        return DB::uniqueObject($sql,array($id));
//        return DB::uniqueObject(
//                "SELECT `mast`.`fk_products_id`,`mast`.`f_products_name`,`mast`.`fk_item_category_id`,
//            `mast`.`f_photo1`,`mast`.`f_photo2`,`mast`.`f_photo3`,`mast`.`f_photo4`,`mast`.`f_photo5`,
//            `mast`.`f_photo1mb`,`mast`.`f_photo2mb`,`mast`.`f_photo3mb`,`mast`.`f_photo4mb`,
//            `mast`.`f_photo5mb`,`mast`.`f_top_description_pc`,`mast`.`f_main_description_pc`,
//            `mast`.`f_top_description_mb`,`mast`.`f_main_description_mb`,`mast`.`f_start_price`,
//            `mast`.`f_min_price`,`mast`.`f_r_min_price`,`mast`.`f_r_max_price`,mast.`fk_auction_type_id` AS auction_type,
//            `mast`.`f_market_price`,`mast`.`f_start_time`,`mast`.`fk_auction_type_id`,`mast`.`f_plus_coins`,
//            `mast`.`f_address_flag`,`mast`.`f_delflg`,`mast`.`f_memo`,`mast`.`f_target_hard`,
//            `mast`.`f_recmmend`,`mast`.`f_regdate`,`mast`.`fk_auction_type_id`,product.f_last_bidder_handle,category.f_category_name,
//            t.f_auction_name,t.f_spend_coin,t.f_bidder_flag,t.f_auto_flag,mast.fk_products_template_id,
//            t.f_rev_flag,t.f_break_flag,f_up_price,t.f_down_price,t.f_start_time,t.f_stop_time FROM t_products_master mast
//            INNER JOIN t_auction_type t ON mast.fk_auction_type_id=t.fk_auction_type_id
//            INNER JOIN t_products product ON product.fk_products_id=mast.fk_products_id
//            LEFT OUTER JOIN t_item_category category ON category.fk_item_category=mast.fk_item_category_id
//            WHERE (product.f_status=1 OR product.f_status=3 OR product.f_status=4 OR product.f_status=5) AND product.fk_products_id=?"
//                ,array($id));
    }

    /**
     *指定されたIDで商品マスター情報を取得する
     * @param <int> $id 商品ID
     * @return <object> 商品マスター情報
     */
    public function getProductMasterById($id) {
        return DB::uniqueObject("SELECT `fk_products_id`,`f_products_name`,`fk_item_category_id`,`f_photo1`,`f_photo2`,
                        `f_photo3`,`f_photo4`,`f_photo5`,`f_photo1mb`,`f_photo2mb`,`f_photo3mb`,`f_photo4mb`,
                        `f_photo5mb`,`f_top_description_pc`,`f_main_description_pc`,`f_top_description_mb`,
                        `f_main_description_mb`,`f_start_price`,`f_min_price`,`f_r_min_price`,`f_r_max_price`,
                        `f_market_price`,`f_start_time`,`fk_auction_type_id`,`f_plus_coins`,`f_address_flag`,
                        `f_delflg`,`f_memo`,`f_target_hard`,`f_recmmend`,`f_regdate`
                        FROM t_products_master WHERE fk_products_id=?",array($id));
    }

    /**
     *商品情報の削除処理
     * @param <int> $id 商品ID
     */
    public function removeProduct($id) {
        try {
            DB::begin();
            //マスター情報を削除
            DB::executeNonQuery("DELETE FROM t_products_master WHERE fk_products_id=? ", array($id));
            DB::executeNonQuery("DELETE FROM t_products WHERE fk_products_id=? ", array($id));
            DB::commit();
            return true;
        }
        catch (Exception $e) {
            Logger::ERROR("ID[$id]のproductsデータが削除できませんでした。");
            DB::rollback();
            return FALSE;
        }
    }

    /**
     *すべてのオークションタイプを取得
     * @return <object配列>
     */
    public function getAuctionTypes() {
        return DB::getObjectByQuery("SELECT `fk_auction_type_id`,`f_auction_name`,`f_spend_coin`,`f_bidder_flag`,`f_bidder_param1`,
                        `f_bidder_param2`,`f_auto_flag`,`f_rev_flag`,`f_break_flag`,`f_up_price`,`f_down_price`,
                        `f_start_time`,`f_stop_time`,`f_statistics_no` FROM t_auction_type");
    }

    /**
     *指定されたIDでオークションタイプを取得
     * @param <int> $id タイプID
     * @return <object>
     */
    public static function getAuctionTypeById($id) {
        return DB::uniqueObject("SELECT `fk_auction_type_id`,`f_auction_name`,`f_spend_coin`,`f_bidder_flag`,`f_bidder_param1`,
                            `f_bidder_param2`,`f_auto_flag`,`f_rev_flag`,`f_break_flag`,`f_up_price`,`f_down_price`,
                            `f_start_time`,`f_stop_time`,`f_statistics_no`
                            FROM t_auction_type where fk_auction_type_id=?",array($id));
    }


    /**
     *入札処理
     * @param <type> $member
     * @param <type> $product
     * @return <type>
     */
    public static function bidProcess($member,$product) {
        $rows=0;
        $result = false;
//        $max=Utils::getInt(BID_TRY_TIMES,4);
//        $wait=Utils::getInt(BID_TRY_WAIT,500)*1000;
//        for($i=0;$i<$max;$i++) {
            try {
                DB::begin();
                //商品情報を更新
                $rows+=DB::executeNonQuery("UPDATE t_products SET f_bit_buy_count=?,f_bit_free_count=?,f_now_price=?,f_last_bidder=?,f_last_bidder_handle=?,f_auction_time=?,
                                    f_tm_stamp=CURRENT_TIMESTAMP,f_last_free_flg = ? WHERE fk_products_id=? AND f_status=1",
                        array($product->f_bit_buy_count,$product->f_bit_free_count,$product->f_now_price,
                        $product->f_last_bidder,$product->f_last_bidder_handle,
                        $product->f_auction_time,$product->f_last_free_flg,$product->fk_products_id));

                //会員情報を更新
                $memUpdateSql="";
                if(BID_FREE_MODE != 0 && $product->f_spend_coin== 0)
                    $memUpdateSql="UPDATE t_member SET f_coin=?,f_free_coin=?,t_contact_count=?,f_tm_stamp=CURRENT_TIMESTAMP
                                        WHERE fk_member_id=?";
                else
                    $memUpdateSql="UPDATE t_member SET f_coin=?,f_free_coin=?,t_contact_count=?,f_tm_stamp=CURRENT_TIMESTAMP
                                    WHERE fk_member_id=? and (f_coin>0 OR f_free_coin>0)";
                $rows+=DB::executeNonQuery($memUpdateSql, array($member->f_coin,$member->f_free_coin,$member->t_contact_count,$member->fk_member_id));

                //bidログを追加
                $rows+=DB::executeNonQuery("INSERT INTO t_bid_log(f_products_id,fk_member_id,f_price,f_bid_type,f_coin_type,f_tm_stamp)
                                VALUES (?,?,?,?,?,CURRENT_TIMESTAMP)",
                        array($product->fk_products_id,$product->f_last_bidder,$product->f_now_price,$product->f_bid_type,$product->f_coin_type));
                if($product->f_coin_type==0) {
                    //フリーコインログ処理（必要か？）
                }

                if($rows==3){
                    DB::commit();
                    $result= true;
                }
                else
//                $rows=0;
                    DB::rollback();
//                usleep($wait);
            }
            catch (Exception $e) {
//            Logger::ERROR("ID[$id]のproductsデータが削除できませんでした。");
//                Logger::warn("[Message]:".sprintf(Msg::get("BID_ERROR_LOG"),$member->fk_member_id,$product->fk_products_id)."--".$e->getMessage()."\n\t[Trace]:".$e->getTraceAsString());
                DB::rollback();
//                $rows=0;
//                usleep($wait);
            }
//        }
        return $result;
    }


    /**
     * IDで終了商品を取得
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     */
    public function getEndProductById($id) {
        //echo "getEndProductById<BR>"
        $product= DB::uniqueObject("SELECT product.`fk_end_products_id`,product.`f_products_name`,product.`fk_item_category_id`,
                                    product.`f_photo1`,product.`f_photo2`,product.`f_photo3`,product.`f_photo4`,product.`f_photo5`,
                                    product.`f_photo1mb`,product.`f_photo2mb`,product.`f_photo3mb`,product.`f_photo4mb`,
                                    product.`f_photo5mb`,product.`f_top_description_pc`,product.`f_main_description_pc`,product.`f_top_description_mb`,
                                    product.`f_main_description_mb`,product.`f_start_price`,product.`f_end_price`,
                                    product.`f_market_price`,product.`f_start_time`,product.`f_end_time`,product.`fk_auction_type_id`,product.`fk_auction_type_id` AS auction_type,
                                    product.`f_bit_buy_count`,product.`f_bit_free_count`,product.`f_target_hard`,
                                    product.`fk_address_id`,product.`f_status`,product.`f_stock`,product.`f_memo`,bid.bidcoins,
                                    product.`f_last_bidder`,product.`f_pickup_flg`,member.f_handle,`type`.f_auction_name,`type`.f_spend_coin,category.f_category_name
                                        FROM t_end_products product
                                        LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
                                        LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
                                        LEFT OUTER JOIN t_item_category category ON category.fk_item_category=product.fk_item_category_id
                                        LEFT OUTER JOIN
                                        (SELECT COUNT(fk_member_id) AS bidcoins,fk_member_id,f_products_id FROM t_bid_log WHERE f_products_id=$id GROUP BY  fk_member_id,f_products_id) bid ON bid.f_products_id=product.fk_end_products_id and bid.fk_member_id=product.f_last_bidder
                                        WHERE product.fk_end_products_id=? ",array($id));


//        $product->bidCoins="bidcoins";
        return $product;
    }

    public function GetEndProdCoin($pid,$mid)
    {
        $sql="SELECT COUNT(fk_member_id) AS bidcoins 
            FROM t_bid_log WHERE f_products_id=$pid and fk_member_id=$mid ";
        //echo $sql;
        return DB::uniqueObject($sql);
    }
    /**
     *index画面に表示する終了商品をしゅとく
     * @param <int> $qty 取得する商品の数
     */
    public function getEndProductsIndex($qty,$isPc=true) {
        $photo = $isPc ? "product.f_photo1" : "product.f_photo1mb";
        //echo "getEndProductsIndex<BR>";
        if(DISPLAY_END_PROD ==0)
        {
//            $sql="SELECT product.fk_end_products_id AS id, `master`.f_products_name AS `name`,product.f_end_price AS price,product.f_last_bidder AS last_bidder_id,
//                product.f_end_time AS `time`,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,
//                master.f_market_price AS market_price ,products.f_now_price AS now_price,`type`.f_spend_coin f_spend_coin
//                FROM t_end_products product,t_member_master member ,t_auction_type `type` ,t_products_master `master`,t_products products
//                WHERE member.fk_member_id=product.f_last_bidder
//                AND `type`.fk_auction_type_id=product.fk_auction_type_id
//                AND master.fk_products_id=product.fk_end_products_id
//                AND products.fk_products_id=product.fk_end_products_id
//                AND product.f_pickup_flg<>1
//                AND product.f_status <>9
//                AND products.f_status =2
//                ORDER BY product.f_end_time DESC
            $sql ="SELECT product.fk_end_products_id AS id, `product`.f_products_name AS `name`,product.f_end_price AS price,product.f_last_bidder AS last_bidder_id,
                product.f_end_time AS `time`,$photo AS image
                FROM t_end_products product
                WHERE product.f_pickup_flg<>1
                AND product.f_status <>9
                ORDER BY product.f_end_time DESC
                LIMIT 0,$qty";
            //echo $sql."<BR>";
            return DB::getObjectByQuery($sql);
//            return DB::getObjectByQuery("SELECT product.fk_end_products_id AS id,product.f_products_name AS name,product.f_end_price AS price,
//                    product.f_end_time AS time,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,
//                    master.f_market_price AS market_price ,products.f_now_price AS now_price
//                    FROM t_end_products product
//                    LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
//                    LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
//                    LEFT OUTER JOIN t_products_master master ON master.fk_products_id=product.fk_end_products_id
//                    LEFT OUTER JOIN  t_products products ON products.fk_products_id=product.fk_end_products_id
//                    WHERE NOT product.f_pickup_flg=1
//                    and product.f_status <>9
//                    ORDER BY product.f_end_time DESC
//                    LIMIT 0,$qty");
        }
        else if(DISPLAY_END_PROD > 0)
        {
            
//            $sql="SELECT product.fk_end_products_id AS id,product.f_products_name AS `name`,product.f_end_price AS price,product.f_last_bidder AS last_bidder_id,
//                product.f_end_time AS `time`,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,
//                master.f_market_price AS market_price ,products.f_now_price AS now_price,`type`.f_spend_coin f_spend_coin
//                FROM t_end_products product,t_member_master member ,t_auction_type `type` ,t_products_master `master`,t_products products
//                WHERE member.fk_member_id=product.f_last_bidder
//                AND `type`.fk_auction_type_id=product.fk_auction_type_id
//                AND master.fk_products_id=product.fk_end_products_id
//                AND products.fk_products_id=product.fk_end_products_id
//                AND product.f_pickup_flg<>1
//                AND product.f_status <>9
//                AND products.f_status =2
//                and product.f_end_time > date_sub(now(),interval ".DISPLAY_END_PROD." day)
//                ORDER BY product.f_end_time DESC
            $sql ="SELECT product.fk_end_products_id AS id, `product`.f_products_name AS `name`,product.f_end_price AS price,product.f_last_bidder AS last_bidder_id,
                product.f_end_time AS `time`,$photo AS image
                FROM t_end_products product
                WHERE product.f_pickup_flg<>1
                AND product.f_status <>9
                and product.f_end_time > date_sub(now(),interval ".DISPLAY_END_PROD." day)
                ORDER BY product.f_end_time DESC
                LIMIT 0,$qty";

            //echo $sql."<BR>";
            return DB::getObjectByQuery($sql);

//            return DB::getObjectByQuery("SELECT product.fk_end_products_id AS id,product.f_products_name AS name,product.f_end_price AS price,
//                    product.f_end_time AS time,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,
//                    master.f_market_price AS market_price ,products.f_now_price AS now_price
//                    FROM t_end_products product
//                    LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
//                    LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
//                    LEFT OUTER JOIN t_products_master master ON master.fk_products_id=product.fk_end_products_id
//                    LEFT OUTER JOIN  t_products products ON products.fk_products_id=product.fk_end_products_id
//                    WHERE NOT product.f_pickup_flg=1
//                    and product.f_status <>9
//                    and product.f_end_time > date_sub(now(),interval ".DISPLAY_END_PROD." day)
//                    ORDER BY product.f_end_time DESC
//                    LIMIT 0,$qty");

        }
    }

    public function GetEndProdDate($id)
    {
        $sql="SELECT att.f_auction_name,att.fk_auction_type_id ,att.f_spend_coin ,pm.f_market_price 
                FROM auction.t_auction_type att,auction.t_products_master pm
                WHERE pm.fk_products_id =  $id 
                AND pm.fk_auction_type_id = att.fk_auction_type_id";
        //echo $sql."<BR>";
        return DB::uniqueObject($sql);
    }

    public function GetEndBidderDate($id)
    {
        $sql="SELECT f_handle FROM t_member_master WHERE  fk_member_id = $id";
        return DB::uniqueObject($sql);
    }

    /**
     * 終了商品を取得
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     */
    public function getEndProducts($s,$t,$isPc=true) {
        $photo = $isPc ? "product.f_photo1" : "product.f_photo1mb";
        if(DISPLAY_END_PROD ==0)
        {
            return DB::getObjectByQuery("SELECT product.fk_end_products_id AS id,product.f_products_name AS name,product.f_end_price AS price,
                    product.f_end_time AS `time`,product.f_market_price AS market_price,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,products.f_now_price AS now_price
                    FROM t_end_products product
                    LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
                    LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
                    LEFT OUTER JOIN t_products products ON products.fk_products_id=product.fk_end_products_id
                    where product.f_status <>9
                    ORDER BY product.f_end_time DESC
                    LIMIT $s,$t");
        }
        else if(DISPLAY_END_PROD > 0)
        {
            return DB::getObjectByQuery("SELECT product.fk_end_products_id AS id,product.f_products_name AS name,product.f_end_price AS price,
                    product.f_end_time AS `time`,product.f_market_price AS market_price,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name,products.f_now_price AS now_price
                    FROM t_end_products product
                    LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
                    LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
                    LEFT OUTER JOIN t_products products ON products.fk_products_id=product.fk_end_products_id
                    where product.f_status <>9
                    and product.f_end_time > date_sub(now(),interval ".DISPLAY_END_PROD." day) 
                    ORDER BY product.f_end_time DESC
                    LIMIT $s,$t");
        }
    }

    /**
     *index画面の注目商品を取得
     * @param <int> $qty 取得する商品の数
     * @return <type>
     */
    public function getNoticeProduct($qty,$member,$isPc=true) {
        $photo = $isPc ? "f_photo1" : "f_photo1mb";
        if(empty($member)) {
            $t_hummer_count=0;
        }
        else {
            $t_hummer_count=$member->t_hummer_count;
        }
        $sql ="
            SELECT pm.fk_products_id id,p.f_now_price price,
            (CASE WHEN (p.f_last_bidder_handle IS NULL OR p.f_last_bidder_handle='') THEN '".Msg::get("NOT_BIDDER")."' ELSE p.f_last_bidder_handle END) AS last_bidder,
            p.f_auction_time 'time' ,pm.f_products_name 'name' ,$photo image,
            pm.fk_auction_type_id auction_type,att.f_auction_name,pm.f_market_price  market_price
            FROM auction.t_products_master pm,auction.t_products p,auction.t_auction_type att
            WHERE pm.fk_products_id = p.fk_products_id
            AND pm.fk_auction_type_id = att.fk_auction_type_id
            AND (att.f_bidder_flag = 0 OR ( att.f_bidder_flag = 1 AND att.f_bidder_param1 >=?)) AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5 OR p.f_status=6 OR p.f_status=7 ) AND pm.f_delflg <> 9 AND pm.f_recmmend = 1
            ORDER BY f_auction_time LIMIT 0,$qty";
            $products=DB::getObjectByQuery($sql,array($t_hummer_count));
//        $products=DB::getObjectByQuery(
//                "SELECT fk_products_id       AS id,
//                          f_now_price          AS price,
//                          (CASE WHEN f_last_bidder_handle IS NULL OR f_last_bidder_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE f_last_bidder_handle END)  AS last_bidder,
//                          f_auction_time	 AS `time`,
//                          f_products_name	       AS `name`,
//                $photo	       	       AS image,
//                          fk_auction_type_id         AS auction_type,
//                          f_auction_name,
//                          f_market_price         AS  market_price
//                        FROM
//                        (
//                        SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name,mast.f_market_price
//                        FROM t_products p
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_recmmend = 1 and mast.f_delflg <> 9
//                        UNION ALL
//                        SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name,mast.f_market_price
//                        FROM t_products p
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5)  AND mast.f_recmmend = 1 AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL) and mast.f_delflg <> 9
//                        ) product
//                        ORDER BY f_auction_time
//                        LIMIT 0,$qty",array($t_hummer_count));
        return $products;
    }

    /**
     *終了商品の数を取得
     * @return <int>
     */
    public function getEndProductCount() {
        if(DISPLAY_END_PROD ==0)
        {
            return DB::result_rows("SELECT fk_end_products_id FROM t_end_products where f_status <> 9 ORDER BY f_end_time desc");
        }
        else if(DISPLAY_END_PROD >0)
        {
            return DB::result_rows("SELECT fk_end_products_id FROM t_end_products where f_status <> 9 and f_end_time > date_sub(now(),interval ".DISPLAY_END_PROD." day)  ORDER BY f_end_time desc");
        }
    }

    /**
     *注目商品の数を取得
     * @return <int>
     */
    public function getNoticeProductCount() {
        return DB::result_rows("SELECT fk_products_id FROM t_products_master WHERE f_recmmend=1");
    }

    /**
     *自動入札設定するため、商品情報を取得
     * @param <int> $id 商品ID
     * @return <object>
     */
    public function getProductForSetAutoBid($id) {
        return DB::uniqueObject("SELECT product.fk_products_id,product.f_bit_buy_count,product.f_bit_free_count,product.t_r_status,
                            t.f_spend_coin,t.f_auto_flag,t.f_rev_flag,t.f_break_flag,mast.f_market_price 
                            FROM t_products_master mast
                                INNER JOIN t_auction_type t ON mast.fk_auction_type_id=t.fk_auction_type_id
                                INNER JOIN t_products product ON product.fk_products_id=mast.fk_products_id
                                WHERE (product.f_status=1 OR product.f_status=3 OR product.f_status=4 OR product.f_status=5) AND product.fk_products_id=?", array($id));
    }


    /**
     *リバスオークションの場合、上昇、マイナスのステータス更新
     * @param <type> $product_id
     * @param <type> $status
     */
    public function updateRevStatus($product_id,$status) {
        try {
            DB::begin();
            DB::executeNonQuery("UPDATE t_products SET t_r_status=? WHERE fk_products_id =? ", array($status,$product_id));
            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
        }
    }


    /**
     *入札より、商品価格の更新処理
     * @param <object> $product 商品の全情報
     */
    public function updateProductPrice(&$product) {
        //商品価格更新
        $product->f_up_price = Utils::getInt($product->f_up_price,0);
        $product->f_down_price = Utils::getInt($product->f_down_price,0);
        //リバースの場合
        if($product->t_r_status == 0) {
            //上昇中
            if( $product->f_r_max_price < ($product->f_now_price + $product->f_up_price)) {
                //最大価格に達す,かつ、マイナス処理可能場合
                if($product->f_r_min_price <= ($product->f_now_price - $product->f_down_price)) {
                    $product->f_now_price -= $product->f_down_price;
                    //マイナスのステータスに更新
                    $this->updateRevStatus($product->fk_products_id, 1);
                }
            }
            else
                $product->f_now_price +=$product->f_up_price;
        }
        else {
            //マイナス中
            if( $product->f_r_min_price > ($product->f_now_price - $product->f_down_price)) {
                //最小価格に達す場合,かつ、上昇処理可能場合
                if( $product->f_r_max_price >= ($product->f_now_price + $product->f_up_price)) {
                    $product->f_now_price += $product->f_up_price;
                    //マイナスのステータスに更新
                    $this->updateRevStatus($product->fk_products_id, 0);
                }
            }
            else
                $product->f_now_price -=$product->f_down_price;
        }
    }

    public function getPrevSameProduct($productId){
//          return DB::uniqueObject("SELECT FORMAT(endp.f_end_price,0) as f_end_price,endp.f_end_time,endp.f_bit_buy_count,endp.f_bit_free_count,endp.f_last_bidder,mm.f_handle FROM auction.t_end_products endp,auction.t_products_master pm,auction.t_member_master mm
//                                WHERE endp.fk_end_products_id = pm.fk_products_id AND mm.fk_member_id = endp.f_last_bidder
//                                AND EXISTS (SELECT pm.fk_products_id  FROM auction.t_products_master pmm
//                                WHERE pmm.fk_products_template_id = pm.fk_products_template_id AND pmm.fk_products_id=$productId)
//                                ORDER BY endp.f_end_time DESC limit 0,1");
          $sql="SELECT FORMAT(endp.f_end_price,0) AS f_end_price,endp.f_end_time,endp.f_bit_buy_count,endp.f_bit_free_count,endp.f_last_bidder,mm.f_handle
FROM auction.t_end_products endp,auction.t_products_master pm,auction.t_member_master mm ,(SELECT pmm.fk_products_template_id  FROM auction.t_products_master pmm
                                WHERE pmm.fk_products_id=$productId) epm
                                WHERE endp.fk_end_products_id = pm.fk_products_id AND mm.fk_member_id = endp.f_last_bidder
                                AND epm.fk_products_template_id =pm.fk_products_template_id
                                ORDER BY endp.f_end_time DESC LIMIT 0,1";
            return DB::uniqueObject($sql);
//        return DB::uniqueObject("SELECT en.*,mem.f_handle FROM (
//                            SELECT FORMAT(f_end_price,0) AS f_end_price,f_end_time,f_bit_buy_countf_bit_buy_count,f_bit_free_count,f_last_bidder
//                            FROM t_end_products
//                            WHERE fk_end_products_id IN (
//                            SELECT fk_products_id FROM t_products_master
//                            WHERE fk_products_template_id=(
//                            SELECT fk_products_template_id FROM t_products_master WHERE fk_products_id=?)
//                            AND fk_products_id<>? ORDER BY f_end_time DESC)
//                            ) en
//                            LEFT OUTER JOIN t_member_master mem ON mem.fk_member_id=f_last_bidder"
//                            ,array($productId,$productId));

    }

}
?>
