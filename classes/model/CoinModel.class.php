<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.'.model.Model'); 
/**
 * Description of CoinModel
 *
 * @author mw
 */
class CoinModel extends Model {
    //put your code here

    /**
     *メンバーIDでコインのプランを取得する
     * @param <int> $id メンバーID
     * @return <array> コインのプランのObjectの配列
     */
    public function getCoinMenuByMemberId($id) {

//        $sql ="SELECT cs.fk_coin_id,cs.fk_coin_group_id,cs.f_coin + cs.f_free_coin f_coin,cs.f_inp_money
//                FROM auction.t_coin_setup cs,auction.t_coin_group cg,auction.t_member_master mm,auction.t_member_group mg
//                WHERE mm.fk_member_id =$id
//                AND cs.fk_coin_group_id = cg.fk_coin_group_id
//                AND cg.fk_coin_group_id = mg.fk_coin_group_id
//                AND mm.fk_member_group_id = mg.fk_member_group_id order by cs.f_inp_money";

        $sql ="SELECT cs.fk_coin_id,cs.fk_coin_group_id,cs.f_coin + cs.f_free_coin f_coin,cs.f_inp_money
                FROM auction.t_coin_setup cs,auction.t_coin_group cg,auction.t_member_master mm,auction.t_member_group mg
                WHERE mm.fk_member_id =$id
                AND cs.fk_coin_group_id = cg.fk_coin_group_id
                AND cg.fk_coin_group_id = mg.fk_coin_group_id
                AND mm.fk_member_group_id = mg.fk_member_group_id order by cs.fk_coin_id";

        $coins = DB::getObjectByQuery($sql);
//        $coins = DB::getObjectByQuery("SELECT coin.fk_coin_id,coin.fk_coin_group_id,coin.f_coin+coin.f_free_coin AS f_coin,coin.f_inp_money FROM t_coin_setup coin
//                                INNER JOIN
//                                (SELECT member.fk_coin_group_id,mast.fk_member_id FROM t_member_group member INNER JOIN
//                                t_member_master mast ON mast.fk_member_group_id=member.fk_member_group_id
//                                WHERE mast.fk_member_id=?) grp
//                                  ON coin.fk_coin_group_id=grp.fk_coin_group_id
//                                  ORDER BY coin.fk_coin_id",array($id));
//        if($_SESSION["member_id"]!=10002)
//            array_pop($coins);
        return $coins;
    }
    
    /**
     *member_id とcoin_idでコインメニューを取得
     * @param <type> $member_id
     * @param <type> $coin_id
     * @return <type> 
     */
    public function getCoinMenuById($member_id,$coin_id) {
        return DB::uniqueObject("SELECT coin.fk_coin_id,coin.fk_coin_group_id,coin.f_coin+coin.f_free_coin AS f_coin,coin.f_inp_money FROM t_coin_setup coin
                        INNER JOIN
                        (SELECT member.fk_coin_group_id,mast.fk_member_id FROM t_member_group member INNER JOIN
                        t_member_master mast ON mast.fk_member_group_id=member.fk_member_group_id
                        WHERE mast.fk_member_id=?) grp
                          ON coin.fk_coin_group_id=grp.fk_coin_group_id
                          WHERE coin.fk_coin_id=?
                          ORDER BY coin.fk_coin_id",array($member_id,$coin_id));
    }

   /**
    *決済方法を取得
    * @param <int> $type 支払いタイプ、（コイン購入か、商品購入か）
    * @param <int> $price 商品の価格（コインの場合、参照しない）決済可能な最低金額のチェック
    * @param <int> $term_type PCか、モバイルか
    * @return <type>
    */
    public function getPayMethod($type,$price,$term_type = TERM_TYPE_PC)
    {
        //支払いプランのPC,MDフラグ
//        $plan=$term_type==TERM_TYPE_PC ? " AND plan.f_use_pc=0" : " AND plan.f_use_mb=0";
        if($type == SHIHARAI_PRODUCT_TYPE)
            //落札商品購入
            $methods= DB::getObjectByQuery("SELECT `type`.fk_shiharai_type_id,`type`.fk_shiharai_agent_id,`type`.fk_shiharai_id,
                    `type`.fk_shiharai_type,`type`.fk_min_price,`type`.f_module,`type`.f_term_type,`type`.f_status,plan.f_shiharai_name
                    FROM t_shiharai_type `type`
                    LEFT OUTER JOIN t_shiharai_plan plan ON plan.fk_shiharai_id=`type`.fk_shiharai_id 
                    WHERE `type`.fk_shiharai_type=1 AND `type`.f_status=1 AND`type`.fk_publicsite_id=0 AND `type`.fk_min_price <= ?
                    AND `type`.f_term_type=$term_type AND NOT (f_module='WebMoney' AND $price>50000)" , array($price));
        else
            //コイン購入
            $methods= DB::getObjectByQuery("SELECT `type`.fk_shiharai_type_id,`type`.fk_shiharai_agent_id,`type`.fk_shiharai_id,
                    `type`.fk_shiharai_type,`type`.fk_min_price,`type`.f_module,`type`.f_term_type,`type`.f_status,plan.f_shiharai_name
                    FROM t_shiharai_type `type`
                    LEFT OUTER JOIN t_shiharai_plan plan ON plan.fk_shiharai_id=`type`.fk_shiharai_id 
                    WHERE `type`.fk_shiharai_type=0 AND `type`.f_status=1 AND`type`.fk_publicsite_id=0 AND `type`.fk_min_price <= ?
                    AND `type`.f_term_type=$term_type AND NOT (f_module='WebMoney' AND $price>50000)" , array($price));

//        $meth=array();
//        if($_SESSION["member_id"]!=10002){
//            foreach ($methods as $m) {
//                if($m->f_module!="WebMoney")
//                        array_push($meth, $m);
//            }
//            $methods=$meth;
//        }
        return $methods;

        //return DB::getObjectByQuery("SELECT `type`.fk_shiharai_type_id,`type`.fk_shiharai_agent_id,`type`.fk_shiharai_id,
        //                    `type`.f_module,agent.f_name AS agent_name,agent.f_tel_no AS agent_tel_no,agent.f_mail_address AS agent_mail_address,
        //                    plan.f_shiharai_name
        //                    FROM t_shirarai_type `type`
        //                    LEFT OUTER JOIN t_siharai_agent agent ON agent.fk_shiharai_agent_id=`type`.fk_shiharai_agent_id
        //                    LEFT OUTER JOIN t_shirarai_plan plan ON plan.fk_shiharai_id=`type`.fk_shiharai_id
        //                    WHERE `type`.f_status=1 AND `type`.fk_shiharai_type=0");
    }

    /**
     *コインプランの情報を取得する
     */
    public function getCoinSettingById($id){
        return DB::uniqueObject("SELECT fk_coin_id,fk_coin_group_id,f_coin+f_free_coin AS f_coin,fk_shiharai_type_id,f_inp_money FROM t_coin_setup WHERE fk_coin_id=?", array($id));
    }
    

    public function getMemberById($id) {
        return DB::uniqueObject("SELECT f_coin,f_free_coin FROM t_member WHERE fk_member_id=?",array($id));
    }

    /**
     *コイン購入
     * @param <type> $memberId
     * @param <type> $coinId
     * @return <type>
     */
    public function buyCoin($memberId,$coinId) {
        try {
            import('classes.model.MemberModel');
            import(FRAMEWORK_DIR.".utils.Utils");
            $memberModel=new MemberModel();
            DB::begin();
            $menu=$this->getCoinMenuById($coinId);
            $member=$memberModel->getMemberById($memberId);
            if($menu==NULL || $member==NULL)
                return FALSE;
            //t_memberのコイン数を更新
//G.Chin AWKC-218 2010-11-12 chg sta
/*
            DB::executeNonQuery("UPDATE t_member SET f_coin=IFNULL(f_coin,0)+?,f_total_pay=IFNULL(f_total_pay,0)+?,f_tm_stamp=CURRENT_TIMESTAMP WHERE fk_member_id=?",
                    array($menu->f_coin,$menu->f_inp_money,$memberId));
*/
            DB::executeNonQuery("UPDATE t_member SET f_coin=IFNULL(f_coin,0)+?,f_total_pay=IFNULL(f_total_pay,0)+?,f_tm_stamp=CURRENT_TIMESTAMP,f_total_pay_cnt=f_total_pay_cnt+1 WHERE fk_member_id=?",
                    array($menu->f_coin,$menu->f_inp_money,$memberId));
//G.Chin AWKC-218 2010-11-12 chg end
            //t_coin_logに記録を追加
            DB::executeNonQuery("INSERT INTO t_coin_log (f_member_id,f_pay_money,f_coin_id,f_coin,f_coin_before,f_tm_stamp)
                                VALUES (?,?,?,?,CURRENT_TIMESTAMP)", array($memberId,$menu->f_inp_money,$menu->fk_coin_id,$menu->f_coin,
                    Utils::getInt($member->f_coin,0) + Utils::getInt($member->f_coin,0)));
            DB::commit();
            return true;
        }
        catch (Exception $e) {
            DB::rollback();
        }
    }
}
?>
