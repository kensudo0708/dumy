<?php
/*
        * To change this template, choose Tools | Templates
        * and open the template in the editor.
*/

import('classes.model.MemberModel');

//include "/home/auction/lib/define.php";
/**
 * Description of ShiharaiModelclass
 *
 * @author kaz
 */

class ShiharaiModel extends BaseAction {
    //t_shiharai_plan ﾃｰﾌﾞﾙの情報を取得するﾒｿｯﾄﾞ
    public function GetShiharaiType($shiharai_type_id) {
        //$payment_typeの値がfk_shiharai_idから→fk_shiharai_type_idにしました（buymethod.htmlに参照）
        return DB::getObjectByQuery("select `fk_shiharai_type_id`,`fk_shiharai_agent_id`,
                            `fk_shiharai_id`,`fk_shiharai_type`,`fk_min_price`,`f_module`,`f_term_type`,`f_status`
                            from t_shiharai_type where fk_shiharai_type_id=?",array($shiharai_type_id));
        //2010/04/23 meng add end
    }

    //***********このメソッドの処理が一個のトランザクション内部でやるべき*******************
    //
    //ｺｲﾝ購入処理ラッパー関数
    public function buyCoins($member_id, $inp_money, $shiharai_type_id, $pay_log_id) {
        /*      決済完了後の処理を一個のトランザクション内部で纏める                 */
        $i=1;
        while($i<=10) {
            try {
                $pay = $this->getPayTerm($pay_log_id);
                if($pay->f_cert_status==0) {
                    //既に決済完了の場合、二重決済
                    $this->addSecondPay($pay);
//                    return;	//G.Chin 2010-08-28 del
//G.Chin 2010-09-17 add sta
					if(FLG_SECONDPAY_OK == 0)
					{
						return;
					}
//G.Chin 2010-09-17 add end
                }

                DB::begin();
                $add_coins = $this->getAppendCoins($member_id, $inp_money);
                $this->AddMemberCoins($member_id, $add_coins);
                $memberModel = new MemberModel();
                $member = $memberModel->getMemberById($member_id);

                $coin_result = $member->f_coin + $member->f_free_coin;
                //print "COIN-RESULT=> $coin_result<BR>\n";
                $isPc="";

                if($pay->f_pay_pos == 0) {
                    $isPc=true;
                }
                else {
                    $isPc=false;
                }

                $mast = $memberModel->getAdcodeMasterInfo($member_id);
                if(!empty ($mast)) {
                    //広告主が存在する場合
                    import('classes.model.Total');
                    //$isPc=Config::value("PC_DIR") == AgentInfo::getAgentDir();
                    Total::buy($mast->fk_admaster_id,$inp_money, $isPc);
                }
                import('classes.model.TotalSale');
                TotalSale::buycoin($inp_money, $add_coins->f_coin+$add_coins->f_free_coin, $pay->fk_shiharai_type_id);

                //支払い記録実行
                $status     = SHIHARAI_COIN_TYPE;    //支払いﾀｲﾌﾟ：ｺｲﾝ購入
                $staff_id   = 0;    //STAFF-ID  :無し
                $min_price  = 0;    //仕入れ価格 :0円
                $this->FixPayLogOK($member_id,$pay_log_id, $coin_result,$inp_money,$add_coins);
                //親会員のコイン付与
                import("classes.utils.PayCondition");
                $pay=new PayCondition();
                $pay->setAdPrice($member->fk_adcode_id, $member->fk_member_id, $inp_money,null);
                DB::commit();
                Logger::debug("[コイン購入決済完了]pay_log ID:$pay_log_id,会員ID:$member_id,支払金額:$inp_money,加算コイン数:[課金:$add_coins->f_coin,無料:$add_coins->f_free_coin],加算後会員コイン数:[課金:$member->f_coin,無料:$member->f_free_coin]！");

                // ゲーム関係の処理
                if ( defined('GAME_ENABLE') && constant('GAME_ENABLE') && defined('GAME_ID_A') ) {
                    try {
                        import('classes.model.GameModel');
                        try {
                            $options = array();
                            if ( defined( 'GAME_ITEMS_A' ) ) {
                                $options['const.prize_items'] = GAME_ITEMS_A;
                            }
                            $game = new GameModel( GAME_ID_A, $options );
                            // プレイ権利付与と当時に抽選処理を行うため、ボーナス効果を先に付与してください
                            // ボーナス効果を即時利用するために、プレイ権利の付与に先駆けてボーナス効果の付与処理をコミットする必要があります。

                            // ボーナス効果の付与
                            DB::begin();
                            $game->putGameBonusOfCoin( $member->fk_member_id, ($add_coins->f_coin + $add_coins->f_free_coin) );
                            DB::commit();

                            // プレイ権利の付与
                            DB::begin();
                            $game->putPlayTicketOfCoin( $member->fk_member_id );
                            DB::commit();

                        } catch ( Exception $e ) {
                            DB::rollback();
                            Logger::error( $e->getMessage() );
                            // ！！ここで補足した例外はthrowしてはいけない！！（決済処理までロールバックされます。）
                        }
                    } catch ( Exception $e ) {
                        // システムパラメータが定義されているのにGameModel.class.phpがデプロイされていない場合はWARNINGでロギング
                        Logger::warn( $e->getMessage() );
                        // ！！ここで補足した例外はthrowしてはいけない！！（決済処理までロールバックされます。）
                    }
                }
                return true;
            }
            catch (Exception $e) {
                DB::rollback();
                Logger::error("[コイン購入決済完了後処理失敗".$i."回目]pay_log ID:$pay_log_id,会員ID:$member_id,支払金額:$inp_money,！");
                Logger::error("[Message]:".$e->getMessage()."\n\t[Trace]:".$e->getTraceAsString());
                $i++;
//                return false;
            }
        }
//G.Chin #143 2011-01-19 add sta
		$row=array();
		$row["category"] = "決済関連";
		$row["body"] = "コイン購入決済完了後処理が10回失敗 pay_log ID:$pay_log_id,会員ID:$member_id,支払金額:$inp_money,";
		$row["status"] = 0;
		TNewsModel::insert($row);
//G.Chin #143 2011-01-19 add end
        return false;

    }

    //***********このメソッドの処理が一個のトランザクション内部でやるべき*******************
    //商品購入処理
    public function buyProduct($member_id, $inp_money, $shiharai_type_id, $pay_log_id, $pro_id) {
        /*      決済完了後の処理を一個のトランザクション内部で纏める                 */
        $date=DB::uniqueObject("SELECT e.f_products_name ,pm.fk_item_category_id ,pm.f_plus_coins FROM auction.t_products_master pm,auction.t_end_products e
                                    WHERE e.fk_end_products_id=pm.fk_products_id
                                    AND e.fk_end_products_id = $pro_id ");
        //支払い記録実行
        $status     = SHIHARAI_PRODUCT_TYPE;    //支払いﾀｲﾌﾟ：商品購入
        $staff_id   = 0;    //STAFF-ID  :無し
        $min_price  = 0;    //仕入れ価格 :0円
//        $coin_result = 0;   //商品購入なのでコイン付与は無し
        $i=1;
        while($i<=10) {
            $coin_result = 0;   //商品購入なのでコイン付与は無し（！ここで初期化しないと失敗時ループでt_pay_logに意図しない値が設定されます。）
            try {
                $pay = $this->getPayTerm($pay_log_id);
                if($pay->f_cert_status==0) {
                    //既に決済完了の場合、二重決済
                    $this->addSecondPay($pay);
//                    return;	//G.Chin 2010-08-28 del
//G.Chin 2010-09-17 add sta
					if(FLG_SECONDPAY_OK == 0)
					{
						return;
					}
//G.Chin 2010-09-17 add end
                }
                DB::begin();
                $this->FixPayLogOK($member_id,$pay_log_id, $coin_result,$inp_money);
                //Logger::warn("[Message]:アイテムカテゴリ:".$date->fk_item_category_id ."=".COIN_CATEGORY);
                $mem=null;
                $coin_result=null;
                if($date->fk_item_category_id == COIN_CATEGORY) {
                    //Logger::warn("[Message]:コインパック購入");
                    $mem=DB::uniqueObject("select f_coin+f_free_coin as coin from auction.t_member where fk_member_id =$member_id");
                    //コイン発行履歴記録
                    $add_coin   = $date->f_plus_coins;
                    $coin_result = $mem->coin+$add_coin;
//G.Chin 2010-07-13 chg sta
//                $this->FixPayLogOK2($member_id,$coin_result,$add_coin,$pro_id);
                    $this->FixPayLogOK2($member_id,$coin_result,$add_coin,$pro_id,$pay);
//G.Chin 2010-07-13 chg end


                }
                DB::executeNonQuery("update t_member set f_unpain_num=f_unpain_num-1 where fk_member_id=$member_id and f_unpain_num >0");
                DB::executeNonQuery("update t_end_products set f_status=2 where fk_end_products_id=?", array($pro_id));
                if($date->fk_item_category_id == COIN_CATEGORY) {
                    //Logger::warn("[Message]:コインパック購入2");
                    DB::executeNonQuery("update t_end_products set f_status=4 where fk_end_products_id=?", array($pro_id));
                }
                else {
                    DB::executeNonQuery("update t_end_products set f_status=3 where f_address_flag = 0 and fk_address_id IS NOT NULL and fk_end_products_id=?", array($pro_id));
                }
                //t_stat_salesに記録する
                import('classes.model.TotalSale');
                TotalSale::buyproduct($pro_id,$date->fk_item_category_id == COIN_CATEGORY);

                DB::commit();
                if($date->fk_item_category_id == COIN_CATEGORY)
                    Logger::debug("[コインパック購入決済完了]pay_log ID:$pay_log_id,会員ID:$member_id,支払金額:$inp_money,加算コイン数:[無料:$date->f_plus_coins],加算後会員コイン数:[$coin_result]！");
                else
                    Logger::debug("[商品決済完了]pay_log ID:$pay_log_id,会員ID:$member_id,商品ID:$pro_id,支払金額:$inp_money");

                // ゲーム関係の処理(コインパック購入のみ)
                if ( defined('GAME_ENABLE') && constant('GAME_ENABLE') && defined('GAME_ID_A')  && $date->fk_item_category_id == COIN_CATEGORY ) {
                    try {
                        import('classes.model.GameModel');
                        try {
                            $options = array();
                            if ( defined( 'GAME_ITEMS_A' ) ) {
                                $options['const.prize_items'] = GAME_ITEMS_A;
                            }
                            $game = new GameModel( GAME_ID_A, $options );
                            // プレイ権利付与と当時に抽選処理を行うため、ボーナス効果を先に付与してください
                            // ボーナス効果を即時利用するために、プレイ権利の付与に先駆けてボーナス効果の付与処理をコミットする必要があります。

                            // ボーナス効果の付与
                            DB::begin();
                            $game->putGameBonusOfCoinPack( $member_id, $date->f_plus_coins );
                            DB::commit();

                            // プレイ権利の付与
                            DB::begin();
                            $game->putPlayTicketOfCoinPack( $member_id );
                            DB::commit();

                        } catch ( Exception $e ) {
                            DB::rollback();
                            Logger::error( $e->getMessage() );
                            // ！！ここで補足した例外はthrowしてはいけない！！（決済処理までロールバックされます。）
                        }
                    } catch ( Exception $e ) {
                        // システムパラメータが定義されているのにGameModel.class.phpがデプロイされていない場合はWARNINGでロギング
                        Logger::warn( $e->getMessage() );
                        // ！！ここで補足した例外はthrowしてはいけない！！（決済処理までロールバックされます。）
                    }
                }

                return true;
            }
            catch (Exception $e) {
                DB::rollback();
                Logger::error("[商品決済完了後処理失敗".$i."回目]pay_log ID:$pay_log_id,会員ID:$member_id,商品ID:$pro_id,支払金額:$inp_money");
                Logger::error("[Message]:".$e->getMessage()."\n\t[Trace]:".$e->getTraceAsString());
//                return false;
                $i++;
            }
        }
//G.Chin #143 2011-01-19 add sta
		$row=array();
		$row["category"] = "決済関連";
		$row["body"] = "商品決済完了後処理が10回失敗 pay_log ID:$pay_log_id,会員ID:$member_id,商品ID:$pro_id,支払金額:$inp_money,";
		$row["status"] = 0;
		TNewsModel::insert($row);
//G.Chin #143 2011-01-19 add end
        return false;
    }

    //付与ﾎﾟｲﾝﾄ取得関数
    public function getAppendCoins($member_id, $inp_money) {
        //会員ﾃｰﾌﾞﾙから使用ｸﾞﾙｰﾌﾟを取得

        $coin = DB::uniqueObject("SELECT coin.f_coin,coin.f_free_coin FROM t_member_master member
                        LEFT OUTER JOIN t_member_group `group` ON member.fk_member_group_id=`group`.fk_member_group_id
                        LEFT OUTER JOIN t_coin_setup coin ON coin.fk_coin_group_id=`group`.fk_coin_group_id
                        WHERE member.fk_member_id=? AND coin.f_inp_money=?", array($member_id,$inp_money));
        if(empty($coin)) {
            //該当金額の購入コイン数の設定が存在しない場合、f_over_moneyに書きこみ
            //会員のｺｲﾝを更新
            DB::executeNonQuery("UPDATE t_member_master SET f_over_money=IFNULL(f_over_money,0)+? WHERE fk_member_id=?", array($inp_money, $member_id));
            return 0;
        }
        return $coin;
    }

    private function addSecondPay($pay) {
        try {
            DB::begin();
//G.Chin 2010-08-28 chg sta
//G.Chin 2010-09-17 chg sta
			if(FLG_SECONDPAY_OK == 0)
			{
	            DB::executeNonQuery("
	                    INSERT INTO auction.t_pay_log
	                    (f_status, f_member_id, f_staff_id, f_pay_money, f_coin_add, f_free_coin_add, fk_shiharai_type_id,
	                    f_coin_result, f_min_price, f_cert_status, f_pay_pos, f_tm_stamp, fk_adcode_id, fk_products_id, f_double
	                    )
	                    VALUES
	                    ($pay->f_status, $pay->f_member_id, $pay->f_staff_id, $pay->f_pay_money, $pay->f_coin_add, $pay->f_free_coin_add, $pay->fk_shiharai_type_id,
	                    $pay->f_coin_result, $pay->f_min_price, 4, $pay->f_pay_pos, CURRENT_TIMESTAMP, $pay->fk_adcode_id, $pay->fk_products_id, 1
	                    )");
				//▼多重決済のニュースを登録
				$f_category = "決済関連";
				$f_body = "多重決済(未処理)が発生しています。(会員ID:".$pay->f_member_id.")";
			}
			else
			{
	            DB::executeNonQuery("
	                    INSERT INTO auction.t_pay_log
	                    (f_status, f_member_id, f_staff_id, f_pay_money, f_coin_add, f_free_coin_add, fk_shiharai_type_id,
	                    f_coin_result, f_min_price, f_cert_status, f_pay_pos, f_tm_stamp, fk_adcode_id, fk_products_id, f_double
	                    )
	                    VALUES
	                    ($pay->f_status, $pay->f_member_id, $pay->f_staff_id, $pay->f_pay_money, $pay->f_coin_add, $pay->f_free_coin_add, $pay->fk_shiharai_type_id,
	                    $pay->f_coin_result, $pay->f_min_price, 0, $pay->f_pay_pos, CURRENT_TIMESTAMP, $pay->fk_adcode_id, $pay->fk_products_id, 1
	                    )");
				//▼多重決済のニュースを登録
				$f_category = "決済関連";
				$f_body = "多重決済(加算済み)が発生しています。(会員ID:".$pay->f_member_id.")";
			}
//G.Chin 2010-09-17 chg end
//G.Chin 2010-08-28 chg end
            DB::commit();
            Logger::debug("[payログID：".$pay->fk_pay_log_id."の二重決済しました。]");
//G.Chin 2010-09-12 add sta
//G.Chin 2010-09-17 del sta
/*
			//▼多重決済のニュースを登録
			$f_category = "決済関連";
			$f_body = "多重決済が発生しています。(会員ID:".$pay->f_member_id.")";
*/
//G.Chin 2010-09-17 del end
			
			//ニュース登録関数
			RegistTNews($f_category,$f_body);
//G.Chin 2010-09-12 add end
        }
        catch (Exception $e) {
            DB::rollback();
            Logger::error("[payログID：".$pay->fk_pay_log_id."の二重決済失敗しました。]");
            Logger::error("[Message]:".$e->getMessage()."\n\t[Trace]:".$e->getTraceAsString());
        }
    }

    //支払い端末取得
    public function getPayTerm($pay_log_id) {
        //会員ﾃｰﾌﾞﾙから使用ｸﾞﾙｰﾌﾟを取得
        $term = DB::uniqueObject("SELECT * FROM auction.t_pay_log
                        WHERE fk_pay_log_id=?", array($pay_log_id));
        return $term;
    }

    //実際に会員にｺｲﾝを追加する
    public function AddMemberCoins($member_id, $coins) {
        DB::executeNonQuery("update t_member set f_coin=f_coin+?,f_free_coin=f_free_coin+? where fk_member_id=?", array($coins->f_coin,$coins->f_free_coin, $member_id));
    }


    //支払い記録を書き込む(予約ﾓｰﾄﾞ)
    public function PutReservePayLog($status, $member_id, $staff_id, $pay_money, $add_coin, $shiharai_type_id, $coin_result, $min_price,$pro_id) {
        try {
            //int型null時処理(0クリア)
            if ($status == "") {
                $status = 0;
            }
            if ($pay_money == "") {
                $pay_money = 0;
            }
            if ($add_coin == "") {
                $add_coin = 0;
            }

            if(!isset ($_GET['payment']) && !isset ($_POST['payment']))
                $shiharai_type_id="0";
            else {
                $shiharai_type_id = $_GET['payment'];
                if ($shiharai_type_id == "")
                    $shiharai_type_id = $_POST['payment'];
            }

            if ($coin_result == "") {
                $coin_result = 0;
            }
            if ($min_price == "") {
                $min_price = 0;
            }
            $term = AgentInfo::getAccessTerminal();
            $term = $term == "PC" ? 0 : 1;
            DB::begin();
            switch ($status) {
                case SHIHARAI_COIN_TYPE:
                //広告測定、広告からの会員の場合、広告主のコードもt_pay_logに記録する
                    $ad = DB::uniqueObject("SELECT ad.f_type,ad.fk_adcode_id FROM t_member_master mast
                                INNER JOIN t_adcode ad ON mast.fk_parent_ad = ad.fk_adcode_id
                                WHERE mast.fk_member_id= ?", array($member_id));
                    if(!empty ($ad) && $ad->f_type==1) {
                        //広告主の場合,広告主のコードもt_pay_logに記録する
                        //会員のｺｲﾝを更新
                        DB::executeNonQuery("insert into t_pay_log (f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,f_min_price,f_cert_status,f_cert_err,fk_adcode_id,f_pay_pos,f_tm_stamp)
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)", array($status, $member_id, $staff_id, $pay_money, $add_coin, $shiharai_type_id, $coin_result, $min_price, PAYLOG_CERT_ON, "",$ad->fk_adcode_id,$term));
                    }
                    else {
                        //会員のｺｲﾝを更新
                        DB::executeNonQuery("insert into t_pay_log (f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,f_min_price,f_cert_status,f_cert_err,f_pay_pos,f_tm_stamp)
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)", array($status, $member_id, $staff_id, $pay_money, $add_coin, $shiharai_type_id, $coin_result, $min_price, PAYLOG_CERT_ON, "",$term));
                    }
                    break;

                case SHIHARAI_PRODUCT_TYPE:
                //落札商品支払い場合
                    Logger::debug("insert into t_pay_log (f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,f_min_price,f_cert_status,f_cert_err,f_pay_pos,fk_products_id,f_tm_stamp)
                                    VALUES ($status,$member_id, $staff_id, $pay_money, $add_coin, $shiharai_type_id, $coin_result, $min_price,". PAYLOG_CERT_ON .",'', $term, $pro_id, CURRENT_TIMESTAMP)");
                    DB::executeNonQuery("insert into t_pay_log (f_status,f_member_id,f_staff_id,f_pay_money,f_coin_add,fk_shiharai_type_id,f_coin_result,f_min_price,f_cert_status,f_cert_err,f_pay_pos,fk_products_id,f_tm_stamp)
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)", array($status, $member_id, $staff_id, $pay_money, $add_coin, $shiharai_type_id, $coin_result, $min_price, PAYLOG_CERT_ON, "",$term,$pro_id));

                    break;
            }


            DB::commit();
            return true;
        }
        catch (Exception $e) {
            //ｴﾗｰ発生時はﾛｰﾙﾊﾞｯｸね
            DB::rollback();
            return false;
        }
    }

    //    //支払い済みでt_pay_logを更新　更新　０完了　１認証中　２エラ　３支払い済み
    public function UpdatePayLog($fk_pay_log_id) {

        try {
            DB::begin();
            //会員のｺｲﾝを更新
            DB::executeNonQuery("update t_pay_log set f_cert_status=3,f_tm_stamp=CURRENT_TIMESTAMP where fk_pay_log_id=? AND f_cert_status=1", array($fk_pay_log_id));
            DB::commit();

            return true;
        }
        catch (Exception $e) {

            //ｴﾗｰ発生時はﾛｰﾙﾊﾞｯｸね
            DB::rollback();
            return false;
        }

    }

    public function hasPaid($f_member_id,$fk_product_id) {
        $count= DB::result_rows("SELECT f_cert_status FROM t_pay_log
                                WHERE f_member_id= ? AND fk_products_id= ? AND (f_cert_status=0 OR f_cert_status=3)",array($f_member_id,$fk_product_id));
        return $count>0 ? true :false;
    }

    //支払い記録をFIXする（成功パターン）
    public function FixPayLogOK($member_id,$pay_log_id, $coin_result,$inp_money,$add_coins=null) {
        //int型null時処理(0クリア)
        if ($coin_result == "") {
            $coin_result = 0;
        }

        if(empty ($add_coins))
        //商品購入の場合
            DB::executeNonQuery("update t_pay_log set f_cert_status=0,f_coin_result=?,f_tm_stamp=CURRENT_TIMESTAMP where fk_pay_log_id=?", array($coin_result, $pay_log_id));
        else
        //会員のｺｲﾝを更新
            DB::executeNonQuery("update t_pay_log set f_cert_status=0,f_coin_result=?,f_coin_add=?,f_free_coin_add=?,f_tm_stamp=CURRENT_TIMESTAMP where fk_pay_log_id=?", array($coin_result,$add_coins->f_coin,$add_coins->f_free_coin, $pay_log_id));
        if($coin_result != 0)
//G.Chin AWKC-218 2010-11-12 chg sta
            DB::executeNonQuery("UPDATE t_member SET f_total_pay = IFNULL(f_total_pay,0) + ?,f_total_pay_cnt=f_total_pay_cnt+1 WHERE fk_member_id =?", array($inp_money, $member_id));
//G.Chin AWKC-218 2010-11-12 chg end
    }

    //支払い記録をFIXする（成功パターン）
    public function FixPayLogNG($pay_log_id, $payment_type, $rst) {
        import('classes.model.LogModel');
        $log=new LogModel();
        $log->write("getPayResult", $_SERVER['REQUEST_URI']);
        try {
            if ($payment_type == SHIHARAI_TYPE_CREDIT) {
                $f_cert_err = "ｸﾚｼﾞｯﾄ決済に失敗しました。";
            }
            else if ($payment_type == SHIHARAI_TYPE_INETBANKING) {
                if ($rst == IPS_EBANK_OVER) {
                    $f_cert_err = "超過金額が振り込まれました。";
                }
                else if ($rst == IPS_EBANK_LOWER) {
                    $f_cert_err = "金額不足でした。";
                }
            }
            else if ($payment_type == SHIHARAI_TYPE_EDY) {
                $f_cert_err = "Edy決済に失敗しました。";
            }
            else if ($payment_type == SHIHARAI_TYPE_CONV) {
                $f_cert_err = "ｺﾝﾋﾞﾆ決済に失敗しました。";
            }

            $sql="select fk_pay_log_id from t_pay_log where f_cert_status = 0 and fk_pay_log_id = $pay_log_id";
            $log_obj=DB::uniqueObject($sql);
            
//G.Chin 2010-08-12 chg sta
            if($log_obj==null)
            {
                DB::begin();
                DB::executeNonQuery("update t_pay_log set f_cert_status=?,f_cert_err=?,f_tm_stamp=CURRENT_TIMESTAMP where fk_pay_log_id=?", array(2, $f_cert_err, $pay_log_id));
                DB::commit();
            }
            else
            {
                import('classes.model.TNewsModel');
                $news = new TNewsModel();
                DB::begin();
                $news->insert(array('category'=>"決済関連",'body'=>"決済確定済みのデータに失敗のデータが送られてきました:PAY_LOG_ID:$pay_log_id"));
                DB::commit();
            }
//G.Chin 2010-08-12 chg end
            return true;
        }
        catch (Exception $e) {
            //ｴﾗｰ発生時はﾛｰﾙﾊﾞｯｸね
            DB::rollback();
            return false;
        }
    }
    //コイン発行記録（成功パターン）コインパック購入
    public function FixPayLogOK2($member_id,$coin_result,$add_coin,$pro_id,$pay) {
        //int型null時処理(0クリア)
        if ($coin_result == "") {
            $coin_result = 0;
        }
        if(COIN_PACK_PLAN ==0)//全てフリーへの加算
        {
            DB::executeNonQuery("insert into  auction.t_pay_log (f_status,f_member_id,f_free_coin_add,fk_shiharai_type_id,f_coin_result,f_cert_status,f_pay_pos,f_tm_stamp,fk_products_id)
                        values(3,$member_id,$add_coin,$pay->fk_shiharai_type_id,$coin_result,0,2,now(),$pro_id)");
            if($coin_result != 0)
                DB::executeNonQuery("UPDATE t_member SET f_free_coin =f_free_coin+$add_coin WHERE fk_member_id =$member_id");
        }
        else if(COIN_PACK_PLAN==1)//全て課金コインへの加算
        {
            DB::executeNonQuery("insert into  auction.t_pay_log (f_status,f_member_id,f_coin_add,fk_shiharai_type_id,f_coin_result,f_cert_status,f_pay_pos,f_tm_stamp,fk_products_id)
                        values(3,$member_id,$add_coin,$pay->fk_shiharai_type_id,$coin_result,0,2,now(),$pro_id)");
            if($coin_result != 0)
                DB::executeNonQuery("UPDATE t_member SET f_coin =f_coin+$add_coin WHERE fk_member_id =$member_id");
        }
        else if(COIN_PACK_PLAN==2)//計算して追加
        {
            $tmp_add_coin = floor($pay->f_pay_money / POINT_VALUE);
            $tmp_free_add_coin=$add_coin-$tmp_add_coin;
            if($tmp_add_coin >= $add_coin)
            {
                $tmp_add_coin=$add_coin;
                $tmp_free_add_coin = 0;
            }
            DB::executeNonQuery("insert into  auction.t_pay_log (f_status,f_member_id,f_coin_add,f_free_coin_add,fk_shiharai_type_id,f_coin_result,f_cert_status,f_pay_pos,f_tm_stamp,fk_products_id)
                        values(3,$member_id,$tmp_add_coin,$tmp_free_add_coin,$pay->fk_shiharai_type_id,$coin_result,0,2,now(),$pro_id)");
            if($coin_result != 0)
                DB::executeNonQuery("UPDATE t_member SET f_coin =f_coin+$tmp_add_coin,f_free_coin =f_free_coin+$tmp_free_add_coin WHERE fk_member_id =$member_id");
        }
    }
}
//G.Chin 2010-07-13 chg end

?>
