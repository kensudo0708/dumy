<?php
/**
 * ダイレクト会員登録関連処理マネージャクラス
 *
 * ■概要
 * このクラスは通常の会員登録プロセスを経ずにクロスドメインから配信される入力フォームから、
 * 非同期通信にて直接会員情報をPOSTすることで会員登録処理を完了させるため処理が実装されたものです。
 *
 * ■制限事項
 * - アフィリエイト・サービス・プロバイダが提供するプログラムとは連携できません。
 */

/**
 * クラスファイルの導入
 * - import()の実装において、require,require_onceが適切に使い分けられていないことに対処するための苦肉の表現です。
 * - load.phpに記述されているクラスは割愛
 */
if ( !class_exists('MemberModel') ) {
    import('classes.model.MemberModel');
}
if ( !class_exists('StringBuilder') ) {
    import(FRAMEWORK_DIR.'.utils.StringBuilder');
}
if ( !class_exists('InputCheck') ) {
    import("classes.utils.InputCheck");
}

class MemberRegistManager {

    protected $member; // t_member_masterレコード

    protected $adcode; // t_adcodeレコード

    /**
     * コンストラクタ
     */
    public function __construct() {
        // validate()内のチェック処理のみで使用
        $this->member = new MemberModel();
    }

    /**
     * データ構造の変換処理（既存の入力チェック処理を利用するためのデータ構造変換処理）
     * @param <hash> $src リクエストパラメータ
     * @return string
     */
    public function convert( $src ) {
        /**
         * 変換マップ
         * 左辺(変換後の属性名)=右辺(変換前の属性名or変換前の属性がない場合は変換後の属性値として適用)
         */
        $convMap = array(
            'handle'=>'hn',
            'login_id'=>'id',
            'login_pass'=>'pass',
            'mail_address'=>'mail',
            'tel_no'=>'',
            'birth_year'=>'birth',
            'birth_month'=>'birth',
            'birth_day'=>'birth',
            'onamae'=>'name',
            'area'=>'area',
            'job'=>'work',
            'agreement'=>'agr',
        );
        $dest = array();
        foreach ( $convMap as $to=>$from ) {
            $value = NULL;
            if ( isset($src[($from)]) ) {
                $value = $src[($from)];
                switch ( $to ) {
                case 'birth_year':
                    if ( strlen( $value ) ) {
                        $dest[($to)] = substr( $value, 0, 4 );
                    } else {
                        $dest[($to)] = '';
                    }
                    break;
                case 'birth_month':
                    if ( strlen( $value ) ) {
                        $dest[($to)] = substr( $value, 4, 2 );
                    } else {
                        $dest[($to)] = '';
                    }
                    break;
                case 'birth_day':
                    if ( strlen( $value ) ) {
                        $dest[($to)] = substr( $value, 6, 2 );
                    } else {
                        $dest[($to)] = '';
                    }
                    break;
                default:
                    $dest[($to)] = $value;
                }
            } else {
                $dest[($to)] = $convMap[($to)];
            }
        }
        return $dest;
    }

    public function getAdcode( $adcode ) {
        $sql = "
SELECT
    fk_adcode_id, fk_admaster_id, f_type, f_mem_reg_urls
FROM
    auction.t_adcode
WHERE
    f_delflg = 0
    AND f_adcode = ?
";
        $data = DB::uniqueObject( $sql, array( $adcode ) );
        return $data;
    }


    /**
     * 会員登録処理
     *　
     * @param <type> $data 会員情報
     * @return int 会員ID
     *
     *  下記の既存の処理を踏襲しています。
     *  @see MemberAction.register() 仮会員登録
     *  @see MemberModel.updateMemberActivity() 本登録
     *
     * （処理対象テーブル）
     * [INSERT]
     * t_adcode
     * t_member_master
     * t_member
     * t_pay_log
     *
     * [INSERT or UPDATE]
     * t_admster
     * t_adcounter
     * t_stat_ad
     * t_stat_coin
     */
    public function regist( $data, $activity=0 ) {

        // 紹介者、広告主の取得
        $parentAdId = NULL;
        $adcode = $this->getAdcode( $data['ad'] );
        if ( $adcode ) {
             $parentAdId = $adcode->fk_adcode_id;
        } else {
            Logger::error('広告情報を取得できません。('.var_export($data, TRUE).')'.' '.__FILE__.'('.__LINE__.')');
            return NULL;
        }

        /**
         * ■ t_adcode - t_member_master - t_memberの登録
         */

        // この会員の広告コード(友達紹介用コード)を生成
        $ad_code = StringBuilder::uuid();
        $sql = "INSERT INTO auction.t_adcode ( f_adcode, f_type, f_tm_stamp) VALUES ( ?, 0, CURRENT_TIMESTAMP)";
        DB::executeNonQuery( $sql, array($ad_code) );

        $sql = "
INSERT INTO auction.t_member_master (
    f_login_id, f_login_pass, f_handle, f_name, f_tel_no,
    f_sex, f_birthday, fk_address_flg, f_mail_address_pc, f_mailmagazein_pc,
    f_activity, fk_adcode_id, fk_parent_ad, f_adpri_value, fk_member_group_id,
    f_ip, f_regist_pos, f_regist_date, f_delete_flag, f_tm_stamp,
    f_aflkey, fk_area_id, fk_job_id
) VALUES (
    ?, ?, ?, ?, '',
    ?, ?, 0, ?, 1,
    ?, LAST_INSERT_ID(), ?, 0, 1,
    ?, 0, CURRENT_TIMESTAMP, 0, CURRENT_TIMESTAMP,
    ?, ?, ?
)
";
        $tMemberArray = array(
            $data['id'], md5($data['pass']), $data['hn'], $data['name'],
            $data['sex'], $data['birth'], $data['mail'],
            $activity, $parentAdId,
            $_SERVER['REMOTE_ADDR'],
            $data['afc'], $data['area'], $data['work'],
        );
        DB::executeNonQuery( $sql, $tMemberArray );

        // 以後の処理で必要となる項目を取得しています。
        $sql = "
SELECT
    fk_member_id, f_handle, f_login_id, f_mail_address_pc, f_mail_address_mb
FROM
    auction.t_member_master
WHERE
    fk_member_id = LAST_INSERT_ID()
";
        $member = DB::uniqueObject( $sql );
        if ( $member ) {
            $member->f_adcode = $ad_code;
        } else {
            Logger::error('会員登録失敗('.implode(', ', $reqParamArray).')'.' '.__FILE__.'('.__LINE__.')');
            return FALSE;
        }

        // 入会時にプレゼントされるコイン
        $presentCoin = Utils::getInt(REG_PRESENT_COIN,0);

        $sql="INSERT INTO auction.t_member( fk_member_id, f_free_coin, f_tm_stamp ) VALUES ( ?, ?, CURRENT_TIMESTAMP)";
        DB::executeNonQuery( $sql, array( $member->fk_member_id, $presentCoin ) );

        if ( $presentCoin ) {
            // サービスコイン分のt_pay_log(支払いログ)の作成
            $sql = "
INSERT INTO auction.t_pay_log(
    f_status, f_member_id, f_free_coin_add, f_coin_result, f_tm_stamp
) VALUES (
    2, ?, ?, ?, CURRENT_TIMESTAMP
)";
            DB::executeNonQuery( $sql, array( $member->fk_member_id, $presentCoin, $presentCoin ) );

            // サービスコイン分のt_stat_coin(集計テーブル)を更新 -> リアルタイムで処理する必要が無いためコメントアウト
            //$this->updateFreeCoin( $presentCoin );

            Logger::debug('[新規登録のコイン加算]会員ID:'.$member->fk_member_id.',加算コイン数:[無料:'.$presentCoin.']');
        }
        return $member;
    }

    /**
     * 会員登録処理に付随する処理
     * ・広告のカウントアップ
     * ・友達紹介
     * など
     */
    public function afterProcess( $memberId ) {

        // t_member.f_total_pay=0という前提で実装しています。

        // 会員情報の取得
        $sql = "
SELECT
    fk_member_id, fk_parent_ad, f_adpri_value, f_sex
FROM
    auction.t_member_master
WHERE
    fk_member_id=?
FOR UPDATE
";
        $member = DB::uniqueObject( $sql, array( $memberId ) );

        // 広告主、紹介者の取得
        if ( $member->fk_parent_ad ) {
            $sql = "SELECT fk_adcode_id, fk_admaster_id, f_type FROM auction.t_adcode WHERE fk_adcode_id = ?";
            $adcode = DB::uniqueObject( $sql, array( $member->fk_parent_ad ) );
        } else {
            // 設定されていない場合はNOPを適用
            $sql = "SELECT fk_adcode_id, fk_admaster_id, f_type FROM auction.t_adcode WHERE f_adcode = 0";
            $adcode = DB::uniqueObject( $sql );
        }

        // t_admaster(広告主マスタ)のカウントアップ
        $sql = "UPDATE auction.t_admaster SET f_counter=f_counter+1,f_tm_stamp=CURRENT_TIMESTAMP WHERE fk_admaster_id=?";
        DB::executeNonQuery( $sql, array( $adcode->fk_admaster_id ) );

        // t_ad_counter(広告主別時間別集計)のカウントアップ
        $sql = "SELECT fk_admaster_id FROM auction.t_adcounter WHERE f_stat_dt=DATE_FORMAT(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=? FOR UPDATE";
        $adCounter = DB::uniqueObject($sql, array( $adcode->fk_admaster_id ) );
        if ( $adCounter ) {
            // UPDATE
            $sql = "UPDATE auction.t_adcounter SET f_pc_memreg=IFNULL(f_pc_memreg, 0) + 1 WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=?";
        } else {
            // INSERT
            $sql = "INSERT INTO auction.t_adcounter (f_stat_dt,fk_admaster_id,f_pc_memreg) VALUES (date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),?,1)";
        }
        DB::executeNonQuery( $sql, array( $adcode->fk_admaster_id ) );

        // t_ad_stat(広告主別時間別集計)のカウントアップ...↑のt_adcounterとほぼ同じテーブル
        $sql = "SELECT fk_admaster_id FROM auction.t_stat_ad WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=? AND f_type=0 FOR UPDATE";
        $statAd = DB::uniqueObject($sql, array( $adcode->fk_admaster_id ) );
        $field = $member->f_sex ? 'f_pc_memreg_woman':'f_pc_memreg';
        if ( $statAd ) {
            // UPDATE
            $sql = "UPDATE auction.t_stat_ad SET $field=IFNULL($field, 0) + 1 WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=? AND f_type=0";
        } else {
            // INSERT
            $sql = "INSERT INTO auction.t_stat_ad (f_stat_dt,fk_admaster_id,f_type,$field) VALUES (date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),?,0,1)";
        }
        DB::executeNonQuery( $sql, array( $adcode->fk_admaster_id ) );

        /**
         * ■ 紹介者(親会員)へのサービスを付与
         * @see PayCondition.setFriendAddPrice()
         */
        if ( $adcode->f_type == 0 && $adcode->f_adcode != '0' ) {
            $sql = "SELECT fk_member_id, f_free_coin, f_activity FROM auction.t_member_master WHERE fk_adcode_id=? FOR UPDATE";
            $parent = DB::uniqueObject($sql, array( $adcode->fk_adcode_id ));
            if ( $parent ) {
                if ( $parent->f_activity == 1 ) {
                    // サービスの内容を取得
// TODO: f_adpricode > 0という条件は合っているのか？ >=ではないのか？
                    $sql = "
SELECT
    f_adpricode,f_pay_value
FROM
    auction.t_ad_condition
WHERE
    f_spend_value <= 0 AND f_type= 0 AND f_adpricode > 0 AND f_status=0
ORDER BY
    f_adpricode
";
                    $conditions = DB::getObjectByQuery( $sql );
                    if ( $conditions && is_array( $conditions ) ) {
                        $coin = 0;
                        $adPriCode = 0;
                        foreach ( $conditions as $row ) {
                            $coin += $row['f_pay_value'];
                            $adPriCode = $row['f_adpricode'];
                        }
                        if ( $coin ) {
                            // 紹介者へのコイン付与
                            $sql = "UPDATE auction.t_member SET f_free_coin=f_free_coin + ?,f_tm_stamp=CURRENT_TIMESTAMP WHERE fk_member_id=?";
                            DB::executeNonQuery( $sql, array($coin, $parent->fk_member_id));
                            // 支払いログの生成
                            $sql = "
INSERT INTO auction.t_pay_log (
    f_status, f_member_id, f_staff_id, f_pay_money, f_free_coin_add,
    fk_shiharai_type_id, f_coin_result, f_cert_status, f_tm_stamp
) VALUES (
    2, ?, NULL, 0, ?,
    -2, ?,0,CURRENT_TIMESTAMP
)";
                            DB::executeNonQuery( $sql, array( $parent->fk_member_id, $coin, ($parent->free_coin + $coin) ));

                            // サービスコイン分のt_stat_coin(集計テーブル)を更新 -> リアルタイムで処理する必要が無いためコメントアウト
                            //$this->updateFreeCoin( $presentCoin );

                            Logger::debug('[親会員にコイン加算]会員ID:'.$parent->fk_member_id.',加算コイン数:[無料:'.$coin.']');
                        }
                        // この会員の状態を変更
                        $sql = "UPDATE auction.t_member_master SET f_adpricode=? WHERE fk_member_id=?";
                        DB::executeNonQuery( $sql, array($adPriCode, $memberId) );
                    } else {
                        Logger::debug('紹介者に付与するサービス設定はありません。');
                    }
                } else {
                    Logger::warn('紹介者は本会員ではありません。(member_id='.$parent->fk_member_id.',activity='.$parent->f_activity.')'.' '.__FILE__.'('.__LINE__.')');
                }
            } else {
                Logger::warn('紹介者は存在しません。(fk_adcode_id='.$adcode->fk_adcode_id.',adcode='.$adcode->f_adcode.')'.' '.__FILE__.'('.__LINE__.')');
            }
        }
    }

    /**
     * 入力チェック
     *
     * @return bool
     * @see InputCheck.registInputCheck() 原則としてチェック項目を踏襲しています。
     *
     * (エラーコード表)
     * -1: 広告コードが存在しない
     * -2: ハンドル名に不正な文字が入っている
     * -3: 使用できないハンドル名
     * -4: ログインIDに使用できない文字が含まれている
     * -5: ログインIDはすでに使用されている
     * -6: メールアドレスの入力エラー
     * -7: 地域の選択エラー
     * -8: 職業の選択エラー
     * -9: 番組コードエラー
     * -99: その他のエラー
     */
    public function validate( &$reqParamArray, &$statusCode, &$message ) {
        $statusCode = '-99';
        $message = '不明なエラーが発生しました。';

        // 画像認証
        $name = Config::value("SESSION_VERIFY_NAME");
        if ( $_SESSION[($name)] != md5(strtolower($reqParamArray['verify'])) ) {
            $statusCode = '-99';
            $message = Msg::get('REGISTER_VERIFY_INVALID');
            return FALSE;
        }

        // 規約に同意する
        if( !isset($reqParamArray['agr']) || $reqParamArray['agr'] != 1 ) {
            $statusCode = '-99';
            $message = Msg::get("REGISTER_AGREEMENT_NO");
            return FALSE;
        }

        // 広告コード存在チェック(指定された場合のみ)
        $adcode = NULL;
        if ( isset( $reqParamArray['ad'] ) && strlen( $reqParamArray['ad'] ) ) {
            $adcode = $this->getAdcode( $reqParamArray['ad'] );
        }
        if ( $adcode ) {
            // リファラチェック
            if ( strlen($adcode->f_mem_reg_urls) ) {
                $match = FALSE;
                $referer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"]:'';
                if ( strlen($referer) ) {
                    $urlArray = explode( ';', $adcode->f_mem_reg_urls );
                    foreach ( $urlArray as $url ) {
                        if ( strlen($url) && strpos( $referer, $url ) !== FALSE ) {
                            $match = TRUE;
                            break;
                        }
                    }
                }
                if ( !$match ) {
                    $statusCode = '-9';
                    $message = constant('MEM_DIRECT_REG_REFERER_NOT_PERMITTED');
                    return FALSE;
                }
            }
        } else {
            // 広告コード存在せず
            $statusCode = '-1';
            $message = constant('MEM_DIRECT_REG_ADCODE_NOT_FOUND');
            return FALSE;
        }

        // アフィリキー存在チェック ⇒ 設定されるかまちまちなパラメータらしいのでチェック不要

        //++ InputCheck.registInputCheck()を踏襲したチェック処理 START

        // 属性名の変換
        $p = $this->convert( $reqParamArray );
        $model = $this->member;
        // ハンドル名
        if ( empty($p["handle"]) ) {
            $statusCode = '-3';
            $message = Msg::get("REGISTER_HANDLE_EMPTY");
            return FALSE;
        } else {
            if ( InputCheck::isExistKeyWord($p["handle"]) || InputCheck::isFaultWord($p["handle"]) ) {
                $statusCode = '-2';
                $message = sprintf( Msg::get("INPUT_EXIST_KEYWORD"), Msg::get("HANDLE") );
                return FALSE;
            } elseif ( $model->isExistedHandle( $p["handle"] ) ) {
                $statusCode = '-3';
                $message = Msg::get("REGISTER_HANDLE_EXISTED");
                return FALSE;
            } else {
                $tmpMessageArray = array();
                InputCheck::addMaxLenCheck($tmpMessageArray, $p["handle"], Msg::get("HANDLE_LEN"), Msg::get("HANDLE"));
                if ( $tmpMessageArray ) {
                    $statusCode = '-3';
                    $message = $tmpMessageArray[0];
                    return FALSE;
                }
            }
        }
        // ログインID
        if( empty($p["login_id"]) ) {
            $statusCode = '-99';
            $message = Msg::get("REGISTER_LOGINID_EMPTY");
            return FALSE;
        } else {
            $tmpMessageArray = array();
            InputCheck::addMaxLenCheck($tmpMessageArray, $p["login_id"], Msg::get("LOGINID_LEN"), Msg::get("LOGINID"));
            if ( $tmpMessageArray ) {
                $statusCode = '-99';
                $message = $tmpMessageArray[0];
                return FALSE;
            } else {
                if( !ereg("^[A-Za-z0-9]+$", $p["login_id"]) ) {
                    $statusCode = '-4';
                    $message = Msg::get("REGISTER_LOGIN_INVALID_FORMAT");
                    return FALSE;
                } elseif ( $model->isExistedLoginId( $p["login_id"] ) ) {
                    $statusCode = '-5';
                    $message = Msg::get("REGISTER_LOGINID_EXISTED");
                    return FALSE;
                }
            }
        }
        // ログインパスワード
        if ( empty($p["login_pass"]) ) {
            $statusCode = '-99';
            $message = Msg::get("REGISTER_PASS_EMPTY");
            return FALSE;
        } else {
            $tmpMessageArray = array();
            InputCheck::addBetweenLenCheck($tmpMessageArray, $p["login_pass"], PASSWORD_MIN_LEN, Msg::get("PASSWORD_LEN"), Msg::get("PASSWORD"));
            if ( $tmpMessageArray ) {
                $statusCode = '-99';
                $message = $tmpMessageArray[0];
                return FALSE;
            }
        }

        // PCのみ、チェックする必要
        $isPc = Config::value("PC_DIR") == AgentInfo::getAgentDir();
        if ( $isPc ) {
            // メールアドレス
            if( empty($p["mail_address"]) ) {
                $statusCode = '-99';
                $message = Msg::get("REGISTER_EMAIL_EMPTY");
                return FALSE;
            } else {
                $tmpMessageArray = array();
                InputCheck::addMaxLenCheck($tmpMessageArray, $p["mail_address"], Msg::get("EMAIL_LEN"), Msg::get("EMAIL"));
                if ( $tmpMessageArray ) {
                    $statusCode = '-6';
                    $message = $tmpMessageArray[0];
                    return FALSE;
                } else {
                    // 書式チェック
                    if ( Utils::isMailAddress( $p["mail_address"] ) ) {
                        //blackメールチェック
                        if( $model->isBlackMail( $p["mail_address"] ) ) {
                            $statusCode = '-6';
                            $message = Msg::get("IS_BLACK_EMAIL");
                            return FALSE;
                        }
                    } else {
                        $statusCode = '-6';
                        $message = Msg::get("REGISTER_EMAIL_INVALID");
                        return FALSE;
                    }
                }
            }
            // 仮登録チェック(仮登録済みの会員かどうか、チェックする)
            $memberId = $model->getTempMemberIdByMail( $p["mail_address"] );
            if ( $memberId ) {
                // 仮登録している場合、エラーで弾く(本来、仮登録状態を利用しないフローであるため)
                $statusCode = '-6';
                $message = Msg::get("REGISTER_EMAIL_EXISTED");
                return FALSE;
            }
            // 同一IPチェック
            if( FORBID_REG_PCMEMIP_FLAG == 1 ) {
                if( $model->isExistedIp( $_SERVER['REMOTE_ADDR'] ) ) {
                    $statusCode = '-99';
                    $message = Msg::get("REGISTER_IP_EXISTED");
                    return FALSE;
                }
            }
        }
        // メールアドレスの存在チェック
        if ( $model->isExistedEmail( $p["mail_address"], $isPc ) ) {
            $statusCode = '-6';
            $message = Msg::get("REGISTER_EMAIL_EXISTED");
            return FALSE;
        }

        // 電話番号(入力された場合のみチェック)
        if( isset($p["tel_no"]) && strlen($p["tel_no"]) ) {
            if( !ereg("[0-9]+", $p["tel_no"]) ) {
                $statusCode = '-99';
                $message = Msg::get("REGISTER_TEL_INVALID");
                return FALSE;
            } else {
                $tmpMessageArray = array();
                InputCheck::addMaxLenCheck($tmpMessageArray, $p["tel_no"], Msg::get("TEL_LEN"), Msg::get("TEL"));
                if ( $tmpMessageArray ) {
                    $statusCode = '-99';
                    $message = $tmpMessageArray[0];
                    return FALSE;
                }
            }
        }

        // 誕生日のチェック(入力された場合のみチェック)
        $existsY = FALSE;
        $existsM = FALSE;
        $existsD = FALSE;
        if ( isset($p["birth_year"]) && strlen($p["birth_year"]) ) {
            $existsY = TRUE;
            if( $p["birth_year"] === "00" ) {
                $statusCode = '-99';
                $message = Msg::get("REGISTER_BIRTH_YEAR_EMPTY");
                return FALSE;
            }
        }
        if ( isset($p["birth_month"]) && strlen($p["birth_month"]) ) {
            $existsM = TRUE;
            if( $p["birth_month"] === "00" ) {
                $statusCode = '-99';
                $message = Msg::get("REGISTER_BIRTH_MONTH_EMPTY");
                return FALSE;
            }
        }
        if ( isset($p["birth_month"]) && strlen($p["birth_month"]) ) {
            $existsD = TRUE;
            if( $p["birth_day"] === "00" ) {
                $statusCode = '-99';
                $message = Msg::get("REGISTER_BIRTH_DAY_EMPTY");
                return FALSE;
            }
        }
        if ( $existsY && $existsM && $existsD ) {
            if ( checkdate( $p["birth_month"], $p["birth_day"], $p["birth_year"] ) === FALSE ) {
                $statusCode = '-99';
                $message = Msg::get("ERROR_BIRTH_DAY");
                return FALSE;
            }
        }

        // 氏名(入力された場合のみチェック)
        if ( isset( $p["onamae"] ) && strlen( $p["onamae"] ) ) {
            $tmpMessageArray = array();
            InputCheck::addMaxLenCheck($tmpMessageArray, $p["onamae"], Msg::get("NAME_LEN"), Msg::get("NAME"));
            if ( $tmpMessageArray ) {
                $statusCode = '-99';
                $message = $tmpMessageArray[0];
                return FALSE;
            }
        }
        // 地域(システムパラメータ設定で制御)
        if ( MEM_REG_AREA_FLG ) {
            if ( !isset($p["area"]) || !$p["area"] ) {
                $statusCode = '-7';
                $message = Msg::get("REGISTER_AREA_NULL");
                return FALSE;
            }
        }
        // 職業(システムパラメータ設定で制御)
        if ( MEM_REG_JOB_FLG ) {
            if( !isset($p["job"]) || !$p["job"] ) {
                $statusCode = '-8';
                $message = Msg::get("REGISTER_JOB_NULL");
                return FALSE;
            }
        }

        $statusCode = '0';
        return TRUE;
    }

    /**
     * 仮登録完了メールの送信処理
     * @param <type> $member 会員データオブジェクト
     * @return bool
     */
    public function sendPreRegisteredMail( $member ) {
        if ( !class_exists( 'MailReserver' ) ) {
            import("classes.utils.MailReserver");
        }
        $to = $member->f_mail_address_pc;
        $tpl = Config::value("APP_PATH")."/templates/".AgentInfo::getAgentDir()."/parts/memreg_premail.html";
        $from = "From: ".MAIL_ADDRESS."\nReturn-Path:".MAIL_ADDRESS;
        $params = array(
            "member_name"=>$member->f_handle,"site_name"=>SITE_NAME,"point_name"=>POINT_NAME,
            "reg_confirm_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/login/confirm?code=".$member->f_adcode,
            "question_url"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")."/guide/contact",
            "url_home"=>"http://".Config::value("HTTP_HOST").Config::value("SERVER_PATH")
        );
        $judge = MailReserver::sendTemplateMail( MailReserver::REGIST,REGIST_MAIL_SUBJECT, $tpl, $params, $from, $to );
        return $judge;
    }

    /**
     * 本登録完了メールの送信処理
     * @param <type> $member 会員データオブジェクト
     * @return bool
     */
    public function sendRegisteredMail( $member ) {
        if ( !class_exists( 'MailReserver' ) ) {
            import("classes.utils.MailReserver");
        }
        $judge = MailReserver::sendRegistSuccessMail( $member );
        // 現状、MailReserver::sendRegistSuccessMail()は処理ステータスを返却しないようなので、ここでも常にTRUEを返却
        return TRUE;
    }

    /**
     * ■コイン集計テーブル(t_stat_coin)の更新処理
     *
     * ⇒　リアルタイム反映不要という仕様が決まったため未使用
     *
     * @param <type> $coin 払い出したサービスコイン枚数
     * @return void
     */
    protected function updateFreeCoin( $coin ) {
        $sql = "
SELECT
    f_stat_dt, f_type
FROM
    auction.t_stat_coin
WHERE
    f_stat_dt=DATE_FORMAT(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0 FOR UPDATE
";
        $statCoin = DB::uniqueObject( $sql );
        if ( $statCoin ) {
            $sql = "
UPDATE
    auction.t_stat_coin
SET
    f_scoin_gen=f_scoin_gen+?
WHERE
    f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND f_type=0
";
        } else {
            $sql = "
INSERT INTO auction.t_stat_coin (
    f_stat_dt,f_type,f_scoin_gen
) VALUES (
    date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),0,?
)";
        }
        DB::executeNonQuery( $sql, array( $coin ) );
    }
}
?>
