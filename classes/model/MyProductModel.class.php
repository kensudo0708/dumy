<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
import(FRAMEWORK_DIR.'.model.Model');
/**
 * マイページの商品
 *
 * @author ark
 */
class MyProductModel extends Model {
    //put your code here

    /**
     *入札中商品の数
     * @param <int> $member_id
     * @return <type>
     */
    public function getBidProductCount($member) {
//G.Chin 2010-10-09 chg sta
/*
        return DB::result_rows("SELECT p.fk_products_id
                FROM (SELECT DISTINCT f_products_id FROM t_bid_log WHERE fk_member_id=?) bid
                INNER JOIN t_products p ON p.fk_products_id=bid.f_products_id
                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
                WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) and mast.f_delflg <> 9
                UNION ALL
                SELECT p.fk_products_id
                FROM (SELECT DISTINCT f_products_id FROM t_bid_log WHERE fk_member_id=?) bid
                INNER JOIN t_products p ON p.fk_products_id=bid.f_products_id
                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
                WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5)
                AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL) and mast.f_delflg <> 9",
                array($member->fk_member_id,$member->fk_member_id,$member->t_hummer_count));
*/
//		$sql  = "SELECT ";
//		$sql .= "DISTINCT p.fk_products_id ";
//		$sql .= "FROM (SELECT DISTINCT f_products_id FROM t_bid_log WHERE fk_member_id=$member->fk_member_id) bid,";
//		$sql .= "t_products p,t_products_master mast,t_auction_type `type`,t_bid_log bid ";
//		$sql .= "WHERE `type`.f_bidder_flag = 0 ";
//		$sql .= "AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) ";
//		$sql .= "and mast.f_delflg <> 9 ";
//		$sql .= "AND (`type`.f_bidder_param1 >=$member->t_hummer_count OR `type`.f_bidder_param1 IS NULL) ";
//		$sql .= "and bid.fk_member_id=$member->fk_member_id ";
////		$sql .= "and p.fk_products_id=bid.f_products_id ";
//		$sql .= "and p.fk_products_id=mast.fk_products_id ";
//		$sql .= "and mast.fk_auction_type_id=`type`.fk_auction_type_id ";
//echo $sql;
//                $sql="SELECT DISTINCT(bt.f_products_id)
//                FROM t_bid_log bt ,t_products p
//                WHERE bt.f_products_id = p.fk_products_id
//                AND NOT (p.f_status = 2 OR p.f_status = 0 OR p.f_status = 9)
//                AND bt.fk_member_id =$member->fk_member_id";

        $sql = "SELECT DISTINCT(bt.f_products_id)
                FROM (SELECT f_products_id FROM t_bid_log WHERE fk_member_id =$member->fk_member_id) bt,
                (SELECT fk_products_id FROM t_products WHERE f_status =1 OR f_status =3 OR f_status = 4 OR f_status = 5 OR f_status = 6 OR f_status =7) p
                WHERE bt.f_products_id = p.fk_products_id ";
        //echo $sql;
		return DB::result_rows($sql);
//G.Chin 2010-10-09 chg end
    }

    /**
     *入札中商品の数
     * @param <int> $member_id
     * @param <int> $product_id
     * @return <type>
     */
    public function getProductBidCount($member_id,$product_id) {
        return DB::result_rows("SELECT fk_bid_log_id FROM t_bid_log WHERE f_products_id=? AND fk_member_id=?",
                array($product_id,$member_id));
    }

    /**
     *自動入札中商品の数
     * @param <int> $member_id
     * @return <type>
     */
    public function getAutoBidProductCount($member) {
        // INLINE VIEWを展開すると性能が下がったため現状維持 itoh
        return DB::result_rows("SELECT bid.fk_products_id
                            FROM (SELECT DISTINCT fk_products_id FROM t_auto_bidder WHERE fk_member_id=?
                                   AND NOT (f_status=1 or f_status=3 or f_status=4 or f_status=5 or f_status=9)) bid
                            LEFT OUTER JOIN t_products product ON product.fk_products_id=bid.fk_products_id
                            INNER JOIN t_products_master mast ON mast.fk_products_id=bid.fk_products_id AND mast.f_delflg<>9
                            WHERE product.f_status=1 OR product.f_status=3 OR product.f_status=4 OR product.f_status=5", array($member->fk_member_id));
//        return DB::result_rows("SELECT p.fk_products_id
//                    FROM (SELECT DISTINCT fk_products_id FROM t_auto_bidder WHERE fk_member_id=?) bid
//                    INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                    INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                    INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                    WHERE `type`.f_bidder_flag = 0 AND p.f_status = 1
//                    UNION ALL
//                    SELECT p.fk_products_id
//                    FROM (SELECT DISTINCT fk_products_id FROM t_auto_bidder WHERE fk_member_id=?) bid
//                    INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                    INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                    INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                    WHERE `type`.f_bidder_flag >0 AND p.f_status=1
//                    AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL)"
//                    ,array($member->fk_member_id,$member->fk_member_id,$member->t_hummer_count));
    }

    /**
     *watchしている商品の数
     * @param <int> $member_id
     * @return <type>
     */
    public function getWatchProductCount($member) {
        return DB::result_rows("SELECT bid.fk_products_id
                FROM (SELECT DISTINCT fk_products_id FROM t_watch_list WHERE fk_member_id=?) bid
                INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
                WHERE
                (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg<>9
		AND (
		`type`.f_bidder_flag = 0
		) OR (
			`type`.f_bidder_flag >0 AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL)
		)",
                array($member->fk_member_id,$member->t_hummer_count));
//        return DB::result_rows("SELECT p.fk_products_id
//                FROM (SELECT DISTINCT fk_products_id FROM t_watch_list WHERE fk_member_id=?) bid
//                INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg<>9
//                UNION ALL
//                SELECT p.fk_products_id
//                FROM (SELECT DISTINCT fk_products_id FROM t_watch_list WHERE fk_member_id=?) bid
//                INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg<>9
//                AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL)",
//                array($member->fk_member_id,$member->fk_member_id,$member->t_hummer_count));
    }

    /**
     *落札済み商品の数を取得
     * @return <type>
     */
    public function getEndProductCount($member_id) {
        return DB::result_rows("SELECT fk_end_products_id FROM t_end_products WHERE f_last_bidder=? AND (f_status=0 OR f_status=2 OR f_status=3 OR f_status=5)", array($member_id));
    }

    /**
     *入札中商品を取得する
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     *  @param <int> $member_id
     * @return <type>
     */
    public function getBidProduct($s,$t,$member,$isPc=true) {
        $photo = $isPc ? "f_photo1" : "f_photo1mb";

                $sql ="SELECT DISTINCT bt.f_products_id id,p.f_now_price AS price ,
                        p.f_last_bidder_handle last_bidder ,p.f_auction_time `time`,
                        pm.f_products_name name,pm.image AS image,pm.fk_auction_type_id auction_type,
                        pm.f_market_price market_price
                        FROM
                        (SELECT f_products_id FROM t_bid_log WHERE fk_member_id =$member->fk_member_id) bt,
                        (SELECT fk_products_id,f_now_price,f_last_bidder_handle,f_auction_time FROM t_products WHERE f_status =1 OR f_status =3 OR f_status = 4 OR f_status = 5 OR f_status = 6 OR f_status =7) p,
                        (SELECT fk_products_id,f_products_name ,$photo as image ,fk_auction_type_id ,f_market_price FROM t_products_master WHERE f_delflg = 0 AND f_start_time > DATE_SUB(NOW(),INTERVAL 1 MONTH) ) pm,t_auction_type att
                        WHERE bt.f_products_id = p.fk_products_id AND p.fk_products_id = pm.fk_products_id AND att.fk_auction_type_id = pm.fk_auction_type_id
                        ORDER BY  f_auction_time LIMIT $s,$t";
		return DB::getObjectByQuery($sql);
    }

    /**
     *watchしている商品を取得する
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     * @param <type> $member_id
     */
    public function getWatchProduct($s,$t,$member,$isPc=true) {
        $photo = $isPc ? "f_photo1" : "f_photo1mb";
        return DB::getObjectByQuery("SELECT fk_products_id       AS id,
                          f_now_price          AS price,
                          f_last_bidder_handle  AS last_bidder,
                          f_auction_time	 AS `time`,
                          f_products_name	       AS `name`,
                	  $photo	       	       AS image,
                          fk_auction_type_id         AS auction_type,
                          f_auction_name
                        FROM
                        (SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name
                        FROM (SELECT DISTINCT fk_products_id FROM t_watch_list WHERE fk_member_id=?) bid
                        INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
			WHERE
			(p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg<>9
			AND (
				`type`.f_bidder_flag = 0 OR (
					`type`.f_bidder_flag >0 AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL)
				)
			)
                        ) product  ORDER BY f_auction_time
                        LIMIT $s,$t", array($member->fk_member_id,$member->t_hummer_count));
//        return DB::getObjectByQuery("SELECT fk_products_id       AS id,
//                          f_now_price          AS price,
//                          f_last_bidder_handle  AS last_bidder,
//                          f_auction_time	 AS `time`,
//                          f_products_name	       AS `name`,
//                $photo	       	       AS image,
//                          fk_auction_type_id         AS auction_type,
//                          f_auction_name
//                        FROM
//                        (SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name
//                        FROM (SELECT DISTINCT fk_products_id FROM t_watch_list WHERE fk_member_id=?) bid
//                        INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag = 0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg<>9
//                        UNION ALL
//                        SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id,`type`.f_auction_name
//                        FROM (SELECT DISTINCT fk_products_id FROM t_watch_list WHERE fk_member_id=?) bid
//                        INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag >0 AND (p.f_status=1 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5) AND mast.f_delflg<>9
//                        AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL)
//                        ) product  ORDER BY f_auction_time
//                        LIMIT $s,$t", array($member->fk_member_id,$member->fk_member_id,$member->t_hummer_count));
    }

    //    /**
//     *自動入札中商品を取得する
//     * @param <type> $s 最初レコードのindex
//     * @param <type> $t 取得するレコードの数
//     * @param <type> $member_id
//     */
//    public function getAutoBidProduct($s,$t,$member_id,$isPc=true) {
//        $photo = $isPc ? "mast.f_photo1" : "mast.f_photo1mb";
//        return DB::getObjectByQuery("SELECT  product.fk_products_id AS id,product.f_now_price AS price,product.f_last_bidder_handle AS last_bidder,
//                                product.f_auction_time AS `time`,mast.f_products_name AS `name`,$photo AS image,
//                                t.fk_auction_type_id AS auction_type
//                                FROM t_products product
//                                LEFT OUTER JOIN t_products_master mast ON product.fk_products_id=mast.fk_products_id
//                                LEFT OUTER JOIN t_auction_type t ON mast.fk_auction_type_id=t.fk_auction_type_id
//                                WHERE product.f_status=1
//                                AND product.fk_products_id IN (SELECT DISTINCT fk_products_id FROM t_auto_bidder WHERE fk_member_id=?)
//                                ORDER BY product.f_auction_time
//                                LIMIT $s,$t", array($member_id));
//    }

    /**
     *自動入札中商品を取得する
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     * @param <int> $member 会員ID
     * @param <boolean> $isPc
     */
    public function getAutoBidProduct($s,$t,$member,$isPc=true) {
        $photo = $isPc ? "mast.f_photo1" : "mast.f_photo1mb";
        return DB::getObjectByQuery(
                "SELECT auto.fk_auto_id,$photo AS image,mast.f_products_name,mast.fk_auction_type_id AS auction_type,auto.fk_products_id,auto.fk_member_id,
                auto.f_min_price,auto.f_max_price,(auto.f_free_coin+auto.f_buy_coin) AS coins,auto.f_status,auto.stauts_des,`type`.f_auction_name
                FROM
                (SELECT fk_auto_id,fk_products_id,fk_member_id,f_min_price,f_max_price,f_free_coin,f_buy_coin,f_status,
                CASE f_status WHEN 0 THEN 'キャンセル' WHEN 2 THEN '中止中' END AS stauts_des
                FROM t_auto_bidder WHERE fk_member_id=? AND NOT (t_auto_bidder.f_status=1 OR t_auto_bidder.f_status=3 OR t_auto_bidder.f_status=4 OR t_auto_bidder.f_status=5 or t_auto_bidder.f_status=9)
                GROUP BY fk_products_id,fk_member_id,f_min_price,f_max_price,f_free_coin,f_buy_coin,f_status
                LIMIT $s,$t) auto
                INNER JOIN t_products_master mast ON auto.fk_products_id=mast.fk_products_id AND mast.f_delflg<>9
                LEFT OUTER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id",
                array($member->fk_member_id));
//        $photo = $isPc ? "f_photo1" : "f_photo1mb";
//        return DB::getObjectByQuery("SELECT fk_products_id       AS id,
//                          f_now_price          AS price,
//                          f_last_bidder_handle  AS last_bidder,
//                          f_auction_time	 AS `time`,
//                          f_products_name	       AS `name`,
//                          $photo	       	       AS image,
//                          fk_auction_type_id         AS auction_type
//                        FROM
//                        (SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id
//                        FROM (SELECT DISTINCT fk_products_id FROM t_auto_bidder WHERE fk_member_id=?) bid
//                        INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag = 0 AND p.f_status = 1
//                        UNION ALL
//                        SELECT p.fk_products_id,p.f_now_price,p.f_last_bidder_handle,p.f_auction_time,mast.f_products_name,mast.f_photo1,mast.f_photo1mb,mast.fk_auction_type_id
//                        FROM (SELECT DISTINCT fk_products_id FROM t_auto_bidder WHERE fk_member_id=?) bid
//                        INNER JOIN t_products p ON p.fk_products_id=bid.fk_products_id
//                        INNER JOIN t_products_master mast ON p.fk_products_id=mast.fk_products_id
//                        INNER JOIN t_auction_type `type` ON mast.fk_auction_type_id=`type`.fk_auction_type_id
//                        WHERE `type`.f_bidder_flag >0 AND p.f_status=1
//                        AND (`type`.f_bidder_param1 >=? OR `type`.f_bidder_param1 IS NULL)
//                        ) product  ORDER BY f_auction_time
//                        LIMIT $s,$t", array($member->fk_member_id,$member->fk_member_id,$member->t_hummer_count));
    }


//    /**
//     *自動入札設定をキャンセル
//     * @param <int> $product_id
//     * @param <int> $member_id
//     * @return <boolean>
//     */
//    public function autoBidCancel($product_id,$member_id) {
//        try {
//            DB::begin();
//            //自動入札設定をキャンセル処理
//            DB::executeNonQuery("UPDATE t_auto_bidder SET f_status=2 WHERE fk_products_id=? AND fk_member_id=?",
//                    array($product_id,$member_id));
//            DB::commit();
//            return true;
//        }
//        catch (Exception $e) {
//            DB::rollback();
//            return FALSE;
//        }
//    }

    /**
     *自動入札設定をキャンセル
     * @param <int> $auto_id
     * @return <boolean>
     */
    public function autoBidCancel($auto_id) {
        try {
            DB::begin();
            //自動入札設定をキャンセル処理
            DB::executeNonQuery("UPDATE t_auto_bidder SET f_status=2 WHERE fk_auto_id=? and f_status =0",
                    array($auto_id));
            DB::commit();
            return true;
        }
        catch (Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }


    /**
     *落札済み商品を取得
     * @param <type> $s 最初レコードのindex
     * @param <type> $t 取得するレコードの数
     * @param <type> $member_id
     */
    public function getEndProduct($s,$t,$member_id,$isPc=true) {
        //echo "aaaa";
        if($s !=0)
        {
            DB::executeNonQuery("SET @row_num = 1;");
        }
        else if($s == 0)
        {
            DB::executeNonQuery("SET @row_num = 0;");
        }
        $photo = $isPc ? "product.f_photo1" : "product.f_photo1mb";
//G.Chin AWKT-809 2010-12-02 chg sta
/*
        return DB::getObjectByQuery("SELECT (@row_num:=@row_num+1) AS row_number,product.fk_end_products_id AS id,SUBSTR(product.f_products_name,1,10) AS `name`,product.f_end_price AS price,product.fk_address_id,product.f_status,product_master.f_address_flag as flag,
				CASE WHEN product.f_status=0 OR (product.f_status=2 AND product.fk_address_id IS NULL) THEN 'お客様待ち'
				WHEN product.f_status=2 AND product.fk_address_id IS NOT NULL THEN '配送待ち'
				WHEN product.f_status=3 THEN '受付完了' WHEN product.f_status=1 THEN 'キャンセル' END AS `status`,
				CASE WHEN pay.f_cert_status IS NOT NULL THEN '認証中' WHEN product.f_status=0 THEN '支払う' WHEN product.f_status=2 THEN '済み'
                                        WHEN product.f_status=5 THEN '出荷依頼済み' END AS pay_status,
                                CASE WHEN product.fk_address_id IS NULL AND NOT product.f_status=1 THEN '決定する'
                                ELSE (SELECT CONCAT(ad.f_name,' 〒',IFNULL(ad.f_post_code,''),' ',IFNULL(pref.t_pref_name,'')) FROM t_address ad
				LEFT OUTER JOIN t_pref pref ON pref.t_pref_id=ad.fk_perf_id WHERE ad.fk_address_id=product.fk_address_id)END AS address_status,
                                CASE WHEN product.f_status=0 THEN '購入やめる' END AS cancel_status,product.fk_address_id,
                                product.f_end_time AS `time`,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name
                                FROM t_end_products product
                                LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
                                LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
                                LEFT OUTER JOIN auction.t_products_master `product_master` ON `product_master`.fk_products_id = product.fk_end_products_id
                                LEFT OUTER JOIN (SELECT f_cert_status,fk_products_id FROM t_pay_log WHERE f_cert_status=3 GROUP BY f_member_id,fk_products_id,f_cert_status) pay ON pay.fk_products_id=product.fk_end_products_id
                                WHERE product.f_last_bidder=? AND ( product.f_status=0 or product.f_status=2 or product.f_status=3 or product.f_status=5)
                                ORDER BY product.f_status,product.f_end_time desc 
                                LIMIT $s,$t", array($member_id));
*/
        return DB::getObjectByQuery("SELECT (@row_num:=@row_num+1) AS row_number,product.fk_end_products_id AS id,SUBSTR(product.f_products_name,1,10) AS `name`,product.f_end_price AS price,product.fk_address_id,product.f_status,product_master.f_address_flag as flag,
				CASE WHEN product.f_status=0 OR (product.f_status=2 AND product.fk_address_id IS NULL) THEN 'お客様待ち'
				WHEN product.f_status=2 AND product.fk_address_id IS NOT NULL THEN '配送待ち'
				WHEN product.f_status=3 THEN '受付完了' WHEN product.f_status=1 THEN 'キャンセル' WHEN product.f_status=5 THEN '出荷依頼済み' END AS `status`,
				CASE WHEN pay.f_cert_status IS NOT NULL THEN '認証中' WHEN product.f_status=0 THEN '支払う' WHEN product.f_status=2 THEN '済み' END AS pay_status,
                                CASE WHEN product.fk_address_id IS NULL AND NOT product.f_status=1 THEN '決定する'
                                ELSE (SELECT CONCAT(ad.f_name,' 〒',IFNULL(ad.f_post_code,''),' ',IFNULL(pref.t_pref_name,'')) FROM t_address ad
				LEFT OUTER JOIN t_pref pref ON pref.t_pref_id=ad.fk_perf_id WHERE ad.fk_address_id=product.fk_address_id)END AS address_status,
                                CASE WHEN product.f_status=0 THEN '購入やめる' END AS cancel_status,product.fk_address_id,
                                product.f_end_time AS `time`,$photo AS image,member.f_handle AS last_bidder,`type`.fk_auction_type_id AS auction_type,`type`.f_auction_name
                                FROM t_end_products product
                                LEFT OUTER JOIN t_member_master member ON member.fk_member_id=product.f_last_bidder
                                LEFT OUTER JOIN t_auction_type `type` ON `type`.fk_auction_type_id=product.fk_auction_type_id
                                LEFT OUTER JOIN auction.t_products_master `product_master` ON `product_master`.fk_products_id = product.fk_end_products_id
                                LEFT OUTER JOIN (SELECT f_cert_status,fk_products_id FROM t_pay_log WHERE f_cert_status=3 GROUP BY f_member_id,fk_products_id,f_cert_status) pay ON pay.fk_products_id=product.fk_end_products_id
                                WHERE product.f_last_bidder=? AND ( product.f_status=0 or product.f_status=2 or product.f_status=3 or product.f_status=5)
                                ORDER BY product.f_status,product.f_end_time desc 
                                LIMIT $s,$t", array($member_id));
//G.Chin AWKT-809 2010-12-02 chg end
    }

    /**
     *指定した落札済み商品IDで商品取得する
     * @param <type> $pro_id
     * @return <type>
     */
    public function getEndProductById($pro_id,$member_id=null) {
        if(empty ($member_id))
            return DB::uniqueObject("SELECT en.fk_end_products_id,en.fk_item_category_id,en.f_photo1,en.f_photo1mb,en.f_products_name,en.f_end_price,mast.f_address_flag
                FROM t_end_products en
                LEFT OUTER JOIN t_products_master mast ON mast.fk_products_id=en.fk_end_products_id
                WHERE en.fk_end_products_id=?", array($pro_id));
        else
            return DB::uniqueObject("SELECT en.fk_end_products_id,en.fk_item_category_id,en.f_photo1,en.f_photo1mb,en.f_products_name,en.f_end_price,mast.f_address_flag
                FROM t_end_products en
                LEFT OUTER JOIN t_products_master mast ON mast.fk_products_id=en.fk_end_products_id
                WHERE en.fk_end_products_id=? AND en.f_last_bidder=?", array($pro_id,$member_id));
    }

    /**
     *指定した商品を指定したメンバーのウォッチリストに追加
     * @param <type> $member_id
     * @param <type> $product_id
     * @return <boolean>
     */
    public function isExistWatchList($member_id,$product_id) {
        $row = DB::result_rows("SELECT fk_watch_id FROM t_watch_list WHERE fk_products_id=? AND fk_member_id=?",
                array($product_id,$member_id));

        return $row >0 ? true : false;
    }


    /**
     *指定した商品を指定したメンバーのウォッチリストに追加
     * @param <int> $member_id メンバーID
     * @param <int> $product_id 商品ID
     * @return <boolean>
     */
    public function addWatchList($member_id,$product_id) {
        $mail_status = 1;
        if(CALL_WATCH_SECOND > 0 )
        {
            $sec=CALL_WATCH_SECOND>60?CALL_WATCH_SECOND :60;
            
            $row = DB::result_rows("SELECT fk_products_id FROM auction.t_products WHERE  f_auction_time >? AND fk_products_id =?",
                array($sec,$product_id));

            if($row > 0)
            {
                $mail_status =0;
            }
        }
        
        try {
            DB::begin();
            //マスター情報を削除
            DB::executeNonQuery("INSERT INTO t_watch_list (fk_products_id,fk_member_id,f_tm_stamp,f_mail_status)
                        VALUES (?,?,CURRENT_TIMESTAMP,?)", array($product_id,$member_id,$mail_status));
            DB::commit();
            return true;
        }
        catch (Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    /**
     *指定された商品がその会員のウォッチリストに存在するかどうか、チェックする
     * @param <int> $member_id
     * @param <int> $product_id
     * @return <boolean> 存在する場合：true
     */
    public function isExistedWatchList($member_id,$product_id) {
        $count = DB::result_rows("SELECT fk_watch_id FROM t_watch_list WHERE fk_member_id=? AND fk_products_id=?",
                array($member_id,$product_id));
        return $count > 0 ? true : false;
    }

    /**
     *指定された商品が自動入札設定済み、チェックする
     * @param <type> $member_id
     * @param <type> $product_id
     * @return <boolean>
     */
    public function isAutoBidProduct($product_id,$member_id) {
        $row = DB::result_rows("SELECT fk_auto_id FROM t_auto_bidder WHERE fk_products_id=? AND fk_member_id=? AND f_status=0",
                array($product_id,$member_id));
        if(defined("SET_AUTO_BID_QTY"))
            $qty=Utils::getInt(SET_AUTO_BID_QTY, 1);
        else
            $qty=1;
        return $row >=$qty ? true : false;
    }

    /**
     *落札商品の配送先IDの更新処理
     * @param <int> $address_id
     * @param <int> $pro_id
     * @return <boolean>
     */
    public function updateAddress($address_id,$pro_id) {
        try {
            DB::begin();
            DB::executeNonQuery("UPDATE t_end_products SET fk_address_id=? WHERE fk_end_products_id=?",
                    array($address_id,$pro_id));
            DB::executeNonQuery("UPDATE t_end_products SET f_status=3 WHERE f_status=2 and f_address_flag = 0 and fk_end_products_id=?",
                    array($pro_id));

            DB::commit();
            return true;
        }
        catch (Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    /**
     *配送先データを新規追加する
     * @param <array> $p
     */
    public function updateEndProductAddress($p) {
        try {
            DB::begin();
            /*
             * 仕様変更より、コメントする
            //このメンバーの配送先のデータがあるかどうか、チェック
            $count=DB::result_rows("SELECT fk_member_id FROM t_address WHERE fk_member_id=?", array($p["member_id"]));
            if($count>0)
            //他の配送先のstatus＝0(使用禁止にする）
                DB::executeNonQuery("UPDATE t_address SET f_status=0,f_tm_stamp=CURRENT_TIMESTAMP  WHERE fk_member_id=?", array($p["member_id"]));

            //新しい配送先を追加する
            DB::executeNonQuery("INSERT INTO t_address (f_status,fk_member_id,f_last_send,f_name,f_tel_no,f_post_code,fk_perf_id,
                                        f_address1,f_address2,f_address3,f_tm_stamp)
                                        VALUES (1,?,0,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)",
                    array($p["member_id"],$p["f_name"],$p["tel_no"],$p["post_code"],$p["pref_id"],
                    $p["address1"],$p["address2"],$p["address3"]));


            */
            //新規処理
            DB::executeNonQuery("INSERT INTO t_address (f_status,fk_member_id,f_last_send,f_name,f_tel_no,f_post_code,fk_perf_id,
                                        f_address1,f_address2,f_address3,f_tm_stamp)
                                        VALUES (1,?,0,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)",
                    array($p["member_id"],$p["f_name"],$p["f_tel_no"],$p["f_post_code"],$p["fk_perf_id"],
                    $p["f_address1"],$p["f_address2"],$p["f_address3"]));

            //落札商品に配送先を更新する
            DB::executeNonQuery("UPDATE t_end_products SET fk_address_id=LAST_INSERT_ID(),f_tm_stamp=CURRENT_TIMESTAMP WHERE fk_end_products_id=?",
                    array($p["fk_end_products_id"]));
            //ステータス変更
            DB::executeNonQuery("UPDATE t_end_products SET f_status = 3,f_tm_stamp=CURRENT_TIMESTAMP WHERE f_status = 2 and fk_end_products_id=?",
                    array($p["fk_end_products_id"]));
            DB::commit();
            return true;
        }
        catch(Exception $e) {
            DB::rollback();
        }
        return false;
    }

    /**
     *落札商品の購入キャンセル
     * @param <int> $pro_id 商品ID
     */
    public function endProductCancel($pro_id) {
        try {
            DB::begin();
            //f_status(状態)更新
//G.Chin AWKT-933 2010-12-20 chg sta
/*
            DB::executeNonQuery("UPDATE t_member m SET m.f_cancel_num=m.f_cancel_num+1 WHERE Exists (select ep.f_last_bidder from t_end_products ep
            where m.fk_member_id = ep.f_last_bidder and fk_end_products_id =?) ",array($pro_id));
            DB::executeNonQuery("UPDATE t_member m SET m.f_unpain_num=m.f_unpain_num-1 WHERE Exists (select ep.f_last_bidder from t_end_products ep
            where m.fk_member_id = ep.f_last_bidder and fk_end_products_id =?) and f_unpain_num > 0",array($pro_id));
*/
            DB::executeNonQuery("UPDATE t_member m SET m.f_cancel_num=m.f_cancel_num+1,m.f_unpain_num=m.f_unpain_num-1 WHERE Exists (select ep.f_last_bidder from t_end_products ep
            where m.fk_member_id = ep.f_last_bidder and fk_end_products_id =?) and f_unpain_num > 0",array($pro_id));
//G.Chin AWKT-933 2010-12-20 chg end
            DB::executeNonQuery("UPDATE t_end_products SET f_status=1 WHERE fk_end_products_id=?",
                    array($pro_id));
            DB::commit();
            return true;
        }
        catch(Exception $e) {
            DB::rollback();
        }
        return false;
    }

    public function GetProdCancel($prod_id)
    {
//G.Chin AWKT-545 2010-10-25 chg sta
//        return DB::uniqueObject("select member.f_cancel_num from t_member member INNER JOIN t_end_products ep ON ep.f_last_bidder=member.fk_member_id where ep.fk_end_products_id=$prod_id)");
		$sql = "SELECT member.f_cancel_num FROM t_member member,t_end_products ep WHERE ep.fk_end_products_id=$prod_id AND ep.f_last_bidder=member.fk_member_id ";
		return DB::uniqueObject($sql);
//G.Chin AWKT-545 2010-10-25 chg end
    }
    /**
     *会員の自動入札設定の情報を取得する
     * @param <int> $member_id 会員ID
     * @param <int> $product_id 商品ID
     * @return <object>
     */
    public function getAutoBidSettingInfo($member_id,$product_id) {
        return DB::getObjectByQuery("SELECT fk_auto_id, fk_member_id, fk_products_id,CONCAT(FORMAT(f_min_price,0),'円') AS auto_bid_start_price, CONCAT(FORMAT(f_max_price,0),'円') AS auto_bid_end_price,
                            FORMAT(IFNULL(f_free_coin,0)+IFNULL(f_buy_coin,0),0) AS auto_bid_last_coins,
                            f_status,FORMAT(IFNULL(f_set_free_coin,0)+IFNULL(f_set_buy_coin,0),0) AS auto_bid_coins,
                            CASE f_status WHEN 0 THEN 'キャンセル' WHEN 2 THEN '中止中' END AS stauts_des
                            FROM t_auto_bidder
                            WHERE fk_member_id=? AND fk_products_id=? and f_status=0", array($member_id,$product_id));
    }

}

?>
