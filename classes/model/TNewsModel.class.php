<?php
/**
 * t_newsに対するDAOクラス
 *
 */
class TNewsModel {
    /**
     * @param string $row['category']
     * @param string $row['body']
     * @param string $row['status'] 通常は設定しない（初期値を挿入する）
     */
    public function insert( $row ) {
        $default = array(
            'category'=>'不明なカテゴリ',
            'body'=>'本文が空です。',
            'status'=>0,
        );
        foreach ( $default as $name=>$value ) {
            if ( !isset( $row[($name)] ) ) {
                $row[($name)] = $value;
            }
        }
        $sql = "INSERT INTO t_news(f_category,f_body,f_status,f_regist_date)VALUES(?,?,?,NOW())";
        DB::executeNonQuery( $sql, array( $row['category'], $row['body'], $row['status']) );
    }
}

?>
