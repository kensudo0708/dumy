<?php
/**
 * ダイレクト会員登録関連処理マネージャクラス
 *
 * ■概要
 * このクラスは通常の会員登録プロセスを経ずにクロスドメインから配信される入力フォームから、
 * 非同期通信にて直接会員情報をPOSTすることで会員登録処理を完了させるため処理が実装されたものです。
 *
 * ■制限事項
 * - アフィリエイト・サービス・プロバイダが提供するプログラムとは連携できません。
 */

/**
 * クラスファイルの導入
 * - import()の実装において、require,require_onceが適切に使い分けられていないことに対処するための苦肉の表現です。
 * - load.phpに記述されているクラスは割愛
 */
if ( !clsass_exists('MemberModel') ) {
    import('classes.model.MemberModel');
}
//if ( !class_exists('Total') ) {
//    import('classes.model.Total');
//}
//if ( !class_exists('TotalSale') ) {
//    import('classes.model.TotalSale');
//}
if ( !class_exists('StringBuilder') ) {
    import(FRAMEWORK_DIR.'.utils.StringBuilder');
}

class MemberRegistManager {

    protected $member;

    /**
     * コンストラクタ
     */
    public function __construct() {
        // validate()内のチェック処理のみで使用
        $this->member = new MemberModel();
    }

    /**
     * データ構造の変換処理（既存の入力チェック処理を利用するためのデータ構造変換処理）
     * @param <hash> $src リクエストパラメータ
     * @return string
     */
    public function convert( $src ) {
        /**
         * 変換マップ
         * 左辺(変換後の属性名)=右辺(変換前の属性名or変換前の属性がない場合は変換後の属性値として適用)
         */
        $convMap = array(
            'handle'=>'hn',
            'login_id'=>'id',
            'login_pass'=>'pass',
            'mail_address'=>'mail',
            'tel_no'=>NULL,
            'birth_year'=>'birth',
            'birth_month'=>'birth',
            'birth_day'=>'birth',
            'onamae'=>'name',
            'area'=>'area',
            'job'=>'work',
            'agreement'=>'1',
        );
        $dest = array();
        foreach ( $convMap as $to=>$from ) {
            $value = NULL;
            if ( isset($src[($from)]) ) {
                $value = $src[($from)];
                switch ( $to ) {
                case 'birth_year':
                    $dest[($to)] = substr( $value, 0, 2 );
                    break;
                case 'birth_month':
                    $dest[($to)] = substr( $value, 2, 2 );
                    break;
                case 'birth_day':
                    $dest[($to)] = substr( $value, 4, 2 );
                    break;
                default:
                    $dest[($to)] = $value;
                }
            } else {
                $dest[($to)] = $convMap[($to)];
            }
        }
        return $dest;
    }

    /**
     * 会員登録処理
     *　
     *  下記の既存の処理を踏襲しています。
     *
     *  @see MemberAction.register() 仮会員登録
     *  @see MemberModel.updateMemberActivity() 本登録
     *
     * （処理対象テーブル）
     * [INSERT]
     * t_member_master
     * t_member
     * t_adcode
     *
     * [UPDATE]
     * t_stat_ad
     *
     * @param <type> $data
     */
    public function regist( $data ) {
        $model = $this->member;

        // 紹介者、広告主の取得
        $parentAdId = NULL;
        $adcode = NULL;
        $sql = "SELECT fk_adcode_id, fk_admaster_id, f_type FROM auction.t_adcode WHERE f_adcode = ?";
        $adcode = DB::uniqueObject( $sql, array( $data['ad'] ));
        if ( isset( $adcode->fk_adcode_id ) && $data['ad'] != 0 ) {
             $parentAdId = $adcode->fk_adcode_id;
        } else {
            // 取得できなかった場合は広告主(NOP)を適用
            Logger::warn('広告主情報が取得できないため、NOPを適用します。(adcode='.$data['ad'].')');
            $sql = "SELECT fk_adcode_id, fk_admaster_id, f_type FROM auction.t_adcode WHERE f_adcode = 0";
            $adcode = DB::uniqueObject( $sql, array( $data['ad'] ));
        }
        if ( !$adcode ) {
            Logger::warn('広告主情報が取得できませんでした、t_admaster,t_adcodeの設定を見直してください。');
        }

        /**
         * ■ t_adcode - t_member_master - t_memberの登録
         */

        // この会員の広告コード(友達紹介用コード)を生成
        $adCode = StringBuilder::uuid();
        $sql = "INSERT INTO auction.t_adcode (f_adcode,f_type,f_tm_stamp) VALUES (?,0,CURRENT_TIMESTAMP)";
        DB::executeNonQuery( $sql, array($adCode) );

        $sql = "
INSERT INTO auction.t_member_master (
    f_login_id, f_login_pass, f_handle, f_name, f_tel_no,
    f_sex, f_birthday, fk_address_flg, f_mail_address_pc, f_mailmagazein_pc,
    f_activity, fk_adcode_id, fk_parent_ad, f_adpri_value, fk_member_group_id,
    f_ip, f_regist_pos, f_regist_date, f_delete_flag, f_tm_stamp,
    f_aflkey,fk_area_id,fk_job_id
) VALUES (
    ?, ?, ?, ?, '-',
    ?, ?, 0, ?, 1,
    0, LAST_INSERT_ID(), ?, NULL, 1,
    ?, 0, CURRENT_TIMESTAMP, 0, CURRENT_TIMESTAMP,
    ?, ?, ?
)
";
        $tMemberArray = array(
            $data['id'], md5($data['pass']), $data['hn'], $data['name'],
            $data['sex'], $data['birth'], $data['mail'],
            $parentAdId,
            $_SERVER['REMOTE_ADDR'],
            $data['afc'], $data['area'], $data['work'],
        );
        DB::executeNonQuery( $sql, $tMemberArray );

        // 入会時にプレゼントされるコイン
        $presentCoin = intval( REG_PRESENT_COIN );

        $sql="INSERT INTO auction.t_member( fk_member_id, f_free_coin, f_tm_stamp ) VALUES (LAST_INSERT_ID(), ?, CURRENT_TIMESTAMP)";
        DB::executeNonQuery( $sql, array( $presentCoin ) );

        if ( $presentCoin ) {
            // プレゼントコインのt_pay_logの作成
            $sql = "
INSERT INTO auction.t_pay_log(
    f_status,f_member_id,f_free_coin_add,f_coin_result,f_tm_stamp
) VALUES (
    2, LAST_INSERT_ID(), ?, ?, CURRENT_TIMESTAMP
)";
            DB::executeNonQuery( $sql, array( $presentCoin, $presentCoin ) );

            // 集計テーブルを更新  TODO:member_idの取得
            TotalSale::freeCoin( $presentCoin );
            Logger::debug("[新規登録のコイン加算]会員ID:$member->fk_member_id,加算コイン数:[無料:".Utils::getInt(REG_PRESENT_COIN,0)."]");
        }

        $sql = "SELECT LAST_INSERT_ID() AS `id`";
        $res = DB::uniqueObject( $sql );
        return $res->id;
    }

    /**
     * 会員登録処理に付随する処理
     * ・広告のカウントアップ
     * ・友達紹介
     * など
     */
    public function registOption( $memberId ) {

        // 会員情報の取得
        $sql = "SELECT * FROM auction.t_member_master WHERE fk_member_id=? FOR UPDATE";
        $member = DB::uniqueObject( $sql, array( $memberId ) );

        // 広告主、紹介者の取得
        if ( $member->fk_parent_ad ) {
            $sql = "SELECT fk_adcode_id, fk_admaster_id, f_type FROM auction.t_adcode WHERE fk_adcode_id = ?";
            $adcode = DB::uniqueObject( $sql, array( $member->fk_parent_ad ) );
        } else {
            // 設定されていない場合はNOPを適用
            $sql = "SELECT fk_adcode_id, fk_admaster_id, f_type FROM auction.t_adcode WHERE f_adcode = 0";
            $adcode = DB::uniqueObject( $sql );
        }

        // t_admaster(広告主マスタ)のカウントアップ
        $sql = "UPDATE auction.t_admaster SET f_counter=f_counter+1,f_tm_stamp=CURRENT_TIMESTAMP WHERE fk_admaster_id=?";
        DB::executeNonQuery( $sql, array( $adcode->fk_admaster_id ) );

        // t_ad_counter(広告主別時間別集計)のカウントアップ
        $sql = "SELECT fk_admaster_id FROM auction.t_adcounter WHERE f_stat_dt=DATE_FORMAT(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=? FOR UPDATE";
        $adCounter = DB::uniqueObject($sql, array( $adcode->fk_admaster_id ) );
        if ( isset( $adCounter->fk_admaster_id ) ) {
            // UPDATE
            $sql = "UPDATE auction.t_adcounter SET f_pc_memreg=IFNULL(f_pc_memreg, 0) + 1 WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=?";
        } else {
            // INSERT
            $sql = "INSERT INTO auction.t_adcounter (f_stat_dt,fk_admaster_id,f_pc_memreg) VALUES (date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),?,1)";
        }
        DB::executeNonQuery( $sql, array( $adcode->fk_admaster_id ) );

        // t_ad_stat(広告主別時間別集計)のカウントアップ...↑のt_adcounterとほぼ同じテーブル
        $sql = "SELECT fk_admaster_id FROM auction.t_stat_ad WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=? AND f_type=0 FOR UPDATE";
        $statAd = DB::uniqueObject($sql, array( $adcode->fk_admaster_id ) );
        $field = $data['sex'] ? 'f_pc_memreg_woman':'f_pc_memreg';
        if ( isset( $statAd->fk_admaster_id ) ) {
            // UPDATE
            $sql = "UPDATE auction.t_stat_ad SET $field=IFNULL($field, 0) + 1 WHERE f_stat_dt=date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00') AND fk_admaster_id=? AND f_type=0";
        } else {
            // INSERT
            $sql = "INSERT INTO auction.t_stat_ad (f_stat_dt,fk_admaster_id,f_type,$field) VALUES (date_format(CURRENT_TIMESTAMP,'%Y-%m-%d %H:00:00'),?,0,1)";
        }
        DB::executeNonQuery( $sql, array( $adcode->fk_admaster_id ) );

        /**
         * ■ 紹介者(親会員)へのサービスを付与
         * @see PayCondition.setFriendAddPrice()
         */
        if ( $adcode->f_type == 0 && $adcode->f_adcode != '0' ) {
            $sql = "SELECT fk_member_id, f_free_coin, f_activity FROM auction.t_member_master WHERE fk_adcode_id=? FOR UPDATE";
            $parent = DB::uniqueObject($sql, array( $adcode->fk_adcode_id ));
            if ( $parent ) {
                if ( $parent->f_activity == 1 ) {
                    // サービスの内容を取得
                    $sql = "
SELECT
    f_adpricode,f_pay_value
FROM
    auction.t_ad_condition
WHERE
    f_spend_value <= 0 AND f_type= 0 AND f_adpricode > 0 AND f_status=0
ORDER BY
    f_adpricode
";
                    $conditions = DB::getObjectByQuery( $sql );
                    if ( $conditions && is_array( $conditions ) ) {
                        $coin = 0;
                        $adPriCode = 0;
                        foreach ( $conditions as $row ) {
                            $coin += $row['f_pay_value'];
                            $adPriCode = $row['f_adpricode'];
                        }
                        if ( $coin ) {
                            // 紹介者へのコイン付与
                            $sql = "UPDATE auction.t_member SET f_free_coin=f_free_coin + ?,f_tm_stamp=CURRENT_TIMESTAMP WHERE fk_member_id=?";
                            DB::executeNonQuery( $sql, array($coin, $parent->fk_member_id));
                            // 支払いログの生成
                            $sql = "
INSERT INTO auction.t_pay_log (
    f_status, f_member_id, f_staff_id, f_pay_money, f_free_coin_add,
    fk_shiharai_type_id, f_coin_result, f_cert_status, f_tm_stamp
) VALUES (
    2, ?, NULL, 0, ?,
    -2, ?,0,CURRENT_TIMESTAMP
)";
                            DB::executeNonQuery( $sql, array( $parent->fk_member_id, $coin, ($parent->free_coin + $coin) ));
                            Logger::debug('[親会員にコイン加算]会員ID:'.$parent->fk_member_id.',加算コイン数:[無料:'.$coin.']');
                        }
                        // この会員の状態を変更
                        $sql = "UPDATE auction.t_member_master SET f_adpricode=? WHERE fk_member_id=?";
                        DB::executeNonQuery( $sql, array($adPriCode, $memberId) );
                    } else {
                        Logger::debug('紹介者に付与するサービス設定はありません。');
                    }
                } else {
                    Logger::warn('紹介者は本会員ではありません。(member_id='.$parent->fk_member_id.',activity='.$parent->f_activity.')');
                }
            } else {
                Logger::warn('紹介者は存在しません。(fk_adcode_id='.$adcode->fk_adcode_id.',adcode='.$adcode->f_adcode.')');
            }
        }
    }

    /**
     * 入力チェック
     *
     * @return bool
     * @see InputCheck.registInputCheck() 原則としてチェック項目を踏襲しています。
     *
     * (エラーコード表)
     * -1: 広告コードが存在しない
     * -2: ハンドル名に不正な文字が入っている
     * -3: 使用できないハンドル名
     * -4: ログインIDに使用できない文字が含まれている
     * -5: ログインIDはすでに使用されている
     * -6: メールアドレスの入力エラー
     * -7: 地域の選択エラー
     * -8: 職業の選択エラー
     * -9: 番組コードエラー
     * -99: その他のエラー
     */
    public function validate( &$reqParamArray, &$errorCode, &$message ) {

//        // サイトコードチェック
//        if ( $reqParamArray ) {
//        } else {
//        }

        // 広告コード存在チェック(指定された場合のみ)
        if ( isset( $reqParamArray['ad'] ) && strlen( $reqParamArray['ad'] ) ) {
            
        } else {
            // 未指定時はNOT(0)を設定
            $reqParamArray['ad'] = 0;
        }

        // アフィリキー存在チェック(指定された場合のみ)
        if ( isset( $reqParamArray['afc'] ) && strlen( $reqParamArray['afc'] ) ) {

        }

        //++ InputCheck.registInputCheck()を踏襲したチェック処理 START

        // 属性名の変換
        $p = $this->convert( $reqParamArray );

        $model = $this->member;
        // ハンドル名
        if ( empty($p["handle"]) ) {
            $errorCode = -3;
            $message = Msg::get("REGISTER_HANDLE_EMPTY");
            return FALSE;
        } else {
            if ( InputCheck::isExistKeyWord($p["handle"]) || InputCheck::isFaultWord($p["handle"]) ) {
                $errorCode = -2;
                $message = sprintf( Msg::get("INPUT_EXIST_KEYWORD"), Msg::get("HANDLE") );
                return FALSE;
            } elseif ( $model->isExistedHandle( $p["handle"] ) ) {
                $errorCode = -3;
                $message = Msg::get("REGISTER_HANDLE_EXISTED");
                return FALSE;
            } else {
                $tmpMessageArray = array();
                InputCheck::addMaxLenCheck($tmpMessageArray, $p["handle"], Msg::get("HANDLE_LEN"), Msg::get("HANDLE"));
                if ( $tmpMessageArray ) {
                    $errorCode = -3;
                    $message = $tmpMessageArray[0];
                    return FALSE;
                }
            }
        }
        // ログインID
        if( empty($p["login_id"]) ) {
            $errorCode = -99;
            $message = Msg::get("REGISTER_LOGINID_EMPTY");
        } else {
            $tmpMessageArray = array();
            InputCheck::addMaxLenCheck($tmpMessageArray, $p["login_id"], Msg::get("LOGINID_LEN"), Msg::get("LOGINID"));
            if ( $tmpMessageArray ) {
                $errorCode = -99;
                $message = $tmpMessageArray[0];
                return FALSE;
            } else {
                if( !ereg("^[A-Za-z0-9]+$", $p["login_id"]) ) {
                    $errorCode = -4;
                    $message = Msg::get("REGISTER_LOGIN_INVALID_FORMAT");
                    return FALSE;
                } elseif ( $model->isExistedLoginId( $p["login_id"] ) ) {
                    $errorCode = -5;
                    $message = Msg::get("REGISTER_LOGINID_EXISTED");
                    return FALSE;
                }
            }
        }
        // ログインパスワード
        if ( empty($p["login_pass"]) ) {
            $errorCode = -99;
            $message = Msg::get("REGISTER_PASS_EMPTY");
            return FALSE;
        } else {
            $tmpMessageArray = array();
            InputCheck::addBetweenLenCheck($tmpMessageArray, $p["login_pass"], PASSWORD_MIN_LEN, Msg::get("PASSWORD_LEN"), Msg::get("PASSWORD"));
            if ( $tmpMessageArray ) {
                $errorCode = -99;
                $message = $tmpMessageArray[0];
                return FALSE;
            }
        }

        // PCのみ、チェックする必要
        $isPc = Config::value("PC_DIR") == AgentInfo::getAgentDir();
        if ( $isPc ) {
            // メールアドレス
            if( empty($p["mail_address"]) ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_EMAIL_EMPTY");
                return FALSE;
            } else {
                $tmpMessageArray = array();
                InputCheck::addMaxLenCheck($tmpMessageArray, $p["mail_address"], Msg::get("EMAIL_LEN"), Msg::get("EMAIL"));
                if ( $tmpMessageArray ) {
                    $errorCode = -6;
                    $message = $tmpMessageArray[0];
                    return FALSE;
                } else {
                    // 書式チェック
                    if ( Utils::isMailAddress( $p["mail_address"] ) ) {
                        //blackメールチェック
                        if( $model->isBlackMail( $p["mail_address"] ) ) {
                            $errorCode = -6;
                            $message = Msg::get("IS_BLACK_EMAIL");
                            return FALSE;
                        }
                    } else {
                        $errorCode = -6;
                        $message = Msg::get("REGISTER_EMAIL_INVALID");
                        return FALSE;
                    }
                }
            }
            // 仮登録チェック(仮登録済みの会員かどうか、チェックする)
            $memberId = $model->getTempMemberIdByMail( $p["mail_address"] );
            if ( $memberId ) {
                // 仮登録している場合、エラーで弾く(本来、仮登録状態を利用しないフローであるため)
                $errorCode = -6;
                $message = Msg::get("REGISTER_EMAIL_EXISTED");
                return FALSE;
            }
            // 同一IPチェック
            if( FORBID_REG_PCMEMIP_FLAG == 1 ) {
                if( $model->isExistedIp( $_SERVER['REMOTE_ADDR'] ) ) {
                    $errorCode = -99;
                    $message = Msg::get("REGISTER_IP_EXISTED");
                    return FALSE;
                }
            }
        }
        // メールアドレスの存在チェック
        if ( $model->isExistedEmail( $p["mail_address"], $isPc ) ) {
            $errorCode = -6;
            $message = Msg::get("REGISTER_EMAIL_EXISTED");
            return FALSE;
        }

        // 電話番号(入力された場合のみチェック)
        if( isset($p["tel_no"]) && strlen($p["tel_no"]) ) {
            if( !ereg("[0-9]+", $p["tel_no"]) ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_TEL_INVALID");
                return FALSE;
            } else {
                $tmpMessageArray = array();
                InputCheck::addMaxLenCheck($tmpMessageArray, $p["tel_no"], Msg::get("TEL_LEN"), Msg::get("TEL"));
                if ( $tmpMessageArray ) {
                    $errorCode = -99;
                    $message = $tmpMessageArray[0];
                    return FALSE;
                }
            }
        }

        // 誕生日のチェック(入力された場合のみチェック)
        $existsY = FALSE;
        $existsM = FALSE;
        $existsD = FALSE;
        if ( isset($p["birth_year"]) && strlen($p["birth_year"]) ) {
            $existsY = TRUE;
            if( $p["birth_year"] === "00" ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_BIRTH_YEAR_EMPTY");
                return FALSE;
            }
        }
        if ( isset($p["birth_month"]) && strlen($p["birth_month"]) ) {
            $existsM = TRUE;
            if( $p["birth_month"] === "00" ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_BIRTH_MONTH_EMPTY");
                return FALSE;
            }
        }
        if ( isset($p["birth_month"]) && strlen($p["birth_month"]) ) {
            $existsD = TRUE;
            if( $p["birth_day"] === "00" ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_BIRTH_DAY_EMPTY");
                return FALSE;
            }
        }
        if ( $existsY && $existsM && $existsD ) {
            if ( checkdate( $p["birth_month"], $p["birth_day"], $p["birth_year"] ) == FALSE ) {
                $errorCode = -99;
                $message = Msg::get("ERROR_BIRTH_DAY");
                return FALSE;
            }
        }

        // 氏名(入力された場合のみチェック)
        if ( isset( $p["onamae"] ) && strlen( $p["onamae"] ) ) {
            $tmpMessageArray = array();
            $this->addMaxLenCheck($tmpMessageArray, $p["onamae"], Msg::get("NAME_LEN"), Msg::get("NAME"));
            if ( $tmpMessageArray ) {
                $errorCode = -99;
                $message = $tmpMessageArray[0];
                return FALSE;
            }
        }
        // 地域(システムパラメータ設定で制御)
        if ( MEM_REG_AREA_FLG == 1 ) {
            if ( !isset($p["area"]) || $p["area"] == 0 ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_AREA_NULL");
                return FALSE;
            }
        }
        // 職業(システムパラメータ設定で制御)
        if ( MEM_REG_JOB_FLG == 1 ) {
            if( !isset($p["job"]) || $p["job"] == 0 ) {
                $errorCode = -99;
                $message = Msg::get("REGISTER_JOB_NULL");
                return FALSE;
            }
        }

        // TODO:要検討
        if(empty($p["agreement"]) || $p["agreement"] !="1") {
            array_push($message, Msg::get("REGISTER_AGREEMENT_NO"));
        }


        // TODO:サイトコードチェック

    }
}
?>
