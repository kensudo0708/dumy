<?php
/*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
*/
import("classes.model.ProductModel");
/**
 * Description of ProductGeneral
 *
 * @author mw
 */
class ProductGeneral extends Model {
    //put your code here

    private static function isSubAction($actionObj) {
        return $actionObj instanceof BaseAction;
    }

    /**
     *自動入札設定情報の追加
     * @param <Action> $action アクションのインスタンス
     * @param <int> $member_id
     * @param <int> $product_id
     */
    public static function addAutoBidSettingInfo($action,$member_id,$product_id) {
        if(!self::isSubAction($action))
            return;
        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $info=$model->getAutoBidSettingInfo($member_id, $product_id);
        if(!is_array($info) || count($info)<1)
            return;

        $action->addTplParam("auto_bid_counts",count($info));
        $action->addTplParam("setting",$info);
    }

    /**
     *商品のカテゴリの種類データを取得、リクエストに追加
     * @param <Action> $action アクションのインスタンス
     */
    public static function addCategoriyInfo($action) {
        if(!self::isSubAction($action))
            return;
        $model = new ProductModel();
        $cates=$model->getProductCategories();
        if(is_array($cates) && count($cates)>0)
            $action->addTplParam("cates", $cates);
    }

    //    /**
    //     *入札処理（PC,モバイル共通）
    //     * @param <Action> $action アクションのインスタンス
    //     * @param <object> $member
    //     * @param <int> $productId
    //     * @return <ActionResult>
    //     */
    //    public static function bid($action,&$member,$productId) {
    //        if(!self::isSubAction($action))
    //            return;
    //
    //        $isPc = AgentInfo::isPc();
    //        if(empty($member)) {
    //            $returnProduct->message=Msg::get("BIDDER_ERROR");
    //            if($isPc)
    //                return $action->createAjaxResult($returnProduct, "json");
    //            return Message::showConfirmMessage($action,Msg::get("BIDDER_ERROR"));
    //        }
    //        $result=false;
    //        $max=Utils::getInt(BID_TRY_TIMES,4);
    //        $wait=Utils::getInt(BID_TRY_WAIT,500)*1000;
    //
    //        $memberId=$member->fk_member_id;
    //        for($i=0;$i<$max;$i++) {
    //            if(empty ($member)) {
    //                import('classes.model.MemberModel');
    //                $model=new MemberModel();
    //                $member=$model->getMemberById($memberId);
    //                if(empty($member)) {
    //                    $returnProduct->message=Msg::get("BIDDER_ERROR");
    //                    if($isPc)
    //                        return $action->createAjaxResult($returnProduct, "json");
    //                    return Message::showConfirmMessage($action,Msg::get("BIDDER_ERROR"));
    //                }
    //            }
    //            //商品を取得(t_productsから）
    //            $model=new ProductModel();
    //            $p=$model->getProductByID($productId);
    //
    //            //商品終了より、
    //            if(empty ($p) || $p->f_status == 2) {
    //                //商品終了の場合
    //                $returnProduct->message=Msg::get("PRODUCT_BID_FINISHED");
    //                if($isPc)
    //                    return $action->createAjaxResult($returnProduct, "json");
    //                return Message::showConfirmMessage($action,Msg::get("INVALID_AUCTION_PRODUCT"));
    //            }
    //
    //            if($p->f_last_bidder==$member->fk_member_id) {
    //                //ユーザが既に最終入札者の場合
    //                $returnProduct->message=Msg::get("LASTEST_BIDDER");
    //                if($isPc)
    //                    return $action->createAjaxResult($returnProduct, "json");
    //                $action->setSession("message", $returnProduct->message);
    //                return $action->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
    //            }
    //
    //            //商品の詳細情報を取得
    //            $product=$model->getAuctionProductById($productId);
    //
    //            //memberコイン数チェック BID_FREE_MODE
    //            $coinTotal=$member->f_coin + $member->f_free_coin;
    //            if((BID_FREE_MODE == 0 && $coinTotal == 0) || $coinTotal < $product->f_spend_coin) {
    //                //memberコイン数が足りない場合、コイン購入画面にリングさせ
    //                if($isPc) {
    //                    $action->addTplParam("message", Msg::get("COIN_NOT_ENOUGH"));
    //                    $returnProduct->message=Msg::get("COIN_NOT_ENOUGH");
    //                    $returnProduct->time=$p->f_auction_time;
    //                    return $action->createAjaxResult($returnProduct, "json");
    //                }
    //                else {
    //                    $action->setSession("message", Msg::get("COIN_NOT_ENOUGH"));
    //                    return $action->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
    //                }
    //            }
    //
    //            $product=Utils::objectMerge($product, $p);
    //            $product->f_last_free_flg=0;
    //            //フリーコインから消費する
    //            if($member->f_free_coin >= $product->f_spend_coin) {
    //                $member->f_free_coin -= $product->f_spend_coin;
    //                $product->f_bit_free_count++;
    //                $product->f_coin_type=0;
    //                $product->f_last_free_flg=1;
    //            }
    //            else {
    //                if($member->f_free_coin > 0)
    //                    $product->f_last_free_flg=1;
    //                //有料コイン
    //                $member->f_coin =$member->f_coin - $product->f_spend_coin + $member->f_free_coin;
    //                $member->f_free_coin=0;
    //                $product->f_bit_buy_count++;
    //                $product->f_coin_type=1;
    //            }
    //            if($product->f_spend_coin ==0) {
    //                $product->f_last_free_flg=0;
    //            }
    //
    ////        $product->f_last_free_flg = $product->f_spend_coin == 0 ? 2 : $product->f_last_free_flg;
    //            $member->t_contact_count++;
    //            //商品価格更新
    //            $model->updateProductPrice($product);
    //
    //            //ﾃﾞｨｽｶｳﾝﾄ額
    //            $product->f_market_price=$product->f_market_price ==0 ? 1 : $product->f_market_price;
    //            $returnProduct->discount_price=$product->f_market_price-$product->f_now_price;
    //            $returnProduct->discount_rate=floor(($returnProduct->discount_price / $product->f_market_price) * 100);
    //
    //            //最終入札者更新
    //            $product->f_last_bidder=$member->fk_member_id;
    //            $product->f_last_bidder_handle=$action->session("member_handle");
    //            $product->f_auction_time+=Utils::getInt(BID_ADD_SECONDS,0);
    //            $product->f_bid_type=0;
    //
    //            if($model->bidProcess($member,$product)) {
    //                $result=true;
    //                break;
    //            }
    //            $member=null;
    //            usleep($wait);
    //        }
    //        if(!$result) {
    //            Logger::warn(sprintf(Msg::get("BID_ERROR_LOG"),$memberId,$productId));
    //            if($isPc) {
    //                $returnProduct->message=Msg::get("BIDDER_ERROR");
    //                $returnProduct->time=$product->f_auction_time;
    //                return $action->createAjaxResult($returnProduct, "json");
    //            }
    //            else {
    //                $action->setSession("message", Msg::get("BIDDER_ERROR"));
    //                return $action->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
    //            }
    //        }
    //
    //        $returnProduct->price=number_format($product->f_now_price);
    //        $returnProduct->last_bidder=$product->f_last_bidder_handle;
    //        $returnProduct->time=$product->f_auction_time;
    //        return $returnProduct;
    //    }


    /** 　　*******************ログかけるパターン*****************************
     *入札処理（PC,モバイル共通）
     * @param <Action> $action アクションのインスタンス
     * @param <object> $member
     * @param <int> $productId
     * @return <ActionResult>
     */
    public static function bid($action,&$member,$productId) {
 //       echo "12345564758";
        if(!self::isSubAction($action))
            return;

        $isPc = AgentInfo::isPc();
        if(empty($member)) {
            $returnProduct->message=Msg::get("BIDDER_ERROR");
            if($isPc)
                return $action->createAjaxResult($returnProduct, "json");
            return Message::showConfirmMessage($action,Msg::get("BIDDER_ERROR"));
        }
        $result=false;
        $max=Utils::getInt(BID_TRY_TIMES,4);
        $wait=Utils::getInt(BID_TRY_WAIT,500)*1000;

        $memberId=$member->fk_member_id;
        //メンバ落札回数取得
        $member_type=$member->t_hummer_count;
        $AuctionTime = 0;
        for($i=0;$i<$max;$i++) {
            if(empty ($member)) {
                import('classes.model.MemberModel');
                $model=new MemberModel();
                $member=$model->getMemberById($memberId);
                if(empty($member)) {
                    $returnProduct->message=Msg::get("BIDDER_ERROR");
                    if($isPc)
                        return $action->createAjaxResult($returnProduct, "json");
                    return Message::showConfirmMessage($action,Msg::get("BIDDER_ERROR"));
                }

            }
            try {
                DB::begin();
                //商品を取得(t_productsから）
                $model=new ProductModel();
                $p=$model->getLockProductByID($productId);
                $AuctionTime = $p->f_auction_time;
                //商品終了より、
                //if(empty ($p) || $p->f_status == 2) {
                if($p->f_status == 2){
                    //商品終了の場合
                    $returnProduct->message=Msg::get("PRODUCT_BID_FINISHED");
                    if($isPc)
                        return $action->createAjaxResult($returnProduct, "json");
                    return Message::showConfirmMessage($action,Msg::get("INVALID_AUCTION_PRODUCT"));
                }

                if(empty ($p) ){
                    $returnProduct->message=Msg::get("PRODUCT_BID_FINISHED");
                    if($isPc)
                        return $action->createAjaxResult($returnProduct, "json");
                    return Message::showConfirmMessage($action,Msg::get("INVALID_AUCTION_PRODUCT"));
                }
                if($p->f_last_bidder==$member->fk_member_id) {
                    //ユーザが既に最終入札者の場合
                    $returnProduct->message=Msg::get("LASTEST_BIDDER");
                    if($isPc)
                        return $action->createAjaxResult($returnProduct, "json");
                    $action->setSession("message", $returnProduct->message);
                    return $action->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
                }

                //商品の詳細情報を取得
                $product=$model->getAuctionProductById($productId);
                //初心者情報と商品情報対応
                $product_auction_type=$product->fk_auction_type_id;
                if($product_auction_type==BEGINNER_AUCTION_ID){
                   if($member_type!=0){
                       $returnProduct->message=Msg::get("BIDDER_ERROR");
                        return $action->createAjaxResult($returnProduct, "json");
                   }
                   
                }
                //memberコイン数チェック BID_FREE_MODE
                $coinTotal=$member->f_coin + $member->f_free_coin;
                if((BID_FREE_MODE == 0 && $coinTotal == 0) || $coinTotal < $product->f_spend_coin) {
                    //memberコイン数が足りない場合、コイン購入画面にリングさせ
                    if($isPc) {
                        $action->addTplParam("message", Msg::get("COIN_NOT_ENOUGH"));
                        $returnProduct->message=Msg::get("COIN_NOT_ENOUGH");
                        $returnProduct->time=$p->f_auction_time;
                        return $action->createAjaxResult($returnProduct, "json");
                    }
                    else {
                        $action->setSession("message", Msg::get("COIN_NOT_ENOUGH"));
                        return $action->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
                    }
                }

                $product=Utils::objectMerge($product, $p);
                $product->f_last_free_flg=0;
                //フリーコインから消費する
                if($member->f_free_coin >= $product->f_spend_coin) {
                    $member->f_free_coin -= $product->f_spend_coin;
                    $product->f_bit_free_count++;
                    $product->f_coin_type=0;
                    $product->f_last_free_flg=1;
                }
                else {
                    if($member->f_free_coin > 0)
                        $product->f_last_free_flg=1;
                    //有料コイン
                    $member->f_coin =$member->f_coin - $product->f_spend_coin + $member->f_free_coin;
                    $member->f_free_coin=0;
                    $product->f_bit_buy_count++;
                    $product->f_coin_type=1;
                }
                if($product->f_spend_coin ==0) {
                    $product->f_last_free_flg=0;
                }

                //        $product->f_last_free_flg = $product->f_spend_coin == 0 ? 2 : $product->f_last_free_flg;
                $member->t_contact_count++;
                //商品価格更新
                $model->updateProductPrice($product);

                //ﾃﾞｨｽｶｳﾝﾄ額
                $product->f_market_price=$product->f_market_price ==0 ? 1 : $product->f_market_price;
                $returnProduct->discount_price=$product->f_market_price-$product->f_now_price;
                $returnProduct->discount_rate=floor(($returnProduct->discount_price / $product->f_market_price) * 100);

                //最終入札者更新
                $product->f_last_bidder=$member->fk_member_id;
                $product->f_last_bidder_handle=$action->session("member_handle");
//                echo $product->f_last_bidder_handle;

                if($product->f_auction_time > COUNT_UP_LIMIT || COUNT_UP_LIMIT == 0)
                {
                    $product->f_auction_time+=Utils::getInt(BID_ADD_SECONDS,0);
                }
                else
                {
                    $product->f_auction_time = COUNT_UP_LIMIT;
                }
                $product->f_bid_type=0;


                $rows=0;
                $result = false;

                //商品情報を更新
                $rows+=DB::executeNonQuery("UPDATE t_products SET f_bit_buy_count=?,f_bit_free_count=?,f_now_price=?,f_last_bidder=?,f_last_bidder_handle=?,f_auction_time=?,
                                        f_tm_stamp=CURRENT_TIMESTAMP,f_last_free_flg = ?,f_status=1 WHERE fk_products_id=? AND (f_status=1 or f_status=7 or f_status=5 or f_status=3)",
                        array($product->f_bit_buy_count,$product->f_bit_free_count,$product->f_now_price,
                        $product->f_last_bidder,$product->f_last_bidder_handle,
                        $product->f_auction_time,$product->f_last_free_flg,$product->fk_products_id));

                //会員情報を更新
                $memUpdateSql="";
                if(BID_FREE_MODE != 0 && $product->f_spend_coin== 0)
                    $memUpdateSql="UPDATE t_member SET f_coin=?,f_free_coin=?,t_contact_count=?,f_tm_stamp=CURRENT_TIMESTAMP
                                            WHERE fk_member_id=?";
                else
                    $memUpdateSql="UPDATE t_member SET f_coin=?,f_free_coin=?,t_contact_count=?,f_tm_stamp=CURRENT_TIMESTAMP
                                        WHERE fk_member_id=? and (f_coin>0 OR f_free_coin>0)";
                $rows+=DB::executeNonQuery($memUpdateSql, array($member->f_coin,$member->f_free_coin,$member->t_contact_count,$member->fk_member_id));

                //bidログを追加
                $rows+=DB::executeNonQuery("INSERT INTO t_bid_log(f_products_id,fk_member_id,f_price,f_bid_type,f_coin_type,f_tm_stamp)
                                    VALUES (?,?,?,?,?,CURRENT_TIMESTAMP)",
                        array($product->fk_products_id,$product->f_last_bidder,$product->f_now_price,$product->f_bid_type,$product->f_coin_type));

                if($rows==3) {
                    DB::commit();
                    $result= true;
                }
                else
                    DB::rollback();
            }
            catch (Exception $e) {
                DB::rollback();
            }

            if($result)
                break;
            //            if($model->bidProcess($member,$product)) {
            //                $result=true;
            //                break;
            //            }
            $member=null;
            usleep($wait);
        }
        if(!$result) {
            Logger::warn(sprintf(Msg::get("BID_ERROR_LOG"),$memberId,$productId,$AuctionTime));
            if($isPc) {
                $returnProduct->message=Msg::get("BIDDER_ERROR");
                $returnProduct->time=$product->f_auction_time;
                return $action->createAjaxResult($returnProduct, "json");
            }
            else {
                $action->setSession("message", Msg::get("BIDDER_ERROR"));
                return $action->createRedirectResult(Config::value("SERVER_PATH")."/product?pro_id=".$productId);
            }
        }

        $returnProduct->price=number_format($product->f_now_price);
        $returnProduct->last_bidder=$product->f_last_bidder_handle;
        $returnProduct->time=$product->f_auction_time;
        return $returnProduct;
    }


    public static function autoBidCancel($action,$auto_id) {
        if(!self::isSubAction($action))
            return;
        import("classes.model.MyProductModel");
        $model=new MyProductModel();
        $result = $model->autoBidCancel($auto_id);
        if(!$result)
            $action->addTplParam("message", Msg::get("CANNOT_CANCEL"));
        Logger::debug("自動入札キャンセル:$auto_id");
        return $result;
    }

    public static function getPrevSameProduct($action,$productId) {
        //echo "aaa";
        if(!self::isSubAction($action))
            return;
        $model = new ProductModel();
        $pro = $model->getPrevSameProduct($productId);
        if(!empty ($pro))
            $action->addTplParam("prev_product", $pro);

    }

    public static function hasPaid($action,$member_id,$productId) {
        if(!self::isSubAction($action))
            return;
        import("classes.model.ShiharaiModel");
        $model = new ShiharaiModel();
        if($model->hasPaid($member_id, $productId))
            return Message::showMyPageMessage($action, Msg::get("HAS_PAID"));
        return null;

    }
    public static function Product_text($products,$defail=false) {
        $producttext="";
        $log_text="";
        //メンテナンス中 共通
        if(MAINTE_FLAG ==SYS_CLOSE) {
            if($_SERVER['REMOTE_ADDR'] == "219.101.37.233" || $_SERVER['REMOTE_ADDR'] == USER_SERVER)
            {

            }
            else
            {
                foreach ($products as $p) {
                    if(!empty($producttext) && $producttext!=""){
                        $producttext.=";";
                    }
                    $producttext.=$p->id."\"".$p->message;
                }
            }
        }
        if($defail==false) {
            foreach ($products as $p) {
                //メッセージがない正常の場合
                if(property_exists($p, 'message')==false) {
                    if(!empty($producttext) && $producttext!=""){
                         $producttext.=";";
                    }
                    $producttext.=$p->id."\"".$p->price."\"".$p->last_bidder."\"".$p->time."\"".$p->discount_price."\"".$p->discount_rate."\"".$p->sec;
                }else {
                    //落札済み
                    if(!empty($producttext) && $producttext!=""){
                         $producttext.=";";
                    }
                    $producttext.=$p->id."\"".$p->message;
                }
            }
        }else {
            //詳細商品

            foreach ($products as $p) {
                //メッセージがない正常の場合
                if(((property_exists($p, 'message')==false))) {
                    if((property_exists($p, 'logs')==true && ($p->logs)!="")) {

                        foreach ($p->logs as $l) {
                            if(!empty($log_text) && $log_text!=""){
                               $log_text.=":";

                            }
                            $log_text.=$l->f_handle."'".$l->f_price."'".$l->f_bid_type;
                        }
                    }
                    if(!empty ($log_text) &&  $log_text!="" ) {
                        if(!empty($producttext) && $producttext!=""){
                            $producttext.=";";
                        }
                        $producttext.=$p->id."\"".$p->price."\"".$p->last_bidder."\"".$p->time."\"".$p->discount_price."\"".$p->discount_rate."\"".$p->sec."\"".$log_text;
                    }else {
                        $producttext.=$p->id."\"".$p->price."\"".$p->last_bidder."\"".$p->time."\"".$p->discount_price."\"".$p->discount_rate."\"".$p->sec;
                    }
                }else {
                    if((property_exists($p, 'logs')==true)) {

                        foreach ($p->logs as $l) {
                            if(!empty($log_text) && $log_text!=""){
                               $log_text.=":";
                            }
                            $log_text.=$l->f_handle."'".$l->f_price."'".$l->f_bid_type;                        }
                    }
                    if(!empty ($log_text) && $log_text!="") {
                        if(!empty ($producttext) && $producttext!=""){
                            $producttext.=";";
                        }
                        $producttext.=$p->id."\"".$p->message."\"".$log_text."";
                    }else {
                        if(!empty ($producttext) && $producttext!=""){
                            $producttext.=";";
                        }
                        $producttext.=$p->id."\"".$p->message;
                    }
                }
            }


        }

        return $producttext;

    }
}

?>
