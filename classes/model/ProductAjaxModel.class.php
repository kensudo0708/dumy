<?php
import(FRAMEWORK_DIR.'.model.Model');
import("classes.utils.Message");

class ProductAjaxModel extends Model {

    public function convertTextDetail( $data ) {
        $text = '';
        foreach ( $data as $p ) {
            if ( $text ) {
                $text .= ';';
            }
            $logText = '';
            if ( property_exists( $p, 'message' ) ) {
                if ( property_exists( $p, 'logs' ) && $p->logs && is_array( $p->logs ) ) {
                    foreach ( $p->logs as $l ) {
                        if( $logText ){
                           $logText .= ':';
                        }
                        $logText .= $l->f_handle.'\''.$l->f_price.'\''.$l->f_bid_type;
                    }
                }
                if ( $logText ) {
                    $text .= $p->id.'"'.$p->message.'"'.$logText;
                } else {
                    $text .= $p->id.'"'.$p->message;
                }
            } else {
                if ( property_exists( $p, 'logs' ) && $p->logs && is_array( $p->logs ) ) {
                    foreach ( $p->logs as $l ) {
                        if( $logText ){
                           $logText .= ':';
                        }
                        $logText .= $l->f_handle.'\''.$l->f_price.'\''.$l->f_bid_type;
                    }
                }
                if ( $logText ) {
                    $text .= $p->id.'"'.$p->price.'"'.$p->last_bidder.'"'.$p->time.'"'.$p->discount_price.'"'.$p->discount_rate.'"'.$p->sec.'"'.$p->status.'"'.$p->information.'"'.$logText;
                } else {
                    $text .= $p->id.'"'.$p->price.'"'.$p->last_bidder.'"'.$p->time.'"'.$p->discount_price.'"'.$p->discount_rate.'"'.$p->sec.'"'.$p->status.'"'.$p->information;
                }
            }
        }
        return $text;
    }

    public function convertTextList( $data ) {
        $text = '';
        foreach ( $data as $p ) {
            if( $text ){
                 $text .= ';';
            }
            // message属性が設定されている場合は異常
            if ( property_exists( $p, 'message' ) ) {
                $text .= $p->id.'"'.$p->message;
            } else {
                $text .= $p->id.'"'.$p->price.'"'.$p->last_bidder.'"'.$p->time.'"'.$p->discount_price.'"'.$p->discount_rate.'"'.$p->sec.'"'.$p->status.'"'.$p->information;
            }
        }
        return $text;
    }


    public function getDetail( $id ) {
        $now = floor( microtime(TRUE) );
        $timelag = intval(constant('AJAX_AUTO_TIMELAG'));
        if ( $timelag < 0 ) {
            $timelag = 0;
        }
        $data = $this->getProductDetailById( $id );
        import('classes.model.BidModel');
        $bid = new BidModel();
        foreach ( $data as $p ) {
            // 落札済み判定
            if($p->status == 2) {
                $p->information = Msg::get("PRODUCT_BID_FINISHED");
            } else {
                $p->information = '';
            }
            $p->price = number_format($p->f_now_price);
            $p->time = $p->f_auction_time - $timelag;
            $p->last_bidder = $p->f_last_bidder_handle ? $p->f_last_bidder_handle:Msg::get("NOT_BIDDER");
            $p->f_market_price = $p->f_market_price == 0 ? 1 : $p->f_market_price;
            $p->discount_price = $p->f_market_price - $p->f_now_price;
            $p->discount_rate = floor(($p->discount_price / $p->f_market_price) * 100);
            $p->discount_rate = $p->discount_rate < 1 ? 0 :$p->discount_rate;
            $p->discount_price = number_format($p->discount_price);
            //今の時刻より最新の入札ログ
            $logs = $bid->getProductBidLog( $p->id, Utils::getInt(PRODUCT_BID_LOG_QTY,0), $p->t_r_status );
            $p->logs = $logs;
            $p->sec = $now;
        }
        return $data;
    }

    public function getList( $idArray ) {
        $now = floor( microtime(TRUE) );
        $timelag = intval(constant('AJAX_AUTO_TIMELAG'));
        if ( $timelag < 0 ) {
            $timelag = 0;
        }
        $data = $this->getProductByIdArray( $idArray );
        foreach ( $data as $p ) {
            $p->time = $p->time - $timelag;
            ////ﾃﾞｨｽｶｳﾝﾄ額
            if( property_exists( $p, 'price' ) ) {
                $p->discount_price = $p->market_price-$p->price;
                $p->market_price = $p->market_price == 0 ? 1:$p->market_price;
                $p->discount_rate = floor(($p->discount_price/$p->market_price) * 100);
                $p->discount_rate = $p->discount_rate < 1 ? 0:$p->discount_rate;
                $p->price = number_format($p->price);
            }
            // 落札済み判定
            if( $p->status == 2 ) {
                // 落札済みメッセージを追加
                $p->information = Msg::get("PRODUCT_BID_FINISHED");
            } else {
                $p->information = '';
            }
            $p->sec = $now;
        }
        return $data;
    }

    /**
     * Ajax通信で、商品ID(複数）で商品情報を取得
     * @param <type> $ids
     */
    public function getProductByIdArray( $idArray ) {
        $data = array();
        $params = '';
        if ( $idArray && is_array( $idArray ) ) {
            $params = implode( ',', $idArray );
        }
        if ( $params ) {
            $sql = "
SELECT
  p.fk_products_id       AS id,
  p.f_now_price          AS price,
  (CASE WHEN mm.f_handle IS NULL OR mm.f_handle='' THEN '".Msg::get("NOT_BIDDER")."' ELSE mm.f_handle END)  AS last_bidder,
  p.f_auction_time       AS `time`,
  p.f_status             AS `status`,
  mast.f_market_price    AS market_price
FROM
  t_products_master mast,t_products p
LEFT OUTER JOIN
  t_member_master mm ON p.f_last_bidder = mm.fk_member_id
WHERE
  mast.fk_products_id=p.fk_products_id
  AND (p.f_status=1 OR p.f_status=2 OR p.f_status=3 OR p.f_status=4 OR p.f_status=5 OR p.f_status=7)
  AND p.fk_products_id in ($params)
";
            $data = DB::getObjectByQuery( $sql );
        }
        return $data;
    }

    /**
     * 商品詳細画面のAjax通信で、商品IDで商品情報を取得（ディスカウント含む）
     * @param <int> $id
     */
    public function getProductDetailById( $id ) {
        $data = array();
        $sql = "
SELECT
  p.fk_products_id as id,
  p.f_status as `status`,
  mm.f_handle as f_last_bidder_handle,
  p.f_auction_time,
  mast.f_market_price,
  p.f_now_price,
  p.t_r_status
FROM
  t_products p
INNER JOIN
  t_products_master mast ON mast.fk_products_id=p.fk_products_id
LEFT OUTER JOIN
  auction.t_member_master mm ON p.f_last_bidder = mm.fk_member_id
WHERE
  p.fk_products_id = ?
";
        if ( $id ) {
            $data =  DB::getObjectByQuery( $sql, array($id));
        }
        return $data;
    }
}
?>
