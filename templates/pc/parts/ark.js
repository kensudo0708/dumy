    $(document).ready(function() {
        function Info(){
            this.coins=null;
            this.bids=null;
        }
        function Product(){
            this.id=null;
            this.time=null;
            this.price=null;
            this.bidder=null;
            this.button=null;
            this.result=null;
            this.dis_rate=null;
            this.load=null;
            this.area=null;
            this.disprice=null;
            this.disrate=null;
            this.img=null;
            this.log=null;
            this.bidCount=null;
            this.url=null;
            this.no=null;
            this.dis_img=null;
            this.displayBox=null;
            this.status=null;
        }

        function setValue(obj,value){
            try{
                if(obj && value){
                    obj.html(value);
                    return obj;
                }
                return null;
            }
            catch(e){}
        }

        function getValue(obj){
            try{
                if(obj)
                    return obj.html();
                return null;
            }
            catch(e){}
        }

        Product.prototype.equals=equals;
        function equals(obj){
            try{
                if(this == obj)
                    return true;
                if(this.id == obj.id){
                    if(this.no && obj.no && this.no==obj.no)
                        return true;
                    else if(!this.no && !obj.no)
                        return true;
                    else
                        return false;
                }
                return false;
            }
            catch(e){
                return false;
            }

        }

        $("#show_time").click(function(){
            var oS=$("#start_input > select option:selected");
            var oE=$("#end_input > select option:selected");

            var s = new Date($(oS[0]).attr("value"), $(oS[1]).attr("value")-1, $(oS[2]).attr("value"), $(oS[3]).attr("value"), $(oS[4]).attr("value"), 0);
            var e = new Date($(oE[0]).attr("value"), $(oE[1]).attr("value")-1, $(oE[2]).attr("value"), $(oE[3]).attr("value"), $(oE[4]).attr("value"), 0);
            var result =(e.getTime()-s.getTime())/1000;
             $("#result_input>input ").attr("value",result);
        });

        var img=$(".pro_img > tbody > tr > td >img");
        if(img){
            for (i = 0; i < img.length; i++) {
                var src=$(img[i]).attr("src");
                if(!src)
                    $(img[i]).attr("src",$("#no_image").attr("src"));
                else{
                    $(img[i]).click(function () {
                        $("img[name='prdimg']").attr("src",$(this).attr("src"));
                    });
                }
            }
        }

        try{
            var wait=1000;
            if($("#ajax_times")){
                var times=getValue($("#ajax_times"));
                if(times && !isNaN(times) && times!=0)
                    wait=parseInt(1000 / times);
            }
            var products = new Array();
            var items=$(".instant_item,.instant_item_detail");
            var is_list_page=false;
            var pro_hold_arr=new Array();
            for(i = 0; i < items.length; i++){
                var id=($(items[i]).attr('ta'));
                if(!id)
                    continue;
                var item = $(items[i]);
                products[i] =new Product();
                products[i].id=id;
                products[i].time = item.find(".time_"+id);
                products[i].price = item.find(".price_"+id);
                //ﾃﾞｨｽｶｳﾝﾄ額
                products[i].dis_rate = item.find(".dis_rate_"+id);
                products[i].bidder = item.find(".last_bidder_"+id);
                products[i].button = item.find(".button_"+id);
                products[i].load = item.find(".load_"+id);
                products[i].result = item.find(".result_"+id);
                products[i].area = item.find(".button_area_"+id);
                products[i].img=item.find(".img_"+id);
                products[i].dis_img=item.find(".dis_img_"+id);
                products[i].url=products[i].button.attr('href');
                products[i].no=products[i].button.attr('ta');
                products[i].displayBox=item.find(".displayBox_"+id);
                products[i].status=1;
                pro_hold_arr[id]=true;

                var c = item.attr('class');
                if(c.indexOf("instant_item_detail") != -1){
                    products[i].disprice=item.find(".discount_price_"+id);
                    products[i].disrate=item.find(".discount_rate_"+id);
                    products[i].bidCount=item.find(".my_bid_count_"+id);
                    products[i].log=item.find(".log_"+id);
                    is_list_page=false;
                }
                else {
                    is_list_page=true;
                }
            }
            var info = new Info();
            info.coins=$(".coin_count");
            info.bids=$(".bid_count");

            // bidding
            $(".bid_button").click(function() {
                var url = $(this).attr('href');
                var id=url.split("=")[1];
                var ta = $(this).attr('ta');
                var p=null;
                $(products).each(function(index,pro){
                    if(!pro.id || pro.id!=id)
                        return true;
                    if(ta){
                        if(pro.no && ta == pro.no)
                            p=pro;
                    }
                    else{
                        if(!pro.no)
                            p=pro;
                    }
                    if(p){
                        p.area.hide(1);
                        p.load.show(1);
                        return false;
                    }
                    return true;
                });
                $.ajax( {
                    url : url,
                    dataType : 'json',
                    cache : false,
                    type : "POST",
                    timeout : 10000,
                    success : function(data) {
                        $(products).each(function(i,pro){
                            if(!pro.id || pro.id!=id)
                                return true;
                            if(data.message){
                                setValue(p.result,data.message).show(1).animate({
                                    opacity: 1.0
                                }, 2000).hide(1);
                            }
                            else{
                                setValue(p.result,"入札しました！").show(1).animate({
                                    opacity: 1.0
                                }, 2000).hide(1);
                                setValue(info.bids,data.bid_count);
                                setValue(info.coins,data.coin);
                                setValue(pro.bidder,data.last_bidder);
                                setValue(pro.price,data.price);
                                setValue(pro.bidCount,data.product_bid_count);
                                setTime(pro,data.time);
                                if(pro.equals(p))
                                    setHighlight(p);
                                if(p.no && p.no=="no"){
                                    if(!pro.equals(p)){
                                        setValue(p.bidder,data.last_bidder);
                                        setValue(p.price,data.price);
                                        setTime(pro,data.time);
                                    }
                                    else if(i!= ($(products).size() -1)){
                                        return true;
                                    }
                                }
                            }
                            p.load.hide(1);
                            p.area.show(1);
                            return false;
                        });

                    },
                    complete : function( XMLHttpRequest, status ) {
                        if ( status == "timeout" || status == "error" ) {
                            setValue(p.result,"回線が混雑しております").show(1).animate({
                                opacity: 1.0
                            }, 2000).hide(1);
                        }
                        p.load.hide(1);
                        p.area.show(1);
                        return false;
                    }
                });
                return false;
            });

            //ウォッチリストに追加する処理
            $(".add_watch_button").click(function(){
                var src=$("#add_watch_image").attr('src');
                var imageName=src.substr(src.lastIndexOf("/")+1);
                if(imageName=="add_watch_finish.png")
                    return false;
                var url = $(this).attr('href');
                $.ajax({
                    url : url,
                    type : "POST",
                    dataType : 'text',
                    cache : false,
                    success : function(data){
                        if(data.indexOf("ok")!=-1){
                            src=src.replace(imageName,"") + "add_watch_finish.png";
                            $("#add_watch_image").attr('src',src);
                        }
                    }
                });
                return false;
            });

            var host=getValue($("#ajax_host"));
            var limit=getValue($("#ajax_limit"));
            var ajaxMsg=getValue($("#ajax_message"));
            if(host && products.length>0){
                var w = wait;
                var count = 0;
                setInterval(function(){
                    w=wait;
                    if(limit && ajaxMsg && limit>0 && count>limit){
                        $(products).each(function(index,pro){
                            pro.time.css('font-size', '16px');
                            pro.time.css('color', '#ff0000');
                            setValue(pro.time,ajaxMsg);
                        });
                        return;
                    }
                    if(limit && limit>0 && count<=Number.MAX_VALUE)
                        count++;
                    var start = new Date().getTime();
                    var data = '';
                    var chk = new Array();
                    for (var pid in pro_hold_arr) {
                        if(chk[pid]==undefined&&pro_hold_arr[pid]) {
                            chk[pid]=true;
                            if(is_list_page) {
                                if(data)
                                    data = data+','+pid;
                                else
                                    data = 'ids='+pid;
                            }
                            else
                                data = 'detail='+pid;
                        }
                    }
                    if (data=='') {
                        return true;
                    }
                    $.ajax( {
                        url : host+"/index.php/ajax/auto?time="+start,
                        type: 'POST',
                        data: data,
                        dataType : 'text',
                        cache : false,
                        success : function(result){
                            json=new Array();
                            var pros = result.split(";");
                            for (j=0;j<pros.length;j++){// DATA roop
                                var product=new Object();
                                var p=$.trim(pros[j]).split("\"");
                                if(p.length==2){
                                    product.id=p[0];
                                    product.message=p[1];
                                }
                                else if(p.length>=8){
                                    product.id=p[0];
                                    product.price=p[1];
                                    product.last_bidder=p[2];
                                    product.time=p[3];
                                    product.discount_price=p[4];
                                    product.discount_rate=p[5];
                                    product.sec=p[6];
                                    product.status=p[7];
                                    product.info=p[8];
                                    if(p.length==10 && p[9]){
                                        var log = p[9].split(":");
                                        logs=new Array();
                                        for(i=0;i<log.length;i++){
                                            var l=new Object();
                                            logdata= log[i].split("'");
                                            l.f_handle=logdata[0];
                                            l.f_price=logdata[1];
                                            l.f_bid_type=logdata[2];
                                            logs[i]=l;
                                        }
                                        product.logs=logs;
                                    }
                                }
                                json[j]=product;
                            }
                            for(i=0;i<json.length;i++) {//DOM roop
                                $(products).each(function(index,pro){
                                    if(!json[i] || !pro.id || pro.id!=json[i].id)
                                        return true;
                                    if(json[i].message){
                                        pro.time.css('color', '#ff0000');
                                        pro.time.css('font-size', '16px');
                                        setValue(pro.time,json[i].message);
                                    }
                                    else{
                                        var sec = pro.time.attr("sec");
                                        if(!sec)
                                            pro.time.attr("sec",json[i].sec);
                                        else{
                                            if(sec >= json[i].sec)
                                                return true;
                                            else
                                                pro.time.attr("sec",json[i].sec);
                                        }
                                        if (json[i].status==2) {
                                            pro_hold_arr[pro.id]=false;
                                            setValue(pro.time,json[i].info);
                                            setValue(pro.bidder,json[i].last_bidder);
                                            if (pro.status!=2) {
                                                // effect
                                                try {
                                                    setSuccessEffect(pro);
                                                } catch(e) {
                                                    alert(json[i].info+'('+json[i].last_bidder+')');
                                                }
                                                pro.status=json[i].status;
                                            }
                                        } else {
                                            setValue(pro.time,getStringTime(json[i].time));
                                            setValue(pro.bidder,json[i].last_bidder);
                                        }
                                        //ﾃﾞｨｽｶｳﾝﾄ額
                                        if(json[i].discount_rate<1){
                                            pro.dis_img.hide();
                                        }
                                        setValue(pro.dis_rate,json[i].discount_rate);
                                        if(getValue(pro.price) !=json[i].price){
                                            setValue(pro.price,json[i].price);
                                            setHighlight(pro);
                                        }
                                        if(json[i] && json[i].time && json[i].time<10){
                                            if(json[i].time>=0)
                                                pro.time.css('color', '#ff0000');
                                        }
                                        else
                                            pro.time.removeAttr('style');
                                        if(json[i] && json[i].hasOwnProperty("discount_price")){
                                            setValue(pro.disprice,json[i].discount_price);
                                            setValue(pro.disrate,json[i].discount_rate);
                                            if(json[i].logs && json[i].logs.length>0)
                                                setValue(pro.log,getBidLogTag(json[i].logs));
                                        }
                                    }
                                    return true;
                                });
                            }
                        }
                    });
                    w = w- (new Date().getTime()-start);
                },w);
            }
        }
        catch(e){}
    });


    function setTime(obj,seconds){
        obj.time.html(getStringTime(seconds));
        if(seconds<10){
            if(seconds>0)
                obj.time.css('color', '#ff0000');
            else
                obj.time.hide();
        }
        else
            obj.time.removeAttr('style');
    }

    function getStringTime(diff) {
        if (diff > 0) {
            hours=Math.floor(diff / 3600)
            minutes=Math.floor((diff / 3600 - hours) * 60)
            seconds=Math.round((((diff / 3600 - hours) * 60) - minutes) * 60)
        } else {
            hours = 0;
            minutes = 0;
            seconds = 0;
            return "処理中";
        }
        if (seconds == 60)
            seconds = 0;

        if (minutes < 10) {
            if (minutes < 0)
                minutes = 0;
            minutes = '0' + minutes;
        }
        if (seconds < 10) {
            if (seconds < 0)
                seconds = 0;
            seconds = '0' + seconds;
        }
        if (hours < 10) {
            if (hours < 0)
                hours = 0;
            hours = '0' + hours;
        }
        return hours + ":" + minutes + ":" + seconds;
    }

    function checkTime(){
        var oS=$("#start_input > select option:selected");
        var oE=$("#end_input > select option:selected");
        var shou_time=$("#result_input>input ").attr("value");

        var s = new Date($(oS[0]).attr("value"), $(oS[1]).attr("value")-1, $(oS[2]).attr("value"), $(oS[3]).attr("value"), $(oS[4]).attr("value"), 0);
        var e = new Date($(oE[0]).attr("value"), $(oE[1]).attr("value")-1, $(oE[2]).attr("value"), $(oE[3]).attr("value"), $(oE[4]).attr("value"), 0);
        var result =(e.getTime()-s.getTime());
        var time = new Date();
        var resultst = (s.getTime()-time.getTime());
        var resultet = (e.getTime()-time.getTime());

        var Selects=$("#start_input select");
        var oYear=$(Selects[0]).children("option");
        var oMonth=$(Selects[1]).children("option");
        var oDay=$(Selects[2]).children("option");
        var oHour=$(Selects[3]).children("option");
        var oMinutes=$(Selects[4]).children("option");

        var s_year	= time.getFullYear();
        var s_month	= time.getMonth() + 1;
        var s_day	= time.getDate();
        var s_hour	= time.getHours();
        var s_minutes= time.getMinutes();
        var i = 0;
        var n = 0;
        var x = 0;
        var y = 0;
        var z = 0;
        var sYear = 0;
        var sMonth = 0;
        var sDay = 0;
        var sHour = 0;
        var sMinutes = 0;
        if (resultst<0){
            while(i<3){
                sYear=$(oYear[i]).attr("value");
                if(sYear==s_year){
                    $(oS[0]).removeAttr("selected");
                    $(oYear[i]).attr("selected", "true");
                    break;
                }
                i++;
            }
            while(n<12){
                sMonth=$(oMonth[n]).attr("value");
                if(parseInt(sMonth)==s_month){
                    $(oS[1]).removeAttr("selected");
                    $(oMonth[n]).attr("selected", "true");
                    break;
                }
                n++;
            }
            while(x<32){
                sDay=$(oDay[x]).attr("value");
                if(parseInt(sDay)==s_day){
                    $(oS[2]).removeAttr("selected");
                    $(oDay[x]).attr("selected", "true");
                    break;
                }
                x++;
            }
            while(y<61){
                sHour=$(oHour[y]).attr("value");
                if(parseInt(sHour)==s_hour){
                    $(oS[3]).removeAttr("selected");
                    $(oHour[y]).attr("selected", "true");
                    break;
                }
                y++;
            }
            while(z<61){
                sMinutes=$(oMinutes[z]).attr("value");
                if(parseInt(sMinutes)==s_minutes){
                    $(oS[4]).removeAttr("selected");
                    $(oMinutes[z]).attr("selected", "true");
                    break;
                }
                z++;
            }
        }
        if (shou_time <1){
            alert("残り秒数１以上設定してください");
            return false;
        }
        return true;
    }
